﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Amib.Threading;

namespace BaseScrapper
{
   public class Scrapper
    {
        private static SmartThreadPool pool = new SmartThreadPool();
        public static Stopwatch sw = new Stopwatch();

        public Scrapper()
        {
            Initialize();
        }
        private Scrapper _API = null;

        public Scrapper GetAPI(Type t)
        {
            if (_API == null)
                _API = (Scrapper)Activator.CreateInstance(t);
            //_API.Start(Request, Response);

            return _API;
        }


        public void Initialize()
        {


            Init();

            ProcessStart(); //CALL LAST, CALLER WILL HAVE VALID CONNECTION TO USE
        }

        /// <summary>
        /// allows the url of type http://x/y./z
        /// </summary>
        private void AllowTrailingDotsURL()
        {
            try
            {
                //originally felt need for 
                //http://www.woodentoysuk.com/dice-shop-uk-dice-games/dice-games/book-dice-games-properly-explained-by-reiner-knizia.?zenid=5epla5u9tgbb2piqah3vcla5i4

                //http://stackoverflow.com/questions/856885/httpwebrequest-to-url-with-dot-at-the-end
                //this is actually fixed in .NET framework 4.5
                var surl = "http://x/y./z";

                var url2 = new Uri(surl);
                // Console.WriteLine("Broken: " + url2.ToString());

                MethodInfo getSyntax = typeof(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                FieldInfo flagsField = typeof(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                if (getSyntax != null && flagsField != null)
                {
                    foreach (string scheme in new[] { "http", "https" })
                    {
                        UriParser parser = (UriParser)getSyntax.Invoke(null, new object[] { scheme });
                        if (parser != null)
                        {
                            int flagsValue = (int)flagsField.GetValue(parser);
                            // Clear the CanonicalizeAsFilePath attribute
                            if ((flagsValue & 0x1000000) != 0)
                                flagsField.SetValue(parser, flagsValue & ~0x1000000);
                        }
                    }
                }

                url2 = new Uri(surl);
                // Console.WriteLine("Fixed: " + url2.ToString());
            }
            catch (Exception e)
            {

                //LogException("while trying to allow the trailing dots url" + e.Message, e.ToSafeString(), Flow.UNKNOWN);
            }
        }

        protected virtual void ProcessStart()
        {

        }
        /// <summary>
        /// this func is called after all of the processing, before generting xml,summary, exception, closing conn
        /// </summary>
        protected virtual void ProcessFinish()
        {

        }

        private void Init()
        {
            sw.Start();
            //grouping = System.Guid.NewGuid().ToString("N");
            //loggingLevel = ApplicationConstants.LogVerbosity;
            AllowTrailingDotsURL();
            int threads = 100;

            pool.MinThreads = 50;
            pool.MaxThreads = threads;
            pool.Concurrency = 100;
            // Connection(); not required yet
            //ProcessStart();
        }
        private void WaitTillPoolIsIdle()
        {
            while (true)
            {
                //if (Abort)
                //    return;

                if (pool.IsIdle)
                    return;
                Console.WriteLine("WaitTillPoolIsIdle... Sleeping............");
                System.Threading.Thread.Sleep(1000);
            }
        }
        private void Finish()
        {
            // Wait for the completion of all work items
            //pool.WaitForIdle();
            WaitTillPoolIsIdle();
            pool.Shutdown();

            ProcessFinish();
           // GenerateXML(objFeed.Catalogue);
            sw.Stop();// it should be before generating the summary(that will use it)
            //WriteSummaryToFileAndPrint();
            //WriteExceptionsToFile();

            //SQLHelper.ConnectionClose(connection);
        }





        public void ScrapeListing(string url)
        {

        }




    }
}
