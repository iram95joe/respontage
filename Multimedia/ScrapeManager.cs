﻿using BaseScrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Xml;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net;
using System.Collections;
using System.Reflection;
using Core.Helper;

namespace Multimedia
{
    public class ScrapeManager : CommunicationEngine, IDisposable
    {
        #region VARIABLES
        private CommunicationPage commPage = null;
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        static object waitLock = new object();

        #endregion

        #region CONSTRUCTOR

        public ScrapeManager()
        {
            commPage = new CommunicationPage("http://localhost:24585/");
        }

        #endregion

        public Tuple<string, string> GeLongitudeLatitude(string address)
        {
            try
            {
                string api = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}",address,GlobalVariables.GoogleApiKey());
                commPage.RequestURL = api;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;


                ProcessRequest(commPage);

                var ob = JObject.Parse(commPage.Html);
                var geoData = ob["results"][0]["geometry"]["location"];
                return new Tuple<string, string>(geoData["lng"].ToString(), geoData["lat"].ToString());
            }
            catch(Exception e)
            {
            }
            return new Tuple<string, string>(null, null);
        }

        public string GetLocation(string address)
        {
            try
            {
                string api = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}", address, GlobalVariables.GoogleApiKey());
                commPage.RequestURL = api;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                var ob = JObject.Parse(commPage.Html);
                var geoData = ob["results"][0]["address_components"];
                var location = "";

                foreach (var data in geoData)
                {
                    var type = data["types"][0].ToString();
                    switch (type)
                    {
                        case "locality":
                            location += data["long_name"].ToString()+" ";
                            break;
                        case "administrative_area_level_2":
                            location += data["long_name"].ToString()+" ";
                            break;
                        case "country":
                            location += data["long_name"].ToString();
                            break;
                    }
                }
                return location;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        public Tuple<string, string> GetTimeZone(string longitude, string latitude)
        {
            try
            {
                string api = String.Format("https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp=1331161200&key={2}", latitude, longitude, GlobalVariables.GoogleApiKey());
                commPage.RequestURL = api;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;


                ProcessRequest(commPage);

                var ob = JObject.Parse(commPage.Html);

                return new Tuple<string, string>(ob["timeZoneName"].ToString(), ob["timeZoneId"].ToString());
            }
            catch
            {
            }
            return new Tuple<string, string>(null, null);
        }

        #region DISPOSE

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion

        #region CCTV

        public string GetCctvFootage(long listingId,DateTime start, DateTime end)
        {
            try
            {
                commPage.RequestURL = string.Format("{0}/cctv/footage/bylisting?listingId={1}&start={2}&end={3}", GlobalVariables.ApiVideoUrl, listingId,start,end);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    return commPage.Html;
                }
            }
            catch (Exception e)
            {
                return "";
            }

            return "";
        }

        #region ARLO

        public List<string> ArloCctvFootageList(DateTime date, DateTime? endDate = null)
        {
            try
            {
                if (endDate == null)
                    commPage.RequestURL = string.Format("{0}/api/arlo/footage/all?date={1}", GlobalVariables.ApiVideoUrl, date.ToString("yyyy-MM-dd"));
                else
                    commPage.RequestURL = string.Format("{0}/api/arlo/footage/all?date={1}&enddate={2}", GlobalVariables.ApiVideoUrl, date.ToString("yyyy-MM-dd"), ((DateTime)endDate).ToString("yyyy-MM-dd"));

                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    var videos = JArray.Parse(ob["videos"].ToString()).Select(x => x.ToString()).ToList();

                    return videos;
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }

            return new List<string>();
        }

        #endregion

        #region BLINK

        public List<string> BlinkCctvFootageList(DateTime date, DateTime? endDate = null)
        {
            try
            {
                if (endDate == null)
                    commPage.RequestURL = string.Format("{0}/api/blink/footage/all?date={1}", GlobalVariables.ApiVideoUrl, date.ToString("yyyy-MM-dd"));
                else
                    commPage.RequestURL = string.Format("{0}/api/blink/footage/all?date={1}&enddate={2}", GlobalVariables.ApiVideoUrl, date.ToString("yyyy-MM-dd"), ((DateTime)endDate).ToString("yyyy-MM-dd"));

                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    var videos = JArray.Parse(ob["videos"].ToString()).Select(x => x.ToString()).ToList();

                    return videos;
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }

            return new List<string>();
        }

        #endregion


        #endregion
    }
}
