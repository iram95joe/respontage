USE [PropertyDB]
GO
/****** Object:  Table [dbo].[BookingStatus]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookingStatus](
	[Code] [nvarchar](5) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BookingStatus] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cashflow]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cashflow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CashFlowTypeId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Amount] [float] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[PaymentDate] [datetime] NULL,
 CONSTRAINT [PK_Cashflow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CashFlowType]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CashFlowType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CashFlowTypeName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_CashFlowType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Company]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Expense]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expense](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[UnitId] [int] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[BookingId] [int] NOT NULL,
	[PropertyOwnerId] [int] NOT NULL,
	[PropertyManagerId] [int] NOT NULL,
	[WorkerAssignmentId] [int] NOT NULL,
	[RenterId] [int] NOT NULL,
	[BillId] [int] NOT NULL,
	[PaymentMethodId] [int] NOT NULL,
	[Frequency] [int] NOT NULL,
	[DueDateDMn] [int] NOT NULL,
	[DueDateMQt] [int] NOT NULL,
	[DueDateDQt] [int] NOT NULL,
	[DueDateMYr] [int] NOT NULL,
	[DueDateDYr] [int] NOT NULL,
	[DueDate] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[PaymentDate] [datetime] NULL,
	[DateLastJobRun] [datetime] NULL,
	[DateNextJobRun] [datetime] NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[IsAutoCreate] [bit] NOT NULL,
	[IsOwnerExpense] [bit] NOT NULL,
	[IsRenterExpense] [bit] NOT NULL,
	[IsPreDeducted] [bit] NOT NULL,
	[IsPaid] [bit] NOT NULL,
	[FeeTypeId] [int] NOT NULL,
	[ExpenseCategoryId] [int] NOT NULL,
	[Receipts] [nvarchar](max) NULL,
	[LeaseID] [int] NOT NULL,
	[IsStartupCost] [bit] NOT NULL,
	[IsCapitalExpense] [bit] NOT NULL,
	[IsRefund] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExpenseCategory]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpenseCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExpenseCategoryType] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_ExpenseCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExpensePayment]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpensePayment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExpenseId] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
 CONSTRAINT [PK_ExpensePayment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FeeType]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeeType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fee_Type] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_FeeType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Guest]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GuestId] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[ProfilePictureUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Guests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Host]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Host](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteHostId] [nvarchar](50) NOT NULL,
	[SiteType] [int] NOT NULL,
	[Firstname] [nvarchar](255) NOT NULL,
	[Lastname] [nvarchar](255) NOT NULL,
	[ProfilePictureUrl] [nvarchar](255) NULL,
	[Username] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[AutoSync] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_Host] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HostSessionCookie]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HostSessionCookie](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HostId] [int] NOT NULL,
	[Token] [varchar](max) NOT NULL,
	[Cookies] [varchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
 CONSTRAINT [PK_HostSessionCookie] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inbox]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inbox](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[HostId] [int] NOT NULL,
	[ThreadId] [nvarchar](50) NOT NULL,
	[GuestId] [int] NOT NULL,
	[PropertyId] [bigint] NOT NULL,
	[IsArchived] [bit] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[HasUnread] [bit] NOT NULL,
	[LastMessage] [nvarchar](max) NOT NULL,
	[LastMessageAt] [datetime] NOT NULL,
	[CheckInDate] [datetime] NULL,
	[CheckOutDate] [datetime] NULL,
	[GuestCount] [int] NOT NULL,
	[IsInquiryOnly] [bit] NOT NULL,
	[IsSpecialOfferSent] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[RentalCost] [decimal](18, 2) NOT NULL,
	[CleaningCost] [decimal](18, 2) NOT NULL,
	[GuestFee] [decimal](18, 2) NOT NULL,
	[ServiceFee] [decimal](18, 2) NOT NULL,
	[Subtotal] [decimal](18, 2) NOT NULL,
	[GuestPay] [decimal](18, 2) NOT NULL,
	[HostEarn] [decimal](18, 2) NOT NULL,
	[StatusType] [int] NOT NULL,
	[CanPreApproveInquiry] [bit] NOT NULL,
	[CanWithdrawPreApprovalInquiry] [bit] NOT NULL,
	[CanDeclineInquiry] [bit] NOT NULL,
	[InquiryId] [nvarchar](50) NULL,
	[SiteType] [int] NOT NULL,
 CONSTRAINT [PK_Inbox] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Income]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Income](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IncomeTypeId] [int] NOT NULL,
	[BookingId] [int] NULL,
	[PropertyId] [int] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [int] NOT NULL,
	[PaymentDate] [datetime] NULL,
	[CompanyId] [int] NOT NULL,
	[DateRecorded] [datetime] NOT NULL,
	[IsBatchLoad] [bit] NOT NULL,
 CONSTRAINT [PK_Income] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IncomeType]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IncomeType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_IncomeType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inquiry]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inquiry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HostId] [int] NOT NULL,
	[ReservationId] [nvarchar](50) NULL,
	[PropertyId] [int] NOT NULL,
	[GuestId] [int] NOT NULL,
	[ConfirmationCode] [nvarchar](50) NULL,
	[BookingStatusCode] [nvarchar](50) NOT NULL,
	[CheckInDate] [datetime] NULL,
	[CheckOutDate] [datetime] NULL,
	[Nights] [int] NOT NULL,
	[ReservationCost] [decimal](18, 2) NOT NULL,
	[SumPerNightAmount] [decimal](18, 2) NOT NULL,
	[CleaningFee] [decimal](18, 2) NOT NULL,
	[ServiceFee] [decimal](18, 2) NOT NULL,
	[TotalPayout] [decimal](18, 2) NOT NULL,
	[NetRevenueAmount] [decimal](18, 2) NOT NULL,
	[PayoutDate] [datetime] NULL,
	[InquiryDate] [datetime] NULL,
	[BookingDate] [datetime] NULL,
	[BookingCancelDate] [datetime] NULL,
	[BookingConfirmDate] [datetime] NULL,
	[BookingConfirmCancelDate] [datetime] NULL,
	[GuestCount] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsAltered] [bit] NOT NULL,
	[IsImported] [bit] NOT NULL,
	[SiteType] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[ThreadId] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Inquiry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Message]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MessageId] [nvarchar](50) NOT NULL,
	[ThreadId] [nvarchar](50) NOT NULL,
	[MessageContent] [nvarchar](max) NOT NULL,
	[IsMyMessage] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Note]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Note](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ReservationId] [int] NOT NULL,
	[NoteType] [int] NOT NULL,
	[InquiryNote] [nvarchar](max) NULL,
	[FlightNumber] [nvarchar](max) NULL,
	[ArrivalDateTime] [datetime] NULL,
	[DepartureDateTime] [datetime] NULL,
	[DepartureCityCode] [nvarchar](max) NULL,
	[LeavingDate] [datetime] NULL,
	[ArrivalAirportCode] [nvarchar](max) NULL,
	[MeetingPlace] [nvarchar](max) NULL,
	[MeetingDateTime] [datetime] NULL,
	[People] [nvarchar](max) NULL,
	[Airline] [nvarchar](max) NULL,
	[NumberOfPeople] [int] NULL,
	[CarType] [nvarchar](max) NULL,
	[CarColor] [nvarchar](max) NULL,
	[PlateNumber] [nvarchar](max) NULL,
	[SourceCity] [nvarchar](max) NULL,
	[CruiseName] [nvarchar](max) NULL,
	[AirportCodeDeparture] [nvarchar](max) NULL,
	[AirportCodeArrival] [nvarchar](max) NULL,
	[IdentifiableFeature] [nvarchar](max) NULL,
	[DepartureCity] [nvarchar](max) NULL,
	[DeparturePort] [nvarchar](max) NULL,
	[ArrivalPort] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbNote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Property]    Script Date: 9/21/2017 1:06:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Property](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ListingId] [bigint] NOT NULL,
	[ListingUrl] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Longitude] [nvarchar](50) NULL,
	[Latitude] [nvarchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[UnitCount] [int] NOT NULL,
	[MinStay] [int] NOT NULL,
	[Internet] [nvarchar](50) NULL,
	[Pets] [nvarchar](50) NULL,
	[WheelChair] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Reviews] [int] NOT NULL,
	[Sleeps] [int] NOT NULL,
	[Bedrooms] [int] NOT NULL,
	[Bathrooms] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[PropertyType] [nvarchar](50) NULL,
	[AccomodationType] [nvarchar](50) NULL,
	[Smoking] [nvarchar](50) NULL,
	[AirCondition] [nvarchar](50) NULL,
	[SwimmingPool] [nvarchar](50) NULL,
	[PriceNightlyMin] [nvarchar](50) NULL,
	[PriceNightlyMax] [nvarchar](50) NULL,
	[PriceWeeklyMin] [nvarchar](50) NULL,
	[PriceWeeklyMax] [nvarchar](50) NULL,
	[Images] [nvarchar](max) NULL,
	[Status] [nvarchar](50) NULL,
	[MonthlyExpense] [decimal](18, 2) NOT NULL,
	[ExpenseNotes] [nvarchar](max) NULL,
	[ActiveDateStart] [datetime] NULL,
	[ActiveDateEnd] [datetime] NULL,
	[CommissionId] [int] NOT NULL,
	[CommissionAmount] [decimal](18, 2) NOT NULL,
	[AutoCreateIncomeExpense] [bit] NOT NULL,
	[CheckInTime] [time](7) NOT NULL,
	[CheckOutTime] [time](7) NOT NULL,
	[IsLocallyCreated] [bit] NOT NULL,
	[IsParentProperty] [bit] NOT NULL,
	[ParentPropertyId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[SiteType] [int] NOT NULL,
 CONSTRAINT [PK_Property] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PropertyFee]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyFee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeeTypeId] [int] NOT NULL,
	[ComputeBasis] [int] NOT NULL,
	[Show] [bit] NOT NULL,
	[Method] [int] NOT NULL,
	[AmountPercent] [decimal](18, 2) NOT NULL,
	[PropertyId] [int] NOT NULL,
	[InclusiveOrExclusive] [int] NOT NULL,
	[IsPreDeducted] [bit] NOT NULL,
	[CleaningFee] [decimal](18, 2) NOT NULL,
	[ServiceFee] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_PropertyFee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PropertyType]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropertyType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Property_Type] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_PropertyType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SiteType]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiteType](
	[Code] [nvarchar](3) NOT NULL,
	[Description] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbRole]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbRoleLink]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbRoleLink](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_tbRoleLink] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbRolePage]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbRolePage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[PageName] [nvarchar](550) NULL,
	[PageUrl] [nvarchar](550) NULL,
	[Status] [bit] NULL,
	[IconPath] [nvarchar](250) NULL,
	[PageOrder] [int] NOT NULL,
 CONSTRAINT [PK_tbRolePage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplateMessage]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](80) NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TemplateMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[Password] [nvarchar](max) NULL,
	[CompanyId] [int] NULL,
	[TimeZone] [nvarchar](2000) NULL,
	[ConfirmEmail] [bit] NULL,
	[ValidStatus] [bit] NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Worker]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Worker](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [nvarchar](80) NOT NULL,
	[Lastname] [nvarchar](80) NOT NULL,
	[Address] [nvarchar](255) NOT NULL,
	[Mobile] [nvarchar](50) NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_Worker] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkerAssignment]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkerAssignment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkerId] [int] NOT NULL,
	[JobTypeId] [int] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[InquiryId] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_WorkerAssignment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkerException]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkerException](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkerId] [int] NOT NULL,
	[ExceptionStart] [datetime] NOT NULL,
	[ExceptionEnd] [datetime] NOT NULL,
	[ExceptionType] [nvarchar](max) NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_WorkerException] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkerJob]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkerJob](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkerId] [int] NOT NULL,
	[JobTypeId] [int] NOT NULL,
	[PaymentType] [int] NOT NULL,
	[PaymentRate] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_WorkerJob] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkerJobType]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkerJobType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_WorkerJobType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkerSchedule]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkerSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UniqueId] [int] NOT NULL,
	[WorkerId] [int] NOT NULL,
	[AvailDay] [nvarchar](max) NULL,
	[AvailTimeStart] [nvarchar](max) NOT NULL,
	[AvailTimeEnd] [nvarchar](max) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_WorkerSchedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkerType]    Script Date: 9/21/2017 1:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkerType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_WorkerType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[BookingStatus] ([Code], [Description]) VALUES (N'A', N'Confirmed')
INSERT [dbo].[BookingStatus] ([Code], [Description]) VALUES (N'B', N'Booking')
INSERT [dbo].[BookingStatus] ([Code], [Description]) VALUES (N'BC', N'Booking-Cancelled')
INSERT [dbo].[BookingStatus] ([Code], [Description]) VALUES (N'C', N'Confirmed-Cancelled')
INSERT [dbo].[BookingStatus] ([Code], [Description]) VALUES (N'I', N'Inquiry')
INSERT [dbo].[BookingStatus] ([Code], [Description]) VALUES (N'IC', N'Inquiry-Cancelled')
SET IDENTITY_INSERT [dbo].[Cashflow] ON 

INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (11, 3, CAST(N'2017-04-19T22:16:01.593' AS DateTime), 3434, 2, 38, CAST(N'2017-04-21T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (15, 1, CAST(N'2017-08-28T01:10:21.023' AS DateTime), 400000, 1, 2, CAST(N'2017-01-04T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (16, 2, CAST(N'2017-08-28T01:11:54.090' AS DateTime), 50000, 1, 2, CAST(N'2017-01-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (17, 3, CAST(N'2017-08-28T01:12:25.920' AS DateTime), 200000, 1, 2, CAST(N'2017-06-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (18, 2, CAST(N'2017-08-28T04:30:21.223' AS DateTime), 200000, 1, 1, CAST(N'2017-01-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (19, 1, CAST(N'2017-08-28T04:30:52.187' AS DateTime), 1000000, 1, 1, CAST(N'2017-01-04T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (20, 4, CAST(N'2017-08-28T06:15:05.090' AS DateTime), 57, 1, 2, CAST(N'2017-06-13T00:00:00.000' AS DateTime))
INSERT [dbo].[Cashflow] ([Id], [CashFlowTypeId], [DateCreated], [Amount], [CompanyId], [PropertyId], [PaymentDate]) VALUES (21, 5, CAST(N'2017-08-28T06:15:24.417' AS DateTime), 64, 1, 2, CAST(N'2017-05-16T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Cashflow] OFF
SET IDENTITY_INSERT [dbo].[CashFlowType] ON 

INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (1, N'Starting Cash')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (2, N'New Investment')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (3, N'Partner Investment')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (4, N'Write Off')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (5, N'Cash Out')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (6, N'Partner Draw')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (7, N'Partner Investment Draw')
INSERT [dbo].[CashFlowType] ([Id], [CashFlowTypeName]) VALUES (8, N'Less Owner Draw')
SET IDENTITY_INSERT [dbo].[CashFlowType] OFF
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([Id], [CompanyName]) VALUES (1, N'Test1')
INSERT [dbo].[Company] ([Id], [CompanyName]) VALUES (2, N'Test2')
SET IDENTITY_INSERT [dbo].[Company] OFF
SET IDENTITY_INSERT [dbo].[ExpenseCategory] ON 

INSERT [dbo].[ExpenseCategory] ([Id], [ExpenseCategoryType], [Type], [CompanyId]) VALUES (1, N'Booking Fee', 2, 1)
INSERT [dbo].[ExpenseCategory] ([Id], [ExpenseCategoryType], [Type], [CompanyId]) VALUES (2, N'Sales Tax', 2, 1)
INSERT [dbo].[ExpenseCategory] ([Id], [ExpenseCategoryType], [Type], [CompanyId]) VALUES (7, N'Test', 1, 1)
INSERT [dbo].[ExpenseCategory] ([Id], [ExpenseCategoryType], [Type], [CompanyId]) VALUES (1007, N'Labor', 1, 0)
SET IDENTITY_INSERT [dbo].[ExpenseCategory] OFF
SET IDENTITY_INSERT [dbo].[FeeType] ON 

INSERT [dbo].[FeeType] ([Id], [Fee_Type]) VALUES (1, N'Airbnb Fee')
INSERT [dbo].[FeeType] ([Id], [Fee_Type]) VALUES (2, N'HST')
INSERT [dbo].[FeeType] ([Id], [Fee_Type]) VALUES (3, N'Vat')
INSERT [dbo].[FeeType] ([Id], [Fee_Type]) VALUES (4, N'Cleaning Fee')
SET IDENTITY_INSERT [dbo].[FeeType] OFF
SET IDENTITY_INSERT [dbo].[Guest] ON 

INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (1, N'0926998e-4531-4b9d-8e34-fd377d15549d', N'Bev Nash', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (2, N'f8db0749-a495-4b85-abd6-0fb345775e20', N'Mark Rogan', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (3, N'fc9ae6d3-15a2-49e6-8e2e-ad85db2c8f5f', N'Gretel Cruz', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (4, N'077fd519-f35a-4e1e-9cc0-a3e7f29b6c26', N'Frank Klingler', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (5, N'ceeee6c5-74f9-4f35-aa89-b9661ea88942', N'Micah Wright', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (6, N'35375efd-e6a4-496e-aeca-189dded41849', N'Michael Johnson', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (7, N'7de423b3-3409-47d1-8cd2-b99ff0e54c70', N'pamela parker', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (8, N'b2ef132d-4f44-4da4-9611-0d908257d519', N'Shirley Keys', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (9, N'959abfbc-df45-49b4-b496-3131a4788f0d', N'Catherine Bates', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (10, N'0191bf09-2a56-4298-9a3d-cd3a45dd551e', N'Jasmine Henderson', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (11, N'd9397798-9d51-4ecd-997f-33f5f75cb2a7', N'Shakeya Knight', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (12, N'fa73b5b2-90ac-4fe1-9b32-64c228400f9f', N'Renetta Hood', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (13, N'6240459a-f785-4295-a6c5-d764af7964f0', N'Ravette Taylor', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (14, N'0696406d-8041-43d0-8b5f-e7381868b138', N'Tsering Wangden', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (15, N'5a41a825-6991-40af-95da-15759079ffa5', N'jamelia brown', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (16, N'cbac6174-09f1-45f6-8dd0-de7dda965491', N'Jose Leon', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (17, N'fe8f8fc2-2deb-4864-86d0-7a3c0aba48e4', N'Mareyka Webb', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (18, N'3103746b-e93d-40b2-90dc-a62d789a58b7', N'Travis Ewings', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (19, N'0898b04a-43e1-454a-95bd-3833bc540545', N'meredith mckinney', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (20, N'a4da1f73-782f-4779-af69-98398109681a', N'Robert  Edwards ', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (21, N'6b973e5e-8299-4e4d-91f8-0c400ef3b459', N'Comerly  Jackson', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (22, N'17941cd5-1c68-4540-9fa2-b0b0b71e30aa', N'Apryl Lynn', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (23, N'0a7152fc-5b42-4bfa-8cdf-94789cd037f8', N'Stacie Ngo', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (24, N'40a8febf-50d2-44a7-8970-78f18d817fbf', N'Crystal Boss Lady', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (25, N'b0601cfd-c1ba-4029-a82b-c65b15f523fd', N'Jamie Gutierrez', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (26, N'f0334452-9def-49f2-933f-aca4f37f3b48', N'gary white', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (27, N'589466b1-db18-44a6-ac62-5ff5a328f21d', N'Pelly Nikky', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (28, N'3d082d62-dd74-4936-a64e-e6ce2ace6047', N'Kendrick Khoe', NULL)
INSERT [dbo].[Guest] ([Id], [GuestId], [Name], [ProfilePictureUrl]) VALUES (29, N'9043ec1f-b9f7-4807-b639-44e99a57a7f0', N'Melanie Reese', NULL)
SET IDENTITY_INSERT [dbo].[Guest] OFF
SET IDENTITY_INSERT [dbo].[Host] ON 

INSERT [dbo].[Host] ([Id], [SiteHostId], [SiteType], [Firstname], [Lastname], [ProfilePictureUrl], [Username], [Password], [AutoSync], [CompanyId]) VALUES (1, N'a645c082d0844783a87655c974c5184a', 2, N'Kevin', N'Chu', NULL, N'vancouverbb99@gmail.com', N'8Bm6q5gFLLcvlT2Jwd6G+Zs/tKts3Ru/nfT8r8gm8p0=', 1, 1)
SET IDENTITY_INSERT [dbo].[Host] OFF
SET IDENTITY_INSERT [dbo].[HostSessionCookie] ON 

INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (46, 1, N'MaHyalnTJp', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:45+08:00","Name":"abb_fa2","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"%7B%22user_id%22%3A%228%7C1%7CiSIET6HXEgRxYRmgnKmm0fWc6ooL3GuHhQ2oHHWXkLuk3NuS2zEtXA%3D%3D%22%7D","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.airbnb.com","Expired":false,"Expires":"2017-09-11T16:39:45+08:00","Name":"jlp3","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"true","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:47+08:00","Name":"hli","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T15:39:56.8847254+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:47+08:00","Name":"har","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T15:39:56.8847254+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:49+08:00","Name":"bev","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:40:01.9781281+08:00","Value":"1505115581_%2FEQUlG%2BrpeMTwFnM","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"_csrf_token","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"V4%24.airbnb.com%24hFELsgMsrLM%24SCO-M030jn4ufW-HL4y98nf7s0M3pIROZnbxJzAFf24%3D","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"_user_attributes","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:40:01.9781281+08:00","Value":"%7B%22curr%22%3A%22CAD%22%2C%22guest_exchange%22%3A1.216015%2C%22device_profiling_session_id%22%3A%221505115582--d9cc736d6a6decc53b13de3d%22%2C%22giftcard_profiling_session_id%22%3A%221505115582--8717f8c5a844223637f2f6f0%22%2C%22reservation_profiling_session_id%22%3A%221505115582--ddc79a022e2297ab6a6ff546%22%2C%22id%22%3A24056456%2C%22hash_user_id%22%3A%2284b5e2db065748dbe75465fdfe1db76e084da950%22%2C%22eid%22%3A%22I2-JC9tUrtWV6LOgaFcNQw%3D%3D%22%2C%22num_msg%22%3A0%2C%22num_notif%22%3A6%2C%22num_alert%22%3A18%2C%22num_h%22%3A2%2C%22num_pending_requests%22%3A0%2C%22num_trip_notif%22%3A0%2C%22name%22%3A%22Gretel%22%2C%22num_action%22%3A0%2C%22pellet_to%22%3A10%2C%22is_admin%22%3Afalse%2C%22can_access_photography%22%3Afalse%7D","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"flags","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"295051524","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:46+08:00","Name":"_airbed_session_id","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"51a7d6f0040462174913220ef75eb7c0","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"li","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:49+08:00","Name":"_pt","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:40:01.9781281+08:00","Value":"1--WyI4NGI1ZTJkYjA2NTc0OGRiZTc1NDY1ZmRmZTFkYjc2ZTA4NGRhOTUwIl0%3D--3b670dc02756372ad960fcd740c5530b3f9a3924","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"2019-09-11T15:39:45+08:00","Name":"rclmd","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"%7B%2224056456%22%3D%3E%22email%22%7D","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".airbnb.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"roles","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T15:39:54.8731494+08:00","Value":"0","Version":0}]', 1, CAST(N'2017-09-11T15:40:09.277' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (52, 0, N'MmLKnDibbX', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-11T20:32:42+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:49.7735711+08:00","Value":"90ef7675-1394-4df3-b1be-f67aa95dec7d","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T20:32:42+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:49.7735711+08:00","Value":"51107750-b503-470f-8c36-bc28ce8ddaf2","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T20:32:42+08:00","Name":"51107750-b503-470f-8c36-bc28ce8ddaf2SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:49.7735711+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T20:32:42+08:00","Name":"51107750-b503-470f-8c36-bc28ce8ddaf2AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:49.7735711+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:54.5571729+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:54.5583491+08:00","Value":"fokbf0ge8q331a2n535m7g82e","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-11T20:32:54.5583491+08:00","Value":"tgt-355903-YpnX9RsxoTqajaGBo5MIh9zEDoGF5jbRLMgZNcsefIbxmsM3o5-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-11T20:32:45+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"9e0dd206-74de-477f-a81c-86bd102cb192","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T20:32:45+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"9dff63b5-f9dc-468c-8f8c-18c4e97a0bd9","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T20:32:45+08:00","Name":"9dff63b5-f9dc-468c-8f8c-18c4e97a0bd9SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T20:32:45+08:00","Name":"9dff63b5-f9dc-468c-8f8c-18c4e97a0bd9AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T20:32:48.5586666+08:00","Value":"46881669-4a05-4a1f-ae0a-5ac9be4a3693","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-11T01:23:02+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:48.5586666+08:00","Value":"W1o9LohwReWNBcN0ZqkznmiCtlkAAAAAQUIPAAAAAAAYXjOB/lo6RSGEGgbaiFV+","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:48.559672+08:00","Value":"yxefBLVMWgiUrhal/g40/wAAAADpwyxLZ+2fcHuyrvRN1tQ8","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:48.559672+08:00","Value":"w6nXGGMsC23QZkIot8/0BWmCtlkAAAAANXuBMjm/DJuJw2qrULECrw==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-11T01:23:09+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"ZkVYdvGyR2e5Y/urNLMggm2CtlkAAAAAQUIPAAAAAADWaw9PQGEYuN6+m7631XaZ","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"79cHUx7whHgt4Ydvv0PQ0AAAAABCLGfM/pFsXGkaAoMbioZi","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T20:32:52.9949547+08:00","Value":"OKC7KMH8wngyakIot8/0BW2CtlkAAAAA0axPFjDLht1pdb15i1oP6Q==","Version":0}]', 1, CAST(N'2017-09-11T20:32:55.017' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (53, 0, N'e95yFqPsJp', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-11T21:26:56+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:03.4149707+08:00","Value":"f508b588-d924-453d-a6a9-7a9d736b0962","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T21:26:56+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:03.4149707+08:00","Value":"727c04e6-73ad-4bc2-afce-1e2a2ffbc5cc","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T21:26:56+08:00","Name":"727c04e6-73ad-4bc2-afce-1e2a2ffbc5ccSL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:03.4149707+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T21:26:56+08:00","Name":"727c04e6-73ad-4bc2-afce-1e2a2ffbc5ccAVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:03.4149707+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:07.7726293+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:07.7726293+08:00","Value":"10aom6go4j3j31erglm7i3gxl2","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-11T21:27:07.7726293+08:00","Value":"tgt-361826-GC2WuVk7uItMWBaTaIpWR3KluYQ4cbpbej0iqznPYmpvi9FBQd-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-11T21:26:58+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"e59cf553-4909-4dfe-869e-fdb3fa828183","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T21:26:58+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"acd2ef66-f9cd-489f-bc79-e936437b3094","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T21:26:58+08:00","Name":"acd2ef66-f9cd-489f-bc79-e936437b3094SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T21:26:58+08:00","Name":"acd2ef66-f9cd-489f-bc79-e936437b3094AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T21:27:01.2844297+08:00","Value":"466d1bc3-1cfa-4c16-b18e-33168c55ea0a","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-11T01:58:58+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:01.2844297+08:00","Value":"jCMIAeGnRnipTv5p2pkCbxuPtlkAAAAAQUIPAAAAAAAD6eCyVAamcArnahWnRV0f","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:01.2854315+08:00","Value":"RGIpXBF0bTTE9luT/g40/wAAAAAIfiw1zvLnxkiS7evfPz1U","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_628_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:01.2854315+08:00","Value":"ODc2GlrROAvJjg6zCxu3CB2PtlkAAAAALQIg5rjO79dJjSZjQn88Dw==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-11T01:58:58+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"+cayze+0QCeKZhL4eGnPliGPtlkAAAAAQUIPAAAAAADUC8tHOViLGwXsFfbqSNbc","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"3tDMHVRsLguywh09v0PQ0AAAAADQV1V9Pe492OVXpqT/bmjD","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_628_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:27:05.8740809+08:00","Value":"aQ/aFUd9C3HXkw6zCxu3CCGPtlkAAAAAu9CQ2xPAqZr72Uvqws635A==","Version":0}]', 1, CAST(N'2017-09-11T21:27:08.230' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (54, 0, N'MdrNtP03NR', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-11T21:46:23+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:31.1384655+08:00","Value":"a7cc6d62-d53e-412e-8819-bd751e36aa28","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T21:46:23+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:31.1384655+08:00","Value":"61d1367f-313d-4629-bcbc-942a545f5bdf","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T21:46:23+08:00","Name":"61d1367f-313d-4629-bcbc-942a545f5bdfSL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:31.1384655+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-12T21:46:23+08:00","Name":"61d1367f-313d-4629-bcbc-942a545f5bdfAVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:31.1384655+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:35.0849017+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:35.0849017+08:00","Value":"1fyyp6c703ayy1htceyq3yxoti","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-11T21:46:35.0849017+08:00","Value":"tgt-358449-0UUbRd3h0D64XRCethPXvLWVMWVbzMpVVV4lC9vdW0GL7F6s6B-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-11T21:46:25+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"ab0fb3f7-67a0-4e3e-a68b-a8f5534b323d","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T21:46:25+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"d9c564cc-cbd7-42ea-ba2f-0c819a1b8624","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T21:46:25+08:00","Name":"d9c564cc-cbd7-42ea-ba2f-0c819a1b8624SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-12T21:46:25+08:00","Name":"d9c564cc-cbd7-42ea-ba2f-0c819a1b8624AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-11T21:46:29.9009693+08:00","Value":"14441ef8-0344-4c5b-bd2b-04b03bdc0f30","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-11T01:58:41+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:29.9009693+08:00","Value":"DUAv6wV2R6e/kw1neROV2a2TtlkAAAAAQUIPAAAAAADsg5wBRl1KK2Iex/UnE3ZG","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:29.9019593+08:00","Value":"y8aeIlhj5Dx+zlAH/g40/wAAAAAlgvEDMMvzOv6tg0lxki2X","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_628_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:29.9019593+08:00","Value":"joaMXD0YqAlK4RKzCxu3CK2TtlkAAAAAO0t8ANJixGI+S5aNlwA2mA==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-11T01:23:09+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"n9JmsOxIS9KSLFKvWGe/ybGTtlkAAAAAQUIPAAAAAABlv8SsaFEcoqepw+Vxfs2Q","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"G8tyPzIZZg9Jo5eLv0PQ0AAAAAB+fKCKKucemyfhkoI28+RG","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-11T21:46:33.4582697+08:00","Value":"KkbwHxE5HDIpf00ot8/0BbGTtlkAAAAAfX3Z/mUb/mZJHoNpoAqwfg==","Version":0}]', 1, CAST(N'2017-09-11T21:46:35.717' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (56, 0, N'aXDeumGXzh', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-12T20:12:04+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:13.6634269+08:00","Value":"ba825d0f-d61e-45b3-99ce-d6d83e9af7df","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T20:12:04+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:13.6634269+08:00","Value":"38dec109-a314-4265-9cf8-5c78f93a4d57","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T20:12:04+08:00","Name":"38dec109-a314-4265-9cf8-5c78f93a4d57SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:13.6634269+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T20:12:04+08:00","Name":"38dec109-a314-4265-9cf8-5c78f93a4d57AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:13.6634269+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:18.65059+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:18.6516119+08:00","Value":"mgximlqw1v8t98noujpsglp2","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-12T20:12:18.6516119+08:00","Value":"tgt-468437-MFw9BmIAniszZ5k5ZUbz1IFbvZRCdoHbr5HB3jg1d4zRMS4cjg-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-12T20:12:06+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"e0c84e87-f99d-4c98-9058-d7b981c67ed6","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T20:12:06+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"8efcbd55-3414-459d-8a7d-80f789c7a13d","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T20:12:06+08:00","Name":"8efcbd55-3414-459d-8a7d-80f789c7a13dSL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T20:12:06+08:00","Name":"8efcbd55-3414-459d-8a7d-80f789c7a13dAVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-12T20:12:11.899414+08:00","Value":"26eaec08-4aac-421e-a0e7-028988a48e48","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-12T01:23:03+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:11.899414+08:00","Value":"o4XgIVp1TTepSct+gF90fRDPt1kAAAAAQUIPAAAAAADupjQHe6/T1oBqZtAPMzvC","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:11.899414+08:00","Value":"Qh6eS8jlEHCrIxZ6/g40/wAAAABXwzCMIUWtTHPufXzXulgc","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:11.899414+08:00","Value":"r8fNL5eOYQVNsvcot8/0BRHPt1kAAAAAuGK5vq3V2cYT+Z5LYVKoIw==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-12T01:58:59+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"N0SofKV1RXyQtHYXtNylEhbPt1kAAAAAQUIPAAAAAADhUwDsjvWNkWfYl3lGhAii","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"T6w/UEPqrSZg7k1Iv0PQ0AAAAACrSxMMa0jePB4FEPd47Zjq","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_628_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T20:12:15.8427006+08:00","Value":"DqMOZSlBvRMNnjS0Cxu3CBbPt1kAAAAAZSyFFo2Pf0Vak5OdpGcgTw==","Version":0}]', 1, CAST(N'2017-09-12T20:12:19.107' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (57, 0, N'g4rhfU5Vfd', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-12T23:04:55+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:04.732678+08:00","Value":"af120d01-799e-42bc-ab98-afb0443e2df5","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T23:04:55+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:04.732678+08:00","Value":"32db08be-016a-4493-8095-0f0ee0a6289f","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T23:04:55+08:00","Name":"32db08be-016a-4493-8095-0f0ee0a6289fSL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:04.732678+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T23:04:55+08:00","Name":"32db08be-016a-4493-8095-0f0ee0a6289fAVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:04.7331786+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:08.5086777+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:08.5086777+08:00","Value":"bigg2j4aany5aurl6tr4ozwp","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-12T23:05:08.5086777+08:00","Value":"tgt-489644-BLOQZmsUQ1D52CUtGshGTaIIX6Wjs47kIiUdS0StdJJ4un0mqe-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-12T23:04:57+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"5c488ba9-6d5c-4be0-92c4-6fda08576802","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T23:04:57+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"4a886fae-c6f9-42f4-8f8b-71a32a0a6b38","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T23:04:57+08:00","Name":"4a886fae-c6f9-42f4-8f8b-71a32a0a6b38SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T23:04:57+08:00","Name":"4a886fae-c6f9-42f4-8f8b-71a32a0a6b38AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-12T23:05:00.2727886+08:00","Value":"c5e9b47a-c718-4e64-881f-6074e1add39e","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-12T01:58:59+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:00.2727886+08:00","Value":"IUBXoon4TR+nhX1a2si4DJH3t1kAAAAAQUIPAAAAAABPM2W+vNn9k9FFBGI8iisl","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:00.2732887+08:00","Value":"MHAdOuGSLxMaxr8B/g40/wAAAAA5hyQ3gVYYwpUQ7Xm2xMOz","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_628_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:00.2732887+08:00","Value":"yhz2XccmoTca9le0Cxu3CJL3t1kAAAAANXvDRjUpF3jcSQNfLOwqqg==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-12T01:23:03+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"nuIJ+kXuR0Gn5848LkpumJn3t1kAAAAAQUIPAAAAAADunIhy4y6i43sPqa9MTIe/","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"h/UqS79352foGclyv0PQ0AAAAACfMNWdYwPW30uk7koY0W4b","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:05:06.7104432+08:00","Value":"zNHYPILzaW8haxEpt8/0BZn3t1kAAAAAnfzqLbdp3/m8EPTdLjYZZg==","Version":0}]', 1, CAST(N'2017-09-12T23:05:08.993' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (58, 0, N'BuFz6ltzad', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-12T23:19:52+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:01.5037492+08:00","Value":"8bf4901c-cc7a-4351-891b-7a3b6a732d29","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T23:19:52+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:01.5042503+08:00","Value":"c725cb6e-4cba-4744-9ba9-f185da5d317e","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T23:19:52+08:00","Name":"c725cb6e-4cba-4744-9ba9-f185da5d317eSL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:01.5042503+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-13T23:19:52+08:00","Name":"c725cb6e-4cba-4744-9ba9-f185da5d317eAVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:01.5042503+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:05.0692882+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:22:37.7279931+08:00","Value":"1k88oyzrt77nr1ogjrcklf796n","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-12T23:20:05.0692882+08:00","Value":"tgt-489774-mGy1GQj5hsQXZubrzO3MmeCUQYDvGkDsvFPGv4FoEEZAp6f9rx-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-12T23:19:54+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4614921+08:00","Value":"36f3370c-54b4-4f49-965b-42538f3e78bf","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T23:19:54+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4614921+08:00","Value":"e491b5d9-fcd0-4b47-a4d2-933403e5fdae","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T23:19:54+08:00","Name":"e491b5d9-fcd0-4b47-a4d2-933403e5fdaeSL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4614921+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-13T23:19:54+08:00","Name":"e491b5d9-fcd0-4b47-a4d2-933403e5fdaeAVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4614921+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-12T23:20:00.3636178+08:00","Value":"b8aae1c8-2aae-430f-b5a6-52aadcea38e9","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-12T01:23:10+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:00.3636178+08:00","Value":"m8wzlAyeT6efeWFlb4rRmRb7t1kAAAAAQUIPAAAAAABvOz3MfhqlnDJinXKtLtup","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:00.3641169+08:00","Value":"xPZRL7XAy0Box/sw/g40/wAAAACUiPQQIk28aik6bgtIoRi0","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:00.3641169+08:00","Value":"x4lqHCCd23gLpxMpt8/0BRf7t1kAAAAAiKinMMfhXiZpX4s9v749yA==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-12T01:23:03+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4614921+08:00","Value":"Zz4YHG7SQea6flziIa+eLRr7t1kAAAAAQUIPAAAAAAAkBdmCAcZLAt+m8nFP1Bui","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4619768+08:00","Value":"COx9YfaAfzkCViY6v0PQ0AAAAABVhgHdIgqXaGpadToeFdad","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_429_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-12T23:20:03.4619768+08:00","Value":"aEbIRPZlPz2WqRMpt8/0BRr7t1kAAAAASUIssaErFQnx9n8YC2RgHQ==","Version":0}]', 1, CAST(N'2017-09-12T23:14:00.837' AS DateTime))
INSERT [dbo].[HostSessionCookie] ([Id], [HostId], [Token], [Cookies], [IsActive], [CreatedAt]) VALUES (59, 2, N'YxhwEVfTKI', N'[{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2019-09-18T21:29:01+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:12.8530643+08:00","Value":"5bdbbecf-db8b-4259-9b53-8a912fd9b92e","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-19T21:29:01+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:12.8530643+08:00","Value":"1efbcdae-fa26-4092-8e4a-5695cf54bb87","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-19T21:29:01+08:00","Name":"1efbcdae-fa26-4092-8e4a-5695cf54bb87SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:12.8530643+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"2017-09-19T21:29:01+08:00","Name":"1efbcdae-fa26-4092-8e4a-5695cf54bb87AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:12.8530643+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:18.0929928+08:00","Value":"en_US_VRBO","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"cas.homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"JSESSIONID","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:31:03.062409+08:00","Value":"1ap286zmrraj01g69nzgduy0vb","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HATGC_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-18T21:29:18.0929928+08:00","Value":"tgt-1158941-nfBf7DLJLfGC0jdbDUoWJq4f12xLr5tGu6f99dGBqGp3aszOtm-cas.homeaway.com","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".homeaway.com","Expired":false,"Expires":"2018-09-19T21:30:05.062409+08:00","Name":"2FA_LOTC","Path":"/auth","Port":"","Secure":true,"TimeStamp":"2017-09-18T21:31:03.062409+08:00","Value":"23378172&1537363793&EBBFB170164C3B968C8A19EC48C89C9B&0","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2019-09-18T21:29:03+08:00","Name":"hav","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"3d6b93e5-3199-4d8b-b92e-f8a426cf2f80","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-19T21:29:03+08:00","Name":"has","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"c5741f9e-7e83-4ead-9bd2-1c2fefafb448","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-19T21:29:03+08:00","Name":"c5741f9e-7e83-4ead-9bd2-1c2fefafb448SL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":"www.vrbo.com","Expired":false,"Expires":"2017-09-19T21:29:03+08:00","Name":"c5741f9e-7e83-4ead-9bd2-1c2fefafb448AVL","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"1","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":true,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"HASESSIONV3","Path":"/","Port":"","Secure":true,"TimeStamp":"2017-09-18T21:29:11.0523027+08:00","Value":"cb8b2df0-e96c-4383-83c0-7d015cc3293f","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-17T23:20:37+08:00","Name":"visid_incap_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:11.0523027+08:00","Value":"Flo3/SZTR7qdJ0N2JiasCBnKv1kAAAAAQUIPAAAAAAA3tFli9pSjK5+LZpj/OU8p","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:11.0523027+08:00","Value":"d1SBEUUkd2l4cuMz/g40/wAAAABipn0D0aTTWHK4UFsleyI7","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_625_1063985","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:11.0523027+08:00","Value":"ma52fDiP4wfbj5HHgXKsCBrKv1kAAAAAQiyAvi0GRAttwe9tkql25Q==","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"2018-09-17T23:20:37+08:00","Name":"visid_incap_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"AybK11MXQvyl7Yo3QVO5Ox/Kv1kAAAAAQUIPAAAAAABMxE6JnRrAx+/bFmMjMeRY","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"nlbi_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"ISUpHE6bsBl0rEUOv0PQ0AAAAADiJZv+lnCGaua869FzD71K","Version":0},{"Comment":"","CommentUri":null,"HttpOnly":false,"Discard":false,"Domain":".vrbo.com","Expired":false,"Expires":"0001-01-01T00:00:00","Name":"incap_ses_625_1042281","Path":"/","Port":"","Secure":false,"TimeStamp":"2017-09-18T21:29:15.7648624+08:00","Value":"5mfhLXCexkM6l5HHgXKsCB/Kv1kAAAAAsiGDDYdz7RfwJrkiVgXXkw==","Version":0}]', 1, CAST(N'2017-09-12T23:26:09.707' AS DateTime))
SET IDENTITY_INSERT [dbo].[HostSessionCookie] OFF
SET IDENTITY_INSERT [dbo].[Inbox] ON 

INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (1, 1, N'7dd2dcff-049a-4a88-844d-5676cc8ea424', 1, 1157366, 0, N'I', 0, N'Hi Kevin, Thanks for sending me the info. I noticed this site started blocking the phone numbers.Its important for me to speak before book it I have an idea if you can try sending me your number in the format below, It will come through:- Sixx Onne Thhree Thhree Two Fourr Thhree Onne Twoo Zeroo Please try this and Hope it works! Sincerely Beverly', CAST(N'2017-08-24T11:24:03.927' AS DateTime), CAST(N'2017-10-20T00:00:00.000' AS DateTime), CAST(N'2017-10-27T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (2, 1, N'50089bd1-0cdb-4ac8-9e29-b739406ce809', 2, 827199, 0, N'I', 0, N'Your price says $5000 a night. Is that accurate or a typo?? I am interested in renting your property if the price is a mistake. Mark', CAST(N'2017-08-11T18:01:07.733' AS DateTime), CAST(N'2018-03-10T00:00:00.000' AS DateTime), CAST(N'2018-03-17T00:00:00.000' AS DateTime), 0, 1, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (3, 1, N'd2741b19-86e1-4784-8b7f-371844735069', 3, 827199, 0, N'A', 0, N'Testing demo', CAST(N'2017-08-08T20:24:11.137' AS DateTime), CAST(N'2017-08-09T00:00:00.000' AS DateTime), CAST(N'2017-08-10T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (4, 1, N'98bd90ee-d0fe-4bb5-9af7-1ced4c783d6e', 3, 827199, 0, N'IC', 0, N'Sorry, we can''t reserve at the moment', CAST(N'2017-08-08T20:15:01.220' AS DateTime), CAST(N'2017-08-09T00:00:00.000' AS DateTime), CAST(N'2017-08-10T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (5, 1, N'bd017c8c-4aa0-4bed-8418-cc613551321a', 3, 827199, 0, N'BC', 0, N'', CAST(N'2017-08-08T12:08:07.503' AS DateTime), CAST(N'2017-08-11T00:00:00.000' AS DateTime), CAST(N'2017-08-12T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (6, 1, N'9bc073fa-bd46-49d6-bb45-459820bd5dc4', 3, 827199, 0, N'A', 0, N'', CAST(N'2017-08-08T12:06:36.770' AS DateTime), CAST(N'2017-08-08T00:00:00.000' AS DateTime), CAST(N'2017-08-09T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (7, 1, N'23a66954-a392-4bef-9cca-0f9d4dd43281', 3, 827199, 0, N'BC', 0, N'', CAST(N'2017-08-06T19:38:31.740' AS DateTime), CAST(N'2017-08-10T00:00:00.000' AS DateTime), CAST(N'2017-08-11T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (8, 1, N'e65fdf9d-db7d-4c65-b16e-4e2f3e466140', 4, 827199, 0, N'I', 0, N'Hi Frank, I have someone about to book the place. I hope to host your family next time :)', CAST(N'2017-08-03T02:36:38.093' AS DateTime), CAST(N'2017-08-07T00:00:00.000' AS DateTime), CAST(N'2017-08-09T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (9, 1, N'f1f00771-855b-40a5-aa7a-90a8727ea6b2', 3, 827199, 0, N'BC', 0, N'test reply', CAST(N'2017-07-28T19:24:50.117' AS DateTime), CAST(N'2017-08-07T00:00:00.000' AS DateTime), CAST(N'2017-08-08T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (10, 1, N'f2d20091-e85a-4bda-a289-17c05a088739', 3, 827199, 0, N'IC', 0, N'testing..', CAST(N'2017-07-28T19:23:21.223' AS DateTime), CAST(N'2017-08-06T00:00:00.000' AS DateTime), CAST(N'2017-08-07T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (11, 1, N'4ed91711-cb39-4e51-b6df-fb615033efa0', 3, 827199, 0, N'I', 0, N'testing from app', CAST(N'2017-07-24T21:12:39.150' AS DateTime), CAST(N'2017-08-06T00:00:00.000' AS DateTime), CAST(N'2017-08-07T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (12, 1, N'd869318e-b035-44c2-ba71-4ca9b3a0512b', 5, 827199, 0, N'I', 0, N'Is the rate of $5000 per night correct?', CAST(N'2017-07-20T22:13:37.607' AS DateTime), CAST(N'2017-11-22T00:00:00.000' AS DateTime), CAST(N'2017-11-26T00:00:00.000' AS DateTime), 0, 1, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (13, 1, N'20802cbf-706c-42c9-a284-14fa806806d5', 3, 827199, 0, N'A', 0, N'testing', CAST(N'2017-07-20T18:49:37.987' AS DateTime), CAST(N'2017-08-01T00:00:00.000' AS DateTime), CAST(N'2017-08-02T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (14, 1, N'5c58a27d-dcec-417d-9520-978e2d239e88', 3, 827199, 0, N'BC', 0, N'', CAST(N'2017-07-18T16:42:45.560' AS DateTime), CAST(N'2017-08-01T00:00:00.000' AS DateTime), CAST(N'2017-08-02T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (15, 1, N'b2469a39-be51-4f8e-9585-2fb060c42002', 3, 827199, 0, N'A', 0, N'testing demo', CAST(N'2017-07-07T20:47:42.040' AS DateTime), CAST(N'2017-07-04T00:00:00.000' AS DateTime), CAST(N'2017-07-05T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (16, 1, N'c73e70ac-6df7-4b87-9c9b-05926999154a', 6, 827199, 0, N'BC', 0, N'', CAST(N'2017-06-27T03:59:33.873' AS DateTime), CAST(N'2017-10-29T00:00:00.000' AS DateTime), CAST(N'2017-11-04T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (17, 1, N'96012fb6-12e6-4b5e-bb06-f9af5e827c2f', 7, 827199, 0, N'I', 0, N'Hi Pamela I would be glad to host you for the special occassion. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review the house rules and are all ok with them? We live in the house when we are not renting it out, so hosting is really personal to us. I wish to get to know each one who will be coming. Can you tell me a little bit about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)? Regards, Kevin', CAST(N'2017-06-21T19:01:39.737' AS DateTime), CAST(N'2017-11-02T00:00:00.000' AS DateTime), CAST(N'2017-11-06T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (18, 1, N'f4fcaab6-3e7e-4f07-8da8-e9304bf203fe', 8, 827199, 0, N'I', 0, N'Just got off cruise sorry didn''t have service thanks will work this asap', CAST(N'2017-06-12T14:41:26.580' AS DateTime), CAST(N'2017-10-26T00:00:00.000' AS DateTime), CAST(N'2017-10-30T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (19, 1, N'67f9e07a-ea9a-406b-bfb7-c6dbc08e1908', 9, 827199, 0, N'I', 0, N'Hi Catherine, Thank you for your inquiry. Unfortunately, the dates you want to book are not available. Have a great day.', CAST(N'2017-05-31T21:51:38.620' AS DateTime), CAST(N'2017-12-28T00:00:00.000' AS DateTime), CAST(N'2018-01-02T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (20, 1, N'1b922475-6904-46d9-b3e5-ec7f51fa2645', 10, 827199, 0, N'I', 0, N'Hi Jasmine, its $900. - Kevin', CAST(N'2017-04-26T23:44:37.060' AS DateTime), CAST(N'2018-04-20T00:00:00.000' AS DateTime), CAST(N'2018-04-24T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (21, 1, N'd96d8df5-a4e8-4463-9139-5041b92e548b', 11, 827199, 0, N'I', 0, N'Dear Shakeya Knight, Thank you for your inquiry on my vacation rental (827199). We do not hold dates nor do we handle deposits. All payments upon booking are courses thru VRBO. I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it? We live in the house when we are not renting it out on --------, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)? My property is available from Sep 8, 2017 - Sep 13, 2017. If you have any questions or need clarifications, do feel free to message me here anytime. Best Regards, Kevin Chu', CAST(N'2017-04-22T20:27:39.620' AS DateTime), CAST(N'2017-09-08T00:00:00.000' AS DateTime), CAST(N'2017-09-13T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (22, 1, N'0b7bf168-7267-40e7-a7e0-322180ffb2bb', 12, 827199, 0, N'I', 0, N'Hi Renetta, What dates did you book for? I dont think you inquired before about my listing. I look forward to hosting you all. And I have indicated a few things under the listing''s description and house rules section, please take time to read them all and let me know if you and everyone in your group are all ok with the house rules, before booking. We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, what brings you to Las Vegas, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)? Thank you. Regards, Kevin ___________________________________________________', CAST(N'2017-04-10T02:04:37.823' AS DateTime), CAST(N'2017-05-25T00:00:00.000' AS DateTime), CAST(N'2017-05-29T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (23, 1, N'411a15ad-b6db-49f0-852a-1cb6849eddae', 13, 827199, 0, N'I', 0, N'Hi Ravette, Thank you for your inquiry and we look forward to hosting you and your family. The dates you wanted to book (Jul 27, 2017 - Jul 31, 2017) is still available as of this writing. The address you provided is approximately 16 mins away by car. I have indicated a list of house rules under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with all of them? We live in this house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)? Regards, Kevin', CAST(N'2017-04-05T21:30:07.583' AS DateTime), CAST(N'2017-07-27T00:00:00.000' AS DateTime), CAST(N'2017-07-31T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (24, 1, N'618fecc9-3eba-41b5-ae5c-3b2c98e79c44', 14, 827199, 0, N'I', 0, N'Hi Tsering, Thank you for your inquiry, I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it? Please let me know if you are all ok with it? Can you also tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)? We live in this house ourselves when we are not renting it out, so we would appreciate getting to know a little bit about our guests. :-) Regards, Kevin', CAST(N'2017-03-15T18:51:39.483' AS DateTime), CAST(N'2017-11-22T00:00:00.000' AS DateTime), CAST(N'2017-11-26T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (25, 1, N'07ce5a06-463c-49a2-982a-083d9f4d15a4', 15, 827199, 0, N'I', 0, N'Dear jamelia brown, Thank you for your inquiry. We do have washer and dryer, wifi and cable. But we do not have tv in every room. I have indicated a few things under the listing''s description and house rules section, just want to know if each adult in your group had the chance to review it and are all ok with it? Also, I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)? Best Regards, Kevin Chu', CAST(N'2017-03-09T15:16:39.030' AS DateTime), CAST(N'2017-07-25T00:00:00.000' AS DateTime), CAST(N'2017-07-30T00:00:00.000' AS DateTime), 0, 0, 0, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (26, 1, N'245f2d18-6163-49b6-a9d5-aa90d0fb3007', 16, 827199, 0, N'I', 0, N'Dear Jose Leon, Thank you for your inquiry on my vacation rental (827199). I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it? We live in the house when we are not renting it out on --------, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)? My property is available from May 26, 2017 - May 29, 2017. If you have any questions or need clarifications, do feel free to message me here anytime. Best Regards, Kevin Chu', CAST(N'2017-03-08T15:45:08.327' AS DateTime), CAST(N'2017-05-26T00:00:00.000' AS DateTime), CAST(N'2017-05-29T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (27, 1, N'4a40e08d-c233-422a-95fb-4c1443c5eb2e', 17, 827199, 0, N'I', 0, N'Thank you. They have changed their plans an no longer need the home. ~ Mareyka Webb Excuses are the tools of the incompetent', CAST(N'2017-03-07T19:43:24.430' AS DateTime), CAST(N'2017-07-25T00:00:00.000' AS DateTime), CAST(N'2017-07-31T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (28, 1, N'6c9230cc-3226-4218-8c3f-28249ce2525b', 18, 827199, 0, N'I', 0, N'Hi Travis, Thank you for your inquiry on my vacation rental (827199). My property is available from Apr 6, 2017 - Apr 10, 2017. I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it? We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)? Regards, Kevin', CAST(N'2017-03-07T19:03:41.023' AS DateTime), CAST(N'2017-04-06T00:00:00.000' AS DateTime), CAST(N'2017-04-10T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (29, 1, N'3c3cb885-37e4-4875-86f9-d2288f3e331a', 19, 827199, 0, N'I', 0, N'Disregard', CAST(N'2017-02-26T14:40:56.593' AS DateTime), CAST(N'2017-07-26T00:00:00.000' AS DateTime), CAST(N'2017-07-31T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (30, 1, N'1647dd26-d59b-4999-8c56-dbd6a5154b8e', 20, 827199, 0, N'I', 0, N'Hi Robert , VRBO booking system handles the reservation and all payments are coursed thru them too for security reasons. We also would like to know first something about each person we will potentially be hosting (i.e who they are, how many are coming, where are you coming from, what brings you to Las Vegas, what you do, ages, etc.) We live in this house ourselves when we are not renting it out, so hosting is kinda personal to us and would appreciate knowing a little bit about who we are hosting. :-) I also have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review all the house rules and are all ok with it? We highly recommend this so our guest would know what to expect. After letting me know you are fine with the house rules, you may choose to proceed to finalize your booking and VRBO will send us the request to accept and VRBO does the collection. You can read further at the VRBO Help and Support section thier FAQs on how these works. :-) Regards, Kevin', CAST(N'2017-02-24T18:50:08.100' AS DateTime), CAST(N'2017-07-15T00:00:00.000' AS DateTime), CAST(N'2017-07-22T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (31, 1, N'95020efe-6a70-424a-9fc2-c8192c4a3ed1', 21, 827199, 0, N'I', 0, N'Hi Comerly, I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it? We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)? Regards, Kevin', CAST(N'2017-01-15T23:12:09.150' AS DateTime), CAST(N'2017-05-27T00:00:00.000' AS DateTime), CAST(N'2017-06-03T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (32, 1, N'3fc635f1-e306-4f46-9619-1046834c50ca', 22, 827199, 0, N'I', 0, N'Thank you. We choose another home. Thank you again.', CAST(N'2017-01-12T19:39:32.730' AS DateTime), CAST(N'2017-03-16T00:00:00.000' AS DateTime), CAST(N'2017-03-20T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (33, 1, N'8dd38166-f329-4f2a-a7f1-ee449a5cdbf9', 23, 827199, 0, N'I', 0, N'Thank you for letting me know. Enjoy Las Vegas! 😊', CAST(N'2016-11-29T17:23:42.030' AS DateTime), CAST(N'2017-02-18T00:00:00.000' AS DateTime), CAST(N'2017-02-22T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (34, 1, N'63fcf52b-0f00-43e1-adab-b7f3075c8010', 24, 827199, 0, N'I', 0, N'Crystal, I have uploaded the most recent photos.', CAST(N'2016-11-15T20:16:57.623' AS DateTime), CAST(N'2016-12-22T00:00:00.000' AS DateTime), CAST(N'2016-12-27T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (35, 1, N'31fef948-ccf3-4883-8f11-8b0f4f462caa', 25, 827199, 0, N'I', 0, N'Dear Jamie Gutierrez, Thank you for your inquiry on my vacation rental and I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all okay with it? My property is available from Dec 2, 2016 - Dec 5, 2016. We live in the house when we are not renting it out so hosting is personal to ask. I wish to really get to know each one who will be coming. Can you tell me a little bit about each person I will potentially be hosting (who they are, how many are coming, are you all coming from same location, what they do, ages, etc)? Sincerely, Kevin Chu', CAST(N'2016-11-14T22:59:38.400' AS DateTime), CAST(N'2016-12-02T00:00:00.000' AS DateTime), CAST(N'2016-12-05T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (36, 1, N'0214c03b-ec53-4e13-9cec-034a266318f8', 26, 827199, 0, N'I', 0, N'Hi, my yard is not furnished, it is just a desert landscape with some plants.', CAST(N'2016-11-07T19:17:39.540' AS DateTime), CAST(N'2016-12-02T00:00:00.000' AS DateTime), CAST(N'2016-12-05T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (37, 1, N'09694f2f-b416-46a0-a971-a831a44a80a6', 27, 827199, 0, N'I', 0, N'Hi Nikki, I''m sorry i cannot accomodate the direct payments via cheques and prefer the booking to be done online through VRBO. Would also appreciate it if you can tell me something about each person staying (how many are coming, who they are, what they do, are you all arriving together, where is everyone from, what they do, ages, etc). We live in this house ourselves so you can imagine how personal hosting is to us. Have you and your party read all the house rules and is everyone fine with everything stated therein? This is so we know what to expect. As for the yard, we do not have a furnished yard other than a desert landscaped yard with some plants. Regards, Kevin', CAST(N'2016-11-07T19:12:08.517' AS DateTime), CAST(N'2016-12-02T00:00:00.000' AS DateTime), CAST(N'2016-12-06T00:00:00.000' AS DateTime), 0, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (38, 1, N'6b1a5b88-a5a3-4688-8039-e487d3973d70', 28, 827199, 0, N'I', 0, N'Re-sending my booking request', CAST(N'2017-09-18T05:03:51.927' AS DateTime), CAST(N'2017-09-19T00:00:00.000' AS DateTime), CAST(N'2017-09-20T00:00:00.000' AS DateTime), 0, 0, 0, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (39, 1, N'721618ef-4546-416e-8189-bc0033564f1e', 28, 827199, 0, N'B', 0, N'I am re-sending my booking', CAST(N'2017-09-18T05:07:57.403' AS DateTime), CAST(N'2017-09-19T00:00:00.000' AS DateTime), CAST(N'2017-09-20T00:00:00.000' AS DateTime), 0, 0, 0, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
INSERT [dbo].[Inbox] ([Id], [HostId], [ThreadId], [GuestId], [PropertyId], [IsArchived], [Status], [HasUnread], [LastMessage], [LastMessageAt], [CheckInDate], [CheckOutDate], [GuestCount], [IsInquiryOnly], [IsSpecialOfferSent], [CompanyId], [RentalCost], [CleaningCost], [GuestFee], [ServiceFee], [Subtotal], [GuestPay], [HostEarn], [StatusType], [CanPreApproveInquiry], [CanWithdrawPreApprovalInquiry], [CanDeclineInquiry], [InquiryId], [SiteType]) VALUES (40, 1, N'1701f70f-744d-4dbc-9e21-a24958cb9a2c', 29, 827199, 0, N'IC', 0, N'Dear Melanie Reese, Unfortunately, we are unable to confirm your reservation at our vacation rental for Sep 19, 2017 - Sep 20, 2017. Your card has not been charged for this reservation, and you are free to book another property. Thank you, Kevin Chu', CAST(N'2017-09-18T04:26:19.700' AS DateTime), CAST(N'2017-09-19T00:00:00.000' AS DateTime), CAST(N'2017-09-20T00:00:00.000' AS DateTime), 0, 0, 0, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), 0, 0, 0, 0, NULL, 2)
SET IDENTITY_INSERT [dbo].[Inbox] OFF
SET IDENTITY_INSERT [dbo].[IncomeType] ON 

INSERT [dbo].[IncomeType] ([Id], [TypeName]) VALUES (1, N'Booking')
SET IDENTITY_INSERT [dbo].[IncomeType] OFF
SET IDENTITY_INSERT [dbo].[Inquiry] ON 

INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (1, 1, N'ecb00e5a-ce38-4a69-9eaa-23c398d6079b', 827199, 0, N'HA-H4J128', N'I', CAST(N'2017-09-19T00:00:00.000' AS DateTime), CAST(N'2017-09-20T00:00:00.000' AS DateTime), 1, CAST(83.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), CAST(74.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-09-14T13:13:23.000' AS DateTime), CAST(N'2017-09-14T13:13:22.000' AS DateTime), NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'6b1a5b88-a5a3-4688-8039-e487d3973d70')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (2, 1, NULL, 1157366, 0, NULL, N'I', CAST(N'2017-10-20T00:00:00.000' AS DateTime), CAST(N'2017-10-27T00:00:00.000' AS DateTime), 7, CAST(40267.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(38110.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-21T14:37:37.000' AS DateTime), NULL, NULL, NULL, NULL, 8, 0, 0, 1, 2, 1, N'7dd2dcff-049a-4a88-844d-5676cc8ea424')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (3, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2018-03-10T00:00:00.000' AS DateTime), CAST(N'2018-03-17T00:00:00.000' AS DateTime), 7, CAST(38394.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(37995.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-11T18:01:07.000' AS DateTime), NULL, NULL, NULL, NULL, 16, 0, 0, 1, 2, 1, N'50089bd1-0cdb-4ac8-9e29-b739406ce809')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (4, 1, N'7eaa3f79-0040-4bbe-9e81-5473ca6645fb', 827199, 0, N'HA-5W1JRM', N'A', CAST(N'2017-08-09T00:00:00.000' AS DateTime), CAST(N'2017-08-10T00:00:00.000' AS DateTime), 1, CAST(58.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(52.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-08T20:24:10.000' AS DateTime), CAST(N'2017-08-08T20:24:10.000' AS DateTime), NULL, NULL, NULL, 1, 0, 0, 1, 2, 1, N'd2741b19-86e1-4784-8b7f-371844735069')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (5, 1, N'f0cda836-a01c-4083-b400-d459969cf826', 827199, 0, N'HA-X6QC5R', N'IC', CAST(N'2017-08-09T00:00:00.000' AS DateTime), CAST(N'2017-08-10T00:00:00.000' AS DateTime), 1, CAST(58.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(52.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-08T20:15:00.000' AS DateTime), CAST(N'2017-08-08T20:15:00.000' AS DateTime), NULL, NULL, NULL, 1, 0, 0, 1, 2, 1, N'98bd90ee-d0fe-4bb5-9af7-1ced4c783d6e')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (6, 1, N'375805e1-c265-4663-ac4c-f9ee509a7515', 827199, 0, N'HA-LHKMH9', N'BC', CAST(N'2017-08-11T00:00:00.000' AS DateTime), CAST(N'2017-08-12T00:00:00.000' AS DateTime), 1, CAST(46.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(41.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-08T12:08:06.000' AS DateTime), CAST(N'2017-08-08T12:08:06.000' AS DateTime), NULL, NULL, NULL, 3, 0, 0, 1, 2, 1, N'bd017c8c-4aa0-4bed-8418-cc613551321a')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (7, 1, N'faa033ca-d8b6-4114-8cb0-b519a62d8d35', 827199, 0, N'HA-V6FTBX', N'A', CAST(N'2017-08-08T00:00:00.000' AS DateTime), CAST(N'2017-08-09T00:00:00.000' AS DateTime), 1, CAST(47.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(42.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-08T11:58:30.000' AS DateTime), CAST(N'2017-08-08T11:58:30.000' AS DateTime), NULL, NULL, NULL, 1, 0, 0, 1, 2, 1, N'9bc073fa-bd46-49d6-bb45-459820bd5dc4')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (8, 1, N'e743e0d5-8c63-4e5b-8d96-a679ba003c97', 827199, 0, N'HA-S5DBYT', N'BC', CAST(N'2017-08-10T00:00:00.000' AS DateTime), CAST(N'2017-08-11T00:00:00.000' AS DateTime), 1, CAST(179.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(18.00 AS Decimal(18, 2)), CAST(161.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-06T19:38:31.000' AS DateTime), CAST(N'2017-08-06T19:38:31.000' AS DateTime), NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'23a66954-a392-4bef-9cca-0f9d4dd43281')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (9, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-08-07T00:00:00.000' AS DateTime), CAST(N'2017-08-09T00:00:00.000' AS DateTime), 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-08-03T02:36:37.000' AS DateTime), NULL, NULL, NULL, NULL, 5, 0, 0, 1, 2, 1, N'e65fdf9d-db7d-4c65-b16e-4e2f3e466140')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (10, 1, N'4af3dee8-9c3b-42a0-8123-7ba6a3b190f6', 827199, 0, N'HA-5ZFVC9', N'BC', CAST(N'2017-08-07T00:00:00.000' AS DateTime), CAST(N'2017-08-08T00:00:00.000' AS DateTime), 1, CAST(69.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(30.00 AS Decimal(18, 2)), CAST(7.00 AS Decimal(18, 2)), CAST(62.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-28T19:24:49.000' AS DateTime), CAST(N'2017-07-28T19:24:49.000' AS DateTime), NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'f1f00771-855b-40a5-aa7a-90a8727ea6b2')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (11, 1, N'824309a8-d7ec-4cfc-b9fc-1c933e0f3996', 827199, 0, N'HA-C883KT', N'IC', CAST(N'2017-08-06T00:00:00.000' AS DateTime), CAST(N'2017-08-07T00:00:00.000' AS DateTime), 1, CAST(58.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(52.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-28T19:05:48.000' AS DateTime), CAST(N'2017-07-28T19:05:48.000' AS DateTime), NULL, NULL, NULL, 1, 0, 0, 1, 2, 1, N'f2d20091-e85a-4bda-a289-17c05a088739')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (12, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-08-06T00:00:00.000' AS DateTime), CAST(N'2017-08-07T00:00:00.000' AS DateTime), 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-24T21:12:39.000' AS DateTime), NULL, NULL, NULL, NULL, 1, 0, 0, 1, 2, 1, N'4ed91711-cb39-4e51-b6df-fb615033efa0')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (13, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-11-22T00:00:00.000' AS DateTime), CAST(N'2017-11-26T00:00:00.000' AS DateTime), 4, CAST(22119.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(21720.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-20T22:13:37.000' AS DateTime), NULL, NULL, NULL, NULL, 9, 0, 0, 1, 2, 1, N'd869318e-b035-44c2-ba71-4ca9b3a0512b')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (14, 1, N'9d9cab66-c166-4e4b-8dda-2ceef555ac5c', 827199, 0, N'HA-GRDDBH', N'A', CAST(N'2017-08-01T00:00:00.000' AS DateTime), CAST(N'2017-08-02T00:00:00.000' AS DateTime), 1, CAST(83.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), CAST(74.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-20T18:49:37.000' AS DateTime), CAST(N'2017-07-20T18:49:37.000' AS DateTime), NULL, NULL, NULL, 1, 0, 0, 1, 2, 1, N'20802cbf-706c-42c9-a284-14fa806806d5')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (15, 1, N'5a32a473-5050-43dd-aa84-d98de44eaf40', 827199, 0, N'HA-HMQ897', N'BC', CAST(N'2017-08-01T00:00:00.000' AS DateTime), CAST(N'2017-08-02T00:00:00.000' AS DateTime), 1, CAST(1255.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(28.00 AS Decimal(18, 2)), CAST(1148.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-17T15:15:12.000' AS DateTime), CAST(N'2017-07-18T15:18:02.000' AS DateTime), NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'5c58a27d-dcec-417d-9520-978e2d239e88')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (16, 1, N'5a1c5e59-5e3a-4f0b-bb7d-eaff1c0d270e', 827199, 0, N'HA-YBBXVZ', N'A', CAST(N'2017-07-04T00:00:00.000' AS DateTime), CAST(N'2017-07-05T00:00:00.000' AS DateTime), 1, CAST(56.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-07-02T20:08:40.000' AS DateTime), CAST(N'2017-07-04T13:03:23.000' AS DateTime), NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'b2469a39-be51-4f8e-9585-2fb060c42002')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (17, 1, N'873cde56-345d-4642-adc4-37dbf7d7d698', 827199, 0, N'HA-2TRZQF', N'BC', CAST(N'2017-10-29T00:00:00.000' AS DateTime), CAST(N'2017-11-04T00:00:00.000' AS DateTime), 6, CAST(2485.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(133.00 AS Decimal(18, 2)), CAST(2352.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-06-25T20:22:39.000' AS DateTime), CAST(N'2017-06-27T04:39:22.000' AS DateTime), NULL, NULL, NULL, 6, 0, 0, 1, 2, 1, N'c73e70ac-6df7-4b87-9c9b-05926999154a')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (18, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-11-02T00:00:00.000' AS DateTime), CAST(N'2017-11-06T00:00:00.000' AS DateTime), 4, CAST(22119.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(21720.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-06-21T19:01:39.000' AS DateTime), NULL, NULL, NULL, NULL, 6, 0, 0, 1, 2, 1, N'96012fb6-12e6-4b5e-bb06-f9af5e827c2f')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (19, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-10-26T00:00:00.000' AS DateTime), CAST(N'2017-10-30T00:00:00.000' AS DateTime), 4, CAST(22119.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(21720.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-06-08T15:56:37.000' AS DateTime), NULL, NULL, NULL, NULL, 6, 0, 0, 1, 2, 1, N'f4fcaab6-3e7e-4f07-8da8-e9304bf203fe')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (20, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-12-28T00:00:00.000' AS DateTime), CAST(N'2018-01-02T00:00:00.000' AS DateTime), 5, CAST(27544.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(27145.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-05-31T21:51:38.000' AS DateTime), NULL, NULL, NULL, NULL, 9, 0, 0, 1, 2, 1, N'67f9e07a-ea9a-406b-bfb7-c6dbc08e1908')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (21, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2018-04-20T00:00:00.000' AS DateTime), CAST(N'2018-04-24T00:00:00.000' AS DateTime), 4, CAST(22119.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(399.00 AS Decimal(18, 2)), CAST(21720.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-04-26T23:44:37.000' AS DateTime), NULL, NULL, NULL, NULL, 8, 0, 0, 1, 2, 1, N'1b922475-6904-46d9-b3e5-ec7f51fa2645')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (22, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-09-08T00:00:00.000' AS DateTime), CAST(N'2017-09-13T00:00:00.000' AS DateTime), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-04-22T20:27:39.000' AS DateTime), NULL, NULL, NULL, NULL, 10, 0, 0, 1, 2, 1, N'd96d8df5-a4e8-4463-9139-5041b92e548b')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (23, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-05-25T00:00:00.000' AS DateTime), CAST(N'2017-05-29T00:00:00.000' AS DateTime), 4, CAST(3091.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(132.00 AS Decimal(18, 2)), CAST(2959.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-04-10T02:04:37.000' AS DateTime), NULL, NULL, NULL, NULL, 14, 0, 0, 1, 2, 1, N'0b7bf168-7267-40e7-a7e0-322180ffb2bb')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (24, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-07-27T00:00:00.000' AS DateTime), CAST(N'2017-07-31T00:00:00.000' AS DateTime), 4, CAST(2574.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(136.00 AS Decimal(18, 2)), CAST(2438.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-04-05T21:30:07.000' AS DateTime), NULL, NULL, NULL, NULL, 8, 0, 0, 1, 2, 1, N'411a15ad-b6db-49f0-852a-1cb6849eddae')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (25, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-11-22T00:00:00.000' AS DateTime), CAST(N'2017-11-26T00:00:00.000' AS DateTime), 4, CAST(4023.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(152.00 AS Decimal(18, 2)), CAST(3871.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-03-15T18:51:39.000' AS DateTime), NULL, NULL, NULL, NULL, 16, 0, 0, 1, 2, 1, N'618fecc9-3eba-41b5-ae5c-3b2c98e79c44')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (26, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-07-25T00:00:00.000' AS DateTime), CAST(N'2017-07-30T00:00:00.000' AS DateTime), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-03-09T15:16:38.000' AS DateTime), NULL, NULL, NULL, NULL, 8, 0, 0, 1, 2, 1, N'07ce5a06-463c-49a2-982a-083d9f4d15a4')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (27, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-05-26T00:00:00.000' AS DateTime), CAST(N'2017-05-29T00:00:00.000' AS DateTime), 3, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-03-08T15:45:08.000' AS DateTime), NULL, NULL, NULL, NULL, 12, 0, 0, 1, 2, 1, N'245f2d18-6163-49b6-a9d5-aa90d0fb3007')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (28, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-07-25T00:00:00.000' AS DateTime), CAST(N'2017-07-31T00:00:00.000' AS DateTime), 6, CAST(3532.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(139.00 AS Decimal(18, 2)), CAST(3393.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-03-06T06:33:37.000' AS DateTime), NULL, NULL, NULL, NULL, 16, 0, 0, 1, 2, 1, N'4a40e08d-c233-422a-95fb-4c1443c5eb2e')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (29, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-04-06T00:00:00.000' AS DateTime), CAST(N'2017-04-10T00:00:00.000' AS DateTime), 4, CAST(2951.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(122.00 AS Decimal(18, 2)), CAST(2829.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-03-07T19:03:40.000' AS DateTime), NULL, NULL, NULL, NULL, 8, 0, 0, 1, 2, 1, N'6c9230cc-3226-4218-8c3f-28249ce2525b')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (30, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-07-26T00:00:00.000' AS DateTime), CAST(N'2017-07-31T00:00:00.000' AS DateTime), 5, CAST(2623.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(109.00 AS Decimal(18, 2)), CAST(2514.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-02-13T01:07:07.000' AS DateTime), NULL, NULL, NULL, NULL, 10, 0, 0, 1, 2, 1, N'3c3cb885-37e4-4875-86f9-d2288f3e331a')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (31, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-07-15T00:00:00.000' AS DateTime), CAST(N'2017-07-22T00:00:00.000' AS DateTime), 7, CAST(3931.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(147.00 AS Decimal(18, 2)), CAST(3784.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-02-24T18:50:08.000' AS DateTime), NULL, NULL, NULL, NULL, 16, 0, 0, 1, 2, 1, N'1647dd26-d59b-4999-8c56-dbd6a5154b8e')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (32, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-05-27T00:00:00.000' AS DateTime), CAST(N'2017-06-03T00:00:00.000' AS DateTime), 7, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-01-15T23:12:09.000' AS DateTime), NULL, NULL, NULL, NULL, 15, 0, 0, 1, 2, 1, N'95020efe-6a70-424a-9fc2-c8192c4a3ed1')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (33, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-03-16T00:00:00.000' AS DateTime), CAST(N'2017-03-20T00:00:00.000' AS DateTime), 4, CAST(3214.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(124.00 AS Decimal(18, 2)), CAST(3089.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-01-12T19:05:42.000' AS DateTime), NULL, NULL, NULL, NULL, 10, 0, 0, 1, 2, 1, N'3fc635f1-e306-4f46-9619-1046834c50ca')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (34, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2017-02-18T00:00:00.000' AS DateTime), CAST(N'2017-02-22T00:00:00.000' AS DateTime), 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2016-11-28T19:37:44.000' AS DateTime), NULL, NULL, NULL, NULL, 20, 0, 0, 1, 2, 1, N'8dd38166-f329-4f2a-a7f1-ee449a5cdbf9')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (35, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2016-12-22T00:00:00.000' AS DateTime), CAST(N'2016-12-27T00:00:00.000' AS DateTime), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2016-11-15T03:51:37.000' AS DateTime), NULL, NULL, NULL, NULL, 3, 0, 0, 1, 2, 1, N'63fcf52b-0f00-43e1-adab-b7f3075c8010')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (36, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2016-12-02T00:00:00.000' AS DateTime), CAST(N'2016-12-05T00:00:00.000' AS DateTime), 3, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2016-11-14T22:59:38.000' AS DateTime), NULL, NULL, NULL, NULL, 16, 0, 0, 1, 2, 1, N'31fef948-ccf3-4883-8f11-8b0f4f462caa')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (37, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2016-12-02T00:00:00.000' AS DateTime), CAST(N'2016-12-05T00:00:00.000' AS DateTime), 3, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2016-11-07T19:17:39.000' AS DateTime), NULL, NULL, NULL, NULL, 12, 0, 0, 1, 2, 1, N'0214c03b-ec53-4e13-9cec-034a266318f8')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (38, 1, NULL, 827199, 0, NULL, N'I', CAST(N'2016-12-02T00:00:00.000' AS DateTime), CAST(N'2016-12-06T00:00:00.000' AS DateTime), 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2016-11-06T23:49:39.000' AS DateTime), NULL, NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'09694f2f-b416-46a0-a971-a831a44a80a6')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (39, 1, N'e5b25fa0-d5cb-4c4a-bad5-b6ed3043a2ba', 827199, 0, N'HA-X85XQR', N'B', CAST(N'2017-09-19T00:00:00.000' AS DateTime), CAST(N'2017-09-20T00:00:00.000' AS DateTime), 1, CAST(83.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), CAST(74.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-09-18T05:07:55.000' AS DateTime), CAST(N'2017-09-18T05:07:55.000' AS DateTime), NULL, NULL, NULL, 2, 0, 0, 1, 2, 1, N'721618ef-4546-416e-8189-bc0033564f1e')
INSERT [dbo].[Inquiry] ([Id], [HostId], [ReservationId], [PropertyId], [GuestId], [ConfirmationCode], [BookingStatusCode], [CheckInDate], [CheckOutDate], [Nights], [ReservationCost], [SumPerNightAmount], [CleaningFee], [ServiceFee], [TotalPayout], [NetRevenueAmount], [PayoutDate], [InquiryDate], [BookingDate], [BookingCancelDate], [BookingConfirmDate], [BookingConfirmCancelDate], [GuestCount], [IsActive], [IsAltered], [IsImported], [SiteType], [CompanyId], [ThreadId]) VALUES (40, 1, N'2d48030b-7561-4c44-82bf-4cd6c0983745', 827199, 0, N'HA-LJ23QL', N'IC', CAST(N'2017-09-19T00:00:00.000' AS DateTime), CAST(N'2017-09-20T00:00:00.000' AS DateTime), 1, CAST(83.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(9.00 AS Decimal(18, 2)), CAST(74.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, CAST(N'2017-09-18T04:26:19.000' AS DateTime), CAST(N'2017-09-18T04:26:19.000' AS DateTime), NULL, NULL, NULL, 5, 0, 0, 1, 2, 1, N'1701f70f-744d-4dbc-9e21-a24958cb9a2c')
SET IDENTITY_INSERT [dbo].[Inquiry] OFF
SET IDENTITY_INSERT [dbo].[Message] ON 

INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (1, N'3f3f5021-8dc0-4996-a01c-7e084b4b7431', N'7dd2dcff-049a-4a88-844d-5676cc8ea424', N'Hi Kevin,

Thanks for sending me the info.
I noticed this site started blocking the phone numbers.Its important for me
to speak before book it
I have an idea if you can try sending me your number in the format below,
It will come through:-

Sixx
Onne
Thhree

Thhree
Two Fourr

Thhree
Onne
Twoo
Zeroo

Please try this and Hope it works!

Sincerely
Beverly', 0, CAST(N'2017-08-24T11:24:03.927' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (2, N'a0cb950f-47b3-497d-a1b1-3426dc33877b', N'7dd2dcff-049a-4a88-844d-5676cc8ea424', N'Hi Bev, yes it is still available as of this writing.  We look forward to possibly hosting you and your family.  May we know a little bit about each one we will be potentially be hosting (i.e who they are, how many are staying, their names, what they do, ages, etc)?  I''m asking coz we live in this house ourselves when we are not renting it out, so hosting is a bit personal to us.  

Personal information are filtered out if guests is not officially booked yet.  You may address all your questions here and I will gladly answer.  I am not clear about your doubts regarding nearby rental, can you clarify please?  

Also, hope you all have read the house rules?  Please acknowledge that you all are fine with them.  Thank you.

Regards,
Maria', 1, CAST(N'2017-08-21T17:44:11.233' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (3, N'4a883a60-e7c2-468d-9805-bd86c6035b2c', N'7dd2dcff-049a-4a88-844d-5676cc8ea424', N'Hello there
I would like to know if your beautiful property is available for the time I described,Can I pay on the website or should I confirm first through?
I would also like to discuss about few things regarding the rental.Can You please give me your telephone number as well to have my little doubts cleared about the nearby rental?
Actually , We are coming over there for reunion purpose.Look forward to speaking with you.
Thanks though,
Bev Nash', 0, CAST(N'2017-08-21T14:37:37.853' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (4, N'4e68a13b-e215-4b59-b9af-5c03ee3f6263', N'50089bd1-0cdb-4ac8-9e29-b739406ce809', N'Your price says $5000 a night.  Is that accurate or a typo??
I am interested in renting your property if the price is a mistake.

Mark', 0, CAST(N'2017-08-11T18:01:07.733' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (5, N'176befdb-96f2-4550-8582-9f1fa834a084', N'd2741b19-86e1-4784-8b7f-371844735069', N'Hi', 1, CAST(N'2017-08-08T20:32:51.567' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (6, N'f3c00198-febf-4523-8ae0-ef082ed2c602', N'd2741b19-86e1-4784-8b7f-371844735069', N'booking 4', 0, CAST(N'2017-08-08T20:24:11.137' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (7, N'4a4c4f8f-73f3-4704-b272-18c509748374', N'98bd90ee-d0fe-4bb5-9af7-1ced4c783d6e', N'Sorry, we can''t reserve at the moment', 1, CAST(N'2017-08-08T20:19:28.340' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (8, N'274dd67a-c4f3-444b-8ef9-031d2b7f88f8', N'98bd90ee-d0fe-4bb5-9af7-1ced4c783d6e', N'Booking 3 ..', 0, CAST(N'2017-08-08T20:15:01.220' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (9, N'f250e512-b038-4318-852c-2395ba84232c', N'bd017c8c-4aa0-4bed-8418-cc613551321a', N'testing demo', 1, CAST(N'2017-08-08T20:12:16.467' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (10, N'3f23ba56-f191-4b7e-abbd-fc7ac68ea19e', N'bd017c8c-4aa0-4bed-8418-cc613551321a', N'edit from app', 1, CAST(N'2017-08-08T13:21:13.907' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (11, N'a42ba1d5-69e8-428b-a5ed-b694b83e89f0', N'9bc073fa-bd46-49d6-bb45-459820bd5dc4', N'Dear Gretel Cruz,

I have updated your original quote, please review it below. If you wish to continue with the reservation, click the "Pay Now" button below the quote to make a payment. 

Sincerely,
Kevin Chu', 1, CAST(N'2017-08-08T12:05:44.793' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (12, N'31dbd5a3-7f69-4e67-8c46-0f7610cc1fb9', N'23a66954-a392-4bef-9cca-0f9d4dd43281', N'testing from app', 1, CAST(N'2017-08-07T09:02:15.013' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (13, N'e517c219-a356-4ff8-b645-9c1ee1dc2f63', N'23a66954-a392-4bef-9cca-0f9d4dd43281', N'Dear Gretel Cruz,

I have updated your original quote, please review it below. If you wish to continue with the reservation, click the "Pay Now" button below the quote to make a payment. 

Sincerely,
Kevin Chu', 1, CAST(N'2017-08-06T19:55:22.473' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (14, N'aa27e4bc-5277-4d59-8296-ac8ab9f39694', N'23a66954-a392-4bef-9cca-0f9d4dd43281', N'Hi I would like to book the place again!', 0, CAST(N'2017-08-06T19:38:31.740' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (15, N'2317a431-aacf-45d5-8f3c-de03dad434d3', N'e65fdf9d-db7d-4c65-b16e-4e2f3e466140', N'Hi Frank, I have someone about to book the place. I hope to host your family next time :)', 1, CAST(N'2017-08-03T03:52:32.100' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (16, N'33132c9d-479f-4eab-9fbf-a569a8dab206', N'e65fdf9d-db7d-4c65-b16e-4e2f3e466140', N'Hi,
we are looking for a house or condo for our two last vacation days (08/07 to 08/09) before er have to leave to Germany.
Best regards
Frank', 0, CAST(N'2017-08-03T02:36:38.093' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (17, N'712613ec-3744-4138-b890-f64cc8216b7b', N'f1f00771-855b-40a5-aa7a-90a8727ea6b2', N'test reply', 1, CAST(N'2017-08-08T11:32:23.720' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (18, N'949be4de-04b5-4f54-b981-94706bc21856', N'f1f00771-855b-40a5-aa7a-90a8727ea6b2', N'demoing alter reservation', 1, CAST(N'2017-07-28T20:20:07.280' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (19, N'aee1316e-1aa4-44e3-af6e-ff5f1e7626c0', N'f1f00771-855b-40a5-aa7a-90a8727ea6b2', N'special offer', 1, CAST(N'2017-07-28T19:50:43.043' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (20, N'76b854b1-54da-4030-b529-60a29330c874', N'f1f00771-855b-40a5-aa7a-90a8727ea6b2', N'I want to!', 0, CAST(N'2017-07-28T19:24:50.117' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (21, N'2aed3dab-ac73-4ff0-a164-27251ce81640', N'f2d20091-e85a-4bda-a289-17c05a088739', N'testing..', 1, CAST(N'2017-08-08T11:31:12.773' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (22, N'be7dd51f-ccf5-411b-b548-0de2f3353bdb', N'f2d20091-e85a-4bda-a289-17c05a088739', N'Hi I really want to book it!', 0, CAST(N'2017-07-28T19:23:21.223' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (23, N'12c9d2f0-9026-4547-ac87-15bc924221ba', N'f2d20091-e85a-4bda-a289-17c05a088739', N'Sorry, we can''t reserve at the moment', 1, CAST(N'2017-07-28T19:22:46.437' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (24, N'42d3d405-7617-4157-a8ee-f387abb04955', N'f2d20091-e85a-4bda-a289-17c05a088739', N'Hi I am interested to book this.', 0, CAST(N'2017-07-28T19:05:49.480' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (25, N'3012dff2-b0de-4438-b101-7945a4170450', N'4ed91711-cb39-4e51-b6df-fb615033efa0', N'testing from app', 1, CAST(N'2017-07-25T19:08:11.513' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (26, N'91aa4071-9745-46e1-811d-6e47e8750fd1', N'4ed91711-cb39-4e51-b6df-fb615033efa0', N'testing edit quote', 1, CAST(N'2017-07-25T18:40:43.353' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (27, N'3d5d0cc2-9ccb-4bc3-b436-4b3ccaaf14f3', N'4ed91711-cb39-4e51-b6df-fb615033efa0', N'Hi, I would like to book 1 night for August 6 and August 7', 0, CAST(N'2017-07-24T21:12:39.150' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (28, N'bf1822c9-2ac2-416f-85d0-cfe0e4aa04d6', N'd869318e-b035-44c2-ba71-4ca9b3a0512b', N'Is the rate of $5000 per night correct?', 0, CAST(N'2017-07-20T22:13:37.607' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (29, N'ec668cc6-2077-4046-8c83-b7bfe07349d9', N'20802cbf-706c-42c9-a284-14fa806806d5', N'testing', 1, CAST(N'2017-07-20T19:35:35.250' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (30, N'eb64bd3a-0ef4-4653-bbe5-6fbe58a37e9a', N'20802cbf-706c-42c9-a284-14fa806806d5', N'Hi re-booking now', 0, CAST(N'2017-07-20T18:49:37.987' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (31, N'cc3663a4-8462-411a-8d97-5ad0b117c456', N'5c58a27d-dcec-417d-9520-978e2d239e88', N'this is to test edit quote', 1, CAST(N'2017-07-18T15:18:03.790' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (32, N'91128791-cf7d-405e-88f8-851460e84750', N'5c58a27d-dcec-417d-9520-978e2d239e88', N'Dear Gretel Cruz,

I am sorry that we are unable to confirm your reservation at property 827199 for Aug 1, 2017 - Aug 2, 2017. You have not been charged for this reservation and you may now book another property.

Sincerely,
Kevin Chu', 1, CAST(N'2017-07-18T14:28:41.290' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (33, N'c1dc6a60-157e-484e-8c59-beeb3badd583', N'5c58a27d-dcec-417d-9520-978e2d239e88', N'Hi did you get my booking request?', 0, CAST(N'2017-07-17T17:22:36.400' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (34, N'b50d5395-39ae-4849-99dd-0a26783edeca', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'testing demo', 1, CAST(N'2017-07-07T20:55:18.303' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (35, N'a2475ac8-ec6d-4547-a9ab-83acba93d959', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'unread testing', 0, CAST(N'2017-07-07T20:47:42.040' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (36, N'1b9e67fd-bb2c-4060-8fc5-900fd139b9f3', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'test passed', 0, CAST(N'2017-07-07T19:41:50.130' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (37, N'c983c3f1-a808-4d6b-90ed-a7ec561a575d', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'testing..', 1, CAST(N'2017-07-07T19:31:34.543' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (38, N'ef552af4-e403-4354-8e41-a5c117a102a2', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'hello testing', 1, CAST(N'2017-07-07T11:38:45.680' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (39, N'9d4c6aae-1498-4c84-9fac-48af6ee0e75a', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'Dear Gretel Cruz,

I have confirmed your reservation at property 827199 for Sep 12, 2017 - Sep 15, 2017.

As a reminder, your payment has been processed and will appear as PAY*HOMEAWAY on your statement.
I will be in contact with you prior to your arrival with access instructions.

Please let me know if you have any questions regarding your upcoming stay.

Sincerely,
Kevin Chu', 1, CAST(N'2017-07-04T13:04:52.280' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (40, N'234ffd6c-218f-4fef-a8d1-8ba20fbdc65f', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'Dear Gretel Cruz,

Thank you for your inquiry on my vacation rental (827199).  I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out on --------, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)?

My property is available from Sep 12, 2017 - Sep 15, 2017.   If you have any questions or need clarifications, do feel free to message me here anytime.  

Best Regards,
Kevin Chu', 1, CAST(N'2017-07-04T12:58:45.453' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (41, N'd21a3c52-10e8-48c5-a583-0f0f223ad3b4', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'Hi Gretel,

Testing', 1, CAST(N'2017-07-02T20:12:49.847' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (42, N'f312b399-9f92-4555-8efb-1c33ea58512f', N'b2469a39-be51-4f8e-9585-2fb060c42002', N'Hi I am very interested in booking your place. Can you tell me more?
Gretel', 0, CAST(N'2017-07-02T20:08:40.183' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (43, N'bed39223-07de-4935-b50d-0ad57b53aaf1', N'c73e70ac-6df7-4b87-9c9b-05926999154a', N'Noted on that Michael.
Thank you.', 1, CAST(N'2017-06-27T04:39:53.690' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (44, N'9ed60ce6-1fc6-409d-bfe8-62b0c02f8491', N'c73e70ac-6df7-4b87-9c9b-05926999154a', N'Hi Michael,

Thank you for your inquiry on my vacation rental (827199). My property is available from Oct 29, 2017 - Nov 4, 2017 and you are invited to book for these dates. For your convenience, you are pre-approved to book and your reservation will be confirmed instantly once a payment is made.

Please let me know if you have any questions. Hope to have you as a guest and I look forward to speaking with you soon.', 1, CAST(N'2017-06-27T04:39:23.100' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (45, N'3c94aeef-1c0b-4ce5-ba71-b0f093b2187c', N'c73e70ac-6df7-4b87-9c9b-05926999154a', N'Thanks Kevin, but we found another house near the golf course where we will be playing', 0, CAST(N'2017-06-27T03:59:33.873' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (46, N'0bbcdffc-6de4-4146-a1a3-a7ec485ed4ae', N'c73e70ac-6df7-4b87-9c9b-05926999154a', N'Hi Michael, service fee and any other fees collected by VRBO are none refundable. Reservation costs are subject to the cancellation policy of the host.  You may either get full refund or partial refund. In some cases when a reservation is cancelled less than a week from check in date, no refund (depending on host''s cancellation policy) may be given. 

I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?
 
We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Regards,
Kevin', 1, CAST(N'2017-06-26T17:15:47.700' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (47, N'1ae499ce-02f6-4629-9c1c-c9cdd3b263a7', N'c73e70ac-6df7-4b87-9c9b-05926999154a', N'We plan to visit Las Vegas for vacation and would like to know the terms of rental on your home.  Is any part of the fee refundable?', 0, CAST(N'2017-06-25T20:22:39.177' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (48, N'6cd3c2f3-0da9-433b-9a17-44416854d8b5', N'96012fb6-12e6-4b5e-bb06-f9af5e827c2f', N'Hi Pamela
I would be glad to host you for the special occassion. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review the house rules and are all ok with them?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to get to know each one who will be coming. Can you tell me a little bit about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?


Regards,
Kevin', 1, CAST(N'2017-06-25T14:57:01.640' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (49, N'176b3f80-669b-49f3-9682-9cc571ba18d2', N'96012fb6-12e6-4b5e-bb06-f9af5e827c2f', N'I see there are no reviews on your place.  Have you any patio furniture out back? I see you have the highest damage deposit of all $900 even more then the luxury places that are thousands per night.  We are very careful people but $900 is a lot of money, I don''t want to have some loophole take it away.  Your place looks comfortable and clean just wondering about these questions?  We are coming for a graduation at Touro University and want our family to enjoy spending time together.', 0, CAST(N'2017-06-21T19:01:39.737' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (50, N'5b351889-fb97-48f5-829a-212f4d5ad77b', N'f4fcaab6-3e7e-4f07-8da8-e9304bf203fe', N'Just got off cruise sorry didn''t have service thanks will work this asap', 0, CAST(N'2017-06-12T14:41:26.580' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (51, N'a2854ece-def5-48a0-9b19-3a2b0dc95751', N'f4fcaab6-3e7e-4f07-8da8-e9304bf203fe', N'Hi Shirley, yes it is available.  All payments are coursed thru VRBO''s payment system. We do not personally handle them.  Which arena are you referring to?  For reference, we are roughly 15 miles away from The Strip.  I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with everything stated therein?
 
Also, we live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming before we commit. Can you please tell me more about each person I will potentially be hosting (i.e. how many are coming, who they are, what they do, ages, what brings you to Las Vegas, where are you coming from, etc)? 

Regards,
Kevin', 1, CAST(N'2017-06-08T23:43:04.377' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (52, N'19f91092-b396-4a34-ae81-23dff8111401', N'f4fcaab6-3e7e-4f07-8da8-e9304bf203fe', N'Interested in booking your house is it available ?
Can I put a deposit down? 
Are you near the arena', 0, CAST(N'2017-06-08T15:56:37.643' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (53, N'11f4ba61-e3e7-46e1-9a59-1900e7e9139a', N'67f9e07a-ea9a-406b-bfb7-c6dbc08e1908', N'Hi Catherine,
Thank you for your inquiry.
Unfortunately, the dates you want to book are not available.

Have a great day.', 1, CAST(N'2017-06-07T15:24:24.983' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (54, N'5a520e55-1eb1-409f-ba69-d13bcb671b5a', N'67f9e07a-ea9a-406b-bfb7-c6dbc08e1908', N'Hello. my name is Cat Bates. I am a travel agent who is planning a special New Years vacation for a client and some friends. I love your property and the reviews. 

1) Is your property available for the requested dates?
2) do you work with travel agents?
3) What is the total price and payment schedule for the stay?

Thank you.', 0, CAST(N'2017-05-31T21:51:38.620' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (55, N'1faf6749-6bf9-49c7-8c41-90edffe1ec9e', N'1b922475-6904-46d9-b3e5-ec7f51fa2645', N'Hi Jasmine, its $900.  - Kevin', 1, CAST(N'2017-04-27T01:16:12.263' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (56, N'e6d59324-8409-4ab0-ba76-fd4f2f346225', N'1b922475-6904-46d9-b3e5-ec7f51fa2645', N'Hi i wanted to know what is the deposit for booking?', 0, CAST(N'2017-04-26T23:44:37.060' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (57, N'a99a4719-2790-4146-a43c-3b054952ac33', N'd96d8df5-a4e8-4463-9139-5041b92e548b', N'Dear Shakeya Knight,

Thank you for your inquiry on my vacation rental (827199).  We do not hold dates nor do we handle deposits. All payments upon booking are courses thru VRBO.  I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out on --------, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)?

My property is available from Sep 8, 2017 - Sep 13, 2017.   If you have any questions or need clarifications, do feel free to message me here anytime.  

Best Regards,
Kevin Chu', 1, CAST(N'2017-04-22T20:34:14.157' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (58, N'3ec96800-9d3d-449f-aa33-155c4c6649eb', N'd96d8df5-a4e8-4463-9139-5041b92e548b', N'How much would be the deposit to hold the home?', 0, CAST(N'2017-04-22T20:27:39.620' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (59, N'74633e0a-703d-43be-b538-ef52bc60fd54', N'0b7bf168-7267-40e7-a7e0-322180ffb2bb', N'Hi Renetta,

What dates did you book for? I dont think you inquired before about my listing.  I look forward to hosting you all. And I have indicated a few things under the listing''s description and house rules section, please take time to read them all and let me know if you and everyone in your group are all ok with the house rules, before booking.

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, what brings you to Las Vegas, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Thank you.

Regards,
Kevin
___________________________________________________', 1, CAST(N'2017-04-10T02:17:08.460' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (60, N'657c640d-3674-4ca0-95e5-1579f7ac0660', N'0b7bf168-7267-40e7-a7e0-322180ffb2bb', N'I am not sure if I reached out to you about the fees and total for the time I want to stay.   Can you send to me please?', 0, CAST(N'2017-04-10T02:04:37.823' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (61, N'425c0a6d-78f4-4227-a95f-5059a7c05f9c', N'411a15ad-b6db-49f0-852a-1cb6849eddae', N'Hi Ravette,

Thank you for your inquiry and we look forward to hosting you and your family. The dates you wanted to book (Jul 27, 2017 - Jul 31, 2017) is still available as of this writing.  The address you provided is approximately 16 mins away by car.  I have indicated a list of house rules under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with all of them?

We live in this house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Regards,
Kevin', 1, CAST(N'2017-04-09T00:00:51.517' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (62, N'd5296f09-2356-43f1-8e9d-bdad0d0447d4', N'411a15ad-b6db-49f0-852a-1cb6849eddae', N'Hello,

I''m interested in booking your house from July 27- July 30th.  We are coming to Las Vegas for a family reunion fun-filled weekend.  I wanted to know if you house was located close to where our reunion will be located.  The address is 8816 Tierra Hope Ct, Las Vegas, NV 89143.  Please let me know.  Thanks in advance for your help.', 0, CAST(N'2017-04-05T21:30:07.583' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (63, N'85f18cb7-26ae-4986-ac49-c809b7de89af', N'618fecc9-3eba-41b5-ae5c-3b2c98e79c44', N'Hi Tsering,

Thank you for your inquiry, I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it? Please let me know if you are all ok with it?

Can you also tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?  We live in this house ourselves when we are not renting it out, so we would appreciate getting to know a little bit about our guests. :-) 

Regards,
Kevin', 1, CAST(N'2017-03-15T19:16:52.487' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (64, N'f1317bc4-d418-4b40-a2c5-8d41d0fa0735', N'07ce5a06-463c-49a2-982a-083d9f4d15a4', N'Dear jamelia brown,

Thank you for your inquiry. We do have washer and dryer, wifi and cable. But we do not have tv in every room.   I have indicated a few things under the listing''s description and house rules section, just want to know if each adult in your group had the chance to review it and are all ok with it?

Also, I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)?

Best Regards,
Kevin Chu', 1, CAST(N'2017-03-09T17:04:37.010' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (65, N'058d3cc3-dc66-401f-9ea6-c86e14644330', N'07ce5a06-463c-49a2-982a-083d9f4d15a4', N'Good Morning!!

I am interested in renting your property but, have a few questions.

1. is there a washer and dyer
2. Wi-Fi
3. cable
4. is there TV''s in all the rooms

Thank you and have a great day!!', 0, CAST(N'2017-03-09T15:16:39.030' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (66, N'a884c0fd-fb72-4cac-b3a0-e5ea915b3be3', N'245f2d18-6163-49b6-a9d5-aa90d0fb3007', N'Dear Jose Leon,

Thank you for your inquiry on my vacation rental (827199).  I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out on --------, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, what they do, ages, etc)?

My property is available from May 26, 2017 - May 29, 2017.   If you have any questions or need clarifications, do feel free to message me here anytime.  

Best Regards,
Kevin Chu', 1, CAST(N'2017-03-08T19:13:23.587' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (67, N'20b40a71-e4b8-4a93-89dd-cf6ff781aa5b', N'245f2d18-6163-49b6-a9d5-aa90d0fb3007', N'May 26th to may 29th', 0, CAST(N'2017-03-08T15:45:08.327' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (68, N'e1576eec-abea-4973-b839-4164c69101f5', N'4a40e08d-c233-422a-95fb-4c1443c5eb2e', N'Thank you. They have changed their plans an no longer need the home.

~ Mareyka Webb
Excuses are the tools of the incompetent', 0, CAST(N'2017-03-07T19:43:24.430' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (69, N'7d3c02df-4d3b-441b-b549-d15722192b77', N'4a40e08d-c233-422a-95fb-4c1443c5eb2e', N'Hi Mareyka, 

All payments are coursed thru the VRBO payment system.  I believe they charge in full and remit amount later on to hosts.  There is one queen sized bed in each of the 6 bedrooms which can be shared by 2 people in each room.  We also have a couple of convertible couches that unfolds to queen sized bed and each can also sleep 2 people.', 1, CAST(N'2017-03-07T19:28:35.723' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (70, N'9e5f1641-c1a0-4b48-a9bd-f6f25fa583dc', N'4a40e08d-c233-422a-95fb-4c1443c5eb2e', N'Hi Mareyka,

Thank you for your inquiry on my vacation rental (827199). My property is available from Jul 25, 2017 - Jul 31, 2017 and you are invited to book for these dates. I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Regards,
Kevin', 1, CAST(N'2017-03-07T19:25:35.887' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (71, N'b822f49b-417a-4e2b-aef9-5852e088f3e5', N'4a40e08d-c233-422a-95fb-4c1443c5eb2e', N'Hello, my clients are a basketball team coming in tour for a basketball tournament. What would the deposit for your home be and when would it be due? Also when is the final payment due? How many beds in each room? Thank you!', 0, CAST(N'2017-03-06T06:33:37.490' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (72, N'69a00f04-db37-4439-90d1-9e7184d4bd5b', N'6c9230cc-3226-4218-8c3f-28249ce2525b', N'Hi Travis,

Thank you for your inquiry on my vacation rental (827199). My property is available from Apr 6, 2017 - Apr 10, 2017.  I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Regards,
Kevin', 1, CAST(N'2017-03-07T19:24:12.040' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (73, N'4d0155a5-eb46-4831-9491-ebef48633cd2', N'6c9230cc-3226-4218-8c3f-28249ce2525b', N'Hello, I would like to know if your property is available for the following days.', 0, CAST(N'2017-03-07T19:03:41.023' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (74, N'de9fe69a-1245-4d35-9fd9-af59c07ac293', N'3c3cb885-37e4-4875-86f9-d2288f3e331a', N'Disregard', 0, CAST(N'2017-02-26T14:40:56.593' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (75, N'b534338e-61e4-4afe-a727-cc384a1d4c60', N'3c3cb885-37e4-4875-86f9-d2288f3e331a', N'Hi,
I''m interested in renting your home during my son''s basketball tournament.  Can you tell how far your home is located from Bishop Gorman High School at 5959 S Hualapai Way, Las Vegas, NV 89148?  Also, do you accept deposits/partial payments?  I look forward to hearing from you.

Regards,
Meredith

**2 kids are both 16.  Adult age range is 18-47. The 18 yr old is my daughter.', 0, CAST(N'2017-02-26T14:31:09.150' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (76, N'e15c7304-74ca-4264-93a8-a337b32f287e', N'3c3cb885-37e4-4875-86f9-d2288f3e331a', N'Hi Meredith,

Thank you for your inquiry but I''m sorry I do not accept partial payment.  It is also preferable to course all payments thru the VRBO payment system.  Also, I have indicated some house rules under the listing''s description page, and just want to know if you and each one in your group had the chance to review it and are all ok and can abide with it?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Best Regards,
Kevin', 1, CAST(N'2017-02-15T01:20:08.807' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (77, N'68fa78f4-7152-4b3c-8a69-932091cf2219', N'3c3cb885-37e4-4875-86f9-d2288f3e331a', N'Hi,
I''m interested in renting your home during my son''s basketball tournament.  Can you tell how far your home is located from Bishop Gorman High School at 5959 S Hualapai Way, Las Vegas, NV 89148?  Also, do you accept deposits/partial payments?  I look forward to hearing from you.
Regards,
Meredith', 0, CAST(N'2017-02-13T01:07:07.730' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (78, N'd6955476-0c60-4027-b2fe-211ed7ba0dc9', N'1647dd26-d59b-4999-8c56-dbd6a5154b8e', N'Hi Robert ,

VRBO booking system handles the reservation and all payments are coursed thru them too for security reasons.  We also would like to know first something about each person we will potentially be hosting (i.e who they are, how many are coming, where are you coming from, what brings you to Las Vegas, what you do, ages, etc.)  We live in this house ourselves when we are not renting it out, so hosting is kinda personal to us and would appreciate knowing a little bit about who we are hosting.  :-)

I also have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review all the house rules and are all ok with it?  We highly recommend this so our guest would know what to expect.  After letting me know you are fine with the house rules, you may choose to proceed to finalize your booking and VRBO will send us the request to accept and VRBO does the collection.  You can read further at the VRBO Help and Support section thier FAQs on how these works.  :-)

Regards,
Kevin', 1, CAST(N'2017-02-24T19:10:27.467' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (79, N'b82f5f65-6acb-4583-84b2-973b6dff56f1', N'1647dd26-d59b-4999-8c56-dbd6a5154b8e', N'We would like to know how we go about renting this house for our family vacation', 0, CAST(N'2017-02-24T18:50:08.100' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (80, N'eb386dbd-1a27-4508-a1b7-a5c97ed27458', N'95020efe-6a70-424a-9fc2-c8192c4a3ed1', N'Hi Comerly, I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Regards,
Kevin', 1, CAST(N'2017-01-15T23:26:22.930' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (81, N'b2e35322-24c8-4564-8dc4-09d5337fc36a', N'95020efe-6a70-424a-9fc2-c8192c4a3ed1', N'Trying to find a house to accommodate my family', 0, CAST(N'2017-01-15T23:12:09.150' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (82, N'2a681419-e55e-44da-bb43-fd7b4ea96edc', N'3fc635f1-e306-4f46-9619-1046834c50ca', N'Thank you. We choose another home.
Thank you again.', 0, CAST(N'2017-01-12T19:39:32.730' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (83, N'b336c80d-b17a-4dc2-bfc2-fb060c381690', N'3fc635f1-e306-4f46-9619-1046834c50ca', N'We do not have a payment plan and it is highly recommended that guest use the vrbo payment system.', 1, CAST(N'2017-01-12T19:36:52.930' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (84, N'bf02521b-5925-46c0-af7d-672107400148', N'3fc635f1-e306-4f46-9619-1046834c50ca', N'Hi Apryl,

Thank you for your inquiry on my vacation rental. My property is available from Mar 16, 2017 - Mar 20, 2017.

I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all ok with it?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, how many are staying, are you all arriving at same time or separately, where are you coming from, what they do, ages, etc)?

Please let me know if you have any questions. 

Regards,
Kevin', 1, CAST(N'2017-01-12T19:34:17.053' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (85, N'cc73f3f6-ba4d-49d1-a7df-f5cc68eae0fe', N'3fc635f1-e306-4f46-9619-1046834c50ca', N'Please confirm that this property is available. Also can we arrange a payment plan?', 0, CAST(N'2017-01-12T19:05:42.263' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (86, N'c1a6afd4-45a6-4e8f-90d4-5f0abd872699', N'8dd38166-f329-4f2a-a7f1-ee449a5cdbf9', N'Thank you for letting me know. Enjoy Las Vegas! 😊', 1, CAST(N'2016-11-29T17:45:57.317' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (87, N'f16f2104-1a74-483b-9d87-56c18e215989', N'8dd38166-f329-4f2a-a7f1-ee449a5cdbf9', N'Hi Kevin,

Thank You for your reply to my inquiry. I appreciate it greatly, but I
don''t think your home will work not out for our large family retreat.
Thanks again.

Stacie', 0, CAST(N'2016-11-29T17:23:42.030' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (88, N'bbf13446-1ec5-4226-bbeb-1194e55cd33a', N'8dd38166-f329-4f2a-a7f1-ee449a5cdbf9', N'Hi Stacie,

I look forward to hosting you all. I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it carefully and everyone are all ok with it?

We live in the house when we are not renting it out, so hosting is really personal to us. I wish to really get to know each one who will be coming. Can you tell me more about each person I will potentially be hosting (i.e. who they are, what they do, ages, are you all arriving at same time or separately, where are you coming from, etc)?

As for the occupancy, how old are the children?  Hope to hear from you.

Regards,
Kevin
____________________', 1, CAST(N'2016-11-28T20:11:31.020' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (89, N'fa735b97-b2d3-4308-94c7-3c497b40dfc8', N'8dd38166-f329-4f2a-a7f1-ee449a5cdbf9', N'Hello,

I am interested in renting your property for a family gathering which includes 13 adults and 7 children, exceeding your maximum occupancy. I would like to know if you will accept our request with the amount of guests during our stay from 2/18-2/23/17. If so, please let me know what the total would be including fees, cleaning fees, deposit, etc. I will share your property with the family and will get back to you asap. Your response is very much appreciated. Thank You for your time.

Staci', 0, CAST(N'2016-11-28T19:37:44.190' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (90, N'2cc26fd4-074e-4755-9c85-6d0be1915819', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'Crystal, I have uploaded the most recent photos.', 1, CAST(N'2016-11-18T15:15:24.150' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (91, N'b6e3aa25-9a3d-4a9c-a6ae-92f92aa1025c', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'Hi Crystal, I''m awaiting photos from my photographer.', 1, CAST(N'2016-11-15T23:28:03.883' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (92, N'44f93756-8526-45e7-8c9b-b466fda73d00', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'Kevin, I really would like to BOOK this place. Do you have more pictures
PLEASE', 0, CAST(N'2016-11-15T20:16:57.623' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (93, N'2ee9f2b4-e33e-4211-b0bb-1b259ae15617', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'We are very interested if you can send pictures when you can! Thank you for your time', 0, CAST(N'2016-11-15T05:09:22.840' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (94, N'2740c7a3-5b70-481f-8e40-070da4908011', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'Hello it is for myself, husband and our daughter she''s 12. Maybe my mom', 0, CAST(N'2016-11-15T05:05:10.603' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (95, N'80627969-3510-46e2-af91-baa426b05bf9', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'Hi, I just freshly listed it and still awaiting more photos from the photographer.

Can you tell me more about who I might be hosting?', 1, CAST(N'2016-11-15T04:43:23.440' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (96, N'f0cdcf77-4ded-4fcf-b520-366d58752470', N'63fcf52b-0f00-43e1-adab-b7f3075c8010', N'Do you have more pictures', 0, CAST(N'2016-11-15T03:51:38.250' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (97, N'2c56435a-0a30-4ebc-8db1-7d36b3201754', N'31fef948-ccf3-4883-8f11-8b0f4f462caa', N'Dear Jamie Gutierrez,

Thank you for your inquiry on my vacation rental and I look forward to hosting you all.  I have indicated a few things under the listing''s description and house rules section, just want to know if each one in your group had the chance to review it and are all okay with it?

My property is available from Dec 2, 2016 - Dec 5, 2016. 

We live in the house when we are not renting it out so hosting is personal to ask. I wish to really get to know each one who will be coming. Can you tell me a little bit about each person I will potentially be hosting (who they are, how many are coming, are you all coming from same location, what they do, ages, etc)?

Sincerely,
Kevin Chu', 1, CAST(N'2016-11-14T23:54:42.000' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (98, N'910936de-8ea6-410d-a122-8e5c41a039a8', N'31fef948-ccf3-4883-8f11-8b0f4f462caa', N'Hi, 

Were trying to plan a family trip up to Vegas to help my sister celebrate her 50th B-day. She lives in Vegas so were trying to find a house close to her''s and so far your it. She lives off of Aegean Way. I was wondering if it was available 12/2 - 12/5? 

Thank you,
Jamie', 0, CAST(N'2016-11-14T22:59:38.400' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (99, N'4aa6b4fb-5b1b-4f68-9a62-f0366166e0b1', N'0214c03b-ec53-4e13-9cec-034a266318f8', N'Hi, my yard is not furnished, it is just a desert landscape with some plants.', 1, CAST(N'2016-11-08T03:59:10.777' AS DateTime))
GO
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (100, N'72467ebb-3c01-457c-9e2e-08b79d2c00ea', N'0214c03b-ec53-4e13-9cec-034a266318f8', N'To confirm your reservation with the owner, click the button, review and agree to the rental agreement, and make a secure payment through HomeAway.', 1, CAST(N'2016-11-08T03:57:28.847' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (101, N'2d474fc9-85bc-47bd-bb82-4ddb40eeced0', N'0214c03b-ec53-4e13-9cec-034a266318f8', N'Having guest from california for event that weekend and looking to find house close to my home near red rock estates. Can you send me some photos of home,not just the bedrooms? Would like to know if it has backyard? Thank you in advance,

Gary White', 0, CAST(N'2016-11-07T19:17:39.540' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (102, N'ad05652d-d876-439b-ab27-c8cf8925cb42', N'09694f2f-b416-46a0-a971-a831a44a80a6', N'Hi Nikki, I''m sorry i cannot accomodate the direct payments via cheques and prefer the booking to be done online through VRBO. 

Would also appreciate it if you can tell me something about each person staying (how many are coming, who they are, what they do, are you all arriving together, where is everyone from, what they do, ages, etc).  We live in this house ourselves so you can imagine how personal hosting is to us.

Have you and your party read all the house rules and is everyone fine with everything stated therein?  This is so we know what to expect. 

As for the yard, we do not have a furnished yard other than a desert landscaped yard with some plants.

Regards,
Kevin', 1, CAST(N'2016-11-07T19:35:24.093' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (103, N'be7a6f81-91fb-4d0b-bd27-79a78a3d36f0', N'09694f2f-b416-46a0-a971-a831a44a80a6', N'I just read your message and i want you to know that I am presently in Japan for our company''s Project. I''ll like to complete the reservation before i get home. I''ll make an arrangement with our Accountant to make and mail a Check for the full amount through Regular Mail so we can be assured of our reservation. I don''t have access to a bank transfer nor have a paypal account.. I can only instruct my Accountant to issue you a check and send to you for easy cashing at your bank so i hope you can understand. So provide me with the information she need below:

She will need the above information...
Your Full Name,
Billing Address {Not P.O.Box}
City
State,
Zip code
Phone number / Cell phone
Get back to me as soon as possible.....
Have a lovely week.
Sent: Sunday, November 06, 2016 at 7:16 PM', 0, CAST(N'2016-11-07T19:12:08.517' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (104, N'3fbcff39-df77-4b08-8351-842e76732f09', N'09694f2f-b416-46a0-a971-a831a44a80a6', N'Hi Nikky,

Yes the house is available so far.

Can you tell me who is coming and what brings you to Las Vegas?


Kevin', 1, CAST(N'2016-11-07T01:16:22.030' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (105, N'f9938bbd-6dc4-4fc1-b465-d9c5d473c291', N'09694f2f-b416-46a0-a971-a831a44a80a6', N'To confirm your reservation with the owner, click the button, review and agree to the rental agreement, and make a secure payment through HomeAway.', 1, CAST(N'2016-11-07T01:10:27.003' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (106, N'a62873b4-fccb-41d6-89bc-546bc95d46f5', N'09694f2f-b416-46a0-a971-a831a44a80a6', N'Hello Owner, how are you doing today? I want to know if your place will be available  from 2nd of Dec to 6th of Dec.Kindly get back to me as soon as possible.

Nikky', 0, CAST(N'2016-11-06T23:49:39.533' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (107, N'54a39558-103a-4497-8571-e825f3392c40', N'd2741b19-86e1-4784-8b7f-371844735069', N'Testing', 1, CAST(N'2017-09-13T14:39:07.287' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (108, N'2e70e740-fbad-46e7-89d3-7ab64eb9a87f', N'd2741b19-86e1-4784-8b7f-371844735069', N'Test reply again', 1, CAST(N'2017-09-13T13:30:25.360' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (109, N'f44bfe62-4df9-4d1a-a7b1-93ec0efa66b6', N'd2741b19-86e1-4784-8b7f-371844735069', N'Test reply', 1, CAST(N'2017-09-13T13:18:48.033' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (110, N'7c6bdf07-da0d-4d41-9858-9a8774185e3f', N'd2741b19-86e1-4784-8b7f-371844735069', N'Test', 1, CAST(N'2017-09-13T14:58:57.100' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (111, N'6bfb256a-f57b-468f-aefe-0a55368ed08a', N'd2741b19-86e1-4784-8b7f-371844735069', N'Test123', 1, CAST(N'2017-09-13T14:49:35.367' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (112, N'a8c6723c-c0d6-4b19-b6e4-37939d6cb4df', N'd2741b19-86e1-4784-8b7f-371844735069', N'Testing again', 1, CAST(N'2017-09-13T15:06:21.493' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (113, N'a54d7bd5-4c26-47ed-b197-1191d0dfe831', N'd2741b19-86e1-4784-8b7f-371844735069', N'Testing demo', 1, CAST(N'2017-09-13T15:10:30.800' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (114, N'8ba3c1fe-2472-4abb-b630-064b4af3083d', N'6b1a5b88-a5a3-4688-8039-e487d3973d70', N'Hi I am interested to book your house for 2 of us this coming week.', 0, CAST(N'2017-09-14T13:13:25.063' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (115, N'01b5c46f-b2dd-4016-8b53-4e1f85e93838', N'721618ef-4546-416e-8189-bc0033564f1e', N'I am re-sending my booking', 0, CAST(N'2017-09-18T05:07:57.403' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (116, N'bacfcc6d-35b4-407d-8ad7-5ffa4999912c', N'6b1a5b88-a5a3-4688-8039-e487d3973d70', N'Re-sending my booking request', 0, CAST(N'2017-09-18T05:03:51.927' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (117, N'86275b62-5cb7-4b14-b15a-90410c4ddfed', N'1701f70f-744d-4dbc-9e21-a24958cb9a2c', N'Dear Melanie Reese,

Unfortunately, we are unable to confirm your reservation at our vacation rental for Sep 19, 2017 - Sep 20, 2017. Your card has not been charged for this reservation, and you are free to book another property.

Thank you,
Kevin Chu', 1, CAST(N'2017-09-18T05:01:13.987' AS DateTime))
INSERT [dbo].[Message] ([Id], [MessageId], [ThreadId], [MessageContent], [IsMyMessage], [CreatedDate]) VALUES (118, N'4ee9a637-ca34-4e83-b717-c0266c984859', N'1701f70f-744d-4dbc-9e21-a24958cb9a2c', N'We live in Hurricane, Utah. We have an early flight on Sept 20 flying out of Vegas to denver for a concert and don''t want to drive down Wed morning so just looking for a place to sleep for one night.', 0, CAST(N'2017-09-18T04:26:19.700' AS DateTime))
SET IDENTITY_INSERT [dbo].[Message] OFF
SET IDENTITY_INSERT [dbo].[PaymentMethod] ON 

INSERT [dbo].[PaymentMethod] ([Id], [Name]) VALUES (1, N'Payment thru Airbnb')
INSERT [dbo].[PaymentMethod] ([Id], [Name]) VALUES (2, N'Cash')
INSERT [dbo].[PaymentMethod] ([Id], [Name]) VALUES (3, N'Bank Deposit')
INSERT [dbo].[PaymentMethod] ([Id], [Name]) VALUES (4, N'Check')
INSERT [dbo].[PaymentMethod] ([Id], [Name]) VALUES (5, N'Online Payment')
SET IDENTITY_INSERT [dbo].[PaymentMethod] OFF
SET IDENTITY_INSERT [dbo].[Property] ON 

INSERT [dbo].[Property] ([Id], [ListingId], [ListingUrl], [Name], [Longitude], [Latitude], [Address], [UnitCount], [MinStay], [Internet], [Pets], [WheelChair], [Description], [Reviews], [Sleeps], [Bedrooms], [Bathrooms], [Type], [PropertyType], [AccomodationType], [Smoking], [AirCondition], [SwimmingPool], [PriceNightlyMin], [PriceNightlyMax], [PriceWeeklyMin], [PriceWeeklyMax], [Images], [Status], [MonthlyExpense], [ExpenseNotes], [ActiveDateStart], [ActiveDateEnd], [CommissionId], [CommissionAmount], [AutoCreateIncomeExpense], [CheckInTime], [CheckOutTime], [IsLocallyCreated], [IsParentProperty], [ParentPropertyId], [CompanyId], [SiteType]) VALUES (1, 827199, N'https://admin.vrbo.com/lm/321.827199.1375137/location.html', N'Large 6 Bedroom House', N'-115.276748', N'36.22531499999999', NULL, 0, 1, N'Yes', N'False', N'wheelchair inaccessible', N'The house is located 14 miles to the Las Vegas Strip in one of the best neighborhood called Summerlin.

One corner away is a very modern YMCA where you can get a day pass for $30 to use the gym and the pool. A golf course is also available there.
Some guests mentioned that it has better amenities than the 24-hour Fitness nearby.


There are 6 bedrooms and 3 full bathrooms. 
Each bedroom has a queen sized bed that can sleep 2 people each.
The 2 sofas in the living room pulls out into queen sized beds. 
There is also a full sized bed in the family room that can sleep 2.
All in all 18 people can be accommodated.', 0, 0, 6, 3, 0, N'house', N'Vacation Rental', N'False', N'No', N'No', NULL, NULL, NULL, NULL, NULL, N'ACTIVE', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, -1, CAST(0.00 AS Decimal(18, 2)), 0, CAST(N'00:00:00' AS Time), CAST(N'00:00:00' AS Time), 0, 0, 0, 1, 2)
INSERT [dbo].[Property] ([Id], [ListingId], [ListingUrl], [Name], [Longitude], [Latitude], [Address], [UnitCount], [MinStay], [Internet], [Pets], [WheelChair], [Description], [Reviews], [Sleeps], [Bedrooms], [Bathrooms], [Type], [PropertyType], [AccomodationType], [Smoking], [AirCondition], [SwimmingPool], [PriceNightlyMin], [PriceNightlyMax], [PriceWeeklyMin], [PriceWeeklyMax], [Images], [Status], [MonthlyExpense], [ExpenseNotes], [ActiveDateStart], [ActiveDateEnd], [CommissionId], [CommissionAmount], [AutoCreateIncomeExpense], [CheckInTime], [CheckOutTime], [IsLocallyCreated], [IsParentProperty], [ParentPropertyId], [CompanyId], [SiteType]) VALUES (2, 1157366, N'https://admin.vrbo.com/lm/321.1157366.1705606/location.html', N'5 Bedroom house, 14 miles from the Strip', N'-115.1031908', N'36.2725102', NULL, 0, 3, N'No', N'False', N'ask owner', N'This is a large house located in a very quiet neighbourhood.

There are 5 bedrooms with queen sized beds each and 3 bathrooms.
A futon at the playroom folds out into a full sized bed and can sleep 2 people.

All in all a group of 12 can be accommodated in the house.

Drive wise, you will be about 14 miles from the Strip.
If you Uber, the cost of UberX is about 18-23 on reugular non surge hours, while it will be about $5-9 if you use Uber Pool.', 0, 0, 5, 3, 0, N'house', N'Vacation Rental', N'False', N'No', N'No', NULL, NULL, NULL, NULL, NULL, N'ACTIVE', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, -1, CAST(0.00 AS Decimal(18, 2)), 0, CAST(N'00:00:00' AS Time), CAST(N'00:00:00' AS Time), 0, 0, 0, 1, 2)
INSERT [dbo].[Property] ([Id], [ListingId], [ListingUrl], [Name], [Longitude], [Latitude], [Address], [UnitCount], [MinStay], [Internet], [Pets], [WheelChair], [Description], [Reviews], [Sleeps], [Bedrooms], [Bathrooms], [Type], [PropertyType], [AccomodationType], [Smoking], [AirCondition], [SwimmingPool], [PriceNightlyMin], [PriceNightlyMax], [PriceWeeklyMin], [PriceWeeklyMax], [Images], [Status], [MonthlyExpense], [ExpenseNotes], [ActiveDateStart], [ActiveDateEnd], [CommissionId], [CommissionAmount], [AutoCreateIncomeExpense], [CheckInTime], [CheckOutTime], [IsLocallyCreated], [IsParentProperty], [ParentPropertyId], [CompanyId], [SiteType]) VALUES (3, 6144308, N'https://www.airbnb.com/rooms/6144308', N'Center of Downtown 1 Bedroom 1 Bath', N'', N'', NULL, 0, 3, N'Yes', N'Not allowed', N'Not allowed', N'You will be in a location right in the middle of downtown, steps away from Gastown, Yaletown and Coal Harbour.

The room is in a house right in the center of downtown. 
You will have a private bathroom with this room.', 10, 1, 1, 1, 1, N'House', N'Private room', N'Not allowed', N'No', N'Yes', N'5000 CAD', N'', N'', N'', N'', N'Unlisted', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, -1, CAST(0.00 AS Decimal(18, 2)), 1, CAST(N'16:00:00' AS Time), CAST(N'11:00:00' AS Time), 0, 0, 0, 1, 1)
INSERT [dbo].[Property] ([Id], [ListingId], [ListingUrl], [Name], [Longitude], [Latitude], [Address], [UnitCount], [MinStay], [Internet], [Pets], [WheelChair], [Description], [Reviews], [Sleeps], [Bedrooms], [Bathrooms], [Type], [PropertyType], [AccomodationType], [Smoking], [AirCondition], [SwimmingPool], [PriceNightlyMin], [PriceNightlyMax], [PriceWeeklyMin], [PriceWeeklyMax], [Images], [Status], [MonthlyExpense], [ExpenseNotes], [ActiveDateStart], [ActiveDateEnd], [CommissionId], [CommissionAmount], [AutoCreateIncomeExpense], [CheckInTime], [CheckOutTime], [IsLocallyCreated], [IsParentProperty], [ParentPropertyId], [CompanyId], [SiteType]) VALUES (4, 7472180, N'https://www.airbnb.com/rooms/7472180', N'Room with own bathroom in a house', N'', N'', NULL, 0, 3, N'Yes', N'Not allowed', N'Not allowed', N'This room is located at the very center of the city, very near Gastown, Yaletown and the Stadium.', 10, 1, 1, 1, 1, N'House', N'Private room', N'Not allowed', N'No', N'No', N'5000 CAD', N'', N'', N'', N'', N'Active', CAST(0.00 AS Decimal(18, 2)), NULL, NULL, NULL, -1, CAST(0.00 AS Decimal(18, 2)), 1, CAST(N'16:00:00' AS Time), CAST(N'11:00:00' AS Time), 0, 0, 0, 1, 1)
SET IDENTITY_INSERT [dbo].[Property] OFF
SET IDENTITY_INSERT [dbo].[PropertyType] ON 

INSERT [dbo].[PropertyType] ([Id], [Property_Type]) VALUES (1, N'House')
INSERT [dbo].[PropertyType] ([Id], [Property_Type]) VALUES (2, N'Apartment')
INSERT [dbo].[PropertyType] ([Id], [Property_Type]) VALUES (3, N'Condominium')
INSERT [dbo].[PropertyType] ([Id], [Property_Type]) VALUES (4, N'Townhouse')
SET IDENTITY_INSERT [dbo].[PropertyType] OFF
INSERT [dbo].[SiteType] ([Code], [Description]) VALUES (N'A', N'AirBnB')
INSERT [dbo].[SiteType] ([Code], [Description]) VALUES (N'L', N'Local')
SET IDENTITY_INSERT [dbo].[tbRole] ON 

INSERT [dbo].[tbRole] ([Id], [RoleName]) VALUES (1, N'admin')
INSERT [dbo].[tbRole] ([Id], [RoleName]) VALUES (2, N'guest manager')
INSERT [dbo].[tbRole] ([Id], [RoleName]) VALUES (3, N'accounting')
SET IDENTITY_INSERT [dbo].[tbRole] OFF
SET IDENTITY_INSERT [dbo].[tbRoleLink] ON 

INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (9, 1, 7)
INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (31, 2, 24)
INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (32, 1, 25)
INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (33, 1, 26)
INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (34, 1, 27)
INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (35, 1, 28)
INSERT [dbo].[tbRoleLink] ([Id], [RoleId], [UserId]) VALUES (1033, 1, 1026)
SET IDENTITY_INSERT [dbo].[tbRoleLink] OFF
SET IDENTITY_INSERT [dbo].[tbRolePage] ON 

INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (5, 3, N'messages', N'/Messages', 1, N'icon comments', 2)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (6, 1, N'messages', N'/Messages', 1, N'icon comments', 2)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (9, 2, N'properties', N'/Property', 1, N'icon building outline', 10)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (10, 1, N'properties', N'/Property', 1, N'icon building outline', 10)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (11, 2, N'booking', N'/Booking', 1, N'icon book', 3)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (12, 1, N'booking', N'/Booking', 1, N'icon book', 3)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (13, 1, N'income', N'/Income', 1, N'icon dollar', 6)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (14, 2, N'income', N'/Income', 1, N'icon dollar', 6)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (1013, 2, N'expense', N'/Expense', 1, N'icon money', 5)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (1014, 1, N'expense', N'/Expense', 1, N'icon money', 5)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2013, 1, N'cashflow', N'/Cashflow', 1, N'icon university', 7)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2014, 2, N'cashflow', N'/Cashflow', 1, N'icon university', 7)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2015, 1, N'worker', N'/Home/Worker', 1, N'icon child', 9)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2016, 2, N'worker', N'/Home/Worker', 1, N'icon child', 9)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2019, 1, N'calendar', N'/Calendar', 1, N'icon calendar', 4)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2020, 1, N'import', N'/Import', 1, N'icon download', 11)
INSERT [dbo].[tbRolePage] ([Id], [RoleId], [PageName], [PageUrl], [Status], [IconPath], [PageOrder]) VALUES (2022, 1, N'reports', N'/Report', 1, N'icon line chart', 12)
SET IDENTITY_INSERT [dbo].[tbRolePage] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [FirstName], [LastName], [UserName], [Password], [CompanyId], [TimeZone], [ConfirmEmail], [ValidStatus], [Status], [CreatedDate]) VALUES (7, N'kendrick', N'Khoe', N'kkhoe@hotmail.com', N'hODyWLeN/zaPQmhMfFuLcQ==', 1, N'Ontario', 0, 1, 1, CAST(N'2017-04-13T00:54:17.147' AS DateTime))
INSERT [dbo].[User] ([Id], [FirstName], [LastName], [UserName], [Password], [CompanyId], [TimeZone], [ConfirmEmail], [ValidStatus], [Status], [CreatedDate]) VALUES (1026, N'John Frederick', N'Carino', N'depwaswho94@gmail.com', N'4eDNwvjzNkE=', 1026, N'Ontario', 0, 1, 1, CAST(N'2017-08-31T10:54:20.567' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[WorkerJob] ON 

INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (28, 1020, 2, 2, CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (29, 1020, 1, 1, CAST(50.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (33, 1019, 1, 2, CAST(500.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (34, 1019, 2, 1, CAST(60.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (35, 1019, 4, 2, CAST(900.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (36, 1, 1, 1, CAST(50.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (37, 2, 2, 1, CAST(80.00 AS Decimal(18, 2)))
INSERT [dbo].[WorkerJob] ([Id], [WorkerId], [JobTypeId], [PaymentType], [PaymentRate]) VALUES (38, 3, 1, 2, CAST(60.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[WorkerJob] OFF
SET IDENTITY_INSERT [dbo].[WorkerJobType] ON 

INSERT [dbo].[WorkerJobType] ([Id], [Name]) VALUES (1, N'Cleaner')
INSERT [dbo].[WorkerJobType] ([Id], [Name]) VALUES (2, N'Plumber')
INSERT [dbo].[WorkerJobType] ([Id], [Name]) VALUES (3, N'Carpenter')
INSERT [dbo].[WorkerJobType] ([Id], [Name]) VALUES (4, N'Handyman')
SET IDENTITY_INSERT [dbo].[WorkerJobType] OFF
ALTER TABLE [dbo].[Inquiry] ADD  CONSTRAINT [DF_Inquiry_HostingAccountId]  DEFAULT ((0)) FOR [HostId]
GO
ALTER TABLE [dbo].[Note] ADD  CONSTRAINT [DF_Note_ReservationId]  DEFAULT ((0)) FOR [ReservationId]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_ListingId]  DEFAULT ((0)) FOR [ListingId]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_UnitCount]  DEFAULT ((0)) FOR [UnitCount]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_MinStay]  DEFAULT ((0)) FOR [MinStay]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_Reviews]  DEFAULT ((0)) FOR [Reviews]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_Sleeps]  DEFAULT ((0)) FOR [Sleeps]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_Bedrooms]  DEFAULT ((0)) FOR [Bedrooms]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_Bathrooms]  DEFAULT ((0)) FOR [Bathrooms]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_Type]  DEFAULT ((0)) FOR [Type]
GO
ALTER TABLE [dbo].[Property] ADD  CONSTRAINT [DF_Property_MonthlyExpense]  DEFAULT ((0)) FOR [MonthlyExpense]
GO
