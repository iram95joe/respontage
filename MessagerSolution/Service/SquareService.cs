﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using Core.Database.Context;
using MessagerSolution.Models;
using SignRequest.Client;
using Square;
using Square.Apis;
using Square.Models;

namespace MessagerSolution.Service
{
    public class SquareService
    {
        private SquareClient client;
        Square.Environment environment = Square.Environment.Sandbox;
        public SquareService()
        {
          ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
        }
        public void CreateSubscription(UserRoleModel user)
        {

            using (var db = new ApplicationDbContext())
            {
                var subscriptionPlan = db.SubscriptionPlans.Where(x => x.Id == user.SubscriptionPlanId).FirstOrDefault();

                client = new SquareClient.Builder()
                    .Environment(environment)
                    .AccessToken(Core.Helper.GlobalVariables.SquareAccessToken)
                    .Build();
                var customerId = "";
                var planId = "";
                var cardId = "";
                Subscription subscription = new Subscription();
                //Create customer
                var customer = new CreateCustomerRequest.Builder()
                 .IdempotencyKey(Guid.NewGuid().ToString())
                 .GivenName(user.FirstName)
                 .FamilyName(user.LastName)
                 .EmailAddress(user.UserName)
                 .Build();

                try
                {
                    var result = client.CustomersApi.CreateCustomerAsync(body: customer);
                    customerId = result.Result.Customer.Id;

                }
                catch (ApiException e)
                {
                }
                //Create Card
                //var billingAddress = new Address.Builder()
                //.AddressLine1("500 Electric Ave")
                //.AddressLine2("Suite 600")
                //.Locality("New York")
                //.AdministrativeDistrictLevel1("NY")
                //.PostalCode("10003")
                //.Country("US")
                //.Build();

                var card = new CreateCustomerCardRequest.Builder(cardNonce: user.CreditCardNonce)
                  //.BillingAddress(billingAddress)
                  .CardholderName(user.FirstName + " " + user.LastName)
                  .Build();

                try
                {
                    var result = client.CustomersApi.CreateCustomerCardAsync(customerId: customerId, body: card);
                    cardId = result.Result.Card.Id;
                }
                catch (ApiException e)
                {
                    Console.WriteLine("Failed to make the request");
                    Console.WriteLine($"Exception: {e.Message}");
                }
                //Plan
                var recurringPriceMoney = new Money.Builder()
                  .Amount(Convert.ToInt64(subscriptionPlan.Amount.ToString().Replace(".", "")))
                  .Currency("USD")
                  .Build();

                var subscriptionPhase1 = new SubscriptionPhase.Builder(cadence: subscriptionPlan.Type, recurringPriceMoney: recurringPriceMoney)
                  .Build();

                var phases = new List<SubscriptionPhase>();
                phases.Add(subscriptionPhase1);

                var subscriptionPlanData = new CatalogSubscriptionPlan.Builder()
                  .Name("Respontage Subscription")
                  .Phases(phases)
                  .Build();

                var mObject = new CatalogObject.Builder(type: "SUBSCRIPTION_PLAN", id: "#plan")
                  .SubscriptionPlanData(subscriptionPlanData)
                  .Build();

                var bodyPlan = new UpsertCatalogObjectRequest.Builder(idempotencyKey: Guid.NewGuid().ToString(), mObject: mObject).Build();

                try
                {
                    var result = client.CatalogApi.UpsertCatalogObjectAsync(body: bodyPlan);
                    planId = result.Result.CatalogObject.Id;
                }
                catch (ApiException e)
                {
                    Console.WriteLine("Failed to make the request");
                    Console.WriteLine($"Exception: {e.Message}");
                }

                //Subscription
                var body = new CreateSubscriptionRequest.Builder(
                    idempotencyKey: Guid.NewGuid().ToString(),
                    locationId: "LAFFXFSV5ZVGT",
                    planId: planId,
                    customerId: customerId)
                  .StartDate(DateTime.Now.ToString("yyyy-MM-dd"))
                  //.PriceOverrideMoney(priceOverrideMoney)
                  .CardId(cardId)
                  .Timezone("America/Los_Angeles")
                  .Build();

                try
                {
                    var result = client.SubscriptionsApi.CreateSubscriptionAsync(body: body);
                    subscription = result.Result.Subscription;
                }
                catch (ApiException e)
                {
                    Console.WriteLine("Failed to make the request");
                    Console.WriteLine($"Exception: {e.Message}");
                }
                db.UserSubscriptions.Add(new Core.Database.Entity.UserSubscription() { CardId = cardId, CustomerId = customerId, PlanId = planId, SubscriptiondId = subscription.Id, UserId = user.Id, StartDate = subscription.StartDate.ToDateTime() });
                db.SaveChanges();
            }

        }

        public void CreateSubscription(string cardnonce, int invoiceId)
        {
            // Build base client    
            client = new SquareClient.Builder()
                .Environment(environment)
                .AccessToken(Core.Helper.PaymentMethod.ConfigurationKeys.ConfigurationKeyByInvoice(invoiceId).SquareAccessToken)
                .Build();
            var customerId = "";
            var planId = "";
            var cardId = "";
            //Create customer
            var customer = new CreateCustomerRequest.Builder()
             .IdempotencyKey(Guid.NewGuid().ToString())
             .GivenName("Joe Mari")
             .FamilyName("De Mesa")
             .EmailAddress("demesa13@gmail.com")
             .Build();

            try
            {
                var result = client.CustomersApi.CreateCustomerAsync(body: customer);
                customerId = result.Result.Customer.Id;

            }
            catch (ApiException e)
            {
            }
            //Create Card
            var billingAddress = new Address.Builder()
              .AddressLine1("500 Electric Ave")
              .AddressLine2("Suite 600")
              .Locality("New York")
              .AdministrativeDistrictLevel1("NY")
              .PostalCode("10003")
              .Country("US")
              .Build();

            var card = new CreateCustomerCardRequest.Builder(cardNonce: cardnonce)
              .BillingAddress(billingAddress)
              .CardholderName("Joe Mari de Mesa")
              .Build();

            try
            {
                var result = client.CustomersApi.CreateCustomerCardAsync(customerId: customerId, body: card);
                cardId = result.Result.Card.Id;
            }
            catch (ApiException e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }
            //Plan
            var recurringPriceMoney = new Money.Builder()
              .Amount(100L)
              .Currency("USD")
              .Build();



            var subscriptionPhase1 = new SubscriptionPhase.Builder(cadence: "MONTHLY", recurringPriceMoney: recurringPriceMoney)
              .Build();

            var phases = new List<SubscriptionPhase>();
            phases.Add(subscriptionPhase1);

            var subscriptionPlanData = new CatalogSubscriptionPlan.Builder()
              .Name("Respontage Subscription")
              .Phases(phases)
              .Build();

            var mObject = new CatalogObject.Builder(type: "SUBSCRIPTION_PLAN", id: "#plan")
              .SubscriptionPlanData(subscriptionPlanData)
              .Build();

            var bodyPlan = new UpsertCatalogObjectRequest.Builder(idempotencyKey: Guid.NewGuid().ToString(), mObject: mObject).Build();

            try
            {
                var result = client.CatalogApi.UpsertCatalogObjectAsync(body: bodyPlan);
                planId = result.Result.CatalogObject.Id;
            }
            catch (ApiException e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }

            //Subscription
            var body = new CreateSubscriptionRequest.Builder(
                idempotencyKey: Guid.NewGuid().ToString(),
                locationId: "LAFFXFSV5ZVGT",
                planId: planId,
                customerId: customerId)
              .StartDate(DateTime.Now.ToString("yyyy-MM-dd"))
              //.PriceOverrideMoney(priceOverrideMoney)
              .CardId(cardId)
              .Timezone("America/Los_Angeles")
              .Build();

            try
            {
                var result = client.SubscriptionsApi.CreateSubscriptionAsync(body: body);
                var sub = result.Result.Subscription;
            }
            catch (ApiException e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }
        }

        public List<string> ListOfDispute(int accountId)
        {
            try
            {
                // Build base client
                client = new SquareClient.Builder()
                    .Environment(environment)
                    .AccessToken(Core.Helper.PaymentMethod.ConfigurationKeys.PaymentAccount(accountId).SquareAccessToken)
                    .Build();
                var result = client.DisputesApi.ListDisputesAsync();
                var disputes = result.Result.Disputes;
                if (disputes != null)
                {
                    return disputes.Select(x => x.DisputedPayment.PaymentId).ToList();
                }

            }
            catch (ApiException e)
            {
                Console.WriteLine($"Exception: {e.Message}");
                return new List<string>();
            }
            return new List<string>();
        }

        public void CancelPayment(int invoiceId, string paymentId)
        {
            try
            {

                // Build base client
                client = new SquareClient.Builder()
                    .Environment(environment)
                    .AccessToken(Core.Helper.PaymentMethod.ConfigurationKeys.ConfigurationKeyByInvoice(invoiceId).SquareAccessToken)
                    .Build();
                var result = client.PaymentsApi.CancelPaymentAsync(paymentId: paymentId);
            }
            catch (ApiException e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }
        }
        public CreatePaymentResponse ChargeLoad(string nonce, long amountToPay, string currency)
        {


            // Build base client
            client = new SquareClient.Builder()
                .Environment(environment)
                .AccessToken(Core.Helper.GlobalVariables.SquareAccessToken)
                .Build();
            IPaymentsApi PaymentsApi = client.PaymentsApi;
            // Every payment you process with the SDK must have a unique idempotency key.
            // If you're unsure whether a particular payment succeeded, you can reattempt
            // it with the same idempotency key without worrying about double charging
            // the buyer.
            string uuid = Guid.NewGuid().ToString();

            // Monetary amounts are specified in the smallest unit of the applicable currency.
            // This amount is in cents. It's also hard-coded for $1.00,
            // which isn't very useful.
            //Money amount = new Money(amountToPay, (Money.CurrencyEnum)Enum.Parse(typeof(Money.CurrencyEnum), currency));
            Money amount = new Money.Builder()
             .Amount(amountToPay)
             .Currency("USD")
             .Build();
            // To learn more about splitting transactions with additional recipients,
            // see the Transactions API documentation on our [developer site]
            // (https://docs.connect.squareup.com/payments/transactions/overview#mpt-overview).
            //ChargeRequest body = new ChargeRequest(AmountMoney: amount, IdempotencyKey: uuid, CardNonce: nonce);
            CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest.Builder(nonce, uuid, amount)
             .Note("From Respontage")
             .Build();
            try
            {
                CreatePaymentResponse response = PaymentsApi.CreatePayment(createPaymentRequest);
                return response;
                //return transactionsApi.Charge(Core.API.Booking.com.Hosts.ConfigurationKeys.SquareLocationId, body);
            }
            catch (ApiException e)
            {
                return null;
            }
        }

        public CreatePaymentResponse ChargeCard(string nonce, long amountToPay, string currency, int invoiceId)
        {


            // Build base client
            client = new SquareClient.Builder()
                .Environment(environment)
                .AccessToken(Core.Helper.PaymentMethod.ConfigurationKeys.ConfigurationKeyByInvoice(invoiceId).SquareAccessToken)
                .Build();
            IPaymentsApi PaymentsApi = client.PaymentsApi;
            // Every payment you process with the SDK must have a unique idempotency key.
            // If you're unsure whether a particular payment succeeded, you can reattempt
            // it with the same idempotency key without worrying about double charging
            // the buyer.
            string uuid = Guid.NewGuid().ToString();

            // Monetary amounts are specified in the smallest unit of the applicable currency.
            // This amount is in cents. It's also hard-coded for $1.00,
            // which isn't very useful.
            //Money amount = new Money(amountToPay, (Money.CurrencyEnum)Enum.Parse(typeof(Money.CurrencyEnum), currency));
            Money amount = new Money.Builder()
             .Amount(amountToPay)
             .Currency("USD")
             .Build();
            // To learn more about splitting transactions with additional recipients,
            // see the Transactions API documentation on our [developer site]
            // (https://docs.connect.squareup.com/payments/transactions/overview#mpt-overview).
            //ChargeRequest body = new ChargeRequest(AmountMoney: amount, IdempotencyKey: uuid, CardNonce: nonce);
            CreatePaymentRequest createPaymentRequest = new CreatePaymentRequest.Builder(nonce, uuid, amount)
             .Note("From Respontage")
             .Build();
            try
            {
                CreatePaymentResponse response = PaymentsApi.CreatePayment(createPaymentRequest);
                return response;
                //return transactionsApi.Charge(Core.API.Booking.com.Hosts.ConfigurationKeys.SquareLocationId, body);
            }
            catch (ApiException e)
            {
                return null;
            }
        }

        public RefundPaymentResponse Refund(long amountRefund, string paymentId)
        {
            IRefundsApi refund = client.RefundsApi;
            Money amount = new Money.Builder()
           .Amount(amountRefund)
           .Currency("USD")
           .Build();
            string uuid = Guid.NewGuid().ToString();
            RefundPaymentRequest createPaymentRequest = new RefundPaymentRequest.Builder(uuid, amount, paymentId)
            .Build();
            try
            {
                var response = refund.RefundPayment(createPaymentRequest);
                return response;
            }
            catch (ApiException e)
            {
                return null;
            }
        }

        //public ChargeResponse ChargeCard(string nonce, long amountToPay, string currency)
        //{
        //    TransactionsApi transactionsApi = new TransactionsApi();
        //    // Every payment you process with the SDK must have a unique idempotency key.
        //    // If you're unsure whether a particular payment succeeded, you can reattempt
        //    // it with the same idempotency key without worrying about double charging
        //    // the buyer.
        //    string uuid = Guid.NewGuid().ToString();

        //    // Monetary amounts are specified in the smallest unit of the applicable currency.
        //    // This amount is in cents. It's also hard-coded for $1.00,
        //    // which isn't very useful.
        //    Money amount = new Money(amountToPay, (Money.CurrencyEnum)Enum.Parse(typeof(Money.CurrencyEnum), currency));

        //    // To learn more about splitting transactions with additional recipients,
        //    // see the Transactions API documentation on our [developer site]
        //    // (https://docs.connect.squareup.com/payments/transactions/overview#mpt-overview).
        //    ChargeRequest body = new ChargeRequest(AmountMoney: amount, IdempotencyKey: uuid, CardNonce: nonce);

        //    try
        //    {
        //        return transactionsApi.Charge(Core.API.Booking.com.Hosts.ConfigurationKeys.SquareLocationId, body);
        //    }
        //    catch (ApiException e)
        //    {
        //        return null;
        //    }
        //}
    }
}