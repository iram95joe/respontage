﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booking.com.Models;
using PayPal.Api;

namespace MessagerSolution.Service
{
    public class PayPalService
    {
        private Payment _payment;

        public PayPalService()
        {
            //var config = GetConfig();
        }
        #region Invoice
        public Dictionary<string, string> GetConfig(int invoiceId)
        {
            var configurationKeys = Core.Helper.PaymentMethod.ConfigurationKeys.ConfigurationKeyByInvoice(invoiceId);
            var properties = ConfigManager.Instance.GetProperties();

            properties.Add("clientId", configurationKeys.PaypalClientId);
            properties.Add("clientSecret", configurationKeys.PaypalClientSecret);

            return properties;
        }

        private string GetAccessToken(int invoiceId)
        {
            var configurationKeys = Core.Helper.PaymentMethod.ConfigurationKeys.ConfigurationKeyByInvoice(invoiceId);

            var accessToken = new OAuthTokenCredential
                (configurationKeys.PaypalClientId, configurationKeys.PaypalClientSecret, GetConfig(invoiceId))
                .GetAccessToken();

            return accessToken;
        }

        public APIContext GetApiContext(int invoiceId)
        {
            var apiContext = new APIContext(GetAccessToken(invoiceId)) { Config = GetConfig(invoiceId) };
            return apiContext;
        }

        public Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution() { payer_id = payerId };
            _payment = new Payment() { id = paymentId };
            return _payment.Execute(apiContext, paymentExecution);
        }
        public List<string> ListOfDispute(int accountId)
        {
            try
            {

            }
            catch (Exception e)
            {

            }
            return new List<string>();
        }
        public Payment CreateFullPayment(APIContext apiContext, string redirectUrl, Models.Invoice.InvoicePaymentRequest data)
        {
            var itemList = new ItemList() { items = new List<Item>() };

            var paypalitem = new Item
            {
                sku = data.InvoiceId.ToString(),
                name = "Payment for Invoice No. " + data.InvoiceId,
                quantity = "1",
                price = (Convert.ToDouble(data.TotalAmount)).ToString("F"),
                currency = "USD"
            };
            itemList.items.Add(paypalitem);

            var payer = new Payer() { payment_method = "paypal" };

            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl,
                return_url = redirectUrl
            };

            var subtotal = (Convert.ToDouble(data.TotalAmount)).ToString("F");

            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                shipping_discount = "0",
                subtotal = subtotal
            };

            var amount = new Amount()
            {
                currency = "USD",
                total = (Convert.ToDouble(data.TotalAmount)).ToString("F"),
                details = details
            };

            var transactionList = new List<Transaction>
            {
                new Transaction()
                {
                    description = "Transaction description.",
                    invoice_number = Guid.NewGuid().ToString(),
                    amount = amount,
                    item_list = itemList
                }
            };


            _payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            return _payment.Create(apiContext);
        }
        #endregion
        //public Dictionary<string, string> GetConfig()
        //{
        //    var configurationKeys = Core.API.Booking.com.Hosts.ConfigurationKeys;
        //    var properties = ConfigManager.Instance.GetProperties();

        //    properties.Add("clientId", configurationKeys.PaypalClientId);
        //    properties.Add("clientSecret", configurationKeys.PaypalClientSecret);

        //    return properties;
        //}

        //private string GetAccessToken()
        //{
        //    var configurationKeys = Core.API.Booking.com.Hosts.ConfigurationKeys;

        //    var accessToken = new OAuthTokenCredential
        //        (configurationKeys.PaypalClientId, configurationKeys.PaypalClientSecret, GetConfig())
        //        .GetAccessToken();

        //    return accessToken;
        //}

        //public APIContext GetApiContext()
        //{
        //    var apiContext = new APIContext(GetAccessToken()) { Config = GetConfig() };
        //    return apiContext;
        //}

        //public Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        //{
        //    var paymentExecution = new PaymentExecution() { payer_id = payerId };
        //    _payment = new Payment() { id = paymentId };
        //    return _payment.Execute(apiContext, paymentExecution);
        //}

        //public Payment CreateFullPayment(APIContext apiContext, string redirectUrl, SendPaymentDetails reservation)
        //{
        //    var itemList = new ItemList() { items = new List<Item>() };

        //    var paypalitem = new Item
        //    {
        //        sku = reservation.ConfirmationCode,
        //        name = "Kendric's POC Reservation for " + reservation.Customer,
        //        quantity = "1",
        //        price = (Convert.ToDouble(reservation.TotalAmount)).ToString("F"),
        //        currency = reservation.Currency
        //    };
        //    itemList.items.Add(paypalitem);

        //    var payer = new Payer() { payment_method = "paypal" };

        //    var redirUrls = new RedirectUrls()
        //    {
        //        cancel_url = redirectUrl,
        //        return_url = redirectUrl
        //    };

        //    var subtotal = (Convert.ToDouble(reservation.TotalAmount)).ToString("F");

        //    var details = new Details()
        //    {
        //        tax = "0",
        //        shipping = "0",
        //        shipping_discount = "0",
        //        subtotal = subtotal
        //    };

        //    var amount = new Amount()
        //    {
        //        currency = reservation.Currency,
        //        total = (Convert.ToDouble(reservation.TotalAmount)).ToString("F"),
        //        details = details
        //    };

        //    var transactionList = new List<Transaction>
        //    {
        //        new Transaction()
        //        {
        //            description = "Transaction description.",
        //            invoice_number = Guid.NewGuid().ToString(),
        //            amount = amount,
        //            item_list = itemList
        //        }
        //    };


        //    _payment = new Payment()
        //    {
        //        intent = "sale",
        //        payer = payer,
        //        transactions = transactionList,
        //        redirect_urls = redirUrls
        //    };

        //    return _payment.Create(apiContext);
        //}
    }
}