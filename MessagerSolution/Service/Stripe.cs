﻿using Core.Database.Entity;
using Stripe;
using Stripe.Issuing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Service
{
    public class Stripe
    {
        public StripeCharge CreatePayment(string stripeToken, string stripeEmail, Invoice invoice)
        {
            var customers = new StripeCustomerService();
            var charges = new StripeChargeService();
            StripeConfiguration.SetApiKey(Core.Helper.PaymentMethod.ConfigurationKeys.ConfigurationKeyByInvoice(invoice.Id).StripeSecretKey);

            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });

            //Minimum amount is 0.50 cent
            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = Convert.ToInt32(invoice.TotalAmount.ToString().Replace(".", "")),
                Description = "Payment for Invoice No. " + invoice,
                Currency = "USD",
                CustomerId = customer.Id
            });
            return charge;
        }

        public List<string> ListOfDispute(int accountId)
        {
            try
            {
                StripeConfiguration.SetApiKey(Core.Helper.PaymentMethod.ConfigurationKeys.PaymentAccount(accountId).StripeSecretKey);

                var options = new DisputeListOptions
                {
                    Limit = 3,
                };
                var service = new DisputeService();
                StripeList<Dispute> disputes = service.List(
                  options
                );
                if (disputes != null)
                {
                    return disputes.Select(x => x.TransactionId).ToList();
                }
            }
            catch (Exception e)
            {

            }
            return new List<string>();
        }
    }
}