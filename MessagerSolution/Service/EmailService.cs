﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace MessagerSolution.Service
{
    public class EmailService
    {
        public bool SendEmail(string subject, string body, string emailTo, string attachmentFilename = null)
        {
            try
            {
                //BackgroundJob.Enqueue(() => ScheduleSendingEmail(subject, body, emailTo, attachmentFilename));
                ScheduleSendingEmail(subject, body, emailTo, attachmentFilename);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static void ScheduleSendingEmail(string subject, string body, string emailTo, string attachmentFilename)
        {
            using (var mail = new MailMessage())
            {
                mail.From = new MailAddress("noahsleisureparkresort@gmail.com", "Kendric's POC");
                mail.To.Add(emailTo);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                if (attachmentFilename != null)
                {
                    Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                    disposition.FileName = Path.GetFileName(attachmentFilename);
                    disposition.Size = new FileInfo(attachmentFilename).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    mail.Attachments.Add(attachment);
                }
                using (SmtpClient client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential("demesa13@gmail.com","magalingako01");
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    client.Send(mail);
                }


            }
        }
    }
}