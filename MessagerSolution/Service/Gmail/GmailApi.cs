﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using MessagerSolution.Models;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using Message = Google.Apis.Gmail.v1.Data.Message;
using System.Net.Mail;
using AE.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MessagerSolution.Service.Gmail
{
    public class GmailApi
    {
        static string[] Scopes = { GmailService.Scope.MailGoogleCom };
        static string ApplicationName = "Respontage";
        public static void Send(GmailService service, string Message, string sendTo, string subject, List<string> Attachments = null)
        {
            var sender = GetEmailAddress(service);
            var msg = new AE.Net.Mail.MailMessage
            {
                Subject = subject,
                Body = Message,
                From = new MailAddress(sender),
                ContentType = "text/html"
            };
            if (Attachments != null)
            {
                foreach (var file in Attachments)
                {

                    var f = System.Web.HttpContext.Current.Server.MapPath("~" + file);

                    var attachment = new AE.Net.Mail.Attachment(File.ReadAllBytes(f), MediaTypeNames.Application.Octet, file.Split('/').Last(), true);
                    msg.Attachments.Add(attachment);
                }
            }
            msg.To.Add(new MailAddress(sendTo));
            msg.ReplyTo.Add(msg.From); // Bounces without this!!
            var msgStr = new StringWriter();
            msg.Save(msgStr);
            // Context is a separate bit of code that provides OAuth context;
            // your construction of GmailService will be different from mine.
            MimeKit.MimeMessage mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(msg);
            var result = service.Users.Messages.Send(new Message
            {
                Raw = Base64UrlEncode(mimeMessage.ToString())
            }, "me").Execute();
            Console.WriteLine("Message ID {0} sent.", result.Id);
        }
        private static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
              .Replace('+', '-')
              .Replace('/', '_')
              .Replace("=", "");
        }
        public static GmailService RespontageGmailLogin()
        {

            UserCredential credential;
            string path = System.Web.HttpContext.Current.Server.MapPath("~/Service/Gmail/credentials.json");

            if (!File.Exists(path))
            {
                FileStream f = System.IO.File.Create(path);
                f.Close();
            }
            using (var stream =
                new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = System.Web.HttpContext.Current.Server.MapPath("~/Service/Gmail/respontage");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            return service;
        }
        public static async Task<bool> Login(int companyId)
        {
            var service = OAuth();
            if (service != null)
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    using (var db = new ApplicationDbContext())
                    {
                        try
                        {
                            var hostEmail = GetEmailAddress(service);
                            var temp = db.EmailAccounts.Where(x => x.Email == hostEmail && x.CompanyId == companyId).FirstOrDefault();
                            if (temp == null)
                            {
                                db.EmailAccounts.Add(new EmailAccount() { Email = hostEmail, CompanyId = companyId, Password = "" });
                                db.SaveChanges();
                            }
                            var renterEmails = (from renter in db.Renters join email in db.RenterEmails on renter.Id equals email.RenterId join reservation in db.Inquiries on renter.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where property.CompanyId == companyId select new { email.Email, renter }).ToList();
                            var messages = GetAllEmails(service, hostEmail, renterEmails.Select(x => x.Email).ToList());
                            foreach (var email in renterEmails)
                            {
                                var Emails = messages.Where(x => x.From == email.Email || x.To == email.Email).ToList();
                                foreach (var message in Emails)
                                {
                                    SaveEmail(email.renter, message);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }
                });
                return true;
            }
            return false;

        }
        static Renter GetRenter(string email)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var renter = (from r in db.Renters join reservation in db.Inquiries on r.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where property.CompanyId == SessionHandler.CompanyId && !property.IsParentProperty && property.SiteType == 0 && r.Email == email select r).FirstOrDefault();
                    return renter;
                }
            }
            catch
            {
                return null;
            }

        }
        static void SaveEmail(Renter renter, GmailModel gmail)
        {
            using (var db = new ApplicationDbContext())
            {
                var thread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && renter.Id == x.RenterId && x.IsRenter).FirstOrDefault();
                CommunicationThread workerThread;
                if (thread == null)
                {
                    workerThread = new CommunicationThread();
                    workerThread.RenterId = renter.Id;
                    workerThread.ThreadId = PasswordGenerator.Generate(20);
                    workerThread.CompanyId = SessionHandler.CompanyId;
                    db.CommunicationThreads.Add(workerThread);
                    db.SaveChanges();
                }
                else
                {
                    workerThread = thread;
                    workerThread.IsEnd = false;
                    db.Entry(workerThread).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                var temp = db.CommunicationEmails.Where(x => x.From == gmail.From && x.To == gmail.To && x.Message == gmail.Body && x.CreatedDate == gmail.MailDateTime && x.ThreadId == thread.ThreadId).FirstOrDefault();
                if (temp == null)
                {
                    var Email = new CommunicationEmail()
                    {
                        From = gmail.From,
                        To = gmail.To,
                        IsMyMessage = (renter.Email == gmail.To ? true : false),
                        Message = gmail.Body,
                        ThreadId = workerThread.ThreadId,
                        CreatedDate = gmail.MailDateTime
                    };
                    db.CommunicationEmails.Add(Email);
                    db.SaveChanges();
                    if (gmail.Attachments.Count > 0)
                    {
                        foreach (var attachment in gmail.Attachments)
                        {
                            db.EmailAttachments.Add(new CommunicationEmailAttachment()
                            {
                                CommunicationEmailId = Email.Id,
                                FileUrl = attachment
                            });
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    if (gmail.Attachments.Count > 0)
                    {
                        foreach (var attachment in gmail.Attachments)
                        {
                            System.IO.File.Delete(System.AppDomain.CurrentDomain.BaseDirectory + attachment);

                        }
                    }
                }

            }
        }
        public static GmailService OAuth()
        {
            UserCredential credential;
            string path = System.Web.HttpContext.Current.Server.MapPath("~/Service/Gmail/credentials.json");

            if (!File.Exists(path))
            {
                FileStream f = System.IO.File.Create(path);
                f.Close();
            }
            using (var stream =
                new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/Service/Gmail/credentials/Company{0}", SessionHandler.CompanyId));
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            return service;
        }
        public static string GetEmailAddress(GmailService service)
        {
            var user = service.Users.GetProfile("me").Execute();
            var userGmailEmail = user.EmailAddress;
            return userGmailEmail;
        }

        public static string MsgNestedParts(IList<MessagePart> Parts)
        {
            string str = string.Empty;
            if (Parts.Count() < 0)
            {
                return string.Empty;
            }
            else
            {
                IList<MessagePart> PlainTestMail = Parts.Where(x => x.MimeType == "text/plain").ToList();
                IList<MessagePart> AttachmentMail = Parts.Where(x => x.MimeType == "multipart/alternative").ToList();

                if (PlainTestMail.Count() > 0)
                {
                    foreach (MessagePart EachPart in PlainTestMail)
                    {
                        if (EachPart.Parts == null)
                        {
                            if (EachPart.Body != null && EachPart.Body.Data != null)
                            {
                                str += EachPart.Body.Data;
                            }
                        }
                        else
                        {
                            return MsgNestedParts(EachPart.Parts);
                        }
                    }
                }
                if (AttachmentMail.Count() > 0)
                {
                    foreach (MessagePart EachPart in AttachmentMail)
                    {
                        if (EachPart.Parts == null)
                        {
                            if (EachPart.Body != null && EachPart.Body.Data != null)
                            {
                                str += EachPart.Body.Data;
                            }
                        }
                        else
                        {
                            return MsgNestedParts(EachPart.Parts);
                        }
                    }
                }
                return str;
            }
        }
        public static List<string> GetAttachments(GmailService service, string userId, string messageId)
        {
            try
            {
                List<string> FileName = new List<string>();
                Message message = service.Users.Messages.Get(userId, messageId).Execute();
                IList<MessagePart> parts = message.Payload.Parts;
                var path = System.AppDomain.CurrentDomain.BaseDirectory + "/Images/EmailAttachment/";
                System.IO.Directory.CreateDirectory(path);
                foreach (MessagePart part in parts)
                {
                    if (!String.IsNullOrEmpty(part.Filename))
                    {
                        string attId = part.Body.AttachmentId;
                        MessagePartBody attachPart = service.Users.Messages.Attachments.Get(userId, messageId, attId).Execute();

                        byte[] data = Base64ToByte(attachPart.Data);
                        File.WriteAllBytes(Path.Combine(path, part.Filename), data);
                        FileName.Add("/Images/EmailAttachment/" + part.Filename);

                    }
                }
                return FileName;
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
                return null;
            }
        }
        public static string Base64Decode(string Base64Test)
        {
            string EncodTxt = string.Empty;
            //STEP-1: Replace all special Character of Base64Test
            EncodTxt = Base64Test.Replace("-", "+");
            EncodTxt = EncodTxt.Replace("_", "/");
            EncodTxt = EncodTxt.Replace(" ", "+");
            EncodTxt = EncodTxt.Replace("=", "+");

            //STEP-2: Fixed invalid length of Base64Test
            if (EncodTxt.Length % 4 > 0) { EncodTxt += new string('=', 4 - EncodTxt.Length % 4); }
            else if (EncodTxt.Length % 4 == 0)
            {
                EncodTxt = EncodTxt.Substring(0, EncodTxt.Length - 1);
                if (EncodTxt.Length % 4 > 0) { EncodTxt += new string('+', 4 - EncodTxt.Length % 4); }
            }

            //STEP-3: Convert to Byte array
            byte[] ByteArray = Convert.FromBase64String(EncodTxt);

            //STEP-4: Encoding to UTF8 Format
            return Encoding.UTF8.GetString(ByteArray);
        }
        public static byte[] Base64ToByte(string Base64Test)
        {
            string EncodTxt = string.Empty;
            //STEP-1: Replace all special Character of Base64Test
            EncodTxt = Base64Test.Replace("-", "+");
            EncodTxt = EncodTxt.Replace("_", "/");
            EncodTxt = EncodTxt.Replace(" ", "+");
            EncodTxt = EncodTxt.Replace("=", "+");

            //STEP-2: Fixed invalid length of Base64Test
            if (EncodTxt.Length % 4 > 0) { EncodTxt += new string('=', 4 - EncodTxt.Length % 4); }
            else if (EncodTxt.Length % 4 == 0)
            {
                EncodTxt = EncodTxt.Substring(0, EncodTxt.Length - 1);
                if (EncodTxt.Length % 4 > 0) { EncodTxt += new string('+', 4 - EncodTxt.Length % 4); }
            }

            //STEP-3: Convert to Byte array
            return Convert.FromBase64String(EncodTxt);
        }

        public static List<GmailModel> GetAllEmails(GmailService service, string HostEmailAddress, List<string> RenterEmails)
        {
            List<GmailModel> EmailList = new List<GmailModel>();
            List<string> labels = new List<string>() { "SENT", "INBOX" };
            UsersResource.MessagesResource.ListRequest ListRequest = service.Users.Messages.List(HostEmailAddress);
            foreach (var label in labels)
            {
                ListRequest.LabelIds = label;
                ListRequest.IncludeSpamTrash = false;
                //ListRequest.Q = "is:unread"; //ONLY FOR UNDREAD EMAIL'S...
                //GET ALL EMAILS
                ListMessagesResponse ListResponse = ListRequest.Execute();
                if (ListResponse != null && ListResponse.Messages != null)
                {
                    string FromAddress = string.Empty;
                    string Date = string.Empty;
                    string Subject = string.Empty;
                    string MailBody = string.Empty;
                    string ReadableText = string.Empty;
                    string To = string.Empty;
                    //LOOP THROUGH EACH EMAIL AND GET WHAT FIELDS I WANT
                    foreach (Message Msg in ListResponse.Messages)
                    {
                        try
                        {
                            MsgMarkAsRead(service, HostEmailAddress, Msg.Id);
                            UsersResource.MessagesResource.GetRequest Message = service.Users.Messages.Get(HostEmailAddress, Msg.Id);
                            //MAKE ANOTHER REQUEST FOR THAT EMAIL ID...
                            Message MsgContent = Message.Execute();
                            if (MsgContent != null)
                            {

                                //LOOP THROUGH THE HEADERS AND GET THE FIELDS WE NEED (SUBJECT, MAIL)
                                foreach (var MessageParts in MsgContent.Payload.Headers)
                                {
                                    if (MessageParts.Name == "From")
                                    {
                                        var start = MessageParts.Value.IndexOf("<") + 1;
                                        if (start > 1)
                                        {
                                            FromAddress = MessageParts.Value.Substring(start, MessageParts.Value.IndexOf(">") - start);
                                        }
                                        else
                                            FromAddress = MessageParts.Value;
                                    }
                                    else if (MessageParts.Name == "To")
                                    {
                                        var start = MessageParts.Value.IndexOf("<") + 1;
                                        if (start > 1)
                                        {
                                            To = MessageParts.Value.Substring(start, MessageParts.Value.IndexOf(">") - start);
                                        }
                                        else
                                            To = MessageParts.Value;

                                    }
                                    else if (MessageParts.Name == "Date")
                                    {
                                        Date = MessageParts.Value;
                                    }
                                    else if (MessageParts.Name == "Subject")
                                    {
                                        Subject = MessageParts.Value;
                                    }
                                }
                                if ((RenterEmails.Contains(To) || RenterEmails.Contains(FromAddress)) && (HostEmailAddress == FromAddress || HostEmailAddress == To))
                                {
                                    //READ MAIL BODY
                                    List<string> FileName = GetAttachments(service, HostEmailAddress, Msg.Id);

                                    if (FileName != null && FileName.Count() > 0)
                                    {
                                        foreach (var EachFile in FileName)
                                        {
                                            //GET USER ID USING FROM EMAIL ADDRESS-------------------------------------------------------
                                            string[] RectifyFromAddress = FromAddress.Split(' ');
                                            string FromAdd = RectifyFromAddress[RectifyFromAddress.Length - 1];

                                            if (!string.IsNullOrEmpty(FromAdd))
                                            {
                                                FromAdd = FromAdd.Replace("<", string.Empty);
                                                FromAdd = FromAdd.Replace(">", string.Empty);
                                            }
                                        }
                                    }
                                    else
                                    {
                                    }

                                    //READ MAIL BODY-------------------------------------------------------------------------------------
                                    MailBody = string.Empty;
                                    if (MsgContent.Payload.Parts == null && MsgContent.Payload.Body != null)
                                    {
                                        MailBody = MsgContent.Payload.Body.Data;
                                    }
                                    else
                                    {
                                        MailBody = MsgNestedParts(MsgContent.Payload.Parts);
                                    }

                                    //BASE64 TO READABLE TEXT--------------------------------------------------------------------------------
                                    ReadableText = string.Empty;
                                    ReadableText = Base64Decode(MailBody);
                                    if (!string.IsNullOrEmpty(ReadableText))
                                    {
                                        GmailModel GMail = new GmailModel();
                                        GMail.From = FromAddress;
                                        GMail.Body = ReadableText;
                                        char[] separator = { '+', '-' };
                                        GMail.MailDateTime = Convert.ToDateTime(Date.Split(separator)[0]);
                                        GMail.Attachments = FileName;
                                        GMail.To = To;
                                        EmailList.Add(GMail);
                                    }
                                }
                                else
                                {
                                    continue;
                                }

                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }
            return EmailList;

        }
        public static void MsgMarkAsRead(GmailService service, string HostEmailAddress, string MsgId)
        {
            //MESSAGE MARKS AS READ AFTER READING MESSAGE
            ModifyMessageRequest mods = new ModifyMessageRequest();
            mods.AddLabelIds = null;
            mods.RemoveLabelIds = new List<string> { "UNREAD" };
            service.Users.Messages.Modify(mods, HostEmailAddress, MsgId).Execute();
        }
    }
}