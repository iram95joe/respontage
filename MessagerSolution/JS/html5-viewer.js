/*
 *
 * This JavaScript segment should be used in your HTML5
 * page. It can be used inline between the HEAD tags or
 * referred there from an external JS file.
 *
 * Variables that can be changed by you or needs to be
 * set by you are marked with triple slashes (///). Rest
 * of the code should be left as it is.
 *
 */

var documentViewer;
$(document).ready(function() {
  documentViewer =
    /* Creates a new HTML5 viewer and initializes it with
     * with specified settings.
     */
    new DocumentViewer({
      /* Specifies ASP.NET handler for generating the content
       * for the viewer control.
       */
      handlerUri: "DocumentViewer.axd",

      /// Specifies ID of the HTML element representing the
      /// viewer.
      id: 'doc-viewer-id',

      /// Specifies pixel density of the content displayed in
      /// the viewer.
      renderingDpi: 96,

      /// Specifies settings for the viewer.
      preferences: {
        /// Specifies whether the floating toolbar of the viewer
        /// should be displayed.
        toolbarVisible: true,

        /// Specifies whether page navigation controls needs to
        /// be displayed.
        visibleNavigationControls: {
          /// Specifies whether the "First Page" navigation control
          /// needs to be displayed.
          firstPage: true,

          /// Specifies whether the "Previous Page" navigation
          /// control needs to be displayed.
          prevPage: true,

          /// Specifies whether the "Next Page" navigation control
          /// needs to be displayed.
          nextPage: true,

          /// Specifies whether the "Last Page" navigation control
          /// needs to be displayed.
          lastPage: true,

          /// Specifies whether the "Page Number" indicator needs
          /// to be displayed.
          pageIndicator: true,

          /// Specifies whether the "Page Number" indicator needs
          /// to be displayed.
          gotoPage: true
        },

        /// Specifies whether page magnification controls needs to be
        /// displayed.
        visibleZoomControls: {
          /// Specifies whether page magnification drop-down options
          /// list needs to be displayed.
          fixedSteps: true,

          /// Specifies whether page zoom-in control needs to be
          /// displayed.
          zoomIn: true,

          /// Specifies whether page zoom-out control needs to be
          /// displayed.
          zoomOut: true
        },

        /// Specifies whether page rotation operation controls needs to be
        /// displayed.
        visibleRotationControls: {
          /// Specifies whether clockwise page rotation needs to be
          /// displayed.
          clockwise: true,

          /// Specifies whether counterClockwise page rotation needs to be
          /// displayed.
          counterClockwise: true
        },

        /// Specifies whether file operation controls needs to be
        /// displayed.
        visibleFileOperationControls: {
          /// Specifies whether "File Open" control needs to be
          /// displayed.
          open: true,

          /// Specifies whether "File Download" control needs to be
          /// displayed.
          download: true,

          /// Specifies whether "File Print" control needs to be
          /// displayed.
          print: true,
        }
      },

      /// Specifies event handlers for the viewer.
      events: {
        /// Occurs immediately before navigating to a new page.
        beforePageChange:
          function(
              // Number of the current page (before navigation).
              currentPage,

              /// Number of the page to which the viewer will
              /// navigate to.
              newCurrentPage,

              /// Whether page change should not be allowed.
              cancel) {
            /// Your event-handler code


          },

        /// Occurs immediately after navigating to a page.
        afterPageChange:
          function(
              // Number of the page to which the viewer has
              // navigated to.
              currentPage) {
              /// Your event-handler code


          },

        /// Occurs immediately before the page magnification is
        /// is to be changed
        beforeZoomChange:
          function(
              /// Percentage of current page magnification
              /// (before it is changed)
              zoomPercentage) {
            /// Your event-handler code


          },

        afterZoomChange:
          function(
              // Percentage of current page magnification
              // (after it had been changed)
              zoomPercentage) {
            /// Your event handler code


          },

        /// Occurs immediately before document load into viewer.
        beforeDocumentLoad:
          function(
              /// documenturi of loading document
              /// (before it is loaded)
              documenturi,
              /// Whether loading should not be allowed.
              cancel) {
            /// Your event-handler code

          },

        /// Occurs immediately after document load into viewer.
        afterDocumentLoad:
          function() {
            /// Your event-handler code

          },


       /// Occurs immediately after document upload to the server.
       afterDocumentUpload:
       function (uri) {
           /// Your event-handler code
       },

        /// Occurs immediately before print the document.
        beforeDocumentPrint:
          function(
              /// documenturi of printing document
              /// (before it is printing)
              documenturi,
              /// Whether printing should not be allowed.
              cancel) {
            /// Your event-handler code


          },

        /// Occurs immediately after completing printing of
        ///the document.
        afterDocumentPrint:
          function() {
            /// Your event-handler code


          },

        /// Occurs immediately before close the document.
        beforeDocumentClose:
          function (
              /// documenturi of closing document
              /// (before it is closing)
              documenturi,
              /// Whether closing should not be allowed.
              cancel) {
            /// Your event-handler code
            

          },

        /// Occurs immediately after closing the document.
        afterDocumentClose:
            function () {
                /// Your event-handler code
          },

        /// Occurs immediately before rotation of the document.
        beforePageRotate:
          function (
              /// Current angle of rotation  (before rotation).
              currentRotationAngle,
              /// New angle of rotation to apply
              /// (Before rotation)
              newRotationAngle,
              /// Whether page rotation should not be allowed.
              cancel) {
            /// Your event-handler code


          },

        /// Occurs immediately after rotation of the document.
        afterPageRotate:
          function (
              /// Current angle of rotation (after rotation).
              currentRotationAngle) {
            /// Your event-handler code


          },

        /// Occurs immediately before downloading the document.
        beforeDocumentDownload:
          function (
              /// Download file format (before download).
              downloadFileformat,
              /// Whether download should not be allowed.
              cancel) {
            /// Your event-handler code


          },

        /// Occurs immediately after document has been downloaded.
        afterDocumentDownload:
          function () {
            /// Your event-handler code

          }
        }
      }
    );
  }
);