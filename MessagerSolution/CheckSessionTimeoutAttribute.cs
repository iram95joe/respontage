﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MessagerSolution.Helper;
using Core.Helper;

namespace MessagerSolution
{
    public class CheckSessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (!ctx.User.Identity.IsAuthenticated || ctx.User.Identity.Name=="")
            {
                FormsAuthentication.SignOut();
                ctx.Session.Abandon();
                SessionHandler.IsSessionExpired = true;
                filterContext.Result = new RedirectResult("~/");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}