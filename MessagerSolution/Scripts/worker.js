


// initialization scripts
var tableAssignedWorkerReports;
var tableReimburseReport;
var tableFixedRetainerReport;
var tableWorkerAvailability;
var jobTypeTable;
var tableWorker;
var tablehireWorker;
var tablePayment;
GetJobTypeReport();

function GetJobTypeReport() {
	$.ajax({
		type: 'GET',
		url: "/Worker/JobTypeReport",
		dataType: "json",
		data: {
			workerIds: $('#hidden-worker-filter').val(),
			propertyIds: $('#hidden-property-filter').val(),
			month: $('#assign-month-filter-inp').val()
		},
		success: function (data) {
			if (data.success) {
				$('#jobtype-report tbody').empty();
				var reports = data.reports;
				for (var x = 0; x < reports.length; x++) {
					var columnData = '';

					for (var i = 0; i < reports[x].JobTypReports.length; i++) {
						columnData += '<tr>' +
							'<td>' + reports[x].JobTypReports[i].JobType + '</td>' +
							'<td class="right aligned">' + reports[x].JobTypReports[i].Amount + '</td>' +
							'<td class="right aligned">' + reports[x].JobTypReports[i].Count + '</td>' +
							'<td class="right aligned">' + reports[x].JobTypReports[i].Total + '</td>' +
							'</tr>';
					}
					var htmlData = '<tr>' +
						'<td rowspan="' + (reports[x].JobTypReports.length + 1) + '">' + reports[x].Worker.Firstname + ' ' + reports[x].Worker.Lastname + '</td>' +
						'</tr>' +
						columnData;
					$('#jobtype-report tbody').append(htmlData);
				}
			}
		},
		error: function (error) {
			toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
		}
	});
}
var workerId;

var reimbursementTotal = 0;
var assignmentTotal = 0;
var fixedRetainerTotal = 0;
var Total = 0;

//Assign worker
var nonBookingWorkerTable;
function NonBookingAssignWorker(e) {

	var reinburstDiv = $('#non-booking-re-inburst-note-wrapper');
	var ReinburstmentNote = [];
	$('.non-booking-re-inburst-note').each(function (index, element) {
		var note;
		var reinburstNoteJson;
		var Id = $(this).attr('reimbusement-id')
		var ReinburstNote = $(this).find('input[name="ReinburstNote"]').val();
		var Amount = $(this).find('input[name="Amount"]').val();
		note = {
			Id:Id,
			Note: ReinburstNote,
			Amount: Amount
		};
		ReinburstmentNote.push(note);
	});


	$("input.non-booking-chk-assign").prop("disabled", true);
	$('#non-booking-assign-worker-success-message-wrapper').hide();
	$('#non-booking-assign-worker-error-message-wrapper').hide();

	var workerId = $('input.non-booking-chk-assign:checked').parents('tr').attr('data-id');
	var assignStart = $('#non-booking-worker-start-date').val();
	var assignEnd = $('#non-booking-worker-end-date').val();
	var jobTypeId = $('#non-booking-job-type-id').val();
	var propertyId = $('#non-booking-property-filter').val();
	var description = $('#non-booking-assignment-description').val();

	if (assignStart == '' || assignEnd == '') {
		$('#non-booking-assign-worker-success-message-wrapper').hide();
		$('#non-booking-assign-worker-error-message-wrapper').find('.header').text('Please select assign duration.');
		$('#non-booking-assign-worker-error-message-wrapper').show();
		$('#non-booking-assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.non-booking-chk-assign").prop("disabled", false);
	}
	else if (workerId == null) {
		$('#non-booking-assign-worker-success-message-wrapper').hide();
		$('#non-booking-assign-worker-error-message-wrapper').find('.header').text('Please select worker.');
		$('#non-booking-assign-worker-error-message-wrapper').show();
		$('#non-booking-assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.non-booking-chk-assign").prop("disabled", false);
	}
	else if (jobTypeId == "" || jobTypeId == null || jobTypeId == "0") {
		$('#non-booking-assign-worker-success-message-wrapper').hide();
		$('#non-booking-assign-worker-error-message-wrapper').find('.header').text('Please select Job Type.');
		$('#non-booking-assign-worker-error-message-wrapper').show();
		$('#non-booking-assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.non-booking-chk-assign").prop("disabled", false);
	} else if (propertyId == "" || propertyId == null || propertyId == "0") {
		$('#non-booking-assign-worker-success-message-wrapper').hide();
		$('#non-booking-assign-worker-error-message-wrapper').find('.header').text('Please select Property.');
		$('#non-booking-assign-worker-error-message-wrapper').show();
		$('#non-booking-assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.non-booking-chk-assign").prop("disabled", false);
	}
	else {
		$.ajax({
			type: "POST",
			url: "/Home/AssignWorker",
			data: {
				PropertyId: propertyId,
				ReservationId: 0,
				WorkerId: workerId,
				JobTypeId: jobTypeId,
				StartDate: assignStart,
				Description: description,
				EndDate: assignEnd,
				ReinburstmentNote: JSON.stringify(ReinburstmentNote)
			},
			success: function (data) {
				$('#non-booking-assign-worker-error-message-wrapper').hide();
				$('#non-booking-assign-worker-success-message-wrapper').find('.header').text('Changes has been saved.');
				$('#non-booking-assign-worker-success-message-wrapper').show();
				$('#non-booking-assign-worker-success-message-wrapper').removeClass('hidden');
				$("input.non-booking-chk-assign").prop("disabled", false);
				nonBookingWorkerTable.ajax.reload(null, false);
				tableAssignedWorkerReports.ajax.reload(null, false);
				tableReimburseReport.ajax.reload(null, false);
				tableFixedRetainerReport.ajax.reload(null, false);
				$('#non-booking-job-type-id').dropdown('set selected', '0');
				$('#non-booking-property-filter').dropdown('set selected', '0');
				$('#non-booking-worker-start-date').val('');
				$('#non-booking-worker-end-date').val('');
				$('#non-booking-assignment-description').val('');
				$('.non-booking-re-inburst-note-wrapper').empty();
			}
		});
	}
}

function NonBookingJobTypeChange() {
	nonBookingWorkerTable.ajax.reload(null, false);
}
function NonBookingChangeStartDate() {
	nonBookingWorkerTable.ajax.reload(null, false);
}
function NonBookingChangeEndDate() {
	nonBookingWorkerTable.ajax.reload(null, false);
}

$(document).ready(function () {
	initialize();

	function initialize() {

		setEventListeners();
		LoadPaymentTable();
		WorkerInbox();
		loadAssignmentReportTable();
		loadReimbursementReportTable();
		LoadWorkerAvailability();

		loadRetainerFixedReportTable();

		onAssignmentRowClick();
		onReimbursementRowClick();
		setupSemanticUI();
		GetUnhiredWorkerList();
		NonBooking();
		DeleteNonBookingAssignment();
		SaveSearchCriteria();
		GetSearchCriteria();
		onChangeSearchCriteria();
		OnChangeMMS();
		onUploadImage();
		BatchPaymentClick();
		OnchangeBatchPaymentImage();
		CreateBatchPayment();
		AddNoAssignmentWorkerForBonus();
		AllocatePayment();
		BatchPaymentAmountKeyup();
		ExpensePaymentAmountKeyup();
		BatchPaymentChange();
		BatchSelectAll();
		WorkerAssignmentJsEvent();
		setupSemanticUI();
	}
	function LoadPaymentTable() {
		tablePayment = $('#payment-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": true,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Worker/WorkerPaymentReport",
				"type": 'GET',
				"data": function () {
					return {
						type: $('#fee-type-filter').val()
					};
				}
			},
			"columns": [
				{ "data": "Description" },
				{ "data": "Amount" },
				{ "data": "ExpensesPaid" },
				{ "data": "Balance" },
				{ "data": "Status" },
				{ "data": "Date" },
				{ "data": "Action" }
			],
			columnDefs: [{
				targets: 5, render: function (data) {
					return moment(data).format('MMMM D,YYYY');
				}
			}],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('batch-payment-id', data.Id);
			}
		});
	}

	function ImageExist(url) {
		var result = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(url);
		return result;
	}

	function BatchPaymentChange() {
		$(document).on('change', '#batch-dropdown', function () {
			var id = $(this).val();
			if (id != 0) {
				$.ajax({
					type: "GET",
					url: '/Worker/GetBatchPayment',
					data: { id: id },
					dataType: "json",
					success: function (data) {
						$('#worker-batch-id').val(data.batchpayment.Id);
						$('#worker-batch-payment-amount').val(data.batchpayment.Amount);
						$('#worker-batch-payment-description').val(data.batchpayment.Description);
						$('#worker-batch-payment-date').val(moment.utc(data.batchpayment.PaymentDate).format("MMMM D, YYYY"));
						$('.worker-batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
						LoadNoAssignmentWorker();
						$('.dateInputField').calendar({
							type: 'date',
						});
						$('#worker-assignment-expense-edit').empty();
						$('#worker-retainer-edit').empty();
						$('#worker-bonus-edit').empty();
						$('#worker-batch-image-upload').empty();
						var images = data.images;
						for (var x = 0; x < images.length; x++) {
							var image = images[x];
							$('#worker-batch-image-upload').append((ImageExist(image) ? '<div class="ui small image"><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div class="ui small image"><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
						}
						var assignmentReimbursement = data.assignmentReimbursement;
						for (var x = 0; x < assignmentReimbursement.length; x++) {
							$('#worker-assignment-expense-edit').prepend(AssignmentReimbursementModel(assignmentReimbursement[x].ExpenseId, assignmentReimbursement[x].Description, assignmentReimbursement[x].Amount, assignmentReimbursement[x].DueDate, assignmentReimbursement[x].Balance, assignmentReimbursement[x].ExpensePaymentId, true));

						}
						var retainerFixed = data.retainerFixed;
						for (var x = 0; x < retainerFixed.length; x++) {
							$('#worker-retainer-edit').prepend(BonusRetainerModel(retainerFixed[x].ExpenseId, retainerFixed[x].Description, retainerFixed[x].Amount, retainerFixed[x].DueDate, "", retainerFixed[x].Category, retainerFixed[x].Balance, retainerFixed[x].ExpensePaymentId, true));

						}

						var bonus = data.bonus;
						for (var x = 0; x < bonus.length; x++) {
							$('#worker-bonus-edit').prepend(BonusRetainerModel(bonus[x].ExpenseId, bonus[x].Description, bonus[x].Amount, bonus[x].DueDate, "", bonus[x].Category, bonus[x].Balance, bonus[x].ExpensePaymentId, true));

						}
						$('#worker-batch-payment-amount').keyup();
					}
				});
			}
			else {
				$('#worker-assignment-expense').empty();
				$('#worker-bonus').empty();
				$('#worker-retainer').empty();
				$('#worker-assignment-expense-edit').empty();
				$('#worker-bonus-edit').empty();
				$('#worker-retainer-edit').empty();
				$('#worker-batch-proof-pic').val('');
				$('#worker-batch-image-upload').empty();
				$('#worker-batch-payment-description').val('');
				$('#worker-batch-payment-amount').val('');
				$('#worker-batch-id').val('0');
				$('#worker-batch-payment-remaining').text('0');
            }
		});
    }
	function LoadBatchPayment() {
		$.ajax({
			type: "GET",
			url: '/Expense/GetBatchPaymentList',
			success: function (data) {
					var batchPayment = data.batchPayment;
					$('#batch-dropdown').empty();
					var optionData = '<option value="0">New Payment</option>';
					for (var i = 0; i < batchPayment.length; i++) {
						optionData += '<option value="' + batchPayment[i].Id + '">' + batchPayment[i].Description + '</option>';
					}
				$('#batch-dropdown').append(optionData);
				$('#batch-dropdown').dropdown('set selected','0');
			}
		});
    }
	function AllocatePayment() {
		$(document).on('click', '.allocate-btn', function () {
			var id = $(this).parents('tr').attr("batch-payment-id");
			$.ajax({
				type: "GET",
				url: '/Worker/GetBatchPayment',
				data: { id: id },
				dataType: "json",
				success: function (data) {
					$('#worker-batch-id').val(data.batchpayment.Id);
					$('#worker-batch-payment-amount').val(data.batchpayment.Amount).keyup();
					$('#worker-batch-payment-description').val(data.batchpayment.Description);
					$('#worker-batch-payment-date').val(moment.utc(data.batchpayment.PaymentDate).format("MMMM D, YYYY"));
					$('.worker-batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
					LoadNoAssignmentWorker();
					$('.dateInputField').calendar({
						type: 'date',
					});
					$('#worker-assignment-expense').empty();
					$('#worker-retainer').empty();
					$('#worker-bonus').empty();
					$('#worker-assignment-expense-edit').empty();
					$('#worker-retainer-edit').empty();
					$('#worker-bonus-edit').empty();
					$('#worker-batch-image-upload').empty();
					var images = data.images;
					for (var x = 0; x < images.length; x++) {
						var image = images[x];
						$('#worker-batch-image-upload').append((ImageExist(image) ? '<div class="ui small image"><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div class="ui small image"><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
					}
					var assignmentReimbursement = data.assignmentReimbursement;
					for (var x = 0; x < assignmentReimbursement.length; x++) {
						$('#worker-assignment-expense').append(AssignmentReimbursementModel(assignmentReimbursement[x].ExpenseId, assignmentReimbursement[x].Description, assignmentReimbursement[x].Amount, assignmentReimbursement[x].DueDate, assignmentReimbursement[x].Balance, assignmentReimbursement[x].ExpensePaymentId,true));

					}
					var retainerFixed = data.retainerFixed;
					for (var x = 0; x < retainerFixed.length; x++) {
						$('#worker-retainer').append(BonusRetainerModel(retainerFixed[x].ExpenseId, retainerFixed[x].Description, retainerFixed[x].Amount, retainerFixed[x].DueDate, "", retainerFixed[x].Category, retainerFixed[x].Balance, retainerFixed[x].ExpensePaymentId,true));

					}
					
					var bonus = data.bonus;
					for (var x = 0; x < bonus.length; x++) {
						$('#worker-bonus').append(BonusRetainerModel(bonus[x].ExpenseId, bonus[x].Description, bonus[x].Amount, bonus[x].DueDate, "", bonus[x].Category, bonus[x].Balance, bonus[x].ExpensePaymentId, true));

					}
					$('#worker-batch-payment-amount').keyup();
				}
			});
		});
	}
	function CreateBatchPayment() {
		$(document).on('click', '#worker-save-batch-payment', function (e) {
			if ($('#worker-batch-payment-description').val() != "" && $('#worker-batch-payment-date').val() != "" && $('#worker-batch-payment-amount').val() != "") {
				var Payment = [];
				$('.expense').each(function (index, element) {

					var amount = $(this).find('.expense-payment-amount').val();
					var id = $(this).find('.expense-id').attr('expense-id');
					var paymentId = $(this).find('.expense-payment-amount').attr('payment-id');
					var description = $(this).attr('description');
					var duedate = $(this).find('.expense-id').attr('duedate');

					//JM 10/09/19 Check if payment is greater than balance if not don`t save
					//if (parseFloat($(this).find('.expense-payment-amount').attr('max')) < parseFloat(amount)) {
					//	isOverpaid = true;
					//}
					var jsonPayment = {
						ExpenseId: id,
						Amount: amount,
						PaymentId: paymentId,
						Description: description,
						DueDate: duedate
					};
					Payment.push(jsonPayment);

				});
				$('.non-expense').each(function (index, element) {

					var amount = $(this).find('.expense-payment-amount-balance').val();
					var id = $(this).find('.expense-id').attr('expense-id');
					var paymentId = $(this).find('.expense-payment-amount-balance').attr('payment-id');
					var description = $(this).find('.expense-id').attr('description');
					var category = $(this).find('.expense-id').attr('category');
					var expenseAmount = $(this).find('.expense-id').attr('expense-amount');
					var duedate = $(this).find('.expense-id').attr('duedate');
					//JM 10/09/19 Check if payment is greater than balance if not don`t save
					//if (parseFloat($(this).find('.expense-payment-amount').attr('max')) < parseFloat(amount)) {
					//	isOverpaid = true;
					//}
					var jsonPayment = {
						ExpenseId: id,
						Amount: amount,
						PaymentId: paymentId,
						Description: description,
						Category: category,
						ExpenseAmount: expenseAmount,
						DueDate: duedate
					};
					Payment.push(jsonPayment);

				});
				//if (isOverpaid == false) {
				var formData = new FormData();
				for (var i = 0; i < $('#worker-batch-proof-pic').get(0).files.length; ++i) {
					formData.append("Images[" + i + "]", $('#worker-batch-proof-pic').get(0).files[i]);
				}
				formData.append("batchid", $('#worker-batch-id').val());
				formData.append("description", $('#worker-batch-payment-description').val());
				formData.append("paymentDate", $('#worker-batch-payment-date').val());
				formData.append("amount", $('#worker-batch-payment-amount').val());
				formData.append("payment", JSON.stringify(Payment));
				$.ajax({
					type: "POST",
					url: '/Expense/WorkerCreateBatchPayment',
					contentType: false,
					cache: false,
					dataType: "json",
					processData: false,
					data: formData,
					success: function (data) {
						if (data.success) {
							tablePayment.ajax.reload(null, false);
							tableAssignedWorkerReports.ajax.reload(null, false);
							tableReimburseReport.ajax.reload(null, false);
							tableFixedRetainerReport.ajax.reload(null, false);
							$('.worker-batch-payment-modal').modal('hide');
							swal("Payment saved!", "", "success");
						}
					}
				});
			}
		});
	}
	function AssignmentReimbursementModel(expenseId, description, amount, date, balance, paymentId, IsEdit = false) {
		return '<div class="fields expense"><div class="five wide field"><p class="expense-id" duedate="' + date + '" expense-id="' + expenseId + '" description="' + description + '">' + description + ' ' + moment.utc(date).format("MMMM D, YYYY") + '<br><b>Balance:' + (IsEdit ? (balance - amount) : amount) + '</b></p></div>' +
			'<div class="six wide field">' +
			'<input type="number" min="0" max="' + (IsEdit ? balance : amount )+ '" payment-id="' + paymentId + '" class="expense-payment-amount" required placeholder="Payment Amount" value=' + amount + ' />' +
			'</div></div>'
	}
	function BonusRetainerModel(expenseId, description, amount, date, status, category, balance, paymentId, IsEdit = false) {
		return '<div class="fields non-expense"><div class="five wide field"><p class="expense-id" duedate="' + date + '" expense-amount="' + amount + '" category="' + category + '" expense-id="' + expenseId + '" description="' + description + '">' + description + ' ' + moment.utc(date).format("MMMM D, YYYY") + '<br><b>Balance:' + (IsEdit ? (balance - amount) : amount) + '</b>' + status + '</p></div>' +
			'<div class="six wide field">' +
			'<input type="number" min="0" max="' + (IsEdit ? balance : amount) + '" payment-id="' + paymentId + '" class="expense-payment-amount-balance" required placeholder="Payment Amount" value=' + amount + ' />' +
			'</div></div>'
	}
	function BatchSelectAll() {
		$(document).on('change', '.select-all-for-batch', function () {
			var table = $(this).parents('table').DataTable();
			var isCheck = $(this).prop("checked");
			table.rows().eq(0).each(function (index) {
				$(table.row(index).node()).find('input').prop('checked', isCheck)
				//var checkbox = $(row).find('input').prop("checked");
				//if ($(row).find('.worker-reimbursement-checkbox').prop("checked")) {
				//	reimbursementIds.push($(row).attr('reimbursement-id'));
				//}
			});
		});
	}
	function BatchPaymentClick() {
		$(document).on('click', '#assignment-batch-payment-btn', function () {
			LoadBatchPayment();
			var total = 0;
			$('#worker-assignment-expense').empty();
			$('#worker-bonus').empty();
			$('#worker-retainer').empty();
			$('#worker-assignment-expense-edit').empty();
			$('#worker-bonus-edit').empty();
			$('#worker-retainer-edit').empty();
			var assignmentIds = [];
			var reimbursementIds = [];
			var assignmenttable = $('#worker-assignment-report-table').DataTable();
			assignmenttable.rows().eq(0).each(function (index) {
				var row = assignmenttable.row(index).node();
				if ($(row).find('.worker-assignment-checkbox').prop("checked")) {
				assignmentIds.push($(row).attr('assignment-id'));
				}
			});
			var reimbursementtable = $('#worker-reimburse-table').DataTable();
			reimbursementtable.rows().eq(0).each(function (index) {
				var row = reimbursementtable.row(index).node();
				if ($(row).find('.worker-reimbursement-checkbox').prop("checked")) {
					reimbursementIds.push($(row).attr('reimbursement-id'));
				}
			});

			$.ajax({
				type: 'GET',
				url: "/Expense/GetAssignmentExpenses",
				data: { assignmentIds: JSON.stringify(assignmentIds), reimbursementIds: JSON.stringify(reimbursementIds) },
				async: false,
				success: function (data) {
					var expenses = data.expenses;
					for (var x = 0; x < expenses.length; x++) {
						if (!isNaN(parseFloat(expenses[x].Amount))) {
							total = parseFloat(total) + parseFloat(expenses[x].Amount);
						}
						$('#worker-assignment-expense').append(AssignmentReimbursementModel(expenses[x].Id, expenses[x].Description, expenses[x].Amount, expenses[x].DueDate, 0, 0));
					}
				}
			});
			$.ajax({
				type: 'GET',
				url: "/Worker/GetWorkerBonus",
				async:false,
				data: { month: $('#assign-month-filter-inp').val()},
				success: function (data) {
					var workers = data.workers;
					for (var x = 0; x < workers.length; x++) {
						if (!isNaN(parseFloat(workers[x].Amount))) {
							total = parseFloat(total) + parseFloat(workers[x].Amount);
						}
						$('#worker-bonus').append(BonusRetainerModel(0, workers[x].Description, workers[x].Amount, workers[x].DueDate, (workers[x].AssignmentCount > 0 ? "" : "(No assignment)"),"Bonus",0,0));
					}
				}
			});

			var retainerFixedtable = $('#worker-retainer-monthly-table').DataTable();
			retainerFixedtable.rows().eq(0).each(function (index) {
				var rowNode = retainerFixedtable.row(index).node();
				if ($(rowNode).find('.fixed-retainer-checkbox').prop("checked")) {
					var row = $(rowNode);
					if (!isNaN(parseFloat(row.find('td:eq(2)').html()))) {
						total = parseFloat(total) + parseFloat(row.find('td:eq(2)').html());
					}
					$('#worker-retainer').append(BonusRetainerModel(0, row.find('td:eq(1)').html(), row.find('td:eq(2)').html(), row.find('td:eq(3)').html(), "", row.find('td:eq(4)').html(), 0, 0));
				}
			});	
			$('#worker-batch-proof-pic').val('');
			$('#worker-batch-image-upload').empty();
			$('#worker-batch-payment-description').val('');
			$('#worker-batch-payment-amount').val('');
			$('#worker-batch-payment-date').val('');
			$('#worker-batch-id').val('0');
			$('#worker-batch-payment-remaining').text('0');
			$('.worker-batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
			LoadNoAssignmentWorker();
			$('.dateInputField').calendar({
				type: 'date',
			});
			$('#worker-batch-payment-date').val(moment(new Date()).format("MMMM DD,YYYY"));
			
			//$(document).find('.expense-payment-amount').each(function (index, element) {
			//	total = total + $(this).val().toFixed(2);
			//});
			//$(document).find('.expense-payment-amount-balance').each(function (index, element) {
			//	total = total + $(this).val();
			//});
			$('#worker-batch-payment-amount').val(total);

		});

	}
	function LoadNoAssignmentWorker() {
		$.ajax({
			type: 'GET',
			url: "/Worker/GetWorkerWithoutAssignment",
			data: { month: $('#assign-month-filter-inp').val(), IsWithAssignment: false },
			success: function (data) {
				$('#no-assignment-worker').empty();
				var workers = data.workers;
				$('#no-assignment-worker').append('<option value="">Add Bonus for Worker</option>');
				for (var x = 0; x < workers.length; x++) {
					$('#no-assignment-worker').append('<option value="' + workers[x].Id + '">' + workers[x].Description + '</option>');
				}
				//$('.ui.dropdown').dropdown();
			}
		});
	}
	function AddNoAssignmentWorkerForBonus() {
			$(document).on('change', '#no-assignment-worker', function () {
				var id = $(this).val();
				$.ajax({
					type: 'GET',
					url: "/Worker/GetWorkerWithoutAssignmentById",
					data: { month: $('#assign-month-filter-inp').val(), id:id },
					success: function (data) {
						var worker = data.worker
						$('#worker-bonus').append(BonusRetainerModel(0, worker.Description, worker.Amount, worker.DueDate, (worker.AssignmentCount > 0 ? "" : "(No assignment)"), "Bonus"));
						$('#no-assignment-worker option[value=' + id + ']').remove();
					}
				});
			});
	}


	function ExpensePaymentAmountKeyup() {
		$(document).on('keyup', '.expense-payment-amount,.expense-payment-amount-balance', function () {


			var totalAmountToPay = 0;
			$('.expense-payment-amount').each(function (index, element) {
				if (!isNaN(parseFloat($(this).val()))) {
					totalAmountToPay = totalAmountToPay + parseFloat($(this).val());
				}
			});
			$('.expense-payment-amount-balance').each(function (index, element) {
				if (!isNaN(parseFloat($(this).val()))) {
					totalAmountToPay = totalAmountToPay + parseFloat($(this).val());
				}
			});
			ValidateTotalPayment($('#worker-batch-payment-amount'), totalAmountToPay);
			var maxValue = $(this).attr('max');
			ValidatePayment($(this), maxValue);
			$('#worker-batch-payment-amount').text(parseFloat($('#worker-batch-payment-amount').val()) - parseFloat(totalAmountToPay));

		});
	}
	function BatchPaymentAmountKeyup() {
		$(document).on('keyup', '#worker-batch-payment-amount', function () {

			var totalAmountToPay = 0;
			$('.expense-payment-amount').each(function (index, element) {
				if (!isNaN(parseFloat($(this).val()))) {
					totalAmountToPay = totalAmountToPay + parseFloat($(this).val());
				}
			});
			$('.expense-payment-amount-balance').each(function (index, element) {
				if (!isNaN(parseFloat($(this).val()))) {
					totalAmountToPay = totalAmountToPay + parseFloat($(this).val());
				}
			});
			ValidateTotalPayment($(this), totalAmountToPay);

			$('#worker-batch-payment-remaining').text(parseFloat($(this).val()) - parseFloat(totalAmountToPay));

		});
	}
	function ValidatePayment(obj, max) {
		if (parseFloat(obj.val()) > parseFloat(max)) {
			obj.next().remove();
			obj.after('<div class="ui basic red pointing prompt label transition visible">Payment must not greater than balance and a number</div>');
		} else {
			obj.next().remove();
		}
	}
	function ValidateTotalPayment(obj, max) {
		if (parseFloat(obj.val()) < parseFloat(max)) {
			obj.next().remove();
			obj.after('<div class="ui basic red pointing prompt label transition visible">Payment must greater than allocated amount</div>');
		} else {
			obj.next().remove();
		}

	}
	function OnchangeBatchPaymentImage() {
		$(document).on('change', '#worker-batch-proof-pic', function (e) {
			$('#worker-batch-image-upload').empty();
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				var type = this.files[i]['type'].split('/')[0];
				if (type == 'image') {
					$('#worker-batch-image-upload').append('<div class="ui small image"><img src="' + fileUrl + '"><div>');
				}
				else {
					$('#worker-batch-image-upload').append('<a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a>');
				}
			}
		});
	}
	function GetSearchCriteria() {
		$.ajax({
			type: 'GET',
			url: "/Worker/GetSearchCriteria",
			success: function (data) {
				var criterias = data.searchCriterias;

				if (criterias.length == 0) {
					$('#worker-report-criteria').parents('.field').attr('hidden', true);
				}
				else {
					$('#worker-report-criteria').parents('.field').removeAttr('hidden');
					for (var x = 0; x < criterias.length; x++) {
						$('#worker-report-criteria').append('<option value="' + criterias[x].Id + '">' + criterias[x].Name + '</option>');
					}
				}
			}
		});
	}
	function onChangeSearchCriteria() {
		$(document).on('change', '#worker-report-criteria', function () {
			var id = $(this).val();
			$.ajax({
				type: 'GET',
				url: "/Worker/SearchCriteriaGetData",
				data: { id: id },
				success: function (data) {
					$('#worker-filter').dropdown('clear');
					$('#property-filter').dropdown('clear');
					var propertyIds = $.parseJSON(data.searchCriteria.PropertyIds);
					for (var x = 0; x < propertyIds.length; x++) {
						$('#property-filter').dropdown('set selected', [propertyIds[x]]);
					}
					var workerIds = $.parseJSON(data.searchCriteria.WorkerIds);
					console.log(workerId);
					for (var x = 0; x < workerIds.length; x++) {
						$('#worker-filter').dropdown('set selected', [workerIds[x]]);
					}
					$('#assign-month-filter').val(moment.utc(data.searchCriteria.Date).format("MMMM YYYY"));
				}
			});
		});
	}

	function SaveSearchCriteria() {
		
		$(document).on('click', '#save-search-criteria', function () {
			var propertyIds = [];
			for (var e = 0; e < $('#property-filter').val().length; e++) {
				propertyIds.push($('#property-filter').val()[e]);
			}
			var workerIds = [];
			for (var e = 0; e < $('#worker-filter').val().length; e++) {
				workerIds.push($('#worker-filter').val()[e]);
			}
			swal({
				title: "Save Search Criteria?",
				text: "",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Accept",
				showCancelButton: true,
				closeOnConfirm: true,
				showLoaderOnConfirm: true,
				inputPlaceholder: "Write name"
			},
				function (message) {
					
					var formData = new FormData();
					formData.append("Name", message);
					formData.append("PropertyIds", JSON.stringify(propertyIds));
					formData.append("WorkerIds", JSON.stringify(workerIds));
					formData.append("Date", $('#assign-month-filter').val());
					$.ajax({
						type: 'POST',
						url: "/Worker/SaveSearch",
						data: formData,
						processData: false,
						contentType: false,
						success: function (data) {
							$('#worker-report-criteria').append('<option value="' + data.searchCriteria.Id + '">' + data.searchCriteria.Name + '</option>');
							$('#worker-report-criteria').parents('.field').removeAttr('hidden');
						}
					});
				});
		});
	}
	function DeleteNonBookingAssignment() {
		$(document).on('click', '.delete-assignment', function () {
			var assignmentId = $(this).attr('assignmentId');
			swal({
				title: "Are you sure you want to delete this assignment?",
				text: "",
				type: "warning",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Delete",
				showCancelButton: true,
				closeOnConfirm: true,

			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Worker/DeleteNonBookingAssignment",
						data: { assignmentId: assignmentId },
						success: function (data) {
							if (data.success) {
								tableAssignedWorkerReports.ajax.reload(null, false);
								tableReimburseReport.ajax.reload(null, false);
							}
						}
					});
				});
		});
	}



	function NonBooking() {
		NonBookingBtnClick();
		onAvaiableLoadWorker();
		onAddNonBookingReinburstmentNote();
	}
	function onAddNonBookingReinburstmentNote() {
		$(document).off('click', '.non-booking-add-reinburst-button');
		$(document).on('click', '.non-booking-add-reinburst-button', function () {
			var noteHtml =
				'<div class="non-booking-re-inburst-note" reimbusement-id="0">' +
				'<div class="ui input twelve wide field">' +
				'<input type="text" name="ReinburstNote" placeholder="Note">' +
				'</div>' +
				'<div class="ui input twelve wide field">' +
				'<input type="text" name="Amount" placeholder="Amount">' +
				'</div>' +
				'<a href="#" class="delete-note-reinburst-button" data-tooltip="Remove"><i class="orange minus circle icon remove-field-icon"></i></a>' +
				'</div></br>';
			$('.non-booking-re-inburst-note-wrapper').append(noteHtml);
			$('#non-booking-assignment-modal').modal('refresh');
		});
	}

	function onAvaiableLoadWorker() {
		$('#non-booking-assign-worker-success-message-wrapper').hide();
		$('#non-booking-assign-worker-error-message-wrapper').hide();
		nonBookingWorkerTable = $('#non-booking-worker-list-table').DataTable({
			"aaSorting": [],
			"bAutoWidth": false,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Home/GetAvailableWorkers",
				"type": 'GET',
				"data": function () {
					return {
						jobTypeId: $('#non-booking-job-type-id').val(),
						startDate: $('#non-booking-worker-start-date').val(),
						endDate: $('#non-booking-worker-end-date').val(),
						IsNonBooking: true
					};
				}
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Job" },
				{ "data": "PaymentType" },
				{ "data": "PaymentRate" },
				{ "data": "Action", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-id', data.Id);
			},
			//"rowCallback": function( row, data ) {
			//    if ( data.grade == "A" ) {
			//        $('td:eq(4)', row).html( '<b>A</b>' );
			//    }
			//},
			"drawCallback": function (settings) {
				$('.ui.calendar.dt').calendar({ type: 'datetime' });
			}
		});
	}
	function NonBookingBtnClick() {
		$(document).on('click', '#non-booking-btn', function () {
			$('#non-booking-assignment-modal').modal({
				onApprove: function (e) {
					$.ajax({
						type: 'POST',
						url: "/Worker/AssignWorker",
						data: { siteType: siteType, hostId: hostId, reservationCode: reservationCode, message: message, threadId: threadId },
						success: function (data) {
							if (data.success) {
								//parent.find("*").removeClass("blue");
								//parent.find("*").attr("disabled", true);
							}
						}
					});
				}
			})
				.modal('show')
				.modal('refresh');

			$('.ui.calendar.dt').calendar({ type: 'datetime' });
			$('.ui.dropdown').dropdown();
		});
	}


	function setupSemanticUI() {
		$('.ui.accordion').accordion();
		$('.ui.dropdown').dropdown({ on: 'hover' });
		$('.ui.checkbox').checkbox();
		$('.ui.calendar').calendar({ type: 'date' });
		//$('.ui.calendar.month').calendar({ type: 'month' });
		$('.ui.calendar.time').calendar({ type: 'time' });
		$('table td a.table-tooltip').popup();
		$('.ui.checkbox').checkbox();
		$('.menu .item').tab({
			onVisible: function (tabPath) {
				if (tabPath == "worker-messages-tab") {
					$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
				}
			}
		});
		$('.assign-month-filter').calendar({
			type: 'month', onChange: function (date, text, mode) {
				$('#assign-month-filter-inp').val(moment(date).format('MMMM YYYY'));
				tableAssignedWorkerReports.ajax.reload(null, false);
				tableReimburseReport.ajax.reload(null, false);
				tableFixedRetainerReport.ajax.reload(null, false);
				GetJobTypeReport();
			}
		});
		$('#assign-month-filter-inp').val(moment(new Date()).format('MMMM YYYY'));
	}

	function setEventListeners() {
		OnchangeImage();
		onImageClick();
		onAssignWorker();
		onCreateNewWorker();
		onCreateWorkerSchedule();
		onHeaderSearch();

		EditWorker();
		AddWorker();
		onChangeAddWorker();
		onChangeEditWorker();
		onSearchButton();
		AddSchedule();
		EditSchedule();
		AddJobType();
		AddContact();
		AddRetainer();
		AddJobTypeEditForm();
		RemoveJobType();
		RemoveContact();
		RemoveRetainer();
		AvailabilitySubmit();
		LoadJobTypeTableData();
		OnTypeFilterChange();
		OnJobTypeModal();
		onEditJobType();
		OnDeleteJobType();
		RetainerFixedGenerateExpense();
		SaveAutoAssign();

	}
	// ------------------
	// ------------------
	//   Event Listeners
	// ------------------
	// ------------------

	function SaveAutoAssign() {
		$(document).on('click', '.auto-assign-save', function (e) {
			var tdparent = $(this).parent();
			var propertyId = tdparent.attr('data-property-id');
			var AutoAssignments = [];
			tdparent.find('.assignment-story-container').each(function (index, element) {
				if ($(this).attr('data-has-departure-arrival') == "True") {
					var arrivalDiv = $(this).find('.arrival');
					var departureDiv = $(this).find('.departure');

					var arrivalId = arrivalDiv.attr('data-auto-assign-id');
					var arrivalWorkerId = arrivalDiv.find('select[name="worker"]').val();
					var arrivalJobtypeId = arrivalDiv.find('select[name="jobtype"]').val();
					var arrivalisautodelete = arrivalDiv.find('input[name="auto-delete"]').is(':checked');
					if (arrivalWorkerId != "" && arrivalJobtypeId != "") {
						var autoAssignment = {
							Id: arrivalId,
							PropertyId: propertyId,
							WorkerId: arrivalWorkerId,
							JobTypeId: arrivalJobtypeId,
							Type: 1,
							IsAutoDelete: arrivalisautodelete
						};
						AutoAssignments.push(autoAssignment);
					}
					var id = departureDiv.attr('data-auto-assign-id');
					var workerId = departureDiv.find('select[name="worker"]').val();
					var jobtypeId = departureDiv.find('select[name="jobtype"]').val();
					var isautodelete = departureDiv.find('input[name="auto-delete"]').is(':checked');
					if (workerId != "" && jobtypeId != "") {
						var autoAssignment = {
							Id: id,
							PropertyId: propertyId,
							WorkerId: workerId,
							JobTypeId: jobtypeId,
							Type: 2,
							IsAutoDelete: isautodelete
						};
						AutoAssignments.push(autoAssignment);
					}
				}
				else {
					var autoAssignmentDiv = $(this).find('.assignment');
					var id = autoAssignmentDiv.attr('data-auto-assign-id');
					var workerId = autoAssignmentDiv.find('select[name="worker"]').val();
					var jobtypeId = autoAssignmentDiv.find('select[name="jobtype"]').val();
					var type = autoAssignmentDiv.find('select[name="put-checkin-checkout"]').val();
					var isautodelete = autoAssignmentDiv.find('input[name="auto-delete"]').is(':checked');
					if (workerId != "" && jobtypeId != "" && type != "") {
						var autoAssignment = {
							Id: id,
							PropertyId: propertyId,
							WorkerId: workerId,
							JobTypeId: jobtypeId,
							Type: type,
							IsAutoDelete: isautodelete
						};
						AutoAssignments.push(autoAssignment);
					}
				}
			});
			$.ajax({
				type: "POST",
				url: "/Worker/SaveDefaultWorker",
				data: { list: JSON.stringify(AutoAssignments) },
				dataType: "json",
				success: function (data) {
					swal("Default worker has been saved.", "", "success");
				}
			});
		});
	}
	function onImageClick() {
		$(document).on('click', '.image', function (e) {
			$('.profile-pic').click();
			//$(this).parent().find('input[class="profile-pic"]').click();
		});
	}
	function OnchangeImage() {
		$(document).on('change', '.profile-pic', function () {
			var imageUrl = window.URL.createObjectURL(this.files[0])
			$('.image:not(.avatar)').attr("src", imageUrl);
			//$('#profile-image').attr("src", imageUrl);


		});
	}


	// added funtionality to add/edit/delete worker job type 9/30/17
	var workerJobTypeAddEdit = function () {
		var id = $('#worker-job-type-form input[name=job-type-id]').val();
		var name = $('#worker-job-type-form input[name=job-type]').val();
		var classificationId = $("#worker_classification_type").val();

		if (id == "0" && name != '') {
			$.ajax({
				type: 'POST',
				url: "/Home/AddJobType",
				data: {
					name: name,
					classificationId: classificationId   //Add by Danial 10-3-2018
				},
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Job Type has been saved.", type: "success" },
							function () {
								clearWorkerJobTypeModal();
								refreshJobTypeTableData();
								window.location.reload();
							}
						);
						$('#worker-job-type-modal').modal('hide');
					}
					else {
						$('#worker-job-type-modal').modal('hide');
						swal("Error in saving new job type", "", "error");
					}
				}
			});
		}
		else if (id > 0 && name != '') {
			$.ajax({
				type: 'POST',
				url: "/Home/UpdateJobType",
				data: {
					id: id,
					name: name,
					classificationId: classificationId   //Add by Danial 10-3-2018
				},
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Job Type has been updated.", type: "success" },
							function () {
								clearWorkerJobTypeModal();
								refreshJobTypeTableData();
								window.location.reload();
							}
						);
						$('#worker-job-type-modal').modal('hide');
					}
					else {
						$('#worker-job-type-modal').modal('hide');
						swal("Error in updating job type", "", "error");
					}
				}
			});
		}
	}
	function LoadJobTypeTableData() {
		jobTypeTable = $('#job-type-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": true,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Home/GetAllJobTypes",
				"type": 'GET',
				"data": function () {
					return {
						type: $('#job-type-filter').val()
					}
				}
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Type" },
				{ "data": "ClassificationType" },  //Add by Danial 10-3-2018
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-job-type-id', data.Id);
			}
		});
	}
	function refreshJobTypeTableData() {
		jobTypeTable.ajax.reload(null, false);
	}
	function OnTypeFilterChange() {
		$("#job-type-filter").on('change', function () {
			refreshJobTypeTableData()
		});
	}
	function OnJobTypeModal() {
		$("#create-job-type-btn").on('click', function () {
			clearWorkerJobTypeModal();
			$('#worker-job-type-modal').modal({
				onApprove: workerJobTypeAddEdit
			})
				.modal('show')
				.modal('refresh');
		});
	}
	function onEditJobType() {
		$(document).on('click', '.edit-job-type-btn', function () {
			var id = $(this).parents('tr').attr("data-job-type-id");

			$.ajax({
				type: 'GET',
				url: "/Home/WorkerJobDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						clearWorkerJobTypeModal();
						var form = $('#worker-job-type-form');
						form.find('input[name=job-type-id]').val(data.worker.Id);
						form.find('input[name=job-type]').val(data.worker.Name);
						$('#worker-job-type-modal').modal({
							onApprove: workerJobTypeAddEdit
						})
							.modal('show')
							.modal('refresh');
					}
				}
			});
		});
	}
	function OnDeleteJobType() {
		$(document).on('click', '.delete-job-type-btn', function () {
			var id = $(this).parents('tr').attr("data-job-type-id");
			swal({
				title: "Are you sure you want to delete this worker job type ?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: "POST",
						url: "/Home/DeleteJobType",
						data: { id: id },
						success: function (data) {
							if (data.success) {
								swal({ title: "Success", text: "Job Type has been deleted.", type: "success" },
									function () {
										clearWorkerJobTypeModal();
										refreshJobTypeTableData();
										window.location.reload();
									}
								);
							}
						}
					});
				});
		});
	}
	function clearWorkerJobTypeModal() {
		var form = $('#worker-job-type-form');
		form.find('input[name=job-type-id]').val('0');
		form.find('input[name=job-type]').val('');
	}
	// end worker job type

	function WorkerAssignmentJsEvent() {

		$(document).on('change', '#worker-filter', function () {
			document.getElementById("hidden-worker-filter").value = $("#worker-filter").val().join();
			tableAssignedWorkerReports.ajax.reload(null, false);
			tableReimburseReport.ajax.reload(null, false);
			tableFixedRetainerReport.ajax.reload(null, false);
			GetJobTypeReport();

		});

		$(document).on('change', '#property-filter', function () {
			document.getElementById("hidden-property-filter").value = $("#property-filter").val().join();
			tableAssignedWorkerReports.ajax.reload(null, false);
			tableReimburseReport.ajax.reload(null, false);
			tableFixedRetainerReport.ajax.reload(null, false);
			GetJobTypeReport();

		});
	}

	function GetReportTotal() {
		Total = assignmentTotal + reimbursementTotal + fixedRetainerTotal;
		$('#total').text(Total.toFixed(2));

	}

	function onAssignmentRowClick() {
		$(document).on('click', '#worker-assignment-report-table tr td:not(:first-child)', function (e) {
			var tr = $(this).parents('tr');
			var threadId = $(tr).attr('data-thread-id');
			var hostId = $(tr).attr('data-host-id');
			var inquiryId = $(tr).attr('data-inquiry-id');

			if (threadId != '' && hostId != '' && threadId != null & hostId != null) {
				quickActionModal({
					hostId: hostId,
					threadId: threadId,
					inquiryId: inquiryId,
					page: 'workers'
				});
			}
		});
	}

	function onReimbursementRowClick() {
		$(document).on('click', '#worker-reimburse-table tr td:not(:first-child)', function () {
			var tr = $(this).parents('tr');
			var threadId = $(tr).attr('data-thread-id');
			var hostId = $(tr).attr('data-host-id');
			var inquiryId = $(tr).attr('data-inquiry-id');
			if (threadId != '' && hostId != '' && threadId != null & hostId != null) {
				quickActionModal({
					hostId: hostId,
					threadId: threadId,
					inquiryId: inquiryId,
					page: 'workers'
				});
			}
		});
	}

	function loadAssignmentReportTable() {
		tableAssignedWorkerReports = $('#worker-assignment-report-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Worker/GetWorkerAssignmentReport",
				"type": 'GET',
				"async": true,
				"data": function () {
					return {
						workerIds: $('#hidden-worker-filter').val(),
						propertyIds: $('#hidden-property-filter').val(),
						month: $('#assign-month-filter-inp').val()
					};
				}
			},
			"columns": [
				{ "data": "Checkbox" },
				{ "data": "WorkerName" },
				{ "data": "AssignmentStartDate" },
				{ "data": "AssignmentEndDate" },
				{ "data": "Property" },
				{ "data": "Description" },
				{ "data": "Job" },
				{ "data": "Unit" },
				{ "data": "Rate" },
				{ "data": "Amount" },
				{ "data": "ExpenseStatus" },
				{ "data": "Action" }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).css('cursor', 'pointer');
				$(row).attr('data-inquiry-id', data.InquiryId);
				$(row).attr('data-host-id', data.HostId);
				$(row).attr('data-thread-id', data.ThreadId);
				$(row).attr('assignment-id', data.AssignmentId);

			},
			"footerCallback": function (row, data, start, end, display) {
				var api = this.api(),
					intVal = function (i) {
						return typeof i === 'string' ?
							i.replace(/[, Rs]|(\.\d{2})/g, "") * 1 :
							typeof i === 'number' ?
								i : 0;
					},
					total2 = api
						.column(8)
						.data()
						.reduce(function (a, b) {
							return intVal(a) + intVal(b);
						}, 0);

				$(api.column(9).footer()).html(
					'Total : ' + total2.toFixed(2)
				);
				assignmentTotal = total2;
				GetReportTotal();
			}
		});
	}

	function OnChangeMMS() {
		$(document).on('change', '#mms-pic', function (e) {
			$('#mms-image-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#mms-image-upload').append('<img class="zoom" src="' + imageUrl + '">');
			}
		});
	}
	function onUploadImage() {
		$(document).on('click', '#worker-add-image', function (e) {
			$('#mms-pic').click();
		});
	}
	function loadReimbursementReportTable() {
		tableReimburseReport = $('#worker-reimburse-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Worker/GetWorkerReimbursementReport",
				"type": 'GET',
				"async": true,

				"data": function () {
					return {
						workerIds: $('#hidden-worker-filter').val(),
						propertyIds: $('#hidden-property-filter').val(),
						month: $("#assign-month-filter-inp").val()
					}
				}
			},
			"columns": [
				{ "data": "Checkbox" },
				{ "data": "Name" },
				{ "data": "Note" },
				{ "data": "Amount" },
				{ "data": "Date" },
				{ "data": "ExpenseStatus" }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).css('cursor', 'pointer');
				$(row).attr('data-inquiry-id', data.InquiryId);
				$(row).attr('data-host-id', data.HostId);
				$(row).attr('data-thread-id', data.ThreadId);
				$(row).attr('reimbursement-id', data.ReimbursementId);

			},
			"footerCallback": function (row, data, start, end, display) {
				var api = this.api(),
					intVal = function (i) {
						return typeof i === 'string' ?
							i.replace(/[, Rs]|(\.\d{2})/g, "") * 1 :
							typeof i === 'number' ?
								i : 0;
					},
					total2 = api
						.column(3)
						.data()
						.reduce(function (a, b) {
							return intVal(a) + intVal(b);
						}, 0);

				$(api.column(3).footer()).html(
					'Total : ' + total2.toFixed(2)
				);
				reimbursementTotal = total2;
				GetReportTotal();
			}
		});
	}
	function RetainerFixedGenerateExpense() {
		$(document).on('click', '#retainer-fixed-btn', function () {
			var table = $('#worker-retainer-monthly-table');
			var Expenses = $('#worker-retainer-monthly-table').tableToJSON();
			if (Expenses.length > 0) {
				$.ajax({
					type: "POST",
					url: "/Worker/RetainerFixedGenerateExpense",
					data: { expenses: JSON.stringify(Expenses) },
					dataType: "json",
					success: function (data) {
						if (data.success) {
							$('#retainer-fixed-btn').text('Regenerate Expenses');
							swal("Generate expense  successfully.", "", "success");

						} else {
							swal("No expense", "", "warning");
						}
					},
					error: function (error) {
						swal("Error connecting to server.", "", "error");

					}
				})
			}

		});
		return;
	}

	function loadRetainerFixedReportTable() {

		tableFixedRetainerReport = $('#worker-retainer-monthly-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Worker/GetWorkerRetainerFixedReport",
				"type": 'GET',
				"async": true,
				"data": function () {
					return {
						workerIds: $('#hidden-worker-filter').val(),
						month: $("#assign-month-filter-inp").val()
					}
				}
			},
			"columns": [
				{ "data": "Checkbox" },
				{ "data": "Name" },
				{ "data": "Amount" },
				{ "data": "Date" },
				{ "data": "Type" },
				{ "data": "ExpenseStatus" },
				//{ "data": "HasExpense", visible: false }


			],
			"footerCallback": function (row, data, start, end, display) {
				var api = this.api(),
					intVal = function (i) {
						return typeof i === 'string' ?
							i.replace(/[, Rs]|(\.\d{2})/g, "") * 1 :
							typeof i === 'number' ?
								i : 0;
					},
					total2 = api
						.column(2)
						.data()
						.reduce(function (a, b) {
							return intVal(a) + intVal(b);
						}, 0);

				$(api.column(2).footer()).html(
					'Total : ' + total2.toFixed(2)
				);
				fixedRetainerTotal = total2;
				GetReportTotal();
				if (data.find(x => x.HasExpense == true) != null) {
					$("#retainer-fixed-btn").text("Regenerate Expenses");
				}
				else {
					$("#retainer-fixed-btn").text("Generate Expenses");
				}
			}
		});
	}
	function AddJobType() {
		$(document).on('click', '.add-worker-job-type-btn', function () {
			//var count = $('#job-type-wrapper').children().length;

			var field = '<div class="fields">' +
				'<div class="five wide field">' +
				'<label>Job Type</label>' +
				'<select class="ui dropdown" name="WorkerJobTypeId[]">' +
				($("meta[name=job_type_list_for_select]").attr('content')) +
				'</select>' +
				'</div>' +
				'<div class="four wide field">' +
				'<label>Payment Type</label>' +
				'<select name="PaymentType[]" class="ui search dropdown">' +
				'<option value="">Select Payment Type</option>' +
				'<option value="1">Hourly</option>' +
				'<option value="2">Daily</option>' +
				'<option value="3">Per Appointment</option>' +
				'</select>' +
				'</div>' +
				'<div class="three wide field">' +
				'<label>Payment Rate</label>' +
				'<input name="PaymentRate[]" type="text" placeholder="Rate">' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon orange mini button remove-worker-job-type-btn" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
				'</button>' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon blue mini button add-worker-job-type-btn" data-tooltip="Add">' +
				'<i class="plus icon"></i>' +
				'</button>' +
				'</div>' +
				'</div>';

			//if (count <= 3) {
			$('#job-type-wrapper').append(field);
			activateDropdown();
			//}
		});
	}
	function AddContact() {
		$(document).on('click', '.add-contact', function () {
			var fields = '<div class="fields">' +
				'<div class="five wide field">' +
				'<label>Phone Number</label>' +
				'<input type="text" name="Mobile[]" id="Mobile" placeholder="Phone Number"/>' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon blue mini button add-contact" data-tooltip="Add Contact Number">' +
				'<i class="plus icon"></i>' +
				'</button>' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon orange mini button remove-contact" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
				'</button>' +
				'</div>' +
				'</div>';
			$('#contact-div').append(fields);
		});
	}
	function AddRetainer() {
		$(document).on('click', '.add-retainer', function () {
			var fields = '<div class="fields">' +
				'<div class="three wide field">' +
				'<label>Retainer Rate</label>' +
				'<input  name="RetainerRate[]" type="text" placeholder="Amount" />' +
				'</div>' +
				'<div class="three wide field">' +
				'<label>Frequency</label>' +
				'<select  name="RetainerFrequency[]" type="text" placeholder="Frequency">' +
				'<option value="1">Monthly</option>' +
				'<option value="2">Weekly</option>' +
				'<option value="3">Daily</option>' +
				'</select>' +
				'</div>' +
				'<div class="field">' +
				'<label>Effective Date</label>' +
				'<div id="effective-date" class="ui calendar month">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input  name="EffectiveDate[]" type="text" placeholder="Select Month" />' +
				'</div>' +
				'</div>' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon blue mini button add-retainer" data-tooltip="Add retainer">' +
				'<i class="plus icon"></i>' +
				'</button>' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon orange mini button remove-retainer" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
				'</button>' +
				'</div>' +
				'</div>';
			$('#retainer-div').append(fields);
			$('.ui.month').calendar({
				type: 'month'
			});
		});
	}


	function AddJobTypeEditForm() {
		$(document).on('click', '.add-worker-job-type-edit-form-btn', function () {
			//var count = $('#job-type-edit-form-wrapper').children().length;

			var field = '<div class="fields">' +
				'<div class="five wide field">' +
				'<label>Job Type</label>' +
				'<select class="ui dropdown" name="WorkerJobTypeId[]">' +
				($("meta[name=job_type_list_for_select]").attr('content')) +
				'</select>' +
				'</div>' +
				'<div class="four wide field">' +
				'<label>Payment Type</label>' +
				'<select name="PaymentType[]" class="ui search dropdown">' +
				'<option value="">Select Payment Type</option>' +
				'<option value="1">Hourly</option>' +
				'<option value="2">Daily</option>' +
				'<option value="3">Per Appointment</option>' +
				'</select>' +
				'</div>' +
				'<div class="three wide field">' +
				'<label>Payment Rate</label>' +
				'<input name="PaymentRate[]" type="text" placeholder="Rate">' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon orange mini button remove-worker-job-type-btn" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
				'</button>' +
				'</div>' +
				'<div class="one wide field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon blue mini button add-worker-job-type-edit-form-btn" data-tooltip="Add">' +
				'<i class="plus icon"></i>' +
				'</button>' +
				'</div>' +
				'</div>';

			//if (count <= 3) {
			$('#job-type-edit-form-wrapper').append(field);
			activateDropdown();
			//}
		});
	}

	function RemoveJobType() {
		$(document).on('click', '.remove-worker-job-type-btn', function () {
			$(this).parents('.fields').remove();
		});
	}
	function RemoveContact() {
		$(document).on('click', '.remove-contact', function () {
			$(this).parents('.fields').remove();
		});
	}
	function RemoveRetainer() {
		$(document).on('click', '.remove-retainer', function () {
			$(this).parents('.fields').remove();
		});
	}

	function onHeaderSearch() {
		$('#js-btn_menu-search').dropdown({
			action: 'nothing'
		});
	}

	function onAssignWorker() {
		$('.js-btn_assign-worker').on('click', function () {
			$('.assign-worker-modal').modal('show');
			$('.ui.calendar').calendar({
				type: 'date'
			});
		});
	}

	$(document).on('click', '.edit-schedule', function (e) {
		$('#edit-schedule-form').removeClass('hidden');
		$('#create-schedule-button').css('display', 'none');
		$.ajax({
			type: "GET",
			url: "/Home/EditSchedule/",
			data: { intScheduleId: $(this).attr('name') },
			success: function (html) {
				$("#edit-schedule-form").html(html);
				setupSemanticUI();
				EditSchedule();
			}
		});
	});

	$(document).on('click', '.cancel-add-schedule', function (e) {
		$('#schedule-form').addClass('hidden');
		$('#create-schedule-button').css('display', '');
	});

	$(document).on('click', '#create-schedule-button', function (e) {
		$('#create-schedule-button').css('display', 'none');
		$('#schedule-form').removeClass('hidden');
	});

	$(document).on('click', '#btn_cancelSchedule', function (e) {
		$('#edit-schedule-form').addClass('hidden');
		$('#create-schedule-button').css('display', '');
	});

	$(document).on('change', '#commission-property', function (e) {
		var commission = $('#commission-property').val();
		if (commission == 3 || commission == 4) {
			$('#commission-amount').attr('placeholder', 'Percent');
		}
		else {
			$('#commission-amount').attr('placeholder', 'Amount');
		}
	});
});

function GetWorkerList() {
	tableWorker = $('#table_workerList').DataTable({
		"aaSorting": [],
		"responsive": true,
		"bAutoWidth": false,
		"bLengthChange": false,
		"searching": true,
		"search": { "caseInsensitive": true },
		"ajax": {
			"url": "/Home/GetWorkers",
			"type": 'GET',
			"async": true,

			"data": function () {
				return {
					WorkerTypeId: $("#select_Search").val(), search: $("#txtWorkerName").val()
				}
			}
		},
		"columns": [
			{ "data": "Firstname" },
			{ "data": "Lastname" },
			{ "data": "Mobile" },
			{ "data": "Action" }
		]
		,
		"footerCallback": function (row, data, start, end, display) {

			onEditNewWorker();
			DeleteWorker();
			AvailabilityWorker();
			onCreateWorkerSchedule();
		}
	});
}

function GetUnhiredWorkerList() {


	tablehireWorker = $('#table_hireWorkerList').DataTable({
		"aaSorting": [],
		"responsive": true,
		"bAutoWidth": false,
		"bLengthChange": false,
		"searching": true,
		"search": { "caseInsensitive": true },
		"ajax": {
			"url": "/Worker/GetUnhiredWorkers",
			"type": 'GET',
			"async": true

			//"data": function () {
			//	return {
			//		WorkerTypeId: $("#select_Search").val(), search: $("#txtWorkerName").val()
			//	}
			//}
		},
		"columns": [
			{ "data": "Firstname" },
			{ "data": "Lastname" },
			{ "data": "Mobile" },
			{ "data": "Action" }
		]
		,
		"footerCallback": function (row, data, start, end, display) {

			onHireWorker();
		}
	});
}

function changeStatus() {
	if ($("#hdnchangeStatus").val() == "false") {
		$("#hdnchangeStatus").val("true");
	}
	else {
		$("#hdnchangeStatus").val("false");
	}
}

function onCreateNewWorker() {
	$('.js-btn_create-new-worker').on('click', function () {
		$.ajax({
			type: "GET",
			url: "/Home/AddWorker/",
			success: function (html) {
				$("#partialAddWorker").html(html);
				onChangeAddWorker();
				$('.create-new-worker-modal').modal('setting', {}).modal('show');
				$('.ui.dropdown').dropdown();
				$('.ui.month').calendar({
					type: 'month'
				});
			}
		});
	});
}

function onSearchButton() {
	$('#btn_SearchWorker').on('click', function () {
		tableWorker.ajax.reload(null, false);
	});
}
function Validate(obj) {
	var data = obj.val();
	if (data.length == 0) {
		obj.next().remove();
		obj.after('<div class="ui basic red pointing prompt label transition visible">Please fill out this field</div>');
	}
}

function AddWorker() {
	$('#btn_AddWorker').on('click', function () {
		$("#txtAddress0").val($("#txtAddress0").val() + " " + $("#txtAddress1").val() + " " + $("#txtAddress2").val());
		$('#form_AddWorker').form({
			fields: {
				username: {
					identifier: 'Username',
					rules: [{
						type: 'empty',
						prompt: '{name} is required'
					}]
				},
				password: {
					identifier: 'Password',
					rules: [{
						type: 'empty',
						prompt: '{name} is required'
					}]
				},
				firstName: {
					identifier: 'Firstname',
					rules: [{
						type: 'empty',
						prompt: '{name} is required'
					}]
				},
				lastName: {
					identifier: 'Lastname',
					rules: [{
						type: 'empty',
						prompt: '{name} is required'
					}]
				},
				workerType: {
					identifier: 'worker-job-type-id',
					rules: [{
						type: 'empty',
						prompt: 'Worker type is required'
					}]
				},
				paymentFrequency: {
					identifier: 'payment-type',
					rules: [{
						type: 'empty',
						prompt: 'Payment frequency is required'
					}]
				},
				paymentRate: {
					identifier: 'payment-rate',
					rules: [{
						type: 'empty',
						prompt: 'Payment rate is required'
					}]
				},
				fixedRate: {
					identifier: 'fixed-rate',
				},
				frequency: {
					identifier: 'frequency-rate'

				},
				retainerRate: {
					identifier: 'retainer-rate'
				},
				retainerFrequency: {
					identifier: 'retainer-frequency'
				}

			},
			inline: true,
			on: 'blur',
			required: true
		});
		Validate($('#Username'));
		Validate($('#Password'));
		Validate($('#Firstname'));
		Validate($('#Lastname'));
		if ($('#form_AddWorker').form('is valid')) {

			$("#form_AddWorker input[name=worker_job_id]").val($('#form_AddWorker select[name="WorkerJobTypeId[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_AddWorker input[name=payment_type]").val($('#form_AddWorker select[name="PaymentType[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_AddWorker input[name=payment_rate]").val($('#form_AddWorker input[name="PaymentRate[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_AddWorker input[name=contactNumber]").val($('#form_AddWorker input[name="Mobile[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_AddWorker input[name=retainer_rate]").val($('#form_AddWorker input[name="RetainerRate[]"]').map(function () {
				return this.value;
			}).get().join());
			$("#form_AddWorker input[name=retainer_frequency]").val($('#form_AddWorker input[name="RetainerFrequency[]"]').map(function () {
				return this.value;
			}).get().join());
			$("#form_AddWorker input[name=retainer_date]").val($('#form_AddWorker input[name="EffectiveDate[]"]').map(function () {
				return this.value;
			}).get().join());

			var formData = new FormData($("#form_AddWorker")[0]);
			formData.append("ImageUrl", $('#form_AddWorker input[name="profile-pic"]').get(0).files[0]);

			//formData.append($("#form_AddWorker").serialize());
			$.ajax({
				type: "POST",
				url: "/Home/AddWorker/",
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					if (data.success) {
						tableWorker.ajax.reload(null, false);
						$('.create-new-worker-modal').modal('hide');
						swal("New worker has been added .", "", "success");
					}
					else {
						swal("Error in adding worker.", "", "error");
					}
				}
			});
		}
	});
}

function onChangeAddWorker() {
	$('#selectAddPayment').on('change', function () {
		$("#txtPaymentRate").removeAttr("name")
		$('#txtPaymentRate').attr('name', $(this).val());
		var paymentType = $('#selectAddPayment').val();
		if (paymentType == "PayHour") { $('#active-payment').val(1); $('#active-payment-edit').val(1); }
		else if (paymentType == "PayDay") { $('#active-payment').val(2); $('#active-payment-edit').val(2); }
		else if (paymentType == "PayAppoint") { $('#active-payment').val(3); $('#active-payment-edit').val(3); }
		//else if (paymentType == "PayFixed") { $('#active-payment').val(4); $('#active-payment-edit').val(4); }
		else { $('#active-payment').val(0); }
	});
}

function onHireWorker() {
	$(document).on('click', '.js-hired-worker', function () {
		//$('.js-btn_edit-new-worker').on('click', function () {

		workerId = $(this).attr('name');
		$.ajax({
			type: "POST",
			url: "/Worker/HireWorker/",
			data: { id: $(this).attr('name') },
			success: function (data) {
				if (data.success) {
					tablehireWorker.ajax.reload(null, false);
					swal("Worker hired.", "", "success");
				}
			}
		});
	});
}

function onEditNewWorker() {
	$(document).on('click', '.js-btn_edit-new-worker', function () {
		//$('.js-btn_edit-new-worker').on('click', function () {

		workerId = $(this).attr('name');
		$.ajax({
			type: "GET",
			url: "/Home/EditWorker/",
			data: { intWorkerId: $(this).attr('name') },
			success: function (html) {
				$("#partialEditWorker").html(html);
				onChangeEditWorker();
				$('.edit-new-worker-modal').modal('setting', {}).modal('show');
				$('.ui.dropdown').dropdown();
				$('.ui.month').calendar({
					type: 'month'
				});
			}
		});
	});
}


function EditWorker() {
	$('#btn_EditWorker').on('click', function () {
		$("#txtAddress0").val($("#txtAddress0").val() + " " + $("#txtAddress1").val() + " " + $("#txtAddress2").val());

		$('#form_EditWorker').form({
			fields: {
				firstName: {
					identifier: 'Firstname',
					rules: [{
						type: 'empty',
						prompt: '{name} is required'
					}]
				},
				lastName: {
					identifier: 'Lastname',
					rules: [{
						type: 'empty',
						prompt: '{name} is required'
					}]
				},
				workerType: {
					identifier: 'WorkerTypeId',
					rules: [{
						type: 'empty',
						prompt: 'Worker type is required'
					}]
				},
				paymentFrequency: {
					identifier: 'selectEditPayment',
					rules: [{
						type: 'empty',
						prompt: 'Payment frequency is required'
					}]
				},
				paymentRate: {
					identifier: 'txtEditPaymentRate',
					rules: [{
						type: 'empty',
						prompt: 'Payment rate is required'
					}]
				},
				fixedRate: {
					identifier: 'fixed-rate',

				},
				frequency: {
					identifier: 'frequency-rate',

				},
				retainerRate: {
					identifier: 'retainer-rate',
				},
				retainerFrequency: {
					identifier: 'retainer-frequency',
				},
			},
			inline: true,
			on: 'blur'
		});

		if ($('#form_EditWorker').form('is valid')) {

			$("#form_EditWorker input[name=worker_job_id]").val($('#form_EditWorker select[name="WorkerJobTypeId[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_EditWorker input[name=payment_type]").val($('#form_EditWorker select[name="PaymentType[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_EditWorker input[name=payment_rate]").val($('#form_EditWorker input[name="PaymentRate[]"]').map(function () {
				return this.value;
			}).get().join());
			$("#form_EditWorker input[name=contactNumber]").val($('#form_EditWorker input[name="Mobile[]"]').map(function () {
				return this.value;
			}).get().join());


			$("#form_EditWorker input[name=retainer_rate]").val($('#form_EditWorker input[name="RetainerRate[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_EditWorker input[name=retainer_frequency]").val($('#form_EditWorker select[name="RetainerFrequency[]"]').map(function () {
				return this.value;
			}).get().join());

			$("#form_EditWorker input[name=retainer_date]").val($('#form_EditWorker input[name="EffectiveDate[]"]').map(function () {
				return this.value;
			}).get().join());

			var formData = new FormData($("#form_EditWorker")[0]);
			formData.append("ImageUrl", $('#form_EditWorker input[name="profile-pic"]').get(0).files[0]);

			$.ajax({
				type: "POST",
				url: "/Home/EditWorker/",
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					if (data.success) {
						tableWorker.ajax.reload(null, false);
						$('.edit-new-worker-modal').modal('hide');
						swal("Changes has been saved .", "", "success");
					}
					else {
						swal("Error on edit worker.", "", "error");
					}
				}
			});
		}
	});
}

function onChangeEditWorker() {
	$('#selectEditPayment').on('change', function () {
		$("#txtEditPaymentRate").removeAttr("name")
		$('#txtEditPaymentRate').attr('name', $(this).val());
		var paymentType = $('#selectEditPayment').val();
		if (paymentType == "PayHour") { $('#active-payment-edit').val(1); }
		else if (paymentType == "PayDay") { $('#active-payment-edit').val(2); }
		else if (paymentType == "PayAppoint") { $('#active-payment-edit').val(3); }
		//else if (paymentType == "PayFixed") { $('#active-payment-edit').val(4); }
		else { $('#active-payment-edit').val(0); }
	});
}

function LoadWorkerAvailability() {

	tableWorkerAvailability = $('#worker-availability-table').DataTable({
		"aaSorting": [],
		"responsive": true,
		"bAutoWidth": false,
		"bLengthChange": false,
		"searching": true,
		"search": { "caseInsensitive": true },
		"ajax": {
			"url": "/Home/GetWorkerAvailability",
			"type": 'GET',
			"async": true,
			"data": function () {
				return {
					workerId: workerId == undefined ? 0 : workerId,

				}
			}
		},
		"columns": [
			{ "data": "StartDate" },
			{ "data": "EndDate" },
			{ "data": "Type" },

		]

	});
}
function AvailabilityWorker() {
	$(document).on('click', '.availability-btn', function () {
		//$('.js-btn_delete-worker-schedule').on('click', function () {
		var isAssigned = false;
		workerId = $(this).attr('name');

		tableWorkerAvailability.ajax.reload(null, false);

		$('#availability-modal').modal('show');



		$('#start-date').calendar();
		$('#end-date').calendar();

	});
}
function AvailabilitySubmit() {
	$(document).on('click', '#availability-submit', function () {

		var start = $('#availability-start-date').val();
		var end = $('#availability-end-date').val();
		var type = $('#availability-type').val()
		if ((start != "" && end != "") && (moment(start).format('MMM D,YYYY') == moment(end).format('MMM D,YYYY'))) {
			$.ajax({
				type: "POST",
				url: "/Home/AddWorkerAvailability/",
				data: {
					workerId: workerId,
					startDate: start,
					endDate: end,
					type: type
				},
				success: function (data) {
					tableWorkerAvailability.ajax.reload(null, false);
				}
			});
		}
		else {
			swal("Warning", "Dates must be in same day!")
		}
	})
}

function DeleteWorker() {
	$(document).on('click', '.js-btn_delete-worker-schedule', function () {
		//$('.js-btn_delete-worker-schedule').on('click', function () {
		var isAssigned = false;
		var workerId = $(this).attr('name');
		$.ajax({
			type: "GET",
			url: "/Home/CheckIfAssigned/",
			data: { workerId: workerId },
			async: false,
			success: function (data) {
				isAssigned = data.isAssigned;
			}
		});

		if (isAssigned) {
			swal("Unable to delete this worker", "The selected worker has been assigned to a task.", "error");
		}
		else {
			swal({
				title: "Are you sure you want to delete this worker ?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: "GET",
						url: "/Home/DeleteWorker/",
						data: { WorkerId: workerId },
						success: function (html) {
							tableWorker.ajax.reload(null, false);

							swal("Worker has been successfully deleted.", "", "success");
						}
					});
				});
		}
	});
}

function GetSchedules() {
	$.ajax({
		type: "GET",
		url: "/Home/GetSchedules/",
		data: { WorkerId: $("#WorkerId").val() },
		success: function (model) {
			$("#tb_ScheduleList").html("");
			$.each(model, function (k, v) {
				var strAvailDay = "";
				var strStart = v.AvailTimeStart.toString().split(',');
				var strEnd = v.AvailTimeEnd.toString().split(',');
				$.each(v.AvailDay.toString().split(','), function (kk, vv) {
					if (strStart[kk] != "" && strEnd[kk] != "") {
						strAvailDay += '<div>' + vv + '- ' + strStart[kk] + ' to ' + strEnd[kk] + '</div>'
					}
				});
				$("#tb_ScheduleList").append('<tr> <td> ' + strAvailDay + ' </td> <td> ' + GetDateDisplay(v.StartDate) + ' </td> <td> ' + GetDateDisplay(v.EndDate) + ' </td> <td> ' + v.Status + ' </td> <td> <a class="ui mini button edit-schedule" name="' + v.Id + '" title="Edit"> <i class="icon edit"></i> </a> <a class="ui mini button delete-schedule" name="' + v.Id + '" title="Delete"> <i class="icon ban"></i> </a> </td> </tr>');
			});
		}
	});
}

function GetDateDisplay(jsonDate) {
	try {
		return moment(jsonDate).tz("America/Vancouver").format('MMM D, YYYY (dddd)');
		//var dateValue = jsonDate.replace("/Date(", "").replace(")/", "");
		//var dateObject = new Date(parseInt(dateValue));

		//return (dateObject.getMonth() + 1) + '/' + dateObject.getDate() + '/' + dateObject.getFullYear() + " " + FormatAMPM(dateObject);
	}
	catch (err) {
		return "";
	}
}
$(document).on('click', '.delete-schedule', function (e) {
	$.ajax({
		type: "GET",
		url: "/Home/DeleteSchedule/",
		data: { ScheduleId: $(this).attr('name') },
		success: function (html) {
			GetSchedules();
		}
	});
});

function AddSchedule() {
	$('#btn_AddSchedule').on('click', function () {
		$.ajax({
			type: "POST",
			url: "/Home/AddSchedule/",
			data: $("#form_AddSchedule").serialize(),
			success: function (html) {
				$('#schedule-form').addClass('hidden');
				$('#create-schedule-button').css('display', 'inline');
				GetSchedules();
				return false;
			}
		});
	});
}

function ActivateCalendar() {
	$('ui.month').calendar({
		type: 'month'
	});
}
function EditSchedule() {
	$('#btn_editSchedule').on('click', function () {
		$.ajax({
			type: "POST",
			url: "/Home/EditSchedule/",
			data: $("#form_EditSchedule").serialize(),
			success: function (html) {
				$('#edit-schedule-form').addClass('hidden');
				$('#create-schedule-button').css('display', 'inline');
				GetSchedules();
				return false;
			}
		});
	});
}

function onCreateWorkerSchedule() {
	$(document).on('click', '.js-btn_create-worker-schedule', function () {
		//$('.js-btn_create-worker-schedule').on('click', function () {
		$('.create-worker-schedule-modal').modal('show');
		$("#WorkerId").val($(this).attr('name'));
		$("#addWorkerIdForSchedule").val($(this).attr('name'));
		GetSchedules();
		$('.ui.calendar').calendar({ type: 'date' });
		$('.ui.calendar.time').calendar({ type: 'time' });
	});
}

function activateDropdown() {
	$('.ui.dropdown').dropdown({ on: 'hover' });
}
