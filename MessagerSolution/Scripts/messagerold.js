﻿
var myHub = $.connection.inboxHub;
$.connection.hub.logging = true;
myHub.client.LockThread = function (lockThreadId, user) {
	var threadId = $('.active-thread').attr('data-thread-id');
	if (lockThreadId == threadId) {
		$('#txtMsgContent').attr("disabled", "disabled");
		$('#btn_Send').attr("disabled", "disabled");
		$('#btn_Clear').attr("disabled", "disabled");
		$('#txtMsgContent').val(user);
	}
};
myHub.client.UnLockThread = function (lockThreadId) {
	var threadId = $('.active-thread').attr('data-thread-id');
	if (lockThreadId == threadId) {
		$('#btn_Send').removeAttr("disabled");
		$('#btn_Clear').removeAttr("disabled");
		$('#txtMsgContent').removeAttr("disabled");
		$('#txtMsgContent').val('');	
	}
};

myHub.client.MessageReceived = function (inboxes) {
	var threadId = $('.active-thread').attr('data-thread-id');
	
	$.each(inboxes, function (key, value) {

		//$.each(value, function (key, value) {
		//	alert(value);
		//});
			var clock = '';
		var date1 = moment(value.LastMessageAt);
		var date2 = moment();
		differenceInMs = date2.diff(date1); // diff yields milliseconds
		duration = moment.duration(differenceInMs); // moment.duration accepts ms
		differenceInMinutes = duration.asMinutes();
		if (differenceInMinutes <= 30 && value.IsIncoming) {
			clock = '<div class="message-indicator ui wait icon green right floated"></div>';
		}
		else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && value.IsIncoming) {
			clock = '<div class="message-indicator ui wait icon yellow rightfloated"></div>';
		}
		else if (differenceInMinutes > 60 && value.IsIncoming) {
			clock = '<div class="message-indicator ui wait icon red right floated"></div>';
		}
		var html = '<div class="item message-thread" data-host-id="' + value.HostAccountId + '" data-thread-id="' + value.ThreadId + '" data-guest-id="' + value.GuestId + '" data-site-type="' + value.SiteType + '" data-reservation-id="">' +
			'<div class="avatar">' +
			'<img src="' + value.GuestProfilePictureUrl + '" height="40" width="40">' +
			'</div>' +
			'<div class="content">' +
			'<a class="author">' + value.GuestName + '</a>' +
			clock +
			'<br>' +
			'<div class="metadata">' +
			'<div class="date">' + (moment(value.LastMessageAt).isSame(Date.now(), 'day') ? moment(value.LastMessageAt).format("h:mm A") : moment(value.LastMessageAt).format("MMM D, YYYY")) + '</div>' +
			'</div>' +
			'<div class="text">' + (value.LastMessage.length > 15 ? (value.LastMessage.substr(0, 14) + '...') : value.LastMessage) + '</div>' +
			'</div>' +
			'</div>';
		$('.message-thread[data-thread-id="' + value.ThreadId + '"]').remove();
		$('#inbox-list').prepend(html);

		if (value.ThreadId == $('#inbox-list').attr('data-active-thread-id')) {
			var inboxItem = $('.message-thread[data-thread-id="' + $('#inbox-list').attr('data-active-thread-id') + '"]');
			inboxItem.addClass('active-thread');
			inboxItem.css('border-left', '3px solid #00b5ad');
			inboxItem.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
		}

		if (threadId == value.ThreadId) {
			loadConversation(value.Messages);
		}

	});
};

myHub.client.WorkerMessageReceived = function (inbox) {
	var threadId = $('.worker-active-thread').attr('data-thread-id');
	var thread = inbox.Thread;

	var clock = '';
	var date1 = moment(thread.LastMessageAt);
	var date2 = moment();
	differenceInMs = date2.diff(date1);
	duration = moment.duration(differenceInMs);
	differenceInMinutes = duration.asMinutes();
	var html = '<div class="item worker-message-thread" data-thread-id="' + thread.ThreadId + '" data-worker-id="' + thread.WorkerId + '">' +
		'<div class="avatar">' +
		'<img src="' + thread.ImageUrl + '" height="40" width="40">' +
		'</div>' +
		'<div class="content">' +
		'<a class="author">' + thread.Name + '</a>' +
		'<br>' +
		'<div class="metadata">' +
		'<div class="date">' + (thread.LastMessageAt != null?(moment(thread.LastMessageAt).isSame(Date.now(), 'day') ? moment(thread.LastMessageAt).format("h:mm A") : moment(thread.LastMessageAt).format("MMM D, YYYY")):'') + '</div>' +
		'</div>' +
		'<div class="text">' + (thread.LastMessage.length > 15 ? (thread.LastMessage.substr(0, 14) + '...') : thread.LastMessage) + '</div>' +
		'</div>' +
		'</div>';
	$('.worker-message-thread[data-thread-id="' + thread.ThreadId + '"]').remove();
	$('#worker-inbox-list').prepend(html);
	//alert(threadId + " " + thread.ThreadId);

	if (thread.ThreadId == $('#worker-inbox-list').attr('data-active-thread-id')) {
		var inboxItem = $('.worker-message-thread[data-thread-id="' + $('#worker-inbox-list').attr('data-active-thread-id') + '"]');
		inboxItem.addClass('worker-active-thread');
		inboxItem.css('border-left', '3px solid #00b5ad');
		inboxItem.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
	}	
	if (threadId == thread.ThreadId) {
		
		loadWorkerConversation(inbox.Messages);
		
	}
};

function loadWorkerConversation(messages) {
	$("#worker-dv_Conversation").html(""); 
	$.each(messages, function (index, value) {

		if (!value.IsMyMessage) {
			if (value.Type == 2) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>'+

						(
						value.RecordingUrl == null ?
							'' :
							(
								'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
							)
						) +
					'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
					+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 1) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui orange small left pointing label message-label message-incoming">' +
					'<h5>' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 3) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
					'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 4) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
					'<h5>' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
					'</div>');
			} else {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
					'<h5>' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
			}
		}
		else {
			if (value.Type == 2) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
					(
						value.RecordingUrl == null ?
							'' :
							(
								'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
							)
					) +
					'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
					+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 1) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui orange small right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 3) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 4) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
					'</div>');
			} else {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
			}
		}   

	});
	$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
}
myHub.client.MessageSent = function (threadId, messages) {
	loadConversation(messages);
};

function loadConversation(messages) {
	$("#dv_Conversation").html("");
	$.each(messages, function (index, value) {
		
		if (!value.IsMyMessage) {
			$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
				'<h5>' + value.MessageContent + '</h5>' +
				'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
				'</div>');
		}
		else {
			$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
				'<h5>' + value.MessageContent + '</h5>' +
				'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
				'</div>');
		}

	});
	$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
}

$.connection.hub.start();
$(document).ready(function () {
	var currentStartDate;
	var currentEndDate;
	//$("#alter-checkin")
	$('#alter-checkIn').calendar({
		type: 'date', onChange: function (date, text, mode) {
			$("#alter-booking-form input[name=checkIn]").val(text);
			checkAlterAvailability();
			//checkSpecialOffer();
			//checkAlter();
		}
	});

	$('#alter-checkOut').calendar({
		type: 'date', onChange: function (date, text, mode) {
			$("#alter-booking-form input[name=checkOut]").val(text);
			//alert("Checkout");
			checkAlterAvailability();
			//checkSpecialOffer();
			//checkAlter();
		}
	});

	$('.ui.accordion').accordion();
	$('.ui.dropdown').dropdown({ on: 'hover' });
	var code = "";
	var hostid = "";
	var firstRun = true;
	var tableTemplates;
	var inqId = "";
	var skipNum = 0;
	//getInquiry(code);
	loadInbox();
	loadWorkerInbox();
	onAddNote();
	onDeleteNote();
	onSaveNote();
	onSelectedNoteChange();
	onInboxClick();
	onWorkerInboxClick();
	onButtonSendMessageClick();
	onWorkerSendMessageClick();
	//onButtonSendSMSMessageClick();
	onButtonCallClick();
	//setTimeout(function () { checkNewHostMessages(); }, 10000);
	onDeclineBooking();
	onAcceptBooking();
	onAlterBooking();
	onSubmitAlterBooking();
	onCancelAlterationBooking();
	onSaveTemplate();
	getTemplateMessage();
	onTemplateMessageSelect();
	onAddTemplate();
	onBackToTemplateList();
	loadTemplateTableData();
	refreshTemplateTableData();
	onEditTemplate();
	onDeleteTemplate();
	onTemplateFormSubmit();
	onSelectVariable();
	onSendSpecialOfferClick();
	onSendSpecialOffer();
	onSpecialOfferSubmit();
	onWithdrawSpecialOffer();
	PreApproveClick();
	WithdrawPreApproveClick();
	DeclineInquiryClick();
	AcceptBookingClick();
	DeclineBookingClick();
	CancelBookingClick();
	CancelBookingClick();
	CancelAlterationBookingClick();
	onDeleteMessageSMSClick();
	onDeleteMessageCallClick();
	onReplyMessageSMSClick();
	//alterBooking();
	onCheckReservationAvailability();
	onAlterCheck();
	checkAlterAvailability();

	onVrboAlterBooking();
	onSubmitAlterBookingVrbo();
	onAddMessageRulesModal();
	onAssignMessagePhoneModal();
	onMessageSMSRefresh();

	loadMessageRuleTableData();
	loadMessageSMSTableData();
	onMessageRulesFilter();
	onEditMessageRules();
	onDeleteMessageRules();
	onTripClick();
	SetForwardingClick();
	SaveForwardingNumber();
	AddContact();
	saveContact();
	ontxtMsgContentClick();
	ontxtMsgContentFocusOut();
	onMessageEndSubmit();
	onButtonSendSMSClick();
	OnChangeMMS();
	onUploadImage();
	RenterInbox();
	//JM 08/14/20 Renter Messages Event
	function RenterInbox() {
		loadRenterInbox();
		onRenterInboxClick();
		//onRenterSendMessageClick();
		onButtonSendRenterSMSClick();
		OnChangeRenterMMS();
		onUploadRenterImage();
	}

	function OnChangeRenterMMS() {
		$(document).on('change', '#renter-mms-pic', function (e) {
			$('#renter-mms-image-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#renter-mms-image-upload').append('<img class="zoom" src="' + imageUrl + '">');
			}
		});
	}
	function onUploadRenterImage() {
		$(document).on('click', '#renter-add-mms', function (e) {
			$('#renter-mms-pic').click();
		});
	}
	function onButtonSendRenterSMSClick() {
		$(document).on('click', '#renter-send-btn-sms', function () {
			$('#renter-send-btn-sms').attr('disabled', true);
			var contact = $('#renter-contact-info').find('.checked');
			let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
			let type = $(contact).parents(".field").attr('data-type');
			var renterId = $('.renter-active-thread').attr('data-id');
			var threadId = $('.renter-active-thread').attr('data-thread-id');
			var formData = new FormData();
			for (var i = 0; i < $('#renter-mms-pic').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#renter-mms-pic').get(0).files[i]);
			}
			formData.append("to", phoneNumber);
			formData.append("message", $('#renter-txtMsgContent').val());
			formData.append("type", type);
			formData.append("renterId", renterId);
			formData.append("threadId", threadId);
			$.ajax({
				type: 'POST',
				url: "/SMS/RenterSend",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#renter-send-btn-sms').attr('disabled', false);
						$('#renter-txtMsgContent').val('');
						$('.renter-active-thread').attr('data-thread-id', data.threadId);
						$('.renter-active-thread').click();
						$('#renter-inbox-list').attr('data-active-thread-id', data.threadId);
						$('#renter-mms-image-upload').empty();
						$('#renter-mms-pic').val('');
						toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						//$('.item.worker-message-thread.worker-active-thread').click();
					} else {
						toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});

		});
	}
	$(document).on('click', '#renter-end-btn', function () {
		var threadId = $('.renter-active-thread').attr('data-thread-id');
		$.ajax({
			type: 'POST',
			url: "/Messages/EndCommunicationThread",
			data: {
				threadId: threadId
			},
			success: function (data) {
				swal("This thread mark as end!", "", "success");
				$('.active-thread').children('.content').children('i').remove();
			}
		});
	});
	$(document).on('click', '.button.renter-sms-button', function () {
		let phoneNumber = $(this).parents(".field").attr('data-phonenumber');
		let type = $(this).parents(".field").attr('data-type');
		var renterId = $('.renter-active-thread').attr('data-id');
		var threadId = $('.renter-active-thread').attr('data-thread-id');

		$('.send-sms-modal [name="phone-number"]').val(phoneNumber);
		$('.send-sms-modal [name="phone-number"]').attr('disabled', true);
		$('.send-sms-modal [name="message"]').val('');

		$('.send-sms-modal.modal')
			.modal({
				closable: true,
				onApprove: function () {
					$.ajax({
						type: 'POST',
						url: "/SMS/RenterSend",
						data: {
							to: phoneNumber,
							message: $('.send-sms-modal [name="message"]').val(),
							type: type,
							threadId: threadId,
							renterId: renterId
						},
						dataType: "json",
						success: function (data) {
							if (data.success) {

								$('.renter-active-thread').attr('data-thread-id', data.threadId);
								$('.renter-active-thread').click();
								$('#renter-inbox-list').attr('data-active-thread-id', data.threadId);
								toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
								//$('.item.worker-message-thread.worker-active-thread').click();
							} else {
								toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
							}
						},
						error: function (error) {
							toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						}
					});
				}
			})
			.modal('show').modal('refresh');

	});

	$(document).on('click', '.button.renter-call-button', function () {
		let phoneNumber = $(this).parents(".field").attr('data-phonenumber');

		var threadId = $('.renter-active-thread').attr('data-thread-id');
		var renterId = $('.renter-active-thread').attr('data-id');
		$.ajax({
			type: 'GET',
			url: "/Rentals/RenterThreadInfo",
			data: { threadId: threadId, renterId: renterId },
			dataType: "json",
			beforeSend: function () {
				$(this).addClass('loading');
			},
			success: function (data) {
				$(this).removeClass('loading');
				var params = {
					From: userId,
					To: phoneNumber
				};
				if (device) {
					$('.mini.modal .phone-number').text(phoneNumber);
					$('.mini.modal .header.name').text(data.renter.Name);
					$('.mini.modal .avatar.image').attr('src', '/Images/Worker/default-image.png');
					$('.mini.modal .avatar.image').show();
					device.connect(params);
					openOutgoingCallModal();
				}
			}
		});
	});
	function onRenterInboxClick() {
		//get the message and details of inquiry
		$(document).on('click', '.renter-message-thread', function () {
			var messageThread = $(this);
			$('#renter-inbox-list').children().css('border', 'none');
			$('#renter-inbox-list').children().css('background-color', '#fff', '!important');
			messageThread.css('border-left', '3px solid #00b5ad');
			messageThread.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
			var id = messageThread.attr('data-thread-id');
			var renterId = messageThread.attr('data-id');
			$('#renter-inbox-list').attr('data-active-thread-id', id);
			$("#renter-dv_Conversation").html('');
			$("#renter-img_Loader").show();

			$.ajax({
				type: 'POST',
				url: "/Rentals/LoadRenterInboxMessage",
				data: { threadId: id, renterId: renterId },
				dataType: "json",
				async: true,
				success: function (data) {
					$("#renter-img_Loader").hide();
					if (data.success) {
						//display the conversation
						if (data.Messages.length != 0) {
							//$('#dvConvo').html('');
							console.log(data.Messages);
							$.each(data.Messages, function (index, value) {
								var imageHtml = '<div class="ui tiny images">';
								if (value.Images != null) {
									for (var x = 0; x < value.Images.length; x++) {
										imageHtml += '<img class="zoom" src="' + value.Images[x] + '">';
									}
								}
								imageHtml += '</div>';
								if (!value.IsMyMessage) {
									if (value.Type == 2) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' +

											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui orange small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
								else {
									if (value.Type == 2) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui orange small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
							});
						}
						else {
							var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
							$('#dv_Conversation').append(html);
						}
						$('#renter-contact-info').empty();
						$.each(data.contactInfos, function (index, contactInfo) {
							let buttons =
								`<button class="circular ui green icon button renter-call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
                                <button class="circular ui orange icon button renter-sms-button" data-tooltip="SMS">
									<i class="icon comments"></i>
								</button>`;
							$('#renter-contact-info').append(`
								<div class="field" data-phonenumber="${contactInfo}" data-type="1" >
									<div class="ui radio checkbox checked">
									<input type="radio" name="contact" contact-id="${contactInfo}" checked>
									<label>${contactInfo}</label>
									</div><br/>
                                        ${ buttons}
								</div>
							`);
						});


					}
					$('.ui.radio.checkbox').checkbox();
					$('#renter-dvConvo').scrollTop($('#renter-dvConvo')[0].scrollHeight);
				}
			});

			$('.renter-message-thread').removeClass('active-thread');
			$(this).addClass('renter-active-thread');
			document.getElementById('renter-dvConvo').scrollIntoView()



		});
	}
	function loadRenterInbox() {
		$.ajax({
			type: 'POST',
			url: "/Rentals/LoadRenterInbox",
			//data: { skip: skipNum, take: 10 },
			success: function (data) {

				if (data.result.length > 0) {
					$.each(data.result, function (k, v) {

						var clock = '';
						var date1 = moment(v.LastMessageAt);
						var date2 = moment();
						var differenceInMs = date2.diff(date1); // diff yields milliseconds
						var duration = moment.duration(differenceInMs); // moment.duration accepts ms
						var differenceInMinutes = duration.asMinutes();
						if (differenceInMinutes != 0) {
							if (differenceInMinutes <= 30 && v.IsIncoming) {
								clock = '<i class="wait icon green right floated"></i>';
							}
							else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
								clock = '<i class="wait icon yellow right floated"></i>';
							}
							else if (differenceInMinutes > 60 && v.IsIncoming) {
								clock = '<i class="wait icon red right floated"></i>';
							}
						}
						var html = '<div class="item renter-message-thread" data-thread-id="' + v.ThreadId + '" data-id="' + v.RenterId + '">' +
							'<div class="avatar">' +
							'<img src="' + v.ImageUrl + '" height="40" width="40">' +
							'</div>' +
							'<div class="content">' +
							'<a class="author">' + v.Name + '</a>' +
							clock +
							'<br>' +
							'<div class="metadata">' +
							(v.LastMessageAt != null ? '<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' : '') +
							'</div>' +
							'<div class="text">' + (v.LastMessage!=null?(v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage):"Photo") + '</div>' +
							'</div>' +
							'</div>';
						$('#renter-inbox-list').append(html);

					});
					$('#renter-inbox-list').attr('data-active-thread-id', (data.result.length > 0 ? data.result[0].ThreadId : "0"));
					$("#renter-inbox-list").find('.item[data-thread-id="' + $('#renter-inbox-list').attr('data-active-thread-id') + '"]').first().trigger("click");
				}
				else {
					var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
					$('#renter-dvConvo').append(html);
				}
			}
		});
	}

	function OnChangeMMS() {
		$(document).on('change', '#mms-pic', function (e) {
			$('#mms-image-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#mms-image-upload').append('<img class="zoom" src="' + imageUrl + '">');
			}
		});
	}
	function onUploadImage() {
		$(document).on('click', '#add-mms', function (e) {
			$('#mms-pic').click();
		});
	}
	function onMessageEndSubmit() {
		$(document).off('click', '#end-message-btn');
		$(document).on('click', '#end-message-btn', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			$.ajax({
				type: 'POST',
				url: "/Messages/EndThread",
				data: {
					threadId: threadId
				},
				success: function (data) {
					swal("This thread mark as end!", "", "success");
					$('.active-thread').children('.content').children('i').remove();
				}
			});
		});
	}
	function ontxtMsgContentFocusOut(){
		$('#txtMsgContent').focusout(function () {
			$.ajax({
				type: 'POST',
				url: "/Messages/UnLockThread",
				data: {
					threadId: $('.active-thread').attr('data-thread-id')
				},
				success: function (data) {
				}
			});
		});
	}
	function ontxtMsgContentClick() {
		$(document).on('click', '#txtMsgContent', function () {
			if ($('#txtMsgContent').attr('disabled') == undefined) {
				$.ajax({
					type: 'POST',
					url: "/Messages/LockThread",
					data: {
						threadId: $('.active-thread').attr('data-thread-id')
					},
					success: function (data) {
						//if (data.success) {
						//}
						//else {
						//}
					}
				});
		}
		});
	}

	function saveContact(){
		var ContactList = [];

		$(document).on('click', '#save-phone-number', function () {
			var guestId = $('.active-thread').attr('data-guest-id');
			
			$('.contact-field').each(function (index, element) {
				ContactList.push($(this).find('input[name="contact-num"]').val());
				//var input = $(this).find('input[name="contact-num"]');
				//var iti = intlTelInput(input);
				//alert(window.intlTelInputGlobals.getInstance(iti));
			});
			$.ajax({
				type: "POST",
				url: "/Messages/SaveContact",
				data: { guestId: guestId, ContactList: ContactList/*JSON.stringify(ContactList)*/ },
				success: function (data) {
					$('#contact-info').empty();
					$('#contact-wrapper').empty();
					$('#contact-save').addClass("hidden");
					$.each(data.contactInfos, function (index, contactInfo) {
						let buttons =
							`<button class="circular ui green icon button call-button" data-tooltip="Call" data-position="left center">
									<i class="icon phone"></i>
								</button>
                                <button class="circular ui orange icon button sms-button" data-tooltip="SMS" data-position="left center">
									<i class="icon comments"></i>
								</button>`;

						if (contactInfo.Type == 2) {
							buttons =
								`<button class="circular ui teal icon button sms-button" data-tooltip="WhatsApp" data-position="left center">
                                        <i class="icon whatsapp"></i>
                                    </button>`;
						} else if (contactInfo.Type == 3) {
							buttons =
								`<button class="circular ui blue icon button sms-button" data-tooltip="Messenger" data-position="left center">
                                        <i class="icon facebook messenger"></i>
                                    </button>`;
						}
						var isDefault = contactInfo.IsDefault ? "checked" : "";
						$('#contact-info').append(`<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}">
															<div class="ui radio checkbox ${isDefault}">
															 <input type="radio" name="contact" ${isDefault}>
															<label>${contactInfo.Value} (${getSiteName(contactInfo.Source)})</label>
															</div><br/>
															${ buttons}
															<button class="circular mini ui icon button reassign-button" data-tooltip="Re-assign Contact Info">
																<i class="icon exchange"></i>
															</button>
														</div>`);
					});
				}
			});
		});


	}

	function AddContact() {
		$(document).on('click', '.add-contact', function () {
			var fields = '<div class="fields">' +
				'<div class="three wide field">' +
				'<div class="contact-field"><div class"ui input">'+
				'<input type="tel" name="contact-num" class="contact-num" placeholder="Phone Number"/></div>' +
				'<button type="button" class="circular ui icon orange mini button remove-contact" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
					'</button>' +
					'</div>'+
				'</div>' +
				'</div>';
			$('#contact-wrapper').append(fields);
			//var input = document.querySelector(".contact-num");
			//window.intlTelInput(input, { separateDialCode: true, utilsScript: "~/Content/country-code/utils.js" });
			$('#contact-save').removeClass("hidden");
		});
	}
    //$('.countries.ui.dropdown').dropdown('set selected', 'CA');

    var twilioNumberTable = $('#twilio-number-table').DataTable({
        "aaSorting": [],
        "responsive": true,
        "bFilter": false,
        "ajax": {
            "url": "/Messages/GetOwnedNumbers",
            "type": 'GET',
            "data": function () {
                return {
                }
            }
        },
        "columns": [
			{ "data": "PhoneNumber", "orderable": false },
			{ "data": "CallForwardingNumber", "orderable": false },
			{ "data": "FriendlyName", "orderable": false },
			{ "data": "CallLength", "orderable": false },
			{ "data": "VoicemailMessage", "orderable": false },
			{ "data": "CallforwardingMessage", "orderable": false },
			{ "data": "Capabilities", "orderable": false },
            { "data": "Actions", "orderable": false }
        ],
        "createdRow": function (row, data) {
            $(row).attr('data-id', data.Id);
            $(row).attr('data-phone-number', data.PhoneNumber);
        }
    });

    $(document).on('click', '#twilio-number-table .release.button', function () {
        var phoneNumber = $(this).parents('tr').attr("data-phone-number");

        swal({
            title: "Are you sure you want to delete this number?",
            text: `Number: ${phoneNumber}`,
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
			showCancelButton: true,
			closeOnConfirm: true,
        },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/Messages/ReleaseNumber",
                    data: {
                        phoneNumber: phoneNumber
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({ title: "Success", text: "Phone number has been deleted", type: "success" },
                                function () {
                                    twilioNumberTable.ajax.reload();
                                }
                            );
                        } else {
                            toastr.error(data.message, null, { timeOut: 5000, positionClass: "toast-top-right" });
                        }
                    }
                });
            });
    });

    $(document).on('click', '#buy-twilio-number-table .buy.button', function () {
        var phoneNumber = $(this).parents('tr').attr("data-phone-number");
        var currentPrice = $(this).parents('tr').attr("data-current-price");

        swal({
            title: "Are you sure you want to buy this number?",
            text: `Number: ${phoneNumber}
            Current Price: ${currentPrice}
            `,
            confirmButtonText: "Confirm",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/Messages/BuyNumber",
                    data: {
                        phoneNumber: phoneNumber
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({ title: "Success", text: "Phone number has been registered", type: "success" },
                                function () {
                                    twilioNumberTable.ajax.reload();;
                                    buyTwilioNumberTable.ajax.reload();
                                }
                            );
                        } else {
                            toastr.error(data.message, null, { timeOut: 5000, positionClass: "toast-top-right" });
                        }
                    }
                });
            });
    });

    $(document).on('click', '.twilio-phone-number .refresh.button', function () {
        twilioNumberTable.ajax.reload();;
    });

    $(document).on('click', '.blocked-phone-number .refresh.button', function () {
        blockedPhoneNumberTable.ajax.reload();;
    });

    var blockedPhoneNumberTable = $('#blocked-phone-number-table').DataTable({
        "aaSorting": [],
        "responsive": true,
        "bFilter": false,
        "ajax": {
            "url": "/Messages/GetBlockedPhoneNumbers",
            "type": 'GET',
            "data": function () {
                return {
                }
            }
        },
        "columns": [
            { "data": "PhoneNumber", "orderable": false },
            {
                "data": "BlockedDate", "orderable": true,
                render: function (data, type, row) {
                    if (type === "sort" || type === "type") {
                        return data;
                    }
                    return moment(data).format("MMM DD, YYYY hh:mm a");
                }
            },
            { "data": "Actions", "orderable": false }
        ],
        "createdRow": function (row, data) {
            $(row).attr('data-phone-number', data.PhoneNumber);
        }
    });

    $(document).on('click', '.blocked-phone-number .block-phone-number-form .block.button', function () {
        var phoneNumber = $('.blocked-phone-number .block-phone-number-form [name="phone-number"]').val();
        if (!phoneNumber) {
            return;
        }

        swal({
            title: "Are you sure you want to block this number?",
            text: `Number: ${phoneNumber}`,
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Block",
            showCancelButton: true,
            closeOnConfirm: false,
        },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/Messages/BlockNumber",
                    data: {
                        phoneNumber: phoneNumber
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({ title: "Success", text: "Phone number has been added on blocked list", type: "success" },
                                function () {
                                    blockedPhoneNumberTable.ajax.reload();
                                    $('.blocked-phone-number .block-phone-number-form [name="phone-number"]').val('')
                                }
                            );
                        } else {
                            toastr.error(data.message, null, { timeOut: 5000, positionClass: "toast-top-right" });
                        }
                    }
                });
            });
    });

    $(document).on('click', '#blocked-phone-number-table .remove.button', function () {
        var phoneNumber = $(this).parents('tr').attr("data-phone-number");

        swal({
            title: "Are you sure you want to remove this number?",
            text: `Number: ${phoneNumber}`,
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            showCancelButton: true,
            closeOnConfirm: false,
        },
            function () {
                $.ajax({
                    type: "POST",
                    url: "/Messages/RemoveBlockedNumber",
                    data: {
                        phoneNumber: phoneNumber
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({ title: "Success", text: "Phone number has been removed from blocked list", type: "success" },
                                function () {
                                    blockedPhoneNumberTable.ajax.reload();
                                }
                            );
                        } else {
                            toastr.error(data.message, null, { timeOut: 5000, positionClass: "toast-top-right" });
                        }
                    }
                });
            });
    });

    var buyTwilioNumberTable = $('#buy-twilio-number-table').DataTable({
        "aaSorting": [],
        "responsive": true,
        "bFilter": false,
        "lengthChange": false,
        "pageLength": 10,
        "deferLoading": 0,
        'processing': true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Searching...'
        },
        "ajax": {
            "url": "/Messages/SearchNumber",
            "type": 'GET',
            "data": function () {
                let mmsEnabled = null;
                let smsEnabled = null;
                let voiceEnabled = null;
                //let faxEnabled = null;
                //if (!$('.buy-twilio-phone-number [name="any-capabilities"]').is(':checked')) {
                    smsEnabled = $('.buy-twilio-phone-number [name="sms-enabled"]').is(':checked') || null;
                    mmsEnabled = $('.buy-twilio-phone-number [name="mms-enabled"]').is(':checked') || null;
                    voiceEnabled = $('.buy-twilio-phone-number [name="voice-enabled"]').is(':checked') || null;
                    //faxEnabled = $('.buy-twilio-phone-number [name="fax-enabled"]').is(':checked') || null;
                //}

                let type = $('.buy-twilio-phone-number [name="type"]:checked').val();
                return {
                    country: $('.countries.ui.dropdown').dropdown("get value"),
                    smsEnabled: smsEnabled,
                    mmsEnabled: mmsEnabled,
                    voiceEnabled: voiceEnabled,
                    //faxEnabled: faxEnabled,
                    type: type,
                    contains: $('.buy-twilio-phone-number [name="number"]').val(),
                    location: $('.buy-twilio-phone-number [name="location"]').val(),
                    areaCode: null,
                    pageSize: 20
                }
            }
        },
        "columns": [
            { "data": "FriendlyName", "orderable": false },
            { "data": "Type", "orderable": false },
            { "data": "VoiceEnabled", "orderable": false },
            { "data": "SMSEnabled", "orderable": false },
            { "data": "MMSEnabled", "orderable": false },
            //{ "data": "BasePrice", "orderable": false },
            { "data": "CurrentPrice", "orderable": false },
            //{ "data": "FaxEnabled", "orderable": false },
            { "data": "Actions", "orderable": false }
        ],
        "createdRow": function (row, data) {
            $(row).attr('data-phone-number', data.PhoneNumber);
            $(row).attr('data-current-price', data.CurrentPrice);
        }
    });

    $(document).on('click', '.buy-twilio-phone-number .search.button', function () {
        buyTwilioNumberTable.ajax.reload();
    });

    $(document).on('click', '.button.manual-sms', function () {
        $('.send-sms-modal [name="phone-number"]').val($('.form.manual-form [name="phone-number"]').val());

        $('.send-sms-modal.modal')
            .modal({
                closable: true,
                onApprove: function () {
                    $.ajax({
                        type: 'POST',
                        url: "/SMS/Send",
                        data: {
                            to: $('.send-sms-modal [name="phone-number"]').val(),
                            message: $('.send-sms-modal [name="message"]').val()
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.success) {
                                toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                                $('.send-sms-modal [name="phone-number"]').val('');
                                $('.send-sms-modal [name="message"]').val('');
                                $('.item.message-thread.active-thread').click();
                                refreshMessageSMSTableData();
                            } else {
                                toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                            }
                        },
                        error: function (error) {
                            toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                        }
                    });
                }
            })
            .modal('show');

    });
	
    $(document).on('click', '.button.sms-button', function () {
		let phoneNumber = $(this).parents(".field").attr('data-phonenumber');
        let type = $(this).parents(".field").attr('data-type');
        var guestId = $('.active-thread').attr('data-guest-id');

        $('.send-sms-modal [name="phone-number"]').val(phoneNumber);
        $('.send-sms-modal [name="phone-number"]').attr('disabled', true);
        $('.send-sms-modal [name="message"]').val('');

        $('.send-sms-modal.modal')
            .modal({
                closable: true,
                onApprove: function () {
                    $.ajax({
                        type: 'POST',
                        url: "/SMS/Send",
                        data: {
                            to: phoneNumber,
                            message: $('.send-sms-modal [name="message"]').val(),
                            type: type,
                            guestId: guestId
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.success) {
                                toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                                $('.item.message-thread.active-thread').click();
                            } else {
                                toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                            }
                        },
                        error: function (error) {
                            toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                        }
                    });
                }
			})
			.modal('show').modal('refresh');

	});

	//Worker Call/sms start
	$(document).on('click', '.button.worker-sms-button', function () {
		let phoneNumber = $(this).parents(".item").attr('data-phonenumber');
		let type = $(this).parents(".item").attr('data-type');
		var workerId = $('.worker-active-thread').attr('data-worker-id');
		var threadId = $('.worker-active-thread').attr('data-thread-id');

		$('.send-sms-modal [name="phone-number"]').val(phoneNumber);
		$('.send-sms-modal [name="phone-number"]').attr('disabled', true);
		$('.send-sms-modal [name="message"]').val('');

		$('.send-sms-modal.modal')
			.modal({
				closable: true,
				onApprove: function () {
					$.ajax({
						type: 'POST',
						url: "/SMS/WorkerSend",
						data: {
							to: phoneNumber,
							message: $('.send-sms-modal [name="message"]').val(),
							type: type,
							workerId: workerId,
							threadId: threadId
						},
						dataType: "json",
						success: function (data) {
							if (data.success) {

								$('.worker-active-thread').attr('data-thread-id', data.threadId);
								$('#worker-inbox-list').attr('data-active-thread-id', data.threadId);
								toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
								//$('.item.worker-message-thread.worker-active-thread').click();
							} else {
								toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
							}
						},
						error: function (error) {
							toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						}
					});
				}
			})
			.modal('show');

	});

	$(document).on('click', '.button.worker-call-button', function () {
		let phoneNumber = $(this).parents(".item").attr('data-phonenumber');

		var threadId = $('.worker-active-thread').attr('data-thread-id');
		var workerId = $('.worker-active-thread').attr('data-worker-id');
		$.ajax({
			type: 'GET',
			url: "/Messages/WorkerThreadInfo",
			data: { threadId: threadId,workerId:workerId },
			dataType: "json",
			beforeSend: function () {
				$(this).addClass('loading');
			},
			success: function (data) {
				console.log(data);

				$(this).removeClass('loading');

				var params = {
					From: userId,
					To: phoneNumber
				};

				if (device) {
					$('.mini.modal .phone-number').text(phoneNumber);
					$('.mini.modal .header.name').text(data.data.Firstname + ' '+data.data.Lastname);
					$('.mini.modal .avatar.image').attr('src', (data.data.ImageUrl != null ? data.data.ImageUrl:'/Images/Worker/default-image.png'));
					$('.mini.modal .avatar.image').show();
					device.connect(params);
					openOutgoingCallModal();
				}
			}
		});
	});
	//Worker Call/sms End
	
	$(document).on('click', '.button.call-button', function() { 
		let phoneNumber = $(this).parents(".field").attr('data-phonenumber');

		var threadId = $('.active-thread').attr('data-thread-id');
        $.ajax({
            type: 'GET',
            url: "/Messages/ThreadInfo",
            data: { threadId: threadId },
            dataType: "json",
            beforeSend: function () {
                $(this).addClass('loading');
            },
            success: function (data) {
                console.log(data);
                $(this).removeClass('loading');

                var params = {
                    From: userId,
                    To: phoneNumber
                };

                if (device) {
                    $('.mini.modal .phone-number').text(phoneNumber);
                    $('.mini.modal .header.name').text(data.data.Name);
                    $('.mini.modal .avatar.image').attr('src', data.data.ProfilePictureUrl);
                    $('.mini.modal .avatar.image').show();
                    device.connect(params);
                    openOutgoingCallModal();
                }
            }
        });
	});


	$(document).on('click', '.button.reassign-button', function() { 
        let phoneNumber = $(this).parents(".item").attr('data-phonenumber');
		$("#guest").val('');
		$('#renter').val('');
        $('#sms-assign-phone-modal').modal({
			onApprove: function () {
				let newGuestId = $("#guest").val()//.dropdown().val();
                if (newGuestId) {
                    $.ajax({
                        type: 'POST',
                        url: "/Messages/AssignSMSPhoneNumber",
                        data: {
                            phoneNumber: phoneNumber,
                            guestId: newGuestId
                        },
                        success: function (data) {
                            if (data.success) {
                                $('#sms-assign-phone-modal').modal('hide');
                                swal({ title: "Success", text: "Phone Number has been re-assigned", type: "success" },
                                    function () {
                                        refreshMessageSMSTableData();
                                        $('.item.message-thread.active-thread').click();
                                    }
                                );
                                $('.item.message-thread.active-thread').click();
                            }
                            else {
                                $('#sms-assign-phone-modal').modal('hide');
                                swal("Error in assigning phone number", "", "error");
                            }
                        },
                        error: function (error) {
                            toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
                        }
                    });
                }
            }
        })
            .modal('show')
            .modal('refresh');
	});

	function onEditMessageRules() {
		$(document).on('click', '.edit-message-rule-btn', function () {
			var id = $(this).parents('tr').attr("data-message-rule-id");

			$.ajax({
				type: 'GET',
				url: "/Messages/MessageRuleDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						clearMessageRulesModal();
						var form = $('#message-rule-form');
						form.find('input[name=message-rule-id]').val(data.messageRules.Id);
						form.find('select[name=property]').dropdown('set selected', data.messageRules.PropertyId);
						form.find('select[name=event-type]').dropdown('set selected', data.messageRules.Type);
						form.find('select[name=template]').dropdown('set selected', data.messageRules.TemplateMessageId);
						form.find('input[name=send-to-sms]').prop('checked', data.messageRules.IsSendToSMS);
						form.find('input[name=send-to-site]').prop('checked', data.messageRules.IsSendToSite);
						form.find('input[name=send-to-email]').prop('checked', data.messageRules.IsSendToEmail);
						form.find('input[name=is-active]').prop('checked', data.messageRules.IsActive);
						if (data.messageRules.DateDifference > 0) {
							form.find('input[name=days]').val(data.messageRules.DateDifference);
						}
						$('#message-rule-modal').modal({
							onApprove: messageRuleAddEdit
						})
							.modal('show')
							.modal('refresh');
					}
				}
			});
		});
	}
	function onDeleteMessageRules() {
		$(document).on('click', '.delete-message-rule-btn', function () {
			var id = $(this).parents('tr').attr("data-message-rule-id");
			swal({
				title: "Are you sure you want to delete this message rule ?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: "POST",
						url: "/Messages/DeleteMessageRule",
						data: { id: id },
						success: function (data) {
							if (data.success) {
								swal({ title: "Success", text: "Message Rule has been deleted.", type: "success" },
									function () {
									    clearMessageRulesModal();
									    refreshMessageRulesTableData();
									    refreshMessageSMSTableData();
										refreshTemplateTableData();
										getTemplateMessage();
									}
								);
							}
						}
					});
				});
		});
	}
	function onMessageRulesFilter() {
	    $("#message-rules-filter-form select[name=property]").on('change', function () {
	        refreshMessageRulesTableData();
	    });
	    $("#message-rules-filter-form select[name=event-type]").on('change', function () {
	        refreshMessageRulesTableData();
	    });
	}
	function onMessageSMSRefresh() {
	    $("#refresh-sms-btn").on('click', function () {
	        refreshMessageSMSTableData();
	    });
	}
	function onAddMessageRulesModal() {
		$(document).on('click', '#add-message-rule-btn', function () {
			clearMessageRulesModal();
			$('#message-rule-modal').modal({
				onApprove: messageRuleAddEdit
			})
				.modal('show')
				.modal('refresh');
		});
	}
	function onAssignMessagePhoneModal() {
	    $(document).on('click', '.assign-sms-btn', function () {
			var phoneNumber = $(this).parents('tr').attr("data-phone-number");
			$('#sms-assign-phone-modal').find('.header').text(phoneNumber + " Assign to");
			//$('#guest').dropdown();
			//$('#renter').dropdown();
	        $('#sms-assign-phone-modal').modal({
	            onApprove: function () {
					let guestId = $("#guest").val();
					let renterId = $('#renter').val();
					if (guestId || renterId) {
                        $.ajax({
                            type: 'POST',
                            url: "/Messages/AssignSMSPhoneNumber",
                            data: {
								guestId: guestId,
								renterId: renterId,
								phoneNumber: phoneNumber
                            },
                            success: function (data) {
                                if (data.success) {
                                    swal({ title: "Success", text: "Number has been assigned", type: "success" },
                                        function () {
                                            //refreshMessageRulesTableData();
                                            refreshMessageSMSTableData();
                                            //refreshTemplateTableData();
                                            //getTemplateMessage();
                                        }
                                    );
                                    $('#sms-assign-phone-modal').modal('hide');
                                }
                                else {
                                    $('#sms-assign-phone-modal').modal('hide');
                                    swal("Error in assigning number", "", "error");
                                }
                            }
                        });
                    }
	            }
	        })
               .modal('show')
               .modal('refresh');
	    });

	}

	//var messageRuleAddEdit = function () {
	//    var form = $('#message-rule-form');
	//    debugger;
	//    var id = form.find('input[name=message-rule-id]').val();
	//    var type = form.find('select[name=event-type]').val();
	//    var propertyId = form.find('select[name=property]').val();
	//    var templateId = form.find('select[name=template]').val();
	//    var days = form.find('input[name=days]').val();
	//    var title = form.find('input[name=template-title]').val();
	//    var message = form.find('textarea[name=template-message]').val();

	//    if (days == '' || days == null || type == 1 || type == 3 || type == 5 || type == 6 || type == 7 || type == 8 || type == 9 || type == 10) {
	//        days = 0;
	//    }
	//    if (id == '0') {
	//        $.ajax({
	//            type: 'POST',
	//            url: "/Messages/AddMessageRules",
	//            data: {
	//                templateMessageId: templateId,
	//                propertyId: propertyId,
	//                type: type,
	//                days: days,
	//                title: title,
	//                message: message
	//            },
	//            success: function (data) {
	//                if (data.success) {
	//                    swal({ title: "Success", text: "Message Rule has been saved.", type: "success" },
	//						function () {
	//						    clearMessageRulesModal();
	//						    refreshMessageRulesTableData();
	//						    refreshMessageSMSTableData();
	//						    refreshTemplateTableData();
	//						    getTemplateMessage();
	//						}
	//					);
	//                    $('#message-rule-modal').modal('hide');
	//                }
	//                else {
	//                    $('#message-rule-modal').modal('hide');
	//                    swal("Error in saving new message rule", "", "error");
	//                }
	//            }
	//        });
	//    }
	//    else if (id > 0) {
	//        $.ajax({
	//            type: 'POST',
	//            url: "/Messages/EditMessageRule",
	//            data: {
	//                id: id,
	//                title: title,
	//                message: message,
	//                propertyId: propertyId,
	//                templateMessageId: templateId,
	//                type: type,
	//                days: days
	//            },
	//            success: function (data) {
	//                if (data.success) {
	//                    swal({ title: "Success", text: "Message Rule has been updated.", type: "success" },
	//						function () {
	//						    clearMessageRulesModal();
	//						    refreshMessageRulesTableData();
	//						    refreshMessageSMSTableData();
	//						    refreshTemplateTableData();
	//						    getTemplateMessage();
	//						}
	//					);
	//                    $('#message-rule-modal').modal('hide');
	//                }
	//                else {
	//                    $('#message-rule-modal').modal('hide');
	//                    swal("Error in updating message rule", "", "error");
	//                }
	//            }
	//        });
	//    }
	//}

	function loadMessageRuleTableData() {
		tableMessageRules = $('#message-rule-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"bFilter": false,
			"ajax": {
				"url": "/Messages/GetMessageRulesList",
				"type": 'GET',
				"data": function () {
					return {
						propertyFilter: $("#message-rules-filter-form select[name=property]").val(),
						eventFilter: $("#message-rules-filter-form select[name=event-type]").val()
					}
				}
			},
			"columns": [
				{ "data": "PropertyName" },
				{ "data": "EventType" },
				{ "data": "Title" },
				{ "data": "Message", "orderable": false },
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-message-rule-id', data.Id);
			}
		});
	}
	function loadMessageSMSTableData() {
	    tableMessageSMS = $('#message-sms-table').DataTable({
	        "aaSorting": [],
	        "responsive": true,
	        "search": { "caseInsensitive": true },
	        "bFilter": false,
	        "ajax": {
	            "url": "/Messages/GetSMSList",
	            "type": 'GET',
	            "data": function () {
	                return {
	                    propertyFilter: $("#message-sms-filter-form select[name=property]").val()
	                }
	            }
	        },
	        "columns": [
				{ "data": "From", "orderable": false },
				{ "data": "Message", "orderable": false },
				{
				    "data": "CreatedDate", "orderable": true,
				    render: function (data, type, row) {
				        if (type === "sort" || type === "type") {
				            return data;
				        }
				        return moment(data).format("MMM DD, YYYY hh:mm a");
				    }
				},
				{ "data": "Actions", "orderable": false }
	        ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-message-sms-id', data.Id);
                $(row).attr('data-phone-number', data.From);
	        }
	    });
	}
	function clearMessageRulesModal() {
		var form = $('#message-rule-form');
		form.find('select[name=property]').dropdown('clear');
		form.find('select[name=event-type]').dropdown('clear');
		form.find('input[name=days]').val('');
		form.find('select[name=template]').dropdown('clear');
		form.find('input[name=message-rule-id]').val('0');
		$('#new-template-wrapper').hide();
		form.find('input[name=template-title]').val('');
		form.find('select[name=template-variable]').dropdown('clear');
		form.find('textarea[name=template-message]').val('');
		form.find('input[name=send-to-sms]').prop('checked',false);
		form.find('input[name=send-to-site]').prop('checked',false);
		form.find('input[name=send-to-email]').prop('checked',false);
		form.find('input[name=is-active]').prop('checked',false);

	}
	function refreshMessageRulesTableData() {
	    tableMessageRules.ajax.reload(null, false);
	}
	function refreshMessageSMSTableData() {
	    tableMessageSMS.ajax.reload(null, false);
	}
	var tableMessageSMS;
	function onDeleteMessageCallClick() {

		$(document).on('click', '.delete-call-btn', function () {
			var id = $(this).parents('tr').attr('data-message-sms-id');
			swal({
				title: "Are you sure you want to delete this call?",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Messages/DeleteMessageCall",
						data: { id: id },
						success: function (data) {
							refreshMessageSMSTableData();
							if (data.success) {
								swal("Successfully Deleted.", "", "success");
							}
							else {
								swal("An error occured on deleting", "", "error");
							}
						}
					});
				});
		});
	}
	function onDeleteMessageSMSClick() {

	    $(document).on('click', '.delete-sms-btn', function () {
	        var id = $(this).parents('tr').attr('data-message-sms-id');
	        

	        swal({
	            title: "Are you sure you want to delete this message?",
	            confirmButtonColor: "##abdd54",
	            confirmButtonText: "Yes",
	            showCancelButton: true,
	            closeOnConfirm: false,
	            showLoaderOnConfirm: true
	        },
				function () {
				    $.ajax({
				        type: 'POST',
				        url: "/Messages/DeleteMessageSMS",
				        data: { id: id },
				        success: function (data) {
				            refreshMessageSMSTableData();
				            if (data.success) {
				                swal("Successfully Deleted.", "", "success");
				            }
				            else {
				                swal("An error occured on deleting", "", "error");
				            }
				        }
				    });
				});
	    });
	}
	function onReplyMessageSMSClick() {

	    $(document).on('click', '.reply-sms-btn', function () {
	        var id = $(this).parents('tr').attr('data-message-sms-id');


	        swal({
	            title: "Reply",
                type: "input",
	            confirmButtonColor: "##abdd54",
	            confirmButtonText: "Yes",
	            showCancelButton: true,
	            closeOnConfirm: false,
	            showLoaderOnConfirm: true
	        },
				function (message) {
				    if (message) {
				        $.ajax({
				            type: 'POST',
				            url: "/Messages/ReplyMessageSMS",
				            data: { id: id, message: message },
				            success: function (data) {
				                refreshMessageSMSTableData();
				                if (data.success) {
				                    swal("Message Sent.", "", "success");
				                }
				                else {
				                    swal("An error occured on sending", "", "error");
				                }
				            }
				        });
				    }
				});
	    });
	}

	var messageRuleAddEdit = function () {
		var form = $('#message-rule-form');
		var id = form.find('input[name=message-rule-id]').val();
		var type = form.find('select[name=event-type]').val();
		var propertyId = form.find('select[name=property]').val();
		var templateId = form.find('select[name=template]').val();
		var days = form.find('input[name=days]').val();
		var title = form.find('input[name=template-title]').val();
		var message = form.find('textarea[name=template-message]').val();
		var isSendToSms = form.find('input[name=send-to-sms]').prop('checked');
		var isSendToSite = form.find('input[name=send-to-site]').prop('checked');
		var isSendToEmail = form.find('input[name=send-to-email]').prop('checked');
		var isActive = form.find('input[name=is-active]').prop('checked');

		if (days == '' || days == null || type == 1 || type == 3 || type == 5 || type == 6 || type == 7 || type == 8 || type == 9 || type == 10) {
			days = 0;
		}
		if (id == '0') {
			$.ajax({
				type: 'POST',
				url: "/Messages/AddMessageRules",
				data: {
					templateMessageId: templateId,
					propertyId: propertyId,
					type: type,
					days: days,
					title: title,
					message: message,
					isSendToSms: isSendToSms,
					isSendToSite: isSendToSite,
					isSendToEmail: isSendToEmail,
					isActive: isActive
				},
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Message Rule has been saved.", type: "success" },
							function () {
								clearMessageRulesModal();
								refreshMessageRulesTableData();
								refreshMessageSMSTableData();
								refreshTemplateTableData();
								getTemplateMessage();
							}
						);
						$('#message-rule-modal').modal('hide');
					}
					else {
						$('#message-rule-modal').modal('hide');
						swal("Error in saving new message rule", "", "error");
					}
				}
			});
		}
		else if (id > 0) {
			$.ajax({
				type: 'POST',
				url: "/Messages/EditMessageRule",
				data: {
					id: id,
					title: title,
					message: message,
					propertyId: propertyId,
					templateMessageId: templateId,
					type: type,
					days: days,
					isSendToSms: isSendToSms,
					isSendToSite: isSendToSite,
					isSendToEmail: isSendToEmail,
					isActive: isActive
				},
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Message Rule has been updated.", type: "success" },
							function () {
								clearMessageRulesModal();
								refreshMessageRulesTableData();
								refreshMessageSMSTableData();
								refreshTemplateTableData();
								getTemplateMessage();
							}
						);
						$('#message-rule-modal').modal('hide');
					}
					else {
						$('#message-rule-modal').modal('hide');
						swal("Error in updating message rule", "", "error");
					}
				}
			});
		}
	}

	function AcceptBookingClick() {

		$(document).on('click', '.js-btn_accept-booking', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_accept-booking ", "");
			//alert(reservationCode);
			swal({
				title: "Are you sure you want to accept booking?",
				text: "",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Accept",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				inputPlaceholder: "Write message"
			},
				function (message) {
					$.ajax({
						type: 'POST',
						url: "/Booking/AcceptBooking",
						data: { siteType: siteType, threadId: threadId, hostId: hostId, message: message, reservationCode: reservationCode, message: message },
						success: function (data) {
							if (data.success) {
								debugger;
								var action = '<div><div class="two ui mini buttons">' +
									'<button style="margin: 1%" class="ui blue button js-btn_alter-booking ' + reservationCode + '">Alter Reservation</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationCode + '">Cancel Reservation</button>' +
									'</div></div>';
								$('#' + reservationCode + '').html(action);
								//$('#booking-actions-withdraw-pre-approve').show();
								//$('#booking-actions-inquiry').hide();
								//$('#booking-actions-withdraw-special-offer').hide();
								swal("Successfully accepted", "", "success");
							}
							else {
								swal("An error occured on accepting booking", "", "error");
							}
						}
					});
				});
		});
	}

	function DeclineBookingClick() {

		$(document).on('click', '.js-btn_decline-booking', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_decline-booking ", "");

			swal({
				title: "Are you sure you want to decline ?",
				text: "",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				inputPlaceholder: "Write message"
			},
				function (message) {
					$.ajax({
						type: 'POST',
						url: "/Booking/DeclineBooking",
						data: { siteType: siteType, threadId: threadId, hostId: hostId, reservationCode: reservationCode, message: message },
						success: function (data) {
							if (data.success) {
								debugger;
								//$('#booking-actions-withdraw-pre-approve').show();
								//$('#booking-actions-inquiry').hide();
								//$('#booking-actions-withdraw-special-offer').hide();
								//swal("Successfully Pre-approved.", "", "success");
							}
							else {
								swal("An error occured on declining", "", "error");
							}
						}
					});
				});
		});
	}

	function CancelAlterationBookingClick() {

		$(document).on('click', '.js-btn_cancel-alteration-booking', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_cancel-alteration-booking ", "");

			swal({
				title: "Are you sure you want to cancel alteration ?",
				text: "",
				type: "warning",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				//inputPlaceholder: "Write message"
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Booking/CancelAlteration",
						data: { siteType: siteType, hostId: hostId, reservationCode: reservationCode },
						success: function (data) {
							if (data.success) {
								debugger;
								var action = '<div><div class="two ui mini buttons">' +
									'<button style="margin: 1%" class="ui blue button js-btn_alter-booking ' + reservationCode + '">Alter Reservation</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationCode + '">Cancel Reservation</button>' +
									'</div></div>';
								$('#' + reservationCode + '').html(action);
								swal("Successfully canceled alteration", "", "success");
							}
							else {
								swal("An error occured on canceling alteration", "", "error");
							}
						}
					});
				});
		});
	}
	function CancelBookingClick() {

		$(document).on('click', '.js-btn_cancel-booking', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_cancel-booking ", "");

			swal({
				title: "Are you sure you want to decline ?",
				text: "",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				inputPlaceholder: "Write message"
			},
				function (message) {
					$.ajax({
						type: 'POST',
						url: "/Booking/DeclineBooking",
						data: { siteType: siteType, threadId: threadId, hostId: hostId, reservationCode: reservationCode, message: message },
						success: function (data) {
							if (data.success) {
								debugger;
								//$('#booking-actions-withdraw-pre-approve').show();
								//$('#booking-actions-inquiry').hide();
								//$('#booking-actions-withdraw-special-offer').hide();
								$('#' + reservationCode + '').html();
								swal("Successfully Pre-approved.", "", "success");
							}
							else {
								swal("An error occured on declining", "", "error");
							}
						}
					});
				});
		});
	}

	//$('#inbox-list').scroll()
	//$(document).on('scroll', '#inbox-list', function () {

	//	alert("Scrool!!!");
	//});


	//Joem 07/27/18 It triggered the Lazy load
	$('.message-list').scroll(function () {
		
		var elem = $('.message-list');
		if (elem[0].scrollHeight - elem.scrollTop() - elem.innerHeight() < 1) {
			skipNum = skipNum + 10;
			$.ajax({
				type: 'POST',
				url: "/Messages/LoadInbox",
				async: true,
				data: { skip: skipNum, take: 10 },
				success: function (data) {
					var html = '';
					$.each(data.result, function (k, v) {
						var clock = '';
						var date1 = moment(v.LastMessageAt).utc();
						var date2 = moment();
						var differenceInMs = date2.diff(date1); // diff yields milliseconds
						var duration = moment.duration(differenceInMs); // moment.duration accepts ms
						var differenceInMinutes = duration.asMinutes();

						if (differenceInMinutes != 0) {
							if (differenceInMinutes <= 30 && v.IsIncoming) {
								clock = '<i class="wait icon green right floated"></i>';
							}
							else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
								clock = '<i class="wait icon yellow right floated"></i>';
							}
							else if (differenceInMinutes > 60 && v.IsIncoming) {
								clock = '<i class="wait icon red right floated"></i>';
							}
						}
						
						html+= '<div class="item message-thread" data-host-id="' + v.HostAccountId + '" data-thread-id="' + v.ThreadId + '" data-guest-id="' + v.GuestId + '" data-site-type="' + v.SiteType + '" data-reservation-id="">' +
							'<div class="avatar">' +
							'<img src="' + v.GuestProfilePictureUrl + '" height="40" width="40">' +
							'</div>' +
							'<div class="content">' +
							'<a class="author">' + v.GuestName + '</a>' +
							clock +
							'<br>' +
							'<div class="metadata">' +
							'<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' +
							'</div>' +
							'<div class="text">' + (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage) + '</div>' +
							'</div>' +
							'</div>';
					});
					$('#inbox-list').append(html);
				}
			});

		}
	});
	function loadWorkerInbox() {
		$.ajax({
			type: 'POST',
			url: "/Messages/LoadWorkerInbox",
			//data: { skip: skipNum, take: 10 },
			success: function (data) {

				if (data.result.length > 0) {
					$.each(data.result, function (k, v) {
					
						var clock = '';
						var date1 = moment(v.LastMessageAt);
						var date2 = moment();
						differenceInMs = date2.diff(date1); 
						duration = moment.duration(differenceInMs); 
						differenceInMinutes = duration.asMinutes();
							var html ='<div class="item worker-message-thread" data-thread-id="'+v.ThreadId+'" data-worker-id="'+v.WorkerId+'">' +
							'<div class="avatar">' +
							'<img src="' + v.ImageUrl + '" height="40" width="40">' +
							'</div>' +
							'<div class="content">' +
							'<a class="author">' + v.Name + '</a>' +
							'<br>' +
							'<div class="metadata">' +
							(v.LastMessageAt != null?'<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>':'') +
							'</div>' +
								'<div class="text">' + (v.LastMessage!=null? (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage):"Photos") + '</div>' +
							'</div>' +
							'</div>';
						$('#worker-inbox-list').append(html);
						
					});

					//alert(threadId + " " + data.result[0].ThreadId);
					$('#worker-inbox-list').attr('data-active-thread-id', (data.result.length > 0 ? data.result[0].ThreadId : "0"));
					$("#worker-inbox-list").find('.item[data-thread-id="' + $('#worker-inbox-list').attr('data-active-thread-id') + '"]').first().trigger("click");
				}
				else {
					var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
					$('#worker-dvConvo').append(html);
				}
		}
	});
	}

	function loadInbox() {
		$.ajax({
			type: 'POST',
			url: "/Messages/LoadInbox",
			async: true,
			data: { skip: skipNum, take: 10 },
			success: function (data) {
				if (data.result.length > 0) {
					var html = '';
					$.each(data.result, function (k, v) {
						var clock = '';
						var date1 = moment(v.LastMessageAt);
						var date2 = moment();
						var differenceInMs = date2.diff(date1); // diff yields milliseconds
						var duration = moment.duration(differenceInMs); // moment.duration accepts ms
						var differenceInMinutes = duration.asMinutes();

						if (differenceInMinutes != 0) {
							if (differenceInMinutes <= 30 && v.IsIncoming) {
								clock = '<i class="wait icon green right floated"></i>';
							}
							else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
								clock = '<i class="wait icon yellow right floated"></i>';
							}
							else if (differenceInMinutes > 60 && v.IsIncoming) {
								clock = '<i class="wait icon red right floated"></i>';
							}
						}
						
						html+= '<div class="item message-thread" data-host-id="' + v.HostAccountId + '" data-thread-id="' + v.ThreadId + '" data-guest-id="' + v.GuestId + '" data-site-type="' + v.SiteType + '" data-reservation-id="">' +
							'<div class="avatar">' +
							'<img src="' + v.GuestProfilePictureUrl + '" height="40" width="40">' +
							'</div>' +
							'<div class="content">' +
							'<a class="author">' + v.GuestName + '</a>' +
							clock +
							'<br>' +
							'<div class="metadata">' +
							'<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' +
							'</div>' +
							'<div class="text">' + (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage) + '</div>' +
							'</div>' +
							'</div>';
					});
					$('#inbox-list').append(html);

					var threadId = $("meta[name=initial_thread_id]").attr('content');

					$('#inbox-list').attr('data-active-thread-id', (threadId != "0" ? threadId : (data.result.length > 0 ? data.result[0].ThreadId : "0")));
					$("#inbox-list").find('.item[data-thread-id="' + $('#inbox-list').attr('data-active-thread-id') + '"]').first().trigger("click");
				}
				else {
					var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
					$('#dvConvo').append(html);
				}
			}
		});
	}


	function getVariableList(data) {
		$('#template-variable').empty();

		$('#template-variable').append('<option class="placeholder" selected disabled value="">Select Variable</option>');
		var variables = data.threadDetails.Inquiries[0];
		if (data.threadDetails.Inquiries.length == 1) {
			//vaiables in messages page
			$('#template-variable').append('<option value="' + data.threadDetails.GuestName + '">[Guest Name]</option>');
			$('#template-variable').append('<option value="' + variables.GuestCount + '">[Guest Count]</option>');
			if (variables.Status != 'I' && variables.Status != 'NP') {
				$('#template-variable').append('<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>');
			}
			$('#template-variable').append('<option value="' + moment(variables.CheckInDate).tz("America/Vancouver").format("MMM D, YYYY") + '">[Check In Date]</option>');
			$('#template-variable').append('<option value="' + moment(variables.CheckOutDate).tz("America/Vancouver").format("MMM D, YYYY") + '">[Check Out Date]</option>');

			$('#template-variable').append('<option value="' + variables.PropertyName + '">[Listing Name]</option>');
			$('#template-variable').append('<option value="' + variables.CleaningCost + '">[Cleaning Fee]</option>');
			$('#template-variable').append('<option value="' + (variables.ReservationCost - variables.CleaningCost) + '">[SubTotal]</option>');
			$('#template-variable').append('<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>');
			$('#template-variable').append('<option value="' + variables.PropertyAddress + '">[Listing Address]</option>');
			(variables.PhoneNumber != null && variables.PhoneNumber !="" )?$('#template-variable').append('<option value="' + variables.PhoneNumber.slice(variables.PhoneNumber.length - 4) + '">[Lock Pin Code]</option>'):"";
		}
		else if (data.threadDetails.Inquiries.length > 1) {

			//console.log(data.threadDetails.GuestName);
			$('#template-variable').append('<option value="' + data.threadDetails.GuestName + '">[Guest Name]</option>');
			//$('#template-variable').append('<option value="' + variables.GuestCount + '">[Guest Count]</option>');
			//if (variables.Status != 'I' && variables.Status != 'NP') {
			//	$('#template-variable').append('<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>');
			}
			//$('#template-variable').append('<option value="' + moment(variables.CheckInDate).format("MMM D, YYYY") + '">[Check In Date]</option>');
			//$('#template-variable').append('<option value="' + moment(variables.CheckOutDate).format("MMM D, YYYY") + '">[Check Out Date]</option>');
			//$('#template-variable').append('<option value="' + variables.PropertyName + '">[Listing Name]</option>');
			//$('#template-variable').append('<option value="' + variables.CleaningCost + '">[Cleaning Cost]</option>');
			//$('#template-variable').append('<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>');
			//$('#template-variable').append('<option value="' + variables.PropertyAddress + '">[Listing Address]</option>');
			//$('#template-variable').append('<option value="' + variables.PropertyDescription + '">[Listing Description]</option>');
			//$('#template-variable').append('<option value="' + variables.PropertyType + '">[Listing Type]</option>');
	}

	$('.editable').each(function () {
		this.contentEditable = true;
	});

	function onWorkerSendMessageClick() {
		$(document).on('click', '#worker-send-btn', function () {
				
			var threadId = $('.worker-active-thread').attr('data-thread-id');
			var workerId = $('.worker-active-thread').attr('data-worker-id');
			//alert(workerId + ' ' + threadId)
			var msg = $('#txtMsgContentWorker').val();
			if (msg != '') {
				$.ajax({
					type: 'POST',
					url: "/Messages/WorkerSendMessage",
					data: { workerId: workerId, threadId: threadId, messege: msg },
					dataType: "json",
					beforeSend: function () {
						$('#worker-send-btn').addClass('loading');
					},
					success: function (data) {
					
						if (data.success) {
							$('#txtMsgContentWorker').css('border-color', 'rgba(34,36,38,.15)');
							$('#txtMsgContentWorker').val('');
							$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
							$('#worker-send-btn').removeClass('orange').addClass('teal');
							$('#worker-send-btn').text('Send');
							$('.worker-active-thread').find('.content').find('.text').text(msg);
							$('.worker-active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
						}
						else {
							$('#txtMsgContentWorker').css('border-color', 'orange');
							$('#worker-send-btn').removeClass('teal').addClass('orange');
							$('#worker-send-btn').text('Retry');
						}
						$('#worker-send-btn').removeClass('loading');
						$('.worker-active-thread').attr('data-thread-id', data.threadId);
						$('#worker-inbox-list').attr('data-active-thread-id', data.threadId);
					}
				})
			}

		});
	}
	function onWorkerInboxClick() {
		//get the message and details of inquiry
		$(document).on('click', '.worker-message-thread', function () {
			var messageThread = $(this);
			$('#worker-inbox-list').children().css('border', 'none');
			$('#worker-inbox-list').children().css('background-color', '#fff', '!important');
			messageThread.css('border-left', '3px solid #00b5ad');
			messageThread.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
			var id = messageThread.attr('data-thread-id');
			var workerId = messageThread.attr('data-worker-id');

			$('#worker-inbox-list').attr('data-active-thread-id', id);

			$("#worker-dv_Conversation").html('');

			$("#worker-img_Loader").show();

			$.ajax({
				type: 'POST',
				url: "/Messages/LoadWorkerInboxMessage",
				data: { threadId: id, workerId: workerId },
				dataType: "json",
				async: true,
				success: function (data) {
					 
					$("#worker-img_Loader").hide();
					if (data.success) {
						//display the conversation

						if (data.Messages.length != 0) {
							//$('#worker-dvConvo').html('');
							$.each(data.Messages, function (index, value) {
								if (!value.IsMyMessage) {
									if (value.Type == 2) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>'+

												(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
												) +
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui orange small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
								else {
									if (value.Type == 2) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui orange small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
											'<h5>'  + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else {
										$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
											'<h5>'  + (value.Message !=null?value.Message:"")+ '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}   
							});
						}
						else {
							var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
							$('#worker-dv_Conversation').append(html);
						}
						$('#worker-contact-info').empty();
						$.each(data.contactInfos, function (index, contactInfo) {
							let buttons =
								`<button class="circular ui green icon button worker-call-button" data-tooltip="Call" data-position="left center">
									<i class="icon phone"></i>
								</button>
                                <button class="circular ui orange icon button worker-sms-button" data-tooltip="SMS" data-position="left center">
									<i class="icon comments"></i>
								</button>`;

							if (contactInfo.Type == 2) {
								buttons =
									`<button class="circular ui teal icon button worker-sms-button" data-tooltip="WhatsApp" data-position="left center">
							                                 <i class="icon whatsapp"></i>
							                             </button>`;
							} else if (contactInfo.Type == 3) {
								buttons =
									`<button class="circular ui blue icon button worker-sms-button" data-tooltip="Messenger" data-position="left center">
							                                 <i class="icon facebook messenger"></i>
							                             </button>`;
							}
							$('#worker-contact-info').append(`
								<div class="item" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}" >
									<div class="right floated content">
                                        ${ buttons}
									</div>
									<div class="content">${contactInfo.Value}</div>
								</div>
							`);
						});

					}
					$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
					//alert("after scroll click");
				}
			});

			$('.worker-message-thread').removeClass('worker-active-thread');
			$(this).addClass('worker-active-thread');

			
			
		});
	}
	function getSiteName(siteIndex) {
		switch (siteIndex) {
			case 0: return "Local";
			case 1: return "Airbnb";
			case 2: return "Vrbo";
			case 3: return "Booking";
			case 4: return "Homeaway";
		}
	}

	function onInboxClick() {
		//get the message and details of inquiry
		$(document).on('click', '.message-thread', function () {
			var messageThread = $(this);
			$('#inbox-list').children().css('border', 'none');
			$('#inbox-list').children().css('background-color', '#fff', '!important');
			messageThread.css('border-left', '3px solid #00b5ad');
			messageThread.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
			var id = messageThread.attr('data-thread-id');
			window.history.pushState(null, null, '?threadId=' + id);
			$('#inbox-list').attr('data-active-thread-id', id);
			$('#reservation-details-wrapper').removeClass('hidden').addClass('hidden');
			$('#reservation-details').removeClass('hidden').addClass('hidden');
			$('#booking-actions-active').hide();
			$('#confirmation-code-wrapper').hide();
			$('#booking-actions-pending').hide();
			$('#booking-actions-inquiry').hide();
			$('#booking-actions-withdraw-pre-approve').hide();
			$('#booking-actions-withdraw-special-offer').hide();
			$('#inquiry-note-wrapper').html('');
			$('#reservation-details-inquiry-id').val('');
			$('#dv_Conversation').html('');
			$("#img_Loader").show();
			$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
			$('#note-section').hide();
			$('#note-save').hide();
			$('#reservation-detail-host-image').hide();

			$.ajax({
				type: 'GET',
				url: "/Messages/GetMessageThread",
				data: { threadId: id },
				dataType: "json",
				async: true,
				success: function (data) {
					$("#dv_Conversation").html("");
					
					if (data.success) {
						if (data.user != "") {
							$('#txtMsgContent').attr("disabled", "disabled");
							$('#btn_Send').attr("disabled", "disabled");
							$('#btn_Clear').attr("disabled", "disabled");

							$('#txtMsgContent').val(data.user);
						}
						else {
							$('#txtMsgContent').removeAttr("disabled");
							$('#btn_Send').removeAttr("disabled");
							$('#btn_Clear').removeAttr("disabled");
							$('#txtMsgContent').val('');
						}
						$("#img_Loader").hide();
						getVariableList(data);

						//getTemplateMessage();

						//display the conversation
						if (data.messageThread.length != 0) {
							$.each(data.messageThread, function (index, value) {
								var imageHtml = '<div class="ui tiny images">';
								if (value.Images != null) {
									for (var x = 0; x < value.Images.length; x++) {
										imageHtml += '<img class="zoom" src="' + value.Images[x] + '">';
									}
								}
								imageHtml += '</div>';
								if (!value.IsMyMessage) {
									if (value.Type == 2) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											(
												value.MessageContent == null ?
													'' :
													(
														'<p>Transcription: ' + value.MessageContent + '</p>'
													)
											)
											+
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui orange small left pointing label message-label message-incoming">' +
											'<h5>' + value.MessageContent + '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
											'<h5>' + value.MessageContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
											'<h5>' + value.MessageContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
											'<h5>' + value.MessageContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
								else {
									if (value.Type == 2) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +

											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											(
												value.MessageContent == null ?
													'' :
													(
														'<p>Transcription: ' + value.MessageContent + '</p>'
													)
											)
											+
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui orange small right pointing label message-label message-outgoing">' +
											'<h5>' +
											(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
												'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
												'</div>' : '') + value.MessageContent + '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
											'<h5>' +
											(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
												'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
												'</div>' : '') + value.MessageContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
											'<h5>' +
											(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
												'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
												'</div>' : '') + value.MessageContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
											'<h5>' +
											(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
												'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
												'</div>' : '') + value.MessageContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
							});
						}

						else {
							var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
							$('#dv_Conversation').append(html);
						}
							
						var siteType = data.threadDetails.SiteType;

						messageThread.attr('data-reservation-id', siteType == 1 ? data.threadDetails.ConfirmationCode : siteType == 2 ? data.threadDetails.ReservationId : '');

						//$('#reservation-detail-property-name').text(data.threadDetails.PropertyName);
						$('#reservation-detail-guest-image').attr('src', data.threadDetails.GuestProfilePictureUrl);
						$('#reservation-detail-guest-name').text(data.threadDetails.GuestName);
						$('#reservation-detail-host-name').text(data.threadDetails.HostName);
						$('#reservation-detail-host-image').attr('src', data.threadDetails.HostPicture);

						$('#reservation-detail-guest-image').show();
						$('#reservation-detail-guest-name').show();
						$('#reservation-detail-host-name').show();

						
						$('#reservation-detail-host-image').show();
						var hasDefault = data.hasDefaultContact;
						$('#contact-info').empty();
                        $.each(data.contactInfos, function (index, contactInfo) {
                            let buttons =
                                `<button class="circular mini ui green icon button call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
                                <button class="circular mini ui orange icon button sms-button" data-tooltip="SMS">
									<i class="icon comments"></i>
								</button>`;

                            if (contactInfo.Type == 2) {
                                buttons =
                                    `<button class="circular mini ui teal icon button sms-button" data-tooltip="WhatsApp">
                                        <i class="icon whatsapp"></i>
                                    </button>`;
                            } else if (contactInfo.Type == 3) {
                                buttons =
                                    `<button class="circular mini ui blue icon button sms-button" data-tooltip="Messenger">
                                        <i class="icon facebook messenger"></i>
                                    </button>`;
							}
							var isDefault = "";
							if (hasDefault == false && index == 0) {
								isDefault = "checked";
							}
							else {
								isDefault=contactInfo.IsDefault ? "checked" : "";
							}
							$('#contact-info').append(`<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}">
															<div class="ui radio checkbox ${isDefault}">
															 <input type="radio" name="contact" ${isDefault}>
															<label>${contactInfo.Value} (${getSiteName(contactInfo.Source)})</label>
															</div><br/>
															${ buttons}
															<button class="circular mini ui icon button reassign-button" data-tooltip="Re-assign Contact Info">
																<i class="icon exchange"></i>
															</button>
														</div>`);
						});
			

						//start
						$('#reservations').html('');
					
						$.each(data.threadDetails.Inquiries, function (index, value) {
							//if (value.Status == 'I' || (value.Status == 'A' && value.IsActive) || value.Status == 'B') {
								var siteType = data.threadDetails.SiteType;

								messageThread.attr('data-reservation-id', siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.InquiryId : '');
								//var data-reservation - id  =(siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.ReservationId : '');
								var reservationId = siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.InquiryId : '';
								$('#reservation-detail-property-name').text(value.PropertyName);
								$('#reservation-detail-guest-image').attr('src', data.threadDetails.GuestProfilePictureUrl);
								$('#reservation-detail-guest-name').text(data.threadDetails.GuestName);
								$('#reservation-detail-host-name').text(data.threadDetails.HostName);
								$('#reservation-detail-host-image').attr('src', data.threadDetails.HostPicture);



								//$('#reservation-detail-status').text(reservation_status);
								//$('#reservation-detail-check-in').text(moment(value.CheckInDate).format("h:mm A MMM D,YYYY"));
								//$('#reservation-detail-check-out').text(moment(value.CheckOutDate).format("h:mm A MMM D, YYYY"));
								//$('#reservation-detail-confirmation-code').text(value.ConfirmationCode);
								//$('#reservation-details-value-id').val(value.valueId);
								//$('#reservation-detail-guest-count').text(value.GuestCount == 1 ? (value.GuestCount + ' Guest') : value.GuestCount > 1 ? (value.GuestCount + ' Guests') : '')

								//if (value.CheckInDate != null && value.CheckInDate != null) {
								//	$('#reservation-details').removeClass('hidden');
								//}
								var action = null;
								if (value.ConfirmationCode != null) {
									$('#confirmation-code-wrapper').show();
								}
								if (value.Status == "I" && value.CanWithdrawPreApprovalInquiry && !value.CanPreApproveInquiry && !value.CanDeclineInquiry) {
									//$('#booking-actions-withdraw-pre-approve').show();
									action = '<div><button class="ui blue button js-btn_withdraw-pre-approve ' + reservationId + '">Withdraw Pre-approve</button> </div>'
								}
								else if (value.Status == 'I' && !value.IsSpecialOfferSent) {
									//$('#booking-actions-inquiry').show();
									if (value.CheckInDate != null && value.CheckOutDate != null) {
									action = '<div>' +
										'<div class="three ui mini buttons">' +
										'<button style="margin: 1%" class="ui blue button js-btn_pre-approve ' + reservationId + '">Pre-Approve</button>' +
										'<button style="margin: 1%" class="ui blue button js-btn_decline-inquiry ' + reservationId + '">Decline</button>' +
										'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer ' + reservationId + '">Send Special Offer</button>' +
										'</div>' +
										'</div>';
									}

								}

								else if (value.IsSpecialOfferSent) {
									//$('#booking-actions-withdraw-special-offer').show();
									action = '<div><button class="ui blue button js-btn_withdraw-special-offer ' + reservationId + '" > Withdraw Special Offer</button ></div >';

								}

								if (value.Status == 'A' && value.IsActive) {
									//$('#note-section').show();
									//$('#note-save').show();

									//GetNotes(data.threadDetails.valueId);

									if (value.IsAltered) {
										action = '<div class="three ui mini buttons">' +
											'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration-booking ' + reservationId + '">Cancel Alteration</button>' +
											'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationId + '" >Cancel reservation</button>' +
											 ((value.Status == 'A' && value.IsActive) ? '<button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button>' : '');


											'</div>'
										}
									else {
										action = '<div class="three ui mini buttons">' +
											'<button style="margin: 1%" class="ui blue button js-btn_alter-booking ' + reservationId + '">Alter Reservation</button>' +
											'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationId + '">Cancel Reservation</button>' +
										    ((value.Status == 'A' && value.IsActive) ? '<div class="one ui mini buttons"><button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button></div>' : '');

											'</div>'
											
										//$('.js-btn_cancel-alter-booking').hide();
										//$('.js-btn_alter-booking').show();
									}
									//$('#booking-actions-active').show();
								}
								else if (siteType == 2 && value.Status =='A') {
									action = '<div class="three ui mini buttons">' +
										'<button style="margin: 1%" class="ui blue button js-btn_alter-booking-vrbo ' + reservationId + '">Alter Reservation</button>' +
										'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking-vrbo ' + reservationId + '">Cancel Reservation</button>' +
										((value.Status == 'A') ? '<div class="one ui mini buttons"><button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button></div>' : '');
									'</div>';
												}

								if (value.Status == 'B') {
									action = '<div class="two ui mini buttons">' +
										'<button style="margin: 1%" class="ui blue button js-btn_accept-booking ' + reservationId +'">Accept</button>' +
										'<button style="margin: 1%" class="ui blue button js-btn_decline-booking ' + reservationId +'">Decline</button>' +
										'</div>';
								}





								var reservation_status = value.Status;
								//Status of value
								if (reservation_status == 'I') { reservation_status = 'Inquiry'; }
								else if (reservation_status == 'SO') { reservation_status = 'Inquiry'; }
								else if (reservation_status == 'IC') { reservation_status = 'Inquiry-Cancelled'; }
								else if (reservation_status == 'A') { reservation_status = 'Confirmed'; }
								else if (reservation_status == 'B') { reservation_status = 'Booking'; }
								else if (reservation_status == 'BC') { reservation_status = 'Booking-Cancelled'; }
								else if (reservation_status == 'C') { reservation_status = 'Confirmed-Cancel'; }
								else if (reservation_status == 'NP') { reservation_status = 'Not Possible'; }

								var item = '<div style="border-bottom: solid;" class="inquiry-item" data-reservation-id="' + reservationId + '">' +
									'<h3 class="color-gray">' + value.PropertyName + '</h3>' +
									'<div class="ui three column grid">' +
									'<div id="reservation-details" class="row">' +
									'<div class="six wide column">' +
									'<h5 class="margin-0">Check in</h5>' +
									'<h6 id="reservation-detail-check-in" class="margin-0 gray">' + (value.CheckInDate == null ? 'Not Specified' : moment(value.CheckInDate).tz("America/Vancouver").format('MMMM D, YYYY')) + '</h6>' +
									'</div>' +
									'<div class="two wide column">' +
									'<h5 class="margin-0">&nbsp;</h5>' +
									'<h6 class="margin-0 gray">' +
									'<i class="arrow right icon"></i>' +
									'</h6>' +
									'</div>' +
									'<div class="six wide column">' +
									'<h5 class="margin-0">Check out</h5>' +
									'<h6 id="reservation-detail-check-out" class="margin-0 gray">' + (value.CheckOutDate == null ? 'Not Specified': moment(value.CheckOutDate).tz("America/Vancouver").format('MMMM D, YYYY')) + '</h6>' +
									'</div>' +
									'</div>' +
									(value.ConfirmationCode == '' || value.ConfirmationCode == null ? '' : '<h5 style="margin-top: 0">Confirmation Code:<span id="reservation-detail-confirmation-id" class="margin-left-10-px gray">' + value.ConfirmationCode + '</span></h5>') +
									'</div>' +
									'<h5>Number of guest:<span class="margin-left-10-px gray">' + value.GuestCount + '</span></h5>' +
									'<h5 style="margin-top: 0">Status:<span class="margin-left-10-px gray">' + reservation_status + '</span></h5>' +
									'<div id="'+reservationId+'">' +
									(action != null ? action : '') +
									'</div>' +
                                '</div></div>';

								$('#reservations').append(item);
								//if (value.IsActive && value.Status == 'A') {
								//	$('#reservations').append(item);
								//}
								//else if (value.Status != 'A' && value.Status != 'B') {
								//	$('#reservations').append(item);
								//}

								//$('#booking-actions-withdraw-pre-approve').hide();

								$('.ui.radio.checkbox').checkbox();
								$('#reservation-detail-host-image').show();
								$('#reservation-details-wrapper').removeClass('hidden');
								$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
							
						});
						//end
					}
				}
			});

			//if (firstRun == false) {
			//	$.ajax({
			//		type: 'POST',
			//		url: "/Messages/UpdateInboxStatus",
			//		data: { threadId: id },
			//		dataType: "json"
			//	})
			//}

			firstRun = false;
			$('.message-thread').removeClass('active-thread');
			$('.message-thread[data-thread-id="' + id + '"]').addClass('active-thread');
			document.getElementById('dvConvo').scrollIntoView()
			$("#btn_Send").removeAttr("disabled");
		});
	}

	function onTripClick() {
		$(document).on('click', '#trip-note-id', function () {
			
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			 //var inquiryId =$(this)
			var inquiryId = $(this).attr('data-inquiry-id');
			//var inquiryId = $(this).attr("class").replace("ui blue button js- ", "");
			//alert(threadId + " " + inquiryId + " " + hostId);
			if (threadId != '' && hostId != '' && threadId != null & hostId != null) {
				quickActionModal({
					hostId: hostId,
					threadId: threadId,
					inquiryId: inquiryId,
					page: 'messager'
				});
			}
		});
	}

	function SetForwardingClick() {
		$(document).on('click', '.edit-forwarding-number', function () {
			var id = $(this).parents('tr').attr('data-id');
			//$(this).parents('tr').children('td:eq(2)').text()
			$('#our-number-id').val(id);
			$('#ourNumber').val($(this).parents('tr').children('td:eq(0)').text());
			$('#forwarding-Number').val($(this).parents('tr').children('td:eq(1)').text());
			$('#callLength').val($(this).parents('tr').children('td:eq(3)').text());
			$('#voicemailMessage').val($(this).parents('tr').children('td:eq(5)').text());
			$('#forwardingMessage').val($(this).parents('tr').children('td:eq(6)').text());
			$.ajax({
				type: 'GET',
				url: "/Messages/GetProperty",
				data:{id:id},
				success: function (data) {
					$('#host-table tbody').empty();
					if (data.assignedHost.length == 0 && data.availableHost.length == 0) {
						$('#host-table tbody').append('<tr><td colspan="2" style="text-align: center">No Host available</td></tr>');
					}
					else {
						$.each(data.assignedHost, function (index, value) {
							var rowTemplate = '<tr>' +
								'<td>' +
								'<div class="ui toggle checkbox">' +
								'<input data-id="' + value.Id + '" type="checkbox" class="available-host" checked> <label></label>' +
								'</div>' +
								'</td>' +
								'<td data-id="' + value.Id + '">' + value.Firstname+' '+value.Lastname + '</td>' +
								'<td> ' + value.Username +'</td>' +
								'<td> ' + GetSiteType(value.SiteType) + '</td>' +
								'</tr>';
							$('#host-table tbody').append(rowTemplate);
						});

						$.each(data.availableHost, function (index, value) {
							var rowTemplate = '<tr>' +
								'<td>' +
								'<div class="ui toggle checkbox">' +
								'<input data-id="' + value.Id + '" type="checkbox" class="available-host"> <label></label>' +
								'</div>' +
								'</td>' +
								'<td data-id="' + value.Id + '">' + value.Firstname + ' '+value.Lastname + '</td>' +
									'<td> ' + value.Username + '</td>' +
									'<td> ' + GetSiteType(value.SiteType) + '</td>' +
									'</tr>';
							$('#host-table tbody').append(rowTemplate);
						});
					}



					$('#child-property-table tbody').empty();
					if (data.assignedproperties.length == 0 && data.availableproperties.length == 0 && data.assignedParentproperties.length == 0 && data.availableParentproperties.length == 0) {
						$('#child-property-table tbody').append('<tr><td colspan="2" style="text-align: center">No property available</td></tr>');
					}
					else {
						$.each(data.assignedParentproperties, function (index, value) {
							var rowTemplate = '<tr>' +
								'<td>' +
								'<div class="ui toggle checkbox">' +
								'<input data-id="' + value.Id + '" type="checkbox" class="available-property-chk" checked> <label></label>' +
								'</div>' +
								'</td>' +
								'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
								'<td> ' + GetSiteType(value.SiteType) + '</td>' +
								'<td>' + "" + '<td>' +
								'</tr>';
							$('#child-property-table tbody').append(rowTemplate);
						});
						$('#forwarding-modal').modal('refresh');
						$.each(data.availableParentproperties, function (index, value) {
							var rowTemplate = '<tr>' +
								'<td>' +
								'<div class="ui toggle checkbox">' +
								'<input data-id="' + value.Id + '" type="checkbox" class="available-property-chk"> <label></label>' +
								'</div>' +
								'</td>' +
								'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
								'<td> ' + GetSiteType(value.SiteType) + '</td>' +
								'<td>' +" " + '<td>' +
								'</tr>';
							$('#child-property-table tbody').append(rowTemplate);
						});
						$.each(data.assignedproperties, function (index, value) {
							var rowTemplate = '<tr>' +
								'<td>' +
								'<div class="ui toggle checkbox">' +
								'<input data-id="' + value.Id + '" type="checkbox" class="available-property-chk" checked> <label></label>' +
								'</div>' +
								'</td>' +
								'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
								'<td> ' + GetSiteType(value.SiteType) + '</td>' +
								'<td>' + value.Host + '<td>' +
								'</tr>';
							$('#child-property-table tbody').append(rowTemplate);
						});
						$('#forwarding-modal').modal('refresh');
						$.each(data.availableproperties, function (index, value) {
							var rowTemplate = '<tr>' +
								'<td>' +
								'<div class="ui toggle checkbox">' +
								'<input data-id="' + value.Id + '" type="checkbox" class="available-property-chk"> <label></label>' +
								'</div>' +
								'</td>' +
								'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
								'<td> ' + GetSiteType(value.SiteType) + '</td>' +
								'<td>' + value.Host + '<td>' +
								'</tr>';
							$('#child-property-table tbody').append(rowTemplate);
						});
						$('#forwarding-modal').modal('refresh');
					}
				}
			});


			$('#forwarding-modal').modal('show');
			$('#forwarding-modal').modal('refresh');
			//alert(id + " " + $(this).parents('tr').children('td:eq(0)').text() + " " + $(this).parents('tr').children('td:eq(1)').text())
		});
	}

	function GetSiteType(site) {
		if (site == 0)
			return "Parent Property";
		else if (site == 1)
			return "Airbnb";
		else if (site == 2)
			return "Vrbo";
		else if (site == 3)
			return "Booking";
		else if (site == 4)
			return "Homeaway";
	}

	function SaveForwardingNumber() {
		$(document).on('click', '#SaveForwarding', function () {
			var PropertyIds = [];
			var ToRemovePropertyIds = [];

			var HostIds = [];
			var ToRemoveHostIds = [];
			
			$('.available-host').each(function (index, element) {
				if ($(this).is(':checked')) {
					HostIds .push($(this).attr('data-id'));
				}
				else {
					ToRemoveHostIds.push($(this).attr('data-id'));
				}
			});

			$('.available-property-chk').each(function (index, element) {
				if ($(this).is(':checked')) {
					PropertyIds.push($(this).attr('data-id'));
				}
				else {
					ToRemovePropertyIds.push($(this).attr('data-id'));
				}
			});
			$.ajax({
				type: 'POST',
				url: "/Messages/SaveForwardingNumber",
				data: {
					Id: $('#our-number-id').val(), ForwardingNumber: $('#forwarding-Number').val(), CallLength: $('#callLength').val(),
					VoicemailMessage: $('#voicemailMessage').val(), CallforwadingMessage: $('#forwardingMessage').val(), PropertyIds: PropertyIds, ToRemovePropertyIds: ToRemovePropertyIds, ToRemoveHostIds: ToRemoveHostIds, HostIds: HostIds
				},
				success: function (data) {
					if (data.success) {
					
						swal("Saved", "", "success");
						$('#forwarding-modal').modal('hide');
						twilioNumberTable.ajax.reload(null, false);
					}
					else {
						swal("Error in saving data", "", "error");
					}
				}
			});
		});
	}

	var tripnote = '<div id="note-section" class="row">' +
		'<h3 style="color: #008590">Notes <a href="#" class="add-note-button" data-tooltip="Add" style="float: right;"><i class="teal plus circle icon add-field-icon"></i></a></h3>' +
		'<br><br>' +
		'</div>' +
		'<div class="ui form">' +
		' <input id="reservation-details-inquiry-id" type="hidden" />' +
		'<div id="inquiry-note-wrapper" class="inquiry-note-wrapper">' +
		'</div>' +
		'<div id="note-save" class="hidden">' +
		'<div class="row">' +
		'<div class="sixteen wide column">' +
		' <button type="button" class="medium green ui button right floated btn-save-note">' +
		'Save' +
		'</button>' +
		'</div>' +
		'</div>' +
		' </div>';
	function onWithdrawSpecialOffer() {
		$(document).on('click', '.js-btn_withdraw-special-offer', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_withdraw-special-offer ", "");

			swal({
				title: "Are you sure you want to withdraw special offer ?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, withdraw it!",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Booking/WithdrawSpecialOffer",
						data: { hostId: hostId, threadId: threadId },
						success: function (data) {
							if (data.success) {
								debugger;
								//$('#booking-actions-inquiry').show();
								
								//$('#booking-actions-withdraw-special-offer').hide();
								var action = '<div>' +
									'<div class="three ui mini buttons">' +
									'<button style="margin: 1%" class="ui blue button js-btn_pre-approve ' + reservationCode + '">Pre-Approve</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_decline-inquiry ' + reservationCode + '">Decline</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer ' + reservationCode + '">Send Special Offer</button>' +
									'</div>' +
									'</div>';
								$('#' + reservationCode + '').html(action)
								
								swal("Successfully withdraw special offer.", "", "success");
							}
							else {
								swal("An error occured on withdraw special offer", "", "error");
							}
						}
					});
				});
		});
	}

	function onSendSpecialOffer() {
		$("#special-offer-form select[name=propertyId]").on('change', function () {
			checkSpecialOffer();
		});
		$("#special-offer-form select[name=guestCount]").on('change', function () {
			checkSpecialOffer();
		});
	}
	function onAlterCheck() {
		$("#alter-booking-form select[name=alterGuestCount]").on('change', function () {
			checkAlterAvailability();
		});
		$("#alter-booking-form select[name=propertyId]").on('change', function () {
			checkAlterAvailability();
		});
		$("#alter-booking-form select[name=propertyId]").on('change', function () {
			checkAlterAvailability();
		});
	}


	function PreApproveClick() {

		$(document).on('click', '.js-btn_pre-approve', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_pre-approve ", "");

			swal({
				title: "Are you sure you want to Pre-approve ?",
				text: "",
				type: "warning",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes, Pre-Approved",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Booking/PreApproveInquiry",
						data: { siteType: siteType, threadId: threadId, hostId: hostId },
						success: function (data) {
							if (data.success) {
								var action = '<div><button class="ui blue button js-btn_withdraw-pre-approve ' + reservationId + '">Withdraw Pre-approve</button> </div>'
								$('#' + reservationCode + '').html(action);
							}
							else {
								swal("An error occured on Pre-approve", "", "error");
							}
						}
					});
				});
		});
	}

	function WithdrawPreApproveClick() {

		$(document).on('click', '.js-btn_withdraw-pre-approve', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_withdraw-pre-approve ", "");

			swal({
				title: "Are you sure you want to withdraw Pre-approved ?",
				text: "",
				type: "warning",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Booking/WithdrawPreApproveInquiry",
						data: { siteType: siteType, threadId: threadId, hostId: hostId },
						success: function (data) {
							if (data.success) {
								var action = '<div>' +
									'<div class="three ui mini buttons">' +
									'<button style="margin: 1%" class="ui blue button js-btn_pre-approve ' + reservationCode + '">Pre-Approve</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_decline-inquiry ' + reservationCode + '">Decline</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer ' + reservationCode + '">Send Special Offer</button>' +
									'</div>' +
									'</div>';
								$('#' + reservationCode + '').html(action);
								swal("Successfully withdraw Pre-approved.", "", "success");
							}
							else {
								swal("An error occured on withdraw Pre-approve", "", "error");
							}
						}
					});
				});
		});
	}
	function DeclineInquiryClick() {
		$(document).on('click', '.js-btn_decline-inquiry', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var siteType = $('.active-thread').attr('data-site-type');

			swal({
				title: "Are you sure you want to decline?",
				text: "",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Decline",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
				inputPlaceholder: "Write message"
			},
				function (message) {
					$.ajax({
						type: 'POST',
						url: "/Booking/DeclineInquiry",
						data: { threadId: threadId, hostId: hostId, message: message },
						success: function (data) {
							if (data.success) {
								$('#booking-actions-inquiry').show();
								$('#booking-actions-withdraw-pre-approve').hide();
								$('#booking-actions-withdraw-special-offer').hide();
								swal("Successfully Decline.", "", "success");
							}
							else {
								swal("An error occured on Decline", "", "error");
							}
						}
					});
				});
		});
	}

	function checkAlterAvailability() {
		var hostId = $('.active-thread').attr('data-host-id');
		var listingId = $("#alter-booking-form select[name=propertyId]").val();
		var startDate = $("#alter-booking-form input[name=checkIn]").val();
		var endDate = $("#alter-booking-form input[name=checkOut]").val();
		var guestId = $('.active-thread').attr('data-guest-id');
		var numOfGuest = $("#alter-booking-form select[name=alterGuestCount]").val();
	
	
		if (hostId != '' && listingId != '' && startDate != '' && endDate != '' && guestId != '' && numOfGuest != '') {
			$.ajax({
				type: 'POST',
				url: "/Booking/CheckSpecialOfferValidity",
				data: {
					hostId: hostId,
					listingId: listingId,
					guestId: guestId,
					startDate: startDate,
					endDate: endDate,
					numOfGuest: numOfGuest,
					currentStartDate: currentStartDate,
					currentEndDate: currentEndDate
				},
				success: function (data) {
					if (data.result.IsAvailable) {
						$('#error-message-wrapper').hide();
						$('#special-offer-summary').show();
						$("#alter-booking-form input[name=subtotal]").val(data.result.Subtotal);
						$('#guest-amount-to-pay').text(data.result.GuestPays);
						$('#host-amount-to-earn').text(data.result.HostEarns);
						$('#alter-booking-modal').modal('refresh');
						$('#booking-submit-alter-btn').removeClass('disabled');
					}
					else {
						$('#special-offer-summary').hide();
						$('#booking-submit-alter-btn').removeClass('disabled').addClass('disabled');
						$('#alter-success-message-wrapper').find('p').text('Those dates are not available');
						$('#alter-success-message-wrapper').show();
					}
				}
			});
		}
	}

	function checkSpecialOffer() {
		var hostId = $('.active-thread').attr('data-host-id');
		var listingId = $("#special-offer-form select[name=propertyId]").val();
		var startDate = $("#special-offer-form input[name=startDate]").val();
		var endDate = $("#special-offer-form input[name=endDate]").val();
		var guestId = $('.active-thread').attr('data-guest-id');
		var numOfGuest = $("#special-offer-form select[name=guestCount]").val();
		if (hostId != '' && listingId != '' && startDate != '' && endDate != '' && guestId != '' && numOfGuest != '') {
			$.ajax({
				type: 'POST',
				url: "/Booking/CheckSpecialOfferValidity",
				data: {
					hostId: hostId,
					listingId: listingId,
					guestId: guestId,
					startDate: startDate,
					endDate: endDate,
					numOfGuest: numOfGuest,
				},
				success: function (data) {
				
					if (data.result.IsAvailable) {
						$('#error-message-wrapper').hide();
						$('#special-offer-summary').show();
						$("#special-offer-form input[name=subTotal]").val(data.result.Subtotal);
						$('#guest-amount-to-pay').text(data.result.GuestPays);
						$('#host-amount-to-earn').text(data.result.HostEarns);
						$('#special-offer-modal').modal('refresh');
						$('#submit-special-offer-btn').removeClass('disabled');
					}
					else {
						$('#special-offer-summary').hide();
						$('#submit-special-offer-btn').removeClass('disabled').addClass('disabled');
						$('#error-message-wrapper').find('p').text('Those dates are not available');
						$('#error-message-wrapper').show();
					}
				}
			});
		}
	}

	function onSpecialOfferSubmit() {
		$(document).on('click', '#submit-special-offer-btn', function () {
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var listingId = $("#special-offer-form select[name=propertyId]").val();
			var startDate = $("#special-offer-form input[name=startDate]").val();
			var endDate = $("#special-offer-form input[name=endDate]").val();
			var numOfGuest = $("#special-offer-form select[name=guestCount]").val();
			var price = $("#special-offer-form input[name=subTotal]").val();

			if (hostId != '' && listingId != '' && startDate != '' && endDate != '' && threadId != '' && numOfGuest != '' && price != '') {
				$.ajax({
					type: 'POST',
					url: "/Booking/SendSpecialOffer",
					data: {
						hostId: hostId,
						listingId: listingId,
						threadId: threadId,
						numOfGuest: numOfGuest,
						startDate: startDate,
						endDate: endDate,
						price: price
					},
					success: function (data) {
						console.log(data);
						if (data.success) {

							var action = '<div><button class="ui blue button js-btn_withdraw-special-offer ' + code + '" > Withdraw Special Offer</button ></div >';

							$('#' + code + '').html(action);
							$('#special-offer-modal').modal('hide');

							//$('#booking-actions-inquiry').hide();
							//$('#booking-actions-withdraw-special-offer').show();
							swal("Special offer has been sent .", "", "success");
						}
						else {
							swal("Error in sending special offer", "", "error");
						}
					}
				});
			}
		});
	}

	function onSendSpecialOfferClick() {
		$(document).on('click', '.js-btn_send-special-offer', function () {
			code = $(this).attr("class").replace("ui blue button js-btn_send-special-offer ", "");
			//var siteType = $('.active-thread').attr('data-site-type');
			//var inquiry = getInquiry(code, sitetype);
			var hostId = $('.active-thread').attr('data-host-id');
			var properties = getPropertyByHost(hostId);

			debugger;
			$("#special-offer-form select[name=propertyId]").empty();
			for (i = 0; i < properties.length; i++) {
				$("#special-offer-form select[name=propertyId]").append("<option value =" + properties[i].ListingId + ">" + properties[i].Name + "</option>");
			}

			$('#special-offer-modal').find('#success-message-wrapper').hide();
			$('#special-offer-modal').find('#error-message-wrapper').hide();
			$('#special-offer-summary').hide();
			$("#special-offer-form select[name=propertyId]").dropdown('clear');
			$("#special-offer-form input[name=startDate]").val('');
			$("#special-offer-form input[name=endDate]").val('');
			$("#special-offer-form select[name=guestCount]").dropdown('set value',1);
			$('#special-offer-modal').modal('show');
			$('#special-offer-start-date').calendar({
				type: 'date', onChange: function (date, text, mode) {
					$("#special-offer-form input[name=startDate]").val(text);
					checkSpecialOffer();
				}
			});
			$('#special-offer-end-date').calendar({
				type: 'date', onChange: function (date, text, mode) {
					$("#special-offer-form input[name=endDate]").val(text);
					checkSpecialOffer();
				}
			});
		});
	}


	$("#modal-template-variable").on('change', function () {
		var variable = $(this).children(':selected').text();//.replace('[', '').replace(']', '');
		$('#template-content').append('<i class="icon tags"></i>' + variable);
	});

	function onSelectVariable() {
		$('#template-variable').on('change', function () {
			var variable = $(this).val();
			console.log(variable)
			if (variable == "null" || variable == "undefined")
				$('#txtMsgContent').append("");
			else
				//$('#txtMsgContent').append(variable);
				$('#txtMsgContent').val($('#txtMsgContent').val() + variable);
				//$('#txtMsgContent').text($('#txtMsgContent').text() + variable);
			//if(option == 1)
			//{
			//    var firstname = $('.active-thread').find('.author').text();
			//    $('#txtMsgContent').insertAtCaret(firstname);
			//}
		});
	}

	function rebindEvent() {
		$('#template-variable').off('change');
		onSelectVariable();
	}

	function onEditTemplate() {
		$(document).on('click', '.edit-template-btn', function () {
			var id = $(this).parents('tr').attr("data-template-id");
			$('#template-title').val('');
			$('#template-content').val('');
			$('#template-id').val(0);
			$.ajax({
				type: "POST",
				url: "/Messages/GetTemplate",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						$('#template-id').val(id);
						$('#template-title').val(data.template.Title);
						$('#template-content').text("");
						$('#template-content').append(data.template.Message.replace(/\[/g, '<i class="icon tags"></i>['));
						$('#template-list').hide();
						$('#template-empty-error-message').hide();
						$('#template-success-message').hide();
						$('#template-duplicate-error-message').hide();
						$('#template-server-error-message').hide();
						$("#teplate-clear-btn").show();
						$('.add-edit-template-action-buttons').show();
						$('#add-template-form').show();
					}
				}
			});
		});
	}

	$("#teplate-clear-btn").click(function () {
		$("#template-content").text("");
	});

	function onTemplateFormSubmit() {
		$(document).on('click', '#teplate-add-edit-btn', function () {
			var id = $('#template-id').val();
			$('#template-empty-error-message').hide();
			$('#template-success-message').hide();
			$('#template-duplicate-error-message').hide();
			$('#template-server-error-message').hide();
			var title = $('#template-title').val();
			var message = $('#template-content').text();

			if (title == '' && message == '') {
				$('#template-empty-error-message').show();
			}
			else {
				if (id == 0) // add
				{
					$.ajax({
						type: 'POST',
						url: "/Messages/AddTemplate",
						data: { title: title, message: message },
						dataType: "json",
						success: function (data) {
							if (data.success) {
								$('#template-id').val(0);
								$('#template-title').val('');
								$('#template-content').val('');
								$('#template-success-message').show();
								refreshTemplateTableData();
								getTemplateMessage();
							}
							else if (!data.success && data.is_duplicate) {
								$('#template-duplicate-error-message').show();
							}
							else {
								$('#template-server-error-message').show();
							}
						}
					})
				}
				else // edit
				{
					$.ajax({
						type: 'POST',
						url: "/Messages/EditTemplate",
						data: { Id: id, Title: title, Message: message },
						dataType: "json",
						success: function (data) {
							if (data.success) {
								$('#template-id').val(0);
								$('#template-title').val('');
								$('#template-content').val('');
								$('#template-success-message').show();
								refreshTemplateTableData();
								getTemplateMessage();
							}
							else if (!data.success && data.is_duplicate) {
								$('#template-duplicate-error-message').show();
							}
							else {
								$('#template-server-error-message').show();
							}
						}
					})
				}
			}
		});
	}

	function onDeleteTemplate() {
		$(document).on('click', '.delete-template-btn', function () {
			var id = $(this).parents('tr').attr("data-template-id");
			$.ajax({
				type: "POST",
				url: "/Messages/DeleteTemplate",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						refreshTemplateTableData();
						getTemplateMessage();
					}
				}
			});
		});
	}

	function refreshTemplateTableData() {
		tableTemplates.ajax.reload(null, false);
	}

	function loadTemplateTableData() {
		tableTemplates = $('#templates-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Messages/GetTemplateList",
				"type": 'POST'
			},
			"columns": [
				{ "data": "Title" },
				{ "data": "Message", "orderable": false },
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-template-id', data.Id);
			}
		});
	}

	function onBackToTemplateList() {
		$(document).on('click', '#back-to-template-list-btn', function () {
			$('#add-template-form').hide();
			$("#teplate-clear-btn").hide();
			$('.add-edit-template-action-buttons').hide();
			$('#template-list').show();
		});
	}

	function onAddTemplate() {
		$(document).on('click', '#add-template-btn', function () {
			$('#template-list').hide();
			$('#template-empty-error-message').hide();
			$('#template-success-message').hide();
			$('#template-duplicate-error-message').hide();
			$('#template-server-error-message').hide();
			$("#teplate-clear-btn").show();
			$('.add-edit-template-action-buttons').show();
			$('#template-title').val('');
			$('#template-content').text('');
			$('#template-id').val(0);
			$('#add-template-form').show();
		});
	}

	function onSaveTemplate() {
		$(document).on('click', '#save-template-btn', function () {
			$('#add-template-form').hide();
			$('#template-list').show();
			$("#teplate-clear-btn").hide();
			$('.add-edit-template-action-buttons').hide();
			$('#manage-template-message-modal').modal('show');

		});
	}

	function getTemplateMessage() {
		var drp = $('#template-messages');
		$.ajax({
			type: 'POST',
			url: "/Messages/GetTemplates",
			dataType: "json",
			success: function (data) {
				drp.empty();
				if (data.success) {
					drp.append('<option value="">Select Template</option>');
					$.each(data.templates, function (i, v) {
						drp.append('<option value="' + v.Message + '">' + v.Title + '</option>');
					});
					drp.dropdown();
				}
			}
		})
	}

	function onTemplateMessageSelect() {
		$(document).on('change', '#template-messages', function () {
			if ($(this).val() != '') {
				var templateMessage = $(this).val();
				var extractedMessage = "";
				var potentialVariable = "";

				var isValidTemplate = true;
				var missingVariable = "";
				var extracting = false;

				for (var i = 0; i < templateMessage.length; i++) {
					if (templateMessage[i] != '[' && !extracting) {
					
						extractedMessage = extractedMessage + templateMessage[i];
						extracting = false;
					}
					else if (templateMessage[i] == ']') {
						potentialVariable = potentialVariable + templateMessage[i];
		
						var potentialVariableValue = $("#template-variable option:contains('" + potentialVariable + "')").val();
						//potentialVariable = potentialVariable.replace(' ', '');
						if (potentialVariableValue != "" && potentialVariableValue != null && potentialVariableValue != "null" && potentialVariableValue != "undefined") {
							//console.log(potentialVariableValue);
							extractedMessage = extractedMessage + $("#template-variable option:contains('" + potentialVariable + "')").val();
							
						}
						else {
							//console.log(potentialVariableValue);
							isValidTemplate = false;
							missingVariable = missingVariable + potentialVariable + " ";
								
						}

						potentialVariable = "";		
						extracting = false;
					}	
					else {
						potentialVariable = potentialVariable + templateMessage[i];
						extracting = true;
					}
				}


				if (isValidTemplate) {
					$('#txtMsgContent').val(extractedMessage);
				}
				else {
					swal("You can`t use this template because " + missingVariable + "is missing in this reservation", "", "error");
				}
				
			}
		});
	}

	$("#btn_Clear").click(function () {
		$("#txtMsgContent").val("");
	});

	function onButtonSendSMSClick() {
		$(document).on('click', '#btn_Send_SMS', function () {
			$('#btn_Send_SMS').attr('disabled', true);
			var contact = $('#contact-info').find('.checked');
			let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
			let type = $(contact).parents(".field").attr('data-type');
			var guestId = $('.active-thread').attr('data-guest-id');
			var formData = new FormData();
			for (var i = 0; i < $('#mms-pic').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#mms-pic').get(0).files[i]);
			}
			formData.append("to", phoneNumber);
			formData.append("message", $('#txtMsgContent').val());
			formData.append("type", type);
			formData.append("guestId", guestId);
			$.ajax({
				type: 'POST',
				url: "/SMS/Send",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#btn_Send_SMS').attr('disabled', false);
						toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						$('.item.message-thread.active-thread').click();
					} else {
						toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});
		});
	}
	function onButtonSendMessageClick() {
		$(document).on('click', '#btn_Send', function () {
			var siteType = $('.active-thread').attr('data-site-type');
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var msg = $('#txtMsgContent').val();
			if (msg != '') {
				$.ajax({
					type: 'POST',
					url: "/Messages/SendMessageToInbox",
					data: { siteType: siteType, hostId: hostId, threadId: threadId, message: msg },
					dataType: "json",
					beforeSend: function () {
						$('#btn_Send').addClass('loading');
					},
					success: function (data) {

						if (data.success) {
							$('#txtMsgContent').css('border-color', 'rgba(34,36,38,.15)');
							$('#txtMsgContent').val('');
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' + msg + '</h5>' +
								'<p>' + moment().format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
							$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
							$('#btn_Send').removeClass('orange').addClass('teal');
							$('#btn_Send').text('Send');
							$('.active-thread').find('.content').find('.text').text(msg);
							$('.active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
						}
						//else {
						//	$('#txtMsgContent').css('border-color', 'orange');
						//	$('#btn_Send').removeClass('teal').addClass('orange');
						//	$('#btn_Send').text('Retry');
						//}
						$('#btn_Send').removeClass('loading');
					}
				});
			}

		});
	}

	function onButtonCallClick() {
	    // $(document).on('click', '#btn_Call', function () {
	    //     if (!device) {
	    //         return $('#btn_Call').prop('disabled', true)
	    //     }
	    //     var threadId = $('.active-thread').attr('data-thread-id');
	    //     $.ajax({
	    //         type: 'GET',
	    //         url: "/Messages/ThreadInfo",
	    //         data: { threadId: threadId },
	    //         dataType: "json",
	    //         beforeSend: function () {
	    //             $('#btn_Call').addClass('loading');
	    //         },
	    //         success: function (data) {
	    //             console.log(data);
	    //             $('#btn_Call').removeClass('loading');

	    //             var params = {
	    //                 From: userId,
	    //                 To: data.data.ContactNumber
	    //             };

	    //             if (device) {
	    //                 $('.mini.modal .phone-number').text(data.data.ContactNumber);
	    //                 $('.mini.modal .header.name').text(data.data.Name);
	    //                 $('.mini.modal .avatar.image').attr('src', data.data.ProfilePictureUrl);
	    //                 $('.mini.modal .avatar.image').show();
	    //                 device.connect(params);
	    //                 openOutgoingCallModal();
	    //             }
	    //         }
	    //     })
	    // });
	}

	//Remove by Joem because Paulo change the logic of sending message
	//function onButtonSendSMSMessageClick() {
	//    $(document).on('click', '#btn_SMS_Send', function () {
	//        var siteType = $('.active-thread').attr('data-site-type');
	//        var threadId = $('.active-thread').attr('data-thread-id');
	//        var hostId = $('.active-thread').attr('data-host-id');
	//        var msg = $('#txtMsgContent').val();
	//        if (msg != '') {
 //               console.log
	//            $.ajax({
	//                type: 'POST',
	//                url: "/Messages/ReplyMessageSMSMessage",
	//                data: { threadId: threadId, message: msg },
	//                dataType: "json",
	//                beforeSend: function () {
	//                    $('#btn_SMS_Send').addClass('loading');
	//                },
	//                success: function (data) {
	//                    console.log(data);
	//                    if (data.success) {
	//                        $('#txtMsgContent').css('border-color', 'rgba(34,36,38,.15)');
	//                        $('#txtMsgContent').val('');
	//                        //debugger;
	//                        $("#dvConvo").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
	//							'<h5>' + msg + '</h5>' +
	//							'<p>' + moment().format("h:mm A MMM D, YYYY") + ' (SMS TO ' + data.to + ')</p>' +
	//							'</div>');
	//                        $('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
	//                        $('#btn_SMS_Send').removeClass('orange').addClass('teal');
	//                        $('#btn_SMS_Send').text('Send');
	//                        $('.active-thread').find('.content').find('.text').text(msg);
	//                        $('.active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
	//                    }
	//                    else {
	//                        $('#txtMsgContent').css('border-color', 'orange');
	//                        $('#btn_SMS_Send').removeClass('teal').addClass('orange');
	//                        $('#btn_SMS_Send').text('Retry');
	//                    }
	//                    $('#btn_SMS_Send').removeClass('loading');
	//                }
	//            })
	//        }

	//    });
	//}

	function refreshThreadMessages() {
		var threadId = $('.active-thread').attr('data-thread-id');
		var hostId = $('.active-thread').attr('data-hosting-account-id');
		$.ajax({
			type: 'GET',
			url: "/Messages/RefreshThreadMessages",
			data: { threadId: threadId, hostId: hostId },
			dataType: "json",
			async: true,
			success: function (data) {
				console.log(data);
				if (data.success) {
					$("#dv_Conversation").html("");
					if (data.messages.length > 0) {
						var msgtxt = data.messages[data.messages.length - 1].MsgContent;
						$('.message-thread[data-thread-id="' + threadId + '"]').find('.content').find('.text').text(msgtxt.length > 15 ? (msgtxt.substr(0, 14) + '...') : msgtxt);
					}
					$.each(data.messages, function (index, value) {
						if (!value.IsIncomingMessage) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
								'<h5>' + value.MsgContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
						}
						else {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' + value.MsgContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
						}
					});
					//$('#dv_Conversation').scrollTop($('#dv_Conversation')[0].scrollHeight);
				}
				setTimeout(function () { refreshThreadMessages(); }, 10000);
			}
		});
	}

	function checkNewHostMessages() {
		var threadId = $('.active-thread').attr('data-thread-id');
		var hostId = $('.active-thread').attr('data-hosting-account-id');
		$.ajax({
			type: "GET",
			url: "/Messages/CheckNewHostMessage",
			data: {
				threadId: threadId,
				hostId: hostId
			},
			success: function (data) {
				console.log(data);
				if (data.success) {
					if (data.hasNew) {
						var inboxWrapper = $('#inbox-list');
						$.each(data.newInbox, function (index, value) {
							var threadDiv = $('.message-thread[data-thread-id="' + value.Inbox.ThreadId + '"]');
							var template = threadDiv.clone();
							threadDiv.find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
							threadDiv.remove();
							template.find('.text').text(value.Inbox.LastMessage.length > 15 ? (value.Inbox.LastMessage.substr(0, 14) + '...') : value.Inbox.LastMessage);
							template.find('.date').text(moment(value.Inbox.LastMessageAt).format("MMM D, YYYY"));
							template.find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red').addClass('green');
							inboxWrapper.prepend(template);
							threadDiv.remove();

							if (threadDiv.hasClass('active-thread')) {
								$("#dv_Conversation").html("");
								$.each(data.messages, function (index, value) {
									if (!value.IsIncomingMessage) {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
											'<h5>' + value.MsgContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
									else {
										$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
											'<h5>' + value.MsgContent + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								});
								$('#dv_Conversation').scrollTop($('#dv_Conversation')[0].scrollHeight);
							}
						});
					}
					if (data.messages.length != 0) {
						var lastMessage = data.messages[data.messages.length - 1];
						$('#dv_Conversation').scrollTop($('#dv_Conversation')[0].scrollHeight);
						$('.active-thread').find('.text').text(lastMessage.MsgContent.length > 15 ? (lastMessage.MsgContent.substr(0, 14) + '...') : lastMessage.MsgContent);
						$('.active-thread').find('.date').text(moment(lastMessage.CreatedDate).isSame(Date.now(), 'day') ? moment(lastMessage.CreatedDate).format("h:mm A") : moment(lastMessage.CreatedDate).format("MMM D, YYYY"));
						$("#dv_Conversation").html("");
						$.each(data.messages, function (index, value) {
							if (!value.IsIncomingMessage) {
								$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
									'<h5>' + value.MsgContent + '</h5>' +
									'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
									'</div>');
							}
							else {
								$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
									'<h5>' + value.MsgContent + '</h5>' +
									'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
									'</div>');
							}
						});
						$('#dv_Conversation').scrollTop($('#dv_Conversation')[0].scrollHeight);
						$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);

					}
					setTimeout(function () { checkNewHostMessages(); }, 10000);
				}
			}
		});
	}

	$.urlParam = function (name) {
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results == null) {
			return null;
		}
		else {
			return results[1] || 0;
		}
	}

	function onDeclineBooking() {
		$(document).on('click', '.js-btn_decline-booking', function () {
			$('#decline-booking-modal').modal({
				onApprove: function (e) {
					var reservationCode = $('.active-thread').attr('data-reservation-id');
					var siteType = $('.active-thread').attr('data-site-type');
					var hostId = $('.active-thread').attr('data-host-id');
					var message = $("#decline-reservation-form textarea[name=message]").val();

					$.ajax({
						type: 'POST',
						url: "/Booking/DeclineBooking",
						data: { siteType: siteType, hostId: hostId, reservationCode: reservationCode, message: message },
						success: function (data) {
							if (data.success) {
								$("#decline-reservation-form textarea[name=message]").val('');
								//RefreshSingleBooking(hostId, reservationCode);
								$('#booking-actions-pending').hide();
								$('#reservation-detail-status').text('Declined');
								swal("Booking successfully declined.", "", "success");
							}
							else {
								swal("Error on decline booking", "", "error");
							}
						}
					});
				}
			})
				.modal('show')
				.modal('refresh');
		});
	}

	function onAcceptBooking() {
		$(document).on('click', '.js-btn_accept-booking1', function () {
			$('#accept-booking-modal').modal({
				onApprove: function (e) {
					var reservationCode = $('.active-thread').attr('data-reservation-id');
					var siteType = $('.active-thread').attr('data-site-type');
					var hostId = $('.active-thread').attr('data-host-id');
					var message = $("#accept-reservation-form textarea[name=message]").val();

					$.ajax({
						type: 'POST',
						url: "/Booking/AcceptBooking",
						data: { siteType: siteType, hostId: hostId, reservationCode: reservationCode, message: message },
						success: function (data) {
							if (data.success) {
								$("#accept-reservation-form textarea[name=message]").val('');
								//RefreshSingleBooking(hostId, reservationCode);
								$('#booking-actions-pending').hide();
								$('#reservation-detail-status').text('Accepted');
								swal("Successfully accept booking.", "", "success");
							}
							else {
								swal("Error on accepting booking", "", "error");
							}
						}
					});
				}
			})
				.modal('show')
				.modal('refresh');
		});
	}

	$("#alterGuestCount").change(function () {
		//alert("Handler for .change() called.");
	});


	function onAlterBooking() {
		$(document).on('click', '.js-btn_alter-booking', function () {
			var siteType = $('.active-thread').attr('data-site-type');
			var hostId = $('.active-thread').attr('data-host-id');
			//var confirmationCode = $('#reservation-detail-confirmation-code').text();
			code = $(this).attr("class").replace("ui blue button js-btn_alter-booking ", "");
			debugger;
			$('#alter-success-message-wrapper').hide();
			$('#alter-error-message-wrapper').hide();
			$('#booking-submit-alter-btn').addClass('disabled');
			$('#alter-booking-guest-count').off('change');
			$('#alter-booking-property-id').off('change');
			//$('#alter-checkin').calendar({
			//	type: 'date'
			//});
			//$('#alter-checkout').calendar({
			//	type: 'date'
			//});


			var inquiry = getInquiry(code, siteType);
			var properties = getPropertyByHost(hostId);
			currentEndDate = moment(inquiry.CheckOutDate).tz("America/Vancouver").format("MMM D, YYYY");
			currentStartDate = moment(inquiry.CheckInDate).tz("America/Vancouver").format("MMM D, YYYY");

			$("#alter-booking-form select[name=propertyId]").empty();
			for (i = 0; i < properties.length; i++) {
				$("#alter-booking-form select[name=propertyId]").append("<option value =" + properties[i].ListingId + ">" + properties[i].Name + "</option>");
			}

			$("#alter-booking-form select[name=propertyId]").dropdown('set selected', inquiry.PropertyId);
			$("#alter-booking-form input[name=checkIn]").val(moment(inquiry.CheckInDate).tz("America/Vancouver").format("MMM D, YYYY"));
			$("#alter-booking-form input[name=checkOut]").val(moment(inquiry.CheckOutDate).tz("America/Vancouver").format("MMM D, YYYY"));

			$("#alter-booking-form select[name=alterGuestCount]").dropdown('set selected', inquiry.GuestCount);
			//$("#alter-booking-form select[name=alterChildrenCount]").dropdown('set selected', inquiry.Children);

			$('#alter-booking-form input[name =subtotal]').val(inquiry.ReservationCost);
			$('#alter-booking-modal').modal('show');

			$('#alter-checkIn').calendar({
				type: 'date'
				, onChange: function (date, text, mode) {
					//$("#alter-booking-form input[name=checkIn]").val(text);
					checkAlterAvailability();
				}
			});

			$('#alter-checkOut').calendar({
				type: 'date'
				, onChange: function (date, text, mode) {
					//$("#alter-booking-form input[name=checkOut]").val(text);
					
					checkAlterAvailability();	
				}
			});
		});
	}

	
		

	function onVrboAlterBooking() {
		$(document).on('click', '.js-btn_alter-booking-vrbo', function () {
			var siteType = $('.active-thread').attr('data-site-type');
			var hostId = $('.active-thread').attr('data-host-id');
			//var confirmationCode = $('#reservation-detail-confirmation-code').text();
			code = $(this).attr("class").replace("ui blue button js-btn_alter-booking-vrbo ", "");

			var inquiry = getInquiry(code, siteType);
			inqId = inquiry.Id
			var properties = getPropertyByHost(hostId);

			debugger;
			$("#vrbo-alter-booking-form select[name=propertyId]").empty();
			for (i = 0; i < properties.length; i++) {
				$("#vrbo-alter-booking-form select[name=propertyId]").append("<option value ="+properties[i].ListingId+">" + properties[i].Name + "</option>"); 
			}
	

			debugger;
			$('#vrbo-alter-success-message-wrapper').hide();
			$('#vrbo-alter-error-message-wrapper').hide();
			//$('#vrbo-booking-submit-alter-btn').addClass('disabled');
			$('#vrbo-alter-booking-guest-count').off('change');
			$('#vrbo-alter-booking-property-id').off('change');
			//$('#alter-checkin').calendar({
			//	type: 'date'
			//});
			//$('#alter-checkout').calendar({
			//	type: 'date'
			//});
			$("#vrbo-alter-booking-form select[name=propertyId]").dropdown('set selected', inquiry.PropertyId);
			$("#vrbo-alter-booking-form input[name=vrbo-checkIn]").val(moment(inquiry.CheckInDate).tz("America/Vancouver").format("MMM D, YYYY"));
			$("#vrbo-alter-booking-form input[name=vrbo-checkOut]").val(moment(inquiry.CheckOutDate).tz("America/Vancouver").format("MMM D, YYYY"));

			$("#vrbo-alter-booking-form select[name=alterAdultCount]").dropdown('set selected', inquiry.Adult);
			$("#vrbo-alter-booking-form select[name=alterChildrenCount]").dropdown('set selected',inquiry.Children);

			$('#vrbo-alter-booking-form input[name =subtotal]').val(inquiry.ReservationCost);

			$('#vrbo-alter-booking-modal').modal('show');

			$('#vrbo-alter-checkIn').calendar({
				type: 'date'
				, onChange: function (date, text, mode) {
					//$("#alter-booking-form input[name=checkIn]").val(text);
					//checkAlterAvailability();
				}
			});

			$('#vrbo-alter-checkOut').calendar({
				type: 'date'
				, onChange: function (date, text, mode) {
					//$("#alter-booking-form input[name=checkOut]").val(text);

					//checkAlterAvailability();
				}
			});
		});
	}

	function getInquiry(code,siteType) {
		debugger;
		var inquiry = null;
		$.ajax({
			type: 'GET',
			url: "/Booking/GetInquiry",
			async: false,
			data: { code: code, siteType: siteType },
			success: function (data) {
				debugger
				if (data.success) {
					inquiry = data.inquiry;
				}
			}
		});
		return inquiry;
	}

	function getPropertyByHost(hostId) {
		var properties = null;
		$.ajax({
			type: 'GET',
			url: "/Booking/GetPropertyByHost",
			async: false,
			data: { hostId: hostId },
			success: function (data) {
				if (data.success) {
					properties = data.properties;
				}
			}
		});
		return properties;
	}
	function onCheckReservationAvailability() {
		var form = $('#alter-reservation-form');
		var reservationCode = $('#alter-booking-reservation-code').val();
		var hostId = $('.active-thread').attr('data-hosting-account-id');
		var listingId = $('#alter-booking-property-id').val();
		var checkinDate = $('#alter-booking-check-in').val();
		var checkoutDate = $('#alter-booking-check-out').val();
		var price = $('#alter-booking-subtotal').val();
		var guests = $('#alter-booking-guest-count').val();

		$.ajax({
			type: 'POST',
			url: "/Booking/AlterBooking",
			data: {
				hostId: hostId,
				reservationCode: reservationCode,
				listingId: listingId,
				checkinDate: checkinDate,
				checkoutDate: checkoutDate,
				price: price,
				guests: guests,
				submitAlteration: false
			},
			success: function (data) {
				console.log(data);
				if (data.success) {
					$('#alter-error-message-wrapper').hide();
					$('#alter-success-message-wrapper').find('.header').text(data.message);
					$('#alter-success-message-wrapper').find('p').text('Difference : ' + data.difference);
					$('#alter-success-message-wrapper').show();
					$('#booking-submit-alter-btn').removeClass('disabled');
				}
				else {
					$('#alter-success-message-wrapper').hide();
					$('#alter-error-message-wrapper').find('p').text(data.message);
					$('#alter-error-message-wrapper').show();
					$('#booking-submit-alter-btn').addClass('disabled');
				}
			}
		});
	}

	function onSubmitAlterBooking() {
		$(document).on('click', '#booking-submit-alter-btn', function () {
			$('#booking-submit-alter-btn').addClass('loading')
			$('#booking-submit-alter-btn').addClass('disable')
			var reservationCode = code;
			var hostId = $('.active-thread').attr('data-host-id');
			var listingId = $("#alter-booking-form select[name=propertyId]").val();
			var startDate = $("#alter-booking-form input[name=checkIn]").val();
			var endDate = $("#alter-booking-form input[name=checkOut]").val();
			var guestId = $('.active-thread').attr('data-guest-id');
			var numOfGuest = $("#alter-booking-form select[name=alterGuestCount]").val();
			var price = $('#alter-booking-form input[name =subtotal]').val();
			var siteType = $('.active-thread').attr('data-site-type');
			$.ajax({
				type: 'POST',
				url: "/Booking/AlterBooking",
				data: {
					siteType:siteType,
					hostId: hostId,
					reservationCode: reservationCode,
					listingId: listingId,
					checkinDate: startDate,
					checkoutDate: endDate,
					price: price,
					guests: numOfGuest,
					submitAlteration: true
				},
				success: function (data) {
					debugger;
					console.log(data);
					if (data.success) {
						$('#booking-submit-alter-btn').removeClass('loading')
						$('#booking-submit-alter-btn').removeClass('disable')
						$('#alter-booking-modal').modal('hide');
						action = '<div><div class="two ui mini buttons">' +

							'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration-booking ' + reservationCode + '">Cancel Alteration</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationCode + '" >Cancel reservation</button>' +
							'</div></div>';
						$('#' + reservationCode + '').html(action);
						swal("Your request has been submitted.", "You’ll be notified when your guest responds..", "success");
						//RefreshSingleBooking(hostId, reservationCode);
						//$('.js-btn_alter-booking').hide();
						//$('.js-btn_cancel-alter-booking').show();
					}
					else {
						$('#alter-booking').modal('hide');
						swal(data.message, "", "error");
					}
				}
			});
		});
	}


	function onSubmitAlterBookingVrbo() {
		$(document).on('click', '#vrbo-booking-submit-alter-btn', function () {
			var reservationCode = code;
			var hostId = $('.active-thread').attr('data-host-id');
			var listingId = $("#vrbo-alter-booking-form select[name=propertyId]").val();
			var startDate = $("#vrbo-alter-booking-form input[name=vrbo-checkIn]").val();
			var endDate = $("#vrbo-alter-booking-form input[name=vrbo-checkOut]").val();
			var guestId = $('.active-thread').attr('data-guest-id');
			var numOfAdult = $("#vrbo-alter-booking-form select[name=alterAdultCount]").val();
			var numOfChildren = $("#vrbo-alter-booking-form select[name=alterChildrenCount]").val();
			var threadId = $('.active-thread').attr('data-thread-id');
			var price = $('#vrbo-alter-booking-form input[name =subtotal]').val();
			var siteType = $('.active-thread').attr('data-site-type');
			debugger;
			$.ajax({
				type: 'POST',
				url: "/Booking/AlterBooking",
				data: {
					siteType: siteType,
					hostId: hostId,
					reservationCode: reservationCode,
					listingId: listingId,
					checkinDate: startDate,
					checkoutDate: endDate,
					price: price,
					adults: numOfAdult,
					children: numOfChildren,
					threadId: threadId,
					//guests: numOfGuest,	
					submitAlteration: true,
					alterType: 2,
					id: inqId
				},
				success: function (data) {
					debugger;
					console.log(data);
					if (data.success) {

						$('#vrbo-alter-booking-modal').modal('hide');
						action = '<div><div class="two ui mini buttons">' +

							'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration-booking-vrbo ' + reservationCode + '">Cancel Alteration</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationCode + '" >Cancel reservation</button>' +
							'</div></div>';
						$('#' + reservationCode + '').html(action);
						swal("Your request has been submitted.", "You’ll be notified when your guest responds..", "success");
						//RefreshSingleBooking(hostId, reservationCode);
						//$('.js-btn_alter-booking').hide();
						//$('.js-btn_cancel-alter-booking').show();
					}
					else {
						$('#vrbo-alter-booking').modal('hide');
						swal(data.message, "", "error");
					}
				}
			});
		});
	}
	function onCancelAlterationBooking() {
		$(document).on('click', '.js-btn_cancel-alter-booking', function () {
			//var reservationCode = $('#reservation-detail-confirmation-code').text();
			var reservationCode = $(this).attr("class").replace("ui blue button js-btn_cancel-alter-booking ", "");
			var hostId = $('.active-thread').attr('data-hosting-account-id');
			swal({
				title: "Are you sure you want to cancel this alteration ?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Booking/CancelAlteration",
						data: { hostId: hostId, reservationCode: reservationCode },
						success: function (data) {
							console.log(data);
							if (data.success) {
								RefreshSingleBooking(hostId, reservationCode);
								//$('.js-btn_alter-booking').show();
								//$('.js-btn_cancel-alter-booking').hide();
								var action = '<div class="two ui mini buttons">' +
									'<button style="margin: 1%" class="ui blue button js-btn_alter-booking ' + reservationCode + '">Alter Reservation</button>' +
									'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationCode + '">Cancel Reservation</button>' +
									'</div>';
								$('#' + reservationCode + '').html(action);
								swal("Successfully cancel alteration.", "", "success");
							}
							else {
								swal("An error occured on cancellation of booking alter", "", "error");
							}
						}
					});
				});
		});
	}

	function RefreshSingleBooking(hostId, reservationCode) {
		$.ajax({
			type: 'GET',
			url: "/Booking/RefreshSingleBooking",
			data: { hostId: hostId, reservationCode: reservationCode },
			success: function (data) {
				console.log(data);
				if (data.success) {
					console.log('success on refresh single booking');
				}
				else {
					console.log('error on refresh single booking');
				}
			}
		});
	}

	// ------------------
	// ------------------
	// -- Start Notes ---
	// ------------------
	// ------------------

	function onAddNote() {
		$(document).on('click', '.add-note-button', function () {
			var count = $('#inquiry-note-wrapper').children().length;
			var inquiryNoteWrapper = $('#inquiry-note-wrapper');
			if (count < 5) {
				var noteHtml = '<div class="note">' +
					'<div class="fields">' +
					'<div class="twelve wide field">' +
					'<select class="ui fluid search dropdown select-note">' +
					'<option value="1">Note</option>' +
					'<option value="2">Flight Arrival</option>' +
					'<option value="3">Flight Departure</option>' +
					'<option value="4">Meeting</option>' +
					'<option value="5">Drive</option>' +
					'<option value="6">Cruise</option>' +
					'</select>' +
					'</div>' +
					'<div class="four wide field">' +
					'<div class="note-actions">' +
					'<a href="#" class="delete-note-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
					'<a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<div class="two wide field"></div>' +
					'<div class="fourteen wide field">' +
					'<div class="note-fields">' +
					'<div class="fields">' +
					'<input type="text" name="InquiryNote" placeholder="Inquiry Note">' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</div>';
				$('.inquiry-note-wrapper').find('.add-note-button').remove();
				$('.inquiry-note-wrapper').append(noteHtml);
				activateDropdown();
			}
			$('#note-save').removeClass('hidden');
		});
	}

	function onDeleteNote() {
		$(document).on("click", ".delete-note-button", function () {
			var parent = $(this).parents('.note');
			var noteWrapper = $('#inquiry-note-wrapper');
			parent.remove();

			// remove save button if no notes
			if ($('.inquiry-note-wrapper').children().length == 0) {
				$('#note-save').addClass('hidden');
			}
			noteWrapper.children().last().find('.note-actions').empty();
			noteWrapper.children().last().find('.note-actions')
				.append('<a href="#" class="delete-note-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>');
			noteWrapper.children().last().find('.note-actions')
				.append('<a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>');
			if (noteWrapper.children().length == 0) {
				$.ajax({
					type: "POST",
					url: "/Messages/DeleteAllNoteForInquiry",
					data: { InquiryId: $('#reservation-details-inquiry-id').val() },
					dataType: "json",
					success: function (data) {
						console.log(data);
					}
				});
			}
		});
	}

	function onSaveNote() {
		$(document).on('click', '.btn-save-note', function () {
			// note to programmer (inquiry id is extracted in the left panel of messaging inbox list)
			var InquiryId = $('#reservation-details-inquiry-id').val();
			console.log(InquiryId);
			var Note = [];
			$('.note').each(function (index, element) {
				var noteType = $(this).find('.select-note').dropdown('get value');
				// note
				if (noteType == 1) {
					var inquiryNote = $(this).find('input[name="InquiryNote"]').val();
					Note.push({
						InquiryId: InquiryId,
						NoteType: noteType,
						InquiryNote: inquiryNote

					});
				}
				// flight arrival
				else if (noteType == 2) {
					var people = $(this).find('input[name="People"]').val();
					var airline = $(this).find('input[name="Airline"]').val();
					var flightNumber = $(this).find('input[name="FlightNumber"]').val();
					var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
					var airportCodeDeparture = $(this).find('input[name="AirportCodeDeparture"]').val();
					var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
					var airportCodeArrival = $(this).find('input[name="AirportCodeArrival"]').val();
					Note.push({
						InquiryId: InquiryId,
						People: people,
						Airline: airline,
						NoteType: noteType,
						FlightNumber: flightNumber,
						DepartureDateTime: departureDateTime,
						AirportCodeDeparture: airportCodeDeparture,
						ArrivalDateTime: arrivalDateTime,
						AirportCodeArrival: airportCodeArrival
					});
				}
				// flight departure
				else if (noteType == 3) {
					var people = $(this).find('input[name="People"]').val();
					var airline = $(this).find('input[name="Airline"]').val();
					var flightNumber = $(this).find('input[name="FlightNumber"]').val();
					var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
					var airportCodeDeparture = $(this).find('input[name="AirportCodeDeparture"]').val();
					var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
					var airportCodeArrival = $(this).find('input[name="AirportCodeArrival"]').val();
					Note.push({
						InquiryId: InquiryId,
						People: people,
						Airline: airline,
						NoteType: noteType,
						FlightNumber: flightNumber,
						DepartureDateTime: departureDateTime,
						AirportCodeDeparture: airportCodeDeparture,
						ArrivalDateTime: arrivalDateTime,
						AirportCodeArrival: airportCodeArrival
					});
				}
				//meeting
				else if (noteType == 4) {
					var meetingPlace = $(this).find('input[name="MeetingPlace"]').val();
					var people = $(this).find('input[name="People"]').val();
					var numberOfPeople = $(this).find('input[name="NumberOfPeople"]').val();
					var meetingDateTime = $(this).find('input[name="MeetingDateTime"]').val();
					var identifiableFeature = $(this).find('input[name="IdentifiableFeature"]').val();
					Note.push({
						InquiryId: InquiryId,
						NoteType: noteType,
						MeetingPlace: meetingPlace,
						People: people,
						NumberOfPeople: numberOfPeople,
						MeetingDateTime: meetingDateTime,
						IdentifiableFeature: identifiableFeature
					});
					console.log(Note);
				}
				//drive in
				else if (noteType == 5) {
					var people = $(this).find('input[name="People"]').val();
					var departureCity = $(this).find('input[name="DepartureCity"]').val();
					var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
					var carType = $(this).find('input[name="CarType"]').val();
					var carColor = $(this).find('input[name="CarColor"]').val();
					var plateNumber = $(this).find('input[name="PlateNumber"]').val();
					var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
					Note.push({
						InquiryId: InquiryId,
						People: people,
						NoteType: noteType,
						DepartureCity: departureCity,
						DepartureDateTime: departureDateTime,
						CarType: carType,
						CarColor: carColor,
						PlateNumber: plateNumber,
						ArrivalDateTime: arrivalDateTime
					});
				}
				// cruise
				else if (noteType == 6) {
					var people = $(this).find('input[name="People"]').val();
					var cruiseName = $(this).find('input[name="CruiseName"]').val();
					var departurePort = $(this).find('input[name="DeparturePort"]').val();
					var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
					var arrivalPort = $(this).find('input[name="ArrivalPort"]').val();
					var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
					Note.push({
						InquiryId: InquiryId,
						People: people,
						NoteType: noteType,
						CruiseName: cruiseName,
						DeparturePort: departurePort,
						DepartureDateTime: departureDateTime,
						ArrivalPort: arrivalPort,
						ArrivalDateTime: arrivalDateTime
					});
				}
			});
			if (Note.length > 0 && InquiryId != '') {
				$.ajax({
					type: "POST",
					url: "/Messages/SaveNote",
					data: { InquiryId: InquiryId, list: JSON.stringify(Note) },
					dataType: "json",
					success: function (data) {
						swal("Note has been saved successfully.", "", "success");
					},
					error: function (error) {
						swal("Error connecting to server.", "", "error");
					}
				})
			}
		});
	}

	// start onchange note type
	function onSelectedNoteChange() {
		$(document).on('change', '.select-note', function () {
			var noteType = $(this).dropdown('get value');
			var noteFields = '';
			var fieldWrapper = $(this).parents('.note').find('.note-fields');

			fieldWrapper.empty();

			// Note
			if (noteType == 1) {
				noteFields = '<div class="fields">' +
					'<input type="text" name="InquiryNote" placeholder="Inquiry Note">' +
					'</div>';
			}
			// Flight Arrival
			else if (noteType == 2) {
				noteFields = '<div class="fields">' +
					'<input type="text" name="People" placeholder="People">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="Airline" placeholder="Airline">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="FlightNumber" placeholder="Flight Number">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival">' +
					'</div>';
			}
			// Flight Departure
			else if (noteType == 3) {
				noteFields = '<div class="fields">' +
					'<input type="text" name="People" placeholder="People">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="Airline" placeholder="Airline">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="FlightNumber" placeholder="Flight Number">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival">' +
					'</div>';
			}
			// Meeting
			else if (noteType == 4) {
				noteFields = '<div class="fields">' +
					'<input type="text" name="MeetingPlace" placeholder="Meeting Place">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="People" placeholder="People">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="NumberOfPeople" placeholder="Number of People">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="MeetingDateTime" placeholder="Meeting Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="IdentifiableFeature" placeholder="Identifiable Feature">' +
					'</div>';
			}
			// Drive in
			else if (noteType == 5) {
				noteFields = '<div class="fields">' +
					'<input type="text" name="People" placeholder="People">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="DepartureCity" placeholder="Departure City">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="CarType" placeholder="Type of car">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="CarColor" placeholder="Color of car">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="PlateNumber" placeholder="Plate Number">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time">' +
					'</div>' +
					'</div>';
			}
			// Cruise
			else if (noteType == 6) {
				noteFields = '<div class="fields">' +
					'<input type="text" name="People" placeholder="People">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="CruiseName" placeholder="Cruise Name">' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="DeparturePort" placeholder="Departure Port">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time">' +
					'</div>' +
					'</div>' +
					'<div class="fields">' +
					'<input type="text" name="ArrivalPort" placeholder="Arrival Port">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time">' +
					'</div>' +
					'</div>';
			}

			fieldWrapper.append(noteFields);
			$('.ui.calendar.dt').calendar({ type: 'datetime' });
		});
	}

	function GetNotes(inquiryId) {
		$.ajax({
			type: "GET",
			url: "/Messages/GetNotes",
			data: { InquiryId: inquiryId },
			dataType: "json",
			success: function (data) {
				if (data.result.length > 0) $('#note-save').removeClass('hidden');
				$.each(data.result, function (index, value) {
					var type = value.Notes.NoteType;
					var noteHtml = '<div class="note">' +
						'<div class="fields">' +
						'<div class="twelve wide field">' +
						'<select class="ui fluid search dropdown select-note">' +
						'<option ' + (type == 1 ? 'selected' : '') + ' value="1">Note</option>' +
						'<option ' + (type == 2 ? 'selected' : '') + ' value="2">Flight Arrival</option>' +
						'<option ' + (type == 3 ? 'selected' : '') + ' value="3">Flight Departure</option>' +
						'<option ' + (type == 4 ? 'selected' : '') + ' value="4">Meeting</option>' +
						'<option ' + (type == 5 ? 'selected' : '') + ' value="5">Drive</option>' +
						'<option ' + (type == 6 ? 'selected' : '') + ' value="6">Cruise</option>' +
						'</select>' +
						'</div>' +
						'<div class="four wide field">' +
						'<div class="note-actions">' +
						'<a href="#" class="delete-note-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
						'<a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<div class="two wide field"></div>' +
						'<div class="fourteen wide field">' +
						'<div class="note-fields">' +
						'<div class="fields">';
					var noteFields = '';
					var noteWrapper = $('#inquiry-note-wrapper');

					// Note
					if (type == 1) {
						noteFields =
							'<input type="text" name="InquiryNote" placeholder="Inquiry Note" value="' + value.Notes.InquiryNote + '">';
					}
					// Flight Arrival
					else if (type == 2) {
						noteFields =
							'<input type="text" name="People" placeholder="People" value="' + value.Notes.People + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="Airline" placeholder="Airline" value="' + value.Notes.Airline + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="FlightNumber" placeholder="Flight Number" value="' + value.Notes.FlightNumber + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.Notes.DepartureDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure" value="' + value.Notes.AirportCodeDeparture + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.Notes.ArrivalDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival" value="' + value.Notes.AirportCodeArrival + '">';
					}
					// Flight Departure
					else if (type == 3) {
						noteFields =
							'<input type="text" name="People" placeholder="People" value="' + value.Notes.People + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="Airline" placeholder="Airline" value="' + value.Notes.Airline + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="FlightNumber" placeholder="Flight Number" value="' + value.Notes.FlightNumber + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.Notes.DepartureDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure" value="' + value.Notes.AirportCodeDeparture + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.Notes.ArrivalDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival" value="' + value.Notes.AirportCodeArrival + '">';
					}
					// Meeting
					else if (type == 4) {
						noteFields =
							'<input type="text" name="MeetingPlace" placeholder="Meeting Place" value="' + value.Notes.MeetingPlace + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="People" placeholder="People" value="' + value.Notes.People + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="NumberOfPeople" placeholder="Number of People" value="' + value.Notes.NumberOfPeople + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="MeetingDateTime" placeholder="Meeting Date/Time" value="' + moment(value.Notes.MeetingDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="IdentifiableFeature" placeholder="Identifiable Feature" value="' + value.Notes.IdentifiableFeature + '">';
					}
					// Drive in
					else if (type == 5) {
						noteFields =
							'<input type="text" name="People" placeholder="People" value="' + value.Notes.People + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="DepartureCity" placeholder="Departure City" value="' + value.Notes.DepartureCity + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.Notes.DepartureDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="CarType" placeholder="Type of car" value="' + value.Notes.CarType + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="CarColor" placeholder="Color of car" value="' + value.Notes.CarColor + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="PlateNumber" placeholder="Plate Number" value="' + value.Notes.PlateNumber + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.Notes.ArrivalDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>';
					}
					// Cruise
					else if (type == 6) {
						noteFields =
							'<input type="text" name="People" placeholder="People" value="' + value.Notes.People + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="CruiseName" placeholder="Cruise Name" value="' + value.Notes.CruiseName + '">' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="DeparturePort" placeholder="Departure Port" value="' + value.Notes.DeparturePort + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.Notes.DepartureDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>' +
							'</div>' +
							'<div class="fields">' +
							'<input type="text" name="ArrivalPort" placeholder="Arrival Port" value="' + value.Notes.ArrivalPort + '">' +
							'</div>' +
							'<div class="ui calendar dt fields">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.Notes.ArrivalDateTime).tz("America/Vancouver").format('MMMM D, YYYY h:mm A') + '">' +
							'</div>';
					}
					noteHtml += (noteFields + '</div></div></div></div></div>');
					noteWrapper.append(noteHtml);
					$('.ui.calendar.dt').calendar({ type: 'datetime' });
					//$('.ui.dropdown').dropdown('set selected', type);
                    $('.search.ui.dropdown').dropdown({ on: 'click' });
				});
			}
		});
	}

	function activateDropdown() {
		$('.search.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
	}

	jQuery.fn.extend({
		insertAtCaret: function (myValue) {
			return this.each(function (i) {
				if (document.selection) {
					//For browsers like Internet Explorer
					this.focus();
					sel = document.selection.createRange();
					sel.text = myValue;
					this.focus();
				}
				else if (this.selectionStart || this.selectionStart == '0') {
					//For browsers like Firefox and Webkit based
					var startPos = this.selectionStart;
					var endPos = this.selectionEnd;
					var scrollTop = this.scrollTop;
					this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
					this.focus();
					this.selectionStart = startPos + myValue.length;
					this.selectionEnd = startPos + myValue.length;
					this.scrollTop = scrollTop;
				} else {
					this.value += myValue;
					this.focus();
				}
			})
		}
	});
	// ------------------
	// ------------------
	// --- End Notes ----
	// ------------------
	// ------------------
});

