﻿$(document).ready(function () {
	var tableEmails;
	LoadEmails();
	DeleteEmail();
	GetEmailDetails();
	AddEmail();
	SaveEmail();
	SendEmailClick();
	UploadEmailAttachment();
	SyncEmail();
	AddGmail();
	function AddGmail() {
		$(document).on('click', '.add-gmail', function (e) {
			$.ajax({
				type: 'POST',
				url: "/Email/AddGmail",
				success: function (data) {
					alert(data.success);
					if (data.success) {
						toastr.success("Gmail Succesfully saved!", null, { timeOut: 3000, positionClass: "toast-top-right" });
						tableEmails.ajax.reload(null, false);
					} else {
						toastr.error("Error while Saving Email", null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				}
			});
		});
	}
	function SendEmailClick() {
		$(document).on('click', '#send-btn-email', function () {
			$('#send-btn-email').attr('disabled', true);
			var renterId = $('.active-thread').attr('data-id');
			var threadId = $('.active-thread').attr('data-thread-id');
			var formData = new FormData();
			for (var i = 0; i < $('#file-pic').get(0).files.length; ++i) {
				formData.append("Attachments[" + i + "]", $('#file-pic').get(0).files[i]);
			}
			formData.append("renterId", renterId);
			formData.append("threadId", threadId);
			formData.append("message", $('#txtMsgContent').val());
			$.ajax({
				type: 'POST',
				url: "/Email/Send",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#send-btn-email').attr('disabled', false);
						$('#txtMsgContent').val('');
						$('.active-thread').attr('data-thread-id', data.threadId);
						$('.active-thread').click();
						$('#inbox-list').attr('data-active-thread-id', data.threadId);
						$('#file-upload').empty();
						$('#file-pic').val('');
						toastr.success("Email sent!", null, { timeOut: 3000, positionClass: "toast-top-right" });
					} else {
						toastr.error("Failed sending email!", null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error("Failed sending email!", null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});
			$('#send-btn-email').attr('disabled', false);
		});
	}
	function UploadEmailAttachment() {
		$(document).on('click', '#add-attachment', function (e) {
			$('#file-pic').attr('accept', "");
			$('#file-pic').click();
		});
	}
	function LoadEmails() {
		tableEmails = $('#email-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Email/GetEmailAccount",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "Email" },
				{ "data": "Actions" }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-email-id', data.Id);
			}
		});
	}
	function GetEmailDetails() {
		$(document).on('click', '.edit-email', function (e) {
			var id = $(this).parents('tr').attr('data-email-id');
			var email = $(this).parents('tr').find('td:eq(0)').text();
			$('#user-email').val(email);
			$('#email-account-modal').attr('email-id', id);
			$('#email-account-modal').modal('show');
		});
	}
	function SyncEmail() {
		$(document).on('click', '.sync-email', function (e) {
			var id = $(this).parents('tr').attr('data-email-id');
			$.ajax({
				type: "POST",
				url: "/Email/SyncEmail",
				data: { id: id },
				dataType: "json",
				async: true,
				success: function (data) {

				}
			});
		});
	}
	function SaveEmail() {
		$(document).on('click', '#email-account-save', function (e) {
			var id = $('#email-account-modal').attr('email-id');
			var formData = new FormData();
			formData.append("Id", id);
			formData.append("Email", $('#user-email').val())
			formData.append("Password", $('#email-password').val());

			$.ajax({
				type: 'POST',
				url: "/Email/SaveEmail",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				async: false,
				success: function (data) {
					if (data.success) {
						toastr.success("Email Succesfully saved!", null, { timeOut: 3000, positionClass: "toast-top-right" });
						tableEmails.ajax.reload(null, false);
						$('#email-account-modal').modal('hide');
					} else {
						toastr.error("Error while Saving Email", null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				}
			});
		});
	}
	function AddEmail() {
		$(document).on('click', '.add-email', function (e) {
			$('#email-account-modal').attr('email-id', '0');
			$('#user-email').val('');
			$('#email-password').val('');
			$('#email-account-modal').modal('show');
		});
	}
	function DeleteEmail() {
		$(document).on('click', '.delete-email', function (e) {
			var id = $(this).parents('tr').attr('data-email-id');
			alert(id);
		});
	}
});