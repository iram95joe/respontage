﻿$(document).ready(function () {
    var invoiceTable;
    LoadInvoiceTable();
    GetInvoiceTemplate();
    GetEmailTemplate();
    function LoadInvoiceTable() {
        invoiceTable = $('#invoice-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Rentals/GetInvoices",
                "type": 'GET',
                "data": function () {
                    return {
                        bookingId: $('#BookingId').val()
                    }
                }
            },
            "columns": [
                { "data": "DateCreated" },
                { "data": "FileUrl" },
                { "data": "PaymentMethodName" },
                { "data": "StatusName" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-invoice-id', data.Id);
            },
            columnDefs: [{
                targets: [0], type: "date", render: function (data) {
                    return moment(data).format('MMMM D,YYYY');
                }
            },
                {
                    targets: [1], type: "text", render: function (data) {
                        return '<a target ="_blank" href="' + data + '"><i class="huge icon file alternate"></i></a>';
                    }
                }
            ],

        });
    }
    CreateInvoice();

    function CreateInvoice() {
        $(document).on('click', '#create-invoice', function () {
            $.ajax({
                type: "GET",
                url: '/Home/GetUnpaidTransaction',
                data: { bookingId: $("#BookingId").val() },
                dataType: "json",
                async: false,
                success: function (data) {
                    $('#payment-method').val('');
                    $('#unpaid-transaction-list').html('');
                    var total =0.0;
                    for (var x = 0; x < data.transactions.length; x++) {
                        total += data.transactions[x].Amount
                        $('#unpaid-transaction-list').append('<div class="fields transaction"><div class="five wide field"><p class="payment-request-description" incomeId="' + data.transactions[x].Id + '" description="' + data.transactions[x].Description + '" duedate="' + moment.utc(data.transactions[x].DueDate).format("MMMM D, YYYY") + '">' + data.transactions[x].Description + " " + moment.utc(data.transactions[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + data.transactions[x].Amount + '</b></p></div>' +
                            '<div class="six wide field">' +
                            '<input type="text" onkeypress="return isNumberKey(this, event);"  min="0" max="' + data.transactions[x].Amount + '" class="payment-request-amount" placeholder="Payment Amount" value="' + data.transactions[x].Amount + '" />' +
                            '</div></div>');
                    }
                    $('#total-payment-request').text("Total:" + total);
                    $('#invoice-template').dropdown('clear');
                    $('#unpaid-transaction-modal').modal({ closable: false }).modal('show').modal('refresh');
                }
            });

        });
    }
    $(document).on('click', '#save-invoice', function () {
        var formData = new FormData();
        var request = [];

        $('#unpaid-transaction-list').children('.transaction').each(function (index, element) {
            var desc = $(this).find('.payment-request-description').attr('description');
            var duedate = $(this).find('.payment-request-description').attr('duedate');
            var amount = $(this).find('.payment-request-amount').val();
            var incomeId = $(this).find('.payment-request-description').attr('incomeId');
            if (amount.length > 0) {
                var jsonPayment = {
                    IncomeId: incomeId,
                    Amount: amount,
                    Description: desc,
                    DueDate: duedate
                };
                request.push(jsonPayment);
            }

        });
        formData.append("PaymentMethod", $('#payment-method').val());
        formData.append("BookingId", $('#BookingId').val());
        formData.append("RequestPaymentJsonString", JSON.stringify(request));
        formData.append("InvoiceTemplateId", $('#invoice-template').val());
        formData.append("EmailTemplateId", $('#email-template').val());
        $.ajax({
            type: 'POST',
            url: "/Payment/CreateInvoice",
            contentType: false,
            cache: false,
            dataType: "json",
            processData: false,
            data: formData,
            success: function (result) {
                invoiceTable.ajax.reload(null, false);
                $('#unpaid-transaction-modal').modal('hide');
            }
        });

    });

    $(document).on('keyup', '.payment-request-amount', function () {
        var total = 0;
        $('.payment-request-amount').each(function (index, element) {
            if (!isNaN(parseFloat($(this).val()))) {
                total += parseFloat($(this).val());
            }
        });
        $('#total-payment-request').text("Total:" + total);
    });
    function GetInvoiceTemplate() {
        $.ajax({
            type: "GET",
            url: "/Payment/GetTemplates",
            success: function (data) {
                var documents = data.documents;
                $('#invoice-template').empty();
                var optionData = '<option value="">Select Template</option>';
                for (var i = 0; i < documents.length; i++) {
                    optionData += '<option value="' + documents[i].Id + '">' + documents[i].Name + '</option>';
                }
                $('#invoice-template').append(optionData);
            }
        });
    }
    function GetEmailTemplate() {
        $.ajax({
            type: "GET",
            url: "/Payment/GetEmailTemplates",
            success: function (data) {
                var template = data.data;
                $('#email-template').empty();
                var optionData = '<option value="">Select Template</option>';
                for (var i = 0; i < template.length; i++) {
                    optionData += '<option value="' + template[i].Id + '">' + template[i].Name + '</option>';
                }
                $('#email-template').append(optionData);
            }
        });
    }
});