﻿$(document).ready(function () {
	function GetInboxAction(value) {
		var action = null;
		if (value.BookingStatus == "Accepted") {
			action = (value.SiteType == 1 ? '<div class="two mini buttons">' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="declineBooking()">Cancel Reservation</button>' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="alterBooking()">Alter Reservation</button>' +
				'</div>' :
				value.SiteType == 2 || value.SiteType == 4 ? '<div class="two mini buttons">' +
					'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="cancelReservation()">Cancel Reservation</button>' +
					'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="alterBooking()">Alter Reservation</button>' +
					'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="sendRefund()">Send Refund</button>' +
					'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="addPaymentRequest()">Add Payment Request</button>' +
					'</div>' : '');
		}
		else if (value.BookingStatus == "Altered") {
			action = '<div class="two mini buttons">' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="cancelReservation()">Cancel Reservation</button>' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="cancelAlteration()">Cancel Alteration</button>' +
				'</div>';
		}
		else if (value.BookingStatus == "CancelAlter") {
			action ='<div class="two mini buttons">' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="cancelReservation()">Cancel Reservation</button>' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="alterBooking()">Alter Reservation</button>' +
				'</div>';
		}
		if (value.BookingStatus == "Preapproved") {
			//$('#booking-actions-withdraw-pre-approve').show();
			action = //'<button class="ui blue button js-btn_withdraw-pre-approve">Withdraw Pre-approve</button>'
				'<div class="three mini buttons">' +
				'<button style="margin: 1%" class="ui blue button js-btn_withdraw-pre-approve">Withdraw Pre-Approve</button>'+
				'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="alterBooking()">Send Special Offer</button>' +
				'<button style="margin: 1%" class="ui blue button js-btn_cancel-reservation" onClick="onDeclineInquiry()">Decline</button>';
			+ '</div>';
		}
		else if (value.BookingStatus == 'WithdrawPreapprove') {
			action =
				'<div class="three mini buttons">' +
				'<button style="margin: 1%" class="ui blue button js-btn_pre-approve" onClick="onPreApproveInquiry()">Pre-Approve</button>'+	
				'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="alterBooking()">Send Special Offer</button>' +
				'<button style="margin: 1%" class="ui blue button js-btn_decline-inquiry">Decline</button>' +
				'</div>';
		}

	
		return action;
	}

    
    var nToastr = toastr;

	nToastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-right",
		"preventDuplicates": false,
		"onclick": null,
		"timeOut": 0,
		"extendedTimeOut": 0,
		"tapToDismiss": false,
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut",
	};

	var nHub = $.connection.notificationHub;
	$.connection.hub.logging = true;

	nHub.client.NotifyUser = function (message, notificationType, outbox) {
        nToastr.clear();
	    var type = "info";
	    switch (notificationType) {
	        case 0:
	            type = "info";
	            break;
	        case 1:
	            type = "success";
	            break;
	        case 2:
	            type = "warning";
	            break;
	        case 3:
	            type = "error";
	            break;
	    }
		nToastr[type](message);

		if (outbox != null) {
			if (outbox.IsSent && outbox.IsCompleted == false) {
				var parent = $('#booking-actions');
				parent.find("*").addClass("blue");
				parent.find("*").attr("disabled", false);
			}
			else {
				var action = GetInboxAction(outbox);
				$('#booking-actions').html(action);
			}
		}
	};

	$.connection.hub.start();
	$.connection.hub.disconnected(function () {

		setTimeout(function () {

			$.connection.hub.start();

		}, 5000);

	});
});