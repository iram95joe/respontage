﻿$(document).ready(function () {

	toastr.options = {
		positionClass: "toast-bottom-right",
		timeOut: 0,
		extendedTimeOut: 0,
		tapToDismiss: false,
		preventOpenDuplicates: true,
		onclick: null,
		closeButton: true
	};

	var iHub = $.connection.importHub;
	$.connection.hub.logging = true;

	iHub.client.DoneImporting = function () {
		toastr.clear();
		enableImportButtons();
	};
	iHub.client.UpdateAnalyticsProgress = function (count, cityId,location) {
		if (!$('#cityId-' + cityId).length) {
			toastr["info"]("<span id='cityId-" + cityId + "'></span>");
		}
		$('#cityId-' + cityId).html("<h4>" + location + "<br/> Property:" + count + "</h4>");
		$('#city-table').find('tr[city-id="' + cityId + '"]').find('td:eq(1)').text(count);
		$('#city-table').find('tr[city-id="' + cityId + '"]').find('td:eq(2)').text(count);

	}
	iHub.client.ShowHostName = function (profile, hostId) {
		disableImportButtons();

		$("#import-modal").modal('hide');

		if (!$('#toaster-' + hostId).length) {
			toastr["info"]("<span id='toaster-" + hostId + "'></span>");
		}

		$('#toaster-' + hostId).html("<h4>" + profile + "</h4>");

	};

	iHub.client.UpdateProgress = function (profile, propertyCount, messageCount, bookingCount, hostId) {
		disableImportButtons();

		if (!$('#toaster-' + hostId).length) {
			toastr["info"]("<span id='toaster-" + hostId + "'></span>");
		}

		$('#toaster-' + hostId).html(
			"<h4>" + profile + "</h4>" +
			"Properties : <span class='property-count'>" + propertyCount + " Record/s Imported<br /><i class='ip'/>" +
			"Message/s : <span class='message-count'>" + messageCount + " Record/s Imported<br /><i class='im'/>" +
			"Booking/s : <span class='booking-count'>" + bookingCount + " Record/s Imported<br /><i class='ib'/>");
	};

	$.connection.hub.start();

	function disableImportButtons() {

		$(".js-btn_redownload").prop("disabled", true);
		$(".js-btn_delete-account").prop("disabled", true);
		$(".js-btn_add-new-account").prop("disabled", true);
		
	}

	function enableImportButtons() {
		$(".js-btn_redownload").prop("disabled", false);
		$(".js-btn_delete-account").prop("disabled", false);
		$(".js-btn_add-new-account").prop("disabled", false);
	}
});