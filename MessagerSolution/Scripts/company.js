﻿$(document).ready(function () {

	$("#background-color").spectrum({
		allowEmpty: true
	});
	$("#trim-color").spectrum({
		allowEmpty: true
	});
	
	$(document).on('click', '#logo-image', function (e) {
		$('#logo-pic').click();
	});
	$(document).on('change', '#logo-pic', function () {
		var imageUrl = window.URL.createObjectURL(this.files[0])
		$('#logo-image').attr("src", imageUrl);
	});
	$(document).on('click', '#background-image', function (e) {
		$('#background-pic').click();
	});
	$(document).on('change', '#background-pic', function () {
		var imageUrl = window.URL.createObjectURL(this.files[0])
		$('#background-image').attr("src", imageUrl);
	});

	$(document).on('click', '#save-company', function () {
		var formData = new FormData();
		formData.append("Image", $('#logo-pic').get(0).files[0]);
		formData.append("BackgroundImage", $('#background-pic').get(0).files[0]);
		formData.append("CompanyName", $('#companyName').val());
		formData.append("Email", $('#companyEmail').val());
		formData.append("Contact", $('#companyContact').val());
		formData.append("About", $('#companyAbout').val());
		formData.append("Facebook", $('#companyFacebook').val());
		formData.append("Twitter", $('#companyTwitter').val());
		formData.append("Address", $('#companyAddress').val());
		formData.append("BackgroundColor", $('#background-color').val());
		formData.append("TrimColor", $('#trim-color').val());
		$.ajax({
			type: "POST",
			url: "/Company/UpdateDetails",
			data: formData,
			contentType: false,
			processData: false,
			success: function (html) {
				toastr["success"]("Company Updated Successfully!");
			}
		});
	})
});