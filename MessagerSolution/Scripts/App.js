﻿var cashFlowRecordDashboard = $("#cashFlowRecordDashboard");
var selectCashFlowType = $("#selectCashFlowType");
var selectProperty = $("#selectProperty");
var totalAmountText = $("#totalAmountText");
var selectCompany = 0;//$("#selectCompany");
var selectPaymentDate = $("#selectPaymentDate");
var crudMessageDisplay = $("#crudMessageDisplay");
var crudMessageStatus = $("#crudMessageStatus");
var searchProperty = $("#searchProperty");
var searchCashFlowType = $("#searchCashFlowType");
var searchAmount = $("#searchAmount");
var searchPaymentDateFrom = $("#searchPaymentDateFrom");
var searchPaymentDateTo = $("#searchPaymentDateTo");

/**
 * CONFIG SECTION
 */
var hideMessageTimeout = 3000;
var toUpdateCashModal = false;
var recordsPerPage = 10;

function GetAllCashFlowRecord(pageIndex) {
    $.ajax({
        url: "/Home/GetAllCashFlowRecord/",
        type: "POST",
        data: {
            propertyId: !isValueNull(searchProperty.val()) ? searchProperty.val() : 0,
            cashFlowType: !isValueNull(searchCashFlowType.val()) ? searchCashFlowType.val(): 0,
            totalAmount: !isValueNull(searchAmount.val()) ? searchAmount.val(): 0,
            paymentDateFrom: searchPaymentDateFrom.val(),
            paymentDateTo: searchPaymentDateTo.val(),
            pageIndex: pageIndex,
            recordsPerPage: recordsPerPage
        },
        success: function(data) {
            if (data.status == 1) {
                cashFlowRecordDashboard.empty(); // Emtpy All Row Elements in UI to Redraw
                var cashFlowList = data.cashFlowList;
                if (cashFlowList.length >= 1) {
                    for (var value = 0; value < cashFlowList.length; value++) {
                        var renderRow;
                        renderRow = "<tr>";
                        renderRow += "<td>" + cashFlowList[value].PropertyName + "</td>";
                        renderRow += "<td>" + cashFlowList[value].CashFlowTypeName + "</td>";
                        renderRow += "<td>$" + cashFlowList[value].Amount + "</td>";
                        renderRow += "<td>" + formatDateTimeDisplay(cashFlowList[value].PaymentDate) + "</td>";
                        renderRow += "<td>" + cashFlowList[value].CompanyName + "</td>";
                        renderRow += "<td><a class=\"ui mini button js-btn_create-new-cash-flow\" title=\"Edit\" onclick=\"LaunchEditPopup(); ResetCashFlowModalValues(); LoadSelectedCashFlowInfo(" + cashFlowList[value].Id + ")\"><i class=\"icon edit\"></i></a><a class=\"ui mini button\" title=\"Delete\" onclick=\"DeleteCashFlow(" + cashFlowList[value].Id + ")\"><i class=\"icon ban\"></i></a></td>";
                        renderRow += "</tr>";
                        cashFlowRecordDashboard.append(renderRow);
                    }
                    RenderPagination(data.totalRecordCount);
                    RemoveActivePageClass();
                    setActivePageClass(pageIndex);
                } else {
                    var noRecordHtml = "<tr><td colspan=\"7\" style=\"text-align:center;\">No Record(s)</td></tr>";
                    cashFlowRecordDashboard.append(noRecordHtml);
                }
                
            } else {
                alert("Fail to Get All Cash Flow Record. Please Try Again");
            }
        },
        error: function (data) {

            alert("Fail to Get All Cash Flow Record. Please Try Again");
        }
    });
}


function CreateNewCashFlow() {
    $.ajax({
        url: "/Home/CreateNewCashFlow/",
        type: "POST",
        data: {
            propertyId: selectProperty.val(),
            cashFlowType: selectCashFlowType.val(),
            totalAmount: totalAmountText.val(),
            paymentDate: selectPaymentDate.val(),
            companyId: 0//selectCompany.val()
        },
        success: function (data) {
            if (data.status == 1) {
                GetAllCashFlowRecord(1);
                successMessageSettings("New Cash Flow is Created Successfully");
            } else {
                errorMessageSettings("Fail to Insert New Cash Flow Record. Please Try Again");
            }
        },
        error: function () {
            errorMessageSettings("Fail to Insert New Cash Flow Record. Please Try Again");
        }
    });
}

function ResetCashFlowModalValues() {
    //selectCashFlowType.dropdown("restore defaults");
    //selectProperty.dropdown("restore defaults");
    //selectCompany.dropdown("restore defaults");
    totalAmountText.val("");
    selectPaymentDate.val("");
}

function LoadSelectedCashFlowInfo(cashFlowId) {
    window.currentCashFlowIDinEditMode = cashFlowId;
    $.ajax({
        url: "/Home/GetCashFlowById/",
        type: "POST",
        data: {
            cashFlowId: cashFlowId
        },
        success: function (data) {
            if (data.status == 1) {
                //selectCashFlowType.val(data.cashFlowRecord.CashFlowType).change();
                //selectProperty.val(data.cashFlowRecord.PropertyId).change();
                //selectCompany.val(data.cashFlowRecord.CompanyId).change();
                totalAmountText.val(data.cashFlowRecord.Amount);
                selectPaymentDate.val(formatDateTimeDisplayText(data.cashFlowRecord.PaymentDate));
                $("#selectCashFlowType option[value=" + data.cashFlowRecord.CashFlowType + "]").prop('selected', "selected");
                $("#selectProperty option[value=" + data.cashFlowRecord.PropertyId + "]").prop('selected', "selected");
            } else {
                errorMessageSettings("Fail to Search Cash Flow Record. Please Try Again");
            }
        },
        error: function () {
            errorMessageSettings("Fail to Search Cash Flow Record. Please Try Again");
        }
    });
}

function DeleteCashFlow(cashFlowId) {
    $.ajax({
        url: "/Home/DeleteCashFlowById/",
        type: "POST",
        data: {
            cashFlowId: cashFlowId
        },
        success: function (data) {
            if (data.status == 1) {
                GetAllCashFlowRecord(1);
                successMessageSettings("Cash Flow is Deleted Successfully");
            } else {
                errorMessageSettings("Fail to Delete Cash Flow Record. Please Try Again");
            }
        },
        error: function () {
            errorMessageSettings("Fail to Delete Cash Flow Record. Please Try Again");
        }
    });
}

function UpdateCashFlow() {
    $.ajax({
        url: "/Home/UpdateCashFlowById/",
        type: "POST",
        data: {
            cashFlowId: window.currentCashFlowIDinEditMode,
            propertyId: selectProperty.val(),
            cashFlowType: selectCashFlowType.val(),
            totalAmount: totalAmountText.val(),
            paymentDate: selectPaymentDate.val(),
            companyId: 0//selectCompany.val()
        },
        success: function (data) {
            if (data.status == 1) {
                GetAllCashFlowRecord(1);
                successMessageSettings("Cash Flow is Updated Successfully");
            } else {
                errorMessageSettings("Fail to Update Cash Flow Record. Please Try Again");
            }
        },
        error: function () {
            errorMessageSettings("Fail to Update Cash Flow Record. Please Try Again");
        }
    });
}

function hideMessage() {
    $("#crudMessageDisplay").addClass("hidden");
}

function successMessageSettings(message) {
    crudMessageDisplay.addClass("success");
    crudMessageDisplay.removeClass("hidden");
    crudMessageStatus.text(message);
    setTimeout(hideMessage, hideMessageTimeout);
}

function errorMessageSettings(message) {
    crudMessageDisplay.addClass("error");
    crudMessageDisplay.removeClass("hidden");
    crudMessageStatus.text(message);
    setTimeout(hideMessage, hideMessageTimeout);
}

function InitDropDownControlList() {
    $.ajax({
        url: "/Home/GetDropDownListInitData/",
        type: "POST",
        success: function (data) {
            RenderPropertyDropDownList(data.propertyList);
            RenderCashFlowTypeDropDownList(data.cashFlowTypeList);
            RenderCompanyDropDownList(data.companyList);
        },
        error: function () {
        }
    });
}

function RenderPropertyDropDownList(propertyList) {
    var html = "";
    for (var a = 0; a < propertyList.length; a++) {
        html += "<option value=" + propertyList[a].PropertyId + ">" + propertyList[a].PropertyName + "</option>";   
    }
    $("#searchProperty").append(html);
    $("#selectProperty").append(html);
}

function RenderCashFlowTypeDropDownList(cashFlowTypeList) {
    var html = "";
    for (var a = 0; a < cashFlowTypeList.length; a++) {
        html += "<option value=" + cashFlowTypeList[a].CashFlowTypeId + ">" + cashFlowTypeList[a].CashFlowTypeName + "</option>";
    }
    $("#searchCashFlowType").append(html);
    $("#selectCashFlowType").append(html);
}

function RenderCompanyDropDownList(companyList) {
}

function RenderPagination(totalPageCount) {
    $(".pagination").empty(); //Clear Pagination Display on Every Search
    var html = "<a class=\"icon item\"><i class=\"left chevron icon\"></i></a>";

    if (totalPageCount != 0) {

        var totalPaging = Math.ceil(totalPageCount / recordsPerPage);

        for (var value = 0; value < totalPaging; value++) {
            var pageValue = value + 1;

            if (pageValue == 1) {
                html += "<a id='pg"+ pageValue +"' class=\"item active\" onclick=\"GetAllCashFlowRecord(" + pageValue + ")\">" + pageValue + "</a>";
            } else {
                html += "<a id='pg" + pageValue + "' class=\"item\" onclick=\"GetAllCashFlowRecord(" + pageValue + ")\">" + pageValue + "</a>";

            }
        }
    }
    html += "<a class=\"icon item\"><i class=\"right chevron icon\"></i></a>";
    $(".pagination").append(html);
}

function RemoveActivePageClass() {
    $(".pagination").find(".active").removeClass('active');
}

function setActivePageClass(pageValue) {
    if (!isValueNull(pageValue)) {
        $("#pg" + pageValue).addClass("active");
    }
}