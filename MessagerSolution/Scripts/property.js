﻿// initialization scripts

$(document).ready(function () {
	var tableProperty;
	var tableTaskTemplate;
	var taskFiles;
	var taskFileFormData;
	var tableTemplates;
	initialize();

	function initialize() {
		setupSemanticUI();
		setEventListeners();
		LoadPropertyTable();
		LoadTaskTemplateTable();
	}

	function setupSemanticUI() {
		$('.ui.accordion').accordion();
		$('.ui.dropdown').dropdown({ on: 'hover' });
		$('.ui.checkbox').checkbox();
		$('.ui.calendar').calendar({ type: 'date' });
		$('.ui.calendar.month').calendar({ type: 'month' });
		$('.ui.calendar.time').calendar({ type: 'time' });
		$('table td a.table-tooltip').popup();
		$('.menu .item').tab();
	}

	function setEventListeners() {
		onCommissionTypeChange();
		onAmountOrPercentChange();
		onPredeductedNonPredeductedChange();
		onAddChildProperty();
		onRemoveChildProperty();
		onParentPropertyToggle();
		onChildPropertyChangeToggle();
		onSyncByPriceRuleToggle();
		onAddActiveDate();
		onDeleteActiveDate();
		onAddProperty();
		onAddPriceRule();
		onDeletePriceRule();
		onAddPropertyAccount();
        onDeletePropertyAccount();
		onEditProperty();
		onPropertyFormSubmit();
		onSyncPriceChanged();
		onDeleteProperty();
		onAddTaskTemplate();
		onAddTask();
		onAddSubTask();
		onTaskTemplateFormSubmit();
		onEditTaskTemplate();
		onImageClick();
		onImageFileChange();
		onSaveTemplate();
		onAddMessageRulesModal();
		onAddTemplate();
		onBackToTemplateList();
		//onAddTaskFileUploadEvent();
		onTemplateFormSubmit();
		onEditTemplate();
		loadTemplateTableData();
		onDeleteTemplate();
		loadMessageRuleTableData();
		onEditMessageRules();
		onDeleteMessageRules();
		//Parent Child
		onParentTypeChange();
		GetStates();
		GetCountries();
	}
	function GetStates() {
		$(document).on('change', '#countries', function () {
			var id = $(this).val();
			$.ajax({
				type: "GET",
				url: "/Property/GetStates",
				data: { id: id },
					success: function(data) {
						var states = data.states;
						$('#states').empty();
						var optionData = '<option value="">Select State</option>';
						for (var i = 0; i < states.length; i++) {
							optionData += '<option value="' + states[i].Id + '">' + states[i].Name + '</option>';
						}
						$('#states').append(optionData);
					}
			});
		});
	}

	function GetCountries() {
		$(document).on('change', '#states', function () {
			var id = $(this).val();
			$.ajax({
				type: "GET",
				url: "/Property/GetCities",
				data: { id: id },
				success: function (data) {
					var cities = data.cities;
					$('#cities').empty();
					var optionData = '<option value="">Select City</option>';
					for (var i = 0; i < cities.length; i++) {
						optionData += '<option value="' + cities[i].Id + '">' + cities[i].Name + '</option>';
					}
					$('#cities').append(optionData);
				}
			});
		});
	}

	function onParentTypeChange() {
		$(document).on('change', '#parent-type', function () {
			GetChildProperties($(this).val());
		});
	}

	function GetChildProperties(type) {
		$.ajax({
			type: "GET",
			url: "/Property/GetAvailableForChildProperty",
			data: { type: type, propertyId: $('#edit-property-id').val()},
			success: function (data) {
				if (data.success) {
					
					$('#child-property-table tbody').empty();
					if (data.childproperties.length == 0  ) {
						$('#child-property-table tbody').append('<tr><td colspan="2" style="text-align: center">No property available</td></tr>');
					}
					$.each(data.childproperties, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input type="checkbox" '+(value.IsChild?"checked":"")+' class="available-property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetSiteType(value.SiteType) + '</td>' +
							'<td>' + value.Host + '<td>' +
							'</tr>';
						$('#child-property-table tbody').append(rowTemplate);
					});
				}
			}
		});
	}
	$("#modal-template-variable").on('change', function () {
		var variable = $(this).children(':selected').text();//.replace('[', '').replace(']', '');
		$('#template-content').append('<i class="icon tags"></i>' + variable);
	});
	$("#teplate-clear-btn").click(function () {
		$("#template-content").text("");
	});

	function onEditMessageRules() {
		$(document).on('click', '.edit-message-rule-btn', function () {
			var id = $(this).parents('tr').attr("data-message-rule-id");

			$.ajax({
				type: 'GET',
				url: "/Property/MessageRuleDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						clearMessageRulesModal();
						var form = $('#message-rule-form');
						form.find('input[name=message-rule-id]').val(data.messageRules.Id);
						form.find('select[name=property]').dropdown('set selected', data.messageRules.PropertyId);
						form.find('select[name=event-type]').dropdown('set selected', data.messageRules.Type);
						form.find('select[name=template]').dropdown('set selected', data.messageRules.TemplateMessageId);
						if (data.messageRules.DateDifference > 0) {
							form.find('input[name=days]').val(data.messageRules.DateDifference);
						}
						$('#message-rule-modal').modal({
							onApprove: messageRuleAddEdit
						})
							.modal('show')
							.modal('refresh');
					}
				}
			});
		});
	}
	function onDeleteMessageRules() {
		$(document).on('click', '.delete-message-rule-btn', function () {
			var id = $(this).parents('tr').attr("data-message-rule-id");
			swal({
				title: "Are you sure you want to delete this message rule ?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			},
				function () {
					$.ajax({
						type: "POST",
						url: "/Property/DeleteMessageRule",
						data: { id: id },
						success: function (data) {
							if (data.success) {
								swal({ title: "Success", text: "Message Rule has been deleted.", type: "success" },
									function () {
										clearMessageRulesModal();
										refreshMessageRulesTableData();
										refreshMessageSMSTableData();
										refreshTemplateTableData();
										getTemplateMessage();
									}
								);
							}
						}
					});
				});
		});
	}
	function loadMessageRuleTableData() {
		tableMessageRules = $('#message-rule-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"bFilter": false,
			"ajax": {
				"url": "/Property/GetMessageRulesList",
				"type": 'GET',
				"data": function () {
					return {
						propertyFilter: $("#message-rules-filter-form select[name=property]").val()
					}
				}
			},
			"columns": [
				{ "data": "PropertyName" },
				{ "data": "Title" },
				{ "data": "Message", "orderable": false },
				{ "data": "Time", },
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-message-rule-id', data.Id);
			}
		});
	}

	function loadTemplateTableData() {
		tableTemplates = $('#templates-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Property/GetTemplateList",
				"type": 'POST'
			},
			"columns": [
				{ "data": "Title" },
				{ "data": "Message", "orderable": false },
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-template-id', data.Id);
			}
		});
	}
	function onDeleteTemplate() {
		$(document).on('click', '.delete-template-btn', function () {
			var id = $(this).parents('tr').attr("data-template-id");
			$.ajax({
				type: "POST",
				url: "/Property/DeleteTemplate",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						refreshTemplateTableData();
						getTemplateMessage();	
					}
				}
			});
		});
	}
	function getTemplateMessage() {
		var options = "";
		$.ajax({
			type: 'POST',
			url: "/Property/GetTemplates",
			dataType: "json",
			async: false,
			success: function (data) {

				if (data.success) {
					$('#message-rule-form').find('select[name=template]').empty();
					$.each(data.templates, function (i, v) {
						$('#message-rule-form').find('select[name=template]').append('<option value="' + v.Id + '">' + v.Title + '</option>');
					});


				}
			}
		})
	}

	function onEditTemplate() {
		$(document).on('click', '.edit-template-btn', function () {
			var id = $(this).parents('tr').attr("data-template-id");
			$('#template-title').val('');
			$('#template-content').val('');
			$('#template-id').val(0);
			$.ajax({
				type: "POST",
				url: "/Property/GetTemplate",
				data: { id: id },
				success: function (data) {
					console.log(data);
					if (data.success) {
						$('#template-id').val(id);
						$('#template-title').val(data.template.Title);
						$('#template-content').text("");
						$('#template-content').append(data.template.Message.replace(/\[/g, '<i class="icon tags"></i>['));
						$('#template-list').hide();
						$('#template-empty-error-message').hide();
						$('#template-success-message').hide();
						$('#template-duplicate-error-message').hide();
						$('#template-server-error-message').hide();
						$("#teplate-clear-btn").show();
						$('.add-edit-template-action-buttons').show();
						$('#add-template-form').show();
					}
				}
			});
		});
	}
	function onTemplateFormSubmit() {
		$(document).on('click', '#teplate-add-edit-btn', function () {
			var id = $('#template-id').val();
			$('#template-empty-error-message').hide();
			$('#template-success-message').hide();
			$('#template-duplicate-error-message').hide();
			$('#template-server-error-message').hide();
			var title = $('#template-title').val();
			var message = $('#template-content').text();

			if (title == '' && message == '') {
				$('#template-empty-error-message').show();
			}
			else {
				if (id == 0) // add
				{
					$.ajax({
						type: 'POST',
						url: "/Property/AddTemplate",
						data: { title: title, message: message },
						dataType: "json",
						success: function (data) {
							if (data.success) {
								$('#template-id').val(0);
								$('#template-title').val('');
								$('#template-content').val('');
								$('#template-success-message').show();
								refreshTemplateTableData();
								getTemplateMessage();
							}
							else if (!data.success && data.is_duplicate) {
								$('#template-duplicate-error-message').show();
							}
							else {
								$('#template-server-error-message').show();
							}
						}
					})
				}
				else // edit
				{
					$.ajax({
						type: 'POST',
						url: "/Property/EditTemplate",
						data: { Id: id, Title: title, Message: message },
						dataType: "json",
						success: function (data) {
							if (data.success) {
								$('#template-id').val(0);
								$('#template-title').val('');
								$('#template-content').val('');
								$('#template-success-message').show();
								refreshTemplateTableData();
								getTemplateMessage();
							}
							else if (!data.success && data.is_duplicate) {
								$('#template-duplicate-error-message').show();
							}
							else {
								$('#template-server-error-message').show();
							}
						}
					})
				}
			}
		});
	}

	function onBackToTemplateList() {
		$(document).on('click', '#back-to-template-list-btn', function () {
			$('#add-template-form').hide();
			$("#teplate-clear-btn").hide();
			$('.add-edit-template-action-buttons').hide();
			$('#template-list').show();
		});
	}
	var messageRuleAddEdit = function () {
		var form = $('#message-rule-form');
		var id = form.find('input[name=message-rule-id]').val();
		//var propertyId = form.find('select[name=property]').val();
		var templateId = form.find('select[name=template]').val();
		var ruleType = form.find('select[name=rule-type]').val();
		var PropertyIds = [];
		$('.property-chk').each(function (index, element) {
			if ($(this).is(':checked')) {
				PropertyIds.push($(this).attr('data-id'));
			}
		});

		var time = form.find('input[name=time]').val();
		var title = form.find('input[name=template-title]').val();
		var message = form.find('textarea[name=template-message]').val();
		if (id == '0') {
			$.ajax({
				type: 'POST',
				url: "/Property/AddMessageRules",
				data: {
					templateMessageId: templateId,
					PropertyIds: PropertyIds,
					title: title,
					time: time,
					message: message,
					ruleType: ruleType
					
				},
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Message Rule has been saved.", type: "success" },
							function () {
								clearMessageRulesModal();
								refreshMessageRulesTableData();
								refreshTemplateTableData();
								getTemplateMessage();
							}
						);
						$('#message-rule-modal').modal('hide');
					}
					else {
						$('#message-rule-modal').modal('hide');
						swal("Error in saving new message rule", "", "error");
					}
				}
			});
		}
		else if (id > 0) {
			$.ajax({
				type: 'POST',
				url: "/Property/EditMessageRule",
				data: {
					id: id,
					title: title,
					message: message,
					PropertyIds: PropertyIds,
					templateMessageId: templateId,
					time: time,
					ruleType: ruleType
				},
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Message Rule has been updated.", type: "success" },
							function () {
								clearMessageRulesModal();
								refreshMessageRulesTableData();
								refreshTemplateTableData();
								getTemplateMessage();
							}
						);
						$('#message-rule-modal').modal('hide');
					}
					else {
						$('#message-rule-modal').modal('hide');
						swal("Error in updating message rule", "", "error");
					}
				}
			});
		}
	}

	function refreshTemplateTableData() {
		tableTemplates.ajax.reload(null, false);
	}

	function refreshMessageRulesTableData() {
		tableMessageRules.ajax.reload(null, false);
	}
	function onAddMessageRulesModal() {
		$(document).on('click', '#add-message-rule-btn', function () {
			clearMessageRulesModal();
			$.ajax({
				type: 'GET',
				url: "/Property/GetProperty",
				success: function (data) {
					$('#property-table tbody').empty();
					if (data.Parentproperties.length == 0 && data.Childroperties.length == 0) {
						$('#property-table tbody').append('<tr><td colspan="2" style="text-align: center">No property available</td></tr>');
					}
					else{
					$.each(data.Parentproperties, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input data-id="' + value.Id + '" type="checkbox" class="property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetPropertySiteType(value.SiteType) + '</td>' +
							'<td>' + "" + '<td>' +
							'</tr>';
						$('#property-table tbody').append(rowTemplate);
					});
					$.each(data.Childroperties, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input data-id="' + value.Id + '" type="checkbox" class="property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetPropertySiteType(value.SiteType) + '</td>' +
							'<td>' + value.Host + '<td>' +
							'</tr>';
						$('#property-table tbody').append(rowTemplate);
					});

				}
				}
			});

			$('#message-rule-modal').modal({
				onApprove: messageRuleAddEdit
			})
				.modal('show')
				.modal('refresh');

			$('#time-triggered').calendar({
				type: 'time'
			});
		});
	}
	function onAddTemplate() {
		$(document).on('click', '#add-template-btn', function () {
			$('#template-list').hide();
			$('#template-empty-error-message').hide();
			$('#template-success-message').hide();
			$('#template-duplicate-error-message').hide();
			$('#template-server-error-message').hide();
			$("#teplate-clear-btn").show();
			$('.add-edit-template-action-buttons').show();
			$('#template-title').val('');
			$('#template-content').text('');
			$('#template-id').val(0);
			$('#add-template-form').show();
		});
	}
	function clearMessageRulesModal() {
		var form = $('#message-rule-form');
		form.find('select[name=property]').dropdown('clear');
		form.find('select[name=event-type]').dropdown('clear');
		form.find('input[name=days]').val('');
		form.find('select[name=template]').dropdown('clear');
		form.find('input[name=message-rule-id]').val('0');
		$('#new-template-wrapper').hide();
		form.find('input[name=template-title]').val('');
		form.find('select[name=template-variable]').dropdown('clear');
		form.find('textarea[name=template-message]').val('');
		form.find('input[name=send-to-sms]').prop('checked', false);
		form.find('input[name=send-to-site]').prop('checked', false);
		form.find('input[name=send-to-email]').prop('checked', false);
		form.find('input[name=is-active]').prop('checked', false);
	}
	function onSaveTemplate() {
		$(document).on('click', '#save-template-btn', function () {
			$('#add-template-form').hide();
			$('#template-list').show();
			$("#teplate-clear-btn").hide();
			$('.add-edit-template-action-buttons').hide();
			$('#manage-template-message-modal').modal('show');

		});
	}
	function onChildPropertyChangeToggle() {
		$(document).on('change', '.available-property-chk', function () {
			var ids = '';
			$('#child-property-table tbody tr').each(function (index) {
				if ($(this).find('.available-property-chk').prop('checked'))
					ids += ($(this).find('td:eq(1)').attr('data-id') + ',');
			});
			$('#child-property-ids').val(ids);
		});
	}

	function onParentPropertyToggle() {
		$(document).on('change', '#IsParentProperty', function () {
			var isParent = $(this).prop('checked');
			$('.child-property-wrapper').empty();
			if (isParent) {
				$('#child-property-table').show();
				$('.parent-type-div').removeAttr('hidden');
			}
			else {
				$('#child-property-table').hide();
				$('.parent-type-div').attr('hidden', true);
			}
		});
	}

	function onRemoveChildProperty() {
		$('.child-property-wrapper').on("click", ".remove-child-property", function () {
			$(this).parent('div').remove();
		});
	}

	function onAddChildProperty() {
		$('.add-child-property').on('click', function () {
			var count = $('.child-property-wrapper').children().length;
			if (count <= 4) {
				var templateHtml = '<div class="inline fields">' +
					'<div class="field">' +
					'<select class="ui search dropdown" required>' +
					'<option value="">-Select Property-</option>' +
					'<option value="1">Cavaricci</option>' +
					'</select>' +
					'</div>' +
					'<a href="#" class="remove-child-property" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
					'</div>';
				$('.child-property-wrapper').append(templateHtml);
				activateDropdown();
			}
		});
	}

	function onSyncByPriceRuleToggle() {
	    $(document).on('change', '#priceRuleSync', function () {
	        var isParent = $(this).prop('checked');

	        if (isParent) {
	            $('#price-rule-table').show();
	            $('#addRule').show();
	        }
	        else {
	            $('#price-rule-table').hide();
	            $('#addRule').hide();
	        }
	    });
	}

	function onCommissionTypeChange() {
		$('#commission-property').on('change', function () {
			var commission_type = $(this).val();
			if (commission_type == -1) {
				$('#commission-amount-field').hide();
			}
			else if (commission_type == 3 || commission_type == 4) {
				$('#commission-amount-field').show();
				$('#commission-amount-lbl').text('Percent');
			}
			else {
				$('#commission-amount-field').show();
				$('#commission-amount-lbl').text('Amount');
			}
		});
	}

	function LoadPropertyTable() {
		tableProperty = $('#properties-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": "/Property/GetPropertyList",
			"columns": [
				{ "data": "Name" },
				{ "data": "Host" },
				{ "data": "Site" },
				{ "data": "Type" },
				{ "data": "ActiveBooking" },
				{ "data": "SyncParent" },
				{ "data": "MultiUnitParent" },
				{ "data": "AccountParent" },
				{ "data": "Actions", "orderable": false },
				{ "data": "IsActive" }

			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-property-id', data.Id);
			}
		});
	}
	
	// Task Template 
	function LoadTaskTemplateTable() {
		tableTaskTemplate = $('#tasktemplate-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": "/Property/GetTaskTemplateList",
			"columns": [
				{ "data": "Id" },
				{ "data": "TemplateName" },
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-task-template-id', data.Id);
			}
		});
	}

	var myHub = $.connection.propertyHub;
	$.connection.hub.logging = true;

	myHub.client.RefreshPropertyList = function () {
		$("#property-dimmer").removeClass("inactive").addClass("active");

		tableProperty.ajax.reload(null, false);//RefreshPropertyList();
		$("#property-dimmer").removeClass("active").addClass("inactive");
	};

	$.connection.hub.start();

	// ------------------
	// ------------------
	//   Event Listeners
	// ------------------
	// ------------------

	function GetSiteType(site) {
		if (site == 1)
			return "Airbnb";
		else if (site == 2)
			return "Vrbo";
		else if (site == 3)
			return "Booking";
		else if (site == 4)
			return "Homeaway";
		else if (site == 0)
			return "Local";
	}
	function GetPropertySiteType(site) {
		if (site == 0)
			return "Parent Property";
		else if(site == 1)
			return "Airbnb";
		else if (site == 2)
			return "Vrbo";
		else if (site == 3)
			return "Booking";
		else if (site == 4)
			return "Homeaway";
	}
	function onAddProperty() {
		$('.create-new-property-btn').on('click', function () {
			$('#edit-property-id').val(0);
			$('#child-property-ids').val('');
			$('#parent-type').val('1').change();
			$('#PropertyName').val("");
			$('#PropertyType').dropdown('clear');
			$('#UnitCount').val("");
			$('#Description').val("");
			$('#EndDate').val("");
			$('#StartDate').val("");
			$('#ical-url').val("");
			$('#street').val('');
			$('#building').val('');
			$('#street-number').val("");
			$('#unit-number').val("");
			$('#countries').val('').change();
			$('#states').val('').change();
			$('#cities').val('');
			

			//$("#arloDiv").empty();
			//$("#arloDiv").append('<div class="fields" style="margin-top: 10px;">' +
			//	'<div class="one wide field">' +
			//	'</div>' +
			//	'<div class="four wide field">' +
			//	'<input type="email" name="usernameArlo" placeholder="User Name">' +
			//	'</div>' +
			//	'<div class="four wide field">' +
			//	'<input type="password" name="passwordArlo" placeholder="Password">' +
			//	'</div>' +
			//	'<div class="four wide field">' +
			//	'<input type="password" placeholder="Re-Enter Password">' +
			//	'</div>' +
			//	'<div class="field">' +
			//	'<button type="button" value="arlo" class="circular ui icon blue mini button add-property-account" data-tooltip="Add">' +
			//	'<i class="plus icon"></i>' +
			//	'</button>' +
			//	'</div>' +
			//	'</div>');
			//$("#blinkDiv").empty();
			//$("#blinkDiv").append('<div class="fields" style="margin-top: 10px;">' +
			//	'<div class="one wide field">' +
			//	'</div>' +
			//	'<div class="four wide field">' +
			//	'<input type="email" name="usernameBlink" placeholder="User Name">' +
			//	'</div>' +
			//	'<div class="four wide field">' +
			//	'<input type="password" name="passwordBlink" placeholder="Password">' +
			//	'</div>' +
			//	'<div class="four wide field">' +
			//	'<input type="password" placeholder="Re-Enter Password">' +
			//	'</div>' +
			//	'<div class="field">' +
			//	'<button type="button" value="blink" class="circular ui icon blue mini button add-property-account" data-tooltip="Add">' +
			//	'<i class="plus icon"></i>' +
			//	'</button>' +
			//	'</div>' +
			//	'</div>');

			$('#MonthlyExpense').val("");
			$('#MonthlyExpenseNotes').val("");
			$('#commission-property').dropdown("set selected", -1);
			$('#task-template-property').dropdown('clear');
			$('#commission-amount').val("0");
			$('.child-property-wrapper').empty();
			$('#child-property-table').hide();
			$('#MyContent').removeClass('active');
			$('#IsParentProperty').prop('checked', false).change();
			$('.fee-fields-wrapper-property').empty();
			$('.create-new-property-modal').modal('show');
			$('.ui.calendar').calendar({
				type: 'date'
			});
			BindTaskTemplateOptions();
		});
	}
	$(document).on('keyup', '[required]', function () {

		if ($(this).val().length == 0) {
			$(this).next().remove();
			$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).next().remove();
		}
	});
	
	function ValidateRequiredFields() {
		var hasBlankField = false;
		$('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.next().remove();
				obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				obj.focus();
				hasBlankField = true;
			} else {
				obj.next().remove();
			}

		});
		return hasBlankField;
	}

	function onPropertyFormSubmit() {
		$('#property-form-submit-btn').on('click', function () {

			var hasBlankfield = ValidateRequiredFields();
			if (hasBlankfield) {
				return;
			}
			if ($('#fmbook').form('is valid')) {
				var id = $('#edit-property-id').val();
				if (id == 0) {
					$.ajax({
						type: "POST",
						url: "/Property/Add",
						data: $("#fmbook").serialize(),
						success: function (data) {
							$('.create-new-property-modal').modal('hide');
							swal("New property has been added .", "", "success");
							tableProperty.ajax.reload(null, false);
						}
					});
				}
				else {
				    var editForm = $("#fmbook").serialize();
				    var syncPrice = editForm.includes("syncPrice=on") ? true : false;
				    var syncAvailability = editForm.includes("syncAvailability=on") ? true : false;

				    editForm = editForm
                        .replace("&syncPrice=on", "")
                        .replace("&syncAvailability=on", "")
                        + (syncPrice ? "&syncPrice=true" : "&syncPrice=false")
                        + (syncAvailability ? "&syncAvailability=true" : "&syncAvailability=false");

					$.ajax({
						type: "POST",
						url: "/Property/Edit",
						data: editForm,
						success: function (data) {
							$('.create-new-property-modal').modal('hide');
							swal("Changes has been saved .", "", "success");
							tableProperty.ajax.reload(null, false);
						}
					});
				}
			}
		});
	}

	function priceRuleHtml() {
	    var html = "" +

'<tr class="price-rule-row">' +
    '<td>' +
    '</td>' +
    '<td>' +
        '<select name="priceRuleBasis" class="ui fluid dropdown price-rule-dropdown">' +
            '<option value="0">Occupancy Rate by Date</option>' +
            '<option value="1">Occupancy Rate by Date for \'x\' Rooms</option>' +
            '<option value="2">Occupancy Rate by Month</option>' +
            '<option value="3">Occupancy Rate by Month for \'x\' Rooms</option>' +
            '<option value="4">Availability Rate by Date</option>' +
            '<option value="5">Availability Rate by Date for \'x\' Rooms</option>' +
            '<option value="6">Availability Rate by Month</option>' +
            '<option value="7">Availability Rate by Month for \'x\' Rooms</option>' +
        '</select>' +
    '</td>' +
    '<td>' +
        '<select name="priceRuleCondition" class="ui fluid dropdown price-rule-dropdown">' +
            '<option value="0">greater than</option>' +
            '<option value="1">less than</option>' +
        '</select>' +
    '</td>' +
    '<td>' +
        '<input type="text" name="priceRuleConditionPercentage" placeholder="%" size="10" />' +
    '</td>' +
    '<td>' +
        '<select name="priceRuleAmountPlusMinus" class="ui fluid dropdown price-rule-dropdown">' +
            '<option value="0">+</option>' +
            '<option value="1">-</option>' +
        '</select>' +
    '</td>' +
    '<td>' +
        '<input type="text" name="priceRuleAmount" placeholder="Price in $ or %" size="10" />' +
    '</td>' +
    '<td>' +
        '<select name="priceRuleSign" class="ui fluid dropdown price-rule-dropdown">' +
            '<option value="0">%</option>' +
            '<option value="1">$</option>' +
            '</select>' +
    '</td>' +
    '<td>' +
        '<select name="priceRuleAveragePriceBasis" class="ui fluid dropdown price-rule-dropdown">' +
            '<option value="0">by date</option>' +
            '<option value="1">by date for \'x\' rooms</option>' +
            '<option value="2">by month</option>' +
            '<option value="3">by month for \'x\' rooms</option>' +
        '</select>' +
    '</td>' +
'</tr>' +

            "";

	    return html;
	}

	$(document).on('change', '.price-rule-dropdown', function () {
	    $('span').html($(this).val())
	});

	function onAddPriceRule() {
	    $(document).on('click', '#addRule', function (e) {
	        var clone = $(".price-rule-row").first().clone();
	        clone.find("td").first().html('<button type="button" name="deleteRule" class="tiny ui orange circular icon button price-rule-delete"><i class="minus icon"></i></button>');

	        $("#priceRuleTable").append(clone);
	        $(".price-rule-dropdown").dropdown();
	    });
	}

	function onDeletePriceRule() {
	    $(document).on('click', '.price-rule-delete', function (e) {
	        $(this).parent().parent().remove();
	    });
	}

	function onAddPropertyAccount() {
	    $(document).on('click', '.add-property-account', function (e) {
	        var value = $(this).val();

			var html='<div class="fields" style="margin-top: 10px;">'+
				'<div class="one wide field">'+
				'</div>'+
				'<div class="four wide field">'+
					'<input type="email" name="username'+(value.substr(0,1).toUpperCase()+value.substr(1))+'" placeholder="User Name">'+
				'</div>'+
				'<div class="four wide field">'+
					'<input type="password" name="password'+(value.substr(0,1).toUpperCase()+value.substr(1))+'" placeholder="Password">'+
				'</div>'+
				'<div class="four wide field">'+
					'<input type="password" placeholder="Re-Enter Password">'+
				'</div>'+
				'<div class="field">'+
					'<button type="button" value="'+value+'" class="circular ui icon blue mini button add-property-account" data-tooltip="Add">'+
						'<i class="plus icon"></i>'+
				'</button>' +
				'<button type="button" value="'+value+'" class="circular ui icon orange mini button delete-property-account" data-tooltip="Remove">'+
						'<i class="minus icon"></i>'+
					'</button>'+
				'</div>'+
			'</div>';
			$("#" + value + "Div").append(html);
	    });
	}

	function onDeletePropertyAccount() {
	    $(document).on('click', '.delete-property-account', function (e) {
	        $(this).parent().parent().remove();
	    });
	}
    
	function onEditProperty() {
		$(document).on('click', '.edit-property-btn', function (e) {
			e.preventDefault();
			var id = $(this).parents('tr').attr('data-property-id');
			var ids = '';
			$('#edit-property-id').val(id);
			var dynamicdrp = $('#hdndrpfeetype').val();
			$('.ui.calendar').calendar({
				type: 'date'
			});
			$('#child-property-ids').val('');
			$('#child-property-table').hide();
			$('#MyContent').removeClass('active');
			BindTaskTemplateOptions();
			$.ajax({
				type: "POST",
				url: '/Property/GetDetails',
				data: { Id: id },
				success: function (data) {
					//$('input[name=HostId]').parent().parent().hide();
					$('#PropertyName').val(data[0].PropertyName);
					$("#PropertyType option[value=" + data[0].PropertyType + "]").prop('selected', "selected");
					$("#PropertyType").val(data[0].PropertyType).change();
					$('#UnitCount').val(data[0].UnitCount);
					$('#Description').val(data[0].Description);
					$('#autoCreate').prop("checked", data[0].AutoCreate);
					$('#start-date').val(moment(data[0].StartDate).format("MMMM D, YYYY"));
					$('#end-date').val(moment(data[0].EndDate).format("MMMM D, YYYY"));
					$('#status').prop("checked", data[0].IsActive);
					$('#longitude').val(data[0].Longitude);
					$('#ical-url').val(data[0].IcalUrl);
					$('#latitude').val(data[0].Latitude);
					$('#selfCheckinCheckout').prop("checked", data[0].AllowedSelfCheckinCheckout);
					$('#IsParentProperty').prop('checked', data[0].IsParentProperty).change();
					$('#street').val(data[0].Street);
					$('#building').val(data[0].Building);
					$('#unit-number').val(data[0].UnitNumber);
					$('#street-number').val(data[0].StreetNumber);

					$('#countries').val(data[0].CountryId).change();
					$('#states').val(data[0].StateId).change();
					$('#cities').val(data[0].CityId).change();
					$('#parent-type').val(data[0].ParentType);
					if (data[0].PriceRuleSync) {
					    $('#priceRuleSync').attr('checked', 'checked');
					    $('#price-rule-table').show();
					    $('#addRule').show();
					}
					else {
					    $('#priceRuleSync').removeAttr('checked');
					    $('#price-rule-table').hide();
					    $('#addRule').hide();
					}

					$("#priceRuleTable").html(priceRuleHtml());
					$(".price-rule-dropdown").dropdown();

					if (data[0].PriceRuleSettings.length > 0) {
					    for (var i = 0; i < data[0].PriceRuleSettings.length; i++) {
					        var clone = $(".price-rule-row").first().clone();
					        clone.find("td").first().html('<button type="button" name="deleteRule" class="tiny ui orange circular icon button price-rule-delete"><i class="minus icon"></i></button>');
					        clone.find("td:eq(1) select").val(data[0].PriceRuleSettings[i].PriceRuleBasis);
					        clone.find("td:eq(2) select").val(data[0].PriceRuleSettings[i].PriceRuleCondition);
					        clone.find("td:eq(3) input").val(data[0].PriceRuleSettings[i].PriceRuleConditionPercentage);
					        clone.find("td:eq(4) select").val(data[0].PriceRuleSettings[i].PriceRuleAmount < 0 ? 1 : 0);
					        clone.find("td:eq(5) input").val(data[0].PriceRuleSettings[i].PriceRuleAmount < 0 ? (data[0].PriceRuleSettings[i].PriceRuleAmount * -1) : data[0].PriceRuleSettings[i].PriceRuleAmount);
					        clone.find("td:eq(6) select").val(data[0].PriceRuleSettings[i].PriceRuleSign);
					        clone.find("td:eq(7) select").val(data[0].PriceRuleSettings[i].PriceRuleAveragePriceBasis);
					        $("#priceRuleTable").append(clone);
					    }

					    $(".price-rule-row").first().remove();
					    $(".price-rule-dropdown").dropdown();
					}
					if (data[0].IsParentProperty) {
					    $("#sync-div").show();
						//$("#accounts-div").show();
						$('#IsParentProperty').attr('checked', 'checked').change();
						$('#child-property-table').show();
						$('#parent-type-div').removeAttr('hidden');
					    var childProperties = "";

					    $.each(data[0].ChildProperties, function (index, value) {
					        childProperties = childProperties +
                                '<div style="margin-top: 3%; margin-left: 3%;" class="field">' +
					                '<div class="ui toggle checkbox">' +
					                    '<input type="radio" name="syncParentPropertyId" value="' + value.Id + '" ' + (value.IsSyncParent == true ? "checked" : "") + '>' +
					                    '<label>' + value.Name + ' (' + value.SiteType + ')</label>' +
					                '</div>' +
					            '</div>';
					    });

					    var syncHtml = '<label>Select Price and Availability Sync Source Property :</label>' + childProperties;

					    $("#sync-accordion").html(syncHtml);
					}
					
					else {
						$("#sync-div").hide();
						$("#accounts-div").show();
					    $('#IsParentProperty').removeAttr('checked');
					}

					$('#child-property-table tbody').empty();

					$.each(data[0].ChildProperties, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input type="checkbox" checked class="available-property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetSiteType(value.SiteType) + '</td>' +
							'<td>' + value.Host + '<td>' +
							'</tr>';
						$('#child-property-table tbody').append(rowTemplate);
						ids += (value.Id + ',');
					});
					$('#child-property-ids').val(ids);
					//JM 10/28/19 Local child property
					$.each(data[0].ChildLocal, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input type="checkbox" checked class="available-property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetSiteType(value.SiteType) + '</td>' +
							'<td>' + value.Host + '<td>' +
							'</tr>';
						$('#child-property-table tbody').append(rowTemplate);
						ids += (value.Id + ',');
					});
					$('#child-property-ids').val(ids);
					//Available airbnb,vrbo,homeaway property
					$.each(data[0].AvailableChildProperties, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input type="checkbox" class="available-property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetSiteType(value.SiteType) + '</td>' +
							'<td>'+value.Host+'<td>' +
							'</tr>';
						$('#child-property-table tbody').append(rowTemplate);
					});
					//Available Local
					$.each(data[0].AvailableLocalChild, function (index, value) {
						var rowTemplate = '<tr>' +
							'<td>' +
							'<div class="ui toggle checkbox">' +
							'<input type="checkbox" class="available-property-chk"> <label></label>' +
							'</div>' +
							'</td>' +
							'<td data-id="' + value.Id + '">' + value.Name + '</td>' +
							'<td> ' + GetSiteType(value.SiteType) + '</td>' +
							'<td>' + value.Host + '<td>' +
							'</tr>';
						$('#child-property-table tbody').append(rowTemplate);
					});

					if (data[0].PropertySyncSettings != null) {
					    var settings = data[0].PropertySyncSettings;

					    if (settings.SyncPrice) {
					        $("#price-adjustment").show();
					    }
					    else {
					        $("#price-adjustment").hide();
					    }

					    $("input[name='syncPrice']").prop("checked", settings.SyncPrice ? true : false);
					    $("input[name='syncAvailability']").prop("checked", settings.SyncAvailability ? true : false);

					    $("input[name='airbnbAdjustmentValue']").val(settings.AirbnbAdjustmentValue);
					    $("select[name='airbnbSymbolType']").val(settings.AirbnbSymbolType);
					    $("select[name='airbnbAdjustmentType']").val(settings.AirbnbAdjustmentType);

					    $("input[name='vrboAdjustmentValue']").val(settings.VrboAdjustmentValue);
					    $("select[name='vrboSymbolType']").val(settings.VrboSymbolType);
					    $("select[name='vrboAdjustmentType']").val(settings.VrboAdjustmentType);

					    $("input[name='bookingComAdjustmentValue']").val(settings.BookingComAdjustmentValue);
					    $("select[name='bookingComSymbolType']").val(settings.BookingComSymbolType);
					    $("select[name='bookingComAdjustmentType']").val(settings.BookingComAdjustmentType);
					}

					try {
						$('#EndDate').val(formatDateTimeDisplayWithMonth(data[0].ActiveDateEnd));
						$('#StartDate').val(formatDateTimeDisplayWithMonth(data[0].ActiveDateStart));
					}
					catch (Exception) { $('#EndDate').val(''); $('#StartDate').val(''); }
					var FullAddress = data[0].Address;
					try {
						var arrayData = FullAddress.split(' ');
						if (arrayData.length == 1) {
							$('#Address1').val(arrayData[0]);
						}
						if (arrayData.length == 2) {
							$('#Address1').val(arrayData[0]);
							$('#Address2').val(arrayData[1]);
						}
						if (arrayData.length == 3) {
							$('#Address1').val(arrayData[0]);
							$('#Address2').val(arrayData[1]);
							$('#Address3').val(arrayData[2]);
						}
					}
					catch (Exception) {
						$('#Address1').val('');
						$('#Address2').val('');
						$('#Address3').val('');
					}
					$('#MonthlyExpense').val(data[0].MonthlyExpence);
					$('#MonthlyExpenseNotes').val(data[0].MonthlyExpenceNote);
					$('#commission-property').dropdown('set selected', data[0].commission);

					$.each(data[0].Templates, function (index, value) {
						$('#task-template-property').dropdown('set selected', value.TemplateId);
					});
					//$('#task-template-property').dropdown('set selected', data[0].TemplateId);

					$('#commission-amount').val(data[0].CommissionAmount);
					var FeeAndTaxlist = data[0].FeeAndTaxlist;
					$('.fee-fields-wrapper-property').empty();
					/////
					for (var i = 0; i < FeeAndTaxlist.length; i++) {
						$('.fee-fields-wrapper-property').append('<div class="fields">' +
							'<div class="field margin-top-5">' +
							'<div class="ui toggle checkbox">' +
							'<input type="hidden" name="checkboxName2" value="' + data[0].FeeAndTaxlist[i].Show2 + '">' +
							'<input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" class="show2test prededucted-chk" name="shows2">' +
							'<label>Pre Deducted ?</label>' +
							'</div>' +
							'</div>' +
							'<div class="field">' +
							'<select class="ui fluid search dropdown" required name="vatdrp">' + dynamicdrp +
							'</select>' +
							'</div>' +
							'<div class="field">' +
							'<select class="ui fluid search dropdown" required name="inclusiveexcluve" data-tooltip="Amount Deducted from ?">' +
							'<option value="1">Inclusive</option>' +
							'<option value="2">Exclusive</option>' +
							'</select>' +
							'</div>' +
							'<div class="field margin-top-5">' +
							'<div class="ui toggle checkbox">' +
							'<input type="hidden" name="checkboxName" value="' + data[0].FeeAndTaxlist[i].Show + '">' +
							'<input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" name="shows">' +
							'<label>Display by default ?</label>' +
							'</div>' +
							'</div>' +
							'<div class="field">' +
							'<select class="ui fluid search dropdown amount-percents" required name="AmountPercents">' +
							'<option value="1">Amount</option>' +
							'<option value="2">Percent</option>' +
							'</select>' +
							'</div>' +
							'<div class="field">' +
							'<input type="text" required name="vatamount" value="' + data[0].FeeAndTaxlist[i].AmountPercent + '" placeholder="Amount">' +
							'<input type="hidden" name="FeeAndTaxId" value="' + data[0].FeeAndTaxlist[i].Id + '">' +
							'</div>' +
							'<div class="field booking-payout-field"">' +
							'<select class="ui fluid search dropdown feetype" required name="feetype" data-tooltip="Amount Deducted from ?">' +
							'<option value="1">Booking Price</option>' +
							'<option value="2">Payout Amount</option>' +
							'</select>' +
							'</div>' +
							'<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
							'</div>');
					}
					/////
					var counteri = 0;
					$('select[name="vatdrp"]').each(function () {
						$(this).val(FeeAndTaxlist[counteri].Fee_Type).change();

						counteri++;
					});
					var counteri2 = 0;
					$('select[name="inclusiveexcluve"]').each(function () {
						$(this).val(FeeAndTaxlist[counteri2].InclusiveOrExclesive).change();
						counteri2++;
					});

					var counteri3 = 0;
					$('select[name="feetype"]').each(function () {
						$(this).val(FeeAndTaxlist[counteri3].ComputeBasis).change();
						counteri3++;
					});
					var counteri4 = 0;
					$('select[name="AmountPercents"]').each(function (v, e) {
						$(this).val(FeeAndTaxlist[counteri4].Method).change();
						if ($(this).val() == 1) {
							console.log($(this).parent().parent());
							$(this).parent().parent().find('.booking-payout-field').hide();
							$(this).parent().parent().find('input[name=vatamount]').attr('placeholder', 'Amount');
						}
						else {
							$(this).parent().parent().find('.booking-payout-field').show();
							$(this).parent().parent().find('input[name=vatamount]').attr('placeholder', 'Percent');
						}
						counteri4++;
					});
					var counteri5 = 0;
					$('input[name="shows"]').each(function () {
						$(this).prop('checked', FeeAndTaxlist[counteri5].showbool);
						counteri5++;
					});
					var counteri6 = 0;
					$('input[name="shows2"]').each(function () {
						$(this).prop('checked', FeeAndTaxlist[counteri6].showbool2);
						counteri6++;
					});

					$('.ui.checkbox').checkbox();
					$('.ui.dropdown').dropdown({ on: 'hover' });
					$('.create-new-property-modal').modal('show');

					//initialize calendar on modal show
					$('.ui.calendar').calendar({
						type: 'date'
					});
				}
			});
		});
	}

	//$("#syncPrice").change(function () {
	//    debugger;
	//    if ($(this).checked == true) {
	//        $("#price-adjustment").show();
	//    }
	//    else {
	//        $("#price-adjustment").hide();
	//    }
	//});

	function onSyncPriceChanged() {
	    $("input[name='syncPrice']").change(function () {
	        $("#price-adjustment").transition('fade up');
	    });
	}

	function onDeleteProperty() {
		$(document).on('click', '.delete-property-btn', function () {
			var id = $(this).parents('tr').attr('data-property-id');
			if (id != '') {
				swal({
					title: "Are you sure you want to delete this property ?",
					text: "",
					type: "warning",
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					showCancelButton: true,
					closeOnConfirm: false,
					showLoaderOnConfirm: true,
				},
					function () {
						$.ajax({
							type: 'POST',
							url: "/Property/Delete",
							data: { id: id },
							success: function (data) {
								if (data.success) {
									swal({ title: "Success", text: "Property successfully deleted.", type: "success" },
										function () {
											tableProperty.ajax.reload(null, false);
										});
								}
								else {
									swal("Error occured on deleting property", "", "error");
								}
							}
						});
					});
			}
		})
	}


	$(document).on('click', '.add_fee_button_property', function (e) {
		e.preventDefault();
		var count = $('.fee-fields-wrapper-property').children().length;
		var dynamicdrp = $('#hdndrpfeetype').val();
		if (count <= 5) {
			$('.fee-fields-wrapper-property').append('<div class="fields">' +
				'<div class="field margin-top-5">' +
				'<div class="ui toggle checkbox">' +
				'<input type="hidden" name="checkboxName2" value="0">' +
				'<input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" class="prededucted-chk" name="shows2">' +
				'<label>Pre Deducted ?</label>' +
				'</div>' +
				'</div>' +
				'<div class="field">' +
				'<select class="ui fluid search dropdown" required name="vatdrp">' + dynamicdrp +
				'</select>' +
				'</div>' +
				'<div class="field">' +
				'<select class="ui fluid search dropdown" required name="inclusiveexcluve" data-tooltip="Amount Deducted from ?">' +
				'<option value="1">Inclusive</option>' +
				'<option value="2">Exclusive</option>' +
				'</select>' +
				'</div>' +
				'<div class="field margin-top-5">' +
				'<div class="ui toggle checkbox">' +
				'<input type="hidden" name="checkboxName" value="0">' +
				'<input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" name="shows">' +
				'<label>Display by default ?</label>' +
				'</div>' +
				'</div>' +
				'<div class="field">' +
				'<select class="ui fluid search dropdown amount-percents" required name="AmountPercents">' +
				'<option value="1">Amount</option>' +
				'<option value="2">Percent</option>' +
				'</select>' +
				'</div>' +
				'<div class="field">' +
				'<input type="text" required name="vatamount" placeholder="Amount">' +
				'</div>' +
				'<div style="display: none" class="field booking-payout-field">' +
				'<select id="booking-payout-field" class="ui fluid search dropdown" required name="feetype" data-tooltip="Amount Deducted from ?">' +
				'<option value="1">Booking Price</option>' +
				'<option value="2">Payout Amount</option>' +
				'</select>' +
				'</div>' +
				'<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
				'<input type="hidden" name="Inclusive_exclesiveId" value="0">' +
				'<input type="hidden" name="FeeAndTaxId" value="0">' +
				'</div>');
			activateDropdown();
			$('select[name="vatdrp"]').change(function (e) {
				var indx = $('select[name="vatdrp"]').index(this);
				var inputUserRoles = $(this).val();
			});
		}

	});

	function onPredeductedNonPredeductedChange() {
		$(document).on('change', '.prededucted-chk', function (e) {
			var drp = $(this).parents('.fields').find('.booking-payout-field').find('select');

			var option1 = '<option value="1">Booking Price</option>';
			var option2 = '<option value="1">Booking Price</option>' +
				'<option value="2">Payout Amount</option>';
			drp.empty();
			if ($(this).prop('checked')) {
				drp.append(option1);
				drp.dropdown('set selected', 1);
			}
			else {
				drp.append(option2);
				drp.dropdown();
			}
			console.log();
		});
	}

	function onAmountOrPercentChange() {
		$(document).on('change', '.amount-percents', function (e) {
			if ($(this).find('select').val() == 1) {
				$(this).parent().parent().find('.booking-payout-field').hide();
				$(this).parent().parent().find('input[name=vatamount]').attr('placeholder', 'Amount');
			}
			else {
				$(this).parent().parent().find('.booking-payout-field').show();
				$(this).parent().parent().find('input[name=vatamount]').attr('placeholder', 'Percent');
			}
		});
	}



	$('.fee-fields-wrapper-property').on("click", ".remove_field", function (e) {
		e.preventDefault(); $(this).parent('div').remove();
	});

	$('.task-fields-wrapper').on("click", ".remove_fields", function (e) {
		e.preventDefault(); $(this).parent('div').parent('div').remove();
	});
	$('.task-fields-wrapper').on("click", ".remove_field", function (e) {
		e.preventDefault(); $(this).parent('div').remove();
	});
	function onAddActiveDate() {
		$(document).on('click', '.add-active-date-button', function (e) {
			e.preventDefault();
			var count = $('.active-date-wrapper').children().length;
			if (count < 5) {
				$('.active-date-wrapper').append('<div class="fields">' +
					'<div class="field">' +
					'<div class="ui calendar month">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" placeholder="Start Date"/>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'<span>To</span>' +
					'<div class="field">' +
					'<div class="ui calendar month">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" placeholder="End Date"/>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'<a href="#" class="delete-active-date-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
					'<a href="#" class="add-active-date-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>' +
					'</div>');
				activateDropdown();
				var parent = $(this).parent();
				parent.find('.add-active-date-button').remove();
				parent.find('.delete-active-date-button').remove();
				parent.append('<a href="#" class="delete-active-date-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>');
			}
		});
	}

	function onDeleteActiveDate() {
		$('.active-date-wrapper').on("click", ".delete-active-date-button", function (e) {
			e.preventDefault();
			var parent = $(this).parent();
			if ($(this).parent().find('a').length) {
				$(this).parent('div').remove();
				parent = $('.active-date-wrapper').children().last();
				parent.find('.delete-active-date-button').remove();
				parent.find('.add-active-date-button').remove();
				parent.append('<a href="#" class="add-active-date-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>');
			}
			$(this).parent('div').remove();
			var count = $('.active-date-wrapper').children().length;
			if (count == 1) {
				parent = $('.active-date-wrapper :first-child');
				parent.find('.delete-active-date-button').remove();
			}
		});
	}

	$(document).on('change', '#commission-property', function (e) {
		var commission = $('#commission-property').val();
		if (commission == 3 || commission == 4) {
			$('#commission-amount').attr('placeholder', 'Percent');
		}
		else {
			$('#commission-amount').attr('placeholder', 'Amount');
		}
	});

	$('.ui.checkbox').checkbox();

	function activateDropdown() {
		$('.ui.dropdown').dropdown({ on: 'hover' });
	}

	function onAddTaskTemplate() {
		$('.create-new-task-template-btn').on('click', function () {
			ClearTaskTemplateModalForm();
			taskFiles = [];
			$('#edit-task-template-id').val(0);
			$(".create-new-task-template-modal").modal("show");
		});
	}

	function onAddSubTask() {
		$(document).on('click', '.add_subtask_button', function (e) {
			var subTask = '<div class="fields sub-task-list-field">' +
				'<input type="hidden" id="subTaskId" name="subTaskId" value="0" />' +
				'<div class="four wide field">' +
				'<i class="icon arrow right" style="float:right"></i>'+
				'</div>' +
				'<div class="four wide field">' +
				'<input type="text" placeholder="Sub-Task Title" name="SubTaskTitle">' +
				'</div>' +
				'<div class="six wide field">' +
				'<textarea rows="2" placeholder="Task Description"  name="SubTaskDescription"></textarea>' +
				'</div>' +
				'<div class="three wide field">' +
				'<input name="SubTaskImage" class="image-file" type="File" hidden/>' +
				'<img src="/Images/SubTask/default-image.png" class="ui small image" alt="Image" />' +
				'</div>' +
				'<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
				'<a href="#" class="add_subtask_button" data-tooltip="Add Sub Task"><i class="circular plus icon"></i></a>' +
				'</div>'
			$(this).parent().parent().append(subTask);
		});
	}
	function onAddTask() {
		$(document).on('click', '.add_task_button', function (e) {
			e.preventDefault();
			var count = $('.task-fields-wrapper').children().length;
			if (count <= 100) {
				$('.task-fields-wrapper').append('<div class="ui form tasks ui clearing segment" ><div class="fields task-list-field">' +
					'<input type="hidden"  name="taskId" value="0" />' +
					'<div class="four wide field">' +
					'<input type="text" placeholder="Task Title" id="TaskTitle" name="TaskTitle">' +
					'</div>' +
					'<div class="six wide field">' +
					'<textarea rows="2" placeholder="Task Description" id="TaskDescription" name="TaskDescription"></textarea>' +
					'</div>' +
					'<div class="three wide field">' +
					'<input name="TaskImage" class="image-file" type="File" hidden/>' +
					'<img src="/Images/Task/default-image.png" class="ui small image" alt="Image" />' +
					'</div>' +
					'<a href="#" class="remove_fields" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
					'<a href="#" class="add_subtask_button" data-tooltip="Add Sub Task"><i class="circular plus icon"></i></a>' +
					'</div></div>');
				

				//In edit mode, when add new task, set Task Id = 0
				//if ($('#edit-task-template-id').val() != 0) {
				//	$(".task-list-field:last-child").prepend('<input hidden name="TaskId" value="0">');
				//}
				onAddTaskFileUploadEvent();
			}
			$(".create-new-task-template-modal").modal("refresh");
		});
	}

	//function ReadUrl(input) {
	//	if (input.files && input.files[0]) {
	//		var 
	//	}
	//}

	function onTaskTemplateFormSubmit() {
		$('#task-template-form-submit-btn').on('click', function () {


			$('#fmtasktemplate').form({
				fields: {
					taskTemplateName: {
						identifier: 'TaskTemplateName',
						rules: [{
							type: 'empty',
							prompt: 'Template name is required'
						}]
					}
				},
				inline: true,
				on: 'blur'
			});

			if ($('#fmtasktemplate').form('is valid')) {
				
				var id = $('#edit-task-template-id').val();
				var TaskTemplate = new FormData();
				
		
					var Tasks = [];
					var TaskCount = 0;
					$('.tasks').each(function (index, element) {

						var taskJson;

						var taskId = $(this).find('.task-list-field input[name="taskId"]').val();
						var taskTitle = $(this).find('.task-list-field input[name="TaskTitle"]').val();
						var taskDesc = $(this).find('.task-list-field textarea[name="TaskDescription"]').val();
						var image;

						if ($(this).find('.task-list-field input[name="TaskImage"]')[0].files.length > 0) {
							image = $(this).find('.task-list-field input[name="TaskImage"]').get(0).files[0];
						}
			
						if (taskTitle != undefined && taskDesc != undefined) {
							TaskTemplate.append('Tasks[' + TaskCount + '].Id', taskId);
							TaskTemplate.append('Tasks[' + TaskCount+ '].Title', taskTitle);
							TaskTemplate.append('Tasks[' + TaskCount+ '].Description', taskDesc);
							TaskTemplate.append('Tasks[' + TaskCount+ '].Image', image);

							//var subCount = 0;
							$(this).find('.sub-task-list-field').each(function (index, element) {
								TaskTemplate.append('Tasks[' + TaskCount + '].SubTasks[' + index + '].Id', $(this).find('input[name = "subTaskId"]').val());
								TaskTemplate.append('Tasks[' + TaskCount + '].SubTasks[' + index + '].Title', $(this).find('input[name = "SubTaskTitle"]').val());
								TaskTemplate.append('Tasks[' + TaskCount + '].SubTasks[' + index + '].Description', $(this).find('textarea[name ="SubTaskDescription"]').val());
								TaskTemplate.append('Tasks[' + TaskCount + '].SubTasks[' + index + '].Image', $(this).find('input[name ="SubTaskImage"]').get(0).files[0]);
							});
							TaskCount= TaskCount + 1;
						}
						

				});
					TaskTemplate.append('Id',id);
					TaskTemplate.append('Name', $("#TaskTemplateName").val());
					TaskTemplate.append('Type', $("#TaskTemplateType").val());


				if (id == 0) {
					$.ajax({
						type: "POST",
						url: "/Property/AddTaskTemplate",
						data: TaskTemplate,
						processData: false,
						contentType : false,
						success: function (data) {
							$(".create-new-task-template-modal").modal('hide');
							data.success ? swal("New Task Template has been Created .", "", "success") : swal("New Task Template Fail to Create.", "", "error");
							tableTaskTemplate.ajax.reload(null, false);
						}
					});
				}
				else {
					$.ajax({
						type: "POST",
						url: "/Property/EditTaskTemplate",
						data: TaskTemplate,
						processData: false,
						contentType: false,
						success: function (data) {
							$(".create-new-task-template-modal").modal('hide');
							data.success ? swal("New Task Template has been Created .", "", "success") : swal("New Task Template Fail to Create.", "", "error");
							tableTaskTemplate.ajax.reload(null, false);
						}
					});
				}
			}
		});
	}

	//function onTaskTemplateFormSubmit() {
	//	$('#task-template-form-submit-btn').on('click', function () {
		
			
	//		$('#fmtasktemplate').form({
	//			fields: {
	//				taskTemplateName: {
	//					identifier: 'TaskTemplateName',
	//					rules: [{
	//						type: 'empty',
	//						prompt: 'Template name is required'
	//					}]
	//				}
	//			},
	//			inline: true,
	//			on: 'blur'
	//		});

	//		if ($('#fmtasktemplate').form('is valid')) {
	//			var id = $('#edit-task-template-id').val();
	//			if (id == 0) {
	//				var fdata = new FormData();

	//				//Task Template Name
	//				fdata.append("TaskTemplateName", $("#TaskTemplateName").val());

	//				//Task Template Type
	//				fdata.append("taskTemplateType", $("#TaskTemplateType").val());


	//				var Tasks = [];

	//				$('.tasks').each(function (index, element) {

	//					var taskJson;
	//					var subTaskJson;
	//					var SubTasks = [];

	//					var taskTitle = $(this).find('.task-list-field input[name="TaskTitle"]').val();
	//					var taskDesc = $(this).find('.task-list-field textarea[name="TaskDescription"]').val();
	//					//$('input[name="TaskImage"]').each(function (a, b) {
	//					//var fileInput = $('.task-list-field input[name="TaskImage"]');
						
	//					var image;
	//					//alert(image);
	//					if ($('.task-list-field input[name="TaskImage"]')[0].files.length > 0) {
	//							var image = $('.task-list-field input[name="TaskImage"]')[0].files[0]
	//							//fdata.append("TaskImage", file);
	//							//image = file;
	//						}
	//					//});
	//					alert(image);
	//					console.log(image);



	//					if (taskTitle != undefined && taskDesc != undefined) {

	//						$(this).find('.sub-task-list-field').each(function (index, element) {
	//							subTaskJson = {
	//								Title: $(this).find('input[name = "SubTaskTitle"]').val(),
	//								Description: $(this).find('textarea[name ="SubTaskDescription"]').val()
	//							};
	//							SubTasks.push(subTaskJson);

	//							//alert($(this).find('input[name = "SubTaskTitle"]').val());
	//							//alert($(this).find('textarea[name ="SubTaskDescription"]').val());

	//						});
	//						taskJson = {
	//							Title: taskTitle,
	//							Description: taskDesc,
	//							Image: image,
	//							SubTasks: SubTasks
	//						}
	//						Tasks.push(taskJson);

	//					}

	//				});
	//				fdata.append("Tasks", JSON.stringify(Tasks));
	//				console.log(Tasks)
	//				//// Task Title
	//				//$("input[name='TaskTitle']").each(function (a, b) {
	//				//	fdata.append("TaskTitle", b.value);
	//				//});

	//				//// Task Description
	//				//$("textarea[name='TaskDescription']").each(function (a, b) {
	//				//	fdata.append("TaskDescription", b.value);
	//				//});

	//				//Task Image
	//				//$('input[name="TaskImage"]').each(function (a, b) {
	//				//	var fileInput = $('input[name="TaskImage"]')[a];
	//				//	if (fileInput.files.length > 0) {
	//				//		var file = fileInput.files[0];
	//				//		fdata.append("TaskImage", file);
	//				//	}
	//				//});


	//				debugger;

	//				$.ajax({
	//					type: "POST",
	//					url: "/Property/AddTaskTemplate",
	//					data: fdata, //$("#fmtasktemplate").serialize(),
	//					processData: false,
	//					contentType: false,
	//					success: function (data) {
	//						$(".create-new-task-template-modal").modal('hide');
	//						data.success ? swal("New Task Template has been Created .", "", "success") : swal("New Task Template Fail to Create.", "", "error");
	//						tableTaskTemplate.ajax.reload(null, false);
	//					}
	//				});
	//			}
	//			else {

	//				var fEditdata = new FormData();

	//				//Task Template Id
	//				fEditdata.append("TaskTemplateId", parseInt($('#edit-task-template-id').val()));

	//				//Task Template Name
	//				fEditdata.append("TaskTemplateName", $("#TaskTemplateName").val());

	//				//Task Template Type
	//				fEditdata.append("taskTemplateType", $("#TaskTemplateType").val());

	//				//TaskId(s)
	//				$("input[name='TaskId']").each(function (a, b) {
	//					fEditdata.append("TaskId", parseInt(b.value));
	//				});

	//				var Tasks = [];

	//				$('.tasks').each(function (index, element) {

	//					var taskJson;
	//					var subTaskJson;
	//					var SubTasks = [];

	//					var taskTitle = $(this).find('.task-list-field input[name="TaskTitle"]').val();
	//					var taskDesc = $(this).find('.task-list-field textarea[name="TaskDescription"]').val();


	//					if (taskTitle != undefined && taskDesc != undefined) {

	//						$(this).find('.sub-task-list-field').each(function (index, element) {
	//							subTaskJson = {
	//								Title: $(this).find('input[name = "SubTaskTitle"]').val(),
	//								Description: $(this).find('textarea[name ="SubTaskDescription"]').val()
	//							};
	//							SubTasks.push(subTaskJson);

	//							//alert($(this).find('input[name = "SubTaskTitle"]').val());
	//							//alert($(this).find('textarea[name ="SubTaskDescription"]').val());

	//						});
	//						taskJson = {
	//							Title: taskTitle,
	//							Description: taskDesc,
	//							SubTasks: SubTasks
	//						}
	//						Tasks.push(taskJson);

	//					}

	//				});
	//				fEditdata.append("Tasks", JSON.stringify(Tasks));
	//				console.log(Tasks)
	//				//// Task Title
	//				//$("input[name='TaskTitle']").each(function (a, b) {
	//				//	fEditdata.append("TaskTitle", b.value);
	//				//});

	//				//// Task Description
	//				//$("textarea[name='TaskDescription']").each(function (a, b) {
	//				//	fEditdata.append("TaskDescription", b.value);
	//				//});

	//				//Task Image
	//				$('input[name="TaskImage"]').each(function (a, b) {
	//					var fileInput = $('input[name="TaskImage"]')[a];
	//					if (fileInput.files.length > 0) {
	//						var file = fileInput.files[0];
	//						fEditdata.append("TaskImage", file);
	//					} else {
	//						var noFile = new File([""], "NoNewFile");
	//						fEditdata.append("TaskImage", noFile);
	//					}
	//				});

	//				debugger;

	//				$.ajax({
	//					type: "POST",
	//					url: "/Property/EditTaskTemplate",
	//					data: fEditdata,//$("#fmtasktemplate").serialize(),
	//					processData: false,
	//					contentType: false,
	//					success: function (data) {
	//						$('.create-new-task-template-modal').modal('hide');
	//						data.success ? swal("Changes has been saved .", "", "success") : swal("Fail to Save Changes.", "", "error");
	//						tableTaskTemplate.ajax.reload(null, false);
	//					}
	//				});
	//			}
	//		}
	//	});
	//}

	//function onTaskTemplateFormSubmit() {
	//	$('#task-template-form-submit-btn').on('click', function () {
	//		$('#fmtasktemplate').form({
	//			fields: {
	//				taskTemplateName: {
	//					identifier: 'TaskTemplateName',
	//					rules: [{
	//						type: 'empty',
	//						prompt: 'Template name is required'
	//					}]
	//				}
	//			},
	//			inline: true,
	//			on: 'blur'
	//		});

	//		if ($('#fmtasktemplate').form('is valid')) {
	//			var id = $('#edit-task-template-id').val();
	//			if (id == 0) {
	//				var fdata = new FormData();

	//				//Task Template Name
	//				fdata.append("TaskTemplateName", $("#TaskTemplateName").val());

	//				//Task Template Type
	//				fdata.append("taskTemplateType", $("#TaskTemplateType").val());

	//				// Task Title
	//				$("input[name='TaskTitle']").each(function (a, b) {
	//					fdata.append("TaskTitle", b.value);
	//				});

	//				// Task Description
	//				$("textarea[name='TaskDescription']").each(function (a, b) {
	//					fdata.append("TaskDescription", b.value);
	//				});

	//				//Task Image
	//				$('input[name="TaskImage"]').each(function (a, b) {
	//					var fileInput = $('input[name="TaskImage"]')[a];
	//					if (fileInput.files.length > 0) {
	//						var file = fileInput.files[0];
	//						fdata.append("TaskImage", file);
	//					}
	//				});


	//				debugger;

	//				$.ajax({
	//					type: "POST",
	//					url: "/Property/AddTaskTemplate",
	//					data: fdata, //$("#fmtasktemplate").serialize(),
	//					processData: false,
	//					contentType: false,
	//					success: function (data) {
	//						$(".create-new-task-template-modal").modal('hide');
	//						data.success ? swal("New Task Template has been Created .", "", "success") : swal("New Task Template Fail to Create.", "", "error");
	//						tableTaskTemplate.ajax.reload(null, false);
	//					}
	//				});
	//			}
	//			else {

	//				var fEditdata = new FormData();

	//				//Task Template Id
	//				fEditdata.append("TaskTemplateId", parseInt($('#edit-task-template-id').val()));

	//				//Task Template Name
	//				fEditdata.append("TaskTemplateName", $("#TaskTemplateName").val());

	//				//Task Template Type
	//				fEditdata.append("taskTemplateType", $("#TaskTemplateType").val());

	//				//TaskId(s)
	//				$("input[name='TaskId']").each(function (a, b) {
	//					fEditdata.append("TaskId", parseInt(b.value));
	//				});

	//				// Task Title
	//				$("input[name='TaskTitle']").each(function (a, b) {
	//					fEditdata.append("TaskTitle", b.value);
	//				});

	//				// Task Description
	//				$("textarea[name='TaskDescription']").each(function (a, b) {
	//					fEditdata.append("TaskDescription", b.value);
	//				});

	//				//Task Image
	//				$('input[name="TaskImage"]').each(function (a, b) {
	//					var fileInput = $('input[name="TaskImage"]')[a];
	//					if (fileInput.files.length > 0) {
	//						var file = fileInput.files[0];
	//						fEditdata.append("TaskImage", file);
	//					} else {
	//						var noFile = new File([""], "NoNewFile");
	//						fEditdata.append("TaskImage", noFile);
	//					}
	//				});

	//				debugger;

	//				$.ajax({
	//					type: "POST",
	//					url: "/Property/EditTaskTemplate",
	//					data: fEditdata,//$("#fmtasktemplate").serialize(),
	//					processData: false,
	//					contentType: false,
	//					success: function (data) {
	//						$('.create-new-task-template-modal').modal('hide');
	//						data.success ? swal("Changes has been saved .", "", "success") : swal("Fail to Save Changes.", "", "error");
	//						tableTaskTemplate.ajax.reload(null, false);
	//					}
	//				});
	//			}
	//		}
	//	});
	//}

	//function onEditTaskTemplate() {
	//	$(document).on('click', '.edit-task-template-btn', function (e) {
	//		e.preventDefault();
	//		ClearTaskTemplateModalForm();
	//		var id = $(this).parents('tr').attr('data-task-template-id');
	//		$('#edit-task-template-id').val(id);
	//		debugger;
	//		//Get the task template & associated tasks from database
	//		$.ajax({
	//			type: "POST",
	//			url: "/Property/GetTaskTemplateWithTask",
	//			data: { taskTemplateId: id },
	//			success: function (data) {
	//			    debugger;
	//				$(".create-new-task-template-modal").modal('show');
	//				//Populate the data from the response before show the dialog
	//				$("#TaskTemplateName").val(data.TaskTemplate.TemplateName);
	//				$('#TaskTemplateType').dropdown('set selected', data.TaskTemplate.TemplateTypeId);

	//				for (var e = 0; e < data.Tasks.length; e++) {
	//					var task = data.Tasks[e];

	//					$('.task-fields-wrapper').append('<div class= "ui form tasks" style = "border-top: 1px solid black;border-bottom: 1px solid black">'+
	//						'<div class="fields task-list-field">' +
	//						'<input hidden name="TaskId" value="' + task.Id + '">' +
	//						'<div class="four wide field">' +
	//						'<input type="text" placeholder="Task Title" id="TaskTitle" name="TaskTitle" value="' + task.Title + '">' +
	//						'</div>' +
	//						'<div class="eight wide field">' +
	//						'<textarea rows="2" placeholder="Task Description" id="TaskDescription" name="TaskDescription">' + task.Description + '</textarea>' +
	//						'</div>' +
	//						'<div class="four wide field">' +
	//						//'<a class="ui green right labeled icon button" data-tooltip="Click to View Image" href="/Property/RetrieveImage?taskId=' + task.Id + '" target="_blank">Image</a>' +
	//						'<input class="edit-task-image" id="EditTaskImage' + task.Id + '" name="TaskImage" type="File"/>' +
	//						'<a id="EditTaskImage' + task.Id + '" href="/Property/RetrieveImage?taskId=' + task.Id + '" target="_blank">' +
	//						'<img src="/Property/RetrieveImage?taskId=' + task.Id + '" alt="" height=100 width=100 />' +
	//						'</a>' +
	//						'</br></br><a id="btnEditTaskImage' + task.Id + '" class="ui button green" onclick="EditTaskImage(' + task.Id + ');">Change Image</a>' +
	//						'</br></br><a id="btnUndoTaskImage' + task.Id + '" class="ui button green" style="display:none;" onclick="UndoTaskImage(' + task.Id + ');">Undo</a>' +
	//						'</div>' +

	//						'</div>' +
	//						'<div class="fields sub-task-list-field">' +
	//						'<div class="four wide field">' +
	//						'<i class="icon arrow right" style="float:right"></i>' +
	//						'</div>' +
	//						'<div class="four wide field">' +
	//						'<input type="text" placeholder="Sub-Task Title" id="SubTaskTitle" name="SubTaskTitle">' +
	//						'</div>' +
	//						'<div class="six wide field">' +
	//						'<textarea rows="2" placeholder="Task Description" id="SubTaskDescription" name="SubTaskDescription"></textarea>' +
	//						'</div>' +
	//						'<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
	//						'<a href="#" class="add_subtask_button" data-tooltip="Add Sub Task"><i class="circular plus icon"></i></a>' +
	//						'</div>'+
	//						/*
	//						'<div class="four wide field">' +
	//							'<select class="ui search dropdown" id="task-status-filter" placeholder="TaskStatus">' +
	//								'<option value="">Select Status</option>' +
	//								'<option value="1">New</option>' +
	//								'<option value="2">Completed</option>' +
	//							'</select>' +
	//						'</div>'
	//						+*/
	//						'<a href="#" class="remove_field" data-tooltip="Delete" onclick="deleteTaskOnEditMode(' + task.Id + ',' + task.TemplateId + ')"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
	//				}
	//				$(".create-new-task-template-modal").modal("refresh");
	//			},
	//			error: function (jqxhr, status, exception) {
	//			    debugger;
 //                   alert('Exception:', exception);
 //               }
	//		});
	//	});
	//}
	function onImageClick() {
		$(document).on('click', '.image', function (e) {
			$(this).parent().find('input[class="image-file"]').click();
		});
	}
	function onImageFileChange(){
		$(document).on('change', '.image-file', function (e) {
			var imageUrl = window.URL.createObjectURL(this.files[0]);
			//alert(imageUrl);
			//var path = (window.URL || window.webkitURL).createObjectURL(this.files[0]);
			$(this).next().attr("src", imageUrl);
			//alert($(this).next().attr("src"));
		});
	}
	function onEditTaskTemplate() {
		$(document).on('click', '.edit-task-template-btn', function (e) {
			e.preventDefault();
			ClearTaskTemplateModalForm();
			var id = $(this).parents('tr').attr('data-task-template-id');
			$('#edit-task-template-id').val(id);
			debugger;
			//Get the task template & associated tasks from database
			$.ajax({
				type: "POST",
				url: "/Property/GetTaskTemplateWithTask",
				data: { taskTemplateId: id },
				success: function (data) {
					var Template = data.Template;
					console.log(Template);
					$(".create-new-task-template-modal").modal('show');
					//Populate the data from the response before show the dialog
					$("#TaskTemplateName").val(Template.Name);
					$('#TaskTemplateType').dropdown('set selected',Template.Type);

					for (var e = 0; e < Template.TaskList.length; e++) {
						var task = Template.TaskList[e];
						var subtask = '';
						for (var x = 0; x < task.SubTaskList.length; x++) {
							var sub = task.SubTaskList[x];
							subtask += '<div class="fields sub-task-list-field ">' +
								'<input type="hidden"  name="subTaskId" value="' + sub.Id + '" />' +
								'<div class="four wide field">' +
								'<i class="icon arrow right" style="float:right"></i>' +
								'</div>' +
								'<div class="four wide field">' +
								'<input type="text" placeholder="Sub-Task Title"  name="SubTaskTitle" value="' + sub.Title + '">' +
								'</div>' +
								'<div class="six wide field">' +
								'<textarea rows="2" placeholder="Task Description"  name="SubTaskDescription">' + sub.Description + '</textarea>' +
								'</div>' +
								'<div class="three wide field">' +
								'<input name="SubTaskImage" class="image-file" type="File" hidden/>' +
								'<img src="' + (sub.ImageUrl != null ? sub.ImageUrl:'/Images/SubTask/default-image.png') + '" class="ui small image" alt="Image" />' +
								'</div>' +
								'<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
								'<a href="#" class="add_subtask_button" data-tooltip="Add Sub Task"><i class="circular plus icon"></i></a>' +
								'</div>'

						}

						$('.task-fields-wrapper').append('<div class="ui form tasks ui clearing segment" ><div class="fields task-list-field">' +
							'<input type="hidden"  name="taskId" value="' + task.Id + '" />' +
							'<div class="four wide field">' +

							'<input type="text" placeholder="Task Title" name="TaskTitle" value="' + task.Title + '">' +
							'</div>' +
							'<div class="six wide field">' +
							'<textarea rows="2" placeholder="Task Description" name="TaskDescription" >' + task.Description + '</textarea>' +
							'</div>' +
							'<div class="three wide field">' +
							'<input name="TaskImage" class="image-file" type="File" hidden/>' +
							'<img src="' + (task.ImageUrl != null ? task.ImageUrl:'/Images/Task/default-image.png') + '" class="ui small image" alt="Image" />' +
							'</div>' +
							'<a href="#" class="remove_fields" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
							'<a href="#" class="add_subtask_button" data-tooltip="Add Sub Task"><i class="circular plus icon"></i></a>' +
							'</div>'+
							subtask+
							'</div>');

					
					}
					$(".create-new-task-template-modal").modal("refresh");
				},
				error: function (jqxhr, status, exception) {
					alert('Exception:', exception);
				}
			});
		});
	}

	function ClearTaskTemplateModalForm() {
		$("#TaskTemplateName").val("");
		$("input[name='TaskTitle']").val("");
		$("textarea[name='TaskDescription']").val("");
		$(".tasks").remove();
	}

	function BindTaskTemplateOptions() {
		$.ajax({
			type: "GET",
			url: "/Property/GetTaskTemplateList",
			success: function (response) {
				$("#task-template-property").find('option').remove().end();
				var items = response.data;
				var optionControl = "<option value=''>Select Task Template</opion>";
				optionControl += "<option value='-1'>None</opion>";
				for (var e = 0; e < items.length; e++) {
					optionControl += "<option value=\"" + items[e].Id + "\">" + items[e].TemplateName + "</option>";
				}
				$("#task-template-property").append(optionControl);
			}
		});
	}

	function onAddTaskFileUploadEvent() {
		//    $('input[name="TaskImage"]').on('change',function (e) {
		//        debugger
		//        taskFiles.push(e.target.files);
		//        //var myID = 3; //uncomment this to make sure the ajax URL works
		//        if (taskFiles.length > 0) {
		//            if (window.FormData !== undefined) {
		//                taskFileFormData = new FormData();
		//                for (var x = 0; x < taskFiles.length; x++) {
		//                    taskFileFormData.append("task" + x, taskFiles[x]);
		//                }
		//            }
		//        }
		//    });
	}
});

function UpdateIsActive(e) {

	var propertyId = $(e).parents('tr').attr('data-property-id');
	var isActive = $(e).is(":checked")

	$.ajax({
		type: "POST",
		url: "/Property/UpdateIsActive",
		data: { propertyId: propertyId, isActive: isActive },
		success: function (data) {
			
		}
	});


}

function formatDateTimeDisplayWithMonth(dateTimeValue) {
	var validDateTime = new Date(parseInt(dateTimeValue.substr(6)));
	return monthNames[validDateTime.getMonth()] + " " + validDateTime.getDate() + ", " + validDateTime.getFullYear();
}

function deleteTaskOnEditMode(taskId, taskTemplateId) {
	event.preventDefault();
	$.ajax({
		type: "POST",
		url: "/Property/DeleteTask",
		data: { taskTemplateId: taskTemplateId, taskId: taskId },
		success: function (data) {
			var taskDeleteNotificationLabel = $("#taskDeleteNotificationLabel");
			taskDeleteNotificationLabel.show();
			setTimeout(function () { taskDeleteNotificationLabel.hide() }, 3000);
		}
	});
}

function EditTaskImage(taskId) {
	$("a#EditTaskImage" + taskId).hide();
	$("#btnEditTaskImage" + taskId).hide();
	$("input#EditTaskImage" + taskId).show();
	$("#btnUndoTaskImage" + taskId).show();
}

function UndoTaskImage(taskId) {
	$("a#EditTaskImage" + taskId).show();
	$("#btnEditTaskImage" + taskId).show();
	$("input#EditTaskImage" + taskId).hide();
	$("input#EditTaskImage" + taskId).val("");
	$("#btnUndoTaskImage" + taskId).hide();
}


var monthNames = ["January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December"
];