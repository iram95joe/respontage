﻿
$('.demo.menu .item').tab({ history: false });
var tableHistory;
$(document).ready(function () {
	GetContract();
	ContractChange();
	//GetReservations();
	OnchangeBatchPaymentImage();
	OnchangePaymentImage();
	AddIncome();
	AddBatchPayment();
	IncomeBalanceKeyup();
	BatchBalanceKeyup();
	BatchPaymentKeyup();
	AddMoveinCost();
	RemoveMoveinCost();
	AddUtilities();
	RemoveUtilities();
	GetTitle();
	SetActionButtons();
	SetPostDateStart();
	DateChangesRules();
	OnChangeCheckEvent();
	AddPostDated();
	AddRentAmountApplied();
	RemoveRentAmount();
	RemovePostDated();
	AddRenter();
	RemoveRenter();
	onIncomeSubmit();
	OnchangeGuestStatus();
	AllowEdit();
	//Contract actions
	Save();
	GetBookingDetails();
	GetHistory();
	GetRequestBooking();


	//Get Display label of page
	function GetTitle() {
		if ($("#BookingId").val() == '') {
			$('#contract-title').text("Create New Contract");
		}
		else {
			$('#contract-title').text("Contract Details");
		}
	}
	function SetActionButtons() {
		if ($('#BookingId').val() == '') {
			$('#end-contract-btn').hide();
			$('#new-contract-btn').hide();
		}
		else {
			$('#end-contract-btn').show();
			$('#new-contract-btn').show();
		}
	}
	function GetContract() {
		$.ajax({
			type: "GET",
			url: '/Rentals/GetContract',
			data: { propertyId: $("#PropertyId").val() },
			dataType: "json",
			async: false,
			success: function (data) {
				if (data.contracts.length > 0) {
					var contracts = data.contracts;
					for (var x = 0; x < contracts.length; x++) {
						$('#contract-list').append('<option value=' + contracts[x].BookingId + '>' + contracts[x].Details + '</option>')
					}

				}
				else {
					$('#contract-list').attr();
				}
			}
		});
	}
	function ContractChange() {
		$(document).on('change', '#contract-list', function () {
			$('#BookingId').val($(this).val());
			GetBookingDetails();
		});
	}
	//Get display label of page end
	function Save() {
		$(document).on('click', '#SaveAndConfirm', function () {
			var hasBlankField = ValidateRequiredFields();
			if (hasBlankField) {
				return;
			}
			swal({
				title: "Are you sure you want to Save?",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: true,
				showLoaderOnConfirm: true
			},
				function () {
					var formData = new FormData();
					var SecurityDepositAmount = $("#SecurityDepositAmount").val();
					var SecurityDepositDueOn = $("#SecurityDepositDueOn").val();
					var isPostDatedPayment = $("#isPostDatedPayment").prop("checked");
					var isSecurityDeposit = $("#isSecurityDeposit").prop("checked");
					var isMonthToMonth = $("#isMonthToMonth").prop("checked");
					var isLateFees = $("#isLateFees").prop("checked");
					var LateFeeAmount = $("#LateFeeAmount").val();
					var LateFeeApplyDays = $("#LateFeeApplyDays").val();
					var RecurringRentStartDate = $("#RecurringRentStartDate").val();
					var CheckedOutDate = $("#CheckedOutDate").val();
					var CheckedInDate = $("#CheckedInDate").val();
					var PropertyId = $("#PropertyId").val();
					var AdvancePaymentMonth = $('#AdvancePaymentMonth').val();
					var isAdvancePayment = $('#isAdvancePayment').prop("checked");
					var isFixedMonthToMonth = $('#isFixedMonthToMonth').prop("checked");
					var isLastMonthPayment = $('#isLastMonthPayment').prop("checked");
					var LastPaymentMonth = $('#LastPaymentMonth').val();
					formData.append('LastPaymentMonth', LastPaymentMonth);
					formData.append('isLastMonthPayment', isLastMonthPayment);
					formData.append('isFixedMonthToMonth', isFixedMonthToMonth);
					formData.append('isAdvancePayment', isAdvancePayment);
					formData.append('AdvancePaymentMonth', AdvancePaymentMonth);
					formData.append('PropertyId', PropertyId);
					formData.append('LateFeeApplyDays', LateFeeApplyDays);
					formData.append('CheckedOutDate', CheckedOutDate);
					formData.append('CheckedInDate', CheckedInDate);
					formData.append('RecurringRentStartDate', RecurringRentStartDate);
					formData.append('LateFeeAmount', LateFeeAmount);
					formData.append('SecurityDepositAmount', SecurityDepositAmount);
					formData.append('SecurityDepositDueOn', SecurityDepositDueOn);
					formData.append('isLateFees', isLateFees);
					formData.append('isMonthToMonth', isMonthToMonth);
					formData.append('isSecurityDeposit', isSecurityDeposit);
					formData.append('isPostDatedPayment', isPostDatedPayment);
					//var files = $("#fileUploadDocument").get(0).files;
					//for (var i = 0; i < files.length; i++) {
					//	formData.append("files", files[i]);
					//}

					var AdditionalFees = [];
					//JM 10/08/19 Set Utilities default to month
					$('.utilities').each(function (index, element) {
						if ($(this).find('.description').val() != null && $(this).find('.description').val() != "") {
							var jsonFee = {
								Id: $(this).find('.fields').attr('additional-fee-id'),
								Amount: $(this).find('.amount').val(),
								Description: $(this).find('.description').val(),
								Frequency: $(this).find('.frequency').dropdown('get value'),
								DueOn: $(this).find('.due-on').val(),
								Type: 2

							};
							AdditionalFees.push(jsonFee);
						}
					});

					$('.movein').each(function (index, element) {

						if ($(this).find('.description').val() != null && $(this).find('.description').val() != "") {
							var jsonFee = {
								Id: $(this).find('.fields').attr('additional-fee-id'),
								Amount: $(this).find('.amount').val(),
								Description: $(this).find('.description').val(),
								Frequency: 4,
								DueOn: $(this).find('.due-on').val(),
								Type: 1
							};
							AdditionalFees.push(jsonFee);
						}
					});
					var Rent = [];
					$('.rent').each(function (index, element) {
						var amount = $(this).find('.rent-amount').val();
						var date = $(this).find('.rent-date-apply').val();
						if ((amount != "" && amount != null) && (date != "" && date != null)) {
							var jsonFee = {
								Id: $(this).find('.fields').attr('rent-id'),
								Amount: amount,
								EffectiveDate: date
							};
							Rent.push(jsonFee);
						}
					});
					var PostDated = [];
					$('.post-date').each(function (index, element) {
						var start = $(this).find('.post-date-start').val();
						var end = $(this).find('.post-date-end').val();
						var amount = $(this).find('.amount').val();
						if ((start != null && start != "") && (end != null && end != "") && (amount != null && amount != "")) {
							var jsonFee = {
								Id: $(this).find('.fields').attr('post-dated-id'),
								DateStart: start,
								DateEnd: end,
								Amount: amount
							};
							PostDated.push(jsonFee);
						}

					});
					var Renter = [];
					$('.renter').each(function (index, element) {
						var firstname = $(this).find('.guestFirstname').val();
						var lastname = $(this).find('.guestLastname').val();
						var email = $(this).find('.guestEmail').val();
						var phone = $(this).find('.guestPhone').val();
						var address = $(this).find('.guestAddress').val();
						var status = $(this).find('.guestStatus').dropdown('get value');
						var start = $(this).find('.start-date').val();
						var end = $(this).find('.end-date').val();
						if (firstname != "" && lastname != null) {
							var jsonFee = {
								Id: $(this).find('.fields').attr('renter-id'),
								Lastname: lastname,
								Firstname: firstname,
								Email: email,
								PhoneNumber: phone,
								Address: address,
								IsActive: status,
								StartDate: start,
								EndDate: end
							};
							Renter.push(jsonFee);
						}
					});
					formData.append('RenterString', JSON.stringify(Renter));
					formData.append('RentString', JSON.stringify(Rent));
					formData.append('PostDatedString', JSON.stringify(PostDated));
					formData.append('AdditionalFeeString', JSON.stringify(AdditionalFees));
					if ($("#BookingId").val() == null || $("#BookingId").val() == "") {
						$.ajax({
							type: "POST",
							url: '/Home/PropertyPayment',
							data: formData,
							beforeSend: function () {
								$('#SaveAndConfirm').addClass('loading');
								$('#SaveAndConfirm').attr('disabled','true');
							},
							cache: false,
							dataType: "json",
							contentType: false,
							processData: false,
							success: function (data) {
								$('#SaveAndConfirm').removeClass('loading');
								$('#SaveAndConfirm').removeAttr('disabled');
								if (data.success) {
									$("#BookingId").val(data.bookingId);
									var url = document.location.href + "&rid=" + data.bookingId;
									document.location.href = url;
								}

							}
						});
					}
					else {
						formData.append('BookingId', $("#BookingId").val());
						$.ajax({
							type: "POST",
							url: '/Home/EditPropertyPayment',
							data: formData,
							beforeSend: function () {
								$('#SaveAndConfirm').addClass('loading');
							},
							cache: false,
							dataType: "json",
							contentType: false,
							processData: false,
							success: function (data) {
								$('#SaveAndConfirm').removeClass('loading');

								if (data.success != 0) {
									GetBookingDetails();
									GetPropertyPayment();
									tableHistory.ajax.reload(null, false);
								}

							}
						});
					}
				});
		});
	}

	function SetPostDateStart() {
		$(document).on('keyup', '#AdvancePaymentMonth', function () {

			PostDatedStartChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
		});
	}
	function ValidateIncomeEntryRequiredFields() {
		var hasBlankField = false;
		var hasfirstFocus = false;
		$('#income-modal').find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.next().remove();
				obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.next().remove();
			}

		});
		return hasBlankField;
	}
	function onIncomeSubmit() {
		$('#income-save-btn').on('click', function () {
			var hasBlankField = ValidateIncomeEntryRequiredFields();
			if (hasBlankField) {
				return;
			}
			var id = $('#income-id').val();
			var propertyId = $('#PropertyId').val();
			//var incomeTypeId = $('#IncomeTypeId').val();
			var description = $('#Description').val();
			var amount = $('#Amount').val();
			var dueDate = $('#DueDate').val();

			$.ajax({
				type: "POST",
				url: "/Income/EditTransaction",
				data: {
					Id: id,
					//IncomeTypeId: incomeTypeId,
					//BookingId: 0,
					PropertyId: propertyId,
					Description: description,
					Amount: amount,
					//Status: status,
					DueDate: dueDate
				},
				success: function (data) {
					if (data.success) {
						$('#income-modal').modal('hide');
						swal({ title: "Success", text: "Income has been saved.", type: "success" },
							function () {
								GetPropertyPayment();
							});
					}
					else {
						$('#income-modal').modal('hide');
						swal("Error on edit income", "", "error");
					}
				}
			});

		});
	}
	//Evnt on change of check flag
	function OnChangeCheckEvent() {
		$("#isFixedMonthToMonth").change(function () {
			RentDateApplyChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
		});
		$('#isMonthToMonth').change(function () {
			if ($(this).is(":checked")) {
				//$("#CheckedOutDate").val("");
				$("#CheckedOutDate").val("");
				$("#CheckedOutDate").attr("disabled", true);
				$("#CheckedOutDate").next().remove();
				$("#CheckedOutDate").removeAttr("required");
				$("#isFixedMonthToMonth").attr("disabled", true);
				$("#isFixedMonthToMonth").prop("checked", false);
			}
			else {
				$("#CheckedOutDate").val("");
				$("#CheckedOutDate").removeAttr("disabled");
				$("#CheckedOutDate").attr("required", true);
				$("#isFixedMonthToMonth").prop("checked", false);
				$("#isFixedMonthToMonth").attr("disabled", false);
			}
		});
		$("#isPostDatedPayment").change(function () {
			if ($(this).is(":checked")) {
				$('.divPostPayment').removeAttr('hidden');
				$('.divPostPayment :input:not(button)').attr('required', true);
			}
			else {
				$('.divPostPayment').attr('hidden', true);
				$('.divPostPayment :input:not(button)').attr('required', false);
			}

		});

		$("#isSecurityDeposit").change(function () {
			if ($(this).is(":checked")) {
				$('.securityDepositDiv').removeAttr('hidden');
			}
			else {
				$('.securityDepositDiv').attr('hidden', true);
			}

		});
		$("#isAdvancePayment").change(function () {
			if ($(this).is(":checked")) {
				$('.advancePaymentDiv').removeAttr('hidden');
			}
			else {
				$('.advancePaymentDiv').attr('hidden', true);
			}

		});

		$("#isLastMonthPayment").change(function () {
			if ($(this).is(":checked")) {
				$('.lastPaymentDiv').removeAttr('hidden');
			}
			else {
				$('.lastPaymentDiv').attr('hidden', true);
			}

		});
		$("#isLateFees").change(function () {
			if ($(this).is(":checked")) {
				$('.lateFeeDiv').removeAttr('hidden');
			}
			else {
				$('.lateFeeDiv').attr('hidden', true);
			}

		});
	}
	function CheckPostDatedRange(postDatedStart, postDatedEnd, startDate, endDate) {

		$('.post-date').each(function (index, element) {
			var start = $(this).find('.post-date-start');
			var end = $(this).find('.post-date-end');
			if (!start.is(postDatedStart) && !end.is(postDatedEnd)) {
				if ((new Date(startDate) >= new Date(start.val()) && new Date(startDate) <= new Date(end.val())) ||
					(new Date(endDate) >= new Date(start.val()) && new Date(endDate) <= new Date(end.val())) ||

					(new Date(start.val()) >= new Date(startDate) && new Date(start.val()) <= new Date(endDate)) ||
					(new Date(end.val()) >= new Date(startDate) && new Date(end.val()) <= new Date(endDate))

				) {
					ShowMessageDuplicateDates($(postDatedStart.closest('.ui.calendar')), true);
					ShowMessageDuplicateDates($(postDatedEnd.closest('.ui.calendar')), true);
					return false;
				} else {
					ShowMessageDuplicateDates($(postDatedStart.closest('.ui.calendar')), false);
					ShowMessageDuplicateDates($(postDatedEnd.closest('.ui.calendar')), false);
				}
			}
		});
	}
	function PostDatedStartChange(date, endDate) {

		var minDate = new Date(date);

		var advanceMonth = $('#AdvancePaymentMonth').val();
		if (advanceMonth != "" && advanceMonth != null) {
			minDate.setMonth(minDate.getMonth() + parseInt(advanceMonth));
		}
		$('.postDatedStartDate').calendar({
			type: 'month', onChange: function (date, text, mode) {
				CheckPostDatedRange($(this).closest('.fields').find('.post-date-start'), $(this).closest('.fields').find('.post-date-end'), date, new Date($(this).closest('.fields').find('.post-date-end').val()));
				PostDatedEndChange($(this), date, endDate);

			},
			minDate: minDate,
			maxDate: new Date(endDate)

		});
	}

	function RentDateApplyChange(startdate, enddate) {
		//If FixedMonthtoMonth the rent apply date must be no limit
		var isFixedMonthToMonth = $('#isFixedMonthToMonth').prop("checked");
		var firstRentDate = $('.RentDateApply').first();
		firstRentDate.find('.rent-date-apply').val(moment.utc(startdate).format("MMMM YYYY"));
		firstRentDate.find('.rent-date-apply').attr('disabled', true);
		$('.RentDateApply').calendar({
			type: 'month', onChange: function (date, text, mode) {
				var thisObj = $(this).find('.rent-date-apply');
				var thisDate = moment(date).format("MMMM YYYY");
				$('.rent-date-apply').each(function (index, element) {
					if (!$(this).is(thisObj)) {
						if (thisDate == moment($(this).val()).format("MMMM YYYY")) {
							ShowMessageDuplicateDates($(thisObj.closest('.RentDateApply')), true);
							return false;
						}
						else {
							ShowMessageDuplicateDates($(thisObj.closest('.RentDateApply')), false);
						}
					}
				});
			}, minDate: new Date(startdate),
			maxDate: isFixedMonthToMonth == true ? null : new Date(enddate)
		});
	}

	function RecurringDateChange(startdate, enddate) {
		$('.dateRecurringDate').calendar({
			type: 'date', onChange: function (date, text, mode) {
				PostDatedStartChange(date, $('#CheckedOutDate').val());
				RentDateApplyChange(date, $('#CheckedOutDate').val());
			}
			, minDate: new Date(startdate),
			maxDate: new Date(enddate)
		});
	}
	function CheckInDateChange() {
		$('.CheckedInDate').calendar({
			type: 'date', onChange: function (date, text, mode) {
				var maxDate = new Date($('#CheckedOutDate').val());
				RentDateApplyChange(date, $('#CheckedOutDate').val());
				RecurringDateChange(date, $('#CheckedOutDate').val());
				PostDatedStartChange($('#RecurringRentStartDate').val(), maxDate);
				CheckOutChange(date);
				if ($('#BookingId').val() == "") {
					$('.start-date').val(moment.utc(date).format("MMMM D,YYYY"));
				}
			}
		});
		RecurringDateChange($('#CheckedInDate').val(), $('#CheckedOutDate').val());
	}
	function PostDatedEndChange(postDatedObj, date, endDate) {
		var isFixedMonthToMonth = $('#isFixedMonthToMonth').prop("checked");
		var minDate = new Date(date);
		var maxDate = new Date(endDate);

		$(postDatedObj.closest('.fields').find('.postDatedDate')).calendar({
			type: 'month',
			minDate: minDate,
			maxDate: isFixedMonthToMonth == true ? null : new Date(maxDate),
			onChange: function (date, text, mode) {
				CheckPostDatedRange($(this).closest('.fields').find('.post-date-start'), $(this).closest('.fields').find('.post-date-end'), new Date($(this).closest('.fields').find('.post-date-start').val()), date);
			}
		});
	}
	function CheckOutChange(date) {
		$('.CheckedOutDate').calendar({
			type: 'date', onChange: function (date, text, mode) {
				PostDatedStartChange($('#RecurringRentStartDate').val(), date);

				$('.post-date').each(function (index, element) {
					PostDatedEndChange($(this).find('.postDatedStartDate'), $(this).find('.post-date-start').val(), date);
				});
				RentDateApplyChange($('#RecurringRentStartDate').val(), date);
				RecurringDateChange($('#CheckedInDate').val(), date);
			}, minDate: new Date(date)

		});
	}
	function DateChangesRules() {
		$('.renterDate').calendar({
			type: 'date'
		});
		$('.dateInputField').calendar({
			type: 'date'
		});
		CheckInDateChange();
		PostDatedStartChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
		$('.post-date').each(function (index, element) {
			PostDatedEndChange($(this).find('.postDatedStartDate'), $(this).find('.post-date-start').val(), $('#CheckedOutDate').val());
		});
		RentDateApplyChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
		CheckOutChange($('#CheckedInDate').val());

	}
	var PostDatedTemplate = '<div class="post-date"><div class="fields" post-dated-id="0">' +
		'<div class="six wide field">' +
		'<label>Post Dated Start</label>' +
		'<div class="ui calendar postDatedStartDate">' +
		'<div class="ui input left icon">' +
		'<i class="calendar icon"></i>' +
		'<input type="text" class="post-date-start" name="PostPaymentStartDate" placeholder="Post Dated Start" />' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="six wide field">' +
		'<label>Post Dated Until</label>' +
		'<div class="ui calendar postDatedDate">' +
		'<div class="ui input left icon">' +
		'<i class="calendar icon"></i>' +
		'<input type="text" class="post-date-end" name="PostPaymentEndDate" placeholder="Post Dated Until" />' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="six wide field">' +
		'<label>Post dated amount</label>' +
		'<input type="text" class="amount" onkeypress="return isNumberKey(this, event);" name="PostDatedAmount" placeholder="Amount" />' +
		'</div>' +
		'<div class="field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-post-dated" data-tooltip="Add">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'</button>' +
		'<button type="button" class="circular ui icon orange mini button remove-post-dated" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div></div>';
	function AddPostDated() {
		$(document).on('click', '.add-post-dated', function () {
			$('#post-dated-div').append(PostDatedTemplate);
			PostDatedStartChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
		});

	}
	var AddRentTemplate = '<div class="rent"><div class="fields" rent-id="0">' +
		'<div class="six wide field">' +
		'<label>Rent Amount</label>' +
		'<input type="text" onkeypress="return isNumberKey(this, event);"  class="rent-amount" required placeholder="Rent Amount" />' +
		'</div>' +
		'<div class="six wide field">' +
		'<label>Date Apply</label>' +
		'<div class="ui calendar RentDateApply">' +
		'<div class="ui input left icon">' +
		'<i class="calendar icon"></i>' +
		'<input type="text" class="rent-date-apply" name="RentDateApply" required placeholder="Rent Date Apply" />' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-rent-amount" data-tooltip="Add">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'</button>' +
		'<button type="button" class="circular ui icon orange mini button remove-rent" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div></div>';
	function AddRentAmountApplied() {
		$(document).on('click', '.add-rent-amount', function () {
			$('#rent-div').append(AddRentTemplate);
			RentDateApplyChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
		});
	}

	var AddRenterTemplate = '<div class="renter">' +
		'<div class="fields" renter-id="0">' +
		'<div class="three wide field">' +
		'<label>First Name</label>' +
		'<input type="text" class="guestFirstname" required placeholder="First Name" />' +
		'</div>' +
		'<div class="three wide field">' +
		'<label>Last Name</label>' +
		'<input type="text" class="guestLastname" required placeholder="Last Name" />' +
		'</div>' +
		'<div class="three wide field">' +
		'<label>Email</label>' +
		'<input type="text" class="guestEmail" placeholder="Email" />' +
		'</div>' +
		'<div class="three wide field">' +
		'<label>Phone Number</label>' +
		'<input type="text" class="guestPhone" placeholder="Phone" />' +
		'</div>' +
		'<div class="five wide field">' +
		'<label>Address</label>' +
		'<input type="text" class="guestAddress" required placeholder="Address" />' +
		'</div>' +
		'<div class="three wide field">' +
		'<label>Start Date</label>' +
		'<div class="ui calendar renterDate">' +
		'<div class="ui input left icon">' +
		'<i class="calendar icon"></i>' +
		'<input type="text" class="start-date" required placeholder="Start Date"/>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="three wide field">' +
		'<label>End Date</label>' +
		'<div class="ui calendar renterDate">' +
		'<div class="ui input left icon">' +
		'<i class="calendar icon"></i>' +
		'<input type="text"  class="end-date"  placeholder="End Date"/>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="two wide field">' +
		'<label>Status</label>' +
		'<select class="ui dropdown guestStatus">' +
		'<option selected value="true">Active</option>' +
		'<option value="false">Inactive</option>' +
		'</select>' +
		'</div>' +
		'<div class="field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-renter" data-tooltip="Add">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'</button>' +
		'<button type="button" class="circular ui icon orange mini button remove-renter" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div>' +
		'</div>';
	function AddRenter() {
		$(document).on('click', '.add-renter', function () {
			//alert(AddRenterTemplate);
			$('#renter-div').append(AddRenterTemplate);
			$('.renterDate').calendar({
				type: 'date'
			});
			if ($('#BookingId').val() == "") {
				$('.start-date').val(moment.utc($('#CheckedInDate').val()).format("MMMM D,YYYY"));
			}
			else {
				$('#renter-div').find('.start-date').last().val(moment.utc(new Date()).format("MMMM D,YYYY"));
			}
		});

	}
	function RemoveRenter() {
		$(document).on('click', '.remove-renter', function () {
			$(this).parents('.fields').parent().remove();
		});
	}

	var MoveinTemplate = '<div class="movein">' + '<div class="fields" additional-fee-id="0">' +
		'<div class="field">' +
		'<label>Memo</label>' +
		'<input type="text" class="description" placeholder="Description" />' +
		'</div>' +
		'<div class="field">' +
		'<label>Amount</label>' +
		'<input type="text" onkeypress="return isNumberKey(this, event);" class="amount" placeholder="Amount" />' +
		'</div>' +
		'<div class="field">' +
		'<label>Due on</label>' +
		'<div class="ui calendar dateInputField">' +
		'<div class="ui input left icon">' +
		'<i class="calendar icon"></i>' +
		'<input type="text" class="due-on" placeholder="Due On" />' +
		'</div>' +
		'</div>' +
		'</div>' +
		'<div class="field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-movein-cost" data-tooltip="Add">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'<button type="button" class="circular ui icon orange mini button remove-movein-cost" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div>' +
		'</div>';

	function AddMoveinCost() {
		$(document).on('click', '.add-movein-cost', function () {
			$('#movein-div').append(MoveinTemplate);
			$('.dateInputField').calendar({
				type: 'date'
			});
		});
	}
	function RemoveMoveinCost() {
		$(document).on('click', '.remove-movein-cost', function () {
			$(this).parents('.fields').parent().remove();
			if ($('#movein-div').children().length == 0) {
				$('#movein-div').append(MoveinTemplate);
				$('.dateInputField').calendar({
					type: 'date'
				});
			}
		});
	}
	function RemoveRentAmount() {
		$(document).on('click', '.remove-rent', function () {
			$(this).parents('.fields').parent().remove();
		});
	}
	function RemovePostDated() {
		$(document).on('click', '.remove-post-dated', function () {
			$(this).parents('.fields').parent().remove();
			if ($('#post-dated-div').children().length == 0) {
				$('#post-dated-div').append(PostDatedTemplate);
				PostDatedStartChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
			}
		});
	}
	function AddUtilities() {
		$(document).on('click', '.add-utilities', function () {
			$('#utilities-div').append('<div class="utilities">' +
				'<div class="fields" additional-fee-id="0">' +
				'<div class="field">' +
				'<label>Description</label>' +
				'<input type="text" class="description" placeholder="Description" />' +
				'</div>' +
				'<div class="field">' +
				'<label>Amount</label>' +
				'<input type="text" onkeypress="return isNumberKey(this, event);"  class="amount" placeholder="Amount" />' +
				'</div>' +
				'<div class="field">' +
				'<label>Due on</label>' +
				'<div class="ui calendar dateInputField">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" class="due-on" placeholder="Due On" />' +
				'</div>' +
				'</div>' +
				'</div>' +
				'<div class="field">' +
				'<label>Frequency</label>' +
				'<select class="ui search dropdown frequency" name="frequency">' +
				//'<option value="">Select Frequency</option>' +
				//'<option value="1">Daily</option>' +
				//'<option value="2">Weekly</option>' +
				'<option value="3">Monthly</option>' +
				//'<option value="4">One time</option>' +
				'<option value="5">Yearly</option>' +
				'</select>' +
				'</div>' +
				'<div class="field">' +
				'<label>&nbsp;</label>' +
				'<button type="button" class="circular ui icon blue mini button add-utilities" data-tooltip="Add">' +
				'<i class="plus icon"></i>' +
				'</button>' +
				'<button type="button" class="circular ui icon orange mini button remove-utilities" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
				'</button>' +
				'</div>' +
				'</div>' +
				'</div>');
			$('.dateInputField').calendar({
				type: 'date'
			});
		});
	}
	function RemoveUtilities() {
		$(document).on('click', '.remove-utilities', function () {
			$(this).parents('.utilities').remove();
			if ($('#utilities-div').children().length == 0) {
				$('#utilities-div').append('<div class="utilities">' +
					'<div class="fields" additional-fee-id="0">' +
					'<div class="field">' +
					'<label>Description</label>' +
					'<input type="text" class="description" placeholder="Description" />' +
					'</div>' +
					'<div class="field">' +
					'<label>Amount</label>' +
					'<input type="text" onkeypress="return isNumberKey(this, event);" class="amount" placeholder="Amount" />' +
					'</div>' +
					'<div class="field">' +
					'<label>Due on</label>' +
					'<div class="ui calendar dateInputField">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" class="due-on" placeholder="Due On" />' +
					'</div>' +
					'</div>' +
					'</div>' +
					'<div class="field">' +
					'<label>Frequency</label>' +
					'<select class="ui search dropdown frequency" name="frequency">' +
					//'<option value="">Select Frequency</option>' +
					//'<option value="1">Daily</option>' +
					//'<option value="2">Weekly</option>' +
					'<option value="3">Monthly</option>' +
					//'<option value="4">One time</option>' +
					'<option value="5">Yearly</option>' +
					'</select>' +
					'</div>' +
					'<div class="field">' +
					'<label>&nbsp;</label>' +
					'<button type="button" class="circular ui icon blue mini button add-utilities" data-tooltip="Add">' +
					'<i class="plus icon"></i>' +
					'</button>' +
					'<button type="button" class="circular ui icon orange mini button remove-utilities" data-tooltip="Remove">' +
					'<i class="minus icon"></i>' +
					'</button>' +
					'</div>' +
					'</div>' +
					'</div>');
				$('.dateInputField').calendar({
					type: 'date'
				});
			}

		});
	}

	function BatchBalanceKeyup() {
		$(document).on('keyup', '.income-payment-amount', function () {

			var max = $(this).attr('max');
			var total = parseFloat($('#batch-payment-amount').val());
			var payment = 0;
			$('.income-payment-amount').each(function (index, element) {
				if ($(this).val() != null && $(this).val() != "") {
					payment = payment + parseFloat($(this).val())
				}
			});
			total = total - payment;
			if ((parseFloat($(this).val()) > parseFloat(max)) || total < 0) {
				ValidatePayment($(this), max);
				//ValidatePayment($(this), total);
				$('#batch-payment-remaining').text(total.toFixed(2));
			}
			else {
				ValidatePayment($(this), max);
				$('#batch-payment-remaining').text(total.toFixed(2));
			}
		});
	}
	function BatchPaymentKeyup() {
		$(document).on('keyup', '#batch-payment-amount', function () {
			var total = $('#batch-payment-amount').val();
			var payment = 0;
			var totalPaid = 0;
			$('.income-payment-amount').each(function (index, element) {
				if ($(this).val() != null && $(this).val() != "") {
					payment = payment + parseFloat($(this).val())

					totalPaid = totalPaid + parseFloat($(this).val());
					$(this).keyup();
				}
			});
			total = total - payment;
			ValidateTotalPayment($(this), totalPaid);
			$('#batch-payment-remaining').text(total.toFixed(2));

		});
	}
	function IncomeBalanceKeyup() {
		$(document).on('keyup', '#payment-amount', function () {
			if (parseFloat($('#payment-amount').val()) > parseFloat($('#EditDueIncomeAmount').val())) {
				ValidatePayment($('#payment-amount'), $('#EditDueIncomeAmount').attr('max'));
			}
			else {
				ValidatePayment($('#payment-amount'), $('#EditDueIncomeAmount').attr('max'));
			}
		});
	}

	function AddBatchPayment() {
		$(document).on('click', '#btnPayment', function () {

			$.ajax({
				type: "GET",
				url: '/Home/GetUnpaidTransaction',
				data: { bookingId: $("#BookingId").val() },
				dataType: "json",
				async: false,
				success: function (data) {
					$('#batch-payment-id').hide();
					$('#batch-proof-pic').val('');
					$('#batch-image-upload').html('');
					$('#batch-payment-description').val('');
					$('#batch-payment-amount').val('');
					$('#batch-payment-date').val('');
					$('#batch-id').val('0');
					$('#batch-payment-remaining').text('0');
					$('#unpaid-monthly-rent').html('');
					for (var x = 0; x < data.transactions.length; x++) {
						var amount = "";
						$('#unpaid-monthly-rent').append('<div class="fields monthly-rent"><div class="five wide field"><p class="income-id" income-id="' + data.transactions[x].Id + '">' + data.transactions[x].Description + " " + moment.utc(data.transactions[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + data.transactions[x].Amount + '</b></p></div>' +
							'<div class="six wide field">' +
							'<input type="text" onkeypress="return isNumberKey(this, event);"  min="0" max="' + data.transactions[x].Amount + '" payment-id="0" class="income-payment-amount" placeholder="Payment Amount" value="' + amount + '" />' +
							'</div></div>');
					}
					$('.batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
					$('.dateInputField').calendar({
						type: 'date'
					});
				}
			});

		});
	}
	function AddIncome() {
		$(document).on('click', '#btnRefund, #btnAddBill', function () {
			$('#income-description').val('');
			$('#income-amount').val('');
			$('#income-date').val('');
			$('.create-income-modal').modal({ closable: false }).modal('show').modal('refresh');
			var type = $(this).attr('income-type');
			if (type == 1) {
				$('#income-header').text("Add Bill");
			}
			else if (type == 5) {
				$('#income-header').text("Add Refund");
			}
			$('#income-type').val(type);
			$('.dateInputField').calendar({
				type: 'date'
			});
		});
	}
	function OnchangePaymentImage() {
		$(document).on('change', '#proof-pic', function (e) {
			$('#image-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#image-upload').append('<div class="ui small image"><img src="' + imageUrl + '"><div>');
			}
		});
	}
	function OnchangeGuestStatus() {
		$(document).on('change', '.guestStatus', function (e) {
			//It will check if there`s active renter
			var hasActiveRenter = false;
			$('.guestStatus').each(function (index, element) {
				if ($(this).val() == "true") {
					hasActiveRenter = true;
				}
			});
			if (hasActiveRenter == true) {
				var parent = $(this).closest('.fields');
				if ($(this).val() == "false") {
					parent.find('.end-date').attr('required', true);
				}
				else {
					parent.find('.end-date').removeAttr('required');
				}
			}
			else {
				alert("Required one active renter");
				$(this).val("true");
			}

		});
	}

	function OnchangeBatchPaymentImage() {
		$(document).on('change', '#batch-proof-pic', function (e) {
			$('#batch-image-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#batch-image-upload').append('<div class="ui small image"><img src="' + imageUrl + '"><div>');
			}
		});
	}
	function GetRequestBooking() {
		$.ajax({
			type: "GET",
			url: '/Property/GetRequestContract',
			data: { id: $("#request-id").val() },
			dataType: "json",
			async: false,
			success: function (data) {
				var renter = data.renters;
				var contract = data.contract;
				$("#CheckedInDate").val(contract.StartDate == null ? '' : moment.utc(contract.StartDate).format("MMMM D, YYYY"));
				$("#CheckedOutDate").val(contract.EndDate == null ? '' : moment.utc(contract.EndDate).format("MMMM D, YYYY")).change();
				$('#renter-div').html('');
				if (renter.length > 0) {
					for (var x = 0; x < renter.length; x++) {
						var contractRenter = '<div class="renter">' +
							'<div class="fields" renter-id="' + renter[x].Id + '">' +
							'<div class="three wide field">' +
							'<label>First Name</label>' +
							'<input type="text" class="guestFirstname" required placeholder="First Name" value="' + renter[x].Firstname + '" />' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Last Name</label>' +
							'<input type="text" class="guestLastname" required placeholder="Last Name" value="' + renter[x].Lastname + '" />' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Email</label>' +
							'<input type="text" class="guestEmail" placeholder="Email" value="' + renter[x].Email + '"/>' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Phone Number</label>' +
							'<input type="text" class="guestPhone" placeholder="Phone" value="' + renter[x].PhoneNumber + '"/>' +
							'</div>' +
							'<div class="five wide field">' +
							'<label>Address</label>' +
							'<input type="text" class="guestAddress" required placeholder="Address" value="' + renter[x].Address + '" />' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Start Date</label>' +
							'<div class="ui calendar renterDate">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="start-date" required placeholder="Start Date" value=""/>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>End Date</label>' +
							'<div class="ui calendar renterDate">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text"  class="end-date"  placeholder="End Date" value=""/>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="two wide field">' +
							'<label>Status</label>' +
							'<select class="ui search dropdown guestStatus">' +
							'<option value="true">Active</option>' +
							'<option value="false">Inactive</option>' +
							'</select>' +
							'</div>' +
							//(renter[x].IsActive ?
							//	'<div class="field">' +
							//	'<label>&nbsp;</label>' +
							//	'<button type="button" class="circular ui icon blue mini button add-renter" data-tooltip="Add">' +
							//	'<i class="plus icon"></i>' +
							//	'</button>' +
							//	'</button>' +
							//	(x != 0 ? '<button type="button" class="circular ui icon orange mini button remove-renter" data-tooltip="Remove">' +
							//		'<i class="minus icon"></i>' +
							//		'</button>' : '') +
							//	'</div>' : '') +
							'</div>' +
							'</div>';
						$('#renter-div').append(contractRenter);
					}
				}
				else {
					$('#renter-div').append(AddRenterTemplate);
				}
				//$('#renter-div :input').attr('disabled', true);
				//$('#pass-renter :input').attr('disabled', true);
				DateChangesRules();
			}
		});
	}

	function GetRenter() {
		$.ajax({
			type: "GET",
			url: '/Rentals/GetRenter',
			data: { bookingId: $("#BookingId").val() },
			dataType: "json",
			async: false,
			success: function (data) {
				var renter = data.renter;
				$('#renter-div').html('');
				if (renter.length > 0) {
					for (var x = 0; x < renter.length; x++) {
						var contractRenter = '<div class="renter">' +
							'<div class="fields" renter-id="' + renter[x].Id + '">' +
							'<div class="three wide field">' +
							'<label>First Name</label>' +
							'<input type="text" class="guestFirstname" required placeholder="Name" value="' + renter[x].Firstname + '" />' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Last Name</label>' +
							'<input type="text" class="guestLastname" required placeholder="Name" value="' + renter[x].Lastname + '" />' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Email</label>' +
							'<input type="text" class="guestEmail" placeholder="Email" value="' + renter[x].Email + '"/>' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Phone Number</label>' +
							'<input type="text" class="guestPhone" placeholder="Phone" value="' + renter[x].PhoneNumber + '"/>' +
							'</div>' +
							'<div class="five wide field">' +
							'<label>Address</label>' +
							'<input type="text" class="guestAddress" required placeholder="Address" value="' + renter[x].Address + '" />' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>Start Date</label>' +
							'<div class="ui calendar renterDate">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="start-date" required placeholder="Start Date" value="' + (renter[x].StartDate != null ? moment.utc(renter[x].StartDate).format('MMMM D,YYYY') : '') + '"/>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="three wide field">' +
							'<label>End Date</label>' +
							'<div class="ui calendar renterDate">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text"  class="end-date"  placeholder="End Date" value="' + (renter[x].EndDate != null ? moment.utc(renter[x].EndDate).format('MMMM D,YYYY') : '') + '"/>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="two wide field">' +
							'<label>Status</label>' +
							'<select class="ui search dropdown guestStatus">' +
							'<option value="true" ' + (renter[x].IsActive == true ? "selected" : '') + '>Active</option>' +
							'<option value="false" ' + (renter[x].IsActive == false ? "selected" : '') + '>Inactive</option>' +
							'</select>' +
							'</div>' +
							(renter[x].IsActive ?
								'<div class="field">' +
								'<label>&nbsp;</label>' +
								'<button type="button" class="circular ui icon blue mini button add-renter" data-tooltip="Add">' +
								'<i class="plus icon"></i>' +
								'</button>' +
								'</button>' +
								(x != 0 ? '<button type="button" class="circular ui icon orange mini button remove-renter" data-tooltip="Remove">' +
									'<i class="minus icon"></i>' +
									'</button>' : '') +
								'</div>' : '') +
							'</div>' +
							'</div>';
						if (renter[x].IsActive == true) {
							$('#renter-div').append(contractRenter);
						} else {
							$('#pass-renter').append(contractRenter);
						}
					}
				}
				else {
					$('#renter-div').append(AddRenterTemplate);
				}
				$('#renter-div :input').attr('disabled', true);
				$('#pass-renter :input').attr('disabled', true);

			}
		});
	}
	function GetRentApplied() {
		$.ajax({
			type: "GET",
			url: '/Rentals/GetRentApplied',
			data: { bookingId: $("#BookingId").val() },
			dataType: "json",
			async: true,
			success: function (data) {
				$('#rent-div').html('');
				var rents = data.rents;
				if (rents.length > 0) {

					for (var x = 0; x < rents.length; x++) {
						$('#rent-div').append('<div class="rent"><div class="fields" rent-id="' + rents[x].Id + '">' +
							'<div class="six wide field">' +
							'<label>Rent Amount</label>' +
							'<input type="text" onkeypress="return isNumberKey(this, event);" class="rent-amount" required placeholder="Rent Amount" value="' + rents[x].Amount + '" />' +
							'</div>' +
							'<div class="six wide field">' +
							'<label>Date Apply</label>' +
							'<div class="ui calendar RentDateApply">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="rent-date-apply" name="RentDateApply" required placeholder="Rent Date Apply" value="' + moment.utc(rents[x].EffectiveDate).format("MMMM YYYY") + '" />' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="field">' +
							'<label>&nbsp;</label>' +
							'<button type="button" class="circular ui icon blue mini button add-rent-amount" data-tooltip="Add">' +
							'<i class="plus icon"></i>' +
							'</button>' +
							'</button>' +
							(x != 0 ? '<button type="button" class="circular ui icon orange mini button remove-rent" data-tooltip="Remove">' +
								'<i class="minus icon"></i>' +
								'</button>' : '') +
							'</div>' +
							'</div></div>');
					}
					RentDateApplyChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
				}
				else {
					$('#rent-div').append(AddRentTemplate);
					RentDateApplyChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
				}
				$('#rent-div :input').attr('disabled', true);
			}
		});
	}
	function GetPostDated() {
		$.ajax({
			type: "GET",
			url: '/Rentals/GetPostDated',
			data: { bookingId: $("#BookingId").val() },
			dataType: "json",
			async: true,
			success: function (data) {
				$('#post-dated-div').html('');

				var postdated = data.postdated;

				if (postdated.length > 0) {

					for (var x = 0; x < postdated.length; x++) {
						$('#post-dated-div').append('<div class="post-date"><div class="fields" post-dated-id="' + postdated[x].Id + '">' +
							'<div class="six wide field">' +
							'<label>Post Dated Start</label>' +
							'<div class="ui calendar postDatedStartDate">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="post-date-start" name="PostPaymentStartDate" required placeholder="Post Dated Start" value="' + moment.utc(postdated[x].DateStart).format("MMMM YYYY") + '" />' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="six wide field">' +
							'<label>Post Dated Until</label>' +
							'<div class="ui calendar postDatedDate">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="post-date-end" name="PostPaymentEndDate" required placeholder="Post Dated Until"  value="' + moment.utc(postdated[x].DateEnd).format("MMMM YYYY") + '"/>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="six wide field">' +
							'<label>Post dated amount</label>' +
							'<input type="text" class="amount" name="PostDatedAmount" onkeypress="return isNumberKey(this, event);" required placeholder="Amount" value="' + postdated[x].Amount + '" />' +
							'</div>' +
							'<div class="field">' +
							'<label>&nbsp;</label>' +
							'<button type="button" class="circular ui icon blue mini button add-post-dated" data-tooltip="Add">' +
							'<i class="plus icon"></i>' +
							'</button>' +
							'</button>' +
							'<button type="button" class="circular ui icon orange mini button remove-post-dated" data-tooltip="Remove">' +
							'<i class="minus icon"></i>' +
							'</button>' +
							'</div>' +
							'</div></div>');
					}
					PostDatedStartChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
					$('.post-date').each(function (index, element) {
						PostDatedEndChange($(this).find('.postDatedStartDate'), $(this).find('.post-date-start').val(), $('#CheckedOutDate').val());
					});
				}
				else {
					$('#post-dated-div').append(PostDatedTemplate);
					PostDatedStartChange($('#RecurringRentStartDate').val(), $('#CheckedOutDate').val());
					$('.post-date').each(function (index, element) {
						PostDatedEndChange($(this).find('.postDatedStartDate'), $(this).find('.post-date-start').val(), $('#CheckedOutDate').val());
					});
				}
				$('#post-dated-div :input').attr('disabled', true);
			}
		});
	}
	function GetAdditionalBookingFee() {
		$.ajax({
			type: "GET",
			url: '/Rentals/GetAdditionalBookingFee',
			data: { bookingId: $("#BookingId").val() },
			dataType: "json",
			async: true,
			success: function (data) {
				var htmlUtilities = '';
				var htmlMovein = '';
				$('#movein-div').html('');
				$('#utilities-div').html('');
				var movein = data.movein;
				var utilities = data.utilities;
				if (movein.length > 0) {
					for (var x = 0; x < movein.length; x++) {
						htmlMovein += '<div class="movein">' + '<div class="fields" additional-fee-id="' + movein[x].Id + '">' +
							'<div class="field">' +
							'<label>Memo</label>' +
							'<input type="text" class="description" required placeholder="Description" value="' + movein[x].Description + '" />' +
							'</div>' +
							'<div class="field">' +
							'<label>Amount</label>' +
							'<input type="text" onkeypress="return isNumberKey(this, event);"  class="amount" required placeholder="Amount" value="' + (movein[x].Amount == null ? "" : movein[x].Amount) + '" />' +
							'</div>' +
							'<div class="field">' +
							'<label>Due on</label>' +
							'<div class="ui calendar dateInputField">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="due-on" required placeholder="Due On" value="' + moment.utc(movein[x].DueOn).format("MMMM D, YYYY") + '" />' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="field">' +
							'<label>&nbsp;</label>' +
							'<button type="button" class="circular ui icon blue mini button add-movein-cost" data-tooltip="Add">' +
							'<i class="plus icon"></i>' +
							'</button>' +
							'<button type="button" class="circular ui icon orange mini button remove-movein-cost" data-tooltip="Remove">' +
							'<i class="minus icon"></i>' +
							'</button>' +
							'</div>' +
							'</div>' +
							'</div>';

					}
					$('#movein-div').append(htmlMovein);
					$('.dateInputField').calendar({
						type: 'date'
					});
				}
				else {

					$('#movein-div').append(MoveinTemplate);
					$('.dateInputField').calendar({
						type: 'date'
					});
				}


				if (utilities.length > 0) {
					for (var x = 0; x < utilities.length; x++) {
						htmlUtilities += '<div class="utilities">' +
							'<div class="fields" additional-fee-id="' + utilities[x].Id + '">' +
							'<div class="field">' +
							'<label>Description</label>' +
							'<input type="text" class="description" required placeholder="Description" value="' + utilities[x].Description + '" />' +
							'</div>' +
							'<div class="field">' +
							'<label>Amount</label>' +
							'<input type="text" class="amount" onkeypress="return isNumberKey(this, event);" placeholder="Amount" value="' + (utilities[x].Amount == null ? "" : utilities[x].Amount) + '" />' +
							'</div>' +
							'<div class="field">' +
							'<label>Due on</label>' +
							'<div class="ui calendar dateInputField">' +
							'<div class="ui input left icon">' +
							'<i class="calendar icon"></i>' +
							'<input type="text" class="due-on" required placeholder="Due On"  value="' + moment.utc(utilities[x].DueOn).format("MMMM D, YYYY") + '"/>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'<div class="field">' +
							'<label>Frequency</label>' +
							'<select class="ui search dropdown frequency" name="frequency">' +
							//'<option value="">Select Frequency</option>' +
							//'<option value="1" ' + (utilities[x].Frequency == 1 ? 'selected' : '') + '>Daily</option>' +
							//'<option value="2" ' + (utilities[x].Frequency == 2 ? 'selected' : '') + '>Weekly</option>' +
							'<option value="3" ' + (utilities[x].Frequency == 3 ? 'selected' : '') + '>Monthly</option>' +
							//'<option value="4 ' + (utilities[x].Frequency == 4 ? 'selected' : '') + '">One time</option>' +
							'<option value="5" ' + (utilities[x].Frequency == 5 ? 'selected' : '') + '>Yearly</option>' +
							'</select>' +
							'</div>' +
							'<div class="field">' +
							'<label>&nbsp;</label>' +
							'<button type="button" class="circular ui icon blue mini button add-utilities" data-tooltip="Add">' +
							'<i class="plus icon"></i>' +
							'</button>' +
							'<button type="button" class="circular ui icon orange mini button remove-utilities" data-tooltip="Remove">' +
							'<i class="minus icon"></i>' +
							'</button>' +
							'</div>' +
							'</div>' +
							'</div>';
					}
					$('#utilities-div').append(htmlUtilities);

				}
				else {
					$('#utilities-div').append('<div class="utilities">' +
						'<div class="fields" additional-fee-id="0">' +
						'<div class="field">' +
						'<label>Description</label>' +
						'<input type="text" class="description" placeholder="Description" />' +
						'</div>' +
						'<div class="field">' +
						'<label>Amount</label>' +
						'<input type="text" onkeypress="return isNumberKey(this, event);" class="amount" placeholder="Amount" />' +
						'</div>' +
						'<div class="field">' +
						'<label>Due on</label>' +
						'<div class="ui calendar dateInputField">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" class="due-on" placeholder="Due On" />' +
						'</div>' +
						'</div>' +
						'</div>' +
						'<div class="field">' +
						'<label>Frequency</label>' +
						'<select class="ui search dropdown frequency" name="frequency">' +
						//'<option value="">Select Frequency</option>' +
						//'<option value="1">Daily</option>' +
						//'<option value="2">Weekly</option>' +
						'<option value="3">Monthly</option>' +
						//'<option value="4">One time</option>' +
						'<option value="5">Yearly</option>' +
						'</select>' +
						'</div>' +
						'<div class="field">' +
						'<label>&nbsp;</label>' +
						'<button type="button" class="circular ui icon blue mini button add-utilities" data-tooltip="Add">' +
						'<i class="plus icon"></i>' +
						'</button>' +
						'<button type="button" class="circular ui icon orange mini button remove-utilities" data-tooltip="Remove">' +
						'<i class="minus icon"></i>' +
						'</button>' +
						'</div>' +
						'</div>' +
						'</div>');
					$('.dateInputField').calendar({
						type: 'date'
					});
				}

				$('.dateInputField').calendar({
					type: 'date'
				});
				$('#movein-div :input').attr('disabled', true);
				$('#utilities-div :input').attr('disabled', true);
			}
		});
	}
	function GetBookingDetails() {
		$.ajax({
			type: "GET",
			url: '/Rentals/GetBookingDetails',
			data: { bookingId: $("#BookingId").val() },
			dataType: "json",
			success: function (data) {
				if (data.success) {
					//var guest = data.guest;
					var reservation = data.reservation;

					$('#LastPaymentMonth').val(reservation.LastPaymentMonth);
					$("#SecurityDepositAmount").val(reservation.SecurityDepositAmount);
					$("#SecurityDepositDueOn").val(reservation.SecurityDepositDueOn != null ? moment.utc(reservation.SecurityDepositDueOn).format("MMMM D, YYYY") : "");
					//$("#PostPaymentEndDate").val(reservation.PostPaymentEndDate != null ? moment.utc(reservation.PostPaymentEndDate).format("MMMM YYYY") : "");
					$("#isPostDatedPayment").prop("checked", reservation.IsPostDatedPayment).change();
					$("#isSecurityDeposit").prop("checked", reservation.IsSecurityDeposit).change();
					$("#isMonthToMonth").prop("checked", reservation.IsMonthToMonth).change();
					$("#isLateFees").prop("checked", reservation.IsLateFees).change();
					$("#LateFeeAmount").val(reservation.LateFeeAmount);
					$("#LateFeeApplyDays").val(reservation.LateFeeApplyDays);
					$("#RecurringRentStartDate").val(moment.utc(reservation.RecurringRentStartDate).format("MMMM D, YYYY"));
					//$("#RentAmount").val(reservation.RentAmount).change();
					//$("#guestEmail").val(data.email);
					//$("#guestName").val(guest.Name);
					$("#CheckedOutDate").val(reservation.CheckOutDate == null ? '' : moment.utc(reservation.CheckOutDate).format("MMMM D, YYYY"));
					$("#CheckedInDate").val(reservation.CheckInDate == null ? '' : moment.utc(reservation.CheckInDate).format("MMMM D, YYYY")).change();
					//$('#PostPaymentStartDate').val(reservation.PostPaymentStartDate != null ? moment.utc(reservation.PostPaymentStartDate).format("MMMM YYYY"):"");
					$('#AdvancePaymentMonth').val(reservation.AdvancePaymentMonth);
					$('#isAdvancePayment').prop("checked", reservation.IsAdvancePayment).change();
					$('#isFixedMonthToMonth').prop("checked", reservation.IsFixedMonthToMonth).change();
					$('#isLastMonthPayment').prop("checked", reservation.IsLastMonthPayment).change();

					//if (reservation.IsActive == false) {
					//	$('#rent-div :input').attr('disabled', true);
					//	$('#renter-div :input').attr('disabled', true);
					//	$('#pass-renter :input').attr('disabled', true);
					//	$('#post-dated-div :input').attr('disabled', true);
					//	$('#movein-div :input').attr('disabled', true);
					//	$('#utilities-div :input').attr('disabled', true);
					//	$("#SecurityDepositAmount").attr('disabled', true);
					//	$("#SecurityDepositDueOn").attr('disabled', true);
					//	$("#isPostDatedPayment").attr('disabled', true);
					//	$("#isSecurityDeposit").attr('disabled', true);
					//	$("#isMonthToMonth").attr('disabled', true);
					//	$("#isLateFees").attr('disabled', true);
					//	$("#LateFeeAmount").attr('disabled', true);
					//	$("#LateFeeApplyDays").attr('disabled', true);
					//	$("#RecurringRentStartDate").attr('disabled', true);
					//	$("#CheckedOutDate").attr('disabled', true);
					//	$("#CheckedInDate").attr('disabled', true);
					//	$('#AdvancePaymentMonth').attr('disabled', true);
					//	$('#isAdvancePayment').attr('disabled', true);
					//	$('#isFixedMonthToMonth').attr('disabled', true);
					//	$('#isLastMonthPayment').attr('disabled', true);
					//	$('#LastPaymentMonth').attr('disabled', true);
					//	$('#end-contract-btn').hide();
					//	$('#SaveAndConfirm').hide();

					//}
					//else {
					//	$('#rent-div :input').attr('disabled', false);
					//	$('#renter-div :input').attr('disabled', false);
					//	$('#pass-renter :input').attr('disabled', false);
					//	$('#post-dated-div :input').attr('disabled', false);
					//	$('#movein-div :input').attr('disabled', false);
					//	$('#utilities-div :input').attr('disabled', false);
					//	$("#SecurityDepositAmount").attr('disabled', false);
					//	$("#SecurityDepositDueOn").attr('disabled', false);
					//	$("#isPostDatedPayment").attr('disabled', false);
					//	$("#isSecurityDeposit").attr('disabled', false);
					//	$("#isMonthToMonth").attr('disabled', false);
					//	$("#isLateFees").attr('disabled', false);
					//	$("#LateFeeAmount").attr('disabled', false);
					//	$("#LateFeeApplyDays").attr('disabled', false);
					//	$("#RecurringRentStartDate").attr('disabled', false);
					//	$("#CheckedOutDate").attr('disabled', false);
					//	$("#CheckedInDate").attr('disabled', false);
					//	$('#AdvancePaymentMonth').attr('disabled', false);
					//	$('#isAdvancePayment').attr('disabled', false);
					//	$('#isFixedMonthToMonth').attr('disabled', false);
					//	$('#isLastMonthPayment').attr('disabled', false);
					//	$('#LastPaymentMonth').attr('disabled', false);
					//	$('#end-contract-btn').show();
					//	$('#SaveAndConfirm').show();
					//	$('#new-contract-btn').show();
					//}

					GetAdditionalBookingFee();
					GetRenter();
					GetPostDated();
					tableHistory.ajax.reload(null, false);
					GetRentApplied();
					GetPropertyPayment();
					DateChangesRules();

					//$('#rent-div :input').attr('disabled', true);
					//$('#renter-div :input').attr('disabled', true);
					//$('#pass-renter :input').attr('disabled', true);
					//$('#post-dated-div :input').attr('disabled', true);
					//$('#movein-div :input').attr('disabled', true);
					//$('#utilities-div :input').attr('disabled', true);
					$("#SecurityDepositAmount").attr('disabled', true);
					$("#SecurityDepositDueOn").attr('disabled', true);
					$("#isPostDatedPayment").attr('disabled', true);
					$("#isSecurityDeposit").attr('disabled', true);
					$("#isMonthToMonth").attr('disabled', true);
					$("#isLateFees").attr('disabled', true);
					$("#LateFeeAmount").attr('disabled', true);
					$("#LateFeeApplyDays").attr('disabled', true);
					$("#RecurringRentStartDate").attr('disabled', true);
					$("#CheckedOutDate").attr('disabled', true);
					$("#CheckedInDate").attr('disabled', true);
					$('#AdvancePaymentMonth').attr('disabled', true);
					$('#isAdvancePayment').attr('disabled', true);
					$('#isFixedMonthToMonth').attr('disabled', true);
					$('#isLastMonthPayment').attr('disabled', true);
					$('#LastPaymentMonth').attr('disabled', true);
					$('#end-contract-btn').show();
					$('#SaveAndConfirm').hide();
					$('#AllowEdit').show();

				}

			}
		});
	}
	function AllowEdit() {
		$(document).on('click', '#AllowEdit', function () {

			$('#rent-div :input').attr('disabled', false);
			$('#renter-div :input').attr('disabled', false);
			$('#pass-renter :input').attr('disabled', false);
			$('#post-dated-div :input').attr('disabled', false);
			$('#movein-div :input').attr('disabled', false);
			$('#utilities-div :input').attr('disabled', false);
			$("#SecurityDepositAmount").attr('disabled', false);
			$("#SecurityDepositDueOn").attr('disabled', false);
			$("#isPostDatedPayment").attr('disabled', false);
			$("#isSecurityDeposit").attr('disabled', false);
			$("#isMonthToMonth").attr('disabled', false);
			$("#isLateFees").attr('disabled', false);
			$("#LateFeeAmount").attr('disabled', false);
			$("#LateFeeApplyDays").attr('disabled', false);
			$("#RecurringRentStartDate").attr('disabled', false);
			$("#CheckedOutDate").attr('disabled', false);
			$("#CheckedInDate").attr('disabled', false);
			$('#AdvancePaymentMonth').attr('disabled', false);
			$('#isAdvancePayment').attr('disabled', false);
			$('#isFixedMonthToMonth').attr('disabled', false);
			$('#isLastMonthPayment').attr('disabled', false);
			$('#LastPaymentMonth').attr('disabled', false);
			$('#end-contract-btn').show();
			$('#SaveAndConfirm').show();
			$('#new-contract-btn').show();
			$('#AllowEdit').hide();
		});
	}
});



function isNumberKey(txt, evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode == 46) {
		//Check if the text already contains the . character
		if (txt.value.indexOf('.') === -1) {
			return true;
		} else {
			return false;
		}
	} else {
		if (charCode > 31 &&
			(charCode < 48 || charCode > 57))
			return false;
	}
	return true;
}

//remove because we will support income batch payment
//function RequestPayment(id) {
//	swal({
//		title: "You want to send payment request?",
//		confirmButtonColor: "##abdd54",
//		confirmButtonText: "Yes",
//		showCancelButton: true,
//		closeOnConfirm: true,
//		showLoaderOnConfirm: true
//	},
//		function () {
//			$.ajax({
//				type: "POST",
//				url: '/Payment/SendPaymentRequest',
//				data: { incomeId: id },
//				dataType: "json",
//				success: function (data) {
//					if (data.success) {
//						swal("Payment Request Sent!", "", "success");
//					}
//				}
//			});
//		});
//}

function CancelIncome(id) {

	swal({
		title: "Are you sure you want to cancel this income?",
		confirmButtonColor: "##abdd54",
		confirmButtonText: "Yes",
		showCancelButton: true,
		closeOnConfirm: true,
		showLoaderOnConfirm: true
	},
		function () {
			$.ajax({
				type: "POST",
				url: '/Home/CancelIncome',
				data: { id: id, "bookingId": $('#BookingId').val() },
				dataType: "json",
				success: function (data) {
					if (data.success) {
						GetPropertyPayment();
						swal("Income Cancelled!", "", "success");
					}
				}
			});
		});
}
function AllocatePayment(id) {
	$.ajax({
		type: "GET",
		url: '/Home/GetBatchPayment',
		data: { id: id },
		dataType: "json",
		success: function (data) {
			$('#batch-payment-id').text('Receipt Number: ' + data.batchpayment.Id);
			$('#batch-payment-id').show();
			$('#batch-id').val(data.batchpayment.Id);
			$('#batch-proof-pic').val('');
			$('#batch-payment-description').val(data.batchpayment.Description);
			$('#batch-payment-date').val(moment.utc(data.batchpayment.PaymentDate).format("MMMM D, YYYY"));
			$('#unpaid-monthly-rent').html('');
			$('#batch-image-upload').html('');
			var images = data.images;
			for (var x = 0; x < images.length; x++) {
				$('#batch-image-upload').append('<div class="ui small image"><a target="_blank" href="' + images[x] + '"><img src="' + images[x] + '"></a></div>');
			}
			var incomingpayment = data.incomingpayment;
			for (var x = 0; x < incomingpayment.length; x++) {
				$('#unpaid-monthly-rent').append('<div class="fields monthly-rent"><div class="five wide field"><p class="income-id" income-id="' + incomingpayment[x].IncomeId + '">' + incomingpayment[x].Description + " " + moment.utc(incomingpayment[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + incomingpayment[x].Balance + '</b></p></div>' +
					'<div class="six wide field">' +
					'<input type="text" onkeypress="return isNumberKey(this, event);"  min="0" max="' + incomingpayment[x].Balance + '"  payment-id="' + incomingpayment[x].IncomePaymentId + '" value="' + incomingpayment[x].Amount + '" class="income-payment-amount" required placeholder="Payment Amount " />' +
					'</div></div>');
			}
			for (var x = 0; x < data.monthlyrent.length; x++) {
				if (!$('[income-id="' + data.monthlyrent[x].Id + '"]').length > 0) {
					$('#unpaid-monthly-rent').append('<div class="fields monthly-rent"><div class="five wide field"><p class="income-id" income-id="' + data.monthlyrent[x].Id + '">' + data.monthlyrent[x].Description + " " + moment.utc(data.monthlyrent[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + data.monthlyrent[x].Amount + '</b></p></div>' +
						'<div class="six wide field">' +
						'<input type="text" onkeypress="return isNumberKey(this, event);"   min="0" max="' + data.monthlyrent[x].Balance + '" payment-id="0" class="income-payment-amount" required placeholder="Payment Amount " />' +
						'</div></div>');
				}
			}
			$('#batch-payment-amount').val(data.batchpayment.Amount).keyup();
			$('.batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
			$('.dateInputField').calendar({
				type: 'date'
			});
		}
	});
}
function EditIncome(id) {
	$.ajax({
		type: "GET",
		url: "/Income/Details",
		data: { id: id },
		success: function (data) {
			if (data.success) {
				//clearIncomeModalForm();
				$('#income-id').val(data.income.Id);
				//$('#PropertyId').dropdown('set selected', data.income.PropertyId);
				//$('#PropertyId').attr('disabled', true);
				//$('#IncomeTypeId').dropdown('set selected', data.income.IncomeTypeId);
				//$('#Status').dropdown('set selected', data.income.Status);
				$('#Description').val(data.income.Description);
				$('#Amount').val(data.income.Amount);
				$('#DueDate').val(moment.utc(data.income.DueDate).format('MMMM, D YYYY'));
				$('#income-modal').modal({ closable: false }).modal('show').modal('refresh');
				$('.ui.calendar.income').calendar({ type: 'date' });
			}
		}
	});
}

function AddPayment(id, type) {
	$.ajax({
		type: "POST",
		url: '/Home/GetIncomeDetail',
		data: { id: id },
		dataType: "json",
		success: function (data) {
			if (data.success) {
				//$('.payment-modal').
				$('.payment-modal').modal({ closable: false }).modal('show').modal('refresh');

				$('.dateInputField').calendar({
					type: 'date'
				});

				$('#payment-type').val(type);
				if (type == 1) {
					$('#payment-header').text('Payment');
				}
				else if (type == 2) {
					$('#payment-header').text('Credit');
				}
				$('#payment-id').val('0');
				$('#proof-pic').val('');
				var jsDates = moment.utc(data.message.DueDate).format("MMM D, YYYY");
				$("#hdnIncomeId").val(data.message.Id);
				if (data.message.Description.indexOf("Post Dated - ") > -1) {
					$("#payment-amount").val(data.message.Description.replace("Post Dated - ", ""));
				}
				else {
					$("#payment-amount").val('0');
				}
				$("#payment-date").val(jsDates);
				$('#payment-description').val('');
				$("#EditDueIncomeAmount").val(data.message.Amount);
				$("#EditDueIncomeAmount").attr('max', (data.message.Amount));
				$('#image-upload').html('');

			}
		}
	});
}
function ImageExist(url) {
	var result = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(url);
	return result;
}
function GetIncomePayment(id) {
	$.ajax({
		type: "GET",
		url: '/Home/GetIncomePayment',
		data: { id: id },
		dataType: "json",
		async: false,
		success: function (data) {
			if (data.success) {
				var payment = data.payment;
				var images = data.images;
				if (payment.Type == 1) {
					$('#payment-header').text('Payment');
				}
				else if (payment.Type == 2) {
					$('#payment-header').text('Credit');
				}
				$('#payment-id').val(payment.Id);

				$('#payment-description').val(payment.Description);
				$('#hdnIncomeId').val(payment.IncomeId);
				$('#payment-type').val(payment.Type);
				$("#payment-amount").val(payment.Amount);
				$('.payment-modal').modal({ closable: false }).modal('show').modal('refresh');
				$('.dateInputField').calendar({
					type: 'date'
				});
				var jsDates = moment.utc(payment.CreatedAt).format("MMM D, YYYY");
				$("#EditDueIncomeAmount").val(data.DueAmount);
				var max = (parseFloat(data.DueAmount) + parseFloat(payment.Amount));
				$("#EditDueIncomeAmount").attr('max', max);
				$("#payment-date").val(jsDates);
				$('#image-upload').html('');
				$('#proof-pic').val('');
				for (var x = 0; x < images.length; x++) {
					var image = images[x];
					$('#image-upload').append((ImageExist(image) ? '<div class="ui small image"><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div class="ui small image"><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
				}
			}
		}
	});
}

function GetHistory() {
	tableHistory = $('#history-table').DataTable({
		"aaSorting": [],
		"ajax": {
			"url": "/Rentals/GetHistory",
			"type": 'GET',
			"async": true,
			"data": function () {
				return {
					bookingId: $('#BookingId').val() == "" ? 0 : $('#BookingId').val()
				};
			}
		},
		"columns": [
			{ "data": "DateCreated" },
			{ "data": "Message" }
		]
	});
}
function GetPropertyPayment() {
	$.ajax({
		type: "GET",
		url: '/Home/GetPropertyPayment',
		data: { PropertyId: $("#PropertyId").val(), BookingId: $("#BookingId").val() },
		dataType: "json",
		async: true,
		success: function (data) {
			if (data.success) {
				var html = "";
				$.each(data.Income, function (key, value) {
					var jsDates = moment.utc(value.DueDate).format("MMM D, YYYY");
					html +=
						'<tr class="center aligned">' +
						'<td>' + jsDates + '</td>' +
						'<td>' + value.Description + '</td>' +
						'<td>' + (value.IncomeTypeId == 5 ? "-" : "") + value.Amount + '</td>' +
						'<td>' + value.PaymentDetails + '</td>' +
						'<td>' + (value.IncomeTypeId == 5 || value.IsActive == false ? '0' : '<p style="color:' + (value.IncomeTypeId == 6 || value.Status == "Over Paid" ? "#008000" : "#FF0000") + ';">' + value.Balance + '</p>') + '</td>' +
						'<td>' + value.RunningBalance + '</td>' +
						'<td>' + value.Status + '</td>' +
						'<td>' + (data.IsOwner ? '' : (value.IncomeTypeId == 6 ? '<button title="Allocate Payment" onclick="AllocatePayment(' + value.Id + ');" class="ui mini button"><i class="align edit icon"></i></button>' : (value.IsActive == true ?
							'<button title="Edit" onclick="EditIncome(' + value.Id + ');" class="ui mini button"><i class="edit outline icon"></i></button>' +
							//((value.IncomeTypeId != 5 && value.Amount != value.Payments) ? '<button title="Add Payments" onclick="AddPayment(' + value.Id + ',1);" class="ui mini button"><i class="align dollar sign icon"></i></button>' : '') +
							((value.IncomeTypeId == 1 && value.Amount != value.Payments) ? '<button title="Add Credit" onclick="AddPayment(' + value.Id + ',2);" class="ui mini button"><i class="align credit card outline icon"></i></button>' : '') +
							('<button title="Cancel" onclick="CancelIncome(' + value.Id + ');" class="ui mini button"><i class="align trash icon"></i></button>'
							/*+'<button title="Request" onclick="RequestPayment(' + value.Id + ');" class="ui mini button"><i class="align paypal icon"></i></button>'*/)
							: ''))) + '</td>' +
						'</tr>';
				});
				$('#booking-income-tbody').closest('table').DataTable().clear().destroy();
				$('#booking-income-tbody').html('');
				$('#booking-income-tbody').append(html);
				$('#booking-income-tbody').closest('table').DataTable({ "ordering": false });
			}
			else {
				toastr["error"]("Error or some validation issue is coming.").fadeOut("slow");
			}
		}
	});
}

function ValidateBatchPayment() {
	var result = false;
	var hasBlankField = false;
	var hasfirstFocus = false;
	var total = parseFloat($('#batch-payment-amount').val());
	var payment = 0;
	$('.income-payment-amount').each(function (index, element) {
		if ($(this).val() != null && $(this).val() != "") {
			payment = payment + parseFloat($(this).val())
		}
	});
	total = total - payment;
	$('.batch-payment-modal').find('[required]').each(function (index, element) {
		var obj = $(this);
		if (obj.val().length == 0) {
			obj.attr("style", "border-color:red");
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				obj.focus();
			}
			hasBlankField = true;
		} else {
			obj.removeAttr("style");
		}
	});
	result = (total != NaN && total >= 0 && hasBlankField == false) ? true : false;
	return result

}
function CreateBatchPayment() {
	var Payment = [];
	var isValid = false;
	$('.monthly-rent').each(function (index, element) {

		var amount = $(this).find('.income-payment-amount').val();
		var id = $(this).find('.income-id').attr('income-id');
		var paymentId = $(this).find('.income-payment-amount').attr('payment-id');
		//JM 10/09/19 Check if payment is greater than balance if not don`t save
		if (parseFloat($(this).find('.income-payment-amount').attr('max')) < parseFloat(amount)) {
			isValid = true;
		}
		var jsonPayment = {
			IncomeId: id,
			Amount: amount,
			PaymentId: paymentId
		};
		Payment.push(jsonPayment);

	});
	isValid = ValidateBatchPayment();
	if (isValid) {
		var formData = new FormData();
		for (var i = 0; i < $('#batch-proof-pic').get(0).files.length; ++i) {
			formData.append("Images[" + i + "]", $('#batch-proof-pic').get(0).files[i]);
		}
		formData.append("batchid", $('#batch-id').val());
		formData.append("bookingId", $('#BookingId').val());
		formData.append("description", $('#batch-payment-description').val());
		formData.append("paymentDate", $('#batch-payment-date').val());
		formData.append("amount", $('#batch-payment-amount').val());
		formData.append("payment", JSON.stringify(Payment));
		$.ajax({
			type: "POST",
			url: '/Home/CreateBatchPayment',
			contentType: false,
			beforeSend: function () {
				$('#saveBatchPayment').addClass('loading');
				$('#saveBatchPayment').attr('disabled', 'true');
			},
			cache: false,
			dataType: "json",
			processData: false,
			data: formData,
			success: function (data) {
				$('#saveBatchPayment').removeClass('loading');
				$('#saveBatchPayment').removeAttr('disabled');
				if (data.success) {
					$('.batch-payment-modal').modal('hide');
					swal("Payment created!", "", "success");
					GetPropertyPayment();
				}
			}
		});
	}


}
function ValidatePayment(obj, max) {
	if (parseFloat(obj.val()) > parseFloat(max)) {
		obj.next().remove();
		obj.after('<div class="ui basic red pointing prompt label transition visible">Payment must not greater than balance and a number</div>');
	} else {
		obj.next().remove();
	}

}
function ValidateTotalPayment(obj, max) {
	if (parseFloat(obj.val()) < parseFloat(max)) {
		obj.next().remove();
		obj.after('<div class="ui basic red pointing prompt label transition visible">Payment must greater than allocated amount</div>');
	} else {
		obj.next().remove();
	}

}
function ShowMessageDuplicateDates(obj, isDuplicate) {
	//remove error message when change
	obj.next().remove();
	//Add message if duplicate dates
	if (isDuplicate) {
		obj.after('<div class="ui basic red pointing prompt label transition visible">Invalid or Duplicate Dates</div>');
	}
}
function CreatePayment() {
	var hasBlankField = false;
	$('.payment-modal').find('[required]').each(function (index, element) {
		var obj = $(this);
		if (obj.val().length == 0) {
			obj.next().remove();
			obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				obj.focus();
			}
			hasBlankField = true;
		} else {
			obj.next().remove();
		}

	});

	if ($('#payment-id').val() == 0 || $('#payment-id').val() == "0") {
		if (parseFloat($('#payment-amount').val()) > parseFloat($('#EditDueIncomeAmount').val()) || hasBlankField) {
			ValidatePayment($('#payment-amount'), $('#EditDueIncomeAmount').val());
		}
		var model = new FormData();
		model.append("Id", $("#hdnIncomeId").val());
		model.append("Description", $("#payment-description").val());
		model.append("Amount", $("#payment-amount").val());
		model.append("DueAmount", $("#EditDueIncomeAmount").val());
		model.append("DueDate", $("#payment-date").val());
		model.append("IncomeTypeId", $("#payment-type").val());
		model.append("bookingId", $('#BookingId').val());
		for (var i = 0; i < $('#proof-pic').get(0).files.length; ++i) {
			model.append("Images[" + i + "]", $('#proof-pic').get(0).files[i]);
		}
		$.ajax({
			type: "POST",
			url: '/Home/CreatePayment',
			data: model,
			dataType: "json",
			cache: false,
			contentType: false,
			beforeSend: function (arr, $form, options) {
				$('#savePayment').addClass('loading');
				$('#savePayment').attr('disabled', 'true');
			},
			processData: false,
			success: function (data) {
				$('#savePayment').removeClass('loading');
				$('#savePayment').removeAttr('disabled');
				if (data.success) {
					$('#payment-id').val('0');
					$("#payment-amount").val('');
					$("#EditDueIncomeAmount").val('');
					$("#payment-date").val('');
					$("#payment-type").val('');
					$('#proof-pic').val('');
					$('#image-upload').html('');
					GetPropertyPayment();
					$('.payment-modal').modal('hide');
					swal("Payment created!", "", "success");
				}
			}
		});
	}
	else {
		var model = new FormData();
		model.append('Id', $('#payment-id').val());
		model.append('IncomeId', $("#hdnIncomeId").val());
		model.append("Description", $("#payment-description").val());
		model.append("Amount", $("#payment-amount").val());
		model.append("CreatedAt", $("#payment-date").val());
		model.append("Type", $("#payment-type").val());
		model.append("bookingId", $('#BookingId').val());
		for (var i = 0; i < $('#proof-pic').get(0).files.length; ++i) {
			model.append("Images[" + i + "]", $('#proof-pic').get(0).files[i]);
		}
		$.ajax({
			type: "POST",
			url: '/Home/EditPayment',
			data: model,
			dataType: "json",
			beforeSend: function (arr, $form, options) {
				$('#savePayment').addClass('loading');
				$('#savePayment').attr('disabled', 'true');
			},
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				$('#savePayment').removeClass('loading');
				$('#savePayment').removeAttr('disabled');
				if (data.success) {
					$('#payment-id').val('0');
					$("#payment-amount").val('');
					$("#EditDueIncomeAmount").val('');
					$("#payment-date").val('');
					$("#payment-type").val('');
					$('#proof-pic').val('');
					$('#image-upload').html('');

					toastr["success"]("row succesfully updated").fadeOut("slow");
					GetPropertyPayment();
					$('.payment-modal').modal('hide');
				}
			}
		});
	}
}

function Add() {
	if ($("#AdditionalMemos").val() == "") {
		toastr["error"]("please enter the AdditionalMemo field.").fadeOut("slow");
		return false;
	}
	if ($("#AdditionalAmounts").val() == "") {
		toastr["error"]("please enter the AdditionalAmount field.").fadeOut("slow");
		return false;
	}
	if ($("#DueOns").val() == "") {
		toastr["error"]("please enter the DueOn field.").fadeOut("slow");
		return false;
	}
	AddRowResidence($("#AdditionalMemos").val(), $("#AdditionalAmounts").val(), $("#DueOns").val());
	$("#AdditionalMemos").val("");
	$("#AdditionalAmounts").val("");
	$("#DueOns").val("");
};

function AddRowResidence(AdditionalMemos, AdditionalAmounts, DueOns) {
	//Get the reference of the Table's TBODY element.
	var tBody = $("#tblResidenceHistory > TBODY")[0];
	//Add Row.
	row = tBody.insertRow(-1);
	//Add CategoryName cell.
	var cell = $(row.insertCell(-1));
	cell.html('<span name="AdditionalMemo" id="AdditionalMemo">' + AdditionalMemos + '</span>');
	//Add SubCategoryName cell.
	cell = $(row.insertCell(-1));
	cell.html('<span name="AdditionalAmount" id="AdditionalAmount">' + AdditionalAmounts + '</span>');
	//Add ControlType cell.
	cell = $(row.insertCell(-1));
	cell.html('<span name="DueOn" id="DueOn">' + DueOns + '</span>');
	//Add Button cell.
	cell = $(row.insertCell(-1));
	var btnRemove = $("<input />");
	btnRemove.attr("type", "button");
	btnRemove.attr("onclick", "RemoveResidence(this);");
	btnRemove.attr("style", "background-color: #f2711c; color: #fff; text-shadow: none; background-image: none;box-shadow: 0 0 0 0 rgba(34,36,38,.15) inset;    cursor: pointer;display: inline-block;min-height: 1em;outline: 0;border: none;vertical-align: baseline;font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;margin: 0 .25em 0 0;padding: .78571429em 1.5em .78571429em;text-transform: none;text-shadow: none;font-weight: 700;line-height: 1em;font-style: normal;text-align: center;text-decoration: none;border-radius: .28571429rem;");
	btnRemove.val("Remove Residence");
	cell.append(btnRemove);
};

function RemoveResidence(button) {
	//Determine the reference of the Row using the Button.
	var row = $(button).closest("TR");
	var name = $("TD", row).eq(0).html();
	if (confirm("Do you want to delete: " + name)) {
		//Get the reference of the Table.
		var table = $("#tblResidenceHistory")[0];
		//Delete the Table row using it's Index.
		table.deleteRow(row[0].rowIndex);
	}
};

function Edit() {
	$("#booking-form").show();
	$("#confirm-form").hide();
}

function EndContract() {
	$('#end-contract-modal').modal({
		onApprove: function (e) {
			$.ajax({
				type: "POST",
				url: '/Home/EndContract',
				data: { bookingId: $("#BookingId").val(), endDate: $("#end-contract-date").val() },
				dataType: "json",
				success: function (data) {
					swal("Contract End!", "", "success");
					$('#rent-div :input').attr('disabled', true);
					$('#renter-div :input').attr('disabled', true);
					$('#pass-renter :input').attr('disabled', true);
					$('#post-dated-div :input').attr('disabled', true);
					$('#movein-div :input').attr('disabled', true);
					$('#utilities-div :input').attr('disabled', true);
					$("#SecurityDepositAmount").attr('disabled', true);
					$("#SecurityDepositDueOn").attr('disabled', true);
					$("#isPostDatedPayment").attr('disabled', true);
					$("#isSecurityDeposit").attr('disabled', true);
					$("#isMonthToMonth").attr('disabled', true);
					$("#isLateFees").attr('disabled', true);
					$("#LateFeeAmount").attr('disabled', true);
					$("#LateFeeApplyDays").attr('disabled', true);
					$("#RecurringRentStartDate").attr('disabled', true);
					$("#CheckedOutDate").attr('disabled', true);
					$("#CheckedOutDate").val($("#end-contract-date").val());
					$("#CheckedInDate").attr('disabled', true);
					$('#AdvancePaymentMonth').attr('disabled', true);
					$('#isAdvancePayment').attr('disabled', true);
					$('#isFixedMonthToMonth').attr('disabled', true);
					$('#isLastMonthPayment').attr('disabled', true);
					$('#LastPaymentMonth').attr('disabled', true);
					$('#end-contract-btn').hide();
					$('#SaveAndConfirm').hide();
					GetPropertyPayment();

				}
			});
		}
	})
		.modal('show')
		.modal('refresh');
	$('.endContractDate').calendar({
		type: 'date',
	});
}
function EditNow() {
	$("#booking-form").show();
	$("#confirm-form").hide();
	$('.menu .item[data-tab="third"]').removeClass("active");
	$('.menu .item[data-tab="first"]').addClass("active");
	$('.menu .item[data-tab="first"]').click();
}
//function GetReservations() {
//	$.ajax({
//		type: "GET",
//		url: '/Home/GetReservations',
//		data: { inboxId: $("#InboxId").val() },
//		dataType: "json",
//		async: false,
//		success: function (data) {
//			var reservations = data.reservations;
//			if (reservations.length >0) {
//				//$('#contract-select-div').removeAttr('hidden');
//				$('#contract-select').html('');
//				for (var x = 0; x < reservations.length; x++) {
//					$('#contract-select').append('<option value=' + reservations[x].Id + '>' + moment.utc(reservations[x].CheckInDate).format("MMM D") + ' - ' + moment.utc(reservations[x].CheckOutDate).format("MMM D, YYYY") + '</option>');
//				}
//			}
//			$("#BookingId").val(reservations[0].Id);
//			$('#contract-select').val(reservations[0].Id).change();
//		}
//	});
//}

function ValidateRequiredFields() {
	var hasBlankField = false;
	var hasfirstFocus = false;
	$('#booking-form').find('[required]').each(function (index, element) {
		var obj = $(this);
		if (obj.val().length == 0) {
			obj.next().remove();
			obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			obj.attr("style", "border-color:red");
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				obj.focus();
			}
			hasBlankField = true;
		} else {
			obj.next().remove();
			obj.removeAttr("style");
		}

	});
	return hasBlankField;
}

$(document).on('keyup,change', '[required]', function () {

	if ($(this).val().length == 0) {
		$(this).next().remove();
		$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
	}
	else {
		$(this).next().remove();
	}
});

function formatDatesss(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	return year + '-' + (month <= 9 ? '0' + month : month) + '-' + (day <= 9 ? '0' + day : day);
}

function AddOfflinePayment() {
	$('.Add-OffPayment-modal').modal({ closable: false }).modal('show').modal('refresh');
	$('.dateInputField').calendar({
		type: 'date'
	});
}

function AddCredit(id) {
	$('.Add-Credit-modal').modal({ closable: false }).modal('show').modal('refresh');
	$('#income-credit-id').val(id);
}



function SaveIncome() {
	var model = new FormData();
	model.append("PropertyId", $("#PropertyId").val());
	model.append("BookingId", $("#BookingId").val());
	model.append("Description", $("#income-description").val());
	model.append("IncomeTypeId", $('#income-type').val());
	model.append("Amount", $("#income-amount").val());
	model.append("DueDate", $("#income-date").val());

	$.ajax({
		type: "POST",
		url: '/Home/SaveIncome',
		data: model,
		dataType: "json",
		beforeSend: function () {
			$('#btnBillSave').addClass('loading');
			$('#btnBillSave').attr('disabled','true');
		},
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) {
			$('#btnBillSave').removeClass('loading');
			$('#btnBillSave').removeAttr('disabled');
			if (data.success) {
				GetPropertyPayment();
				$("#income-description").val('');
				$('#income-type').val('');
				$("#income-amount").val('');
				$("#income-date").val('');
				$('.create-income-modal').modal('hide');
			}
		}
	});
}

function SaveCredit() {
	var amount = $("#CreditAmount").val();
	var desc = $('#CreditDescription').val();
	var id = $('#income-credit-id').val();
	$('#btnCreditSave').addClass('loading');
	$.ajax({
		type: "POST",
		url: '/Home/SaveCredit',
		data: { amount: amount, id: id, desc: desc },
		success: function (data) {
			$('#btnCreditSave').removeClass('loading');

			if (data.success) {
				GetPropertyPayment();
				$('.Add-Credit-modal').modal('hide');
			}
		}
	});
}

function SaveOfflinePayment() {

	var model = new FormData();
	model.append("PropertyId", $("#PropertyId").val());
	model.append("BookingId", $("#BookingId").val());
	model.append("Description", $("#OffPaymentDescription").val());
	model.append("IncomeTypeId", 2);
	model.append("Amount", $("#OffPaymentAmount").val());
	model.append("PaymentDate", $("#OffPaymentDueOn").val());

	$.ajax({
		type: "POST",
		url: '/Home/SaveIncome',
		data: model,
		dataType: "json",
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) {
			if (data.success) {
				GetPropertyPayment();
				$('.Add-OffPayment-modal').modal('hide');
			}
			else {
				toastr["error"]("Error or some validation issue is coming.").fadeOut("slow");
			}
		}
	});
}
