﻿// Defining a connection to the server hub.
var myHub = $.connection.inboxHub;

// This is the client method which is being called inside the MyHub constructor method every 3 seconds
myHub.client.NewMessages = function (message) {
    // Set the received serverTime in the span to show in browser
    $("#test").html(message);
};

// Setting logging to true so that we can see whats happening in the browser console log. [OPTIONAL]
$.connection.hub.logging = true;
// Start the hub
$.connection.hub.start();