﻿// initialization scripts

$(document).ready(function () {
    var tableIncome;
    var tableIncomeType;
    initialize();

    function initialize() {
        setupSemanticUI();
        setEventListeners();
        LoadIncomeTable();
        ReceivableSummary();
        LoadIncomeTypeTable();
        RemoveFile();
    }


    function setupSemanticUI() {
        $('.ui.accordion').accordion();
        $('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
        $('.ui.checkbox').checkbox();
        $('.ui.calendar').calendar({ type: 'date' });
        $('.ui.calendar.month').calendar({ type: 'month' });
        $('.ui.calendar.time').calendar({ type: 'time' });
        $('table td a.table-tooltip').popup();
    }

    function setEventListeners() {
        onAddIncome();
        onEditIncome();
        onIncomeSubmit();
        onDeleteIncome();
        onFilterButtonClick();
		onGenerateIncomeClick();
		onIncomePayment();
		onSubmitIncomePayment();
        deletePayment();
        paymentStatusChange();
        SaveIncomeType();
        GetIncomeTypeDetails();
        CreateIncomeTypeBtn();
        DeleteIncomeType();
        GetIncomeType();
        OnchangeIncomeImage();
        OnchangeIncomePaymentImage();
    }
    function OnchangeIncomeImage() {
        $(document).on('change', '#income-proof-files', function (e) {
            $('#income-proof-files-upload').empty();
            for (var i = 0; i < e.target.files.length; i++) {
                var fileUrl = window.URL.createObjectURL(this.files[i]);
                var type = this.files[i]['type'].split('/')[0];
                if (type == 'image') {
                    $('#income-proof-files-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><img src="' + fileUrl + '"></div>');
                }
                else {
                    $('#income-proof-files-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"><a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a></div>');
                }
            }
        });
    }
    function OnchangeIncomePaymentImage() {
        $(document).on('change', '#income-payment-proof-files', function (e) {
            $('#income-payment-proof-files-upload').empty();
            for (var i = 0; i < e.target.files.length; i++) {
                var fileUrl = window.URL.createObjectURL(this.files[i]);
                var type = this.files[i]['type'].split('/')[0];
                if (type == 'image') {
                    $('#income-payment-proof-files-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><img src="' + fileUrl + '"></div>');
                }
                else {
                    $('#income-payment-proof-files-upload').append('<div file-id="0" class="ui small image"><a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a></div>');
                }
            }
        });
    }
	function onIncomePayment() {
		$(document).on('click', '.add-income-payment-btn', function (e) {
			$('.payment-table tbody').empty();
			$('.payment-table').hide();

			$('#payment-income-id').val('');
			$('#income-payment-amount').val('');
			$('#income-payment-description').val('');
            $('#income-payment-date').val('');
            $('#income-payment-proof-files').val('');
            $('#income-payment-proof-files-upload').empty();
			var id = $(this).parents('tr').attr('data-income-id');
			$('#payment-income-id').val(id);
			$('.income-table').show();
			$.ajax({
				type: 'POST',
				url: "Income/GetIncomeDetails",
				data: {
					Id: id
                },
                async: false,
				success: function (data) {
					var totalPaid = 0;
					var total = 0;

					if (data.income.length != 0) {
						$('.payment-table').show();
                        $.each(data.income_paid, function (index, value) {
                            $('.payment-table tbody').append('<tr payment-id="' + value.Id + '"><td>' + value.Description + '</td><td>' + value.Amount + '</td><td>' + (value.IsRejected ? "Rejected" : "Accepted") + '</td><td>' + moment(value.CreatedAt).format('MMMM, D YYYY') + '</td><td><a class=\"ui mini button delete-payment-btn\" title=\"Delete Payment\"><i class=\"trash icon\"></i></a><a status=' + (value.IsRejected ? "false" : "true") + ' class=\"ui mini button status-payment-btn\" title=\"' + (value.IsRejected ? "Accepted" : "Rejected") + '\"><i class=\"' + (value.IsRejected ? "ui icon check square outline" : "ui icon window close outline") + '\"></i></a></td></tr>'),
                                totalPaid += (value.IsRejected?0:value.Amount);
						});
						$('.income-amount').text(data.income.Amount);
						total = data.income.Amount - totalPaid;
						$('.remaining-balance').text(total);
					}
				}
			});

            $('.income-payment-modal').modal('show');
			$('.dateInputField').calendar({
                type: 'date'
            });
            $('.income-payment-modal').modal('refresh');
		});
	}
	function onSubmitIncomePayment() {
		$('#income-payment-submit-btn').on('click', function () {
            var formData = new FormData();
            for (var i = 0; i < $('#income-payment-proof-files').get(0).files.length; ++i) {
                formData.append("Images[" + i + "]", $('#income-payment-proof-files').get(0).files[i]);
            }
			formData.append("IncomeId", $('#payment-income-id').val());
			formData.append("Description", $('#income-payment-description').val());
			formData.append("Amount", $('#income-payment-amount').val());
			formData.append("CreatedAt", $('#income-payment-date').val());
			$.ajax({
				type: "POST",
				url: '/Income/AddPayment',
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						tableIncome.ajax.reload(null, false);
						$('.income-payment-modal').modal('hide');
						swal("Payment created!", "", "success");
					}
				}
			});
		});
	}
	function deletePayment() {
		$(document).on('click', '.delete-payment-btn', function (e) {
			var id = $(this).parents('tr').attr('payment-id');
			$.ajax({
				type: 'POST',
				url: "Income/DeletePayment",
				data: {
					Id: id
				},
				success: function (data) {
					if (data.success) {
						tableIncome.ajax.reload(null, false);
						$('.income-payment-modal').modal('hide');
						swal({ title: "Success", text: "Payment successfully deleted.", type: "success" });
					}
				}
			});
		});
    }
    function paymentStatusChange() {
        $(document).on('click', '.status-payment-btn', function (e) {
            var id = $(this).parents('tr').attr('payment-id');
            var status = $(this).attr('status');
            $.ajax({
                type: 'POST',
                url: "Income/UpdatePaymentStatus",
                data: {
                    Id: id,
                    status: status
                },
                success: function (data) {
                    if (data.success) {
                        tableIncome.ajax.reload(null, false);
                        $('.income-payment-modal').modal('hide');
                        swal({ title: "Success", text: (status == "true" ?"Payment successfully rejected.": "Payment successfully accepted."), type: "success" });
                    }
                }
            });
        });
    }
    function onGenerateIncomeClick() {
        $('#generate-income-btn').on('click', function () {
            if ($('#month-year-filter').val() != '' && $('#property-filter').val() != '')
            {
                $.ajax({
                    type: "POST",
                    url: "/Income/Generate",
                    data: {
                        propertyFilter: $('#property-filter').val(),
                        monthYearFilter: $('#month-year-filter').val()
                    },

                    beforeSend: function () {
                        $('#generate-income-btn').addClass('loading');
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({ title: "Success", text: "Successfully generated income.", type: "success" },
                            function () {
                                $('#month-year-filter').val('');
                                RefreshIncomeList();
                            });
                        }
                        else {
                            swal("Error in generate income", "", "error");
                        }
                        $('#generate-income-btn').removeClass('loading');
                    }
                });
            }
            else
            {
                swal("Please fill all fields", "", "error");
            }
        });
    }

    function onFilterButtonClick() {
        $('#filter-btn').on('click', function () {
            RefreshIncomeList();
        });
    }

    function onPropertyFilterChange() {
        $("#property-filter").on('change', function () {
            RefreshIncomeList();
        });
    }

    function onYearMonthFilterChange() {
        $("#month-year-filter").on('change', function () {
            RefreshIncomeList();
        });
    }

    function GetIncomeType() {
        $.ajax({
            type: 'GET',
            url: "Income/GetIncomeType",
            success: function (data) {
                var incomeTypes = data.incomeTypes;
                $('#IncomeTypeId').empty();
                var optionData = '<option value="">Select Income Type</option>';
                for (var i = 0; i < incomeTypes.length; i++) {
                    optionData += '<option value="' + incomeTypes[i].Id + '">' + incomeTypes[i].TypeName + '</option>';
                }
                $('#IncomeTypeId').append(optionData);
            }
        });
       
    }

    function clearIncomeModalForm() {
        $('#PropertyId').dropdown('clear');
        $('#IncomeTypeId').dropdown('clear');
        $('#Status').dropdown('clear');
        $('#Description').val('');
        $('#Amount').val('');
        $('#PaymentDate').val('');
        $('#income-id').val('');
        $('#income-proof-files-upload').empty();
        $('#income-proof-files').val();
    }
    
    function onAddIncome() {
        $('#js-btn_create-new-income').on('click', function () {
            clearIncomeModalForm();
            $('#income-modal').modal('show');
            $('.ui.calendar.income').calendar({ type: 'date' });
        });
	}
	$(document).on('keyup', '[required]', function () {

		if ($(this).val().length == 0) {
			$(this).next().remove();
			$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).next().remove();
		}
	}); $(document).on('change', '#Status, #IncomeTypeId, #PropertyId', function () {

		if ($(this).val().length == 0) {
			$(this).parent().next().remove();
			$(this).parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).parent().next().remove();
		}
	});
	function ValidateRequiredFields() {
		var hasBlankField = false;
		var hasfirstFocus = false;
		property = $('#PropertyId');
		if (property.val() == "" || property.val() == null) {
			property.parent().next().remove();
			property.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				property.focus();
			}
			hasBlankField = true;
		}
		else {
			property.parent().next().remove();
		}
		var incomeType = $('#IncomeTypeId');
		if (incomeType.val() == "" || incomeType.val() == null) {
			incomeType.parent().next().remove();
			incomeType.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				incomeType.focus();
			}
			hasBlankField = true;
		}
		else {
			incomeType.parent().next().remove();
		}
		var status = $('#Status');
		if (status.val() == "" || status.val() == null) {
			status.parent().next().remove();
			status.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				status.focus();
			}
			hasBlankField = true;
		}
		else {
			status.parent().next().remove();
		}
		$('#income-modal').find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.next().remove();
				obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.next().remove();
			}

		});
		return hasBlankField;
    }
    function RemoveFile() {
        $(document).on('click', '#income-proof-files-upload .icon.close', function () {
            $(this).parent().remove();
            //const input = document.getElementById('income-proof-files')
            //// as an array, u have more freedom to transform the file list using array functions.
            //const fileListArr = Array.from(input.files)
            //fileListArr.splice(fileListArr[1], 1)
            //console.log(fileListArr)


        });
        $(document).on('click', '#income-payment-proof-files-upload .icon.close', function () {
            $(this).parent().remove();
        });
    }
    function onIncomeSubmit() {
        $('#income-save-btn').on('click', function () {
            var id = $('#income-id').val();
            var propertyId = $('#PropertyId').val();
            var incomeTypeId = $('#IncomeTypeId').val();
            var description = $('#Description').val();
            var amount = $('#Amount').val();
            var dueDate = $('#DueDate').val();
			var status = $('#Status').val();
			var hasBlankField = ValidateRequiredFields();
			if (hasBlankField) {
				return;
            }
            var formData = new FormData();
            for (var i = 0; i < $('#income-proof-files').get(0).files.length; ++i) {
                formData.append("Images[" + i + "]", $('#income-proof-files').get(0).files[i]);
            }
            formData.append("Id", id);
            formData.append("IncomeTypeId", incomeTypeId);
            formData.append("BookingId", 0);
            formData.append("PropertyId", propertyId);
            formData.append("Description", description);
            formData.append("Amount", amount);
            formData.append("Status", status);
            formData.append("DueDate", dueDate);

            if (id == '') // Add
			{
				if (propertyId != '' && incomeTypeId != '' && description != '' && amount != '' && dueDate != '' && status != '') {
                    $.ajax({
                        type: "POST",
                        url: "/Income/Create",
                        contentType: false,
                        cache: false,
                        dataType: "json",
                        processData: false,
                        data: formData,
                        success: function (data) {
                            if (data.success) {
                                $('#income-modal').modal('hide');
                                swal({ title: "Success", text: "Income has been saved.", type: "success" },
                                function () {
                                    RefreshIncomeList();
                                    clearIncomeModalForm();
                                });
                            }
                            else {
                                $('#income-modal').modal('hide');
                                swal("Error in saving new income", "", "error");
                            }
                        }
                    });
                }
                else {
                    swal("Please fill all fields", "", "error");
                }
            }
            else // Edit
            {
                var fileIds = [];
                $('#income-proof-files-upload').find('[file-id]').each(function (index, element) {
                    fileIds.push($(this).attr('file-id'));
                });
                if (fileIds.length == 0) {
                    fileIds.push(0);
                }
                formData.append("fileIds", JSON.stringify(fileIds));
                if (id != '')
                {
                    $.ajax({
                        type: "POST",
                        url: "/Income/Edit",
                        contentType: false,
                        cache: false,
                        dataType: "json",
                        processData: false,
                        data: formData,
                        success: function (data) {
                            if (data.success) {
                                $('#income-modal').modal('hide');
                                swal({ title: "Success", text: "Income has been saved.", type: "success" },
                                function () {
                                    RefreshIncomeList();
                                    clearIncomeModalForm();
                                });
                            }
                            else
                            {
                                $('#income-modal').modal('hide');
                                swal("Error on edit income", "", "error");
                            }
                        }
                    });
                }
            }
        });
    }
    function ImageExist(url) {
        var result = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(url);
        return result;
    }

    function onEditIncome() {
        $(document).on('click', '.edit-income-btn', function () {
            var id = $(this).parents('tr').attr("data-income-id");
            $.ajax({
                type: "GET",
                url: "/Income/Details",
                data: { id : id },
                success: function (data) {
                    if (data.success) {
                        clearIncomeModalForm();
                        $('#income-id').val(data.income.Id);
                        $('#PropertyId').dropdown('set selected', data.income.PropertyId);
                        $('#IncomeTypeId').dropdown('set selected', data.income.IncomeTypeId);
                        $('#Status').dropdown('set selected', data.income.Status);
                        $('#Description').val(data.income.Description);
                        $('#Amount').val(data.income.Amount);
                        $('#DueDate').val(moment(data.income.DueDate).format('MMMM, D YYYY'));
                        $('#income-proof-files-upload').empty();
                        $('#income-proof-files').val('');
                        var files = data.Files;
                        for (var x = 0; x < files.length; x++) {
                            var image = files[x].FileUrl;
                            $('#income-proof-files-upload').append((ImageExist(image) ? '<div file-id="' + files[x].Id + '" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div file-id="' + files[x].Id +'" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
                        }
                        $('#income-modal').modal('show');
                        $('.ui.calendar.income').calendar({ type: 'date' });
                    }
                }
            });
        });
    }

    function onDeleteIncome() {
        $(document).on('click', '.delete-income-btn', function () {
            var id = $(this).parents('tr').attr('data-income-id');
            if (id != '')
            {
                swal({
                    title: "Are you sure you want to delete this income ?",
                    text: "",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: "/Income/Delete",
                        data: { id: id },
                        success: function (data) {
                            if (data.success) {
                                swal({ title: "Success", text: "Successfully deleted income.", type: "success" },
                                function () {
                                    RefreshIncomeList();
                                });
                            }
                            else {
                                swal("Error occured on deleting income", "", "error");
                            }
                        }
                    });
                });
            }
        });
    }

	$('#property-filter').on('change', function () {
		document.getElementById("property-hidden-ids").value = $("#property-filter").val().join();
	});
    function LoadIncomeTable() {
        tableIncome = $('#income-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "bAutoWidth": false,
            "bLengthChange": true,
            "searching": true,
            "search": { "caseInsensitive": true },
            "ajax": {
               "url": "/Income/GetIncomeList",
               "type": 'POST',
               "data": function () {
				   return {
					   propertyFilter: document.getElementById("property-hidden-ids").value,
					   monthYearFilter: $('#month-year-filter').val(),
                       includeChild: $('#isIncludeChild').is(":checked"),
                       paymentDate: $('#payment-date-filter').val()
				   };
               }
            },
            "columns": [
                    { "data": "IncomeType" },
					{ "data": "Description" },
                    { "data": "SiteType" },
                    { "data": "BookingCode" },
                    { "data": "PropertyName" },
                    { "data": "GuestName" },
                    { "data": "Amount" },
                    { "data": "Receivables" },
					{ "data": "Balance" },
                    { "data": "PaymentDate" },
					{ "data": "DueDate" },
                    { "data": "Status" },
                    { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
				$(row).attr('data-income-id', data.Id);
                $(row).find('td:eq(8)').attr('summary', JSON.stringify(data.ReceivablesSummary));
            }
        });
    }
    function ReceivableSummary() {
        $(document).on('click', "#income-table tr td", function () {
            if ($(this).attr('summary') != undefined) {
                $('#summary-modal').find('.content').html($.parseJSON($(this).attr('summary')));
                $('.summary-table').DataTable();
                $('#summary-modal').modal('show').modal('refresh');
            }
        });
    }
    function RefreshIncomeList() {
        tableIncome.ajax.reload(null, false);
    }

    function LoadIncomeTypeTable() {
        tableIncomeType = $('#income-type-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Income/GetIncomeTypeList",
                "type": 'GET',
            },
            "columns": [
                { "data": "Name" },
                { "data": "Type" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-income-type-id', data.Id);
            }
        });
    }

    function CreateIncomeTypeBtn() {
        $(document).on('click', '#create-income-type-btn', function (e) {
            $('#income-type-modal').attr('income-type-id', '0')
            $('#income-type-modal').modal('show');
        });
    }
    function GetIncomeTypeDetails() {
        $(document).on('click', '.income-type-edit', function (e) {
            var id = $(this).parents('tr').attr('data-income-type-id');
            $.ajax({
                type: 'GET',
                url: "Income/GetIncomeTypeDetails",
                data: { id: id },
            success: function (data) {
                $('#income-type-modal').attr('income-type-id', data.incomeType.Id);
                $('#income-type-name').val(data.incomeType.TypeName);
                $('#companyTermId').val(data.incomeType.CompanyTerm);
                $('#income-type-modal').modal('show');
            }
        });
        });
    }

    function SaveIncomeType() {
        $(document).on('click', '#income-type-save', function (e) {
            var formData = new FormData();
            formData.append("Id", $('#income-type-modal').attr('income-type-id'));
            formData.append("TypeName", $('#income-type-name').val());
            formData.append("CompanyTerm", $('#companyTermId').val());
            $.ajax({
                type: 'POST',
                url: "Income/SaveIncomeType",
                contentType: false,
                cache: false,
                dataType: "json",
                processData: false,
                data: formData,
                success: function (data) {
                    if (data.success) {
                        tableIncomeType.ajax.reload(null, false);
                        $('#income-type-modal').modal('hide');
                        GetIncomeType();
                        swal("Income type saved!", "", "success");
                    }
                    else {
                        swal("Failed to save income type!", "", "error");
                    }
                }
            });
        });
    }

    function DeleteIncomeType() {
        $(document).on('click', '.income-type-delete', function (e) {
            var id = $(this).parents('tr').attr('data-income-type-id');
            swal({
                title: "Are you sure you want to delete this income type?",
                text: "",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
            },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: "Income/DeleteIncomeType",
                        data: { id: id },
                        success: function (data) {
                            if (data.success) {
                                tableIncomeType.ajax.reload(null, false);
                                swal("Income type deleted!", "", "success");
                                GetIncomeType();
                            }
                            else {
                                swal("Failed to delete!", "", "error");
                            }
                        }
                    });
                });
        });
    }
    // ------------------
    // ------------------
    //   Event Listeners
    // ------------------
    // ------------------

    function activateDropdown() {
        $('.ui.dropdown').dropdown({ on: 'hover' });
    }
});