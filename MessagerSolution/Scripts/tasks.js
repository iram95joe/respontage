﻿var $taskDashboardTable = $("#task-dashboard-table > tbody");

$(document).ready(function () {
    initialize();

    function initialize() {
        setupSemanticUI();
        setEventListeners();
    }

    function setupSemanticUI() {
        $('.ui.accordion').accordion();
        $('.ui.dropdown').dropdown({ on: 'hover' });
        $('.ui.checkbox').checkbox();
        $('.ui.calendar').calendar({ type: 'date' });
        $('.ui.calendar.month').calendar({ type: 'month' });
        $('.ui.calendar.time').calendar({ type: 'time' });
        $('table td a.table-tooltip').popup();
        $('.menu .item').tab();
    }

    function setEventListeners() {
        onDisplaySidebar();
        onClickSearchTask();
        onBookingClick();
        onAssignAvailableWorker();
        onSubmitWorkerAssignment();
		onTripStoryClicked();
		onChangeTaskStatus();
		onChangeSubTaskStatus();
		AcceptAssignment();
		DeclineAssignment();
		CancelAssignment();
		OnTheWayAssignment();
		StartAssignment();
		FinishAssignment();
	}
	function OnTheWayAssignment() {
		$(document).on('click', '.otw-btn', function (e) {
			var id = $(this).attr('data-assignment-id');
			$.ajax({
				type: "POST",
				url: "/Worker/OnTheWayAssignment",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Status Change.", type: "success" });
						//$(this).parent().parent().parent().parent().removeClass();
						$('.onday-' + id).empty();
						var action = 'Today Status:<label>On the way</label> <div class="row message-send">' +
							'<div class="sixteen wide column">' +
							'<button type="button" data-assignment-id="' + id + '" class="small ui button start-btn">' +
							'Start' +
							'</button>' +
							'</div>' +
							'</div>';

						$('.onday-' + id).empty();
						$('.onday-' + id).append(action);

					}
				}
			});
		});
	}
	function StartAssignment() {
		$(document).on('click', '.start-btn', function (e) {
			var id = $(this).attr('data-assignment-id');
			$.ajax({
				type: "POST",
				url: "/Worker/StartAssignment",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Status Change.", type: "success" });
						//$(this).parent().parent().parent().parent().removeClass();
						$('.onday-' + id).empty();
						var action = 'Today Status:<label>Started</label> <div class="row message-send">' +
							'<div class="sixteen wide column">' +
							'<button type="button" data-assignment-id="' + id + '" class="small ui button finish-btn">' +
							'Finish' +
							'</button>' +
							'</div>' +
							'</div>';

						$('.onday-' + id).empty();
						$('.onday-' + id).append(action);

					}
				}
			});
		});
	}
	function FinishAssignment() {
		$(document).on('click', '.finish-btn', function (e) {
			var id = $(this).attr('data-assignment-id');
			$.ajax({
				type: "POST",
				url: "/Worker/FinishAssignment",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Status Change.", type: "success" });
						$(this).parent().parent().parent().parent().removeClass();
						$('.onday-' + id).empty();
						var action = 'Today Status:<label>Complate</label>';

						$('.onday-' + id).empty();
						$('.onday-' + id).append(action);

					}
				}
			});
		});
	}
	//Assignment Status Start
	function AcceptAssignment() {
		$(document).on('click', '.accept-btn', function (e) {
			var id = $(this).attr('data-assignment-id');
			var parentDiv = $(this).parent().parent().parent().parent();
			$.ajax({
				type: "POST",
				url: "/Worker/AcceptAssignment",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Assignment Accepted.", type: "success" });
						if (parentDiv.hasClass("worker")) {
							parentDiv.removeClass();
							parentDiv.addClass('worker');
						}
						else if (parentDiv.hasClass("admin")) {
							parentDiv.removeClass();
							parentDiv.addClass('admin accepted-assignment-admin');
						}
						$('.' + id).empty();
						var action ='Action:<div class="row message-send">'+
							'<div class="sixteen wide column">' +
							'<button type="button" data-assignment-id="' + id + '" class="small ui button cancel-btn">' +
									'Cancel'+
                                '</button>'+
							'</div>'+
							'</div>';

						$('.' + id).empty();
						$('.' + id).append(action);

					}
				}
			});
		});
	}
	function DeclineAssignment() {
		$(document).on('click', '.decline-btn', function (e) {
			var id = $(this).attr('data-assignment-id');
			var parentDiv = $(this).parent().parent().parent().parent();
			$.ajax({
				type: "POST",
				url: "/Worker/DeclineAssignment",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Assignment Declined.", type: "success" });
						if (parentDiv.hasClass("worker")) {
							parentDiv.removeClass();
							parentDiv.addClass('worker decline-cancel-assignment');
						}
						else if (parentDiv.hasClass("admin")) {
							parentDiv.removeClass();
							parentDiv.addClass('admin decline-cancel-assignment-admin');
						}
						$('.' + id).empty().append('Action:Decline/Confirmed-Cancel');

					}
				}
			});
		});
	}
	function CancelAssignment() {
		$(document).on('click', '.cancel-btn', function (e) {
			var id = $(this).attr('data-assignment-id');
			var parentDiv = $(this).parent().parent().parent().parent();
			$.ajax({
				type: "POST",
				url: "/Worker/CancelAssignment",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Assignment Cancelled.", type: "success" });
						if (parentDiv.hasClass("worker")) {
							parentDiv.removeClass();
							parentDiv.addClass('worker decline-cancel-assignment');
						}
						else if (parentDiv.hasClass("admin")) {
							parentDiv.removeClass();
							parentDiv.addClass('admin decline-cancel-assignment-admin');
						}

						$('.' + id).empty().append('Action:Decline/Confirmed-Cancel');

					}
				}
			});

		});
	} 
	//Assignment Status end
    /*Event Listeners*/
    function onDisplaySidebar() {
        $('#js-btn_sidebar').on('click', function () {
            $('.ui.sidebar').sidebar('toggle');
        });
    }

    function onClickSearchTask() {
        $("#task-search-btn").click(function () {
            ClearRecords();

            var selectedMonthValue = $('.ui.calendar.month').calendar('get date') != null ? $('.ui.calendar.month').calendar('get date').getMonth() + 1 : '0';
            var selectedYearValue = $('.ui.calendar.month').calendar('get date') != null ? $('.ui.calendar.month').calendar('get date').getFullYear() : '0';

            var selectedMonth = selectedMonthValue;
            var selectedYear = selectedYearValue;
            var propertyValue = $("#property-filter").val();  //Value -1 means all property
				var selectedProperty = parseInt(propertyValue);

            $.ajax({
                type: 'GET',
                url: "/Task/GetInquiryWithInquiryTask",
                data: { selectedMonth: selectedMonth, selectedYear: selectedYear, selectedProperty: selectedProperty },
                async: true,
                success: function (data) {

                    //renderTaskList(data); // Original UI that show All Travel Events in left column and all task in right column
                    renderTaskDashboard(data);
                }
            });
        });
    }
	function onChangeTaskStatus() {
		$(document).on('change', 'input.task-status', function () {
			$.ajax({
				type: "POST",
				url: "/Task/UpdateTaskStatus",
				data: { Id: $(this).attr('data-task-id'), status: $(this).is(':checked') },
				success: function (data) {

				}
			});
		});
	}
	function onChangeSubTaskStatus() {
		$(document).on('change', 'input.subtask-status', function () {
			$.ajax({
				type: "POST",
				url: "/Task/UpdateSubTaskStatus",
				data: { Id: $(this).attr('data-subtask-id'), status: $(this).is(':checked') },
				success: function (data) {
				}
			});
		});
	}
    function ClearRecords() {
        $("#task-dashboard-table > tbody > tr").remove();
    }

    function onBookingClick() {
        $(document).on('click', '.task-record-row', function () {
            var hostingAccountId = $(this).attr("data-host-id");
            var threadId = $(this).attr("data-thread-id");

            quickActionModal({
                hostId: hostingAccountId,
                threadId: threadId,
                page: 'booking'
            });

        });
    }

    function GetTravelEventTypeHtml(noteType) {
        var html = "";

        switch (noteType) {
            case 1: html = "<i class=\"bookmark icon\"></i>"; break;
            case 2: html = "<i class=\"plane icon\"></i>"; break;
            case 3: html = "<i class=\"plane icon\"></i>"; break;
            case 4: html = "<i class=\"key icon\"></i>"; break;
            case 5: html = "<i class=\"car icon\"></i>"; break;
            case 6: html = "<i class=\"car icon\"></i>"; break;
            case 7: html = "<i class=\"ship icon\"></i>"; break;
            case 8: html = "<i class=\"ship icon\"></i>"; break;
            case 9: html = "<i class=\"train icon\"></i>"; break;
            case 10: html = "<i class=\"train icon\"></i>"; break;
            case 11: html = "<i class=\"bus icon\"></i>"; break;
			case 12: html = "<i class=\"bus icon\"></i>"; break;
			case 13: html = "<i class=\"clock icon\"></i>"; break;
			case 14: html = "<i class=\"clock icon\"></i>"; break;
        }

        return html;

    }

    function renderTaskList(data) {
        if (data.length >= 1) {
            for (var e = 0; e < data.length; e++) {
                var html = "";
                html += "<tr class='task-record-row' data-host-id='" + data[e].Inquiry.HostingAccountId + "' data-thread-id='" + data[e].Inquiry.ThreadId + "'>";
                html += "<td>";
                html += "<table class='task-template-table'>";
                html += "<tr>";
                html += "<td class='travel-event-column'>";
                html += "<br/>";
                html += "<div>" + GetDateDisplay(data[e].Inquiry.CheckInDate) + "</div>";
                html += "<div><b>" + data[e].GuestInfo.Name + "</b> <span>check in</span></div>";
                html += "<br/>";
                html += "<div>" + GetDateDisplay(data[e].Inquiry.CheckOutDate) + "</div>";
                html += "<div><b>" + data[e].GuestInfo.Name + "</b> <span>check out</span></div>";
                html += "<br/>";
                html += "<div class='property-info'>" + data[e].PropertyInfo.Name + "</div>";
                html += "<br/>";
                html += "<div><i class='users icon'></i>" + data[e].Inquiry.GuestCount + "</div>";
                html += "<br/>";
                if (data[e].AssignedWorker != null) {
                    html += "<div><b><u>Worker</u></b></div>";
                    html += "<br/>";
                    html += "<div class='worker-info'>";
                    html += "<div>" + data[e].AssignedWorker.FirstName + " " + data[e].AssignedWorker.LastName + "</div>";
                    html += "<div><i class=\"mobile icon\"></i> " + data[e].AssignedWorker.Mobile + "</div>";
                    html += "<div>Start Date: " + GetDateDisplay(data[e].AssignedWorker.StartDate) + "</div>";
                    html += "<div>End Date: " + GetDateDisplay(data[e].AssignedWorker.EndDate) + "</div>";
                    html += "</div>";
                    html += "<br/>";
                }
                // 1/2/2018 Commented
                //html += "<table class='task-template-table'>";
                //html += "<tr>";
                //html += "<td class='travel-event-column'>";
                if (data[e].TravelEvents.length >= 1) {
                    html += "<div>";
                    html += "<div><b><u>Travel Events</u></b></div>";
                    for (var m = 0; m < data[e].TravelEvents.length; m++) {
                        var travelEvent = data[e].TravelEvents[m];
                        html += "<br/>";
                        html += "<div class='travel-event-item'>" + GetTravelEventTypeHtml(travelEvent.NoteType);

                        if (travelEvent.NoteType == 1) {
                            html += " " + travelEvent.InquiryNote;
                        }
                        else if (travelEvent.NoteType == 2) {
                            html += " Flight Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += travelEvent.AirportCodeDeparture + " -> " + travelEvent.AirportCodeArrival;
                            html += "<br/>";
                            html += travelEvent.Airline;

                        } else if (travelEvent.NoteType == 3) {
                            html += " Flight Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += travelEvent.AirportCodeDeparture + " -> " + travelEvent.AirportCodeArrival;
                            html += "<br/>";
                            html += travelEvent.Airline;
                        }
                        else if (travelEvent.NoteType == 4) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.MeetingDateTime);
                            html += "<br/>";
                            html += " @ " + travelEvent.MeetingPlace + " ";
                            html += "<br/>";
                            html += "Meeting Date Time: " + GetDateDisplay(travelEvent.MeetingDateTime);
                        }
                        else if (travelEvent.NoteType == 5) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Car:" + travelEvent.CarType + " " + travelEvent.CarColor;
                            html += "<br/>";
                            html += " Plate Number: " + travelEvent.PlateNumber;
                        }
                        else if (travelEvent.NoteType == 6) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Car:" + travelEvent.CarType + " " + travelEvent.CarColor;
                            html += "<br/>";
                            html += " Plate Number: " + travelEvent.PlateNumber;
                            html += "<br/>";
                            html += "Departure City: " + travelEvent.DepartureCity;
                        }
                        else if (travelEvent.NoteType == 7) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Arrival Port: " + travelEvent.ArrivalPort;
                            html += "<br/>";
                            html += " Cruise Name: " + travelEvent.CruiseName;
                        }
                        else if (travelEvent.NoteType == 8) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Departure Port: " + travelEvent.DeparturePort;
                            html += "<br/>";
                            html += " Cruise Name: " + travelEvent.CruiseName;
                        }
                        else if (travelEvent.NoteType == 9) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Train Name: " + travelEvent.Name;
                        }
                        else if (travelEvent.NoteType == 10) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Train Name: " + travelEvent.Name;
                        }
                        else if (travelEvent.NoteType == 11) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Bus Name: " + travelEvent.Name;
                        }
                        else if (travelEvent.NoteType == 12) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Bus Name: " + travelEvent.Name;
						}
						else if (travelEvent.NoteType == 13) {
							html += " Self Checkin @ " + GetDateDisplay(travelEvent.Checkin);
							html += "<br/>";
							html += " Contact Person: " + travelEvent.ContactPerson;
						}
						else if (travelEvent.NoteType == 14) {
							html += " Self Checkout @ " + GetDateDisplay(travelEvent.Checkout);
							html += "<br/>";
							html += " Contact Person: " + travelEvent.ContactPerson;

						}
                        html += "</div>";
                    }
                    html += "</div>";
                }
                html += "</td>";
                if (data[e].Inquiry.IsActive) {
                    html += "<td class='task-template-column'>";
                    html += "<table class='task-template-table'>";

                    //START: INQUIRY TASK TEMPLATE TYPE
                    if (data[e].InquiryTask.length >= 1) {
                        var inquiryTasks = data[e].InquiryTask;
                        html += "<tr><td><div class='task-template-label'>Inquiry Tasks</div></td></tr>";
                        for (var index = 0; index < inquiryTasks.length; index++) {
                            var object = inquiryTasks[index];
                            html += "<tr>";
                            html += "<td>";
                            html += "<div class=\"ui toggle checkbox\">";
                            html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                            html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                            html += "</div>";
                            html += "<div class=\"task-last-updated-time\">";
                            html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                            html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                            html += "</div>";
                            html += "</td>";
                            html += "</tr>";
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Inquiry Tasks</td>";
                        html += "</tr>";
                    }
                    //END: INQUIRY TASK TEMPLATE TYPE;

                    //START: BOOKING TASK TEMPLATE TYPE
                    if (data[e].BookingTask.length >= 1) {
                        var bookingTasks = data[e].BookingTask;
                        html += "<tr><td><div class='task-template-label'>Booking Tasks</div></td></tr>";
                        for (var index = 0; index < bookingTasks.length; index++) {
                            var object = bookingTasks[index];
                            html += "<tr>";
                            html += "<td>";
                            html += "<div class=\"ui toggle checkbox\">";
                            html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                            html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                            html += "</div>";
                            html += "<div class=\"task-last-updated-time\">";
                            html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                            html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                            html += "</div>";
                            html += "</td>";
                            html += "</tr>";
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Booking Tasks</td>";
                        html += "</tr>";
                    }
                    //END: BOOKING TASK TEMPLATE TYPE;

                    //START: CHECK-IN TASK TEMPLATE TYPE
                    if (data[e].CheckInTask.length >= 1) {
                        var checkInTasks = data[e].CheckInTask;
                        html += "<tr><td><div class='task-template-label'>Check-In Tasks</div></td></tr>";
                        for (var index = 0; index < checkInTasks.length; index++) {
                            var object = checkInTasks[index];
                            html += "<tr>";
                            html += "<td>";
                            html += "<div class=\"ui toggle checkbox\">";
                            html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                            html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                            html += "</div>";
                            html += "<div class=\"task-last-updated-time\">";
                            html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                            html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                            html += "</div>";
                            html += "</td>";
                            html += "</tr>";
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Check-In Tasks</td>";
                        html += "</tr>";
                    }
                    //END: CHECK-IN TASK TEMPLATE TYPE;

                    // Add by Danial - 7/2/2018
                    //START: CHECK-OUT TASK TEMPLATE TYPE
                    if (data[e].CheckOutTask.length >= 1) {
                        var checkOutTasks = data[e].CheckOutTask;
                        html += "<tr><td><div class='task-template-label'>Check-Out Tasks</div></td></tr>";
                        for (var index = 0; index < checkOutTasks.length; index++) {
                            var object = checkOutTasks[index];
                            html += "<tr>";
                            html += "<td>";
                            html += "<div class=\"ui toggle checkbox\">";
                            html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                            html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                            html += "</div>";
                            html += "<div class=\"task-last-updated-time\">";
                            html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                            html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                            html += "</div>";
                            html += "</td>";
                            html += "</tr>";
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Check-Out Tasks</td>";
                        html += "</tr>";
                    }
                    //END: CHECK-OUT TASK TEMPLATE TYPE;

                    //START: TURNOVER TASK TEMPLATE TYPE
                    if (data[e].TurnoverTask.length >= 1) {
                        var turnoverTasks = data[e].TurnoverTask;
                        html += "<tr><td><div class='task-template-label'>Turnover Tasks</div></td></tr>";
                        for (var index = 0; index < turnoverTasks.length; index++) {
                            var object = turnoverTasks[index];
                            html += "<tr>";
                            html += "<td>";
                            html += "<div class=\"ui toggle checkbox\">";
                            html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                            html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                            html += "</div>";
                            html += "<div class=\"task-last-updated-time\">";
                            html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                            html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                            html += "</div>";
                            html += "</td>";
                            html += "</tr>";
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Turnover Tasks</td>";
                        html += "</tr>";
                    }
                    //END: TURNOVER TASK TEMPLATE TYPE;

                    // 1/2/2018 Commented
                    //html += "</table>";
                    //html += "</td>";
                }
                else {
                    html += "<div>No Task</div>";
                }
                html += "</table>";
                html += "</td>";
                html += "</tr>";
                html += "</table>";
                /*
                html += "</td>";
                if (data[e].InquiryTask.length >= 1) {
                    html += "<td class='task-template-column'>";
                    html += "<table class='task-template-table'>";

                    var inquiryTaskList = data[e].InquiryTask;

                    for (var j = 0; j < inquiryTaskList.length; j++) {

                        var object = inquiryTaskList[j];

                        html += "<tr>";
                        html += "<td>";
                        html += "<div class=\"ui toggle checkbox\">";
                        html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                        html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                        html += "</div>";
                        html += "<div class=\"task-last-updated-time\">";
                        html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                        html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                        html += "</div>";
                        html += "</td>";
                        html += "</tr>";
                    }
                    html += "</table>";
                    html += "</td></tr>";
                    
                    //var firstInquiryTask = data[e].InquiryTask[0];
                    //html += "<td>";
                    //html += "<div class=\"ui toggle checkbox\">";
                    //html += firstInquiryTask.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                    //html += "<label class=\"task-status-label\">" + firstInquiryTask.TaskDescription + "</label>";
                    //html += "</div>";
                    //html += "<div class=\"task-last-updated-time\">";
                    //html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                    //html += "<div>" + GetDateDisplay(firstInquiryTask.DateUpdated) + "</div>";
                    //html += "</div>";
                    //html += "</td>";
                    //html += "</tr>";

                    //data[e].InquiryTask.shift();
                    //var inquiryTasksWithoutFirst = data[e].InquiryTask;

                    //for (var j = 0; j < inquiryTasksWithoutFirst.length; j++) {

                    //    var object = inquiryTasksWithoutFirst[j];

                    //    html += "<tr class='task-record-row' data-host-id='" + data[e].Inquiry.HostingAccountId + "' data-thread-id='" + data[e].Inquiry.ThreadId + "'>";
                    //    html += "<td>";
                    //    html += "<div class=\"ui toggle checkbox\">";
                    //    html += object.Status ? "<input type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
                    //    html += "<label class=\"task-status-label\">" + object.TaskDescription + "</label>";
                    //    html += "</div>";
                    //    html += "<div class=\"task-last-updated-time\">";
                    //    html += "<span class=\"last-updated-time-label\">Last Updated Time:</span>";
                    //    html += "<div>" + GetDateDisplay(object.DateUpdated) + "</div>";
                    //    html += "</div>";
                    //    html += "</td>";
                    //    html += "</tr>";
                    //}
                    
                } else {
                    html += "<td class='empty-tasks'>No Tasks</td>";
                    html += "</tr>";
                }
                */
                $taskDashboardTable.append(html);
            }
        }
        else {
            var emptyHtml = "";
            emptyHtml += "<tr class='empty-records'><td colspan='2'>No Records Found</td></tr>";
            $taskDashboardTable.append(emptyHtml);
        }
    }

    /*Add by Danial - 13/2/2018 */
    function renderTaskDashboard(data) {
        if (data.length >= 1) {
            var html = "";

            for (var e = 0; e < data.length; e++) {

                var inquiryTasksCompletedCount = getTaskCompletedCount(data[e].InquiryTask);
                var inquiryTasksTotalCount = data[e].InquiryTask.length;
                var inquiryTaskColumnDisplayValue = inquiryTasksTotalCount == 0 ? inquiryTasksTotalCount : inquiryTasksCompletedCount + "/" + inquiryTasksTotalCount;
                var inquiryTaskColumnCellColor = inquiryTasksTotalCount == 0 ? "text-center" : (inquiryTasksTotalCount == inquiryTasksCompletedCount ? "all-task-completed-col" : (inquiryTasksCompletedCount == 0 ? "no-task-completed-col" : "partial-task-completed-col"));

                var bookingTasksCompletedCount = getTaskCompletedCount(data[e].BookingTask);
                var bookingTasksTotalCount = data[e].BookingTask.length;
                var bookingTaskColumnDisplayValue = bookingTasksTotalCount == 0 ? bookingTasksTotalCount : bookingTasksCompletedCount + "/" + bookingTasksTotalCount;
                var bookingTaskColumnCellColor = bookingTasksTotalCount == 0 ? "text-center" : (bookingTasksTotalCount == bookingTasksCompletedCount ? "all-task-completed-col" : (bookingTasksCompletedCount == 0 ? "no-task-completed-col" : "partial-task-completed-col"));

                var checkInTasksCompletedCount = getTaskCompletedCount(data[e].CheckInTask);
                var checkInTasksTotalCount = data[e].CheckInTask.length;
                var checkInTaskColumnDisplayValue = checkInTasksTotalCount == 0 ? checkInTasksTotalCount : checkInTasksCompletedCount + "/" + checkInTasksTotalCount;
                var checkInTaskColumnCellColor = checkInTasksTotalCount == 0 ? "text-center" : (checkInTasksTotalCount == checkInTasksCompletedCount ? "all-task-completed-col" : (checkInTasksCompletedCount == 0 ? "no-task-completed-col" : "partial-task-completed-col"));

                var checkOutTasksCompletedCount = getTaskCompletedCount(data[e].CheckOutTask);
                var checkOutTasksTotalCount = data[e].CheckOutTask.length;
                var checkOutTaskColumnDisplayValue = checkOutTasksTotalCount == 0 ? checkOutTasksTotalCount : checkOutTasksCompletedCount + "/" + checkOutTasksTotalCount;
                var checkOutTaskColumnCellColor = checkOutTasksTotalCount == 0 ? "text-center" : (checkOutTasksTotalCount == checkOutTasksCompletedCount ? "all-task-completed-col" : (checkOutTasksCompletedCount == 0 ? "no-task-completed-col" : "partial-task-completed-col"));

                var turnoverTasksCompletedCount = getTaskCompletedCount(data[e].TurnoverTask);
                var turnoverTasksTotalCount = data[e].TurnoverTask.length;
                var turnoverTaskColumnDisplayValue = turnoverTasksTotalCount == 0 ? turnoverTasksTotalCount : turnoverTasksCompletedCount + "/" + turnoverTasksTotalCount;
                var turnoverTaskColumnCellColor = turnoverTasksTotalCount == 0 ? "text-center" : (checkOutTasksTotalCount == turnoverTasksCompletedCount ? "all-task-completed-col" : (turnoverTasksCompletedCount == 0 ? "no-task-completed-col" : "partial-task-completed-col"));

                html += "<tr class='task-record-row' data-host-id='" + data[e].Inquiry.HostingAccountId + "' data-thread-id='" + data[e].Inquiry.ThreadId + "'>";
                html += "<td>";
                html += data[e].PropertyInfo.Name;
                html += "</td>";
                html += "<td>";
                html += data[e].GuestInfo.Name;
                html += "</td>";
                html += "<td>";
                html += GetDateDisplay(data[e].Inquiry.CheckInDate);
                html += "</td>";
                html += "<td>";
                html += GetDateDisplay(data[e].Inquiry.CheckOutDate);
                html += "</td>";
                html += "<td class='" + inquiryTaskColumnCellColor + "'>";
                html += inquiryTaskColumnDisplayValue;
                html += "</td>";
                html += "<td class='" + bookingTaskColumnCellColor + "'>";
                html += bookingTaskColumnDisplayValue;
                html += "</td>";
                html += "<td class='" + checkInTaskColumnCellColor + "'>";
                html += checkInTaskColumnDisplayValue;
                html += "</td>";
                html += "<td class='" + checkOutTaskColumnCellColor + "'>";
                html += checkOutTaskColumnDisplayValue;
                html += "</td>";
                html += "<td class='" + turnoverTaskColumnCellColor + "'>";
                html += turnoverTaskColumnDisplayValue;
				html += "</td>";

				html += "<td>";
				var checkinWorkerTurnoverCount = data[e].PreCheckInTurnoverWorkers.length;
				if (data[e].PreCheckInTurnoverWorkers.length > 0) {
					for (var i = 0; i < checkinWorkerTurnoverCount; i++) {
						html += data[e].PreCheckInTurnoverWorkers[i].FirstName + "<span>&nbsp;</span>" + data[e].PreCheckInTurnoverWorkers[i].LastName;
						html += "<br>" + GetDateDisplay(data[e].PreCheckInTurnoverWorkers[i].StartDate) +"<br><br>";
					}
				}
				else {
					html += "<div> No Worker Assigned</div>";
					}
				html += "</td>";

				html += "<td>";
				if (data[e].PostCheckOutTurnoverWorkers.length > 0) {
					var checkoutWorkerTurnoverCount = data[e].PostCheckOutTurnoverWorkers.length;
					for (var i = 0; i < checkoutWorkerTurnoverCount; i++) {
						html += data[e].PostCheckOutTurnoverWorkers[i].FirstName + "<span>&nbsp;</span>" + data[e].PostCheckOutTurnoverWorkers[i].LastName;
						html += "<br>" + GetDateDisplay(data[e].PostCheckOutTurnoverWorkers[i].StartDate) +"<br><br>";
					}
				}
				else {
					html += "<div> No Worker Assigned</div";
				}
                html += "</td>";
                html += "</tr>";

                /*Accordion*/
                html += "<tr>";
                html += "<td colspan='11'>";

                html += "<div class='ui styled accordion task-accordion-header'>";
                html += "<div class=\"title\"><i class=\"dropdown icon\"></i>Task Info</div>";
                html += "<div class=\"content\">";
                html += "<table class=\"task-accordion-table\">";
                html += "<tr>";
                html += "<th class=\"table-header\">Trip Story</th>";
                html += "<th class=\"table-header\">Task Status</th>";
                html += "</tr>";
                html += "<tbody>";
                html += "<tr>";

                /*START Trip Story Column*/
                html += "<td>";
                html += "<div class=\"task-story-container\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='booking' style='cursor: pointer;'>";
                html += "<label class=\"task-story-label\">Inquiry</label>";
                if (data[e].Inquiry.IsActive) {
                    html += "<div>Inquiry Date : " + GetDateDisplay(data[e].Inquiry.InquiryDate) + "</div>";
                }
                else {
                    html += "<div>Property Name : " + data[e].PropertyInfo.Name + "</div>";
                    html += "<div>Check-In Date : " + GetDateDisplay(data[e].Inquiry.CheckInDate) + "</div>";
                    html += "<div>Check-Out Date : " + GetDateDisplay(data[e].Inquiry.CheckOutDate) + "</div>";
                    html += "<div>Guest Count : " + data[e].Inquiry.GuestCount + "</div>";
                    html += "<div>Guest Name : " + data[e].GuestInfo.Name + "</div>";
                }
                html += "</div>";
                html += "<br/>";
                html += "<div class=\"task-story-container\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='booking' style='cursor: pointer;'>";
                html += "<label class=\"task-story-label\">Booking</label>";
                if (data[e].Inquiry.IsActive) {
                    html += "<div>Property Name : " + data[e].PropertyInfo.Name + "</div>";
                    html += "<div>Check-In Date : " + GetDateDisplay(data[e].Inquiry.CheckInDate) + "</div>";
                    html += "<div>Check-Out Date : " + GetDateDisplay(data[e].Inquiry.CheckOutDate) + "</div>";
                    html += "<div>Guest Count : " + data[e].Inquiry.GuestCount + "</div>";
                    html += "<div>Guest Name : " + data[e].GuestInfo.Name + "</div>";
                }
                html += "</div>";
                html += "<br/>";
                html += "<div class=\"task-story-container\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='travel-events' style='cursor: pointer;'>";
                html += "<label class=\"task-story-label\">Notes</label>";
                if (data[e].TravelEvents != null && data[e].TravelEvents.length > 0) {
                    html += "<div>";
                    html += "<div>";
                    for (var index = 0; index < data[e].TravelEvents.length; index++) {
                        var travelEventItem = data[e].TravelEvents[index];
                        if (travelEventItem.NoteType == 1) {
                            html += "<div><i class=\"bookmark icon\"></i>" + travelEventItem.InquiryNote + "</div>";
                        }
                    }
                    html += "</div>";
                    html += "</div>";
                }
                html += "</div>";
                html += "<br/>";
                var checkInDate = GetShortDate(data[e].Inquiry.CheckInDate);
				// onclick=" + (data[e].AssignWorker == null ? 'showTaskWorkerAssignmentModal(' + data[e].Inquiry.InquiryId + ',"' + checkInDate + '",' + 1 + ',' + 1 + ');' : '') + "
				html += "<div class=\"task-story-container " + (data[e].PreCheckInTurnoverWorkers.length==0 ? 'no-worker-turnover' : '') + "\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='workers'>";
				html += "<label class=\"task-story-label\">Arrival Cleaner/s</label>";
			
				var checkinWorkerTurnoverCount = data[e].PreCheckInTurnoverWorkers.length;
				if (data[e].PreCheckInTurnoverWorkers.length > 0) {
					for (var i = 0; i < checkinWorkerTurnoverCount; i++) {
						html += "<div>Worker Name : " + data[e].PreCheckInTurnoverWorkers[i].FirstName + "<span>&nbsp;</span>" + data[e].PreCheckInTurnoverWorkers[i].LastName + "</div>";
						html += "<div>Start Date : " + GetDateDisplay(data[e].PreCheckInTurnoverWorkers[i].StartDate) + "</div>";
						html += "<div>End Date : " + GetDateDisplay(data[e].PreCheckInTurnoverWorkers[i].EndDate) + "</div>";
						html += "<div>Worker Contact : " + data[e].PreCheckInTurnoverWorkers[i].Mobile + "</div>";
						html += "<hr>";
					}
                }
                else {
                    html += "<div>No Worker Assigned</div>";
                }
                html += "</div>";
				html += "<br/>";
				html += "<div class=\"task-story-container " + (data[e].CheckInWorkers.length == 0 ? 'no-worker-turnover' : '') + "\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='workers'>";
				html += "<label class=\"task-story-label\">Arrival Meeting</label>";
				var checkinWorkerCount = data[e].CheckInWorkers.length;
				if (data[e].CheckInWorkers.length != 0) {
					for (var i = 0; i < checkinWorkerCount;i++) {
						html += "<div>Worker Name : " + data[e].CheckInWorkers[i].FirstName + "<span>&nbsp;</span>" + data[e].CheckInWorkers[i].LastName + "</div>";
						html += "<div>Check-In Date : " + data[e].CheckInWorkers[i].StartDate + "</div>";
						html += "<div>Check-Out Date : " + data[e].CheckInWorkers[i].EndDate + "</div>";
						html += "<div>Worker Contact : " + data[e].CheckInWorkers[i].Mobile + "</div>";
						html += "<hr>";
					}
                }
                else {
                    html += "<div>No Worker Assigned</div>";
                }
                html += "</div>";
                html += "<br/>";
                html += "<div class=\"task-story-container\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='travel-events' style='cursor: pointer;'>";
                html += "<label class=\"task-story-label\">Trip Events</label>";
                if (data[e].TravelEvents.length > 0) {
                    html += "<div>";
                    html += "<div>";
                    for (var m = 0; m < data[e].TravelEvents.length; m++) {
                        var travelEvent = data[e].TravelEvents[m];
                        html += "<br/>";
                        html += "<div class='travel-event-item'>" + GetTravelEventTypeHtml(travelEvent.NoteType);

                        if (travelEvent.NoteType == 1) {
                            html += " " + travelEvent.InquiryNote;
                        }
                        else if (travelEvent.NoteType == 2) {
                            html += " Flight Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += travelEvent.AirportCodeDeparture + " -> " + travelEvent.AirportCodeArrival;
                            html += "<br/>";
                            html += travelEvent.Airline;

                        } else if (travelEvent.NoteType == 3) {
                            html += " Flight Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += travelEvent.AirportCodeDeparture + " -> " + travelEvent.AirportCodeArrival;
                            html += "<br/>";
                            html += travelEvent.Airline;
                        }
                        else if (travelEvent.NoteType == 4) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.MeetingDateTime);
                            html += "<br/>";
                            html += " @ " + travelEvent.MeetingPlace + " ";
                            html += "<br/>";
                            html += "Meeting Date Time: " + GetDateDisplay(travelEvent.MeetingDateTime);
                        }
                        else if (travelEvent.NoteType == 5) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Car:" + travelEvent.CarType + " " + travelEvent.CarColor;
                            html += "<br/>";
                            html += " Plate Number: " + travelEvent.PlateNumber;
                        }
                        else if (travelEvent.NoteType == 6) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Car:" + travelEvent.CarType + " " + travelEvent.CarColor;
                            html += "<br/>";
                            html += " Plate Number: " + travelEvent.PlateNumber;
                            html += "<br/>";
                            html += "Departure City: " + travelEvent.DepartureCity;
                        }
                        else if (travelEvent.NoteType == 7) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Arrival Port: " + travelEvent.ArrivalPort;
                            html += "<br/>";
                            html += " Cruise Name: " + travelEvent.CruiseName;
                        }
                        else if (travelEvent.NoteType == 8) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Departure Port: " + travelEvent.DeparturePort;
                            html += "<br/>";
                            html += " Cruise Name: " + travelEvent.CruiseName;
                        }
                        else if (travelEvent.NoteType == 9) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Train Name: " + travelEvent.Name;
                        }
                        else if (travelEvent.NoteType == 10) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Train Name: " + travelEvent.Name;
                        }
                        else if (travelEvent.NoteType == 11) {
                            html += " Arrival @ " + GetDateDisplay(travelEvent.ArrivalDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Bus Name: " + travelEvent.Name;
                        }
                        else if (travelEvent.NoteType == 12) {
                            html += " Departure @ " + GetDateDisplay(travelEvent.DepartureDateTime);
                            html += "<br/>";
                            html += " Terminal Station: " + travelEvent.TerminalStation;
                            html += "<br/>";
                            html += " Bus Name: " + travelEvent.Name;
						}
						else if (travelEvent.NoteType == 13) {
							html += " Self Checkin @ " + GetDateDisplay(travelEvent.Checkin);
							html += "<br/>";
							html += " Contact Person: " + travelEvent.ContactPerson;
						}
						else if (travelEvent.NoteType == 14) {
							html += " Self Checkout @ " + GetDateDisplay(travelEvent.Checkout);
							html += "<br/>";
							html += " Contact Person: " + travelEvent.ContactPerson;

						}
                        html += "</div>";
                    }
                    html += "</div>";
                    html += "</div>";
                }
                html += "</div>";
				html += "<br/>";
				html += "<div class=\"task-story-container " + (data[e].CheckOutWorkers.length== 0 ? 'no-worker-turnover' : '') + "\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='workers'>";
				html += "<label class=\"task-story-label\">Departure Meeting</label>";
				if (data[e].CheckOutWorkers.length > 0) {
					var checkoutWorkerCount= data[e].CheckOutWorkers.length;
					for (var i = 0; i < checkoutWorkerCount; i++) {
						html += "<div>Worker Name : " + data[e].CheckOutWorkers[i].FirstName + "<span>&nbsp;</span>" + data[e].CheckOutWorkers[i].LastName + "</div>";
						html += "<div>Check-In Date : " + data[e].CheckOutWorkers[i].StartDate + "</div>";
						html += "<div>Check-Out Date : " + data[e].CheckOutWorkers[i].EndDate + "</div>";
						html += "<div>Worker Contact : " + data[e].CheckOutWorkers[i].Mobile + "</div>";
						html += "<hr>";
					}
                }
                else {
                    html += "<div>No Worker Assigned</div>";
                }
                html += "</div>";
                html += "<br/>";
                var checkOutDate = GetShortDate(data[e].Inquiry.CheckOutDate);
				// onclick=" + (data[e].PostCheckOutTurnoverWorker == null ? 'showTaskWorkerAssignmentModal(' + data[e].Inquiry.InquiryId + ',"' + checkOutDate + '",' + 1 + ',' + 2 + ');' : '') + "
				html += "<div class=\"task-story-container " + (data[e].PostCheckOutTurnoverWorkers.length == 0 ? 'no-worker-turnover' : '') + "\" data-thread-id='" + data[e].Inquiry.ThreadId + "' data-label='workers'>";
				html += "<label class=\"task-story-label\">Departure Cleaner/s</label>";
				if (data[e].PostCheckOutTurnoverWorkers.length > 0) {
					var checkoutWorkerTurnoverCount = data[e].PostCheckOutTurnoverWorkers.length;
					for (var i = 0; i < checkoutWorkerTurnoverCount; i++) {
						html += "<div>Worker Name : " + data[e].PostCheckOutTurnoverWorkers[i].FirstName + "<span>&nbsp;</span>" + data[e].PostCheckOutTurnoverWorkers[i].LastName + "</div>";
						html += "<div>Start Date : " + GetDateDisplay(data[e].PostCheckOutTurnoverWorkers[i].StartDate) + "</div>";
						html += "<div>End Date : " + GetDateDisplay(data[e].PostCheckOutTurnoverWorkers[i].EndDate) + "</div>";
						html += "<div>Worker Contact : " + data[e].PostCheckOutTurnoverWorkers[i].Mobile + "</div>";
						html += "<hr>";
					}
                }
                else {
                    html += "<div>No Worker Assigned</div>";
                }
                html += "</div>";
                html += "</td>";
                /*END Trip Story Column*/

                /*START Task Status Column*/
				html += "<td class=\"content-top-col\">";
				if (true) {
                    html += "<table class='task-template-table'>";

                    //START: INQUIRY TASK TEMPLATE TYPE
                    if (data[e].InquiryTask.length >= 1) {
                        var inquiryTasks = data[e].InquiryTask;
                        html += "<tr><td><div class='task-template-label'>Inquiry Tasks</div></td></tr>";
                        for (var index = 0; index < inquiryTasks.length; index++) {
                            var object = inquiryTasks[index];
							html += "<tr>";
							html += "<td>";
							html += "<div class=\"ui checkbox\">";
							html += object.Status ? "<input data-task-id=\"" + object.Id + "\" class=\"task-status\" type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
							html += "<label class=\"task-status-label\">" + object.Title + '-' + object.Description + "</label>";
							html += "</div></td><td>";
							html += '<div class="fields">';
							html += '<div class="one width field">';
							html += '<img src="' + (object.ImageUrl != null ? object.ImageUrl : "/Images/Task/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
							html += '</div>';
							html += '</div>';

			
							html += "</td><td></td>";
							html += "</tr>";
							var subcount = object.SubTasks.length;
							var subtask = object.SubTasks;
							for (var x = 0; x < subcount; x++) {
								html += '<tr>';
								html += '<td></td><td>';
								html += '<div class="ui checkbox" style="float:left">';

								if (subtask[x].Status == 1) {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public" checked="checked">';
								}
								else {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public">';
								}
								html += '<label class="task-status-label">' + subtask[x].Title + '-' + subtask[x].Description + '</label>';

								html += '</div>';
								html += '</td>';
								html += '<td>';
								html += '<div class="fields">';
								html += '<div class="one width field">';
								html += '<img src="' + (subtask[x].ImageUrl != null ? subtask[x].ImageUrl : "/Images/SubTask/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
								html += '</div>';
								html += '</div>';
								html += '</td>'
								html += '<td>'
								html += '<h6> Update by:' + (subtask[x].UpdateBy != null ? subtask[x].UpdateBy + "-" + moment(subtask[x].LastUpdateDate).tz("America/Vancouver").format("MM/DD/YYYY hh:mm A"): "")+'</h6>'
								html +=  '</td>';
								html += '</tr>';
							}

                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Inquiry Tasks</td>";
                        html += "</tr>";
                    }
                    //END: INQUIRY TASK TEMPLATE TYPE;

                    //START: BOOKING TASK TEMPLATE TYPE
                    if (data[e].BookingTask.length >= 1) {
                        var bookingTasks = data[e].BookingTask;
                        html += "<tr><td><div class='task-template-label'>Booking Tasks</div></td></tr>";
                        for (var index = 0; index < bookingTasks.length; index++) {
                            var object = bookingTasks[index];
							html += "<tr>";
							html += "<td>";
							html += "<div class=\"ui checkbox\">";
							html += object.Status ? "<input data-task-id=\"" + object.Id + "\" class=\"task-status\" type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
							html += "<label class=\"task-status-label\">" + object.Title + '-' + object.Description + "</label>";
							html += "</div></td><td>";
							html += '<div class="fields">';
							html += '<div class="one width field">';
							html += '<img src="' + (object.ImageUrl != null ? object.ImageUrl : "/Images/Task/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
							html += '</div>';
							html += '</div>';


							html += "</td><td></td>";
							html += "</tr>";
							var subcount = object.SubTasks.length;
							var subtask = object.SubTasks;
							for (var x = 0; x < subcount; x++) {
								html += '<tr>';
								html += '<td></td><td>';
								html += '<div class="ui checkbox" style="float:left">';

								if (subtask[x].Status == 1) {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public" checked="checked">';
								}
								else {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public">';
								}
								html += '<label class="task-status-label">' + subtask[x].Title + '-' + subtask[x].Description + '</label>';

								html += '</div>';
								html += '</td>';
								html += '<td>';
								html += '<div class="fields">';
								html += '<div class="one width field">';
								html += '<img src="' + (subtask[x].ImageUrl != null ? subtask[x].ImageUrl : "/Images/SubTask/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
								html += '</div>';
								html += '</div>';
								html += '</td>';
								html += '<td>'
								html += '<h6> Update by:' + (subtask[x].UpdateBy != null ? subtask[x].UpdateBy + "-" + moment(subtask[x].LastUpdateDate).tz("America/Vancouver").format("MM/DD/YYYY hh:mm A") : "") + '</h6>'
								html += '</td>';
								html += '</tr>';
							}

                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Booking Tasks</td>";
                        html += "</tr>";
                    }
                    //END: BOOKING TASK TEMPLATE TYPE;

                    //START: CHECK-IN TASK TEMPLATE TYPE
                    if (data[e].CheckInTask.length >= 1) {
						var checkInTasks = data[e].CheckInTask;
						console.log(checkInTasks);
                        html += "<tr><td><div class='task-template-label'>Check-In Tasks</div></td></tr>";
                        for (var index = 0; index < checkInTasks.length; index++) {
                            var object = checkInTasks[index];
							html += "<tr>";
							html += "<td>";
							html += "<div class=\"ui checkbox\">";
							html += object.Status ? "<input data-task-id=\"" + object.Id + "\" class=\"task-status\" type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
							html += "<label class=\"task-status-label\">" + object.Title + '-' + object.Description + "</label>";
							html += "</div></td><td>";
							html += '<div class="fields">';
							html += '<div class="one width field">';
							html += '<img src="' + (object.ImageUrl != null ? object.ImageUrl : "/Images/Task/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
							html += '</div>';
							html += '</div>';


							html += "</td><td></td>";
							html += "</tr>";
							var subcount = object.SubTasks.length;
							var subtask = object.SubTasks;
							for (var x = 0; x < subcount; x++) {
								html += '<tr>';
								html += '<td></td><td>';
								html += '<div class="ui checkbox" style="float:left">';

								if (subtask[x].Status == 1) {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public" checked="checked">';
								}
								else {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public">';
								}
								html += '<label class="task-status-label">' + subtask[x].Title + '-' + subtask[x].Description + '</label>';

								html += '</div>';
								html += '</td>';
								html += '<td>';
								html += '<div class="fields">';
								html += '<div class="one width field">';
								html += '<img src="' + (subtask[x].ImageUrl != null ? subtask[x].ImageUrl : "/Images/SubTask/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
								html += '</div>';
								html += '</div>';
								html += '</td>';
								html += '<td>'
								html += '<h6> Update by:' + (subtask[x].UpdateBy != null ? subtask[x].UpdateBy + "-" + moment(subtask[x].LastUpdateDate).tz("America/Vancouver").format("MM/DD/YYYY hh:mm A") : "") + '</h6>'
								html += '</td>';
								html += '</tr>';
							}
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Check-In Tasks</td>";
                        html += "</tr>";
                    }
                    //END: CHECK-IN TASK TEMPLATE TYPE;

                    // Add by Danial - 7/2/2018
                    //START: CHECK-OUT TASK TEMPLATE TYPE
                    if (data[e].CheckOutTask.length >= 1) {
                        var checkOutTasks = data[e].CheckOutTask;
                        html += "<tr><td><div class='task-template-label'>Check-Out Tasks</div></td></tr>";
                        for (var index = 0; index < checkOutTasks.length; index++) {
							var object = checkOutTasks[index];
							html += "<tr>";
							html += "<td>";
							html += "<div class=\"ui checkbox\">";
							html += object.Status ? "<input data-task-id=\"" + object.Id + "\" class=\"task-status\" type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
							html += "<label class=\"task-status-label\">" + object.Title + '-' + object.Description + "</label>";
							html += "</div></td><td>";
							html += '<div class="fields">';
							html += '<div class="one width field">';
							html += '<img src="' + (object.ImageUrl != null ? object.ImageUrl : "/Images/Task/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
							html += '</div>';
							html += '</div>';


							html += "</td><td></td>";
							html += "</tr>";
							var subcount = object.SubTasks.length;
							var subtask = object.SubTasks;
							for (var x = 0; x < subcount; x++) {
								html += '<tr>';
								html += '<td></td><td>';
								html += '<div class="ui checkbox" style="float:left">';

								if (subtask[x].Status == 1) {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public" checked="checked">';
								}
								else {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public">';
								}
								html += '<label class="task-status-label">' + subtask[x].Title + '-' + subtask[x].Description + '</label>';

								html += '</div>';
								html += '</td>';
								html += '<td>';
								html += '<div class="fields">';
								html += '<div class="one width field">';
								html += '<img src="' + (subtask[x].ImageUrl != null ? subtask[x].ImageUrl : "/Images/SubTask/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
								html += '</div>';
								html += '</div>';
								html += '</td>';
								html += '<td>'
								html += '<h6> Update by:' + (subtask[x].UpdateBy != null ? subtask[x].UpdateBy + "-" + moment(subtask[x].LastUpdateDate).tz("America/Vancouver").format("MM/DD/YYYY hh:mm A") : "") + '</h6>'
								html += '</td>';
								html += '</tr>';
							}
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Check-Out Tasks</td>";
                        html += "</tr>";
                    }
                    //END: CHECK-OUT TASK TEMPLATE TYPE;

                    //START: TURNOVER TASK TEMPLATE TYPE
                    if (data[e].TurnoverTask.length >= 1) {
                        var turnoverTasks = data[e].TurnoverTask;
                        html += "<tr><td><div class='task-template-label'>Turnover Tasks</div></td></tr>";
                        for (var index = 0; index < turnoverTasks.length; index++) {
                            var object = turnoverTasks[index];
							html += "<tr>";
							html += "<td>";
							html += "<div class=\"ui checkbox\">";
							html += object.Status ? "<input data-task-id=\"" + object.Id + "\" class=\"task-status\" type=\"checkbox\" name=\"public\" checked=\"checked\">" : "<input type=\"checkbox\" name=\"public\">";
							html += "<label class=\"task-status-label\">" + object.Title + '-' + object.Description + "</label>";
							html += "</div></td><td>";
							html += '<div class="fields">';
							html += '<div class="one width field">';
							html += '<img src="' + (object.ImageUrl != null ? object.ImageUrl : "/Images/Task/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
							html += '</div>';
							html += '</div>';


							html += "</td><td></td>";
							html += "</tr>";
							var subcount = object.SubTasks.length;
							var subtask = object.SubTasks;
							for (var x = 0; x < subcount; x++) {
								html += '<tr>';
								html += '<td></td><td>';
								html += '<div class="ui checkbox" style="float:left">';

								if (subtask[x].Status == 1) {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public" checked="checked">';
								}
								else {
									html += '<input class="subtask-status" data-subtask-id="' + subtask[x].Id + '" type="checkbox" name="public">';
								}
								html += '<label class="task-status-label">' + subtask[x].Title + '-' + subtask[x].Description + '</label>';

								html += '</div>';
								html += '</td>';
								html += '<td>';
								html += '<div class="fields">';
								html += '<div class="one width field">';
								html += '<img src="' + (subtask[x].ImageUrl != null ? subtask[x].ImageUrl : "/Images/SubTask/default-image.png") + '" class="ui tiny circular image" alt="Image" />';
								html += '</div>';
								html += '</div>';
								html += '</td>';
								html += '<td>'
								html += '<h6> Update by:' + (subtask[x].UpdateBy != null ? subtask[x].UpdateBy + "-" + smoment(subtask[x].LastUpdateDate).tz("America/Vancouver").format("MM/DD/YYYY hh:mm A") : "") + '</h6>'
								html += '</td>';
								html += '</tr>';
							}
                        }
                    } else {
                        html += "<tr>";
                        html += "<td class='empty-tasks'>No Turnover Tasks</td>";
                        html += "</tr>";
                    }
                    //END: TURNOVER TASK TEMPLATE TYPE;
                    html += "</table>";
                }
                else {
                    html += "<div>No Task</div>";
                }
                html += "</td>";
                /*END Task Status Column*/
                html += "</tr>";
                html += "</tbody>";
                html += "</table>";
                html += "</div>";
                html += "</div>";
                html += "</td>";
                html += "</tr>";
            }
            $taskDashboardTable.append(html);
            $('.ui.accordion').accordion();
        }
        else {
            var emptyHtml = "";
            emptyHtml += "<tr><td class='empty-records' colspan='11'>No Records Found</td></tr>";
            $taskDashboardTable.append(emptyHtml);
        }
    }

    /*Add by Danial - 13/2/2018 */
    function getTaskCompletedCount(taskList) {
        var count = 0;

        for (var j = 0; j < taskList.length; j++) {
            if (taskList[j].Status) {
                count++;
            }
        }

        return count;
    }

    //Add by Danial 22-2-2018
    function onAssignAvailableWorker() {
        $(document).off('change', 'input.assign-worker');
        $(document).on('change', 'input.assign-worker', function (e) {
            $('input.assign-worker').not(this).prop('checked', false);
            $('.assign-worker').removeClass('disabled');
            if ($('input.assign-worker:checked').length == 0) {
                $('.assign-worker').addClass('disabled');
            }
        });
    }

    //Add by Danial 22-2-2018
    function onSubmitWorkerAssignment() {
        $(document).off('click', '#task-worker-assignment-form-submit-btn');
        $(document).on('click', '#task-worker-assignment-form-submit-btn', function (e) {

            var workerAssignmentRowId = $('input.assign-worker:checked').attr('data-worker-assignment-id');
            var workerAssignmentType = $('input.assign-worker:checked').attr('data-worker-assignment-type');

            $.ajax({
                type: 'POST',
                url: "/Task/UpdateWorkerAssignmentStatus",
                data: { rowId: workerAssignmentRowId, workerAssignmentType: workerAssignmentType },
                async: true,
                success: function (response) {

                    if (response != null && response.success) {
                        swal("Worker is successfully assigned", "", "success");
                        $("#task-search-btn").click();
                    }
                    else {
                        swal("Worker failed to assigned", "", "error");
                    }

                    $('.task-worker-assignment-modal').modal('hide');
                },
                error: function (error) {
                    swal("Error connecting to server.", "", "error");
                }
            });
        });
    }

    function onTripStoryClicked() {
        $(document).on("click", ".task-story-container", function () {
            var threadId = $(this).attr("data-thread-id");
            var page = $(this).attr("data-label");

            quickActionModal({
                threadId: threadId,
                page: page
            });
        });
    }
});

function showTaskWorkerAssignmentModal(inquiryId, dateSearch, category, workerAssignmentType) {
    $("#workerList > tbody").html("");
    $.ajax({
        type: 'GET',
        url: "/Task/GetAvailableWorkers",
        data: { inquiryId: inquiryId, dateSearch: dateSearch, workerCategory: category, workerAssignmentType: workerAssignmentType },
        async: true,
        success: function (workerList) {

            if (workerList != null && workerList.length > 0) {
                var htmlRow = "";
                for (var index = 0; index < workerList.length; index++) {
                    htmlRow += "<tr>";
                    htmlRow += "<td>" + workerList[index].FirstName + "</td>";
                    htmlRow += "<td>" + workerList[index].LastName + "</td>";
                    htmlRow += "<td>" + workerList[index].Mobile + "</td>";
                    htmlRow += "<td>" + GetDateDisplay(workerList[index].StartDate) + "</td>";
                    htmlRow += "<td>" + GetDateDisplay(workerList[index].EndDate) + "</td>";
                    htmlRow += "<td>";
                    htmlRow += "<div class=\"ui toggle checkbox\">";
                    htmlRow += "<input data-worker-assignment-type='" + workerAssignmentType + "' data-worker-assignment-id='" + workerList[index].Id + "' type=\"checkbox\" class='assign-worker'>";
                    htmlRow += "</div>";
                    htmlRow += "</td>";
                    htmlRow += "</tr>";
                }

                $("#workerList > tbody").append(htmlRow);
                $('.ui.checkbox').checkbox();
            }
            else {
                var emptyHtml = "<tr><td colspan='6' class='empty-records'>No Workers Available</td></tr>";

                $("#workerList > tbody").append(emptyHtml);
            }

            $('.task-worker-assignment-modal').modal('show');
        }
    });
}

//Utilities method - add by Danial 21-2-2018
function GetDateDisplay(jsonDate) {
	try {
		return moment(jsonDate).tz("America/Vancouver").format('MMM D, YYYY h:mmA (dddd)');
        //var dateValue = jsonDate.replace("/Date(", "").replace(")/", "");
        //var dateObject = new Date(parseInt(dateValue));

        //return (dateObject.getMonth() + 1) + '/' + dateObject.getDate() + '/' + dateObject.getFullYear() + " " + FormatAMPM(dateObject);
    }
    catch (err) {
        return "";
    }
}

function GetShortDate(jsonDate) {
	return moment(jsonDate).tz("America/Vancouver").format('MM/D/YYYY');
    //var dateValue = jsonDate.replace("/Date(", "").replace(")/", "");
    //var dateObject = new Date(parseInt(dateValue));

    //return (dateObject.getMonth() + 1) + '/' + dateObject.getDate() + '/' + dateObject.getFullYear();
}


