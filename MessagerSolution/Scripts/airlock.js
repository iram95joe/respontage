﻿$(document).ready(function () {

    var myHub = $.connection.airlockHub;
    $.connection.hub.logging = true;

    myHub.client.DisplayAirlock = function (phoneNumbers) {
        var selectPhoneNumberHtml = "";

        for (var i = 0; i < phoneNumbers.length; i++) {
            selectPhoneNumberHtml = selectPhoneNumberHtml + 
                '<div class="field">' +
                    '<div class="ui radio checkbox">' +
                        '<input type="radio" name="example2" checked="checked" selectionId="' + (i + 1) + '">' +
                        '<label>' + phoneNumbers[i] + '</label>' +
                    '</div>' +
                '</div>' +
                '<br />';
        }

        $("#vrbo-airlock-phone-numbers").append(selectPhoneNumberHtml);
        $(".vrbo-airlock").modal('show');
    };

    myHub.client.ShowEnterCode = function () {
        $("#vrbo-airlock-step1").hide();
        $("#vrbo-airlock-action1").hide();

        $("#vrbo-airlock-step2").show();
        $("#vrbo-airlock-action2").show();

        $("#vrbo-airlock-sms-btn").removeClass("loading");
    }

    myHub.client.VRBOAirlockResult = function (success) {
        if (success) {
            $(".vrbo-airlock").modal('hide');
        }
        else {
            $("#vrbo-airlock-error-message").show();
        }

        $("#vrbo-airlock-verify-btn").removeClass("loading");
    }

    $.connection.hub.start();

    $(".vrbo-airlock").modal({
        closable: false
    });

    $("#vrbo-airlock-error-message").hide();

    $("#vrbo-airlock-sms-btn").click(function () {
        var selectedPhone = $("input[type=radio]:checked").attr("selectionId");
        $(this).addClass("loading");

        myHub.server.processAirlock(selectedPhone, 1);
    });

    $("#vrbo-airlock-call-btn").click(function () {
        var selectedPhone = $("input[type=radio]:checked").attr("selectionId");
        $(this).addClass("loading");

        myHub.server.processAirlock(selectedPhone, 2);
    });

    $("#vrbo-airlock-verify-btn").click(function () {
        var verificationCode = $("#vrbo-airlock-verfication-tb").val();
        $("#vrbo-airlock-error-message").hide();
        $(this).addClass("loading");

        myHub.server.verifyCode(verificationCode);
    });

    $("#vrbo-airlock-back-btn").click(function () {
        $("#vrbo-airlock-step2").hide();
        $("#vrbo-airlock-action2").hide();

        $("#vrbo-airlock-step1").show();
        $("#vrbo-airlock-action1").show();
    });
});