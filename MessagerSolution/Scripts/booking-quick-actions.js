﻿
var quickModal = {
	title: 'Quick Action',
	threadId: 0,
	inquiryId: 0,
	selectedBooking: null,
	booking: null,
	booking_status: '',
	page: '',
	workerTable: null,
	workerAssignmentTable: null,
	devices: null,
	cctvFootageNotes: null,
	hostReviews: null,
	guestReviews: null,
	pincode: null,
	hasVideo: false
};

var activeTabMenu = "";
var assignId = 0;
var UserWorkerId=0;
//Get the booking action for Inquiry
function getInquiryAction()
{
	if (quickModal.selectedBooking.SiteType == 2) {
		return '<div class="three mini buttons">' +
			((quickModal.selectedBooking.SiteType == 2 && (quickModal.selectedBooking.Status == 'I' || quickModal.selectedBooking.Status == 'PAYMENT_REQUEST_SENT'))
				? ((quickModal.selectedBooking.Status == 'I')
					? '<button style="margin: 1%" class="ui blue button js-btn_pre-approve" onClick="onPreApproveInquiry()">Pre-Approve</button>'
					: '')
				+ '<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="alterBooking()">Send Special Offer</button>'
				: '') +
			((quickModal.selectedBooking.SiteType == 2 && quickModal.selectedBooking.Status == 'PAYMENT_REQUEST_SENT')
				? '<button style="margin: 1%" class="ui blue button js-btn_cancel-reservation" onClick="onDeclineInquiry()">Cancel Pre-Approval</button>'
				: ''
			) +
		'</div>';

	}
	else if (quickModal.selectedBooking.SiteType == 4) {
	    return '<div class="three mini buttons">' +
			((quickModal.selectedBooking.SiteType == 4 && (quickModal.selectedBooking.Status == 'I' || quickModal.selectedBooking.Status == 'PAYMENT_REQUEST_SENT'))
				? ((quickModal.selectedBooking.Status == 'I')
					? '<button style="margin: 1%" class="ui blue button js-btn_pre-approve" onClick="onPreApproveInquiry()">Pre-Approve</button>'
					: '')
				+ '<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="alterBooking()">Send Special Offer</button>'
				: '') +
			((quickModal.selectedBooking.SiteType == 4 && quickModal.selectedBooking.Status == 'PAYMENT_REQUEST_SENT')
				? '<button style="margin: 1%" class="ui blue button js-btn_cancel-reservation" onClick="onDeclineInquiry()">Cancel Pre-Approval</button>'
				: ''
			) +
		'</div>';

	}
	else if (quickModal.selectedBooking.SiteType == 1) {


		return '<div class="three mini buttons">' +
			//((quickModal.selectedBooking.CanPreApproveInquiry && quickModal.selectedBooking.CanWithdrawPreApprovalInquiry == false) ? '<button style="margin: 1%" class="ui red button js-btn_pre-approve">Pre-Approve</button>' : ((quickModal.selectedBooking.CanPreApproveInquiry == false && quickModal.selectedBooking.CanWithdrawPreApprovalInquiry) ? '<button style="margin: 1%" class="ui red button js-btn_withdraw-pre-approve">Withdraw Pre-Approve</button>' : '')) +
			//(quickModal.selectedBooking.CanDeclineInquiry ? '<button style="margin: 1%" class="ui button js-btn_decline-inquiry">Decline</button>' : '')

			((quickModal.selectedBooking.Status == 'I')
				? '<button style="margin: 1%" class="ui blue button js-btn_pre-approve" onClick="onPreApproveInquiry()">Pre-Approve</button>'
				: '') 
			+ (quickModal.selectedBooking.IsSpecialOfferSent ? '<button style="margin: 1%" class="ui blue button js-btn_withdraw-special-offer" onClick ="onWithdrawSpecialOffer()">Withdraw Special Offer</button>' : '<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="onSpecialOffer()">Send Special Offer</button>')
			+ '<button style="margin: 1%" class="ui blue button js-btn_cancel-reservation" onClick="onDeclineInquiry()">Decline</button>'
			+ '</div>';
	}
}

//Get booking action if the status is booking (B)
function getBookingAction() {
    return '<div class="two mini buttons">' +
                '<button style="margin: 1%" class="ui blue button js-btn_decline-booking" onClick="declineBooking()">Decline</button>' +
                '<button style="margin: 1%" class="ui blue button js-btn_accept-booking" onClick="acceptBooking()">Accept</button>' +
		(quickModal.selectedBooking.SiteType == 2 ? (quickModal.selectedBooking.IsSpecialOfferSent ? '<button style="margin: 1%" class="ui blue button js-btn_withdraw-special-offer">Withdraw Special Offer</button>' : '<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="alterBooking()">Send Special Offer</button>') : '') +
		(quickModal.selectedBooking.SiteType == 1 ? (quickModal.selectedBooking.IsSpecialOfferSent ? '<button style="margin: 1%" class="ui blue button js-btn_withdraw-special-offer" onClick="onWithdrawSpecialOffer()">Withdraw Special Offer</button>' : '<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="onSpecialOffer()">Send Special Offer</button>') : '')+
		'</div>';
}

var bookingAction = '<div class="two mini buttons">' +
                        '<button style="margin: 1%" class="ui blue button js-btn_decline-booking" onClick="declineBooking()">Decline</button>' +
                        '<button style="margin: 1%" class="ui blue button js-btn_accept-booking" onClick="acceptBooking()">Accept</button>' +
                    '</div>';

var tripCoordinationAction = '<div class="two mini buttons">' +
								'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="declineBooking()">Cancel Reservation</button>' +
                                '<button style="margin: 1%" class="ui blue button js-btn_change-booking" onClick="alterBooking()">Alter Reservation</button>' +
                            '</div>';

var tripCoordinationAlteredAction = '<div class="two mini buttons">' +
										'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="declineBooking()">Cancel Reservation</button>' +
                                        '<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="cancelAlteration()">Cancel Alteration</button>' +
                                    '</div>';


var requestCancelBookingForm = '<div class="ui message request-cancel-booking">' +

	'<div class="content">' +
	'<div class="header"><h5>By using this feature, you are giving your guest the option to cancel for free. This reservation will remain valid until the guest confirms.</h5></div><br>' +
	'</div>' +

	'<div class="extra content">' +
	'<div class="field">' +
	'<div class="ui radio checkbox">' +
	'<input name="example2" type="radio" checked onclick="requestCancelBookingSelection(1)">' +
	'<label>Payment was not received</label>' +
	'</div>' +
	'</div>' +
	'<br/>' +
	'<div class="field">' +
	'<div class="ui radio checkbox">' +
	'<input name="example2" type="radio" onclick="requestCancelBookingSelection(2)">' +
	'<label>Guest has requested cancellation</label>' +
	'</div>' +
	'</div>' +
	'</div>' +

	'<br/>' +

	'<div class="content" id="request-cancel-booking-1">' +
	'<div class="header"><h5>Having trouble charging your guest? Close this and mark the credit card as invalid. </h5></div>' +
	'</div>' +

	'<div class="content" id="request-cancel-booking-2" hidden>' +
	'<div class="header"><h5>By sending this request, I agree to waive the cancellation fee (if any).</h5></div>' +
	'</div>' +

	'</div>' +

	'<div style="text-align: right" class="actions">' +
	'<button onClick="onAlterFormClose()" class="ui blue button">Back</button>' +
	'<button onClick="declineBookingSubmit()" id="request-cancel-booking-btn" class="ui blue button" disabled>Send</button>' +
	'</div>';


function markCreditCardAsInvalidForm(confirmationCode, guestName) {
	return (
		'<div class="ui message grid mark-credit-card-as-invalid">' +

		'<div class="row">' +
		'<div class="four wide field">Reservation number : </div>' +
		'<div class="eight wide field"><h5>' + confirmationCode + '</h5></div>' +
		'</div>' +

		'<div class="row">' +
		'<div class="four wide field">Booker name : </div>' +
		'<div class="eight wide field"><h5>' + guestName + '</h5></div>' +
		'</div>' +

		'<div class="row">' +
		'Confirm credit card number (last 4 digits) :' +
		'</div>' +

		'<div class="row">' +
		'<div class="ui labeled input">' +
		'<div class="ui label">xxxx-xxxx-xxxx-</div>' +
		'<input type="text" id="card-4-digit" maxlength="4">' +
		'</div>' +
		'</div>' +

		'<div class="ui info message">' +
		'You are about to report this reservation as having an invalid credit card. ' +
		'</div>' +

		'<div class="row">' +
		'Why are you reporting this credit card as invalid? ' +
		'</div>' +

		'<select class="ui search dropdown" id="cc-reason" name="cc_invalid_reason" onchange="invalidCreditCardReasonChanged(this.value)">' +
		'<option id="declined" value="declined">transaction declined / not permitted </option>' +
		'<option id="no_credit" value="no_credit">insufficient funds / credit </option>' +
		'<option id="expired" value="expired">expiration date before arrival date </option>' +
		'<option id="invalid_nr" value="invalid_nr">card number invalid </option>' +
		'<option id="call_company" value="call_company">message: "call card company" </option>' +
		'<option id="debit_card" value="debit_card">debit card</option>' +
		'<option id="fraud" value="fraud">fraudulent reservation </option>' +
		'<option id="other" value="other" selected>other, please specify: </option>' +
		'</select>' +

		'<div class="row" id="mark-credit-card-as-invalid-other-reason" hidden>' +

		'<div class="ui form">' +
		'<div class="field">' +
		'<label>Specify here :</label>' +
		'<textarea row="1" id="other-reason" placeholder="other reason, specify here..."></textarea>' +
		'</div>' +
		'</div>' +

		'</div>' +

		'</div>' +

		'<div style="text-align: right" class="actions">' +
		'<button onClick="onAlterFormClose()" class="ui blue button">Back</button>' +
		'<button onClick="markCreditCardAsInvalidSubmit()" id="mark-credit-card-as-invalid-btn" class="ui blue button">Confirm</button>' +
		'</div>');
}

function markCreditCardAsInvalidSubmit() {
	var hostId = quickModal.booking.HostId;
	var reservationCode = quickModal.selectedBooking.ConfirmationCode;
	var reason = $("#other-reason").val();
	var reasonId = $("#cc-reason").val();
	var card4Digit = $("#card-4-digit").val();
	var listingId = quickModal.selectedBooking.PropertyId;
	debugger;

	$.ajax({
		type: 'POST',
		url: "/Booking/MarkCreditCardAsInvalid",
		data: {
			hostId: hostId,
			listingid: listingId,
			reservationId: reservationCode,
			reason: reason,
			reasonId: reasonId,
			card4Digit: card4Digit
		},
		success: function (result) {
			if (result.success) {
				$('.inquiry-error-message-wrapper').hide();
				$('.inquiry-success-message-wrapper').find('.header').text(result.message);
				$('.inquiry-success-message-wrapper').show();
				$('.inquiry-success-message-wrapper').removeClass('hidden');
				$('#booking-action-form').empty();
				$('#booking-actions').empty();
				$('#booking-actions').show();
				$('#trip-info-details').show();
				$('#reservation-status').text('Booking-Cancelled');
				quickModal.selectedBooking.Status = "BC";
			}
			else {
				$('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
				$('.inquiry-error-message-wrapper').find('.header').text(result.message);
				$('.inquiry-error-message-wrapper').show();
				$('.inquiry-error-message-wrapper').removeClass('hidden');
			}
		}
	});
}
function requestCancelBookingSelection(type) {
	if (type == 1) {
		$("#request-cancel-booking-1").show();
		$("#request-cancel-booking-2").hide();
		$("#request-cancel-booking-btn").prop("disabled", true);
	}
	else if (type == 2) {
		$("#request-cancel-booking-2").show();
		$("#request-cancel-booking-1").hide();
		$("#request-cancel-booking-btn").prop("disabled", false);
	}
}
function SearchPeople() {
	$(document).on('click', '#search-people-btn', function () {
		$('#possible-person').empty();
		$('#possible-person').append('<div class="ui segment reviews" style="width: 100%;height:70px">' +
			'<div class="ui active inverted dimmer">' +
			'<div class="ui text loader">Searching Data</div>' +
			'</div>' +
			'<p></p>' +
			'</div>');
		$.ajax({
			type: 'GET',
			url: "/SearchPeople/Search",
			async:true,
			data: { firstname: $('#search-firstname').val(), lastname: $('#search-lastname').val(), city: $('#search-address').val(), age: $('#search-age').val(), email: $('#search-email').val() },
			success: function (result) {
				console.log(result.data);
				var data = result.data;
				if (data.length > 0) {
					$('#possible-person').empty();
					for (var x = 0; x < data.length; x++) {

						var recordHtml = "";
						var record = data[x].CriminalRecord;
						for (var q = 0; q < record.length; q++) {
							recordHtml += record[q] + '<br/>';
						}
						var address = data[x].Address;
						var addressHtml = "";
						for (var q = 0; q < address.length; q++) {
							addressHtml += address[q] + '<br/>';
						}

						var relativeHtml = "";
						if (data[x].Relatives.length > 0) {
							for (var q = 0; q < data[x].Relatives.length; q++) {
								relativeHtml += data[x].Relatives[q] + '<br/>';
							}
						}
						var neighborsHtml = "";
						if (data[x].Relatives.length > 0) {
							for (var q = 0; q < data[x].Neighbors.length; q++) {
								neighborsHtml += data[x].Neighbors[q] + '<br/>';
							}
						}

						var nameHtml = "";
						if (data[x].Name != null) {
							var names = data[x].Name.split(",");
							for (var q = 0; q < names.length; q++) {
								nameHtml += names[q] + '<br/>';
							}
						}

						var socialHtml = "";
						if (data[x].SocialMedia != null) {
							var social = data[x].SocialMedia.split(",");
							for (var q = 0; q < social.length; q++) {
								socialHtml += social[q] + '<br/>';
							}
						}

						var emailHtml = "";
						for (var q = 0; q < data[x].Emails.length; q++) {
							emailHtml += data[x].Emails[q] + '<br/>';
						}
						var phoneHtml = "";
						for (var q = 0; q < data[x].PhoneNumber.length; q++) {
							phoneHtml += data[x].PhoneNumber[q] + '<br/>';
						}

						var html = '<div class="ui segment reviews" style="width: 100%;">' +
							'<div class="ui stackable three column grid">' +
							'<div class="column">' +
							'<h3>Possible Name</h3>' +
							'<h5>' + nameHtml + '</h5>' +
							'<h3>Address</h3>' +
							'<h5 style="margin:0px"><i>' + addressHtml + '</i></h5>' +
							(data[x].BODate != null ? '<h3>Birthdate</h3>' + '<h5 style="margin:0px">Birthdate: <i>' + data[x].BODate + '</i></h5><h5 style="margin:0px">Age: <i>' + data[x].Age + '</i></h5>' : '') +
							'<h3>Social Media</h3>' +
							'<h5>' + socialHtml + '</h5>' +
							'</div>' +
							'<div class="column">' +
							'<h3>Court Record</h3>' +
							recordHtml +
							'<h3>Relatives</h3>' +
							'<h5 style="margin:0px"><i>' + relativeHtml + '</i></h5>' +
							'<h3>Neighbors</h3>' +
							'<h5 style="margin:0px"><i>' + neighborsHtml + '</i></h5>' +
							'</div>' +
							'<div class="column">' +
							'<h3>Email</h3>' +
							'<h5>' + emailHtml + '</h5>' +
							'<h3>Phone Number</h3>' +
							'<h5>' + phoneHtml + '</h5>' +
							'</div>' +
							'</div>' +
							'</div>';
						$('#possible-person').append(html);
					}
				}
				else {
					$('#possible-person').empty();
					$('#possible-person').append('<div class="ui segment reviews" style="width: 100%;height:70px">' +
						'<h3>No Record found</h3>' +
						'</div>');
				}
			}
		});
	});
} 
function requestCancelBooking() {
	if ($('#booking-action-form').children().length == 0) {
		$('#booking-action-form').append(requestCancelBookingForm);
		//onAlterBookingFormChange();
		$('#booking-actions-inquiry').hide();
		$('#trip-info-details').hide();
		$('#quick-action-modal').find('#success-message-wrapper').hide();
		$('#quick-action-modal').find('#error-message-wrapper').hide();
		$('#add-payment-request-form').find('#add-payment-request-success-message-wrapper').hide();
		$('#add-payment-request-form').find('#add-payment-request-error-message-wrapper').hide();
		$('#quick-action-modal').modal('refresh');

		$('#add-payment-request-date').calendar({
			type: 'date', onChange: function (date, text, mode) {
				$("#add-payment-request-form input[name=dueDate]").val(text);
			}
		});
	}
}

function markCreditCardAsInvalid(confirmationCode, guestName) {
	if ($('#booking-action-form').children().length == 0) {
		var asd = markCreditCardAsInvalidForm(confirmationCode, guestName);
		$('#booking-action-form').append(markCreditCardAsInvalidForm(confirmationCode, guestName));
		//onAlterBookingFormChange();
		$('#booking-actions-inquiry').hide();
		$('#trip-info-details').hide();
		$('#quick-action-modal').find('#success-message-wrapper').hide();
		$('#quick-action-modal').find('#error-message-wrapper').hide();
		$('#add-payment-request-form').find('#add-payment-request-success-message-wrapper').hide();
		$('#add-payment-request-form').find('#add-payment-request-error-message-wrapper').hide();
		$('#quick-action-modal').modal('refresh');

		$('#add-payment-request-date').calendar({
			type: 'date', onChange: function (date, text, mode) {
				$("#add-payment-request-form input[name=dueDate]").val(text);
			}
		});
	} 
}

function markAsNoShowSubmit() {
	var hostId = quickModal.booking.HostId;
	var reservationId = quickModal.selectedBooking.InquiryId;
	var reservationCode = quickModal.selectedBooking.ConfirmationCode;
	var listingId = quickModal.selectedBooking.PropertyId;
	debugger;

	$.ajax({
		type: 'POST',
		url: "/Booking/MarkAsNoShow",
		data: {
			hostId: hostId,
			reservationId: reservationId,
			bookingReferenceNumber: reservationCode,
			listingId: listingId
		},
		success: function (result) {
			if (result.success) {
				$('.inquiry-error-message-wrapper').hide();
				$('.inquiry-success-message-wrapper').find('.header').text(result.message);
				$('.inquiry-success-message-wrapper').show();
				$('.inquiry-success-message-wrapper').removeClass('hidden');
				$('#booking-action-form').empty();
				$('#booking-actions').empty();
				$('#booking-actions').show();
				$('#trip-info-details').show();
				$('#reservation-status').text('Booking-Cancelled');
				quickModal.selectedBooking.Status = "BC";
			}
			else {
				$('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
				$('.inquiry-error-message-wrapper').find('.header').text(result.message);
				$('.inquiry-error-message-wrapper').show();
				$('.inquiry-error-message-wrapper').removeClass('hidden');
			}
		}
	});
}
function changeReservationDatesAndPrices1() {
	if ($('#booking-action-form').children().length == 0) {
		$('#booking-action-form').append(changeReservationDatesAndPricesForm1);


		$('#change-date-and-price-from').calendar({
			type: 'date'
		});

		$('#change-date-and-price-to').calendar({
			type: 'date'
		});

		//onAlterBookingFormChange();
		$('#booking-actions-inquiry').hide();
		$('#trip-info-details').hide();
		$('#quick-action-modal').find('#success-message-wrapper').hide();
		$('#quick-action-modal').find('#error-message-wrapper').hide();
		$('#add-payment-request-form').find('#add-payment-request-success-message-wrapper').hide();
		$('#add-payment-request-form').find('#add-payment-request-error-message-wrapper').hide();
		$('#quick-action-modal').modal('refresh');

		$('#add-payment-request-date').calendar({
			type: 'date', onChange: function (date, text, mode) {
				$("#add-payment-request-form input[name=dueDate]").val(text);
			}
		});
	}
}

function ConfirmedBookingAction(confirmationCode, guestName) {
    if (quickModal.selectedBooking.SiteType == 2 || quickModal.selectedBooking.SiteType == 4) {
		return '<div class="two mini buttons">' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="declineBooking()">Cancel Reservation</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="alterBooking()">Alter Reservation</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="sendRefund()">Send Refund</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="addPaymentRequest()">Add Payment Request</button>' +
			'</div>';
	}
	else if (quickModal.selectedBooking.SiteType == 1 && quickModal.selectedBooking.IsActive) {
		return '<div class="two mini buttons">' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="declineBooking()">Cancel Reservation</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="alterBooking()">Alter Reservation</button>' +
			'</div>';
	}
	else if (quickModal.selectedBooking.SiteType ==3) {
		return '<div class="two mini buttons">' +
			'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="requestCancelBooking()">Request to Cancel Reservation</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_invalid-credit-card" onClick="markCreditCardAsInvalid(\'' + confirmationCode + '\', \'' + guestName + '\')">Mark Credit Card as Invalid</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_alter-reservation" onClick="changeReservationDatesAndPrices1()">Change Date and Price</button>' +
			'<button style="margin: 1%" class="ui blue button js-btn_mark-as-no-show" onClick="markAsNoShowSubmit()">Mark as No Show</button>' +
			'</div>';
	}

	else return '';
}
//var airbnbConfirmedBookingAction = '<div class="two mini buttons">' +
//	'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration" onClick="cancelReservation()">Cancel Reservation</button>' +
//	'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking" onClick="alterBooking()">Alter Reservation</button>' +
//	'</div>';


var specialOfferForm = '<div class="ui message special-offer-msg">' +
                        '<div class="header">Send Special Offer</div><br>' +
                        '<div id="special-offer-form" class="ui form">' +
                        '<div id="success-message-wrapper" class="ui positive message">' +
                            '<div class="header"></div>' +
                            '<p></p>' +
                        '</div>' +
                        '<div id="error-message-wrapper" class="ui negative message">' +
                            '<p></p>' +
                        '</div>' +
                        '<input type="hidden" name="reservationCode" />' +
                        '<div class="fields">' +
                            '<div class="eight wide field">' +
                                '<div class="field">' +
                                    '<label>Property</label>' +
                                    '<select class="ui fluid dropdown" onChange="checkSpecialOffer()" name="propertyId">' +
                                        ($("meta[name=property_list_for_select]").attr('content')) +
                                    '</select>' +
                                '</div>' +
                            '</div>' +
                            '<div class="three wide field">' +
                                '<div class="field">' +
                                    '<label>Guests</label>' +
                                    '<select class="ui fluid dropdown" onChange="checkSpecialOffer()" name="guestCount">' +
                                        '<option value="1">1</option>' +
                                        '<option value="2">2</option>' +
                                        '<option value="3">3</option>' +
                                        '<option value="4">4</option>' +
                                        '<option value="5">5</option>' +
                                        '<option value="6">6</option>' +
                                        '<option value="7">7</option>' +
                                        '<option value="8">8</option>' +
                                        '<option value="9">9</option>' +
                                        '<option value="10">10</option>' +
                                    '</select>' +
                                '</div>' +
                            '</div>' +
                            '<div class="five wide field"></div>' +
                        '</div>' +
                        '<div class="fields">' +
                            '<div class="six wide field">' +
                                '<label>Check in</label>' +
                                '<div id="special-offer-start-date" class="ui calendar">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
                                        '<input type="text" name="startDate">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="six wide field">' +
                                '<label>Check out</label>' +
                                '<div id="special-offer-end-date" class="ui calendar">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
                                        '<input type="text" name="endDate">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="six wide field"></div>' +
                        '</div>' +
                        '<div id="special-offer-summary" class="ui attached segment">' +
                            '<div class="ui grid">' +
                                '<div class="row">' +
                                    '<div class="six wide column">' +
                                        '<div class="field">' +
                                            '<label>Subtotal</label>' +
                                            '<div class="ui input">' +
                                                '<input type="text" name="subTotal">' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="ten wide column">' +
                                        '<br>' +
                                        '<p>Enter a subtotal that includes any cleaning or extra guest fees. This won’t include service fees or applicable taxes.</p>' +
                                    '</div>' +
                                '</div>' +
                                '<div style="padding-top: 30px; padding-bottom: 30px; min-height: 100px" class="ui divider">' +
                                    '<h5>You guest will pay : <span id="guest-amount-to-pay"></span></h5>' +
                                    '<h5>You will earn : <span id="host-amount-to-earn"></span></h5>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<br>' +
                        '<div style="text-align: right" class="actions">' +
                        '<div id="special-offer-back-btn" class="ui blue button left floated">Back</div>' +
                        '<div id="submit-special-offer-btn" class="ui blue button" onClick="onSpecialOfferSubmit()">Submit</div>' +
                        '</div>' +
                     '</div>' +
                  '</div>';

var acceptBookingForm = '<div class="ui message accept-booking-wrapper">' +
                        '<div class="header">Accept Booking</div><br>' +
                        '<div id="accept-booking-form" class="ui form">' +
                        '<div id="success-message-wrapper" class="ui positive message">' +
                            '<div class="header"></div>' +
                            '<p></p>' +
                        '</div>' +
                        '<div id="error-message-wrapper" class="ui negative message">' +
                            '<p></p>' +
                        '</div>' +
                        '<br>' +
                        '<div class="field">' +
                            '<label>Type optional message to guest...</label>' +
                            '<textarea id="accept_booking_message" name="accept_booking_message"></textarea>' +
                        '</div>' +
                        '<div style="text-align: right" class="actions">' +
                        '<div class="ui blue button left floated" onClick="acceptBookingFormClose()">Back</div>' +
                        '<div id="accept-booking-btn" class="ui blue button" onClick="acceptBookingSubmit()">Submit</div>' +
                        '</div>' +
                     '</div>' +
                  '</div>';

var declineBookingForm = '<div class="ui message accept-booking-wrapper">' +
                            //'<div class="header">Decline Booking</div><br>' +
                            '<div id="accept-booking-form" class="ui form">' +
                            '<div id="success-message-wrapper" class="ui positive message">' +
                                '<div class="header"></div>' +
                                '<p></p>' +
                            '</div>' +
                            '<div id="error-message-wrapper" class="ui negative message">' +
                                '<p></p>' +
                            '</div>' +
                            '<br>' +
                            '<div class="field">' +
                                '<label>Tell guest why you can’t accommodate the reservation.</label>' +
                                '<textarea onKeyUp="bookingDeclineTypeMessage(this);" id="decline_booking_reason"></textarea>' +
                            '</div>' +
                            '<div style="text-align: right" class="actions">' +
                            '<div id="decline-booking-back-btn" class="ui blue button left floated" onClick="declineBookingFormClose()">Back</div>' +
                            '<div id="decline-booking-btn" class="ui blue button disabled" onClick="declineBookingSubmit()">Submit</div>' +
                            '</div>' +
                         '</div>' +
                      '</div>';

var declineInquiryForm = '<div class="ui message decline-inquiry-wrapper">' +
                            '<div class="header">Decline Inquiry</div><br>' +
                            '<div id="decline-inquiry-form" class="ui form">' +
                            '<div id="success-message-wrapper" class="ui positive message">' +
                                '<div class="header"></div>' +
                                '<p></p>' +
                            '</div>' +
                            '<div id="error-message-wrapper" class="ui negative message">' +
                                '<p></p>' +
                            '</div>' +
                            '<br>' +
                            '<div class="field">' +
                                '<label>Reason for declining.</label>' +
                                '<textarea id="decline_inquiry_reason"></textarea>' +
                            '</div>' +
                            '<div style="text-align: right" class="actions">' +
                            '<div id="decline-inquiry-back-btn" class="ui blue button left floated">Back</div>' +
                            '<div id="decline-inquiry-submit-btn" class="ui blue button" onClick="onDeclineInquirySubmit()">Submit</div>' +
                            '</div>' +
                         '</div>' +
                      '</div>';

var alterBookingForm = '<div class="ui message alter-booking-wrapper">' +
                            '<div class="header">Alter Booking</div><br>' +
                            '<div id="alter-booking-form" class="ui form">' +
                            '<div id="alter-success-message-wrapper" class="ui positive message">' +
                                '<div class="header"></div>' +
                                '<p></p>' +
                            '</div>' +
                            '<div id="alter-error-message-wrapper" class="ui negative message">' +
                                '<p></p>' +
                            '</div>' +
                            '<br>' +
                            '<div class="fields">' +
                                '<div class="six wide field">' +
                                    '<label>Check in</label>' +
                                    '<div id="alter-booking-start-date" class="ui calendar">' +
                                        '<div class="ui input left icon">' +
                                            '<i class="calendar icon"></i>' +
                                            '<input id="alter-booking-check-in" type="text" name="checkIn">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="six wide field">' +
                                    '<label>Check out</label>' +
                                    '<div id="alter-booking-end-date" class="ui calendar">' +
                                        '<div class="ui input left icon">' +
                                            '<i class="calendar icon"></i>' +
                                            '<input id="alter-booking-check-out" type="text" name="checkOut">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="fields">' +
                                '<div class="three wide field">' +
                                    '<div class="field">' +
                                        '<label>Adults</label>' +
                                        '<select id="alter-booking-adults-count" class="ui fluid dropdown" name="guestCount">' +
                                            '<option value="1">1</option>' +
                                            '<option value="2">2</option>' +
                                            '<option value="3">3</option>' +
                                            '<option value="4">4</option>' +
                                            '<option value="5">5</option>' +
                                            '<option value="6">6</option>' +
                                            '<option value="7">7</option>' +
                                            '<option value="8">8</option>' +
                                            '<option value="9">9</option>' +
											'<option value="10">10</option>' +
											'<option value="11">11</option>' +
											'<option value="12">12</option>' +
											'<option value="13">13</option>' +
											'<option value="14">14</option>' +
											'<option value="15">15</option>' +
											'<option value="16">16</option>' +
											'<option value="17">17</option>' +
											'<option value="18">18</option>' +
											'<option value="19">19</option>' +
											'<option value="20">20</option>' +

                                        '</select>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="three wide field">' +
                                    '<div class="field">' +
                                        '<label>Children</label>' +
                                        '<select id="alter-booking-children-count" class="ui fluid dropdown" name="guestCount">' +
                                            '<option value="1">1</option>' +
                                            '<option value="2">2</option>' +
                                            '<option value="3">3</option>' +
                                            '<option value="4">4</option>' +
                                            '<option value="5">5</option>' +
                                            '<option value="6">6</option>' +
                                            '<option value="7">7</option>' +
                                            '<option value="8">8</option>' +
                                            '<option value="9">9</option>' +
                                            '<option value="10">10</option>' +
											'<option value="11">11</option>' +
											'<option value="12">12</option>' +
											'<option value="13">13</option>' +
											'<option value="14">14</option>' +
											'<option value="15">15</option>' +
											'<option value="16">16</option>' +
											'<option value="17">17</option>' +
											'<option value="18">18</option>' +
											'<option value="19">19</option>' +
											'<option value="20">20</option>' +
                                        '</select>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="five wide field">' +
                                    '<label>Subtotal</label>' +
                                    '<div class="ui input">' +
                                        '<input id="alter-booking-subtotal" type="text" onKeyup="onChangeAlterPrice(this)" name="subtotal">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            //'<div class="fields" id="alter-booking-get-details-div" hidden>' +
                            //    '<div class="five wide field">' +
                            //        'Reservation Cost : <span id="alter-booking-get-details-reservation">0</span>' +
                            //    '</div>' +
                            //    '<div class="five wide field">' +
                            //        'Total : <span id="alter-booking-get-details-total">0</span>' +
                            //    '</div>' +
                            //'</div>' +
                            '<div style="text-align: right" class="actions">' +
                            '<div onClick="onAlterFormClose()" class="ui blue button left floated">Back</div>' +
                            //'<div id="alter-booking-get-details-btn" onClick="" class="ui blue button">Get Details</div>' +
                            '<div id="alter-booking-submit-btn" onClick="onSubmitAlteration()" class="ui blue button">Submit</div>' +
                            '</div>' +
                         '</div>' +
					'</div>';


var airbnbAlterBookingForm = '<div class="ui message alter-booking-wrapper">' +
	'<div class="header">Alter Booking</div><br>' +
	'<div id="alter-booking-form" class="ui form">' +
	'<div id="alter-success-message-wrapper" class="ui positive message">' +
	'<div class="header"></div>' +
	'<p></p>' +
	'</div>' +
	'<div id="alter-error-message-wrapper" class="ui negative message">' +
	'<p></p>' +
	'</div>' +
	'<br>' +
	'<div class="fields">' +
	'<div class="eight wide field">' +
	'<div class="field">' +
	'<label>Property</label>' +
	'<select id="alter-booking-property-id" class="ui fluid dropdown" name="propertyId">' +
	($("meta[name=property_list_for_select]").attr('content')) +
	'</select>' +
	'</div>' +
	'</div>' +
	'<div class="three wide field">' +
	'<div class="field">' +
	'<label>Guests</label>' +
	'<select id="alter-booking-guest-count" class="ui fluid dropdown" name="guestCount">' +
	'<option value="1">1</option>' +
	'<option value="2">2</option>' +
	'<option value="3">3</option>' +
	'<option value="4">4</option>' +
	'<option value="5">5</option>' +
	'<option value="6">6</option>' +
	'<option value="7">7</option>' +
	'<option value="8">8</option>' +
	'<option value="9">9</option>' +
	'<option value="10">10</option>' +
	'<option value="11">11</option>' +
	'<option value="12">12</option>' +
	'<option value="13">13</option>' +
	'<option value="14">14</option>' +
	'<option value="15">15</option>' +
	'<option value="16">16</option>' +
	'<option value="17">17</option>' +
	'<option value="18">18</option>' +
	'<option value="19">19</option>' +
	'<option value="20">20</option>' +
	'</select>' +
	'</div>' +
	'</div>' +
	'<div class="five wide field">' +
	'<label>Subtotal</label>' +
	'<div class="ui input">' +
	'<input id="alter-booking-subtotal" type="text" onKeyup="onChangeAlterPrice(this)" name="subtotal">' +
	'</div>' +
	'</div>' +
	'</div>' +
	'<div class="fields">' +
	'<div class="six wide field">' +
	'<label>Check in</label>' +
	'<div id="alter-booking-start-date" class="ui calendar">' +
	'<div class="ui input left icon">' +
	'<i class="calendar icon"></i>' +
	'<input id="alter-booking-check-in" type="text" name="checkIn">' +
	'</div>' +
	'</div>' +
	'</div>' +
	'<div class="six wide field">' +
	'<label>Check out</label>' +
	'<div id="alter-booking-end-date" class="ui calendar">' +
	'<div class="ui input left icon">' +
	'<i class="calendar icon"></i>' +
	'<input id="alter-booking-check-out" type="text" name="checkOut">' +
	'</div>' +
	'</div>' +
	'</div>' +
	'<div class="six wide field"></div>' +
	'</div>' +
	'<div style="text-align: right" class="actions">' +
	'<div onClick="onAlterFormClose()" class="ui button left floated">Back</div>' +
	'<div id="alter-booking-submit-btn" onClick="onSubmitAlteration()" class="ui blue button">Submit</div>' +
	'</div>' +
	'</div>' +
	'</div>';


var cancelAlterationForm = '<div class="card">' +
                               '<div class="content">' +
                                    '<div class="header">Are you sure that you want to cancel the alteration request?</div><br>' +
                                '</div>' +
                                '<div class="extra content">' +
                                    '<div class="ui two buttons">' +
                                    '<div style="margin: 1%" class="ui blue button" onClick="cancelAlterationSubmit()">Yes</div>' +
                                    '<div style="margin: 1%" class="ui blue button" onClick="cancelAlterationFormClose()">No</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
var preApproveForm = '<div class="card">' +
                        '<div class="content">' +
                        '<div class="header">Are you sure that you want to pre-approve this guest ?</div><br>' +
                        '</div>' +
                        '<div class="extra content">' +
                        '<div class="ui two buttons">' +
                        '<div style="margin: 1%" onClick="preApproveSubmit()" class="ui blue button">Yes</div>' +
                        '<div style="margin: 1%" onClick="preApproveFormClose()" class="ui blue button">No</div>' +
                        '</div>' +
                        '</div>' +
                     '</div>';
var withdrawPreApproveForm = '<div class="card">' +
                        '<div class="content">' +
                        '<div class="header">Are you sure ?</div><br>' +
                        '</div>' +
                        '<div class="extra content">' +
                        '<div class="ui two buttons">' +
                        '<div onClick="withdrawPreApproveSubmit()" class="ui blue button">Yes</div>' +
                        '<div onClick="withdrawPreApproveFormClose()" class="ui blue button">No</div>' +
                        '</div>' +
                        '</div>' +
                     '</div>';
var cancelSpecialOfferForm = '<div class="card">' +
                                '<div class="content">' +
                                '<div class="header">Are you sure that you want to withdraw this special offer ?</div><br>' +
                                '</div>' +
                                '<div class="extra content">' +
                                '<div class="ui two buttons">' +
                                '<div class="ui blue button" onClick="withdrawSpecialOfferSubmit()">Yes</div>' +
                                '<div class="ui blue button" onClick="withdrawSpecialOfferCloseForm()">No</div>' +
                                '</div>' +
                                '</div>' +
                             '</div>';
var cancelReservationForm = '<div class="card">' +
                        '<div class="content">' +
                        '<div class="header">Are you sure that you want cancel this reservation ?</div><br>' +
                        '</div>' +
                        '<div class="extra content">' +
                        '<div class="ui two buttons">' +
                        '<div class="ui blue button" onClick="declineBookingSubmit()">Yes</div>' +
                        '<div class="ui blue button" onClick="backToCancelReservationForm()">No</div>' +
                        '</div>' +
                        '</div>' +
                     '</div>';

var addPaymentRequestForm = '<div class="ui message add-payment-request-wrapper">' +
                                '<div class="header">Add Payment Request</div><br>' +
                            '<div id="add-payment-request-form" class="ui form">' +
                            '<div id="add-payment-request-success-message-wrapper" class="ui positive message">' +
                                '<div class="header"></div>' +
                                '<p></p>' +
                            '</div>' +
                            '<div id="add-payment-request-error-message-wrapper" class="ui negative message">' +
                                '<p></p>' +
                            '</div>' +
                            '<br>' +
							'<div class="fields">' +
								'<div class="field">' +
									'<label>Payment Request Description</label>' +
									'<input type="text" onKeyUp="" id="add-payment-request-description"></input>' +
								'</div>' +
							'</div>' +
                            '<div class="fields">' +
								'<div class="field">' +
                                '<label>Due Date</label>' +
                                '<div id="add-payment-request-date" class="ui calendar">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
                                        '<input id="add-payment-request-due-date" type="text" name="dueDate">' +
                                    '</div>' +
								'</div>' +
							  '</div>' +
                            '</div>' +
							'<div class="fields">' +
							  '<div class="field">'+
                                '<label>Amount</label>' +
                                '<input type="text" onKeyUp="" id="add-payment-request-amount"></input>' +
							  '</div>'+
                            '</div>' +
							'<div class="fields">' +
							  '<div class="fields">' +
                                '<label>Your Message</label>' +
                                '<textarea onKeyUp="" id="add-payment-request-message"></textarea>' +
							    '</div>' +
							'</div>' +
                            '<div style="text-align: right" class="actions">' +
                            '<div onClick="onAlterFormClose()" class="ui blue button left floated">Back</div>' +
                            '<div id="alter-booking-submit-btn" onClick="onSubmitAddPaymentRequest()" class="ui blue button">Submit</div>' +
                            '</div>' +
                         '</div>' +
                            '</div>';

var sendRefundForm = '<div class="ui message send-refund-wrapper">' +
                                '<div class="header">Send Refund</div><br>' +
                            '<div id="send-refund-form" class="ui form">' +
                            '<div id="send-refund-success-message-wrapper" class="ui positive message">' +
                                '<div class="header"></div>' +
                                '<p></p>' +
                            '</div>' +
                            '<div id="send-refund-error-message-wrapper" class="ui negative message">' +
                                '<p></p>' +
                            '</div>' +
                            '<br>' +
                            '<div class="fields">' +
                                '<label>Refund Description</label>' +
                                '<input type="text" onKeyUp="" id="send-refund-description"></input>' +
                            '</div>' +
                            '<div class="fields">' +
                                '<label>Amount</label>' +
                                '<input type="text" onKeyUp="" id="send-refund-amount"></input>' +
                            '</div>' +
                            '<div style="text-align: right" class="actions">' +
                            '<div onClick="onAlterFormClose()" class="ui blue button left floated">Back</div>' +
                            '<div id="send-refund-submit-btn" onClick="onSubmitSendRefund()" class="ui blue button">Submit</div>' +
                            '</div>' +
                         '</div>' +
                            '</div>';


//Joem 7/26/18 This is for booking.com transaction
function populateBookingComFees(selectedBooking) {

	if (selectedBooking.SiteType == 3 || selectedBooking.SiteType == 0) {
		if (selectedBooking.RateDetails != null) {
		var table = '<div><h5>Booking Reference Number : ' + selectedBooking.ConfirmationCode + '</h5>';
		table += '<center><table style="width:80%; text-align:left;"><tr><th>Date</th><th>Rate</th><th>Price per night</th></tr>'
		
		for (var i = 0; i < selectedBooking.RateDetails.length; i++) {

			table += '<tr stype="border-top:1pt solid black;"><td> ' + selectedBooking.RateDetails[i]["DateString"] +
				'</td><td>' + selectedBooking.RateDetails[i]["DateString"] + '</td><td>' +
				selectedBooking.RateDetails[i]["Price"] + '</td></tr>'


		table += '<tr><td><b>Subtotal</b>' +
			'</td><td></td><td><b>' +
			selectedBooking.SumPerNight + '</b></td></tr>'
		
		for (var i = 0; i < selectedBooking.Fees.length; i++) {
					table += '<tr><td>' + selectedBooking.Fees[i]["FeeName"] +
						'</td><td>' + selectedBooking.Fees[i]["Rate"] + '</td><td>' +
				selectedBooking.Fees[i]["Price"] + '</td></tr>'
		}

				table += '<tr><td><b>Total Room Price' +
					'</b></td><td></td><td><b>' + selectedBooking.Payout + '</b></td></tr>'

		table += '</table></center></br></div>';


		return table;
	}
		}
	}
	return '';
}

var changeReservationDatesAndPricesForm1 = '<div class="ui message change-reservation-dates-prices">' +

	'<div class="content">' +
	'<div class="header"><h5>Change reservation dates & prices.</h5></div><br>' +
	'</div>' +

	'<div class="extra content">' +
	'<div class="field">' +
	'Check In Date' +
	'<div class="ui calendar" id="change-date-and-price-from">' +
	'<div class="ui input left icon">' +
	'<i class="calendar icon"></i>' +
	'<input type="text" id="change-date-and-price-checkIn" placeholder="Check In Date">' +
	'</div>' +
	'</div>' +
	'</div>' +
	'<br/>' +
	'<div class="field">' +
	'Check Out Date' +
	'<div class="ui calendar" id="change-date-and-price-to">' +
	'<div class="ui input left icon">' +
	'<i class="calendar icon"></i>' +
	'<input type="text" id="change-date-and-price-checkOut" placeholder="Check Out Date">' +
	'</div>' +
	'</div>' +
	'</div>' +
	'</div>' +

	'<br/>' +

	'</div>' +

	'<div style="text-align: right" class="actions">' +
	'<button onClick="onAlterFormClose()" class="ui blue button">Back</button>' +
	'<button onClick="checkAvailabilityAndPriceSubmit()" id="check-availability-and-price-submit" class="ui blue button">Check Availability & Change Price</button>' +
	'</div>';

function changeReservationDatesAndPricesForm2(result) {
	return (
		'<div class="ui message change-reservation-dates-prices">' +

		'<div class="content">' +
		'<div class="header">Change reservation dates & prices.</div><br>' +
		'</div>' +

		'<div class="content">' +
		'<div class="header"><h5>You have available rooms and can proceed to change the dates of this reservation.</h5></div><br>' +
		'</div>' +

		'<div class="content">' +
		'<div class="header"><h5>' + result.ReservationId + '</h5></div><br>' +
		'</div>' +

		'<div class="content">' +
		'<div>DATE : ' + result.CheckInDate + ' - ' + result.CheckOutDate + '</div>' +
		'</div>' +

		'<input type="hidden" id="change-date-and-price-checkInDate" value="' + result.CheckInDate + '" />' +
		'<input type="hidden" id="change-date-and-price-checkOutDate" value="' + result.CheckOutDate + '" />' +

		'<br/>' +

		'<div class="content">' +
		'<div class="header"><h5>Price</h5></div><br>' +
		'</div>' +

		'<div class="content">' +
		'<div>Adjust the total price for this reservation. The total amount will be equally distributed among the rooms.</div>' +
		'</div>' +

		'<br/>' +

		'from <strike>' + result.OldPrice + '</strike> to ' +

		'<br/>' +

		'<div class="ui labeled input">' +
		'<div class="ui label">' +
		result.CurrencySymbol +
		'</div>' +
		'<input placeholder="New Price" id="new-price-tb" type="text" value="' + result.NewPrice + '">' +
		'</div>' +

		'<br/><br/>' +

		'<div class="content">' +
		'<div>If you\'d like to adjust the price per room, please select which rooms under the Applicable rooms section in the previous step.</div>' +
		'</div>' +

		'<br/>' +

		'<div class="content">' +
		'<div class="header"><h5>Commission</h5></div><br>' +
		'</div>' +

		'from <strike>' + result.OldCommission + '</strike> to ' + result.NewCommission +

		'<br/>' +

		'</div>' +

		'<div style="text-align: right" class="actions">' +
		'<button onClick="onAlterFormClose()" class="ui blue button">Back</button>' +
		'<button onClick="saveChangeDateAndPriceSubmit()" id="save-change-and-price-submit" class="ui blue button">Save Changes</button>' +
		'</div>');

}

function saveChangeDateAndPriceSubmit() {
	debugger;
	var hostId = quickModal.booking.HostId;
	var reservationId = quickModal.selectedBooking.InquiryId;
	var reservationCode = quickModal.selectedBooking.ConfirmationCode;
	var listingId = quickModal.selectedBooking.PropertyId;
	var checkInDate = $("#change-date-and-price-checkInDate").val();
	var checkOutDate = $("#change-date-and-price-checkOutDate").val();
	var newPrice = $("#new-price-tb").val();
	debugger;

	$.ajax({
		type: 'POST',
		url: "/Booking/AlterBooking",
		data: {
			hostId: hostId,
			reservationId: reservationId,
			reservationCode: reservationCode,
			listingId: listingId,
			checkinDate: checkInDate,
			checkoutDate: checkOutDate,
			price: newPrice,
			siteType: 3
		},
		success: function (result) {
			if (result.success) {
				$('.inquiry-error-message-wrapper').hide();
				$('.inquiry-success-message-wrapper').find('.header').text(result.message);
				$('.inquiry-success-message-wrapper').show();
				$('.inquiry-success-message-wrapper').removeClass('hidden');
				$('#booking-action-form').empty();
				$('#booking-actions').empty();
				$('#booking-actions').show();
				$('#trip-info-details').show();
				$('#reservation-status').text('Booking-Cancelled');
				quickModal.selectedBooking.Status = "BC";
			}
			else {
				$('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
				$('.inquiry-error-message-wrapper').find('.header').text(result.message);
				$('.inquiry-error-message-wrapper').show();
				$('.inquiry-error-message-wrapper').removeClass('hidden');
			}
		}
	});
}

function checkAvailabilityAndPriceSubmit() {
	debugger;
	var reservationId = quickModal.selectedBooking.InquiryId;
	var reservationCode = quickModal.selectedBooking.ConfirmationCode;
	var hostId = quickModal.booking.HostId;
	var listingId = quickModal.selectedBooking.PropertyId;
	var checkInDate = $("#change-date-and-price-checkIn").val();
	var checkOutDate = $("#change-date-and-price-checkOut").val();

	$.ajax({
		type: 'POST',
		url: "/Booking/CheckAvailabilityAndPrice",
		data: {
			hostId: hostId,
			reservationId: reservationId,
			confirmationCode: reservationCode,
			listingId: listingId,
			checkInDate: checkInDate,
			checkOutDate: checkOutDate
		},
		success: function (result) {
			changeReservationDatesAndPrices2(result);
		}
	});
}

function changeReservationDatesAndPrices2(result) {
	debugger;
	var result = JSON.parse(result.result);
	$('#booking-action-form').children().remove();
	$('#booking-action-form').append(changeReservationDatesAndPricesForm2(result.Data));

	//onAlterBookingFormChange();
	$('#booking-actions-inquiry').hide();
	$('#trip-info-details').hide();
	$('#quick-action-modal').find('#success-message-wrapper').hide();
	$('#quick-action-modal').find('#error-message-wrapper').hide();
	$('#add-payment-request-form').find('#add-payment-request-success-message-wrapper').hide();
	$('#add-payment-request-form').find('#add-payment-request-error-message-wrapper').hide();
	$('#quick-action-modal').modal('refresh');

	$('#add-payment-request-date').calendar({
		type: 'date', onChange: function (date, text, mode) {
			$("#add-payment-request-form input[name=dueDate]").val(text);
		}
	});
}

function getVariableList(data) {
	
	var variables = data.Inquiries[0];
	return '<option class="placeholder" selected disabled value="">Select Variable</option>' +
		'<option value="' + data.GuestName + '">[Guest Name]</option>' +
		'<option value="' + variables.GuestCount + '">[Guest Count]</option>' +
		(variables.Status != 'I' ? '<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>':'')
		+
		'<option value="' + (quickModal.selectedBooking.TimeZoneId != null ? moment(variables.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY") : moment.utc(variables.CheckInDate).format("MMM D, YYYY")) + '">[Check In Date]</option>' +
		'<option value="' + (quickModal.selectedBooking.TimeZoneId != null ? moment(variables.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY") : moment.utc(variables.CheckOutDate).format("MMM D, YYYY")) + '">[Check Out Date]</option>' +
		'<option value="' + variables.PropertyName + '">[Listing Name]</option>' +
		'<option value="' + variables.CleaningCost + '">[Cleaning Fee]</option>' +
		'<option value="' + (variables.ReservationCost - variables.CleaningCost) + '">[SubTotal]</option>'+
		'<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>' +
		'<option value="' + variables.PropertyAddress + '">[Listing Address]</option>' +
		((variables.PhoneNumber != null && variables.PhoneNumber != "") ? '<option value="' + variables.PhoneNumber.slice(variables.PhoneNumber.length - 4) + '">[Lock Pin Code]</option>': "");
		//'<option value="' + variables.PropertyDescription + '">[Listing Description]</option>' +
		//'<option value="' + variables.PropertyType + '">[Listing Type]</option>'


	//$('#template-variable').append('<option class="placeholder" selected disabled value="">Select Variable</option>');
	//var variables = data.Inquiries[0];
	//console.log(variables);
	//if (data.Inquiries.length == 1) {
	//	//vaiables in messages page
	//	$('#template-variable').append('<option value="' + data.GuestName + '">[Guest Name]</option>');
	//	$('#template-variable').append('<option value="' + variables.GuestCount + '">[Guest Count]</option>');
	//	if (variables.Status != 'I' && variables.Status != 'NP') {
	//		$('#template-variable').append('<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>');
	//	}
	//	$('#template-variable').append('<option value="' + moment(variables.CheckInDate).format("MMM D, YYYY") + '">[Check In Date]</option>');
	//	$('#template-variable').append('<option value="' + moment(variables.CheckOutDate).format("MMM D, YYYY") + '">[Check Out Date]</option>');

	//	$('#template-variable').append('<option value="' + variables.PropertyName + '">[Listing Name]</option>');
	//	$('#template-variable').append('<option value="' + variables.CleaningCost + '">[Cleaning Cost]</option>');
	//	$('#template-variable').append('<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>');
	//	$('#template-variable').append('<option value="' + variables.PropertyAddress + '">[Listing Address]</option>');
	//	$('#template-variable').append('<option value="' + variables.PropertyDescription + '">[Listing Description]</option>');
	//	$('#template-variable').append('<option value="' + variables.PropertyType + '">[Listing Type]</option>');


	//}
	//else if (data.threadDetails.Inquiries.length > 1) {

	//	//console.log(data.threadDetails.GuestName);
	//	$('#template-variable').append('<option value="' + data.GuestName + '">[Guest Name]</option>');
	//	//$('#template-variable').append('<option value="' + variables.GuestCount + '">[Guest Count]</option>');
	//	//if (variables.Status != 'I' && variables.Status != 'NP') {
	//	//	$('#template-variable').append('<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>');
	//}
	//$('#template-variable').append('<option value="' + moment(variables.CheckInDate).format("MMM D, YYYY") + '">[Check In Date]</option>');
	//$('#template-variable').append('<option value="' + moment(variables.CheckOutDate).format("MMM D, YYYY") + '">[Check Out Date]</option>');
	//$('#template-variable').append('<option value="' + variables.PropertyName + '">[Listing Name]</option>');
	//$('#template-variable').append('<option value="' + variables.CleaningCost + '">[Cleaning Cost]</option>');
	//$('#template-variable').append('<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>');
	//$('#template-variable').append('<option value="' + variables.PropertyAddress + '">[Listing Address]</option>');
	//$('#template-variable').append('<option value="' + variables.PropertyDescription + '">[Listing Description]</option>');
	//$('#template-variable').append('<option value="' + variables.PropertyType + '">[Listing Type]</option>');
}

function tabIsActive(dataTab) {
    return activeTabMenu == dataTab ? " active" : "";
}

function getBookingTab(booking) {

	var selectedBooking = quickModal.selectedBooking;
	var html = "";
	return '<div class="ui tab' + tabIsActive("booking") + '" data-tab="booking">' +
                '<div>' +
                    '<div class="content">' +
                        '<div class="description">' +
                            // start item
                            '<div class="booking-item">' +
                                '<div class="ui positive message inquiry-success-message-wrapper"><i class="close icon"></i><div class="header"></div></div>' +
                                '<div class="ui negative message inquiry-error-message-wrapper"><i class="close icon"></i><div class="header"></div></div>' +
								'<div id="booking-actions">' +
								(selectedBooking.CheckInDate != null && selectedBooking.CheckOutDate !=null && UserWorkerId == 0 && selectedBooking.Source != 5 && selectedBooking.Source != 6 ? (
                                    (selectedBooking.Status == 'I' || selectedBooking.Status == 'NP' || selectedBooking.Status == 'SO' || selectedBooking.Status == 'PAYMENT_REQUEST_SENT')
										? getInquiryAction()
										 : (selectedBooking.Status == 'B')
                                            ? getBookingAction()
                                            : (selectedBooking.Status == 'A')
					                           ? ConfirmedBookingAction(selectedBooking.ConfirmationCode, booking.GuestName)
                                                : ((selectedBooking.Status == 'A' && selectedBooking.IsActive && selectedBooking.IsAltered == false)
                                                    ? tripCoordinationAction
                                                    : ((selectedBooking.Status == 'A' && selectedBooking.IsActive && selectedBooking.IsAltered)
								                        ? tripCoordinationAlteredAction : ''))
		                        ):'') +
                                '</div><br>' +
                                '<div id="booking-action-form"></div><br>' +
								'<div id="trip-info-details">' +
								(UserWorkerId==0?(populateBookingComFees(selectedBooking) + getPaymentTab()):'') +
								'<div>' +
									(UserWorkerId == 0 ?('<div style="float: right; margin-right: 40px;">' +
									'<div class="ui accordion">'+
										'<div class="title">'+
										'<h5 style="margin-top: 4px;"><i class="dropdown icon"></i>'+selectedBooking.Nights+' Night(s) : <span id="reservation-sum-night" class="margin-left-10-px gray">' + selectedBooking.NightsFee  + '</span></h5>'+
										'</div>'+
										'<div class="content">'+
										 	'<p class="transition hidden">'+(selectedBooking.NightsFeeSummary!=null?selectedBooking.NightsFeeSummary:'')+'</p>'+
										'</div>'+
									'</div>'+
                                    '<h5 style="margin-top: 4px;">Reservation Cost : <span id="reservation-cost" class="margin-left-10-px gray">' + selectedBooking.ReservationCost + '</span></h5>' +
									'<h5 style="margin-top: 4px;">Cleaning Fee : <span id="reservation-cleaning-fee" class="margin-left-10-px gray">' + selectedBooking.CleaningCost + '</span></h5>' +
									'<h5 style="margin-top: 4px;">' + getSiteName(selectedBooking.SiteType) + ' Fee : <span id="reservation-airbnb-fee" class="margin-left-10-px gray">' + selectedBooking.AirbnbFee + '</span></h5>' +
                                    '<h5 style="margin-top: 4px;">Total Payout : <span id="reservation-total-payout" class="margin-left-10-px gray">' + selectedBooking.Payout + '</span></h5>' +
								   '</div>'):'') +
								'<div>' +
								'<h5>' + selectedBooking.GuestCount + ' ' + (selectedBooking.GuestCount > 1 ? 'Guests' : 'Guest') + '</h5>' +
								('<h5 id="inq-h5" ' + ((selectedBooking.InquiryDate != null) ? '' : 'hidden') + '>Inquiry Date:<span id="inq-date" class="margin-left-10-px gray"> ' + (quickModal.selectedBooking.TimeZoneId != null ? moment(selectedBooking.InquiryDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a") : moment.utc(selectedBooking.InquiryDate).format("MMM D, YYYY h:m a"))+ '</span></h5>') +
								('<h5 id="confirm-date-h5" ' + ((selectedBooking.ConfirmationDate != null) ? '' : 'hidden') + '>Confirmation Date:<span id="confirm-date" class="margin-left-10-px gray">' +( quickModal.selectedBooking.TimeZoneId != null? moment(selectedBooking.ConfirmationDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a") : moment.utc(selectedBooking.ConfirmationDate).format("MMM D, YYYY h:m a")) + '</span></h5>') +
								('<h5 id="decline-h5" ' + ((selectedBooking.BookingCancelDate != null) ? '' : 'hidden') + '>Decline Date:<span id="booking-cancel-date" class="margin-left-10-px gray"> ' + (quickModal.selectedBooking.TimeZoneId != null? moment(selectedBooking.BookingCancelDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a") : moment.utc(selectedBooking.BookingCancelDate).format("MMM D, YYYY h:m a")) + '</span></h5>') +
								('<h5 id ="confirm-cancel-h5" ' + ((selectedBooking.BookingConfirmCancelDate != null) ? '' : 'hidden') + '>Cancel Date:<span id="confirm-cancel-date" class="margin-left-10-px gray"> ' +( quickModal.selectedBooking.TimeZoneId != null? moment(selectedBooking.BookingConfirmCancelDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a") : moment.utc(selectedBooking.BookingConfirmCancelDate).format("MMM D, YYYY h:m a"))+ '</span></h5>') +
									((selectedBooking.Status != 'I' && selectedBooking.Status != 'NP' && selectedBooking.Status != 'Closed') ? '<h5 id="confirmation-code-wrapper">Confirmation Code : <span id="reservation-confirmation-code" class="margin-left-10-px gray">' + selectedBooking.ConfirmationCode + '</span></h5>':'') +
                                '<div class="ui three column grid">' +
                                    '<div class="row">' +
                                        '<div class="four wide column">' +
										'<h5 class="margin-0">Check in</h5>' +
										'<h5 id="reservation-check-in" class="margin-0 gray">' + (selectedBooking.CheckInDate == null ? 'Not Specified' : (quickModal.selectedBooking.TimeZoneId != null ? moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY (dddd)") : moment.utc(selectedBooking.CheckInDate).format("MMM D, YYYY (dddd)") )) + '</h5>' +
                                        '</div>' +
                                        '<div class="one wide column">' +
                                            '<h5 class="margin-0">&nbsp;</h5>' +
                                            '<h6 class="margin-0 gray"><i class="arrow right icon"></i></h6>' +
                                        '</div>' +
                                        '<div class="four wide column">' +
                                            '<h5 class="margin-0">Check out</h5>' +
										'<h5 id="reservation-check-out" class="margin-0 gray">' + (selectedBooking.CheckOutDate == null ? 'Not Specified' : (quickModal.selectedBooking.TimeZoneId != null ? moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY (dddd)") : moment.utc(selectedBooking.CheckOutDate).format("MMM D, YYYY (dddd)"))) + '</h5>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
								'<br/>'+
								(quickModal.pincode!=null?'<div class="ui form">'+
									'<div class="fields">' +
									'<div class="field">' +
									'<h5>Pin Name:' + quickModal.pincode.PinName + '</h5>' +
									'</div>' +
									'</div>' +
									'<div class="fields">' +
									'<div class="field">' +
									'<h5>Pin Code:' + quickModal.pincode.PinCode + '</h5>' +
									'</div>' +
									'</div>' +
									'<div class="fields">' +
									'<div class="field">' +
									'<h5>Date Created:' + moment(quickModal.pincode.DateCreated).format("h:mm A MMM D, YYYY") + '</h5>' +
									'</div>' +
									'</div>' +
									'<div class="fields">' +
									'<div class="field">' +
									'<h5>Date Deleted:' +(quickModal.pincode.DateDeleted!=null?  moment(quickModal.pincode.DateDeleted).format("h:mm A MMM D, YYYY"):"N/A") + '</h5>' +
									'</div>' +
									'</div>' +
								'</div>':'')+
                                '<br />' +
                                '</div>' +
                                '<br />' +
                                '</div>' +

                                //(selectedBooking.SiteType == '0' ? getPaymentTab() : '') +

                                '<div class="ui negative message change-time-error-message">' +
                                    '<i class="close icon"></i>' +
                                    '<div class="header">Unable to change check in/ check out time</div>' +
                                '</div>' +
                                '<div class="ui positive message change-time-success-message">' +
                                    '<i class="close icon"></i>' +
                                    '<div class="header">Successfully updated.</div>' +
                                '</div>' +
                                (UserWorkerId==0?((selectedBooking.Status == 'A' && selectedBooking.IsActive) ?
                                ('<div id="change-booking-time-form" class="ui form">' +
                                    '<div class="fields">' +
                                        '<div class="five wide field">' +
                                            '<label>Check in time</label>' +
                                            '<div class="ui calendar time">' +
                                                '<div class="ui input left icon">' +
												'<i class="calendar icon"></i>' +
												'<input type="text" name="checkInTime" value="' + (quickModal.selectedBooking.TimeZoneId != null ? moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('h:mm A') : moment.utc(selectedBooking.CheckInDate).format('h:mm A') )+ '">' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="five wide field">' +
                                            '<label>Check out time</label>' +
                                            '<div class="ui calendar time">' +
                                                '<div class="ui input left icon">' +
                                                    '<i class="calendar icon"></i>' +
													'<input type="text" name="checkOutTime" value="' + (quickModal.selectedBooking.TimeZoneId != null ? moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('h:mm A') : moment(selectedBooking.CheckOutDate).format('h:mm A')) + '">' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<button type="button" class="ui blue button" onClick="saveNewBookingTime()">Save</button>' +
                                    '</div>') : '') :'')+
                                '</div>' +
                            '</div>' +
                            // end item
                        '</div>' +
                        '<br><br>' +
                    '</div>' +
                '</div>' +
            '</div>';
}

function getMessageHistoryTab(conversation, hostId, threadId,booking) {
	var messages = '';
    $.each(conversation.messages, function (index, value) {
		if (!value.IsMyMessage) {
			if (value.Type == 2) {
				messages +=('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
					(
						value.RecordingUrl == null ?
							'' :
							(
								'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
							)
					) +
					(
						value.MessageContent == null ?
							'' :
							(
								'<p>Transcription: ' + value.MessageContent + '</p>'
							)
					)
					+
					'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
					+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 1) {
				messages +=('<div style="width: 80%;" class="ui orange small left pointing label message-label message-incoming">' +
					'<h5>' + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 3) {
				messages +=('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
					'<h5>' + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 4) {
				messages +=('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
					'<h5>' + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
					'</div>');
			} else {
				messages +=('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
					'<h5>' + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
			}
		}
		else {
			if (value.Type == 2) {
				messages +=('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +

					(
						value.RecordingUrl == null ?
							'' :
							(
								'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
							)
					) +
					(
						value.MessageContent == null ?
							'' :
							(
								'<p>Transcription: ' + value.MessageContent + '</p>'
							)
					)
					+
					'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
					+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 1) {
				messages +=('<div style="width: 80%;" class="ui orange small right pointing label message-label message-outgoing">' +
					'<h5>' +
					(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
						'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
						'</div>' : '') + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 3) {
				messages +=('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
					'<h5>' +
					(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
						'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
						'</div>' : '') + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 4) {
				messages +=('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
					'<h5>' +
					(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
						'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
						'</div>' : '') + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
					'</div>');
			} else {
				messages +=('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
					'<h5>' +
					(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
						'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
						'</div>' : '') + value.MessageContent + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
			}
		}
    });
    return '<div class="ui tab' + tabIsActive("message-history") + '" data-tab="message-history">' +
                '<div>' +
                '<div id="dvConvo" class="conversation-box" data-thread-id="' + threadId + '" data-host-id="' + hostId + '">' +
                    '<div id="dv_Conversation">' +
                        messages +
                    '</div>' +
                '</div>' +
                '<div class="reply-panel">' +
                    '<div class="ui stackable grid">' +
                        '<div class="row reply-options">' +
                            '<div class="fourteen wide column">' +
                                '<div class="ui mini form">' +
                                    '<div class="fields">' +
                                        '<div class="field">' +
										'<select id="template-variable" class="ui search dropdown">' +
									    	getVariableList(booking)+
                                            '</select>' +
                                        '</div>' +
                                        '<div class="field">' +
                                            '<select id="template-messages" class="ui search dropdown">' +
                                                '<option value="">Select Template</option>' +
												getTemplateMessage()+
                                            '</select>' +
                                        '</div>' +
                                        //'<button id="save-template-btn" class="tiny ui blue button right floated">Saved Templates</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row message-reply">' +
                            '<div class="sixteen wide column">' +
                                '<div class="ui form">' +
                                    '<div class="field">' +
                                        '<textarea id="message-box" name="messageBox" class="message-box" value="" rows="3" style="resize: none;"></textarea>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '<div class="row message-send">' +
							'<div class="sixteen wide column">' +
								'<button type="button" id="send-message-btn" class="medium teal ui blue button right floated" disabled="true">Send</button>' +
								'<button type="button" id="end-message-btn" class="medium teal ui blue button right floated">End</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
}

//JM 8/29/2018 Code before new code of travel events
//function getTravelEventsTab() {
//    return '<div class="ui tab" data-tab="calendar-events">' +
//                '<div id="note-section" class="row" data-inquiry-id="">' +
//                    '<h3 style="color: #008590">Notes <a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a></h3>' +
//                    '<br><br>' +
//                '</div>' +
//                '<div class="ui small form">' +
//                    '<input id="reservation-details-inquiry-id" type="hidden" />' +
//                    '<div id="inquiry-note-wrapper" class="inquiry-note-wrapper">' +
//                    '</div>' +
//                    '<div id="note-save" class="hidden">' +
//                        '<div class="row">' +
//                            '<div class="sixteen wide column">' +
//                                '<button type="button" class="medium blue ui blue button" onClick="saveNote()">Save</button>' +
//                            '</div>' +
//                        '</div>' +
//                    '</div>' +
//                '</div>' +
//            '</div>';
//}
function getTravelEventsTab() {
    return '<div class="ui tab' + tabIsActive("travel-events") + '" data-tab="travel-events">' +
        '<div class="content">' +
		'<div id="note-section" class="row" data-inquiry-id="">' +
		(UserWorkerId==0?('<h3 style="color: #008590">Notes <a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a></h3>' +
		'<h5>Guest\'s Travel Event Form : http://' + quickModal.booking.SiteBaseUrl + '/Form/TravelEvent?id=' + quickModal.selectedBooking.Id + '</h5>') : '')+
		'<br><br>' +
		'</div>' +
		'<div class="ui small form">' +
		'<input id="reservation-details-inquiry-id" type="hidden" />' +
		'<div id="inquiry-note-wrapper" class="inquiry-note-wrapper">' +
		'</div>' +
		'<div id="note-save" class="hidden">' +
		'<div class="row">' +
		'<div class="sixteen wide column">' +
		'<button type="button" class="medium blue ui blue button" onClick="saveNote()">Save</button>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div>' +
		'</div>' +
        '</div>';
}

function getTaskTab() {
    return '<div class="ui tab' + tabIsActive("tasks") + '" data-tab="tasks">' +
		RenderTaskStatistics() +
		'<table id="task-list-table" class="ui table">' +
		'<thead>' +
		'<tr>' +
		'<th>Template Name</th>' +
		'<th>Task</th>' +
		//'<th>Task Description</th>' +
		//'<th>Status</th>' +
		//'<th>Image</th>' +
		'<th>Actions</th>' +
		'</tr>' +
		'</thead>' +
		'<tbody></tbody>' +
		'</table>' +
		'<button class="ui  icon button add_custom_task_button" data-tooltip="Add Custom Task">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'<div class="row">' +
		'<div class="sixteen wide column">' +
		'<button type="button" id="update-task-btn" class="medium teal ui button right floated" disabled="true">Update</button>' +
		'</div>' +
		'</div><br/><br/>' +
		'</div>';
}
function getWorkerTab() {
    return '<div class="ui tab' + tabIsActive("workers") + '" data-tab="workers">' +
                '<div id="worker-form-wrapper" style="display: none">' +
                    '<div id="worker-assign-form" class="ui mini form">' +
                        '<button onClick="backToWorkerAssignments()" type="button" class="ui orange button">Back</button><br><br>' +
                        '<div id="assign-worker-success-message-wrapper" class="ui positive message">' +
                            '<i class="close icon"></i>' +
                            '<div class="header">Success on assign worker.</div>' +
                            '<p></p>' +
                        '</div>' +
                        '<div id="assign-worker-error-message-wrapper" class="ui negative message">' +
                            '<i class="close icon"></i>' +
                            '<div class="header">Please fill all required fields.</div>' +
                        '</div>' +
                        '<div class="fields">' +
                            '<div class="four wide field">' +
                                '<select id="qm-job-type-id" class="ui dropdown" onChange="onJobTypeChange()">' +
                                    ($("meta[name=job_type_list_for_select]").attr('content')) +
                                '</select>' +
                            '</div>' +
                            '<div class="four wide field">' +
                                '<div class="ui calendar dt">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
										'<input type="text" id="worker-start-date" name="AssignStartDate" placeholder="Start Date"  onChange="onChangeStartDate()"/>' +
									'</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="four wide field">' +
                                '<div class="ui calendar dt">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
											'<input type="text" id="worker-end-date" name="AssignEndDate" placeholder="End Date" onChange="onChangeEndDate()" />' +	
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
						'<div class="fields">'+
								 '<div class="seven wide field">' +
									'<textarea id="assignment-description" class="message-box" value="" placeholder="Description" rows="2" style="resize: none;"></textarea>' +
									'</div>' +
							'</div>' +
                    '</div>' +
                    '<table id="worker-list-table" class="ui celled red table">' +
                        '<thead>' +
                            '<tr>' +
                                '<th>Name</th>' +
                                '<th>Job</th>' +
                                '<th>Payment</th>' +
                                '<th>Rate</th>' +
                                //'<th>Dates</th>' +
                                '<th></th>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody></tbody>' +
                    '</table>' +
				'<h3 style="color: #008590">Reimbursement note <a href="#" class="add-reinburst-button" data-tooltip="Add reimbursement details"><i class="teal plus circle icon add-field-icon"></i></a></h3>'+
				'<div id="re-inburst-note-wrapper" class="re-inburst-note-wrapper"></div>' +
				'</div>' +
                //'<div>' +
                //'</div>' +

	
                '<div id="worker-assignment-wrapper">' +
                    '<button type="button" class="ui primary button" onClick="addWorkerAssignment()">Add</button>' +
                    '<br><br>' +
                    '<table id="assigned-worker-list-table" class="ui celled red table">' +
                        '<thead>' +
                            '<tr>' +
                                '<th>Name</th>' +
                                '<th>Job</th>' +
                                '<th>Start</th>' +
								'<th>End</th>' +
								'<th>Description</th>' +
								'<th>Reimburse</th>' +
								'<th>Status</th>' +
                                '<th>Action</th>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody></tbody>' +
                    '</table>' +
                '</div>' +

		'<div id="edit-assignment-wrapper">' +
		'<div><div class="ui input twelve wide field"><b><h4 id="worker-name">Joe</h4></b></div></div><br>'+
		'<div class="ui grid">'+
		                     '<div class="six wide field">' +
                                '<div class="ui calendar dt">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
                                        '<input type="text" id="edit-worker-start-date" name="StartDate" placeholder="Start Date" />' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="six wide field">' +
                                '<div class="ui calendar dt">' +
                                    '<div class="ui input left icon">' +
                                        '<i class="calendar icon"></i>' +
                                        '<input type="text" id="edit-worker-end-date" name="EndDate" placeholder="End Date" />' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
						'<div class="seven wide field">' +
									'<input id="edit-assignment-description" class="message-box" value="" placeholder="Description" rows="2" style="resize: none;"/>' +
						'</div>' +
	   '</div>'+
		'<h3 style="color: #008590">Reimbursement note <a href="#" class="edit-reinburst-button" data-tooltip="Add reimbursement details"><i class="teal plus circle icon add-field-icon"></i></a></h3>' +
		'<div id="edit-re-inburst-note-wrapper" class="edit-re-inburst-note-wrapper"></div>' +
		'<div><button onClick="backToWorkerAssignments()" type="button" class="ui blue button">Cancel</button>' +
		'<button id="edit-assignment-btn" type="button" class="ui blue button">Save</button></div>' +
		'</div>' +

         '</div>';
}

function getAlterHistoryTab(alterHistory) {
	var history = 'No record';
	//if (alterHistory.length>0 && alterHistory != null) {
	//	history = '';
	//	history =history+ "RESERVATION ID : <b>" + alterHistory[0].ThreadId + "</b><br/>LISTING ID : <b>" + alterHistory[0].ListingId + "</b><br/><br/><hr/><br/>";

	//	for (var i = alterHistory.length - 1; i >= 0; i--) {
	//		history = history +
	//			"Detail was updated in " + moment(alterHistory[i].DateUpdated).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY - hh:mm A") + "<br/>" +
	//			"Price : <b>" + alterHistory[i].Price.toFixed(2) + "</b><br/>" +
	//			"Price Difference : <b>" + alterHistory[i].PriceDifference.toFixed(2) + "</b><br/>" +
	//			"Check-In Date : <b>" + moment(alterHistory[i].CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY - hh:mm A") + "</b><br/>" +
	//			"Check-Out Date : <b>" + moment(alterHistory[i].CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY - hh:mm A") + "</b><br/>" +
	//			"Adults : <b>" + alterHistory[i].Adults + "</b><br/>" +
	//			"Children : <b>" + alterHistory[i].Children + "</b><br/>" +
	//			"Guest Count : <b>" + alterHistory[i].GuestCount + "</b><br/><br/>" +
	//			((i == alterHistory.length - 1)
	//				?
	//				(
	//					"<button>" +
	//					(
	//						alterHistory[i].PriceDifference.toFixed(2) < 0
	//							? ("Refund " + alterHistory[i].PriceDifference.toFixed(2).replace("-", ""))
	//							: ("Request " + alterHistory[i].PriceDifference.toFixed(2))
	//					) +
	//					"</button><br/><br/><hr/><br/>"
	//				)
	//				: "");


	//	}
		
	//}
	return '<div class="ui tab' + tabIsActive("alter-history") + '" data-tab="alter-history">' +
		'<div style="margin-left:50px;">' +
		history +
		'</div>' +
		'</div>';
}

function getReviewsTab(GuestName) {
    var hostHtml = quickModal.hostReviews.length == 0 ? "No Reviews Given" : "";
    var guestHtml = quickModal.guestReviews.length == 0 ? "No Reviews Given" : "";
    var hostReviews = quickModal.hostReviews;
    var guestReviews = quickModal.guestReviews;

    for (var i = 0; i < quickModal.hostReviews.length; i++) {
        hostHtml +=
            "<div class='ui divider'/>" +
            "<div class='ui grid'>" +
                "<div class='two wide column'>" +
                    "<img class='ui tiny avatar image' src='" + hostReviews[i].ProfilePictureUrl + "'/>" +
                    "<br />" +
                    "<center>" + hostReviews[i].Name + "</center>" +
                "</div>" +
                "<div class='thirteen wide column'>" +
                    "<b>\"" + hostReviews[i].Comments + "\"</b>" +
                    "<br />" +
                    "<p style='float: right;'><i>" + moment(hostReviews[i].CreatedAt).format("MMMM YYYY") + "</i></p>" +
                "</div>" +
            "</div>";
    }

    for (var i = 0; i < quickModal.guestReviews.length; i++) {
        guestHtml +=
            "<div class='ui divider'/>" +
            "<div class='ui grid'>" +
                "<div class='two wide column'>" +
                    "<img class='ui tiny avatar image' src='" + guestReviews[i].ProfilePictureUrl + "'/>" +
                    "<br />" +
                    "<center>" + guestReviews[i].Name + "</center>" +
                "</div>" +
                "<div class='thirteen wide column'>" +
                    "<b>\"" + guestReviews[i].Comments + "\"</b>" +
                    "<br />" +
                    "<p style='float: right;'><i>" + moment(guestReviews[i].CreatedAt).format("MMMM YYYY") + "</i></p>" +
                "</div>" +
            "</div>";
    }

    return '<div class="ui tab' + tabIsActive("reviews") + '" data-tab="reviews">' +
		'<div style="margin-left:50px;">' +
            '<div class="ui grid" style="margin-top: 5px; margin-bottom: 5px; margin-right: 5px;">' +
                '<div class="five wide column">' +
                '</div>' +
                '<div class="five wide column">' +
                '</div>' +
			    '<select class="ui fluid dropdown selection six wide column" onchange="switchReviews(this.value)">' +
			        '<option value="host-reviews" selected>Reviews for '+GuestName+'</option>' +
			        '<option value="guest-reviews">Reviews by '+GuestName+'</option>' +
			    '</select>' +
            '</div>' +
            '<div id="host-reviews-div">' +
		        hostHtml +
            '</div>' +
            '<div id="guest-reviews-div" hidden>' +
		        guestHtml +
            '</div>' +
		'</div>' +
		'</div>';
}

function switchReviews(reviewPage) {
    if (reviewPage == "host-reviews") {
        $("#guest-reviews-div").hide();
        $("#host-reviews-div").show();
    }
    else if (reviewPage == "guest-reviews") {
        $("#host-reviews-div").hide();
        $("#guest-reviews-div").show();
    }
}
function SearchTab(booking) {
	(booking.GuestFullName != null ? booking.GuestFullName : booking.GuestName)
	var fname = booking.GuestName;
	var lname = booking.GuestFullName != null ? booking.GuestFullName.replace(booking.GuestName, '').trim():'';
	var html = '<div class="ui tab" data-tab="search-people">'+
					'<div id="search-people-form" class="ui form">'+
						'<div class="fields" style="margin-top:3px">'+
							'<div class="field">'+
								'<div class="ui input">'+
									'<input autocomplete="off" type="text" id="search-firstname" value="'+fname+'" placeholder="First name">'+
								'</div>'+
							'</div>'+
							'<div class="field">'+
								'<div class="ui input">'+
									'<input autocomplete="off" type="text" id="search-lastname" value="'+lname+'" placeholder="Last name">'+
								'</div>'+
							'</div>'+
							'<div class="field">'+
								'<div class="ui input">'+
									'<input autocomplete="off" type="text" id="search-address" value="'+booking.Location+'" placeholder="City">'+
								'</div>'+
							'</div>'+
							'<div class="field">'+
								'<div class="ui input">'+
									'<input autocomplete="off" type="text" id="search-age" placeholder="Age">'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="fields" style="margin-top:3px">'+
							'<div class="field">'+
								'<div class="ui input">'+
									'<input autocomplete="off" type="text" id="search-email" placeholder="Email">'+
								'</div>'+
							'</div>'+
							'<div class="field" style="margin-top:3px">'+
								'<button id="search-people-btn" class="ui blue right labeled icon button">'+
									'Search<i class="search icon"></i>'+
								'</button>'+
							'</div>'+
							'<div class="field" style="margin-top:3px">'+
								'<button id="clear-filter" class="ui blue button">'+
									'Clear Filter'+
								'</button>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<div id="possible-person">'+
					'</div>'+
					'</div>';
	return html;
}
function getCctvTab() {
	
	videoHtml = '<div class="ui tab' + tabIsActive("cctv") + '" data-tab="cctv" class="conversation">';
	if (quickModal.devices != null && quickModal.devices.length!=0) {
		var cameraIds = '<option value="">Select Camera</option><option value="0">All</option>';
		var reservationDates = '<option value="">Select Date</option>';
		//var cctv = JSON.parse(quickModal.cctv);
		var devices = quickModal.devices;
		for (var i = 0; i < devices.length; i++) {
			cameraIds = cameraIds + '<option value="' + devices[i].Id + '">' + devices[i].Name + '</option>';
		}

		var checkInDate = quickModal.selectedBooking.TimeZoneId != null ? moment(quickModal.selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM DD,YYYY') : moment(quickModal.selectedBooking.CheckInDate).format('MMM DD,YYYY');
		var checkOutDate = quickModal.selectedBooking.TimeZoneId != null ? moment(quickModal.selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM DD,YYYY') : moment(quickModal.selectedBooking.CheckOutDate).format('MMM DD,YYYY');
		for (var d = new Date(checkInDate); d <= new Date(checkOutDate); d.setDate(d.getDate() + 1)) {
			reservationDates = reservationDates + '<option value="' + moment(d).format('MMM DD,YYYY') + '">' + moment(d).format('MMM DD,YYYY') + '</option>';
		}
		//while (now.isSameOrBefore(checkOutDate)) {
		
		//	now.add(1, 'days');
		//}
		videoHtml = videoHtml +
			'<div class="ui form" style="float: left; background-color: white;">' +
			'<div class="field">' +
			'<label>Camera</label>' +
			'<select id="cctv-camera" class="ui fluid dropdown selection">' +
			cameraIds +
			'</select>' +
			'</div>' +
			'<div class="field">' +
			'<label>Date</label>' +
			'<select id="cctv-date" class="ui fluid dropdown selection">' +
			reservationDates +
			'</select>' +
			'</div>' +
			'<div class="field">' +
			'<label>Notes</label>' +
			'<textarea style="height: 30px;" id="cctv-note" placeholder="Your notes here..." disabled></textarea>' +
			'</div>' +
			'<div class="field" style="margin-top: 10px; margin-bottom: 5px; margin-left: 150px;">' +
			'<button class="ui tiny button blue" id="cctv-save-note" disabled>Save</button>' +
			'</div>' +
			'</div>';
			videoHtml = videoHtml +
			'<center>' +
			'<div class="conversation" id="cctv-videos" style="margin-left: 20px;">' +
			'<div style="margin-top: 220px;">Select camera and date...</div>' +
			'</div>' +
			'</center>' +
			'</div>';

		return videoHtml;
	} else { return videoHtml + 'No Record Found!'; }
	
}

function getCctvFootageNote(camera, date) {
    var note = quickModal.cctvFootageNotes;
    var textAreaNote = "";

    if (note != null) {
        for (var i = 0; i < note.length; i++) {
            if (note[i].Camera == camera && moment(note[i].ReservationDate).format("MMM DD,YYYY") == date) {
                textAreaNote = note[i].Notes;
                break;
            }
        }
    }
    return textAreaNote;
}

function DeviceSiteType(site) {
	switch (site) {
		case 0: return "Arlo"
		case 1: return "Blink"
		case 4: return "Wyze"
	}
}
function onCctvDropdownChanged() {
	$(document).on("change", "#cctv-camera,#cctv-date", function () {
        var deviceId = $("#cctv-camera").val();
        var date = $("#cctv-date").val();
        //var note = getCctvFootageNote(cameraDropdownValue, dateDropdownValue);
		var note = "";
        var videoHtml = '';

		$.ajax({
			type: "GET",
			url: "/Video/GetVideosByDateAndCamera",
			data: {
				date: date,
				deviceId: deviceId,
				startDate: (quickModal.selectedBooking.TimeZoneId != null ? moment(quickModal.selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY") : moment(quickModal.selectedBooking.CheckInDate).format("MMM D, YYYY")),
				endDate: (quickModal.selectedBooking.TimeZoneId != null ? moment(quickModal.selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY") : moment(quickModal.selectedBooking.CheckOutDate).format("MMM D, YYYY")),
				listingId: quickModal.selectedBooking.PropertyId
			},
			success: function (result) {
				if (result.success) {
					var videos = result.videos;
					if (videos.length > 0) {
						for (var i = 0; i < videos.length; i++) {
							$("#cctv-note").val(note);

							videoHtml = videoHtml +
								'<div>' +
								'<video width="400" height="200"  poster="' + videos[i].Video.VideoUrl.replace("mp4", "jpg") + '" preload="none"controls>' +
								'<source src="' + videos[i].Video.VideoUrl + '" type="video/mp4">' +
								'your browser does not support html5 video.' +
								'</video>' +
								'</div>' +
								'<div class="fields">' +
								'<div class="field">' +
								'<h5>' + DeviceSiteType(videos[i].Account.SiteType) + ' : Camera "' + videos[i].Device.Name + '", ' + moment(videos[i].Video.DateCreated).format('MMM D, YYYY hh:mm A') + '</h5>' +
								'</div>' +
								'<div class="field">' +
								'<input type="checkbox"> Watched</input>' +
								'<div class="ui checked checkbox"><input type="checkbox" videoId="' + videos[i].Video.Id + '" ' + (videos[i].Video.IsSaved ? 'checked' : '') + ' class="save-video"><label>Save</label></div><a href="' + videos[i].Video.VideoUrl + '" download="' + videos[i].Device.Name + " " + moment(videos[i].Video.DateCreated).format('MMM D YYYY hh mm A') +'"><button class="ui mini button" data-tooltip="Download"><i class="icon download"></i></button></a><button class="ui mini red button delete-video" videoId="' + videos[i].Video.Id + '" data-tooltip="Delete"><i class="icon trash alternate outline"></i></button>'+
								'</div>' +
								'</div>' +
								'<div class="ui divider"></div>';
						}
						$("#cctv-videos").html('<div class="ui divider"></div>' + videoHtml);

						if (deviceId != "0") {
							$("#cctv-note").removeAttr("disabled");
							$("#cctv-save-note").removeAttr("disabled");
						}
					}
					else {
						$("#cctv-videos").html('<div style="margin-top: 150px;">No CCTV footage for this camera and date...</div>');
						$("#cctv-note").prop("disabled", true);
						$("#cctv-save-note").prop("disabled", true);
					}
				}
			}
		});
    });
}
$(document).on("click", ".delete-video", function (e) {
	var videoId = $(this).attr('videoId');
	var parent = $(this).closest('.div-video');
	swal({
		title: "Are you sure you want to delete video?",
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Delete",
		showCancelButton: true,
		closeOnConfirm: true,
	},
		function () {
			$.ajax({
				type: 'POST',
				url: "/Video/DeleteVideo",
				data: { videoId: videoId },
				success: function (data) {
					if (data.success) {
						parent.remove();
					}
				}
			});
		});

});
$(document).on("click", ".save-video", function (e) {
	var videoId = $(this).attr('videoId');
	var isSave = $(this).prop("checked")
	$.ajax({
		type: 'POST',
		url: "/Video/SaveVideo",
		data: { videoId: videoId, isSave: isSave },
		success: function (data) {

		}
	});

});
function onCctvNoteSaved() {
    $(document).on('click', "#cctv-save-note", function () {
        var button = $(this);
        button.addClass("loading");
        $.ajax({
            type: "GET",
            url: "/Booking/UpdateCctvFootageNote",
            data: {
                inquiryLocalId: quickModal.selectedBooking.Id,
                note: $("#cctv-note").val(),
                date: $("#cctv-date").val(),
                camera: $("#cctv-camera").val()
            },
            success: function (result) {
                if (result.success) {
                    quickModal.cctvFootageNotes = result.notes;
                    swal("Note successfully saved.", "", "success");
                    button.removeClass("loading");
                }
            },
            error: function (error) {
                swal("Error on saving note.", "", "error");
                button.removeClass("loading");
            }
        });
    });
}

//function onSelectVariable() {
function RenderTaskStatistics() {
	var html = '<div class="ui statistics">' +
		'<div class="blue statistic">' +
		'<div class="total-task-value value"></div>' +
		'<div class="label">TOTAL TASK</div>' +
		'</div>' +
		'<div class="green statistic">' +
		'<div class="completed-task-value value"></div>' +
		'<div class="label">Completed Task</div>' +
		'</div>' +
		'<div class="red statistic">' +
		'<div class="pending-task-value value"></div>' +
		'<div class="label">Pending Task</div>' +
		'</div>' +
		'</div>';
	return html;
}

function UpdateTaskStatisticCount(data) {
	var totalTaskCount = data.TotalTaskCount;
	var completedTaskCount = data.CompletedTaskCount;
	var pendingTaskCount = data.PendingTaskCount;

	$(".total-task-value").text(totalTaskCount);
	$(".completed-task-value").text(completedTaskCount);
	$(".pending-task-value").text(pendingTaskCount);

}


function onClickAddCustomTaskButton() {
  
	$(".add_custom_task_button").click(function () {

		var html = '<tr name="custom_task">' +
			'<td><textarea class="custom-task" name="custom-task-template-name"></textarea></td>' +
			'<td><textarea class="custom-task" name="custom-task-title"></textarea></td>' +
			'<td><textarea class="custom-task" name="custom-task-description"></textarea></td>' +
			'<td>' +
			'<div class="ui toggle checkbox">' +
			'<input type="checkbox" name="custom-task-status">' +
			'</div>' +
			'</td>' +
			'<td></td>' +
			'<td></td>' +
			'</tr>';

		//TODO: 4/10/2017 Need to do checking, if the current booking got no task, need to remove the "No Task Available Row"
		$.ajax({
			type: "GET",
			url: "/Task/GetInquiryTask",
			data: {
				inquiryId: quickModal.booking.InquiryId
			},
			success: function (data) {
				debugger;
				if (data != null) {
					var totalTaskCount = data.TotalTaskCount;
					var currentCustomTask = $("#task-list-table > tbody > tr[name=\"custom_task\"]");

					if (totalTaskCount <= 0 && currentCustomTask.length <= 0) {
						$('#task-list-table > tbody').find('tr').remove();
					}
					$("#task-list-table > tbody").append(html);
					$('.ui.checkbox').checkbox();
					$('#update-task-btn').attr('disabled', false);
					//TODO: 4/10/2017, continue tonight for the insert operation for add new custom task
				}
				$('#quick-action-modal').modal('refresh');
			},
			error: function (error) {
				swal("Error on loading task list.", "", "error");
			}
		});
	});
}

function deleteInquiryTask(inquiryTaskId) {
	$.ajax({
		type: "POST",
		url: "/Task/DeleteInquiryTask",
		data: {
			inquiryTaskId: inquiryTaskId
		},
		success: function (data) {
			if (data.success) {
				swal("Task Deleted Successfully", "", "success");
				onLoadInquiryTaskList();
			} else {
				swal("Fail to Delete Task", "", "error");
			}
			$('#quick-action-modal').modal('refresh');
		},
		error: function (error) {
			swal("Error on loading task list.", "", "error");
		}
	});
}

	$(document).on('change', '#template-variable', function () {
	//$('#template-variable').on('change', function () {
		//alert($(this).val());
		var variable = $(this).val();
		
		if (variable == "null" || variable == "undefined")
			$('#message-box').append("");
		else
			//$('#txtMsgContent').append(variable);
			$('#message-box').val($('#message-box').val() + variable);
		$('#send-message-btn').attr('disabled', false);
		//$('#txtMsgContent').text($('#txtMsgContent').text() + variable);
		//if(option == 1)
		//{
		//    var firstname = $('.active-thread').find('.author').text();
		//    $('#txtMsgContent').insertAtCaret(firstname);
		//}
	});
//}

$(document).on('change', '#template-messages', function () {
	if ($(this).val() != '') {
		var templateMessage = $(this).val();
		var extractedMessage = "";
		var potentialVariable = "";
		var extracting = false;

		for (var i = 0; i < templateMessage.length; i++) {
			if (templateMessage[i] != '[' && !extracting) {
				extractedMessage = extractedMessage + templateMessage[i];
				extracting = false;
			}
			else if (templateMessage[i] == ']') {
				potentialVariable = potentialVariable + templateMessage[i];
				var potentialVariableValue = $("#template-variable option:contains('" + potentialVariable + "')").val();
				//potentialVariable = potentialVariable.replace(' ', '');
				if (potentialVariableValue != "null" && potentialVariableValue != "undefined") {
					extractedMessage = extractedMessage + $("#template-variable option:contains('" + potentialVariable + "')").val();
				}

				potentialVariable = "";
				extracting = false;
			}
			else {
				potentialVariable = potentialVariable + templateMessage[i];
				extracting = true;
			}
		}

		$('#message-box').val(extractedMessage);
	}
});
function getTemplateMessage() {
	var options = "";
	$.ajax({
		type: 'POST',
		url: "/Messages/GetTemplates",
		dataType: "json",
		async: false,
		success: function (data) {
			if (data.success) {
				$.each(data.templates, function (i, v) {
					options += '<option value="' + v.Message + '">' + v.Title + '</option>';
				});
			}
		}
	});
	return options;
}

function tabMenuIsActive(page, dataTab) {
    if (page == dataTab) {
        activeTabMenu = page;
        return " active";
    }
    else return "";
}

function getTabMenu(page,rating) {

	var hasWorker = quickModal.selectedBooking.HasWorkerAssignment;
	var hasTravelEvent = quickModal.selectedBooking.HasTravelEvent;
	return '<div id="quick-action-tab" class="ui tabular menu">' +
		'<div class="item' + tabMenuIsActive(page, "booking") + '" data-tab="booking">Trip Info</div>' +
		(UserWorkerId == 0 ? ((page != "messager") ? '<div class="item' + tabMenuIsActive(page, "message-history") + '" data-tab="message-history">Message History</div>' : '') : '') +
		((quickModal.selectedBooking.Status == 'A' || (quickModal.selectedBooking.Status != 'I' && (hasTravelEvent))) ? '<div class="item' + tabMenuIsActive(page, "travel-events") + '" data-tab="travel-events">Travel Events</div>' : '') +
		(UserWorkerId == 0 ? ((quickModal.selectedBooking.Status == 'A' && page != "messager" || ((hasWorker))) ? '<div class="item' + tabMenuIsActive(page, "workers") + '" data-tab="workers">Workers</div>' : '') : '') +
			(UserWorkerId == 0 ? '<div class="item" data-tab="tasks' + tabMenuIsActive(page, "tasks") + '">Tasks</div>' : '') +
			((quickModal.selectedBooking.Status == 'A' || quickModal.selectedBooking.Status == 'PAYMENT_REQUEST_SENT') ? '<div class="item" data-tab="alter-history' + tabMenuIsActive(page, "alter-history") + '">Alter History</div>' : '') +
		//(quickModal.selectedBooking.Status == 'I' ? '<div class="item" data-tab="reviews' + tabMenuIsActive(page, "reviews") + '"><i class="star icon"></i>Reviews</div>' : '') +
		'<div class="item" data-tab="reviews' + tabMenuIsActive(page, "reviews") + '">' + (rating == null || rating.indexOf("null") >= 0 ? "No reviews" : rating) + '</div>' +
		(quickModal.selectedBooking.Status != 'I' && quickModal.hasVideo ? '<div class="item ' + tabMenuIsActive(page, "cctv") + '" data-tab="cctv">CCTV</div>' : '') +
		'<div class="item" data-tab="search-people">Search</div>'+
           '</div>';
}

function AssignWorkerCount(inquiryId) {
	var count = 0;
	$.ajax({
		type: "GET",
		url: "/Home/GetWorkerAssignments",
		data: { inquiryId: inquiryId },
		async: false,
		success: function (data) {
			count = data.data.length;
		}
	});
	return count;

}
function MenuChange() {
	var hasWorker = quickModal.selectedBooking.HasWorkerAssignment;
	var hasTravelEvent = quickModal.selectedBooking.HasTravelEvent;
	if (quickModal.selectedBooking.Status == 'A' || hasWorker || hasTravelEvent > 0)
	{
		$('.item.travel-events').remove();
		$('.item.workers').remove();
		$('[data-tab="travel-events"]').remove();
		$('[data-tab="workers"]').remove();
		$('.item.workers').length == 0 && $('.item.travel-events').length == 0 ? UserWorkerId == 0 ? $('#quick-action-tab').append('<div class="item travel-events" data-tab="travel-events">Travel Events</div><div class="item workers" data-tab="workers">Workers</div>') : '' : ''
		$('.table-scroll').append(getWorkerTab() + getTravelEventsTab());
		$('.menu .item').tab();
		onAddNote();
		onAddReinburstmentNote();
		onEditReinburstmentNote();
		onDeleteNote();
		onDeleteReinburstNote();
		onSelectedNoteChange();
		GetNotes(quickModal.selectedBooking.Id);
		onEditAssignmentSubmit();
		onLoadAssignedWorker();
		onSelectWorker();
		onEditWorkerAssignment();
		onDeleteWorkerAssignment();
		onComposeMessage();
		onMessageGuestSubmit();
		quickModal.workerAssignmentTable.ajax.reload(null, false);

		$('#worker-form-wrapper').hide();
		$('#worker-assignment-wrapper').show();
		$('#edit-assignment-wrapper').hide();
		$('#assign-worker-success-message-wrapper').hide();
		$('#assign-worker-error-message-wrapper').hide();
		$('.change-time-error-message').hide();
		$('.change-time-success-message').hide();
		$('.menu .item').tab();
		$('.ui.dropdown').dropdown();
		$('.ui.calendar.time').calendar({ type: 'time' });
		$('.ui.calendar.dt').calendar({ type: 'datetime' });
		$('#quick-action-tab .item').tab({
			onVisible: function (tabPath) {
				$('#quick-action-modal').modal('refresh');
				$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
			}
		});
	
	}
	else
	{
		$('.item.travel-events').remove();
		$('.item.workers').remove();
		$('[data-tab="travel-events"]').remove();
		$('[data-tab="workers"]').remove();

	}

}
function getDropdownOptions(bookings) {
	var options = ''
	//var index = 0;
	$.each(bookings, function (index, booking) {
		if (quickModal.selectedBooking.TimeZoneId != null && quickModal.selectedBooking.TimeZoneId != "") {
			options += '<option value="' + index + '" data-options="' + booking + '">' + moment(booking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM D, YYYY - ') + moment(booking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM D, YYYY') + '</option>';
		}
		else {
			options += '<option value="' + index + '" data-options="' + booking + '">' + moment(booking.CheckInDate).format('MMM D, YYYY - ') + moment(booking.CheckOutDate).format('MMM D, YYYY') + '</option>';

		}
		});
    return options;
}

//function onBookingDateRangeChange() {
//	//var selectedIndex = $(e).find(":selected").index();
//	var selectedIndex = $('#date-range-selector').find(":selected").index();
//    quickModal.selectedBooking = quickModal.booking.Inquiries[selectedIndex];
//    var selectedBooking = quickModal.selectedBooking;
//    var propertyName = selectedBooking.PropertyName;
//    var status = (selectedBooking.Status == 'I' ? 'Inquiry' : selectedBooking.Status == 'IC' ? 'Inquiry-Cancelled' : selectedBooking.Status == 'B' ? 'Booking' : selectedBooking.Status == 'BC' ? 'Booking-Cancelled' : selectedBooking.Status == 'A' ? 'Confirmed' : selectedBooking.Status == 'C' ? 'Confirmed-Cancelled' : selectedBooking.Status == 'NP' ? 'Inquiry' : '')
//	var reservationDateRange = (moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM D, YYYY - ') + moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM D, YYYY'));
//	var checkIn = moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY");
//	var checkOut = moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY");
//    var guestCount = (selectedBooking.GuestCount > 0 ? (selectedBooking.GuestCount + ' ' + (selectedBooking.GuestCount > 1 ? 'Guests ' : 'Guest ')) : '');
//    var confirmationCode = selectedBooking.ConfirmationCode;
//    var reservationCost = selectedBooking.ReservationCost;
//    var cleaningFee = selectedBooking.CleaningCost;
//    var airbnbFee = selectedBooking.AirbnbFee;
//    var totalPayout = selectedBooking.Payout;
//    var bookingActionHtml = ((selectedBooking.Status == 'I' || selectedBooking.Status == 'NP' || selectedBooking.Status == 'SO') ? getInquiryAction() : (selectedBooking.Status == 'B') ? bookingAction : ((selectedBooking.Status == 'A' && selectedBooking.IsActive && selectedBooking.IsAltered == false) ? tripCoordinationAction : ((selectedBooking.Status == 'A' && selectedBooking.IsActive && selectedBooking.IsAltered) ? tripCoordinationAlteredAction : '')))

//    $('#reservation-property-name').text(propertyName);
//    $('#reservation-status').text(status);
//    $('#reservation-guest-count').text(guestCount);
//    $('#reservation-confirmation-code').text(confirmationCode);
//    $('#reservation-check-in').text(checkIn);
//    $('#reservation-check-out').text(checkOut);
//    $('#reservation-cost').text(reservationCost);
//    $('#reservation-cleaning-fee').text(cleaningFee);
//    $('#reservation-airbnb-fee').text(airbnbFee);
//    $('#reservation-total-payout').text(totalPayout);
//    $('#booking-actions').html(bookingActionHtml);

//    $(this).parents('.message').hide();
//    $('#booking-action-form').html('');
//    $('#booking-actions-inquiry').show();
//    $('#trip-info-details').show();

//	$('#change-booking-time-form input[name=checkInTime]').val(moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('h:mm A'));
//	$('#change-booking-time-form input[name=checkOutTime]').val(moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('h:mm A'));

//    GetNotes(quickModal.selectedBooking.InquiryId);
//    quickModal.workerAssignmentTable.ajax.reload(null, false);
//    $('#quick-action-modal').modal('refresh');
//}







//$(document).on('click', '#date-range-selector', function () {
//	alert("DOcu Click!");
//});

$(document).on('change', '.date-range-selector', function () {
	var selectedIndex = $('#date-range-selector option:selected').val();
	//var selectedIndex = $('#date-range-selector').find(":selected").index();
	//var selectedIndex = $('#date-range-selector').val();
	quickModal.selectedBooking = quickModal.booking.Inquiries[selectedIndex];
	var selectedBooking = quickModal.selectedBooking;
	var propertyName = selectedBooking.PropertyName;
	var status = (selectedBooking.Status == 'I' ? 'Inquiry' : selectedBooking.Status == 'IC' ? 'Inquiry-Cancelled' : selectedBooking.Status == 'B' ? 'Booking' : selectedBooking.Status == 'BC' ? 'Booking-Cancelled' : selectedBooking.Status == 'A' ? 'Confirmed' : selectedBooking.Status == 'C' ? 'Confirmed-Cancelled' : selectedBooking.Status == 'NP' ? 'Inquiry' : selectedBooking.Status == 'X' ? 'Expired' : '')
	var reservationDateRange = (moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM D, YYYY - ') + moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMM D, YYYY'));
	var checkIn = moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY");
	var checkOut = moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY");
	var guestCount = (selectedBooking.GuestCount > 0 ? (selectedBooking.GuestCount + ' ' + (selectedBooking.GuestCount > 1 ? 'Guests ' : 'Guest ')) : '');
	var confirmationCode = selectedBooking.ConfirmationCode;
	var reservationCost = selectedBooking.ReservationCost;
	var cleaningFee = selectedBooking.CleaningCost;
	var airbnbFee = selectedBooking.AirbnbFee;
	var totalPayout = selectedBooking.Payout;
	var inquiryDate = moment(selectedBooking.InquiryDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a");
	var confirmDate = moment(selectedBooking.ConfirmationDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a");
	var bookingCancelDate = moment(selectedBooking.BookingCancelDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a");
	var confirmCancelDate = moment(selectedBooking.BookingConfirmCancelDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY h:m a");
	var bookingActionHtml = ((selectedBooking.Status == 'I' || selectedBooking.Status == 'NP' || selectedBooking.Status == 'SO') ? '<div class="three mini buttons"><button style="margin: 1%" class="ui blue button js-btn_pre-approve" onClick="onPreApproveInquiry()">Pre-Approve</button>'(selectedBooking.IsSpecialOfferSent ? '<button style="margin: 1%" class="ui blue button js-btn_withdraw-special-offer" onClick ="onWithdrawSpecialOffer()">Withdraw Special Offer</button>' : '<button style="margin: 1%" class="ui blue button js-btn_send-special-offer" onClick="onSpecialOffer()">Send Special Offer</button>') + '<button style="margin: 1%" class="ui blue button js-btn_cancel-reservation" onClick="onDeclineInquiry()">Decline</button>' + '</div>' : (selectedBooking.Status == 'B') ? bookingAction : ((selectedBooking.Status == 'A' && selectedBooking.IsActive && selectedBooking.IsAltered == false) ? tripCoordinationAction : ((selectedBooking.Status == 'A' && selectedBooking.IsActive && selectedBooking.IsAltered) ? tripCoordinationAlteredAction : '')))
	var sumAmount = selectedBooking.SumPerNight;
	if (selectedBooking.InquiryDate != null) {
		$('#inq-h5').removeAttr('hidden');
	}
	else {
		$('#inq-h5').attr('hidden','');
	}

	if (selectedBooking.ConfirmationDate != null) {
		$('#confirm-date-h5').removeAttr('hidden');
	}
	else {
		$('#confirm-date-h5').attr('hidden','');
	}
	if (selectedBooking.BookingCancelDate != null) {
		$('#decline-h5').removeAttr('hidden');
	}
	else {
		$('#decline-h5').attr('hidden','');
	}
	if (selectedBooking.BookingConfirmCancelDate != null) {
		$('#confirm-cancel-h5').removeAttr('hidden');
	}
	else {
		$('#confirm-cancel-h5').attr('hidden','');
	}
	MenuChange();
	$('#reservation-sum-night').text(sumAmount);
	$('#booking-cancel-date').text(bookingCancelDate);
	$('#confirm-cancel-date').text(confirmCancelDate);
	$('#reservation-property-name').text(propertyName);
	$('#reservation-status').text(status);
	$('#reservation-guest-count').text(guestCount);
	$('#reservation-confirmation-code').text(confirmationCode);
	$('#reservation-check-in').text(checkIn);
	$('#reservation-check-out').text(checkOut);
	$('#reservation-cost').text(reservationCost);
	$('#reservation-cleaning-fee').text(cleaningFee);
	$('#reservation-airbnb-fee').text(airbnbFee);
	$('#reservation-total-payout').text(totalPayout);
	$('#booking-actions').html(bookingActionHtml);
	$('#confirm-date').text(confirmDate);
	$('#inq-date').text(inquiryDate);
	$(this).parents('.message').hide();
	$('#booking-action-form').html('');
	$('#booking-actions-inquiry').show();
	$('#trip-info-details').show();
	$('#change-booking-time-form input[name=checkInTime]').val(moment(selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format('h:mm A'));
	$('#change-booking-time-form input[name=checkOutTime]').val(moment(selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format('h:mm A'));
	getPaymentTab();
	MultibookingPopulatePaymentHistory(selectedBooking);
});

function getHeader(booking) {
	var selectedBooking = quickModal.selectedBooking;
	
	return '<div class="ui items">' +
		'<div class="item">' +
		'<div class="ui tiny image">' +
		'<img src="' + booking.GuestProfilePictureUrl + '">' +
		'</div>' +
		'<div class="content">' +
		'<div style="margin-right: 10%" class="header view-inquiry-guest-name">' + (booking.GuestFullName != null ? booking.GuestFullName : booking.GuestName) + '</div>' +
		(booking.JoinDate == null || booking.JoinDate.toLowerCase().indexOf("null") >= 0 ? '' : '<p><i class="calendar check outline icon"></i>'  +booking.JoinDate + '</p>')+
		(booking.Location == null ? '' : '<p><i class="home icon"></i>Lives in ' + booking.Location + '</p>') +
		(selectedBooking.Email != null ? '<p><i class="envelope outline icon"></i>' + selectedBooking.Email + '</p>' : '') +
		(selectedBooking.PhoneNumber != null ? '<p><i class="phone icon"></i>' + selectedBooking.PhoneNumber + '</p>' : '') +
		'<div ' + (booking.Inquiries.length > 1 ? '' : 'style="display: none;"') + '>' +
		'<select id="date-range-selector" class="ui compact selection dropdown date-range-selector">' +
		getDropdownOptions(booking.Inquiries) +
		'</select>' +
		'</div>' +
		'<div class="description">' +
		'<h4 id="reservation-property-name" style="margin-bottom: 0; color: #9b9b9b;" class="view-inquiry-property">' + selectedBooking.PropertyName + '</h4>' +
		'<p id="reservation-status" style="margin-bottom: 0; color: orange;" class="view-inquiry-status">' + (selectedBooking.Status == 'I' ? 'Inquiry' : selectedBooking.Status == 'IC' ? 'Inquiry-Cancelled' : selectedBooking.Status == 'B' ? 'Booking' : selectedBooking.Status == 'BC' ? 'Booking-Cancelled' : selectedBooking.Status == 'A' ? 'Confirmed' : selectedBooking.Status == 'C' ? 'Confirmed-Cancelled' : selectedBooking.Status == 'NP' ? 'Inquiry' : selectedBooking.Status=='X'?'Expired':'') + '</p>' +
		//'<p id="reservation-guest-count" style="margin-bottom: 0" class="view-inquiry-date-range">' + (selectedBooking.GuestCount > 0 ? (selectedBooking.GuestCount + ' ' + (selectedBooking.GuestCount > 1 ? 'Guests ' : 'Guest ')) : '') + ' (' + (moment(selectedBooking.CheckInDate).format('MMM D, YYYY - ') + moment(selectedBooking.CheckOutDate).format('MMM D, YYYY')) + ' )' + '</p>' +
		'</div>' +
		'</div>' +
		'<div style="float: right; font-size: 16px"><span style="margin-right: 10px">' + (booking.HostName != "" ? booking.HostName : "Local") + '<img class="ui avatar image" src="' +( booking.HostPicture != ""?booking.HostPicture :"/Content/img/eve.png") + '"></span><div><p style="color: brown;float: right; margin-right: 45px;">Host</p></div></div>' +
		'</div>' +
		'</div>';
}
	

function getSiteName(siteIndex) {
    switch (siteIndex) {
		case 0: return "PropertyDB";
        case 1: return "Airbnb";
		case 2: return "Vrbo";
        case 3: return "Booking";
        case 4: return "Homeaway";
    }
}

function getSource(source) {
	switch (source) {
		case 0: return "L"
		case 1: return "S";
		case 2: return "S";
		case 3: return "S";
		case 4: return "S";
		case 5: return "E";
		case 6: return "I";
		default: return "S";
	}
}
function getModalTemplateHtml(settings) {
	var booking = getBooking(settings.threadId, settings.inquiryId);
	quickModal.page = settings.page;
	quickModal.selectedBooking = booking.Inquiries[0];
	UserWorkerId = booking.UserWorkerId;
	
	var conversation = getConversation(settings.threadId);

    var hostId = settings.hostId;
    quickModal.threadId = settings.threadId;
    quickModal.booking_status = booking.InquiryStatus;	

	var template = '<div id="quick-action-modal" style="width: 60%;" class="ui fullscreen modal" data-thread-id=' + settings.threadId + '>' +
		'<i class="close icon"></i>' +
		'<div class="header">' + quickModal.title + '<img style="float: right;" class="ui mini circular image" src="/Content/img/' + getSiteName(booking.SiteType).toLowerCase() + '400.png" /><pre style="float: right;"> </pre><p id="reservation-guest-count" style="float: right; margin-bottom: 0" class="view-inquiry-date-range">from ' + getSiteName(booking.SiteType) + '</p> <div class="ui horizontal label">' + getSource(booking.Source) + '</div>' + '</div>' +
                            '<div class="content">' +
                                '<div class="table-scroll">' + 
                                getHeader(booking) +
                                getTabMenu(settings.page,booking.Rating) +
                                getBookingTab(booking) +
								(UserWorkerId==0?getWorkerTab():'') + 
								(UserWorkerId==0?(settings.page!="messager"?getMessageHistoryTab(conversation, hostId, settings.threadId,booking):''):'') +	
								(UserWorkerId == 0 ? getTaskTab() : '') +
                                getReviewsTab(booking.GuestName) +
                                (quickModal.selectedBooking.Status == 'A' ? getTravelEventsTab() : '') +
                                ((quickModal.selectedBooking.Status == 'A' || quickModal.selectedBooking.Status == 'PAYMENT_REQUEST_SENT') ? getAlterHistoryTab(quickModal.selectedBooking.AlterHistory) : '') +
								SearchTab(booking) +
								(quickModal.hasVideo ?getCctvTab():'') +
						
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
    return template;
}


String.prototype.count = function (s1) {
	var count = 0;

	for (var i = 0; i < s1.length; i++) {
		count = count + (s1[i] == "." ? 1 : 0);
	}

	return count;
}

function paymentTypeChanged(selectedElement) {
	if (selectedElement.value == "Full") {
		$("#payment-process").transition("fade");
		$("#send-payment-request-btn").transition("fade up");
	}
	else {
		$("#payment-process").transition("fade up");
		$("#send-payment-request-btn").transition("fade");
	}
}

function paymentMethodChanged(selectedElement) {
	$("#send-payment-request-btn").prop("disabled", false);
}

var lastValue = 0;
var balanceAfter = 0;
var paymentPrice = 0;
var remainingBalance = 0;

function paymentValueChanged() {
	//var paymentScale = $("#payment-scale").val();
	//var paymentValue = $("#payment-value").val();
	//var selectedBooking = quickModal.selectedBooking;
	//var isValid = paymentValue.match(/^[0-9]*\.?[0-9]*$/);
	//var hasManyPeriod = paymentValue.count('.') > 1;
	//var noPaymentMethod = $("#payment-method").val() == null || $("#payment-method").val() == "";

	//if (paymentScale == "Money") {
	//	if (isNaN(paymentValue[paymentValue.length - 1]) ||
	//		!isValid ||
	//		hasManyPeriod ||
	//		paymentValue > remainingBalance ||
	//		paymentValue <= 0 ||
	//		noPaymentMethod) {
	//		paymentValue = "";
	//		$("#add-partial-payment-btn").prop("disabled", true);
	//	}
	//	else {
	//		lastValue = paymentValue;
	//		$("#add-partial-payment-btn").prop("disabled", false);
	//	}
	//}
	//else {
	//	if (isNaN(paymentValue[paymentValue.length - 1]) ||
	//		!isValid ||
	//		hasManyPeriod ||
	//		paymentValue > 100 ||
	//		paymentValue < 1 ||
	//		noPaymentMethod) {
	//		paymentValue = "";
	//		$("#add-partial-payment-btn").prop("disabled", true);
	//	}
	//	else {
	//		lastValue = parseFloat((remainingBalance / 100) * paymentValue);
	//		$("#add-partial-payment-btn").prop("disabled", false);
	//	}
	//}
	//balanceAfter = (remainingBalance - (paymentValue == "" ? 0 : lastValue)).toFixed(2);
	//paymentPrice = parseFloat(paymentValue == "" ? 0 : lastValue).toFixed(2);

	//$("#remaining-balance-after").text(balanceAfter);
	//$("#partial-payment").text(paymentPrice);
}

function paymentTypeToByte(value) {
	switch (value) {
		case "Full": return 0;
		case "Partial": return 1;
	}
}

function paymentMethodToByte(value) {
	switch (value) {
		case "paypal": return 0;
		case "square": return 1;
		case "stripe": return 2;
	}
}

function toPaymentType(value) {
	switch (value) {
		case 0: return "Full";
		case 1: return "Partial";
	}
}

function toPaymentMethod(value, sitetype) {
	if (sitetype == 1) {
		return "Airbnb"
	}
	else if (sitetype == 2) {
		return "Vrbo"
	}
	else {
		switch (value) {
			case 0: return "PayPal";
			case 1: return "Square";
			case 2: return "Stripe";
		}
	}
}

function addAndSendPaymentRequestSubmit(btn) {
	btn.classList.add("loading");

	var pType = paymentTypeToByte($("#payment-type").val());
	var pMethod = paymentMethodToByte($("#payment-method").val());
	var name = $("#payment-name").val();
	//var price = parseFloat(pType == 0 ? quickModal.selectedBooking.Payout : paymentPrice);
	//var balance = parseFloat(pType == 0 ? 0 : balanceAfter);
	var code = quickModal.selectedBooking.ConfirmationCode;
	var email = $("#payment-email").val();
	var Payment = [];
	$('.unpaid-income').each(function (index, element) {

		var amount = $(this).find('.income-payment-amount').val();
		var id = $(this).find('.income-id').attr('income-id');
		var paymentId = $(this).find('.income-payment-amount').attr('payment-id');
		//JM 10/09/19 Check if payment is greater than balance if not don`t save
		if (parseFloat($(this).find('.income-payment-amount').attr('max')) < parseFloat(amount)) {
			isOverpaid = true;
		}
		var jsonPayment = {
			IncomeId: id,
			Amount: amount,
			PaymentId: paymentId
		};
		Payment.push(jsonPayment);

	});

	$.ajax({
		type: "POST",
		url: "/Booking/AddAndSendPaymentRequest",
		data: {
			paymentType: pType,
			paymentMethod: pMethod,
			paymentName: name,
			paymentPrice: $('#payment-value').val(),
			//balanceAfter: balance,
			confirmationCode: code,
			guestEmail: email,
			paymentString: JSON.stringify(Payment)

		},
		cache: false,
		success: function (result) {
			if (result.success) {
				swal({ title: "Success", text: result.message, type: "success" });
				location.reload();
			}
			else {
				swal(result.message, "", "error");
			}

			btn.classList.remove("loading");
		}
	});
}

function deletePaymentRequest(btn, paymentHistoryId) {
	debugger;
	btn.removeAttribute("data-tooltip");
	btn.classList.add("loading");

	$.ajax({
		type: "POST",
		url: "/Booking/DeletePaymentRequest",
		data: {
			paymentHistoryId: paymentHistoryId
		},
		cache: false,
		success: function (result) {

			var att = document.createAttribute("data-tooltip");
			att.value = "Delete";
			btn.setAttributeNode(att);
			btn.classList.remove("loading");

			if (result.success) {
				swal({ title: "Success", text: result.message, type: "success" });
				location.reload();
			}
			else {
				swal(result.message, "", "error");
			}
		}
	});
}

function reSendPaymentRequest(btn, paymentHistoryId) {
	btn.removeAttribute("data-tooltip");
	btn.classList.add("loading");

	$.ajax({
		type: "POST",
		url: "/Booking/ResendPaymentRequest",
		data: {
			confirmationCode: quickModal.selectedBooking.ConfirmationCode,
			paymentHistoryId: paymentHistoryId
		},
		cache: false,
		success: function (result) {

			var att = document.createAttribute("data-tooltip");
			att.value = "Re-send";
			btn.setAttributeNode(att);
			btn.classList.remove("loading");

			if (result.success) {
				swal({ title: "Success", text: result.message, type: "success" });
			}
			else {
				swal(result.message, "", "error");
			}
		}
	});
}

function getPaymentTab() {
	var selectedBooking = quickModal.selectedBooking;
	if (selectedBooking.SiteType == 3 || selectedBooking.SiteType == 0 || selectedBooking.SiteType == 2 || selectedBooking.SiteType == 1 || selectedBooking.SiteType == 4) {

		var _paymentHistory = selectedBooking.PaymentHistory == null || selectedBooking.PaymentHistory.length == 0 ? null : selectedBooking.PaymentHistory;
		balanceAfter = (_paymentHistory == null ? selectedBooking.Payout : _paymentHistory[_paymentHistory.length - 1].BalanceAfter);
		remainingBalance = _paymentHistory == null ? selectedBooking.Payout : balanceAfter;

		var income = selectedBooking.Incomes;
		var incomeHtml = '';
		if (income.length > 0) {
			for (var i = 0; i < income.length; i++) {
				incomeHtml += '<div class="fields unpaid-income"><div class="five wide field"><p class="income-id" income-id="' + income[i].Id + '">' + income[i].Description + " " + moment.utc(income[i].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + income[i].Amount + '</b></p></div>' +
					'<div class="six wide field">' +
					'<input type="number" min="0" max="' + income[i].Amount + '" payment-id="0" class="income-payment-amount" required placeholder="Payment Amount"/>' +
					'</div></div>';
			}
		}
		return (
			((selectedBooking.SiteType != 1 && selectedBooking.SiteType !=3 && selectedBooking.SiteType != 4)?
			'<div class="ui form" id="payment-form" '/* + (parseFloat(remainingBalance) == 0 ? 'hidden' : '') */+''+ '>' +
			'<br />' +
			'<h5>How do you want your guest to pay this reservation</h5>' +
			'<br />' +

			'<div style="width: 82%; margin-left: 65px;">' +

			'<div class="fields">' +

			'<div class="five wide field">' +
			'<label>Guest E-mail</label>' +
			'<input type="text" id="payment-email" placeholder="E-mail (i.e. example@gmail.com)" value="' + (selectedBooking.Email == null ? "" : selectedBooking.Email) + '">' +
			'</div>' +
			
			'<div class="five wide field" ' + (selectedBooking.SiteType== 0 ? 'hidden' : '') +'>' +
			'<label>Payment Type</label>' +
			'<select class="ui fluid dropdown" id="payment-type" onchange="paymentTypeChanged(this)" value="Full">' +
			'<option value="Full" ' + (selectedBooking.SiteType !=0?'selected':'')+'>Full Payment</option>' +
			'<option' + (selectedBooking.SiteType == 0 ? 'selected' : '') +' value="Partial">Partial Payment</option>' +
			'</select>' +
			'</div>' +

			'<div class="four wide field">' +
			'<label>Payment Method</label>' +
			'<div class="ui selection dropdown">' +
			'<input type="hidden" id="payment-method" onchange="paymentMethodChanged(this)">' +
			'<i class="dropdown icon"></i>' +
			'<div class="default text">Select Payment Method</div>' +

			'<div class="menu">' +
			'<div class="item" data-value="paypal">' +
			'<img class="ui mini image" src="/Content/img/paypalIcon.png">PayPal' +
			'</div>' +

			'<div class="item" data-value="square">' +
			'<img class="ui mini image" src="/Content/img/squareIcon.png">Square' +
			'</div>' +

			'<div class="item" data-value="stripe">' +
			'<img class="ui mini image" src="/Content/img/stripeIcon.png">Stripe' +
			'</div>' +
			'</div>' +
			'</div>' +

			'</div>' +

			'</div>' +
			'<br />' +
			 incomeHtml+
			'<div id="payment-process" ' + (selectedBooking.SiteType !=0?'hidden':'')+'>' +
			'<div class="inline fields">' +
			//'<div class="six wide field">' +
			//'<h5>Remaining Balance : <span>' + remainingBalance + '</span></h5>' +
			//'</div>' +
			//'<div class="six wide field">' +
			//'<h5>Partial Payment : <span id="partial-payment">0.0</span></h5>' +
			//'</div>' +
			//'<div class="six wide field">' +
			//'<h5>Balance After : <span id="remaining-balance-after">' + balanceAfter + '</span></h5>' +
			//'</div>' +
			'</div>' +

			'<h5>Add Payment :</h5>' +
			'<div class="fields">' +
			'<div class="six wide field">' +
			'<input type="text" id="payment-name" placeholder="Name of Payment Bill" value="Reservation\'s Payment Bill">' +
			'</div>' +
			'<div class="four wide field">' +
				'<input type="text" id="payment-value" placeholder="' + (selectedBooking.SiteType != 0 ? 'Amount or Percent' : 'Amount') +'" onkeyup="paymentValueChanged()" value="0">' +
			'</div>' +
			'<div class="two wide field" ' + (selectedBooking.SiteType == 0 ? 'disabled' : '') +'>' +
			'<select class="ui fluid dropdown" id="payment-scale" value="Money" onchange="paymentValueChanged()">' +
			'<option value="Money" selected>$</option>' +
			'<option value="Percentage">%</option>' +
			'</select>' +
			'</div>' +
			'<div class="five wide field">' +
				'<button class="ui blue button" id="add-partial-payment-btn" onclick="addAndSendPaymentRequestSubmit(this)" ' + (selectedBooking.SiteType != 0 || selectedBooking.SiteType != 2 ? 'disabled' : '') +'>Send Payment Request</button>' +
			'</div>' +
			'</div>' +
			'<br />' +
			'</div>' +

			(selectedBooking.SiteType !=0?'<div>' +
			'<button style="margin-left: 65%;" class="ui blue button" id="send-payment-request-btn" onclick="addAndSendPaymentRequestSubmit(this)" disabled>Send Payment Request</button>' +
			'</div>':'') +

			'</div>' +
			'</div>' +

			'<br />' 
				:'')+
			
			'<div id="payment-request-list">' +
			(
				_paymentHistory == null
					? '<h5>(No transaction history to show)</h5>'
					: '<h5>Your payment request history with this guest :</h5>' +
					populatePaymentHistory(selectedBooking)
			) +
			'</div>' +
			'<br />'
		);
	}
}

function MultibookingPopulatePaymentHistory(selectedBooking) {
	$('#payment-request-list').empty();
	selectedBooking.PaymentHistory.length ==0
		? $('#payment-request-list').append('<h5>(No transaction history to show)</h5>')
		: '<h5>Your payment request history with this guest :</h5>' +
	$('#payment-request-list').append(populatePaymentHistory(selectedBooking));
}

function populatePaymentHistory(selectedBooking) {
	var html ='<div><center><table style="width:80%; text-align:left;">';

	//html += '<tr><th>Method</th><th>Price</th><th>Balance</th><th>Paid</th><th>Date</th><th>Action</th></tr>'
	html += '<tr><th>Method</th><th>Price</th><th>Date</th><th>Action</th></tr>';

	for (var i = 0; i < selectedBooking.PaymentHistory.length; i++) {
				var paid = (selectedBooking.PaymentHistory[i]["Paid"] == 0 ? "No" : "Yes");
		html += '<tr>' +
			'<td>' + toPaymentMethod(selectedBooking.PaymentHistory[i]["PaymentMethod"], selectedBooking.SiteType) + '</td>' +
			'<td>' + selectedBooking.PaymentHistory[i]["PaymentPrice"] + '</td>' +
			//'<td>' + selectedBooking.PaymentHistory[i]["BalanceAfter"] + '</td>' +
			//'<td>' + paid + '</td>' +
			'<td>' + (quickModal.selectedBooking.TimeZoneId != null ? moment(selectedBooking.PaymentHistory[i]["DateCreated"]).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM DD,YYYY hh:mm a') : moment(selectedBooking.PaymentHistory[i]["DateCreated"]).format('MMMM DD,YYYY hh:mm a')) + '</td>' +

			'<td>' + ((selectedBooking.SiteType != 1 && selectedBooking.SiteType != 2) ?
				'<div class="ui tiny basic icon buttons">' +
				'<button class="ui centered input button ' + (paid == "Yes" ? 'disabled' : '') + '" onclick="deletePaymentRequest(this, ' + selectedBooking.PaymentHistory[i]["Id"] + ')" data-tooltip="Delete"><i class="delete icon"></i></button>' +
				'<button class="ui centered input button ' + (paid == "Yes" ? 'disabled' : '') + '" onclick="reSendPaymentRequest(this, ' + selectedBooking.PaymentHistory[i]["Id"] + ')" data-tooltip="Re-send"><i class="send icon"></i></button>' +
				'</div>' : '') + '</td>' +
			'</tr>';
	}
	return html + '</table></center></div>';
	//html = html +
	//	'<div style="width: 80%; margin-left: 65px;">' +
	//	'<hr width="100%"/>' +
	//	'<div class="row" style="float: top; margin top: 0px;">' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 15%;"><h5>METHOD</h5></div>' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 14%;"><h5>PRICE</h5></div>' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 14%;"><h5>BALANCE</h5></div>' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 10%;"><h5>PAID</h5></div>' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 30%;"><h5>DATE</h5></div>' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 2%;"><h5></h5></div>' +
	//	'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 15%;"><h5>ACTIONS</h5></div>' +
	//	'</div>' +
	//	'<br />' +
	//	'<hr width="100%"/>';


	//for (var i = 0; i < selectedBooking.PaymentHistory.length; i++) {

	//	var paid = (selectedBooking.PaymentHistory[i]["Paid"] == 0 ? "No" : "Yes");
	//	html = html +
	//		'<div class="row" style="float: top; margin top: 0px;" >' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 15%;">' + toPaymentMethod(selectedBooking.PaymentHistory[i]["PaymentMethod"], selectedBooking.SiteType) + '</div>' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 14%;">' + selectedBooking.PaymentHistory[i]["PaymentPrice"] + '</div>' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 14%;">' + selectedBooking.PaymentHistory[i]["BalanceAfter"] + '</div>' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 10%;">' + paid + '</div>' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 30%;">' + moment(selectedBooking.PaymentHistory[i]["DateCreated"]).format('MMMM DD,YYYY hh:mm a') + '</div>' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-top: 4px; width: 2%;"><h5></h5></div>' +
	//		'<div class="column" style="float: left; margin left: 5px; margin-bottom: 4px; width: 15%;">' +
	//		((selectedBooking.SiteType != 1 && selectedBooking.SiteType != 2 )?
	//		'<div class="ui tiny basic icon buttons">' +
	//		'<button class="ui centered input button ' + (paid == "Yes" ? 'disabled' : '') + '" onclick="deletePaymentRequest(this, ' + selectedBooking.PaymentHistory[i]["Id"] + ')" data-tooltip="Delete"><i class="delete icon"></i></button>' +
	//		'<button class="ui centered input button ' + (paid == "Yes" ? 'disabled' : '') + '" onclick="reSendPaymentRequest(this, ' + selectedBooking.PaymentHistory[i]["Id"] + ')" data-tooltip="Re-send"><i class="send icon"></i></button>' +
	//		'</div>':'')+
			
	//		'</div>' +
	//		'<br />' +
	//		'<hr width="100%"/>';
	//}
	//return html + '</table></center></div>';
}

function getBooking(threadId, inquiryId) {
    var booking = null;
    $.ajax({
        type: "GET",
        url: "/Booking/GetBookingDetailsByThreadId",
        data: { threadId: threadId, inquiryId: inquiryId },
        async: false,
        success: function (data) {
			console.log(data);
			if (data.isSuccess) {

				booking = data.booking;
				quickModal.devices = data.devices;
                quickModal.booking = data.booking;
                quickModal.booking_status = data.booking.InquiryStatus;
                quickModal.cctvFootageNotes = data.cctvFootageNotes;
                quickModal.hostReviews = data.hostReviews;
				quickModal.guestReviews = data.guestReviews;
				quickModal.pincode = data.pincode;
				quickModal.hasVideo = data.hasVideo;
            }
        }
    });
    return booking;
}

function getConversation(threadId) {

    var conversation = null;

    $.ajax({
        type: "GET",
        url: "/Booking/GetGuestConversation",
		data: { threadId: threadId },
		async: false,
		success: function (data) {

            if (data.success) {
                conversation = data;
            }
        }
    });

    return conversation;
}

function onSpecialOffer() {
    $(document).on('click', '.js-btn_send-special-offer', function () {
        
        if ($('#booking-action-form').children().length == 0) {
            $('#booking-action-form').append(specialOfferForm);
            $('.ui.dropdown').dropdown();
            $('#booking-actions-inquiry').hide();
            $('#trip-info-details').hide();
            $('#special-offer-summary').hide();
            $('#quick-action-modal').find('#success-message-wrapper').hide();
            $('#quick-action-modal').find('#error-message-wrapper').hide();
            $("#special-offer-form select[name=propertyId]").dropdown('set selected', quickModal.selectedBooking.PropertyId);
            $("#special-offer-form select[name=guestCount]").dropdown('set value', quickModal.selectedBooking.GuestCount);
			$("#special-offer-form input[name=startDate]").val(moment(quickModal.selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY"));
			$("#special-offer-form input[name=endDate]").val(moment(quickModal.selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY"));
            checkSpecialOffer();
            $('#quick-action-modal').modal('refresh');
            onSpecialOfferFormChange();
            onCloseSpecialOffer();
        }
    });
}

function onWithdrawSpecialOffer() {
    $(document).on('click', '.js-btn_withdraw-special-offer', function () {
        if ($('#booking-action-form').children().length == 0) {
            $('#booking-action-form').append(cancelSpecialOfferForm);
            $('#booking-actions-inquiry').hide();
            $('#trip-info-details').hide();
            $('#quick-action-modal').find('#success-message-wrapper').hide();
            $('#quick-action-modal').find('#error-message-wrapper').hide();
            $('#quick-action-modal').modal('refresh');
        }
    });
}

function withdrawSpecialOfferCloseForm() {
    $(this).parents('.message').hide();
    $('#booking-action-form').empty();
    $('#booking-actions-inquiry').show();
    $('#trip-info-details').show();
    $('#quick-action-modal').modal('refresh');
}

function withdrawSpecialOfferSubmit() {
    var threadId = quickModal.threadId;
    var hostId = quickModal.booking.HostId;

    $.ajax({
        type: 'POST',
        url: "/Booking/WithdrawSpecialOffer",
        data: { hostId: hostId, threadId: threadId },
        beforeSend: function () {
            $('.js-btn_withdraw-special-offer').addClass('loading');
        },
        success: function (data) {
            if (data.success) {
                $('.inquiry-error-message-wrapper').hide();
                $('.inquiry-success-message-wrapper').find('.header').text('Special offer has been successfully withdrawed.');
                $('.inquiry-success-message-wrapper').show();
                $('#booking-action-form').empty();
                $('#booking-actions-inquiry').empty();
                quickModal.booking.IsSpecialOfferSent = false;
                $('#booking-actions-inquiry').append(getInquiryAction());
                $('#booking-actions-inquiry').show();
                $('#trip-info-details').show();
                $('#quick-action-modal').modal('refresh');
            }
            else {

            }
            $('.js-btn_withdraw-special-offer').removeClass('loading');
        }
    });
}

function onCloseSpecialOffer() {
    $(document).off('click', '#special-offer-back-btn');
    $(document).on('click', '#special-offer-back-btn', function () {
        $(this).parents('.message').transition('fade').remove();
        $('#booking-actions-inquiry').show();
        $('#trip-info-details').show();
    });
}

function onSpecialOfferFormChange() {
    $("#special-offer-form select[name=guestCount]").on('change', function () {
        checkSpecialOffer();
    });
    $('#special-offer-start-date').calendar({
        type: 'date', onChange: function (date, text, mode) {
            $("#special-offer-form input[name=startDate]").val(text);
            checkSpecialOffer();
        }
    });
    $('#special-offer-end-date').calendar({
        type: 'date', onChange: function (date, text, mode) {
            $("#special-offer-form input[name=endDate]").val(text);
            checkSpecialOffer();
        }
    });
}

function checkSpecialOffer() {
    var hostId = quickModal.booking.HostId;
    var listingId = $("#special-offer-form select[name=propertyId]").val();
    var startDate = $("#special-offer-form input[name=startDate]").val();
    var endDate = $("#special-offer-form input[name=endDate]").val();
    var guestId = quickModal.selectedBooking.GuestId;
	var numOfGuest = $("#special-offer-form select[name=guestCount]").val();
	var currentStartDate = $('').val();
	var currentEndDate = $('').val();
	
    if (hostId != '' && listingId != '' && startDate != '' && endDate != '' && guestId != '' && numOfGuest != '') {
        $.ajax({
            type: 'POST',
            url: "/Booking/CheckSpecialOfferValidity",
            data: {
                hostId: hostId,
                listingId: listingId,
                guestId: guestId,
                startDate: startDate,
                endDate: endDate,
                numOfGuest: numOfGuest,
            },
            beforeSend: function () {
                $('#quick-action-modal').find('#success-message-wrapper').hide();
                $('#quick-action-modal').find('#error-message-wrapper').hide();
            },
            success: function (data) {
				if (data.result.IsAvailable) {
                    $('#error-message-wrapper').hide();
                    $('#special-offer-summary').show();
                    $("#special-offer-form input[name=price]").val(data.result.Subtotal);
                    $('#guest-amount-to-pay').text(data.result.GuestPays);
                    $('#host-amount-to-earn').text(data.result.HostEarns);
                    $('#special-offer-modal').modal('refresh');
                    $('#submit-special-offer-btn').removeClass('disabled');
                }
                else {
                    $('#special-offer-summary').hide();
                    $('#submit-special-offer-btn').removeClass('disabled').addClass('disabled');
                    $('#error-message-wrapper').find('p').text('Those dates are not available');
                    $('#error-message-wrapper').show();
                }
            }
        });
    }
}

function onSpecialOfferSubmit() {
    //$(document).off('click', '#submit-special-offer-btn');
    $(document).on('click', '#submit-special-offer-btn', function () {
        var threadId = quickModal.threadId;
        var hostId = quickModal.booking.HostId;
        var listingId = $("#special-offer-form select[name=propertyId]").val();
        var startDate = $("#special-offer-form input[name=startDate]").val();
        var endDate = $("#special-offer-form input[name=endDate]").val();
        var numOfGuest = $("#special-offer-form select[name=guestCount]").val();
        var price = $("#special-offer-form input[name=subTotal]").val();

        if (hostId != '' && listingId != '' && startDate != '' && endDate != '' && threadId != '' && numOfGuest != '' && price != '') {
            $.ajax({
                type: 'POST',
                url: "/Booking/SendSpecialOffer",
                data: {
                    hostId: hostId,
                    listingId: listingId,
                    threadId: threadId,
                    numOfGuest: numOfGuest,
                    startDate: startDate,
                    endDate: endDate,
                    price: price
                },
                success: function (data) {
                    if (data.success) {
                        $('.inquiry-error-message-wrapper').hide();
                        $('.inquiry-success-message-wrapper').find('.header').text('Special offer has been sent.');
                        $('.inquiry-success-message-wrapper').show();
                        $('#booking-action-form').empty();
                        $('#booking-actions-inquiry').empty();
                        quickModal.booking.IsSpecialOfferSent = true;
                        $('#booking-actions-inquiry').append(getInquiryAction());
                        $('#booking-actions-inquiry').show();
                        $('#trip-info-details').show();
                    }
                    else {
                        $('.inquiry-success-message-wrapper').hide();
                        $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
                        $('.inquiry-error-message-wrapper').show();
                    }
                }
            });
        }
    });
}

function onComposeMessage() {
    
    $(document).off('keyup', '#message-box');
    $(document).on('keyup', '#message-box', function () {
        if ($(this).val() == '') {
            $('#send-message-btn').attr('disabled', true);
        }
        else {
            $('#send-message-btn').attr('disabled', false);
        }
    });
}

function unBindMessagesJsEvents() {
    $(document).off("click", "#send-message-btn");
    $(document).off('keyup', '#message-box');
}


function onMessageEndSubmit() {
	$(document).off('click', '#end-message-btn,#short-term-end-message-btn');
	$(document).on('click', '#end-message-btn,#short-term-end-message-btn', function () {
		$.ajax({
			type: 'POST',
			url: "/Messages/EndThread",
			data: {
				threadId: $('#dvConvo').attr('data-thread-id')
			},
			success: function (data) {
				swal("This thread mark as end!", "", "success");
			}
		});
	});
}
function onMessageGuestSubmit() {
    
    $(document).off('click', '#send-message-btn');
	$(document).on('click', '#send-message-btn', function () {

        var threadId = $('#dvConvo').attr('data-thread-id');
        var hostId = $('#dvConvo').attr('data-host-id');
		var message = $('#message-box').val();
		var siteType = quickModal.selectedBooking.SiteType;

        if (message != '') {
			$.ajax({
				type: 'POST',
				url: "/Messages/SendMessageToInbox",
				data: { siteType: siteType, hostId: hostId, threadId: threadId, message: message },
				beforeSend: function () {
					$('#send-message-btn').addClass('loading');
				},
				success: function (data) {
					if (data.success) {
						$('#message-box').css('border-color', 'rgba(34,36,38,.15)');
						$('#send-message-btn').removeClass('orange').addClass('teal');
						$('#send-message-btn').text('Send');
						$('#message-box').val('');
						$('#send-message-btn').attr('disabled', true);
						$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
							'<h5>' + message + '</h5>' +
							'<p>' + moment().format("h:mm A MMM D, YYYY") + '</p>' +
							'</div>');
						$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
					}
					//else {
					//	$('#message-box').css('border-color', 'orange');
					//	$('#message-box').css('border-color', 'orange');
					//	$('#send-message-btn').removeClass('teal').addClass('orange');
					//	$('#send-message-btn').text('Retry');
					//}
					$('#send-message-btn').removeClass('loading');
				}
			});
        }
    });
}



function onMessageClose() {
    $('.message .close').on('click', function () {
        $(this).closest('.message').hide();
    });
}

/****************************************** Start accept/decline/special offer booking *********************************************/
function acceptBooking() {
    if ($('#booking-action-form').children().length == 0) {
        $('#booking-action-form').append(acceptBookingForm);
        $('#booking-actions').hide();
        $('#trip-info-details').hide();
        $('#quick-action-modal').find('#success-message-wrapper').hide();
        $('#quick-action-modal').find('#error-message-wrapper').hide();
        $('#quick-action-modal').modal('refresh');
    }
}
function declineBooking() {
    if ($('#booking-action-form').children().length == 0) {
        $('#booking-action-form').append(declineBookingForm);
        $('#booking-actions').hide();
        $('#trip-info-details').hide();
        $('#quick-action-modal').find('#success-message-wrapper').hide();
        $('#quick-action-modal').find('#error-message-wrapper').hide();
        $('#quick-action-modal').modal('refresh');
    }
}
function acceptBookingFormClose() {
    $(this).parents('.message').hide();
    $('#booking-action-form').empty();
    $('#booking-actions').show();
    $('#trip-info-details').show();
}
function declineBookingFormClose() {
    $(this).parents('.message').hide();
    $('#booking-action-form').empty();
    $('#booking-actions').show();
    $('#trip-info-details').show();
}
function bookingDeclineTypeMessage(event) {
    if ($(event).val() == '') {
        $('#decline-booking-btn').removeClass('disabled').addClass('disabled');
    }
    else {
		$('#decline-booking-btn').removeClass('disabled');
    }
}
function acceptBookingSubmit() {
    var hostId = quickModal.booking.HostId;
    var reservationCode = quickModal.selectedBooking.ConfirmationCode;
    var reservationId = quickModal.selectedBooking.InquiryId;
    var message = $('#accept_booking_message').val();
    var siteType = quickModal.selectedBooking.SiteType;
    var threadId = quickModal.threadId;

    $.ajax({
        type: 'POST',
        url: "/Booking/AcceptBooking",
        data: { siteType: siteType, hostId: hostId, reservationCode: (siteType == 1 ? reservationCode : reservationId), message: message, threadId: threadId },
        success: function (data) {
			if (data.success) {
				$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
					'<h5>' + message + '</h5>' +
					'<p>' + moment().format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
				$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
                //$('.inquiry-error-message-wrapper').hide();
                //$('.inquiry-success-message-wrapper').find('.header').text('Booking Confirmed.');
                //$('.inquiry-success-message-wrapper').show();
                //$('.inquiry-success-message-wrapper').removeClass('hidden');
                $('#booking-action-form').empty();
                //$('#booking-actions').empty();
                $('#booking-actions').show();
                $('#trip-info-details').show();
                $('#reservation-status').text('Confirmed');
                quickModal.selectedBooking.Status = "A";
                quickModal.selectedBooking.IsActive = true;
            }
            else {
                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
                $('.inquiry-error-message-wrapper').show();
                $('.inquiry-error-message-wrapper').removeClass('hidden');
            }
        }
    });
}
//Joem 8/4/2018 Decline old code
//function declineBookingSubmit() {
//    var reservationCode = quickModal.selectedBooking.ConfirmationCode;
//    var reservationId = quickModal.selectedBooking.InquiryId;
//    var hostId = quickModal.booking.HostId;
//    var reason = $('#decline_booking_reason').val();
//    var siteType = quickModal.selectedBooking.SiteType;
//    var threadId = quickModal.threadId;
//    debugger;

//    $.ajax({
//        type: 'POST',
//        url: "/Booking/DeclineBooking",
//        data: { siteType: siteType, hostId: hostId, reservationCode: (siteType == 1 ? reservationCode : reservationId), message: reason, threadId: threadId },
//        success: function (data) {
//            if (data.success) {
//                $('.inquiry-error-message-wrapper').hide();
//                $('.inquiry-success-message-wrapper').find('.header').text('Booking Declined.');
//                $('.inquiry-success-message-wrapper').show();
//                $('.inquiry-success-message-wrapper').removeClass('hidden');
//                $('#booking-action-form').empty();
//                $('#booking-actions').empty();
//                $('#booking-actions').show();
//                $('#trip-info-details').show();
//                $('#reservation-status').text('Booking-Cancelled');
//                quickModal.selectedBooking.Status = "BC";
//            }
//            else {
//                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
//                $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
//                $('.inquiry-error-message-wrapper').show();
//                $('.inquiry-error-message-wrapper').removeClass('hidden');
//            }
//        }
//    });
//}

function declineBookingSubmit() {
	var reservationCode = quickModal.selectedBooking.ConfirmationCode;
	var reservationId = quickModal.selectedBooking.InquiryId;
	var hostId = quickModal.booking.HostId;
	var reason = $('#decline_booking_reason').val();
	var siteType = quickModal.selectedBooking.SiteType;
	var threadId = quickModal.threadId;
	var listingId = quickModal.selectedBooking.PropertyId;
	
	$.ajax({
		type: 'POST',
		url: "/Booking/DeclineBooking",
		data: { siteType: siteType, hostId: hostId, reservationCode: (siteType == 1 || siteType == 3 ? reservationCode : reservationId), message: reason, threadId: threadId, listingid: listingId },
		success: function (data) {
			if (data.success) {
				//$('.inquiry-error-message-wrapper').hide();
				//$('.inquiry-success-message-wrapper').find('.header').text('Booking Declined.');
				//$('.inquiry-success-message-wrapper').show();
				//$('.inquiry-success-message-wrapper').removeClass('hidden');
				//$('#booking-action-form').empty();
				$('#booking-actions').empty();
				//$('#booking-actions').show();
				$('#trip-info-details').show();
				//$('#reservation-status').text('Booking-Cancelled');
				//quickModal.selectedBooking.Status = "BC";
			}
			//else {
			//	$('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
			//	$('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
			//	$('.inquiry-error-message-wrapper').show();
			//	$('.inquiry-error-message-wrapper').removeClass('hidden');
			//}
		}
	});
}
function unBindPendingBookingEvents() {
    $(document).off('click', '.js-btn_accept-booking');
    $(document).off('click', '.js-btn_decline-booking');
    $(document).off('click', '#accept-booking-back-btn');
    $(document).off('click', '#decline-booking-back-btn');
    $(document).off('keyup', '#decline_booking_reason');
    $(document).off('click', '#accept-booking-btn');
    $(document).off('click', '#decline-booking-btn');
}
/****************************************** End accept/decline booking *********************************************/

/****************************************** Start alter booking *********************************************/


function alterBooking() {
    if (quickModal.selectedBooking.SiteType == 2 || quickModal.selectedBooking.SiteType == 4) {
		if ($('#booking-action-form').children().length == 0) {
			$(document).off('change', '#alter-booking-form select[name=guestCount]');
			$('#booking-action-form').append(alterBookingForm);
			//onAlterBookingFormChange();
			$('#booking-actions-inquiry').hide();
			$('#trip-info-details').hide();
			$('#quick-action-modal').find('#success-message-wrapper').hide();
			$('#quick-action-modal').find('#error-message-wrapper').hide();
			$('#alter-booking-form').find('#alter-success-message-wrapper').hide();
			$('#alter-booking-form').find('#alter-error-message-wrapper').hide();
			$('#quick-action-modal').modal('refresh');
			$("#alter-booking-form select[name=propertyId]").dropdown('set selected', quickModal.selectedBooking.PropertyId);
			$("#alter-booking-form select[name=guestCount]").dropdown('set value', quickModal.selectedBooking.GuestCount);

			$(document).on('change', '#alter-booking-form select[name=guestCount]', function () {
				//alert("HAHA");
				onCheckReservationAvailability();
			});
			$('#alter-booking-start-date').calendar({
				type: 'date', onChange: function (date, text, mode) {
					$("#alter-booking-form input[name=checkIn]").val(text);
					onCheckReservationAvailability();

				}
			});
			$('#alter-booking-end-date').calendar({
				type: 'date', onChange: function (date, text, mode) {
					$("#alter-booking-form input[name=checkOut]").val(text);
					onCheckReservationAvailability();
				}
			});
			$("#alter-booking-form input[name=checkIn]").val(moment(quickModal.selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY"));
			$("#alter-booking-form input[name=checkOut]").val(moment(quickModal.selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMM D, YYYY"));
			$("#alter-booking-form input[name=subtotal]").val(quickModal.selectedBooking.ReservationCost);
		}
	}
	else if (quickModal.selectedBooking.SiteType == 1) {
		if ($('#booking-action-form').children().length == 0) {
			$(document).off('change', '#alter-booking-form select[name=guestCount]');
			$('#booking-action-form').append(airbnbAlterBookingForm);
			//onAlterBookingFormChange();
			$('#booking-actions-inquiry').hide();
			$('#trip-info-details').hide();
			$('#quick-action-modal').find('#success-message-wrapper').hide();
			$('#quick-action-modal').find('#error-message-wrapper').hide();
			$('#alter-booking-form').find('#alter-success-message-wrapper').hide();
			$('#alter-booking-form').find('#alter-error-message-wrapper').hide();
			$('#quick-action-modal').modal('refresh');
			$("#alter-booking-form select[name=propertyId]").dropdown('set selected', quickModal.selectedBooking.PropertyId);
			$("#alter-booking-form select[name=guestCount]").dropdown('set value', quickModal.selectedBooking.GuestCount);

			$(document).on('change', '#alter-booking-form select[name=guestCount]', function () {
				//alert("HAHA!");
				onCheckReservationAvailability();
			});
			$('#alter-booking-start-date').calendar({
				type: 'date', onChange: function (date, text, mode) {
					$("#alter-booking-form input[name=checkIn]").val(text);
					onCheckReservationAvailability();

				}
			});
			$('#alter-booking-end-date').calendar({
				type: 'date', onChange: function (date, text, mode) {
					$("#alter-booking-form input[name=checkOut]").val(text);
					onCheckReservationAvailability();
				}
			});
			$("#alter-booking-form input[name=checkIn]").val(moment(quickModal.selectedBooking.CheckInDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMMM D, YYYY"));
			$("#alter-booking-form input[name=checkOut]").val(moment(quickModal.selectedBooking.CheckOutDate).tz(quickModal.selectedBooking.TimeZoneId).format("MMMM D, YYYY"));
			$("#alter-booking-form input[name=subtotal]").val(quickModal.selectedBooking.ReservationCost);
		}
	}
}

function sendRefund() {
    if ($('#booking-action-form').children().length == 0) {
        $('#booking-action-form').append(sendRefundForm);
        //onAlterBookingFormChange();
        $('#booking-actions-inquiry').hide();
        $('#trip-info-details').hide();
        $('#quick-action-modal').find('#success-message-wrapper').hide();
        $('#quick-action-modal').find('#error-message-wrapper').hide();
        $('#send-refund-form').find('#send-refund-success-message-wrapper').hide();
        $('#send-refund-form').find('#send-refund-error-message-wrapper').hide();
        $('#quick-action-modal').modal('refresh');
    }
}

function addPaymentRequest() {
    if ($('#booking-action-form').children().length == 0) {
        $('#booking-action-form').append(addPaymentRequestForm);
        //onAlterBookingFormChange();
        $('#booking-actions-inquiry').hide();
        $('#trip-info-details').hide();
        $('#quick-action-modal').find('#success-message-wrapper').hide();
        $('#quick-action-modal').find('#error-message-wrapper').hide();
        $('#add-payment-request-form').find('#add-payment-request-success-message-wrapper').hide();
        $('#add-payment-request-form').find('#add-payment-request-error-message-wrapper').hide();
        $('#quick-action-modal').modal('refresh');

        $('#add-payment-request-date').calendar({
            type: 'date', onChange: function (date, text, mode) {
                $("#add-payment-request-form input[name=dueDate]").val(text);
            }
        });
    }
}

function cancelAlteration() {
    //$(document).off('click', '.js-btn_cancel-alteration');
    //$(document).on('click', '.js-btn_cancel-alteration', function () {
    if ($('#booking-action-form').children().length == 0) {
        $('#booking-action-form').append(cancelAlterationForm);
        $('#booking-actions-inquiry').hide();
        $('#trip-info-details').hide();
        $('#quick-action-modal').find('#success-message-wrapper').hide();
        $('#quick-action-modal').find('#error-message-wrapper').hide();
        $('#quick-action-modal').modal('refresh');
    }
    //});
}

function onAlterFormClose() {
    $(this).parents('.message').hide();
    $('#booking-action-form').html('');
    $('#booking-actions-inquiry').show();
    $('#trip-info-details').show();
}

function cancelAlterationFormClose() {
    $(this).parents('.message').hide();
    $('#booking-action-form').empty();
    $('#booking-actions-inquiry').show();
    $('#trip-info-details').show();
    $('#quick-action-modal').modal('refresh');
}
function cancelAlterationSubmit() {
    var reservationCode = quickModal.selectedBooking.ConfirmationCode;
    var reservationId = quickModal.selectedBooking.InquiryId;
    var hostId = quickModal.booking.HostId;
    var siteType = quickModal.selectedBooking.SiteType;

    $.ajax({
        type: 'POST',
        url: "/Booking/CancelAlteration",
        data: { siteType: siteType, hostId: hostId, reservationCode: (siteType == 1 ? reservationCode : reservationId) },
        success: function (data) {
            if (data.success) {
                $('.inquiry-error-message-wrapper').hide();
                $('.inquiry-success-message-wrapper').find('.header').text('Alteration request has been successfully cancelled.');
                $('.inquiry-success-message-wrapper').show();
                $('.inquiry-success-message-wrapper').removeClass('hidden');
                quickModal.selectedBooking.IsAltered = false;
                $('#booking-action-form').empty();
                $('#booking-actions').empty();
                $('#booking-actions').append(tripCoordinationAction);
                $('#booking-actions').show();
                $('#trip-info-details').show();
                $('#quick-action-modal').modal('refresh');
            }
            else {
                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
                $('.inquiry-error-message-wrapper').show();
                $('.inquiry-error-message-wrapper').removeClass('hidden');
                $('#quick-action-modal').modal('refresh');
            }
        }
    });
}


$('alter-booking-check-in').change(function () {
		onCheckReservationAvailability();
});
$('alter-booking-check-out').change(function () {
		onCheckReservationAvailability();
});
$('#alter-booking-guest-count').change(function () {
	     
		onCheckReservationAvailability();
	});

function onCheckReservationAvailability() {
	if (true) {//quickModal.selectedBooking.Status == "A") {
		if (quickModal.selectedBooking.SiteType == '1') {
			var reservationCode = quickModal.selectedBooking.ConfirmationCode;
			var reservationId = quickModal.selectedBooking.InquiryId;
			//var siteType = quickModal.selectedBooking.SiteType;
			var hostId = quickModal.booking.HostId;
			var listingId = $('#alter-booking-property-id').val();/*quickModal.selectedBooking.PropertyId;*/
			var checkinDate = $('#alter-booking-check-in').val();
			var checkoutDate = $('#alter-booking-check-out').val();
			var price = $('#alter-booking-subtotal').val();
			var guests = $('#alter-booking-guest-count').val();
			var guestId = quickModal.selectedBooking.GuestId;
			var currentStartDate = moment($('#reservation-check-in').text()).format('MMM D,YYYY');
			var currentEndDate = moment($('#reservation-check-out').text()).format('MMM D,YYYY');


			$.ajax({
				type: 'POST',
				url: "/Booking/CheckSpecialOfferValidity",
				data: {
					hostId: hostId,
					listingId: listingId,
					guestId: guestId,
					startDate: checkinDate,
					endDate: checkoutDate,
					numOfGuest: guests,
					submitAlteration: false,
					currentStartDate: currentStartDate,
					currentEndDate: currentEndDate

				},
				success: function (data) {
					if (data.success) {
						$('#alter-error-message-wrapper').hide();
						$('#alter-success-message-wrapper').find('.header').text(data.message);
						//$('#alter-success-message-wrapper').find('p').text('Difference : ' + data.difference);
						//$('#alter-success-message-wrapper').show();
						$('#alter-booking-submit-btn').removeClass('disabled');
						//$('#alter-booking-subtotal').val(data.difference.match(/\d+/)[0]);
						$('#alter-booking-subtotal').val(data.result.Subtotal)

					}
					else {
						$('#alter-success-message-wrapper').hide();
						$('#alter-error-message-wrapper').find('p').text("Invalid data");
						$('#alter-error-message-wrapper').show();
						$('#alter-booking-submit-btn').removeClass('disabled').addClass('disabled');
					}
				}
			});
		}
		//VRBO 
		else if (quickModal.selectedBooking.SiteType == '2' || quickModal.selectedBooking.SiteType == '4') {
			var hostId = quickModal.booking.HostId;
		    var siteType = quickModal.selectedBooking.SiteType;
			var listingId = quickModal.selectedBooking.PropertyId//$('#alter-booking-property-id').val();/*quickModal.selectedBooking.PropertyId;*/
			var checkinDate = $('#alter-booking-check-in').val();
			var checkoutDate = $('#alter-booking-check-out').val();
			var price = $('#alter-booking-subtotal').val();
			var threadId = quickModal.threadId;
			var adults = parseInt($("#alter-booking-adults-count").val());
			var children = parseInt($("#alter-booking-children-count").val());


			$.ajax({
				type: 'POST',
				url: "/Booking/CheckReservationValidity",
				data: {
					hostId: hostId,
				    siteType: siteType,
					conversationId: threadId,
					checkInDate: checkinDate,
					checkoutDate: checkoutDate,
					price: price,
					adults: adults,
					children: children,
					bookingStatus: quickModal.selectedBooking.Status
				},
				success: function (data) {
					if (data.success) {
						$('#alter-error-message-wrapper').hide();
						$('#alter-success-message-wrapper').find('.header').text(data.message);
						//$('#alter-success-message-wrapper').find('p').text('Difference : ' + data.difference);
						//$('#alter-success-message-wrapper').show();
						$('#alter-booking-submit-btn').removeClass('disabled');
						//$('#alter-booking-subtotal').val(data.difference.match(/\d+/)[0]);
						$('#alter-booking-subtotal').val(data.subtotal)

					}
					else {
						$('#alter-success-message-wrapper').hide();
						$('#alter-error-message-wrapper').find('p').text("Invalid data");
						$('#alter-error-message-wrapper').show();
						$('#alter-booking-submit-btn').removeClass('disabled').addClass('disabled');
					}
				}
			});

		}
    }
}

function onChangeAlterPrice(e) {
    if ($(e).val() == '') {
        $('#alter-booking-submit-btn').addClass('disabled');
    }
    else {
        $('#alter-booking-submit-btn').removeClass('disabled');
    }
}

function onSubmitSendRefund() {
    var siteType = quickModal.selectedBooking.SiteType;
    var hostId = quickModal.booking.HostId;
    var threadId = quickModal.threadId;
    var amount = $("#send-refund-amount").val();
    var description = $("#send-refund-description").val();

   
    $.ajax({
        type: 'POST',
        url: "/Booking/SendRefund",
        data: {
            hostId: hostId,
            siteType: siteType,
            threadId: threadId,
            amount: amount,
            description: description
        },
        success: function (data) {
            if (data.success) {
                $('.inquiry-error-message-wrapper').hide();
                $('.inquiry-success-message-wrapper').find('.header').text(data.message);
                $('.inquiry-success-message-wrapper').show();
                $('.inquiry-success-message-wrapper').removeClass('hidden');
                quickModal.selectedBooking.IsAltered = true;
                //$('#booking-action-form').empty();
                //$('#booking-actions').empty();
                ////$('#booking-actions').append(tripCoordinationAlteredAction);
                //$('#booking-actions').show();
                $('#trip-info-details').show();
            }
            else {
                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                $('.inquiry-error-message-wrapper').find('.header').text(data.message);
                $('.inquiry-error-message-wrapper').show();
                $('.inquiry-error-message-wrapper').removeClass('hidden');
            }
        }
    });
}

function onSubmitAddPaymentRequest() {
    var siteType = quickModal.selectedBooking.SiteType;
    var hostId = quickModal.booking.HostId;
    var threadId = quickModal.threadId;
    var description = $("#add-payment-request-description").val();
    var dueDate = $("#add-payment-request-due-date").val();
    var message = $("#add-payment-request-message").val();
    var amount = $("#add-payment-request-amount").val();

    $.ajax({
        type: 'POST',
        url: "/Booking/AddPaymentRequest",
        data: {
            hostId: hostId,
            siteType: siteType,
            threadId: threadId,
            description: description,
            dueDate: dueDate,
            message: message,
            amount: amount
        },
        success: function (data) {
            if (data.success) {
                $('.inquiry-error-message-wrapper').hide();
                $('.inquiry-success-message-wrapper').find('.header').text(data.message);
                $('.inquiry-success-message-wrapper').show();
                $('.inquiry-success-message-wrapper').removeClass('hidden');
                quickModal.selectedBooking.IsAltered = true;
                $('#booking-action-form').empty();
                //$('#booking-actions').empty();
                //$('#booking-actions').append(tripCoordinationAlteredAction);
                //$('#booking-actions').show();
                $('#trip-info-details').show();
            }
            else {
                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                $('.inquiry-error-message-wrapper').find('.header').text(data.message);
                $('.inquiry-error-message-wrapper').show();
                $('.inquiry-error-message-wrapper').removeClass('hidden');
            }
        }
    });
}

function onSubmitAlteration() {
    var reservationCode = quickModal.selectedBooking.ConfirmationCode;
    var reservationId = quickModal.selectedBooking.InquiryId;
    var id = quickModal.selectedBooking.Id;
    var hostId = quickModal.booking.HostId;
    var siteType = quickModal.selectedBooking.SiteType;
    var threadId = quickModal.threadId;
    var listingId = quickModal.selectedBooking.PropertyId;
    var checkinDate = $('#alter-booking-check-in').val();
    var checkoutDate = $('#alter-booking-check-out').val();
	var price = $('#alter-booking-subtotal').val();
	var guests= $('#alter-booking-guest-count').val();
    var adults = parseInt($("#alter-booking-adults-count").val());
    var children = parseInt($("#alter-booking-children-count").val());
    var status = quickModal.selectedBooking.Status;
	var alterType = 1;
	$('#alter-booking-submit-btn').addClass("loading")
	$('#alter-booking-submit-btn').addClass("disabled")
    //var originalCheckInDate = moment(quickModal.selectedBooking.CheckInDate).format("MMM D, YYYY");
    //var originalCheckOutDate = moment(quickModal.selectedBooking.CheckOutDate).format("MMM D, YYYY");
    //var originalGuestCount = quickModal.selectedBooking.GuestCount;

    if (siteType == 1) {
        guests = parseInt($('#alter-booking-guest-count').val());
    }
    else if (siteType == 2 || siteType == 4) {
        guests = adults + children;

        if (status == "PAYMENT_REQUEST_SENT") {
            alterType = 2;
        }
        if (status == "A") {
            //if (guests <= originalGuestCount) {
            //    adults = null;
            //    children = null;
            //}
            //if (checkinDate == originalCheckInDate) {
            //    checkinDate = null;
            //}
            //if (checkoutDate == originalCheckOutDate) {
            //    checkoutDate = null;
            //}

            alterType = 3;
        }
    }
    $.ajax({
        type: 'POST',
        url: "/Booking/AlterBooking",
        data: {
            siteType: siteType,
            id: id,
            hostId: hostId,
            reservationCode: (siteType == 1 ? reservationCode : reservationId),
            listingId: listingId,
            checkinDate: checkinDate,
            checkoutDate: checkoutDate,
            price: price,
            guests: guests,
            adults: adults,
            children: children,
            submitAlteration: true,
            threadId: threadId,
            alterType: alterType
        },
		success: function (data) {
			$('#alter-booking-submit-btn').removeClass("loading")
			$('#alter-booking-submit-btn').removeClass("disabled")
			if (data.success) {
                $('.inquiry-error-message-wrapper').hide();
                $('.inquiry-success-message-wrapper').find('.header').text('Booking altered successfully');
                $('.inquiry-success-message-wrapper').show();
                $('.inquiry-success-message-wrapper').removeClass('hidden');
                quickModal.selectedBooking.IsAltered = true;
                $('#booking-action-form').empty();
                $('#booking-actions').empty();
                $('#booking-actions').append(tripCoordinationAlteredAction);
                $('#booking-actions').show();
                $('#trip-info-details').show();
            }
            else {
                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
                $('.inquiry-error-message-wrapper').show();
                $('.inquiry-error-message-wrapper').removeClass('hidden');
            }
        }
    });
}
function cancelReservation() {
    if ($('#booking-action-form').children().length == 0) {
        $('#booking-action-form').append(cancelReservationForm);
        $('#booking-actions').hide();
        $('#trip-info-details').hide();
        $('#quick-action-modal').find('#success-message-wrapper').hide();
        $('#quick-action-modal').find('#error-message-wrapper').hide();
        $('#quick-action-modal').modal('refresh');
    }
}
function backToCancelReservationForm() {
    $(this).parents('.message').hide();
    $('#booking-action-form').empty();
    $('#booking-actions').show();
    $('#trip-info-details').show();
    $('#quick-action-modal').modal('refresh');
}
//function cancelReservationSubmit() {
//    var hostId = quickModal.booking.HostId;
//    var siteType = quickModal.selectedBooking.SiteType;
//    var threadId = quickModal.threadId;
//    var message = "";
//	$.ajax({
//        type: 'POST',
//        url: "/Booking/WithdrawPreApproveInquiry",
//        data: {
//            siteType: siteType,
//            hostId: hostId,
//            threadId: threadId,
//            message: message
//        },
//        success: function (data) {
//            if (data.success) {
//                $('.inquiry-error-message-wrapper').hide();
//                $('.inquiry-success-message-wrapper').find('.header').text('Reservation successfully cancelled.');
//                $('.inquiry-success-message-wrapper').show();
//                $('#booking-action-form').empty();
//                $('#booking-actions').empty();
//                quickModal.selectedBooking.Status = "C";
//                $('#reservation-status').text('Cancelled');
//                $('#booking-actions').show();
//                $('#trip-info-details').show();
//                $('#quick-action-modal').modal('refresh');
//            }
//            else {
//                $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
//                $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
//                $('.inquiry-error-message-wrapper').show();
//                $('.inquiry-error-message-wrapper').removeClass('hidden');
//            }
//        }
//    });
//}
function unbindTripCoordinationJsEvents() {
    $(document).off('click', '.js-btn_change-booking');
    $(document).off('click', '.js-btn_cancel-alteration');
    $(document).off('click', '#alter-booking-back-btn');
    $(document).off('click', '#alter-booking-submit-btn');
    $(document).off('click', '#save-new-booking-time-btn');
}
/****************************************** End alter booking *********************************************/

/************************************** Start travel events booking ***************************************/
function NotesCount(inquiryId) {
	var count = 0;
	$.ajax({
		type: "GET",
		url: "/Messages/GetNotes",
		data: { InquiryId: inquiryId },
		dataType: "json",
		success: function (data) {
			count = data.result.length;
				}
	});
	return count;
}
function GetNotes(inquiryId) {

	$.ajax({
		type: "GET",
		url: "/Messages/GetNotes",
		data: { InquiryId: inquiryId },
		dataType: "json",
		async: true,
		success: function (data) {
			if (data.result.length > 0) { (UserWorkerId == 0 ? $('#note-save').removeClass('hidden') : ''); }
			
			$.each(data.result, function (index, value) {
				var type = value.NoteType;
				var noteHtml = '<div class="note">' +
					'<div class="fields">' +
					'<div class="seven wide field">' +
					'<select class="ui dropdown select-note">' +
					'<option ' + (type == 1 ? 'selected' : '') + ' value="1">Note</option>' +
					'<option ' + (type == 2 ? 'selected' : '') + ' value="2">Flight Arrival</option>' +
					'<option ' + (type == 3 ? 'selected' : '') + ' value="3">Flight Departure</option>' +
					'<option ' + (type == 4 ? 'selected' : '') + ' value="4">Meeting</option>' +
					'<option ' + (type == 5 ? 'selected' : '') + ' value="5">Drive Arrival</option>' +
					'<option ' + (type == 6 ? 'selected' : '') + ' value="6">Drive Departure</option>' +
					'<option ' + (type == 7 ? 'selected' : '') + ' value="7">Cruise Arrival</option>' +
					'<option ' + (type == 8 ? 'selected' : '') + ' value="8">Cruise Departure</option>' +
					'<option ' + (type == 9 ? 'selected' : '') + ' value="9">Train Arrival</option>' +
					'<option ' + (type == 10 ? 'selected' : '') + ' value="10">Train Departure</option>' +
					'<option ' + (type == 11 ? 'selected' : '') + ' value="11">Bus Arrival</option>' +
					'<option ' + (type == 12 ? 'selected' : '') + ' value="12">Bus Departure</option>' +
					'<option ' + (type == 13 ? 'selected' : '') + ' value="13">Group Checkin</option>' +
					'<option ' + (type == 14 ? 'selected' : '') + ' value="14">Group Checkout</option>' +
					'</select>' +
					'</div>' +
					(UserWorkerId==0?('<div class="four wide field">' +
					'<div class="note-actions">' +
					'<a href="#" class="delete-note-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
					'<a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>' +
					'</div>' +
					'</div>'):'') +
					'</div>' +
					'<div class="fields">' +
					'<div class="two wide field"></div>' +
					'<div class="fourteen wide field">' +
					'<div class="note-fields">' +
					'<div><input type="hidden" type="text" name="NumberOfPeople" placeholder="Number Of People" value="' + value.NumberOfPeople + '">' +
					'<input type="hidden" type="text" name="ContactPerson" placeholder="Contact Person" value="' + value.ContactPerson + '">' +
					'<input type="hidden" type="text" name="ContactNumber" placeholder="Contact Number" value="' + value.ContactNumber + '">' +
					'<input type="hidden" type="text" name="HashValue" placeholder="Hash Value" value="' + value.HashValue + '">' +
					'<input type="hidden" type="text" name="NoteId" placeholder="Note Id" value="' + value.Id + '"></div>' +
					'<div class="fields">';
				var noteFields = '';
				var noteWrapper = $('#inquiry-note-wrapper');
				//noteWrapper.empty();

				// Note
				if (type == 1) {
					noteFields =
						'<input type="text" name="InquiryNote" placeholder="Inquiry Note" value="' + value.InquiryNote + '">';
				}
				// Flight Arrival
				else if (type == 2) {
					noteFields =
						'<input type="text" name="People" placeholder="Number of People" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="Airline" placeholder="Airline" value="' + value.Airline + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="FlightNumber" placeholder="Flight Number" value="' + value.FlightNumber + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure" value="' + value.AirportCodeDeparture + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival" value="' + value.AirportCodeArrival + '">';
				}
				// Flight Departure
				else if (type == 3) {
					noteFields =
						'<input type="text" name="People" placeholder="Number of People" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="Airline" placeholder="Airline" value="' + value.Airline + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="FlightNumber" placeholder="Flight Number" value="' + value.FlightNumber + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure" value="' + value.AirportCodeDeparture + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival" value="' + value.AirportCodeArrival + '">';
				}
				// Meeting
				else if (type == 4) {
					noteFields =
						'<input type="text" name="MeetingPlace" placeholder="Meeting Place" value="' + value.MeetingPlace + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="NumberOfPeople" placeholder="Number of People" value="' + value.NumberOfPeople + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="MeetingDateTime" placeholder="Meeting Date/Time" value="' + moment(value.MeetingDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="IdentifiableFeature" placeholder="Identifiable Feature" value="' + value.IdentifiableFeature + '">';
				}  //TODO 10/11/2017: This part need to handle
				// Drive Arrival
				else if (type == 5) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="DepartureCity" placeholder="Departure City" value="' + value.DepartureCity + '">' +
						'</div>' +
						//'<div class="ui calendar dt fields">' +
						//'<div class="ui input left icon">' +
						//'<i class="calendar icon"></i>' +
						//'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).format('MMMM D, YYYY h:mm A') + '">' +
						//'</div>' +
						//'</div>' +
						'<div class="fields">' +
						'<input type="text" name="CarType" placeholder="Type of car" value="' + value.CarType + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="CarColor" placeholder="Color of car" value="' + value.CarColor + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="PlateNumber" placeholder="Plate Number" value="' + value.PlateNumber + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>';
				}
				// Drive Departure
				else if (type == 6) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="DepartureCity" placeholder="Departure City" value="' + value.DepartureCity + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="CarType" placeholder="Type of car" value="' + value.CarType + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="CarColor" placeholder="Color of car" value="' + value.CarColor + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="PlateNumber" placeholder="Plate Number" value="' + value.PlateNumber + '">' +
						'</div>';
					//'<div class="ui calendar dt fields">' +
					//'<div class="ui input left icon">' +
					//'<i class="calendar icon"></i>' +
					//'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).format('MMMM D, YYYY h:mm A') + '">' +
					//'</div>';
				}
				// Cruise Arrival
				else if (type == 7) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="CruiseName" placeholder="Cruise Name" value="' + value.CruiseName + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="DeparturePort" placeholder="Departure Port" value="' + value.DeparturePort + '">' +
						'</div>' +
						//'<div class="ui calendar dt fields">' +
						//'<div class="ui input left icon">' +
						//'<i class="calendar icon"></i>' +
						//'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).format('MMMM D, YYYY h:mm A') + '">' +
						//'</div>' +
						//'</div>' +
						'<div class="fields">' +
						'<input type="text" name="ArrivalPort" placeholder="Arrival Port" value="' + value.ArrivalPort + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>';
				}
				// Cruise Departure
				else if (type == 8) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="CruiseName" placeholder="Cruise Name" value="' + value.CruiseName + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="DeparturePort" placeholder="Departure Port" value="' + value.DeparturePort + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>';
					//'<div class="fields">' +
					//'<input type="text" name="ArrivalPort" placeholder="Arrival Port" value="' + value.ArrivalPort + '">' +
					//'</div>' +
					//'<div class="ui calendar dt fields">' +
					//'<div class="ui input left icon">' +
					//'<i class="calendar icon"></i>' +
					//'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).format('MMMM D, YYYY h:mm A') + '">' +
					//'</div>';
				}
				// Train Arrival
				else if (type == 9) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="Name" placeholder="Train Name" value="' + value.Name + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="TerminalStation" placeholder="Terminal Station" value="' + value.TerminalStation + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>';
				}
				// Train Departure
				else if (type == 10) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="Name" placeholder="Train Name" value="' + value.Name + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="TerminalStation" placeholder="Terminal Station" value="' + value.TerminalStation + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>';
				}
				// Bus Arrival
				else if (type == 11) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="Name" placeholder="Bus Name" value="' + value.Name + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="TerminalStation" placeholder="Terminal Station" value="' + value.TerminalStation + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time" value="' + moment(value.ArrivalDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>';
				}
				// Bus Departure
				else if (type == 12) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.People + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="Name" placeholder="Bus Name" value="' + value.Name + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="TerminalStation" placeholder="Terminal Station" value="' + value.TerminalStation + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time" value="' + moment(value.DepartureDateTime).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>';
				}
				//Group Checkin
				else if (type == 13) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.ContactPerson + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="NumberOfPeople" placeholder="Number of People" value="' + value.NumberOfPeople + '">' +
					'</div>' +
					'<div class="ui calendar dt fields">' +
					'<div class="ui input left icon">' +
					'<i class="calendar icon"></i>' +
					'<input type="text" name="GroupCheckin" placeholder="Group Checkin" value="' + moment(value.Checkin).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
					'</div>' +
					'</div>' 
				}
				else if (type == 14) {
					noteFields =
						'<input type="text" name="People" placeholder="Name" value="' + value.ContactPerson + '">' +
						'</div>' +
						'<div class="fields">' +
						'<input type="text" name="NumberOfPeople" placeholder="Number of People" value="' + value.NumberOfPeople + '">' +
						'</div>' +
						'<div class="ui calendar dt fields">' +
						'<div class="ui input left icon">' +
						'<i class="calendar icon"></i>' +
						'<input type="text" name="GroupCheckout" placeholder="Group Checkout" value="' + moment(value.Checkout).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A') + '">' +
						'</div>' +
						'</div>';
				}
				noteHtml += (noteFields + '</div></div></div></div></div>');
				noteWrapper.append(noteHtml);
				$('.ui.calendar.dt').calendar({ type: 'datetime' });
				$('.ui.dropdown').dropdown({ on: 'click' });
			});
		}
	});
}

function onAddReinburstmentNote() {
	$(document).on('click', '.add-reinburst-button', function () {
		//if (count < 5) {  //TODO 1/11/17 : Remove the hard-code 5 checking when add travel events and test whether can add more than 5 travel events from UI.
		var noteHtml =
			'<div class="re-inburst-note" reimbusement-id="0">' +
					'<div class="ui input twelve wide field">' +
						'<input type="text" name="ReinburstNote" placeholder="Note">' +
					'</div>' +
					'<div class="ui input twelve wide field">' +
						'<input type="text" name="Amount" placeholder="Amount">' +
					'</div>' +
			'<a href="#" class="delete-note-reinburst-button" data-tooltip="Remove"><i class="orange minus circle icon remove-field-icon"></i></a>' +
			'</div></br>';
		$('.re-inburst-note-wrapper').append(noteHtml);
		$('#quick-action-modal').modal('refresh');
		//}
	});
}

function onEditReinburstmentNote() {
	$(document).on('click', '.edit-reinburst-button', function () {
		//if (count < 5) {  //TODO 1/11/17 : Remove the hard-code 5 checking when add travel events and test whether can add more than 5 travel events from UI.

		var noteHtml =
			'<div class="re-inburst-note">' +
			'<div class="ui input twelve wide field">' +
			'<input type="text" name="ReinburstNote" placeholder="Note">' +
			'</div>' +
			'<div class="ui input twelve wide field">' +
			'<input type="text" name="Amount" placeholder="Amount">' +
			'</div>' +
			'<a href="#" class="delete-note-reinburst-button" data-tooltip="Remove"><i class="orange minus circle icon remove-field-icon"></i></a>' +
			'</div></br>';
		$('.edit-re-inburst-note-wrapper').append(noteHtml);
		$('#quick-action-modal').modal('refresh');
		//}
	});
}
function onAddNote() {
	$(document).on('click', '.add-note-button', function () {
		var count = $('#inquiry-note-wrapper').children().length;
		var inquiryNoteWrapper = $('#inquiry-note-wrapper');
		//if (count < 5) {  //TODO 1/11/17 : Remove the hard-code 5 checking when add travel events and test whether can add more than 5 travel events from UI.
		var noteHtml = '<div class="note">' +
			'<div class="fields">' +
			'<div class="seven wide field">' +
			'<select class="ui dropdown select-note">' +
			'<option value="1">Note</option>' +
			'<option value="2">Flight Arrival</option>' +
			'<option value="3">Flight Departure</option>' +
			'<option value="4">Meeting</option>' +
			'<option value="5">Drive Arrival</option>' +
			'<option value="6">Drive Departure</option>' +
			'<option value="7">Cruise Arrival</option>' +
			'<option value="8">Cruise Departure</option>' +
			'<option value="9">Train Arrival</option>' +
			'<option value="10">Train Departure</option>' +
			'<option value="11">Bus Arrival</option>' +
			'<option value="12">Bus Departure</option>' +
			'<option value="13">Group Checkin</option>' +
			'<option value="14">Group Checkout</option>' +
			'</select>' +
			'</div>' +
			'<div class="four wide field">' +
			'<div class="note-actions">' +
			'<a href="#" class="delete-note-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>' +
			'<a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<div class="fields">' +
			'<div class="two wide field"></div>' +
			'<div class="fourteen wide field">' +
			'<div class="note-fields">' +
			'<div class="fields">' +
			'<input type="text" name="InquiryNote" placeholder="Inquiry Note *">' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';
		$('.inquiry-note-wrapper').find('.add-note-button').remove();
		$('.inquiry-note-wrapper').append(noteHtml);
		activateDropdown();
		$('#quick-action-modal').modal('refresh');
		//}
		$('#note-save').removeClass('hidden');
	});
}


function onDeleteReinburstNote() {
	$(document).on("click", '.delete-note-reinburst-button', function () {
		parent = $(this).parents('.re-inburst-note');
		parent.remove();
	});
}
function onDeleteNote() {
	$(document).on("click", ".delete-note-button", function () {
		var parent = $(this).parents('.note');
		var noteWrapper = $('#inquiry-note-wrapper');
		parent.remove();

		if ($('.inquiry-note-wrapper').children().length == 0) {
			$('#note-save').addClass('hidden');
		}
		noteWrapper.children().last().find('.note-actions').empty();
		noteWrapper.children().last().find('.note-actions')
			.append('<a href="#" class="delete-note-button" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a>');
		noteWrapper.children().last().find('.note-actions')
			.append('<a href="#" class="add-note-button" data-tooltip="Add"><i class="teal plus circle icon add-field-icon"></i></a>');
		if (noteWrapper.children().length == 0) {
			$.ajax({
				type: "POST",
				url: "/Messages/DeleteAllNoteForInquiry",
				data: { InquiryId: quickModal.selectedBooking.Id },
			});
		}
		$('#quick-action-modal').modal('refresh');
	});
}
function onSelectedNoteChange() {
	$(document).on('change', '.select-note', function () {
		var noteType = $(this).find(":selected").val();
		var noteFields = '';
		var fieldWrapper = $(this).parents('.note').find('.note-fields');

		fieldWrapper.empty();

		// Note
		if (noteType == 1) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="InquiryNote" placeholder="Inquiry Note (*required)">' +
				'</div>';
		}
		// Flight Arrival
		else if (noteType == 2) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="Airline" placeholder="Airline">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="FlightNumber" placeholder="Flight Number">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival">' +
				'</div>';
		}
		// Flight Departure
		else if (noteType == 3) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="Airline" placeholder="Airline">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="FlightNumber" placeholder="Flight Number">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="AirportCodeDeparture" placeholder="Airport Code - Departure">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="AirportCodeArrival" placeholder="Airport Code - Arrival">' +
				'</div>';
		}
		// Meeting
		else if (noteType == 4) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="MeetingPlace" placeholder="Meeting Place">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="NumberOfPeople" placeholder="Number of People">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="MeetingDateTime" placeholder="Meeting Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="IdentifiableFeature" placeholder="Identifiable Feature">' +
				'</div>';
		}
		// Drive in
		else if (noteType == 5) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="DepartureCity" placeholder="Departure City">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="CarType" placeholder="Type of car">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="CarColor" placeholder="Color of car">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="PlateNumber" placeholder="Plate Number">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		// Cruise
		else if (noteType == 6) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="CruiseName" placeholder="Cruise Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="DeparturePort" placeholder="Departure Port">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="ArrivalPort" placeholder="Arrival Port">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		// Cruise Arrival
		else if (noteType == 7) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="CruiseName" placeholder="Cruise Name">' +
				'</div>' +
				//'<div class="fields">' +
				//'<input type="text" name="DeparturePort" placeholder="Departure Port">' +
				//'</div>' +
				//'<div class="ui calendar dt fields">' +
				//'<div class="ui input left icon">' +
				//'<i class="calendar icon"></i>' +
				//'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time">' +
				//'</div>' +
				//'</div>' +
				'<div class="fields">' +
				'<input type="text" name="ArrivalPort" placeholder="Arrival Port">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		// Cruise Departure
		else if (noteType == 8) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="CruiseName" placeholder="Cruise Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="DeparturePort" placeholder="Departure Port">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>';
			//'<div class="fields">' +
			//'<input type="text" name="ArrivalPort" placeholder="Arrival Port">' +
			//'</div>' +
			//'<div class="ui calendar dt fields">' +
			//'<div class="ui input left icon">' +
			//'<i class="calendar icon"></i>' +
			//'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time">' +
			//'</div>' +
			//'</div>';
		}
		// Train Arrival
		else if (noteType == 9) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="Name" placeholder="Train Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="TerminalStation" placeholder="Terminal Station">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		// Train Departure
		else if (noteType == 10) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="Name" placeholder="Train Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="TerminalStation" placeholder="Terminal Station">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		// Bus Arrival
		else if (noteType == 11) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="Name" placeholder="Bus Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="TerminalStation" placeholder="Terminal Station">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="ArrivalDateTime" placeholder="Arrival Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		// Bus Departure
		else if (noteType == 12) {
			noteFields = '<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="Name" placeholder="Bus Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="TerminalStation" placeholder="Terminal Station (*required)">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="DepartureDateTime" placeholder="Departure Date/Time (*required)">' +
				'</div>' +
				'</div>';
		}
		else if (noteType == 13) {
			noteFields =
				'<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="NumberOfPeople" placeholder="Number of People" >' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="GroupCheckin" placeholder="Group Checkin" ">' +
				'</div>' +
				'</div>';
		}
		else if (noteType == 14) {
			noteFields =
				'<div class="fields">' +
				'<input type="text" name="People" placeholder="Name">' +
				'</div>' +
				'<div class="fields">' +
				'<input type="text" name="NumberOfPeople" placeholder="Number of People">' +
				'</div>' +
				'<div class="ui calendar dt fields">' +
				'<div class="ui input left icon">' +
				'<i class="calendar icon"></i>' +
				'<input type="text" name="GroupCheckout" placeholder="Group Checkout">' +
				'</div>' +
				'</div>';
		}

		fieldWrapper.append(noteFields);
		$('#quick-action-modal').modal('refresh');
		$('.ui.calendar.dt').calendar({ type: 'datetime' });
	});
}


function saveNote() {

	var InquiryId = quickModal.selectedBooking.Id;

	var Note = [];
	$('.note').each(function (index, element) {
		//TODO 13/11/2017: QA this part on saving & update (CONTINUE HERE TONIGHT ON DATA SAVING, CLEAR ALL EXISTING DATA IN tbNOTE Table FIRST)
		var noteType = $(this).find('.select-note').val();

		var contactPerson = $(this).find('input[name="ContactPerson"]').val();
		var contactNumber = $(this).find('input[name="ContactNumber"]').val();
		var hashValue = $(this).find('input[name="HashValue"]').val();
		var numberOfPeople = $(this).find('input[name="NumberOfPeople"]').val();
		var noteId = $(this).find('input[name="NoteId"]').val();
		var checkin = $(this).find('input[name="GroupCheckin"]').val();
		var checkout= $(this).find('input[name="GroupCheckout"]').val();

		var noteJson;

		// note
		if (noteType == 1) {
			var inquiryNote = $(this).find('input[name="InquiryNote"]').val();
			if (inquiryNote == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				NoteType: noteType,
				InquiryNote: inquiryNote
			};
		}
		// flight arrival
		else if (noteType == 2) {
			var people = $(this).find('input[name="People"]').val();
			var airline = $(this).find('input[name="Airline"]').val();
			var flightNumber = $(this).find('input[name="FlightNumber"]').val();
			var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
			var airportCodeDeparture = $(this).find('input[name="AirportCodeDeparture"]').val();
			var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
			var airportCodeArrival = $(this).find('input[name="AirportCodeArrival"]').val();

			if (departureDateTime == "" || arrivalDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				Airline: airline,
				NoteType: noteType,
				FlightNumber: flightNumber,
				DepartureDateTime: departureDateTime,
				AirportCodeDeparture: airportCodeDeparture,
				ArrivalDateTime: arrivalDateTime,
				AirportCodeArrival: airportCodeArrival,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		}
		// flight departure
		else if (noteType == 3) {
			var people = $(this).find('input[name="People"]').val();
			var airline = $(this).find('input[name="Airline"]').val();
			var flightNumber = $(this).find('input[name="FlightNumber"]').val();
			var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
			var airportCodeDeparture = $(this).find('input[name="AirportCodeDeparture"]').val();
			var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
			var airportCodeArrival = $(this).find('input[name="AirportCodeArrival"]').val();

			if (departureDateTime == "" || arrivalDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				Airline: airline,
				NoteType: noteType,
				FlightNumber: flightNumber,
				DepartureDateTime: departureDateTime,
				AirportCodeDeparture: airportCodeDeparture,
				ArrivalDateTime: arrivalDateTime,
				AirportCodeArrival: airportCodeArrival,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		}
		//meeting
		else if (noteType == 4) {
			var meetingPlace = $(this).find('input[name="MeetingPlace"]').val();
			var people = $(this).find('input[name="People"]').val();
			//var numberOfPeople = $(this).find('input[name="NumberOfPeople"]').val();
			var meetingDateTime = $(this).find('input[name="MeetingDateTime"]').val();
			var identifiableFeature = $(this).find('input[name="IdentifiableFeature"]').val();
			if (meetingDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				NoteType: noteType,
				MeetingPlace: meetingPlace,
				People: people,
				NumberOfPeople: numberOfPeople,
				MeetingDateTime: meetingDateTime,
				IdentifiableFeature: identifiableFeature
			};
		
		}
		//drive arrival
		else if (noteType == 5) {
			var people = $(this).find('input[name="People"]').val();
			//var departureCity = $(this).find('input[name="DepartureCity"]').val();
			//var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
			var carType = $(this).find('input[name="CarType"]').val();
			var carColor = $(this).find('input[name="CarColor"]').val();
			var plateNumber = $(this).find('input[name="PlateNumber"]').val();
			var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();

			if (arrivalDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				//DepartureCity: departureCity,
				//DepartureDateTime: departureDateTime,
				CarType: carType,
				CarColor: carColor,
				PlateNumber: plateNumber,
				ArrivalDateTime: arrivalDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		}  // Drive departure
		else if (noteType == 6) {
			var people = $(this).find('input[name="People"]').val();
			var departureCity = $(this).find('input[name="DepartureCity"]').val();
			var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
			var carType = $(this).find('input[name="CarType"]').val();
			var carColor = $(this).find('input[name="CarColor"]').val();
			var plateNumber = $(this).find('input[name="PlateNumber"]').val();
			//var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();

			if (departureDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				DepartureCity: departureCity,
				DepartureDateTime: departureDateTime,
				CarType: carType,
				CarColor: carColor,
				PlateNumber: plateNumber,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
				//ArrivalDateTime: arrivalDateTime
			};
		}
		// Cruise arrival
		else if (noteType == 7) {
			var people = $(this).find('input[name="People"]').val();
			var cruiseName = $(this).find('input[name="CruiseName"]').val();
			var departurePort = $(this).find('input[name="DeparturePort"]').val();
			//var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
			var arrivalPort = $(this).find('input[name="ArrivalPort"]').val();
			var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();

			if (arrivalDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				CruiseName: cruiseName,
				DeparturePort: departurePort,
				//DepartureDateTime: departureDateTime,
				ArrivalPort: arrivalPort,
				ArrivalDateTime: arrivalDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		} // Cruise Departure
		else if (noteType == 8) {
			var people = $(this).find('input[name="People"]').val();
			var cruiseName = $(this).find('input[name="CruiseName"]').val();
			var departurePort = $(this).find('input[name="DeparturePort"]').val();
			var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();
			var arrivalPort = $(this).find('input[name="ArrivalPort"]').val();
			//var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();
			if (departureDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				CruiseName: cruiseName,
				DeparturePort: departurePort,
				DepartureDateTime: departureDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				ArrivalPort: arrivalPort,
				NumberOfPeople: numberOfPeople
				//ArrivalDateTime: arrivalDateTime
			};
		} // Train Arrival
		else if (noteType == 9) {
			var people = $(this).find('input[name="People"]').val();
			var name = $(this).find('input[name="Name"]').val();
			var terminalStation = $(this).find('input[name="TerminalStation"]').val();
			var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();

			if (arrivalDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				Name: name,
				TerminalStation: terminalStation,
				ArrivalDateTime: arrivalDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		} // Train Departure
		else if (noteType == 10) {
			var people = $(this).find('input[name="People"]').val();
			var name = $(this).find('input[name="Name"]').val();
			var terminalStation = $(this).find('input[name="TerminalStation"]').val();
			var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();

			if (departureDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				Name: name,
				TerminalStation: terminalStation,
				DepartureDateTime: departureDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		} // Bus Arrival
		else if (noteType == 11) {
			var people = $(this).find('input[name="People"]').val();
			var name = $(this).find('input[name="Name"]').val();
			var terminalStation = $(this).find('input[name="TerminalStation"]').val();
			var arrivalDateTime = $(this).find('input[name="ArrivalDateTime"]').val();

			if (arrivalDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				Name: name,
				TerminalStation: terminalStation,
				ArrivalDateTime: arrivalDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		} // Bus Departure
		else if (noteType == 12) {
			var people = $(this).find('input[name="People"]').val();
			var name = $(this).find('input[name="Name"]').val();
			var terminalStation = $(this).find('input[name="TerminalStation"]').val();
			var departureDateTime = $(this).find('input[name="DepartureDateTime"]').val();

			if (departureDateTime == "") {

				swal("Fill all required fields.", "", "warning");
				return;
			}
			noteJson = {
				InquiryId: InquiryId,
				People: people,
				NoteType: noteType,
				Name: name,
				TerminalStation: terminalStation,
				DepartureDateTime: departureDateTime,
				ContactPerson: contactPerson,
				ContactNumber: contactNumber,
				HashValue: hashValue,
				NumberOfPeople: numberOfPeople
			};
		}
		//Group Checkin
		else if (noteType == 13) {
			noteJson = {
				InquiryId: InquiryId,
				NoteType: noteType,
				People: people,
				NumberOfPeople: numberOfPeople,
				Checkin: checkin,
			};

		}
			//Group Checkout
		else if (noteType == 14) {
			noteJson = {
				InquiryId: InquiryId,
				NoteType: noteType,
				People: people,
				NumberOfPeople: numberOfPeople,
				Checkout: checkout
				
			};

		}
		if (noteId != null) {
			noteJson.Id = noteId;
		}

		Note.push(noteJson);
	});

	if (Note.length > 0 && InquiryId != '') {
		$.ajax({
			type: "POST",
			url: "/Messages/SaveNote",
			data: { InquiryId: InquiryId, list: JSON.stringify(Note) },
			dataType: "json",
			success: function (data) {
				swal("Note has been saved successfully.", "", "success");
				try {
					calendar.fullCalendar('refetchEvents');
					$('#quick-action-modal').modal('refresh');
				}
				catch (Exception) { }
			},
			error: function (error) {
				swal("Error connecting to server.", "", "error");
				$('#quick-action-modal').modal('refresh');
			}
		})
	}
}
function unBindTravelEventJsEvent() {
	$(document).off('click', '.add-note-button');
	$(document).off("click", ".delete-note-button");
	$(document).off('change', '.select-note');
	$(document).off('click', '.btn-save-note');
}
/************************************** End travel events booking ***************************************/

/************************************** Start Pre-Approve Inquiry ***************************************/
function onPreApproveInquiry() {
    $(document).on('click', '.js-btn_pre-approve', function () {
        if ($('#booking-action-form').children().length == 0) {
            $('#booking-action-form').append(preApproveForm);
            $('#booking-actions-inquiry').hide();
            $('#trip-info-details').hide();
            $('#quick-action-modal').find('#success-message-wrapper').hide();
            $('#quick-action-modal').find('#error-message-wrapper').hide();
            $('#quick-action-modal').modal('refresh');
        }
    });
}
function preApproveFormClose() {
	$(this).parents('.message').hide();
    $('#booking-action-form').empty();
    $('#booking-actions-inquiry').show();
    $('#trip-info-details').show();
    $('#quick-action-modal').modal('refresh');
}
function preApproveSubmit() {
    var threadId = quickModal.threadId;
    var hostId = quickModal.booking.HostId;
    var siteType = quickModal.selectedBooking.SiteType;

    $.ajax({
        type: 'POST',
        url: "/Booking/PreApproveInquiry",
        data: { siteType: siteType, hostId: hostId, threadId: threadId },
        success: function (data) {
            if (data.success) {
                //$('.inquiry-error-message-wrapper').hide();
                //$('.inquiry-success-message-wrapper').find('.header').text('Success on pre-approve on this guest.');
                //$('.inquiry-success-message-wrapper').show();
                //$('.inquiry-success-message-wrapper').removeClass('hidden');
                $('#booking-action-form').empty();
                quickModal.booking.CanPreApproveInquiry = false;
                quickModal.booking.CanDeclineInquiry = false;
                quickModal.booking.CanWithdrawPreApprovalInquiry = true;
                //$('#booking-action-form').append(getInquiryAction());
                $('#trip-info-details').show();
                $('#quick-action-modal').modal('refresh');
            }
			else {
				//JM 07/26/19 commented this because we support queing messages and action
                //$('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                //$('.inquiry-error-message-wrapper').find('.header').text(' Unfortunately, you cannot pre-approve this guest because your calendar is not available for the specified date range.');
                //$('.inquiry-error-message-wrapper').show();
                //$('.inquiry-error-message-wrapper').removeClass('hidden');
                //$('#booking-action-form').empty();
                //$('#booking-actions').show();
                //$('#trip-info-details').show();
                //$('#quick-action-modal').modal('refresh');
            }
        }
    });
}
function onWithdrawPreApproveInquiry() {
    $(document).on('click', '.js-btn_withdraw-pre-approve', function () {
        if ($('#booking-action-form').children().length == 0) {
            $('#booking-action-form').append(withdrawPreApproveForm);
            $('#booking-actions').hide();
            $('#trip-info-details').hide();
            $('#quick-action-modal').find('#success-message-wrapper').hide();
            $('#quick-action-modal').find('#error-message-wrapper').hide();
            $('#quick-action-modal').modal('refresh');
        }
    });
}
function withdrawPreApproveFormClose() {
    $(this).parents('.message').transition('fade').remove();
    $('#booking-action-form').empty();
    $('#booking-actions-inquiry').show();
    $('#trip-info-details').show();
    $('#quick-action-modal').modal('refresh');
}
function withdrawPreApproveSubmit() {
    var threadId = quickModal.threadId;
	var hostId = quickModal.booking.HostId;
	var siteType = quickModal.selectedBooking.SiteType;
    $.ajax({
        type: 'POST',
        url: "/Booking/WithdrawPreApproveInquiry",
        data: { siteType :siteType,hostId: hostId, threadId: threadId },
        success: function (data) {
            if (data.success) {
                //$('.inquiry-error-message-wrapper').hide();
                //$('.inquiry-success-message-wrapper').find('.header').text('Success on withdraw pre-approve on this guest.');
                //$('.inquiry-success-message-wrapper').show();
                //$('.inquiry-success-message-wrapper').removeClass('hidden');
                $('#booking-action-form').empty();
                quickModal.booking.CanPreApproveInquiry = true;
                quickModal.booking.CanDeclineInquiry = true;
                quickModal.booking.CanWithdrawPreApprovalInquiry = false;
                //$('#booking-action-form').append(getInquiryAction());
                $('#trip-info-details').show();
                $('#quick-action-modal').modal('refresh');
            }
            //else {
            //    $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
            //    $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
            //    $('.inquiry-error-message-wrapper').show();
            //    $('.inquiry-error-message-wrapper').removeClass('hidden');
            //    $('#booking-action-form').empty();
            //    $('#booking-actions-inquiry').show();
            //    $('#trip-info-details').show();
            //    $('#quick-action-modal').modal('refresh');
            //}
        }
    });
}
/************************************** End Pre-Approve Inquiry ***************************************/

/************************************** Start Decline Inquiry ***************************************/
function onDeclineInquiry() {
    $(document).on('click', '.js-btn_cancel-reservation', function () {
        if ($('#booking-action-form').children().length == 0) {
            $('#booking-action-form').append(declineInquiryForm);//declineInquiryForm
            $('#booking-actions-inquiry').hide();
            $('#trip-info-details').hide();
            $('#quick-action-modal').find('#success-message-wrapper').hide();
            $('#quick-action-modal').find('#error-message-wrapper').hide();
            $('#quick-action-modal').modal('refresh');
        }
    });
}
function onDeclineInquiryFormClose() {
    $(document).on('click', '#decline-inquiry-back-btn', function () {
        $(this).parents('.message').transition('fade').remove();
        $('#booking-action-form').empty();
        $('#booking-actions-inquiry').show();
        $('#trip-info-details').show();
        $('#quick-action-modal').modal('refresh');
    })
}
function onDeclineInquiryTypeMessage() {
    $(document).on('keyup', '#decline_inquiry_reason', function () {
        if ($(this).val() == '') {
            $('#decline-inquiry-submit-btn').removeClass('disabled').addClass('disabled');
        }
        else {
            $('#decline-inquiry-submit-btn').removeClass('disabled')
        }
    })
}
function onDeclineInquirySubmit() {
    $(document).on('click', '#decline-inquiry-submit-btn', function () {
        var threadId = quickModal.threadId;
        var hostId = quickModal.booking.HostId;
        var message = $('#decline_inquiry_reason').val();
        var siteType = quickModal.selectedBooking.SiteType;
		$.ajax({
            type: 'POST',
			url: "/Booking/DeclineInquiry",
			data: { threadId: threadId, hostId: hostId, message: message },
            success: function (data) {
                if (data.success) {
                    //$('.inquiry-error-message-wrapper').hide();
                    //$('.inquiry-success-message-wrapper').find('.header').text('Booking Declined.');
                    //$('.inquiry-success-message-wrapper').show();
					//$('.inquiry-success-message-wrapper').removeClass('hidden');
					$('#booking-action').empty();
                    $('#booking-action-form').empty();
                    $('#booking-actions-inquiry').empty();
                    $('#booking-actions-inquiry').hide();
                    $('#trip-info-details').show();
                    //$('#reservation-status').text('Inquiry-Cancelled');
                    //$('.view-inquiry-status').text('Inquiry-Cancelled');
                }
                //else {
                //    $('.inquiry-success-message-wrapper').removeClass('hidden').addClass('hidden');
                //    $('.inquiry-error-message-wrapper').find('.header').text('An error occur.');
                //    $('.inquiry-error-message-wrapper').show();
                //    $('.inquiry-error-message-wrapper').removeClass('hidden');
                //}
            }
        });
    });
}
/************************************** End Decline Inquiry ***************************************/
function activateDropdown() {
    $('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
}
function saveNewBookingTime() {
    $('.change-time-error-message').hide();
    $('.change-time-success-message').hide();
    var checkInTime = $('#change-booking-time-form input[name=checkInTime]').val();
    var checkOutTime = $('#change-booking-time-form input[name=checkOutTime]').val();
    $.ajax({
        type: "POST",
        url: "/Booking/ChangeBookingTime",
        data: {
            inquiryId: quickModal.selectedBooking.Id,
            checkInTime: checkInTime,
            checkOutTime: checkOutTime
        },
        success: function (data) {
            if (data.success) { $('.change-time-success-message').show(); }
            else { $('.change-time-error-message').show(); }
            calendar.fullCalendar('refetchEvents');
        }
    });
}
function unbindInquiryJsEvents() {
    $(document).off('click', '.js-btn_send-special-offer');
    $(document).off('click', '#special-offer-back-btn');
    $(document).off('click', '#submit-special-offer-btn');
    $(document).off('click', '.special-offer-msg .close');
    $(document).off('click', '.js-btn_withdraw-special-offer');
    $(document).off('click', '#cancel-special-offer-back-btn');
    $(document).off('click', '#cancel-special-offer-submit-btn');
    $(document).off('click', '.js-btn_pre-approve');
    $(document).off('click', '#pre-approve-back-btn');
    $(document).off('click', '#pre-approve-submit-btn');
    $(document).off('click', '.js-btn_withdraw-pre-approve');
    $(document).off('click', '#withdraw-pre-approve-back-btn');
    $(document).off('click', '#withdraw-pre-approve-submit-btn');
    $(document).off('click', '.js-btn_decline-inquiry');
    $(document).off('click', '#decline-inquiry-back-btn');
    $(document).off('click', '#decline-inquiry-submit-btn');
}

/************************************** Start Worker Tab ***************************************/
function onLoadAssignedWorker() {
    if (quickModal.page == 'calendar' || quickModal.page == 'booking' || quickModal.page == 'dashboard'
        || quickModal.page == 'workers' || quickModal.page == 'travel-events') {
        quickModal.workerAssignmentTable = $('#assigned-worker-list-table').DataTable({
            "bAutoWidth": false,
            "aaSorting": [],
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Home/GetWorkerAssignments",
				"type": 'GET',
				"async":"true",
				"data": {
					inquiryId: function () { return quickModal.selectedBooking.Id }
                }
            },
            "columns": [
                    { "data": "Name" },
                    { "data": "Job" },
                    { "data": "StartDate" },
					{ "data": "EndDate" },
					{ "data": "Description" },
					{ "data": 'Notes' },
					{ "data": 'AssignmentStatus' },
                    { "data": "Action", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
				$(row).attr('data-assignment-id', data.Id);
				if (data.Status == 2 || data.Status == 3){
					$(row).addClass('error'); 
		}
            }
		});
        quickModal.workerTable = $('#worker-list-table').DataTable({
            "aaSorting": [],
            "bAutoWidth": false,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Home/GetAvailableWorkers",
                "type": 'GET',
                "data": function () {
                    return {
						jobTypeId: $('#qm-job-type-id').val() == '' ? 0 : $('#qm-job-type-id').val(),
						startDate: $('#worker-start-date').val(),
						endDate: $('#worker-end-date').val()
                    }
                }
            },
            "columns": [
                    { "data": "Name" },
                    { "data": "Job" },
                    { "data": "PaymentType" },
                    { "data": "PaymentRate" },
                    //{ "data": "Dates" },
                    { "data": "Action", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-worker-id', data.Id);
            },
            //"rowCallback": function( row, data ) {
            //    if ( data.grade == "A" ) {
            //        $('td:eq(4)', row).html( '<b>A</b>' );
            //    }
            //},
            "drawCallback": function (settings) {
                $('.ui.calendar.dt').calendar({ type: 'datetime' });
            }
        });
    }
}


function addWorkerAssignment() {
    $('#worker-assignment-wrapper').hide();
    $('#worker-form-wrapper').show();
	$('#qm-job-type-id').val("0").change();
}
function backToWorkerAssignments() {
	$('.re-inburst-note-wrapper').empty();
	$('.edit-re-inburst-note-wrapper').empty();
	$('#worker-form-wrapper').hide();
	$('#edit-assignment-wrapper').hide();
    $('#worker-assignment-wrapper').show();
}
function onDeleteWorkerAssignment() {
    $(document).on('click', '.delete-worker-assignment', function () {
        var assignmentId = $(this).parents('tr').attr("data-assignment-id");
        if (assignmentId != '' && assignmentId != null) {
            $.ajax({
                type: "POST",
                url: "/Home/UnassignWorker/",
                data: { assignmentId: assignmentId },
				success: function () {
					swal("Worker Succesfully Deleted!", "", "success");
                    if (quickModal.page == 'calendar') { $('#calendar').fullCalendar('refetchEvents'); }
                    quickModal.workerAssignmentTable.ajax.reload(null, false);
                }
            });
        }
    });
}

function onEditAssignmentSubmit(){
	$(document).on('click', '#edit-assignment-btn', function () {

		var startDate = $('#edit-worker-start-date').val();
		var endDate = $('#edit-worker-end-date').val();
		var description = $('#edit-assignment-description').val();
		var ReinburstmentNote = [];
		
		$('.re-inburst-note').each(function (index, element) {
			var note;
			var Id = $(this).attr('reimbusement-id');
			var ReinburstNote = $(this).find('input[name="ReinburstNote"]').val();
			var Amount = $(this).find('input[name="Amount"]').val();
			note = {
				Id: Id,
				Note: ReinburstNote,	
				Amount: Amount
			};

			ReinburstmentNote.push(note);
		});

		if (assignId != 0 && startDate != "" && endDate !="") {
			$.ajax({
				type: "POST",
				url: "/Home/EditAssignReimburseWorker/",
				data: { assignmentId: assignId, startDate: startDate, endDate: endDate, description:description, Notes: JSON.stringify(ReinburstmentNote) },
				success: function (data) {
					if (data.success) {
						quickModal.workerAssignmentTable.ajax.reload(null, false);
						$('#edit-assignment-wrapper').hide();
						$('.edit-re-inburst-note-wrapper').empty();
						$('#worker-assignment-wrapper').show();
					} else {
						swal("Error on editting assignment", "", "error");
					}
				
				}
			});
		}
	});
}

function ConvertJsonDate(date) {

	var dt = new Date();
	dt = new Date(parseInt(date.substr(6)));
    

	//var dt = new Date(d);
	return dt;
}

function onEditWorkerAssignment() {
	$(document).on('click', '.edit-worker-assignment', function () {
		var assignmentId = $(this).parents('tr').attr("data-assignment-id");
		//var start = $(this).parents("tr").find('td:eq(2)').text();
		//var end = $(this).parents("tr").find('td:eq(3)').text();
		

		if (assignmentId != '' && assignmentId != null) {
			$.ajax({
				type: "GET",
				url: "/Home/EditAssignWorker/",
				data: { assignmentId: assignmentId },
				success: function (data) {
					if (data.success) {
						assignId = assignmentId;
						$('#worker-assignment-wrapper').hide();
						$('#worker-name').text('Worker: '+data.Worker.Firstname + ' ' + data.Worker.Lastname);
						for (var i = 0; i < data.Notes.length; i++) {
							var noteHtml =
								'<div class="re-inburst-note" reimbusement-id=' + data.Notes[i].Id + '>' +
								'<div class="ui input twelve wide field">' +
								'<input type="text" name="ReinburstNote" placeholder="Note" value="' + data.Notes[i].Note + '">' +
								'</div>' +
								'<div class="ui input twelve wide field">' +
								'<input type="text" name="Amount" placeholder="Amount" value="' + data.Notes[i].Amount + '">' +
								'</div>' +
								'<a href="#" class="delete-note-reinburst-button" data-tooltip="Remove"><i class="orange minus circle icon remove-field-icon"></i></a>' +
								'</br></div>';
							$('.edit-re-inburst-note-wrapper').append(noteHtml);

						}
						$('#edit-assignment-description').val(data.Assignment.Description);
						$('#edit-worker-start-date').val(moment(data.Assignment.StartDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A'));
						$('#edit-worker-end-date').val(moment(data.Assignment.EndDate).tz(quickModal.selectedBooking.TimeZoneId).format('MMMM D, YYYY h:mm A'));
						$('#edit-assignment-wrapper').show();

					}
					else
					{
						swal("Error on getting data", "", "error");
					}
				}
			});
		}
	});
}

function assignWorker(e) {

	var reinburstDiv = $('#re-inburst-note-wrapper');
	var ReinburstmentNote = [];
	$('.re-inburst-note').each(function (index, element) {
		var note;
		var reinburstNoteJson;
		var Id = $(this).attr('reimbusement-id');
		var ReinburstNote = $(this).find('input[name="ReinburstNote"]').val();
		var Amount = $(this).find('input[name="Amount"]').val();
		note = {
			Id:Id,
			Note: ReinburstNote,
			Amount: Amount
		};
		ReinburstmentNote.push(note);
	});


	$("input.chk-assign").prop("disabled", true);
	$('#assign-worker-success-message-wrapper').hide();
	$('#assign-worker-error-message-wrapper').hide();

	var workerId = $('input.chk-assign:checked').parents('tr').attr('data-worker-id');
	var assignStart = $('#worker-start-date').val();
	var assignEnd = $('#worker-end-date').val();
	var jobTypeId = $('#qm-job-type-id').val();
	var description = $('#assignment-description').val();

	if (assignStart == '' || assignEnd == '') {
		$('#assign-worker-success-message-wrapper').hide();
		$('#assign-worker-error-message-wrapper').find('.header').text('Please select assign duration.');
		$('#assign-worker-error-message-wrapper').show();
		$('#assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.chk-assign").prop("disabled", false);
	}
	else if (workerId == null) {
		$('#assign-worker-success-message-wrapper').hide();
		$('#assign-worker-error-message-wrapper').find('.header').text('Please select worker.');
		$('#assign-worker-error-message-wrapper').show();
		$('#assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.chk-assign").prop("disabled", false);
	}
	else if (jobTypeId == "" || jobTypeId == null || jobTypeId == "0") {
		$('#assign-worker-success-message-wrapper').hide();
		$('#assign-worker-error-message-wrapper').find('.header').text('Please select Job Type.');
		$('#assign-worker-error-message-wrapper').show();
		$('#assign-worker-error-message-wrapper').removeClass('hidden');
		$("input.chk-assign").prop("disabled", false);}
    else {
        $.ajax({
            type: "POST",
            url: "/Home/AssignWorker",
            data: {
                PropertyId: quickModal.selectedBooking.LocalPropertyId,
                ReservationId: quickModal.selectedBooking.Id,
                WorkerId: workerId,
                JobTypeId: jobTypeId,
                StartDate: assignStart,
				EndDate: assignEnd,
				ReinburstmentNote: JSON.stringify(ReinburstmentNote),
				Description: description
            },
			success: function (data) {
                $('#assign-worker-error-message-wrapper').hide();
                $('#assign-worker-success-message-wrapper').find('.header').text('Changes has been saved.');
                $('#assign-worker-success-message-wrapper').show();
                $('#assign-worker-success-message-wrapper').removeClass('hidden');
                $("input.chk-assign").prop("disabled", false);
                if (quickModal.page == 'calendar') { $('#calendar').fullCalendar('refetchEvents'); }
                quickModal.workerTable.ajax.reload(null, false);
                $('#worker-start-date').val('');
				$('#worker-end-date').val('');
				quickModal.workerAssignmentTable.ajax.reload(null, false);
				backToWorkerAssignments();
				$('#qm-job-type-id').val("0").change();
            }
        });
    }
}

//function submitAssignWorker() {
//    $('#assign-worker-success-message-wrapper').hide();
//    $('#assign-worker-error-message-wrapper').hide();

//    var assignStart = $('#worker-start-date').val();
//    var assignEnd = $('#worker-end-date').val();
//    var workerId = $('input.chk-assign:checked').parents('tr').attr('data-worker-id');
//    var jobTypeId = $('#qm-job-type-id').val();
//    if (assignStart == '' || assignEnd == '') {
//        $('#assign-worker-success-message-wrapper').hide();
//        $('#assign-worker-error-message-wrapper').find('.header').text('Please select assign duration.');
//        $('#assign-worker-error-message-wrapper').show();
//        $('#assign-worker-error-message-wrapper').removeClass('hidden');
//    }
//    else if (workerId == null) {
//        $('#assign-worker-success-message-wrapper').hide();
//        $('#assign-worker-error-message-wrapper').find('.header').text('Please select worker.');
//        $('#assign-worker-error-message-wrapper').show();
//        $('#assign-worker-error-message-wrapper').removeClass('hidden');
//    }
//	else {
//        $.ajax({
//            type: "POST",
//            url: "/Home/AssignWorker",
//            data: {
//				PropertyId: quickModal.selectedBooking.LocalPropertyId,
//				ReservationId: quickModal.selectedBooking.Id,
//                WorkerId: workerId,
//                JobTypeId: jobTypeId,
//                StartDate: assignStart,
//                EndDate: assignEnd
//            },
//            success: function (data) {
//                $('#assign-worker-error-message-wrapper').hide();
//                $('#assign-worker-success-message-wrapper').find('.header').text('Changes has been saved.');
//                $('#assign-worker-success-message-wrapper').show();
//                $('#assign-worker-success-message-wrapper').removeClass('hidden');
//                if (quickModal.page == 'calendar') { $('#calendar').fullCalendar('refetchEvents'); }
//                quickModal.workerTable.ajax.reload(null, false);
//				$('#qm-job-type-id').val('');
//                $('#worker-start-date').val('');
//                $('#worker-end-date').val('');
//                quickModal.workerAssignmentTable.ajax.reload(null, false);
//            }
//        });
//    }
//}
function onJobTypeChange() {
    quickModal.workerTable.ajax.reload(null, false);
    $('#quick-action-modal').modal('refresh');
}
function onChangeStartDate() {
	quickModal.workerTable.ajax.reload(null, false);
	$('#quick-action-modal').modal('refresh');
}
function onChangeEndDate() {
	quickModal.workerTable.ajax.reload(null, false);
	$('#quick-action-modal').modal('refresh');
}


function onSelectWorker() {
    $(document).on('change', 'input.chk-assign', function (e) {
        $('input.chk-assign').not(this).prop('checked', false);
        $('.chk-assign').removeClass('disabled');
        $('.edit-chk-assign').removeClass('disabled');
        if ($('input.chk-assign:checked').length == 0) {
            $('.chk-assign').addClass('disabled');
            $('.edit-chk-assign').addClass('disabled');
        }
    });
}
function unbindAssignWorkerJsEvents() {
    $(document).off('click', '#assign-worker-btn');
    $(document).off('change', 'input.assign-worker');
}
/************************************** End Worker Tab ***************************************/
function quickActionModal(settings) {
	var html = getModalTemplateHtml(settings);
    $('body').append(html);
    $('#quick-action-modal')
        .modal('setting', {
            autofocus: false,
            onShow: function () {
				onComposeMessage();
				onMessageEndSubmit();
				onMessageGuestSubmit();
				onChangeTaskStatus();
				onChangeSubTaskStatus();
				onClickUpdateTaskButton();
				onClickAddCustomTaskButton();
				onLoadInquiryTaskList();
				onCctvDropdownChanged();
				onCctvNoteSaved();
				SearchPeople();
				$('#cctv-camera').val("0").change();
				$('#search-people-btn').click();
				var hasWorker = quickModal.selectedBooking.HasWorkerAssignment;
				var hasTravelEvent = quickModal.selectedBooking.HasTravelEvent;

                if (quickModal.selectedBooking.Status == 'I' || quickModal.selectedBooking.Status == 'NP' || quickModal.selectedBooking.Status == 'SO') {
                    onSpecialOffer();
                    onSpecialOfferSubmit();
                    onWithdrawSpecialOffer();
                    onPreApproveInquiry();
                    onWithdrawPreApproveInquiry();
                    onDeclineInquiry();
                    onDeclineInquiryFormClose();
                    onDeclineInquiryTypeMessage();
                    onDeclineInquirySubmit();
				}
				else if (quickModal.selectedBooking.Status == 'A' || hasWorker || hasTravelEvent) {
					onAddNote();
					onAddReinburstmentNote();
					onEditReinburstmentNote();
					onDeleteNote();
					onDeleteReinburstNote();
                    onSelectedNoteChange();
					GetNotes(quickModal.selectedBooking.Id);
					onEditAssignmentSubmit();
                    onLoadAssignedWorker();
					onSelectWorker();
					onEditWorkerAssignment();
                    onDeleteWorkerAssignment();
                    $('#worker-form-wrapper').hide();
					$('#worker-assignment-wrapper').show();
					$('#edit-assignment-wrapper').hide();
                    $('#assign-worker-success-message-wrapper').hide();
                    $('#assign-worker-error-message-wrapper').hide();
                }
                $('.change-time-error-message').hide();
				$('.change-time-success-message').hide();
				//$('#quick-action-modal').find('.menu .item').tab();
				//$('#quick-action-modal').find('.ui.dropdown').dropdown();
				//$('#quick-action-modal').find('.ui.calendar.time').calendar({ type: 'time' });
				//$('#quick-action-modal').find('.ui.calendar.dt').calendar({ type: 'datetime' });
				//$('#quick-action-modal').find('.ui.accordion').accordion();
                $('#quick-action-tab .item').tab({
                    onVisible: function (tabPath) {
                        $('#quick-action-modal').modal('refresh');
                        $('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
                    }
                });
                $('.inquiry-success-message-wrapper').hide();
                $('.inquiry-error-message-wrapper').hide();
                onMessageClose();
            },
            onHidden: function (e) {
				try {
						console.log('Animation complete')
						$(this).off(e);    // remove this listener
						$(this).modal('destroy');  // take down the modal object
						$(this).remove();    // remove the modal element, at last.
     //               unbindInquiryJsEvents()();
     //               unBindMessagesJsEvents();
     //               unBindPendingBookingEvents();
     //               unbindTripCoordinationJsEvents();
     //               unBindTravelEventJsEvent();
					//unbindAssignWorkerJsEvents();
					//unbindTaskJsEvents();
					//$(document).off('click', '.add-note-button');
					//$(document).off('click', '.js-btn_send-special-offer');
					//$(document).off('click', '.js-btn_withdraw-special-offer');
					//$(document).off('click', '.js-btn_pre-approve');
					//$(document).off('click', '.js-btn_withdraw-pre-approve');
					//$(document).off('click', '.js-btn_cancel-reservation');
					//$(document).off('click', '#decline-inquiry-back-btn');
					//$(document).off('keyup', '#decline_inquiry_reason');
					//$(document).off('click', '#decline-inquiry-submit-btn');
					//$(document).off('click', '.add-reinburst-button');
					//$(document).off('click', '.edit-reinburst-button');
					//$(document).off("click", ".delete-note-button");
					//$(document).off("click", '.delete-note-reinburst-button');
					//$(document).off('change', '.select-note');
					//$(document).off('click', '#edit-assignment-btn');
					//$(document).off('change', 'input.chk-assign');
					//$(document).off('click', '.edit-worker-assignment');
					//$(document).off('click', '.delete-worker-assignment');

					//$('#quick-action-modal').modal('destory').remove();
                }
                catch (Exception) { }
            }
        })
		.modal("show");

	function onLoadInquiryTaskList() {    
		$('#task-list-table > tbody').find('tr').remove();
		$.ajax({
			type: "GET",
			url: "/Task/GetInquiryTask",
			async:true,
			data: {
				confirmationCode: quickModal.selectedBooking.ConfirmationCode
			},
			success: function (data) {
				var html='';
				if (data != null) {
					$.each(data.Templates, function (i, item) {
						html += '<tr data-task-id="' + item.Id + '" data-id="' + item.Id + '">';
						if (item.Id != -1) {
							html += '<td style="vertical-align:top;">' + item.Name + '</td>';
						}
						html += '<td><table class"ui table">';
						$.each(item.Tasks, function (j, task) { 
								html += "<tr>";
								html += "<td>";
								html += "<div class=\"ui checkbox\">";
								html += task.Status ? "<input type=\"checkbox\" data-task-id=\"" + task.Id + "\" class=\"task-status\" name=\"public\" checked=\"checked\">" : "<input data-task-id=\"" + task.Id + "\" class=\"task-status\" type=\"checkbox\" name=\"public\">";
								html += "<label class=\"task-status-label\">" + task.Title + '-' + task.Description + "</label>";
								html += "</div></td><td>";
								html += '<div class="fields">';
								html += '<div class="one width field">';
								html += '<img src="' + (task.ImageUrl != null ? task.ImageUrl : "/Images/Task/default-image.png") + '" class="ui small image" alt="Image" />';
								html += '</div>';
								html += '</div>';
								
								html += '</td>';
								html += '<td class="task-action-column">' +
								'<a href="#" class="remove_field" data-tooltip="Delete" onclick="deleteInquiryTask(' + task.Id + ')">' +
								'<i class="orange minus circle icon remove-field-icon"></i></a>' +
								'</td>';
								html+='<td></td>';
								html += "</tr>";

								var subcount = task.SubTasks.length;
								var subtask = task.SubTasks;
								for (var x = 0; x < subcount; x++) {
									html += '<tr>';
									html += '<td></td><td>';
									html += '<div class="ui checkbox" style="float:left">';

									if (subtask[x].Status == 1) {
										html += '<input data-subtask-id="' + subtask[x].Id + '" class=\"subtask-status\" type="checkbox" name="public" checked="checked">';
									}
									else {
										html += '<input data-subtask-id="' + subtask[x].Id + '" class=\"subtask-status\" type="checkbox" name="public">';
									}
									html += '<label class="task-status-label">' + subtask[x].Title + '-' + subtask[x].Description + '</label>';

									html += '</div>';
									html += '</td>';
									html += '<td>';
									html += '<div class="fields">';
									html += '<div class="one width field">';
									html += '<img src="' + (subtask[x].ImageUrl != null ? subtask[x].ImageUrl : "/Images/SubTask/default-image.png") + '" class="ui small image" alt="Image" />';
									html += '</div>';
									html += '</div>';
									html += '</td>';
									html += '<td>'
									html += '<h6> Update by:' + (subtask[x].UpdateBy != null ? subtask[x].UpdateBy + "-" + moment(subtask[x].LastUpdateDate).tz(quickModal.selectedBooking.TimeZoneId).format("MM/DD/YYYY hh:mm A") : "") + '</h6>'
									html += '</td>';
									html += '</tr>';
								}
							
						});
					});

					html += '</tr></table></td>'
				
					html += '</tr>';
					$("#task-list-table > tbody").append(html);
					$('.ui.checkbox').checkbox();
				
					UpdateTaskStatisticCount(data);
					if (data.Templates == null) {
						var html = '<tr><td colspan="3" id="no-task-tr" style="text-align: center">No Tasks Available</td></tr>';
						$("#task-list-table > tbody").append(html);
					}
				} else {
					var emptyHtml = '<tr><td colspan="3" id="no-task-tr" style="text-align: center">No Tasks Available</td></tr>';
					$("#task-list-table > tbody").append(emptyHtml);
				}
				
				$('#quick-action-modal').modal('refresh');
				//console.log($(".panorama").panorama_viewer());

			},
			error: function (error) {
				swal("Error on loading task list.", "", "error");
			}
		});
	}

	function onChangeTaskStatus() {
		$(document).on('change', 'input.task-status', function () {
			$.ajax({
				type: "POST",
				url: "/Task/UpdateTaskStatus",
				data: { Id: $(this).attr('data-task-id'), status: $(this).is(':checked') },
				success: function (data) {
					onLoadInquiryTaskList();
				}
			});
		});
	}
	function onChangeSubTaskStatus() {
		$(document).on('change', 'input.subtask-status', function () {
			$.ajax({
				type: "POST",
				url: "/Task/UpdateSubTaskStatus",
				data: { Id: $(this).attr('data-subtask-id'), status: $(this).is(':checked') },
				success: function (data) {
					onLoadInquiryTaskList();
				}
			});
		});
	}

	function onClickUpdateTaskButton() {
	    
		$("#update-task-btn").click(function () {
			debugger;
			var requestDataList = [];
			var customTaskList = [];

			//Get all row from Task table
			var taskRow = $("#task-list-table > tbody > tr");

			for (var e = 1; e <= taskRow.length; e++) {
				var requestData = { Id: 0, TaskId: 0, TaskStatus: 0 };
				var inquiryTaskId = $("#task-list-table > tbody > tr:nth-child(" + e + ")").attr("data-id");
				var taskId = $("#task-list-table > tbody > tr:nth-child(" + e + ")").attr("data-task-id");
				var taskStatus = $("#task-list-table > tbody > tr:nth-child(" + e + ")").find(".assign-task").prop('checked');

				if (inquiryTaskId != null && taskId != null && taskStatus != null) {
					requestData.Id = parseInt(inquiryTaskId);
					requestData.TaskId = parseInt(taskId);
					requestData.TaskStatus = taskStatus ? 1 : 0;
					requestDataList.push(requestData);
				}
			}

			$("#task-list-table > tbody > tr[name=\"custom_task\"]").each(function () {
				var customTaskRequestData = { TemplateName: "", TaskTitle: "", TaskDescription: "", TaskStatus: 0 };

				var customTemplateName = $(this).find("[name=\"custom-task-template-name\"]").val();
				var customTaskTitle = $(this).find("[name=\"custom-task-title\"]").val();
				var customTaskDescription = $(this).find("[name=\"custom-task-description\"]").val();
				var customTaskStatus = $(this).find("[name=\"custom-task-status\"]").prop("checked");

				customTaskRequestData.TemplateName = customTemplateName;
				customTaskRequestData.TaskTitle = customTaskTitle;
				customTaskRequestData.TaskDescription = customTaskDescription;
				customTaskRequestData.TaskStatus = customTaskStatus ? 1 : 0;
				customTaskList.push(customTaskRequestData);
			});

			$.ajax({
				type: "POST",
				url: "/Task/UpdateInquiryTaskStatus",
				data: { requestData: JSON.stringify(requestDataList), customTaskList: JSON.stringify(customTaskList), confirmationCode: quickModal.selectedBooking.ConfirmationCode },
				success: function (data) {
					data.success ? swal("Changes has been saved .", "", "success") : swal("Fail to Save Changes.", "", "error");
					//UpdateTaskStatisticCount(data);
					onLoadInquiryTaskList();
				}
			});
		});
	}

	function unbindTaskJsEvents() {
		$(document).off('change', 'input.assign-task');
	}


}