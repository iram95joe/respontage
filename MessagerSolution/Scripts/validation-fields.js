﻿$(document).ready(function () {
	//OnKeyupValidation();
	OnKeyupRequiredFieldValidation();
	RequiredFieldValidation();
	RequiredSelectValidation();
	//JM 11/8/19 Validate when required field fill by user
	function OnKeyupRequiredFieldValidation(){
		$(document).on('keyup', '[required]', function () {

			if ($(this).val().length == 0) {
				$(this).next().remove();
				$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			}
			else {
				$(this).next().remove();
			}
		});
	}

	function RequiredFieldValidation(parentObj) {
		var hasBlankField = false;
		$(parentObj).find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.next().remove();
				obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.next().remove();
			}
			return hasBlankField;
		});
	}

	function RequiredSelectValidation(obj){
		var hasBlankField = false;
		if (obj.val() == "" || obj.val() == null) {
			obj.parent().next().remove();
			obj.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			hasBlankField = true;
		}
		else {
			obj.parent().next().remove();
		}
		return hasBlankField;
	}


});