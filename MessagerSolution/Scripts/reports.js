﻿

// initialization scripts
var $ddlCashFlowPropertyType = $("#ddlCashFlowPropertyType");
var $ddlCashFlowExpenseType = $("#ddlCashFlowExpenseType");
var $cashFlowStartDate = $("#cashFlowStartDate");
var $cashFlowEndDate = $("#cashFlowEndDate");
var $cashFlowMainAccordion = $("#cashFlowMainAccordion");
var $cashFlowFullTotalIncome = $("#cashFlowFullTotalIncome");
var $cashFlowFullTotalExpense = $("#cashFlowFullTotalExpense");
//var $cashFlowFullTotalSharedExpense = $("#cashFlowFullTotalSharedExpense");
var $cashFlowFullTotalNettCashFlow = $("#cashFlowFullTotalNettCashFlow");

/*Commission Report UI Controls*/
var commissionGoButton = $(".js-btn_tabulate-commission");
//var commissionAccordionWrapper = $("#CommissionAccordionWrapper");
var commissionSelectProperty = $("#CommissionSelectProperty");
var commissionSelectDate = $("#CommissionSelectDate");
var commissionTableRecords = $("#CommissionTableRecords");
var commissionEmptyRecord = $("#CommissionEmptyRecord");


var monthNames = ["January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December"
];

$(document).ready(function () {
	initialize();

	function initialize() {
		setupSemanticUI();
		setEventListeners();
		AppInit();
	}
	$('#cashflow-include-child').prop('checked', true);
	$('#income-expense-include-child').prop('checked', true);

	function setupSemanticUI() {
		var today = moment(new Date()).format('MMMM YYYY');

		$('.ui.accordion').accordion();
		$('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
		$('.ui.checkbox').checkbox();
		$('.ui.calendar').calendar({ type: 'date' });
		$('.ui.calendar.month').calendar({ type: 'month' });
		$('.ui.calendar.time').calendar({ type: 'time' });
		$cashFlowStartDate.val(today);
		$cashFlowEndDate.val(today);
		incomeExpenseSelectDateFrom.val(today);
		incomeExpenseSelectDateTo.val(today);
	}

	function setEventListeners() {
		//onDisplayReport();
		onCreateExpenses();
		SaveIncomeExpenseSearch();
		SaveCashflowSearch();
		onChangeSaveIncomeExpenseSearch();
		onChangeSaveCashflowSearch();
		IncomeExpenseShowSummary();
		CashflowShowSummary();
		ReportTypeChange();
	}

	// ------------------
	// ------------------
	//   Event Listeners
	// ------------------
	// ------------------
	function ReportTypeChange() {
		$(document).on('change', '#report-type', function () {
			var value = $(this).val();
			switch (value) {
				case 'js-show-report-forecasting':
					$('.report').hide();
					$('.report.forecasting').show();
					break;
				case 'js-show-report-income-expense':
					$('.report').hide();
					$('.report.income-expense').show();
					break;
				case 'js-show-report-roi-summary':
					$('.report').hide();
					$('.report.roi-summary').show();
					break;
				case 'js-show-report-income':
					$('.report').hide();
					$('.report.income').show();
					break;
				case 'js-show-report-cash-flow':
					$('.report').hide();
					$('.report.cash-flow').show();
					break;
				case 'js-show-report-commission':
					$('.report').hide();
					$('.report.commission').show();
					break;
			}
		});
	}
	//  function onDisplayReport() {
	//      $('.js-show-report-forecasting').click(function () {
	//          $('.report').hide();
	//          $('.report.forecasting').show();
	//      });
	//      $('.js-show-report-income-expense').click(function () {
	//          $('.report').hide();
	//          $('.report.income-expense').show();
	//      });
	//      $('.js-show-report-roi-summary').click(function () {
	//          $('.report').hide();
	//          $('.report.roi-summary').show();
	//      })
	//      $('.js-show-report-income').click(function () {
	//          $('.report').hide();
	//          $('.report.income').show();
	//      });
	//      $('.js-show-report-cash-flow').click(function () {
	//          $('.report').hide();
	//          $('.report.cash-flow').show();
	//});

	//$('.js-show-report-commission').click(function () {
	//	$('.report').hide();
	//	$('.report.commission').show();
	//});
	//  }


});

function onChangeSaveIncomeExpenseSearch() {
	$(document).on('change', "#incomeExpenseSaveCriteria", function () {
		var id = $(this).val();
		$.ajax({
			type: 'GET',
			url: "/Report/SearchCriteriaGetData",
			data: { id: id },
			success: function (data) {
				var propertyIds = $.parseJSON(data.searchCriteria.PropertyIds);
				incomeExpenseSelectProperty.dropdown('clear');
				for (var x = 0; x < propertyIds.length; x++) {
					incomeExpenseSelectProperty.dropdown('set selected', [propertyIds[x]]);
				}
				chkIsPaidOnly.prop('checked', data.searchCriteria.IsPaid);
				$('#income-expense-report-type').dropdown('set selected', data.searchCriteria.ReportType);
				incomeExpenseSelectDateFrom.val(moment.utc(data.searchCriteria.From).format("MMMM YYYY"));
				incomeExpenseSelectDateTo.val(moment.utc(data.searchCriteria.To).format("MMMM YYYY"));
			}
		});
	});
}

function onChangeSaveCashflowSearch() {
	$(document).on('change', "#cashflowSaveCriteria", function () {
		var id = $(this).val();
		$.ajax({
			type: 'GET',
			url: "/Report/SearchCriteriaGetData",
			data: { id: id },
			success: function (data) {
				$ddlCashFlowPropertyType.dropdown('clear');
				var propertyIds = $.parseJSON(data.searchCriteria.PropertyIds);
				for (var x = 0; x < propertyIds.length; x++) {
					$ddlCashFlowPropertyType.dropdown('set selected', [propertyIds[x]]);
				}
				$('#cashflow-report-by').dropdown('set selected', data.searchCriteria.ReportType);
				$cashFlowStartDate.val(moment.utc(data.searchCriteria.From).format("MMMM YYYY"));
				$cashFlowEndDate.val(moment.utc(data.searchCriteria.To).format("MMMM YYYY"));
			}
		});
	});
}
function SaveIncomeExpenseSearch() {
	$(document).on('click', "#save-income-expense-criteria", function () {
		swal({
			title: "Save Search Criteria?",
			text: "",
			type: "input",
			confirmButtonColor: "##abdd54",
			confirmButtonText: "Accept",
			showCancelButton: true,
			closeOnConfirm: true,
			showLoaderOnConfirm: true,
			inputPlaceholder: "Write name"
		},
			function (message) {
				var propertyIds = [];
				for (var e = 0; e < incomeExpenseSelectProperty.val().length; e++) {
					propertyIds.push(incomeExpenseSelectProperty.val()[e]);
				}
				var formData = new FormData();
				formData.append("Name", message);
				formData.append("PropertyIds", JSON.stringify(propertyIds));
				formData.append("From", incomeExpenseSelectDateFrom.val());
				formData.append("To", incomeExpenseSelectDateTo.val());
				formData.append("Type", 2);
				formData.append("IsPaid", chkIsPaidOnly.is(":checked"));
				formData.append("ReportType", $('#income-expense-report-type').val());


				$.ajax({
					type: 'POST',
					url: "/Report/SaveSearch",
					data: formData,
					processData: false,
					contentType: false,
					success: function (data) {
						$('#incomeExpenseSaveCriteria').append('<option value="' + data.searchCriteria.Id + '">' + data.searchCriteria.Name + '</option>');
						$('#incomeExpenseSaveCriteria').parents('.field').removeAttr('hidden');
					}
				});
			});
	});
}
function SaveCashflowSearch() {

	$(document).on('click', "#save-cashflow-criteria", function () {
		swal({
			title: "Save Search Criteria?",
			text: "",
			type: "input",
			confirmButtonColor: "##abdd54",
			confirmButtonText: "Accept",
			showCancelButton: true,
			closeOnConfirm: true,
			showLoaderOnConfirm: true,
			inputPlaceholder: "Write name"
		},
			function (message) {
				var propertyIds = [];
				for (var e = 0; e < $ddlCashFlowPropertyType.val().length; e++) {
					propertyIds.push($ddlCashFlowPropertyType.val()[e]);
				}
				var formData = new FormData();
				formData.append("Name", message);
				formData.append("PropertyIds", JSON.stringify(propertyIds));
				formData.append("From", $cashFlowStartDate.val());
				formData.append("To", $cashFlowEndDate.val());
				formData.append("Type", 1);
				formData.append("ReportType", $('#cashflow-report-by').val());
				$.ajax({
					type: 'POST',
					url: "/Report/SaveSearch",
					data: formData,
					processData: false,
					contentType: false,
					success: function (data) {
						$('#cashflowSaveCriteria').append('<option value="' + data.searchCriteria.Id + '">' + data.searchCriteria.Name + '</option>');
						$('#cashflowSaveCriteria').parents('.field').removeAttr('hidden');
					}
				});
			});
	});
}
///

function AppInit() {
	$ddlCashFlowPropertyType.dropdown({
		allowAdditions: true
	});
	//LoadAllDropDownOptions();
}

function onCreateExpenses() {
	$('.js-btn_create-expenses').on('click', function () {
		$('.create-expenses-modal').modal('show');
	});
}
function LoadAllDropDownOptions() {
	$.ajax({
		url: "/CashFlow/GetAllDropDownOptions",
		type: "POST",
		success: function (data) {
			var appendPropertyTypeHtml = "";
			for (var index = 0; index < data.fullPropertyList.length; index++) {
				appendPropertyTypeHtml += " <option value=\"" + data.fullPropertyList[index].PropertyId + "\">" + data.fullPropertyList[index].PropertyName + "</option>";
			}
			$ddlCashFlowPropertyType.append(appendPropertyTypeHtml);

			var appendExpenseCategoryTypeHtml = "";
			for (var number = 0; number < data.fullExpenseCategoryList.length; number++) {
				appendExpenseCategoryTypeHtml += " <option value=\"" + data.fullExpenseCategoryList[number].ExpenseCategoryId + "\">" + data.fullExpenseCategoryList[number].ExpenseCategoryType + "</option>";
			}
			$ddlCashFlowExpenseType.append(appendExpenseCategoryTypeHtml);
		},
		error: function (data) {
			swal("Fail to Load All Drop Down Options.", "", "error");
		}
	});
}

function GetCashFlowInfoByMonth(month, propertyIds) {
	$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).html("");
	$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).append('<tr><td colspan="12"><div class="ui active centered inline loader"><div class="ui medium text loader">Loading</div></div></td></tr>');
	$.ajax({
		url: "/CashFlow/GetCashFlowInfoByMonth",
		type: "POST",
		data: {
			propertyIds: propertyIds,
			month: month,
			includeChild: $('#cashflow-include-child').is(":checked"),
		},
		success: function (data) {
			if (data.status == 1) {
				//Clear All Record
				$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).html("");
				if (data.cashFlowReportData.length > 0) {
					console.log(data.cashFlowReportData);
					var NewInvestment = 0;
					var PartnerInvestment = 0;
					var WriteOff = 0;
					var CashOut = 0;
					var PartnerDraw = 0;
					var TotalIncome = 0;
					var TotalExpense = 0;
					var ShareExpense = 0;
					var NettCashFlow = 0;
					var PartnerInvestmentDraw = 0;
					for (var value = 0; value < data.cashFlowReportData.length; value++) {

						NewInvestment += data.cashFlowReportData[value].NewInvestment;
						PartnerInvestment += data.cashFlowReportData[value].PartnerInvestment;
						WriteOff += data.cashFlowReportData[value].WriteOff;
						CashOut += data.cashFlowReportData[value].CashOut;
						PartnerDraw += data.cashFlowReportData[value].PartnerDraw;
						PartnerInvestmentDraw += data.cashFlowReportData[value].PartnerInvestmentDraw;
						TotalIncome += (data.cashFlowReportData[value].TotalIncome - data.cashFlowReportData[value].TotalIncomeRefund);
						TotalExpense += (data.cashFlowReportData[value].TotalExpense - data.cashFlowReportData[value].TotalExpenseRefund);
						//ShareExpense += data.cashFlowReportData[value].ShareExpense;
						NettCashFlow += data.cashFlowReportData[value].NettCashFlow;
						var html = "";
						html += "<tr>";
						html += "<td>" + data.cashFlowReportData[value].Property + "</td>";
						html += "<td>" + data.cashFlowReportData[value].StartingCash.toFixed(2) + (data.cashFlowReportData[value].SuggestedStartingCash.toFixed(2) == 0.00 ? "" : "<br><p " + (data.cashFlowReportData[value].StartingCash.toFixed(2) != data.cashFlowReportData[value].SuggestedStartingCash.toFixed(2) ? "style=\"color:red\" class=\"suggestion\" data-content=\"Doesn`t match computed amount\"" : "") + ">" + data.cashFlowReportData[value].SuggestedStartingCash.toFixed(2) + "</p>") + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].NewInvestmentSummary + "'>" + data.cashFlowReportData[value].NewInvestment.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].PartnerInvestmentSummary + "'>" + data.cashFlowReportData[value].PartnerInvestment.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].WriteOffSummary + "'>" + data.cashFlowReportData[value].WriteOff.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].CashOutSummary + "'>" + data.cashFlowReportData[value].CashOut.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].PartnerDrawSummary + "'>" + data.cashFlowReportData[value].PartnerDraw.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].PartnerInvestmentDrawSummary + "'>" + data.cashFlowReportData[value].PartnerInvestmentDraw.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].IncomeSummary + "'>" + (data.cashFlowReportData[value].TotalIncome.toFixed(2) - data.cashFlowReportData[value].TotalIncomeRefund.toFixed(2)).toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportData[value].ExpenseSummary + "'>" + (data.cashFlowReportData[value].TotalExpense.toFixed(2) - data.cashFlowReportData[value].TotalExpenseRefund.toFixed(2)).toFixed(2) + "</td>";
						//html += "<td>" + data.cashFlowReportData[value].ShareExpense.toFixed(2)+ "</td>";
						html += "<td>" + data.cashFlowReportData[value].NettCashFlow.toFixed(2) + "</td>";
						html += "<td>" + data.cashFlowReportData[value].EndingCash.toFixed(2) + "</td>";
						$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).append(html);
					}
					$('.suggestion').popup();
					var footer = "";
					footer += "<tfoot><tr>";
					footer += "<th><b>Total<b></th>";
					footer += "<th></th>";
					footer += "<th><b>" + NewInvestment.toFixed(2) + "</b></th>";
					footer += "<th><b>" + PartnerInvestment.toFixed(2) + "</b></th>";
					footer += "<th><b>" + WriteOff.toFixed(2) + "</b></th>";
					footer += "<th><b>" + CashOut.toFixed(2) + "</b></th>";
					footer += "<th><b>" + PartnerDraw.toFixed(2) + "</b></th>";
					footer += "<th><b>" + PartnerInvestmentDraw.toFixed(2) + "</b></th>";
					footer += "<th><b>" + TotalIncome.toFixed(2) + "</b></th>";
					footer += "<th><b>" + TotalExpense.toFixed(2) + "</b></th>";
					//footer += "<th><b>" + ShareExpense.toFixed(2) + "</b></th>";
					footer += "<th><b>" + NettCashFlow.toFixed(2) + "</b></th>";
					footer += "<th></th></tr></tfoot>";
					//Create footer with total computation
					$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table').children('tfoot').remove();
					$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table').append(footer);

					if ($.fn.DataTable.isDataTable($("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table'))) {
						$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table').DataTable({
							"ordering": false, fixedHeader: {
								header: true,
								footer: true
							}
						});
					}
				} else {
					var noRecordHtml = "<tr><td colspan='12'>No Records</td></tr>";
					$("#cashFlowReportDashboard" + moment.utc(month).format("MMMM-YYYY")).append(noRecordHtml);
				}
			} else {
				swal("Fail to Get Cash Flow Info. Please Complete Fields Try Again", "", "error");
			}
		},
		error: function (data) {
			swal("Fail to Get Cash Flow Info. Please Complete Fields Try Again", "", "error");
		}
	});


}
function GetCashFlowInfo(propertyId, dateFrom, dateTo) {
	$("#cashFlowReportDashboard" + propertyId).html("");
	$("#cashFlowReportDashboard" + propertyId).append('<tr><td colspan="12"><div class="ui active centered inline loader"><div class="ui medium text loader">Loading</div></div></td></tr>');


	$.ajax({
		url: "/CashFlow/GetCashFlowInfo",
		type: "POST",
		data: {
			propertyId: !isEmtpyValue(propertyId) ? propertyId : 0,
			dateFrom: dateFrom,
			dateTo: dateTo,
			includeChild: $('#cashflow-include-child').is(":checked"),
		},
		success: function (data) {
			if (data.status == 1) {
				//Clear All Record
				$("#cashFlowReportDashboard" + propertyId).html("");
				if (data.cashFlowReportList.length > 0) {

					var NewInvestment = 0;
					var PartnerInvestment = 0;
					var WriteOff = 0;
					var CashOut = 0;
					var PartnerDraw = 0;
					var TotalIncome = 0;
					var TotalExpense = 0;
					var ShareExpense = 0;
					var NettCashFlow = 0;
					var PartnerInvestmentDraw = 0;
					for (var value = 0; value < data.cashFlowReportList.length; value++) {
						NewInvestment += data.cashFlowReportList[value].NewInvestment;
						PartnerInvestment += data.cashFlowReportList[value].PartnerInvestment;
						WriteOff += data.cashFlowReportList[value].WriteOff;
						CashOut += data.cashFlowReportList[value].CashOut;
						PartnerDraw += data.cashFlowReportList[value].PartnerDraw;
						PartnerInvestmentDraw += data.cashFlowReportList[value].PartnerInvestmentDraw;
						TotalIncome += (data.cashFlowReportList[value].TotalIncome - data.cashFlowReportList[value].TotalIncomeRefund);
						TotalExpense += (data.cashFlowReportList[value].TotalExpense - data.cashFlowReportList[value].TotalExpenseRefund);
						ShareExpense += data.cashFlowReportList[value].ShareExpense;
						NettCashFlow += data.cashFlowReportList[value].NettCashFlow;
						var html = "";
						html += "<tr>";
						html += "<td>" + getMonth(data.cashFlowReportList[value].Month) + "-" + data.cashFlowReportList[value].Year + "</td>";
						html += "<td>" + data.cashFlowReportList[value].StartingCash.toFixed(2) + (data.cashFlowReportList[value].SuggestedStartingCash.toFixed(2) == 0.00 ? "" : "<br><p " + (data.cashFlowReportList[value].StartingCash.toFixed(2) != data.cashFlowReportList[value].SuggestedStartingCash.toFixed(2) ? "style=\"color:red\" class=\"suggestion\" data-content=\"Doesn`t match computed amount\"" : "") + ">" + data.cashFlowReportList[value].SuggestedStartingCash.toFixed(2) + "</p>") + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].NewInvestmentSummary + "'>" + data.cashFlowReportList[value].NewInvestment.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].PartnerInvestmentSummary + "'>" + data.cashFlowReportList[value].PartnerInvestment.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].WriteOffSummary + "'>" + data.cashFlowReportList[value].WriteOff.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].CashOutSummary + "'>" + data.cashFlowReportList[value].CashOut.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].PartnerDrawSummary + "'>" + data.cashFlowReportList[value].PartnerDraw.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].PartnerInvestmentDrawSummary + "'>" + data.cashFlowReportList[value].PartnerInvestmentDraw.toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].IncomeSummary + "'>" + (data.cashFlowReportList[value].TotalIncome.toFixed(2) - data.cashFlowReportList[value].TotalIncomeRefund.toFixed(2)).toFixed(2) + "</td>";
						html += "<td summary='" + data.cashFlowReportList[value].ExpenseSummary + "'>" + (data.cashFlowReportList[value].TotalExpense.toFixed(2) - data.cashFlowReportList[value].TotalExpenseRefund.toFixed(2)).toFixed(2) + "</td>";
						//html += "<td>" + data.cashFlowReportList[value].ShareExpense.toFixed(2)+ "</td>";
						html += "<td>" + data.cashFlowReportList[value].NettCashFlow.toFixed(2) + "</td>";
						html += "<td>" + data.cashFlowReportList[value].EndingCash.toFixed(2) + "</td>";
						$("#cashFlowReportDashboard" + propertyId).append(html);

					}
					$('.suggestion').popup();
					var footer = "";
					footer += "<tfoot><tr>";
					footer += "<th><b>Total<b></th>";
					footer += "<th></th>";
					footer += "<th><b>" + NewInvestment.toFixed(2) + "</b></th>";
					footer += "<th><b>" + PartnerInvestment.toFixed(2) + "</b></th>";
					footer += "<th><b>" + WriteOff.toFixed(2) + "</b></th>";
					footer += "<th><b>" + CashOut.toFixed(2) + "</b></th>";
					footer += "<th><b>" + PartnerDraw.toFixed(2) + "</b></th>";
					footer += "<th><b>" + PartnerInvestmentDraw.toFixed(2) + "</b></th>";
					footer += "<th><b>" + TotalIncome.toFixed(2) + "</b></th>";
					footer += "<th><b>" + TotalExpense.toFixed(2) + "</b></th>";
					footer += "<th><b>" + NettCashFlow.toFixed(2) + "</b></th>";
					footer += "<th></th></tr></tfoot>";
					//Create footer with total computation
					$("#cashFlowReportDashboard" + propertyId).parents('table').children('tfoot').remove();
					$("#cashFlowReportDashboard" + propertyId).parents('table').append(footer);

					if ($.fn.DataTable.isDataTable($("#cashFlowReportDashboard" + propertyId).parents('table'))) {
						$("#cashFlowReportDashboard" + propertyId).parents('table').DataTable({
							"ordering": false, fixedHeader: {
								header: true,
								footer: true
							}
						});
					}
				} else {
					var noRecordHtml = "<tr><td colspan='12'>No Records</td></tr>";
					$("#cashFlowReportDashboard" + propertyId).append(noRecordHtml);
				}
			} else {
				swal("Fail to Get Cash Flow Info. Please Complete Fields Try Again", "", "error");
			}
		},
		error: function (data) {
			swal("Fail to Get Cash Flow Info. Please Complete Fields Try Again", "", "error");
		}
	});


}

function populateSelectedPropertyResult() {
	$cashFlowMainAccordion.html("");
	PopulateDashboardTotal();
	if ($('#cashflow-report-by').val() == 1) {
		var start = new Date($cashFlowStartDate.val());
		var end;
		if ($cashFlowEndDate.val() == null || $cashFlowEndDate.val() == "") {
			end = start;
		}
		else {
			end = new Date($cashFlowEndDate.val());
		}
		var loop = new Date(start);
		while (loop <= end) {
			var renderAccordionHtml = "";
			var date = moment(loop).format("MMMM YYYY");
			renderAccordionHtml += "<div class=\"title\" onclick=\"searchCashFlowInfoByMonth('" + date + "');\"><i class=\"icon dropdown\"></i>" + date + "</div>";
			renderAccordionHtml += "<div class=\"content\"><table class=\"ui celled orange table\"><thead><tr><th>Property</th><th>Starting Cash</th><th>New Investment</th><th>Partner Investment</th><th>Write-off</th><th>Cash Out</th><th>Owner Draw</th><th>Partner Investment Draw</th><th>Total Income</th><th>Total Expense</th><th>Net-Cash-Flow</th><th>Ending Cash</th></tr></thead><tbody class=\"cashflow-summary\" id=\"cashFlowReportDashboard" + moment(loop).format("MMMM-YYYY") + "\"></tbody></table></div>";
			$cashFlowMainAccordion.append(renderAccordionHtml);
			var newDate = loop.setMonth(loop.getMonth() + 1);
			loop = new Date(newDate);
		}
	}
	else {
		for (var e = 0; e < $ddlCashFlowPropertyType.val().length; e++) {
			var renderAccordionHtml = "";
			var propertyId = parseInt($ddlCashFlowPropertyType.val()[e]);
			renderAccordionHtml += "<div class=\"title\" onclick=\"searchCashFlowInfo(" + propertyId + ");\"><i class=\"icon dropdown\"></i>" + GetPropertyText(propertyId) + "</div>";
			renderAccordionHtml += "<div class=\"content\"><table class=\"ui celled orange table\"><thead><tr><th>Date</th><th>Starting Cash</th><th>New Investment</th><th>Partner Investment</th><th>Write-off</th><th>Cash Out</th><th>Owner Draw</th><th>Partner Investment Draw</th><th>Total Income</th><th>Total Expense</th><th>Net-Cash-Flow</th><th>Ending Cash</th></tr></thead><tbody class=\"cashflow-summary\" id=\"cashFlowReportDashboard" + propertyId + "\"></tbody></table></div>";
			$cashFlowMainAccordion.append(renderAccordionHtml);
		}
	}
}

function searchCashFlowInfoByMonth(month) {
	var propertyIds = [];
	for (var e = 0; e < $ddlCashFlowPropertyType.val().length; e++) {
		propertyIds.push($ddlCashFlowPropertyType.val()[e]);
	}
	GetCashFlowInfoByMonth(month, JSON.stringify(propertyIds));
}
function searchCashFlowInfo(propertyId) {
	GetCashFlowInfo(propertyId, $cashFlowStartDate.val(), $cashFlowEndDate.val());
}

function PopulateDashboardTotal() {

	var propertyList = $ddlCashFlowPropertyType.val();
	var propertyItem = "";

	for (var e = 0; e < propertyList.length; e++) {
		if (e != propertyList.length - 1) {
			propertyItem += propertyList[e].toString() + ",";
		} else {
			propertyItem += propertyList[e].toString();
		}
	}

	$.ajax({
		url: "/CashFlow/GetDashboardTotal",
		type: "POST",
		data: {
			propertyList: propertyItem,
			dateFrom: $cashFlowStartDate.val(),
			dateTo: $cashFlowEndDate.val(),
			includeChild: $('#cashflow-include-child').is(":checked"),
		},
		success: function (data) {
			if (data.status == 1) {
				$cashFlowFullTotalIncome.text(data.fullTotalIncome);
				$cashFlowFullTotalExpense.text(data.fullTotalExpense);
				//$cashFlowFullTotalSharedExpense.text(data.fullTotalShareExpense);
				$cashFlowFullTotalNettCashFlow.text(data.fullTotalNetCashFlow);
			} else {
				swal("Fail to Populate Info.Please Complete Fields and Try Again.", "", "error");
			}
		},
		error: function (data) {
			swal("Fail to Populate Info.Please Complete Fields and Try Again.", "", "error");
		}
	});
}

function isEmtpyValue(value) {
	if (value == null || value == "" || value == undefined) {
		return true;
	}
	return false;
}

function getMonth(monthValue) {
	return monthNames[monthValue - 1];
}

function GetPropertyText(value) {
	return $('#ddlCashFlowPropertyType option[value=' + value + ']').text();
}

//Start of Income Expense Report

var incomeExpenseSelectProperty = $("#incomeExpenseSelectProperty");
var incomeExpenseInfoDashboard = $("#incomeExpenseInfoDashboard");
var incomeExpenseSelectDateFrom = $("#incomeExpenseSelectDateFrom");
var incomeExpenseSelectDateTo = $("#incomeExpenseSelectDateTo");
var incomeExpenseSelectExpenseCategory = $("#incomeExpenseSelectExpenseCategory");
var incomeExpenseMainAccordion = $("#incomeExpenseMainAccordion");
var chkIsPaidOnly = $("#chkIsPaidOnly");
var selectedPropertyIds = [];

function AppInit() {
	incomeExpenseSelectProperty.dropdown({
		allowAdditions: true

	});


	commissionGoButton.click(function () {
		GetCommissionsReport();
	});
}


function GetIncomeExpenseInfoByMonth(month, propertyIds, isPaidOnly, includeChild) {
	$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).html("");
	$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).append('<tr><td colspan="9"><div class="ui active centered inline loader">Loading</div></td></tr>');
	$.ajax({
		url: "/Report/GetIncomeExpenseInfoByMonth",
		type: "POST",
		data: {
			date: month,
			propertyIds: propertyIds,
			isPaidOnly: isPaidOnly,
			includeChild: includeChild
		},
		success: function (data) {
			$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).html("");
			if (data.incomeExpenseInfoList.length != 0) {
				var IncomeAmount = 0;
				var ExpenseAmount = 0;
				var IncomeRefund = 0;
				var ExpenseRefund = 0;
				var StartupCost = 0;
				var StartupCostRefund = 0;
				var OperatingNetProfit = 0;
				var NetProfit = 0;
				for (var value = 0; value < data.incomeExpenseInfoList.length; value++) {
					IncomeAmount += data.incomeExpenseInfoList[value].IncomeAmount;
					ExpenseAmount += data.incomeExpenseInfoList[value].ExpenseAmount;
					IncomeRefund += data.incomeExpenseInfoList[value].IncomeRefund;
					ExpenseRefund += data.incomeExpenseInfoList[value].ExpenseRefund;
					StartupCost += data.incomeExpenseInfoList[value].StartupCost;
					StartupCostRefund += data.incomeExpenseInfoList[value].StartupCostRefund;
					OperatingNetProfit += data.incomeExpenseInfoList[value].OperatingNetProfit;
					NetProfit += data.incomeExpenseInfoList[value].NetProfit;
					var html = "";
					html += "<tr>";
					html += "<td>" + data.incomeExpenseInfoList[value].Property + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].IncomeSummary + "'>" + data.incomeExpenseInfoList[value].IncomeAmount + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].IncomeRefundSummary + "'>" + data.incomeExpenseInfoList[value].IncomeRefund + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].ExpenseSummary + "'>" + data.incomeExpenseInfoList[value].ExpenseAmount + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].ExpenseRefundSummary + "'>" + data.incomeExpenseInfoList[value].ExpenseRefund + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].StartUpCostSummary + "'>" + data.incomeExpenseInfoList[value].StartupCost + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].StartupCostRefundSummary + "'>" + data.incomeExpenseInfoList[value].StartupCostRefund + "</td>";
					html += "<td>" + data.incomeExpenseInfoList[value].OperatingNetProfit + "</td>";
					html += "<td>" + data.incomeExpenseInfoList[value].NetProfit + "</td></tr>";
					$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).append(html);
				}
				var footer = "";
				footer += "<tfoot><tr>";
				footer += "<th><b>Total<b></th>";
				footer += "<th><b>" + IncomeAmount.toFixed(2) + "</b></th>";
				footer += "<th><b>" + IncomeRefund.toFixed(2) + "</b></th>";
				footer += "<th><b>" + ExpenseAmount.toFixed(2) + "</b></th>";
				footer += "<th><b>" + ExpenseRefund.toFixed(2) + "</b></th>";
				footer += "<th><b>" + StartupCost.toFixed(2) + "</b></th>";
				footer += "<th><b>" + StartupCostRefund.toFixed(2) + "</b></th>";
				footer += "<th><b>" + OperatingNetProfit.toFixed(2) + "</b></th>";
				footer += "<th><b>" + NetProfit.toFixed(2) + "</b></th></tr></tfoot>";
				//Create footer with total computation
				$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table').children('tfoot').remove();
				$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table').append(footer);

				if ($.fn.DataTable.isDataTable($("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table'))) {
					$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).parents('table').DataTable({
						"ordering": false, fixedHeader: {
							header: true,
							footer: true
						}
					});
				}

			} else {
				var emptyRowMessage = "<tr><td colspan='9'>No Records</td></tr>";
				$("#incomeExpenseInfoDashboard" + moment.utc(month).format("MMMM-YYYY")).append(emptyRowMessage);

			}
		},
		error: function (data) {
			swal("Fail to Get Income Expenses Info. Please Try Again", "", "error");
		}
	});
}
function GetIncomeExpenseInfo(propertyId, dateFrom, dataTo, isPaidOnly, includeChild) {
	$("#incomeExpenseInfoDashboard" + propertyId).html("");
	$("#incomeExpenseInfoDashboard" + propertyId).append('<tr><td colspan="9"><div class="ui active centered inline loader"><div class="ui medium text loader">Loading</div></div></td></tr>');
	$.ajax({
		url: "/Report/GetIncomeExpenseInfo",
		type: "POST",
		data: {
			propertyId: !isEmtpyValue(propertyId) ? propertyId : 0,
			dateFrom: dateFrom,
			dateTo: dataTo,
			isPaidOnly: isPaidOnly,
			includeChild: includeChild
		},
		success: function (data) {
			$("#incomeExpenseInfoDashboard" + propertyId).html("");
			if (data.incomeExpenseInfoList.length != 0) {
				var IncomeAmount = 0;
				var ExpenseAmount = 0;
				var IncomeRefund = 0;
				var ExpenseRefund = 0;
				var StartupCost = 0;
				var StartupCostRefund = 0;
				var OperatingNetProfit = 0;
				var NetProfit = 0;
				for (var value = 0; value < data.incomeExpenseInfoList.length; value++) {
					IncomeAmount += data.incomeExpenseInfoList[value].IncomeAmount;
					ExpenseAmount += data.incomeExpenseInfoList[value].ExpenseAmount;
					IncomeRefund += data.incomeExpenseInfoList[value].IncomeRefund;
					ExpenseRefund += data.incomeExpenseInfoList[value].ExpenseRefund;
					StartupCost += data.incomeExpenseInfoList[value].StartupCost;
					StartupCostRefund += data.incomeExpenseInfoList[value].StartupCostRefund;
					OperatingNetProfit += data.incomeExpenseInfoList[value].OperatingNetProfit;
					NetProfit += data.incomeExpenseInfoList[value].NetProfit;
					var html = "";
					html += "<tr>";
					html += "<td>" + getMonth(data.incomeExpenseInfoList[value].Month) + " " + data.incomeExpenseInfoList[value].Year + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].IncomeSummary + "'>" + data.incomeExpenseInfoList[value].IncomeAmount + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].IncomeRefundSummary + "'>" + data.incomeExpenseInfoList[value].IncomeRefund + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].ExpenseSummary + "'>" + data.incomeExpenseInfoList[value].ExpenseAmount + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].ExpenseRefundSummary + "'>" + data.incomeExpenseInfoList[value].ExpenseRefund + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].StartUpCostSummary + "'>" + data.incomeExpenseInfoList[value].StartupCost + "</td>";
					html += "<td summary='" + data.incomeExpenseInfoList[value].StartupCostRefundSummary + "'>" + data.incomeExpenseInfoList[value].StartupCostRefund + "</td>";
					html += "<td>" + data.incomeExpenseInfoList[value].OperatingNetProfit + "</td>";
					html += "<td>" + data.incomeExpenseInfoList[value].NetProfit + "</td></tr>";
					console.log(html);
					$("#incomeExpenseInfoDashboard" + propertyId).append(html);
				}
				var footer = "";
				footer += "<tfoot><tr>";
				footer += "<th><b>Total<b></th>";
				footer += "<th><b>" + IncomeAmount.toFixed(2) + "</b></th>";
				footer += "<th><b>" + IncomeRefund.toFixed(2) + "</b></th>";
				footer += "<th><b>" + ExpenseAmount.toFixed(2) + "</b></th>";
				footer += "<th><b>" + ExpenseRefund.toFixed(2) + "</b></th>";
				footer += "<th><b>" + StartupCost.toFixed(2) + "</b></th>";
				footer += "<th><b>" + StartupCostRefund.toFixed(2) + "</b></th>";
				footer += "<th><b>" + OperatingNetProfit.toFixed(2) + "</b></th>";
				footer += "<th><b>" + NetProfit.toFixed(2) + "</b></th></tr></tfoot>";

				//Create footer with total computation
				$("#incomeExpenseInfoDashboard" + propertyId).parents('table').children('tfoot').remove();
				$("#incomeExpenseInfoDashboard" + propertyId).parents('table').append(footer);

				if ($.fn.DataTable.isDataTable($("#incomeExpenseInfoDashboard" + propertyId).parents('table'))) {
					$("#incomeExpenseInfoDashboard" + propertyId).parents('table').DataTable({
						"ordering": false, fixedHeader: {
							header: true,
							footer: true
						}
					});
				}
			} else {
				var emptyRowMessage = "<tr><td colspan='9'>No Records</td></tr>";
				$("#incomeExpenseInfoDashboard" + propertyId).append(emptyRowMessage);

			}
		},
		error: function (data) {
			swal("Fail to Get Income Expenses Info. Please Try Again", "", "error");
		}
	});
}

function loadSearchResultAccordion() {
	incomeExpenseMainAccordion.html("");
	if ($('#income-expense-report-type').val() == 1) {

		var start = new Date(incomeExpenseSelectDateFrom.val());
		var end;
		if (incomeExpenseSelectDateTo.val() == null || incomeExpenseSelectDateTo.val() == "") {
			end = start;
		}
		else {
			end = new Date(incomeExpenseSelectDateTo.val());
		}


		var loop = new Date(start);
		while (loop <= end) {
			var renderAccordionHtml = "";
			var date = moment(loop).format("MMMM YYYY");
			renderAccordionHtml += "<div class=\"title\" onclick=\"searchIncomeExpenseInfoByMonth('" + date + "');\"><i class=\"icon dropdown\"></i>" + date + "</div>";
			//renderAccordionHtml += "<div class=\"content\"><table class=\"ui celled orange table\"><thead><tr><th>Property</th><th>Income</th><th>Income Refund</th><th>Expense</th><th>Expense Refund</th><th>Startup Cost</th><th>Startup Cost Refund</th><th>Operating Net Profit</th><th>Net Profit</th></tr></thead><tbody class=\"income-expense-report\" id=\"incomeExpenseInfoDashboard" + moment(loop).format("MMMM-YYYY")+ "\"></tbody></table></div>";
			renderAccordionHtml += "<div class=\"content\"><table class=\"ui celled orange table\"><thead><tr><th>Property</th><th>Income</th><th>Income Refund</th><th>Expense</th><th>Expense Refund</th><th>Startup Cost</th><th>Startup Cost Refund</th><th>Operating Net Profit</th><th>Net Profit</th></tr></thead><tbody class=\"income-expense-report\" id=\"incomeExpenseInfoDashboard" + moment(loop).format("MMMM-YYYY") + "\"></tbody></table></div>";

			incomeExpenseMainAccordion.append(renderAccordionHtml);
			var newDate = loop.setMonth(loop.getMonth() + 1);
			loop = new Date(newDate);
		}
	}
	else {
		for (var e = 0; e < incomeExpenseSelectProperty.val().length; e++) {
			var renderAccordionHtml = "";
			var propertyId = parseInt(incomeExpenseSelectProperty.val()[e]);
			renderAccordionHtml += "<div class=\"title\" onclick=\"searchIncomeExpenseInfo(" + propertyId + ");\"><i class=\"icon dropdown\"></i>" + GetPropertyText(propertyId) + "</div>";
			renderAccordionHtml += "<div class=\"content\"><table class=\"ui celled orange table\"><thead><tr><th>Date</th><th>Income</th><th>Income Refund</th><th>Expense</th><th>Expense Refund</th><th>Startup Cost</th><th>Startup Cost Refund</th><th>Operating Net Profit</th><th>Net Profit</th></tr></thead><tbody class=\"income-expense-report\" id=\"incomeExpenseInfoDashboard" + propertyId + "\"></tbody></table></div>";
			incomeExpenseMainAccordion.append(renderAccordionHtml);
		}
	}
}
function IncomeExpenseShowSummary() {
	$(document).on('click', ".income-expense-report tr td", function () {
		if ($(this).attr('summary') != undefined) {
			$('#summary-modal').find('.content').html($.parseJSON($(this).attr('summary')));
			$('.summary-table').DataTable();
			$('#summary-modal').modal('show').modal('refresh');
		}
	});
}
function CashflowShowSummary() {
	$(document).on('click', ".cashflow-summary tr td", function () {
		if ($(this).attr('summary') != undefined) {
			$('#summary-modal').find('.content').html($.parseJSON($(this).attr('summary')));
			$('.summary-table').DataTable();
			$('#summary-modal').modal('show').modal('refresh');
		}
	});
}
function searchIncomeExpenseInfo(propertyId) {
	GetIncomeExpenseInfo(propertyId, incomeExpenseSelectDateFrom.val(), incomeExpenseSelectDateTo.val(), chkIsPaidOnly.is(":checked"), $('#income-expense-include-child').is(":checked"));
}
function searchIncomeExpenseInfoByMonth(month) {
	var propertyIds = [];
	for (var e = 0; e < incomeExpenseSelectProperty.val().length; e++) {
		propertyIds.push(incomeExpenseSelectProperty.val()[e]);
	}
	GetIncomeExpenseInfoByMonth(month, JSON.stringify(propertyIds), chkIsPaidOnly.is(":checked"), $('#income-expense-include-child').is(":checked"));
}
function GetCommissionsReport() {
	var selectedProperties = commissionSelectProperty.val();
	$.ajax({
		url: "/Report/GetCommissionsReport",
		type: "POST",
		data: {
			selectedProperties: !isEmtpyValue(selectedProperties) ? selectedProperties.join() : "-1",
			selectedDate: commissionSelectDate.val()
		},
		success: function (data) {
			$("#CommissionTableRecords").html("");
			if (data.result.length != 0) {
				var html = "";
				for (var value = 0; value < data.result.length; value++) {
					html += "<tr>";
					html += "<td>" + data.result[value].PropertyName + "</td>";
					html += "<td>" + data.result[value].CommissionType + "</td>";
					html += "<td>" + data.result[value].Unit + "</td>";
					html += "<td>" + data.result[value].NetProfit + "</td>";
					html += "<td>" + data.result[value].Total + "</td></tr>";
				}
				$("#CommissionTableRecords").append(html);
				commissionEmptyRecord.hide();
			} else {
				commissionEmptyRecord.show();
			}
		},
		error: function (data) {
			swal("Fail to Get Commission Reports. Please Try Again", "", "error");
		}
	});
}