﻿$(document).ready(function () {

	var tableAccounts;
	initialize();

	function initialize() {
		setupSemanticUI();
		setEventListeners();
		onAutoSyncToggle();
		onSelectAirlockChoice();
		submitAirlockCode();
		onBackToAirlockChoices();
		loadTableData();
	}

	function loadTableData() {
		tableAccounts = $('#accounts-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Import/GetAccountList",
				"type": 'POST'
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Site" },
				{ "data": "AutoSync", "orderable": false },
				{ "data": "Username" },
				{ "data": "Actions", "orderable": false }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-account-id', data.Id);
				$(row).attr('data-siteType', data.SiteType);
			}
		});
	}

	function setupSemanticUI() {
		$('.ui.accordion').accordion();
		$('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
		$('.ui.checkbox').checkbox();
		$('.ui.calendar').calendar({ type: 'date' });
		$('.ui.calendar.month').calendar({ type: 'month' });
		$('.ui.calendar.time').calendar({ type: 'time' });
		$('table td a.table-tooltip').popup();
		$('.message .close').on('click', function () {
			$(this).closest('.message').transition('fade');
		});
	}

	function setEventListeners() {
		onAddAccount();
		onStartImport();
		onStartRedownload();
		onStartReconnect();
		onAddGmailAccount();
		SubmitGmailAccount();
	}

	// ------------------
	// ------------------
	//   Event Listeners
	// ------------------
	// ------------------

	function onAutoSyncToggle() {
		$(document).on('change', '.autosync', function () {
			console.log($(this));
			$(this).checkbox();
			var id = $(this).parents('tr').attr('data-account-id');
			var toggle = $(this).find('input').is(':checked');
			$.ajax({
				type: "POST",
				url: "/Import/ChangeAutoSyncStatus",
				data: { id: id, toggle: toggle },
				success: function (data) {
				}
			});
		});
	}

	function onStartImport() {
		$('.js-btn_start-import').on('click', function (e) {
			e.preventDefault();
			$('#import-error-msg').css('display', 'none');
			$('#import-loader').css('display', 'none');
			var username = $('#import-username').val();
			var password = $('#import-password').val();
			var isLogin = false;

			if (username == '' || password == '') {
				$('#import-error-msg').find('.header').text('Please fill all fields.');
				$('#import-error-msg').css('display', 'block');
			}
			else {
				$.ajax({
					type: "POST",
					url: "/Import/ImportLogin",
					data: { username: username, password: password },
					success: function (data) {
						console.log(data);
						if (data.success) {
					
							isLogin = true;

							downloadData(username, password);
						}
						else if (!data.success && data.is_airlock) {
							$('#import-form-fields').hide();
							$('#submit-airlock-choice-error').hide();
							$('#submit-airlock-code-error').hide();
							$('#airlock-code').val('');
							$('#host-name').html(data.airlock.Name);
							$('#host-profile-pic').attr('src', data.airlock.ProfileImageUrl);
							$('#add-new-account-modal').attr('data-lock-id', data.airlock.LockId);
							$('#add-new-account-modal').attr('data-user-id', data.airlock.UserId);
							$('#add-new-account-modal').attr('data-phone-id', data.airlock.PhoneId);
							$('#add-new-account-modal').attr('data-is-new', data.airlock.IsNew);
							$('#airlock-wrapper').show();
							$('#airlock-choices').show();
							$('#enter-code').hide();
						}
						else {
							$('#import-error-msg').find('.header').text('Invalid username or password.');
							$('#import-error-msg').css('display', 'block');
							isLogin = false;
						}
					}
				});
			}
		});
	}

	function downloadData(username, password) {
		tableAccounts.ajax.reload(null, false);
		$.ajax({
			type: "POST",
			url: "/Import/PostImport",
			data: { username: username, password: password },
			beforeSend: function (data) {
				$('#import-form-fields').css('display', 'none');
				$('#import-loader').css('display', 'block');
			},
			success: function (data) {
				console.log(data);
				if (data.success) {
					$('#import-username').val('');
					$('#import-password').val('');
					$('#import-loader').css('display', 'none');
					$('#add-new-account-modal').modal("hide");
					swal({ title: "Success", text: "Account import complete.", type: "success" },
						function () {
							location.reload();
						});
				}
				else {
					$('#import-error-msg').find('.header').text('Unable to download account data.');
					$('#import-error-msg').css('display', 'block');
				}
			},
			error: function (error) {
				console.log(error);
				$('#import-error-msg').find('.header').text('Unable to connect to server.');
				$('#import-error-msg').css('display', 'block');
			},
		});
	}

	function onAddAccount() {
		$('.js-btn_add-new-account').on('click', function () {
			//$('#airlock-wrapper').hide();
			//$('#airlock-code').val('');
			//$('#import-form-fields').css('display', 'flex');
			//$('#import-error-msg').hide();
			//$('#airlock-wrapper').hide();

			//$("#site-type").val("");
			//$("#import-modal-title").text("Choose Site Where to Import");
			//$("#import-modal-account").hide();
			//$("#import-modal-account-action").hide();
			//$("#import-modal-site-type").show();
			$('#import-modal').modal('show');
			$('#import-modal').modal('refresh');
		});
	}

	function onSelectAirlockChoice() {
		$(document).on('click', '.airlock-choice', function () {
			var choice = $(this).attr('data-airlock-option');
			$('#add-new-account-modal').attr('data-selected-airlock-choice', choice);
			var lockId = $('#add-new-account-modal').attr('data-lock-id');
			var userId = $('#add-new-account-modal').attr('data-user-id');
			var phoneId = $('#add-new-account-modal').attr('data-phone-id');
			var isNew = $('#add-new-account-modal').attr('data-is-new');
			$.ajax({
				type: 'POST',
				url: "/Airlock/AirLockChoice",
				data: {
					lockId: lockId,
					userId: userId,
					phoneId: phoneId,
					choice: choice,
					isNew: isNew
				},
				dataType: "json",
				success: function (data) {
					if (data.success) {
						$('#airlock-choices').hide();
						$('#enter-code').show();
					}
					else {
						$('#submit-airlock-choice-error').show();
					}
				}
			});
		});
	}

	function submitAirlockCode() {
		$(document).on('click', '#submit-code-btn', function () {
			var code = $('#airlock-code').val();
			var lockId = $('#add-new-account-modal').attr('data-lock-id');
			var userId = $('#add-new-account-modal').attr('data-user-id');
			var phoneId = $('#add-new-account-modal').attr('data-phone-id');
			var isNew = $('#add-new-account-modal').attr('data-is-new');
			var choice = $('#add-new-account-modal').attr('data-selected-airlock-choice');
			var username = $('#import-username').val();
			var password = $('#import-password').val();
			if (code != '') {
				$.ajax({
					type: 'POST',
					url: "/Airlock/Verify",
					data: {
						code: code,
						lockId: lockId,
						userId: userId,
						phoneId: phoneId,
						choice: choice,
						isNew: isNew
					},
					dataType: "json",
					success: function (data) {
						if (data.success) {
							$('#airlock-wrapper').hide();
							isLogin = true;
							 downloadData(username, password);
						}
						else {
							$('#submit-airlock-code-error').show();
						}
					}
				});
			}
		});
	}

	function onBackToAirlockChoices() {
		$(document).on('click', '#back-to-airlock-choice-btn', function () {
			$('#enter-code').hide();
			$('#airlock-choices').show();
		});
	}

	//var siteType = "";
	//var siteTypeIndex = 0;
	//var vrboAirlockViewModel;
	//var bookingAirlockViewModel;
	var isReconnect = false;

	function changeSiteType(type) {
		siteType = type;
		$("#site-type").val(type);

		if (type == "Airbnb")
			siteTypeIndex = 1;
		else if (type == "Vrbo")
			siteTypeIndex = 2;
		else if (type == "Booking")
		    siteTypeIndex = 3;
		else if (type == "Homeaway")
		    siteTypeIndex = 4;
	}

	function changeSiteTypeIndex(type) {
		siteTypeIndex = type;

		if (type == 1)
			siteType = "Airbnb";
		else if (type == 2)
			siteType = "Vrbo";
		else if (type == 3)
			siteType = "Booking"
		else if (type == 4)
			siteType = "Homeaway";

		$("#site-type").val(siteType);
	}

	function download() {
		tableAccounts.ajax.reload(null, false);
		$.ajax({
			type: 'POST',
			url: "/Import/PostImport",
			data: {
				username: $("#import-modal-account-username").val(),
				password: $("#import-modal-account-password").val(),
				siteType: siteTypeIndex
			},
			dataType: 'json',
			success: function (result) {
				$("#import-modal").modal('hide');
				//$("#import-modal-importing-success-message").show();
				//window.location.reload();
			}
		});
	}

	$("#import-from-airbnb").click(function () {
		changeSiteType("Airbnb");
		$("#import-modal-title").text("Log In Your " + siteType + " Account");
		$('#import-modal-account-username').removeAttr('disabled');
		$("#import-modal-site-type").hide();
		$("#import-modal-account").show();
		$("#import-modal-account-action").show();
		$("#import-modal").modal('refresh');
	});

	$("#import-from-vrbo").click(function () {
		changeSiteType("Vrbo");
		$("#import-modal-title").text("Log In Your " + siteType + " Account");
		$('#import-modal-account-username').removeAttr('disabled');
		$("#import-modal-site-type").hide();
		$("#import-modal-account").show();
		$("#import-modal-account-action").show();
		$('#import-modal').modal('refresh');
	});

	$("#import-from-booking").click(function () {
		changeSiteType("Booking");
		$("#import-modal-title").text("Log In Your " + siteType + " Account");
		$('#import-modal-account-username').removeAttr('disabled');
		$("#import-modal-site-type").hide();
		$("#import-modal-account").show();
		$("#import-modal-account-action").show();
		$("#import-modal").modal('refresh');
	});

	$("#import-from-homeaway").click(function () {
	    changeSiteType("Homeaway");
	    debugger;
		$("#import-modal-title").text("Log In Your " + siteType + " Account");
		$('#import-modal-account-username').removeAttr('disabled');
	    $("#import-modal-site-type").hide();
	    $("#import-modal-account").show();
		$("#import-modal-account-action").show();
		$("#import-modal").modal('refresh');
	});

	$("#create-propertydb-host").click(function () {
		$("#import-modal-title").text("Create Your PropertyDB Host Account");
		$('#import-modal-account-username').removeAttr('disabled');
	    $("#import-modal-site-type").hide();
	    $("#create-propertydb-host-account").show();
	    $("#create-propertydb-host-account-action").show();
	});

	$("#import-modal-account-action-back, #create-propertydb-host-account-action-back").click(function () {
		$("#site-type").val("");
		$("#import-modal-title").text("Choose Site Where to Import");
		$("#import-modal-account").hide();
		$("#import-modal-account-action").hide();
		$("#import-modal-site-type").show();
		$("#import-modal").modal('refresh');
	});

	$("#create-propertydb-host-account-username, #create-propertydb-host-account-password," +
        "#create-propertydb-host-account-reEnterPassword").on("change", function () {
            $("#create-propertydb-host-account-error-message").hide();
        });

	$("#create-propertydb-host-account-action-submit").click(function () {
	    $(this).addClass("loading");
	    var username = $("#create-propertydb-host-account-username").val();
	    var password = $("#create-propertydb-host-account-password").val();
	    var reEnterPassword = $("#create-propertydb-host-account-reEnterPassword").val();
	    var errorMessage = "";

	    if (username == "") {
	        errorMessage = "Username is required";
	        $("#create-propertydb-host-account-error-message").show();
	    }
	    else if (password == "") {
	        errorMessage = "Password is required";
	        $("#create-propertydb-host-account-error-message").show();
	    }
	    else if (password != reEnterPassword) {
	        errorMessage = "Password did not match";
	        $("#create-propertydb-host-account-error-message").show();
	    }
	    else {
            // do ajax here
	    }

	    $("#create-propertydb-host-account-error").val(errorMessage);
	});

	$("#import-modal-account-action-next").click(function () {
		$("#import-modal-account-error-message").hide();
		$("#import-modal-account-action-next").addClass("loading");

		$.ajax({
			type: 'POST',
			url: "/Import/ImportLogIn",
			data: {
				username: $("#import-modal-account-username").val(),
				password: $("#import-modal-account-password").val(),
				siteType: siteTypeIndex
			},
			dataType: "json",
			success: function (result) {
				$("#import-modal-account-action-next").removeClass("loading");

				if (result.success) {
					$("#import-modal").modal("hide");
					download();
				}
				else if (!result.success && !result.isAirlock) {
					$("#import-modal-account-error-message").show();
				}
				else if (result.isAirlock) {

					if (siteType == "Airbnb") {
						var airlock = result.airlock;
						$("#airbnb-lockId").val(airlock.LockId);
						$("#airbnb-userId").val(airlock.UserId);
						//$("#airbnb-phoneId").val(airlock.PhoneId);
						$("#airbnb-isNew").val(airlock.IsNew);
						$("#airbnb-email").val(airlock.Email);
						//$("#airbnb-phoneNumber").val(airlock.PhoneNumber);
						$("#import-modal-account-username").val(airlock.Username);
						$("#import-modal-account-password").val(airlock.Password);

						var parent = $("#phone-content");
						parent.empty();
						var phoneIds = airlock.PhoneId.split(',');
						var phoneNumber = airlock.PhoneNumber.split(',');
						for (var q = 0; q < phoneIds.length; q++) {
							if (phoneIds[q] != "") {
								var html = '<div class="ui segment airbnb-airlock-choice" data-airlock-option="1" phoneId="' + phoneIds[q] + '" style="cursor: pointer">' +
									'<p id="airbnb-sms">' + '<span><i class="talk outline icon"></i></span>Text Message   (' + phoneNumber[q] + ')' + '</p>' +
									'</div>' +
									'<div class="ui segment airbnb-airlock-choice" data-airlock-option="2" phoneId="' + phoneIds[q] + '" style="cursor: pointer">' +
									'<p id="airbnb-call">' + '<span><i class="talk outline icon"></i></span>Phone Call   (' + phoneNumber[q] + ')' + '</p>' +
									'</div>';
								parent.append(html);
							}
						}
						if (airlock.Email != null && airlock.Email != "") {
							$("#email").show();
							$("#airbnb-mail").html("<span><i class='mail outline icon'></i></span>E-mail   (" + airlock.Email + ")");
						}
						else {
							$("#email").hide();
						}
					}
					else if (siteType == "Vrbo") {
						$("#import-modal-vrbo-airlock-phone-numbers").html("");

						vrboAirlockViewModel = result.airlock;
						var selectPhoneNumberHtml = "";

						for (var i = 0; i < result.phoneNumbers.length; i++) {
							selectPhoneNumberHtml = selectPhoneNumberHtml +
								'<div class="field">' +
								'<div class="ui radio checkbox">' +
								'<input type="radio" name="example2" checked="checked" selectionId="' + (result.phoneNumbers[i] + '-' + result.countryCodes[i])+ '">' +
								'<label>' + (result.phoneNumbers[i] + '-' + result.countryCodes[i])+ '</label>' +
								'</div>' +
								'</div>' +
								'<br />';
						}

						$("#import-modal-vrbo-airlock-phone-numbers").append(selectPhoneNumberHtml);
						$("#import-modal-vrbo-airlock-action").show();
					}
					else if (siteType == "Booking") {
						$("#import-modal-booking-airlock-phone-numbers").html("");

						bookingAirlockViewModel = result.airlock;
						var selectPhoneNumberHtml = "";

						for (var i = 0; i < result.phoneNumbers.length; i++) {
							selectPhoneNumberHtml = selectPhoneNumberHtml +
								'<div class="field">' +
								'<div class="ui radio checkbox">' +
								'<input type="radio" name="example2" checked="checked" selectionId="' + result.phoneNumberKeys[i] + '">' +
								'<label>' + result.phoneNumbers[i] + '</label>' +
								'</div>' +
								'</div>' +
								'<br />';
						}


						$("#import-modal-booking-airlock-phone-numbers").append(selectPhoneNumberHtml);
						$("#import-modal-booking-airlock-action").show();
					}
					else if (siteType == "Homeaway") {
						$("#import-modal-homeaway-airlock-phone-numbers").html("");
						console.log(result);
					    vrboAirlockViewModel = result.airlock;
					    var selectPhoneNumberHtml = "";

					    for (var i = 0; i < result.phoneNumbers.length; i++) {
					        selectPhoneNumberHtml = selectPhoneNumberHtml +
								'<div class="field">' +
								'<div class="ui radio checkbox">' +
								'<input type="radio" name="example2" checked="checked" selectionId="' + (result.phoneNumbers[i] + '-' + result.countryCodes[i]) + '">' +
								'<label>' + (result.phoneNumbers[i] + '-' + result.countryCodes[i]) + '</label>' +
								'</div>' +
								'</div>' +
								'<br />';
					    }

					    $("#import-modal-homeaway-airlock-phone-numbers").append(selectPhoneNumberHtml);
					    $("#import-modal-homeaway-airlock-action").show();
					}

					$("#import-modal").modal('hide');
					$("#import-modal").modal('show');
					$("#import-modal-account").hide();
					$("#import-modal-account-action").hide();
					$("#import-modal-title").text($("#site-type").val() + " Airlock");
					$("#import-modal-" + siteType.toLowerCase() + "-airlock").show();
					$("#import-modal").modal('refresh');
				}
			}
		});
	});

	function onAddGmailAccount() {
		$(document).on('click', '.js-btn_add_gmail', function () {

			var id = $(this).parents('tr').attr('data-account-id');
			$.ajax({
				type: "GET",
				url: "/Import/GetAccountDetails",
				data: { id: id },
				beforeSend: function () {

				}, success: function (result) {
					$('#gmail-username').val(result.host.Username);
					$('#gmail-username').attr("host-id", result.host.Id)
					$('#gmail-modal').modal('show');
				}
			});
		});
	}
	function SubmitGmailAccount() {
		$(document).on('click', '#SaveGmail', function () {
			var email = $('#gmail-username').val();
			var password = $('#gmail-password').val()
			$.ajax({
				type: "POST",
				url: "/Import/SaveGmailAccount",
				data: { email: email, password: password },
				beforeSend: function () {
				}, success: function (result) {
					swal({ title: "Success", text: "Gmail updated", type: "success" });
					$('#gmail-modal').modal('hide');
				}
			});
		});
	}
	function onStartReconnect() {
		$(document).on('click', '.js-btn_reconnect', function () {

			var id = $(this).parents('tr').attr('data-account-id');
			var _siteType = $(this).parents('tr').attr('data-siteType');
			isReconnect = true;
			changeSiteTypeIndex(_siteType);
			var icon = $(this).find('i');
			$.ajax({
				type: "POST",
				url: "/Import/Reconnect",
				data: { id: id, siteType: _siteType },
				beforeSend: function () {
					icon.removeClass('icon sign-in').addClass('spinner loading icon');
				},
				success: function (result) {
					console.log(result);
					icon.removeClass('spinner loading icon').addClass('icon refresh');

					if (result.success) {
						$("#import-modal").modal("hide");
						swal({ title: "Success", text: "Reconnecting Complete!", type: "success" });
						tableAccounts.ajax.reload(null, false);
					}
					else if (!result.success && !result.isAirlock) {
						$("#import-modal-account-error-message").show();
						$('#msg').append('<div class="ui negative message">' +
							'<i class="close icon"></i>' +
							'<div class="header">' +
							'An error occured' +
							'</div>' +
							'</div>');
					}
					else if (result.isAirlock) {

						if (siteType == "Airbnb") {
							var airlock = result.airlock;
							if (result.hostProfilePic != null) {
								$("#airbnb-host-pic").attr("src", result.hostProfilePic);
							}

							$("#airbnb-host-name").text(result.hostName);
							$("#airbnb-lockId").val(airlock.LockId);
							$("#airbnb-userId").val(airlock.UserId);
							$("#airbnb-phoneId").val(airlock.PhoneId);
							$("#airbnb-isNew").val(airlock.IsNew);
							$("#airbnb-email").val(airlock.Email);
							$("#airbnb-phoneNumber").val(airlock.PhoneNumber);
							$("#import-modal-account-username").val(airlock.Username);
							$("#import-modal-account-password").val(airlock.Password);

							$("#airbnb-sms").html("<span><i class='talk outline icon'></i></span>Text Message   (" + airlock.PhoneNumber + ")");
							$("#airbnb-call").html("<span><i class='call outline icon'></i></span>Phone Call   (" + airlock.PhoneNumber + ")");
							if (airlock.Email != null && airlock.Email != "") {
								$("#email").show();
								$("#airbnb-mail").html("<span><i class='mail outline icon'></i></span>E-mail   (" + airlock.Email + ")");
							}
							else {
								$("#email").hide();
							}
						}
						else if (siteType == "Vrbo") {
							$("#import-modal-vrbo-airlock-phone-numbers").html("");

							vrboAirlockViewModel = result.airlock;
							var selectPhoneNumberHtml = "";

							for (var i = 0; i < result.phoneNumbers.length; i++) {
								selectPhoneNumberHtml = selectPhoneNumberHtml +
									'<div class="field">' +
									'<div class="ui radio checkbox">' +
									'<input type="radio" name="example2" checked="checked" selectionId="' + (i + 1) + '">' +
									'<label>' + result.phoneNumbers[i] + '</label>' +
									'</div>' +
									'</div>' +
									'<br />';
							}

							$("#import-modal-vrbo-airlock-phone-numbers").append(selectPhoneNumberHtml);
							$("#import-modal-vrbo-airlock-action").show();
						}
						else if (siteType == "Booking") {
							$("#import-modal-booking-airlock-phone-numbers").html("");

							bookingAirlockViewModel = result.airlock;
							var selectPhoneNumberHtml = "";

							for (var i = 0; i < result.phoneNumbers.length; i++) {
								selectPhoneNumberHtml = selectPhoneNumberHtml +
									'<div class="field">' +
									'<div class="ui radio checkbox">' +
									'<input type="radio" name="example2" checked="checked" selectionId="' + result.phoneNumberKeys[i] + '">' +
									'<label>' + result.phoneNumbers[i] + '</label>' +
									'</div>' +
									'</div>' +
									'<br />';
							}

							$("#import-modal-booking-airlock-phone-numbers").append(selectPhoneNumberHtml);
							$("#import-modal-booking-airlock-action").show();
						}

						$("#import-modal-site-type").hide();
						$("#import-modal-account").hide();
						$("#import-modal-account-action").hide();
						$("#import-modal-title").text($("#site-type").val() + " Airlock");
						$("#import-modal-" + siteType.toLowerCase() + "-airlock").show();
						$("#import-modal").modal('show');
					}
				}
			});
		});
	}
	function onStartRedownload() {
		$(document).on('click', '.js-btn_redownload', function () {
			
			var id = $(this).parents('tr').attr('data-account-id');
			var _siteType = $(this).parents('tr').attr('data-siteType');
			changeSiteTypeIndex(_siteType);
			var icon = $(this).find('i');
			$.ajax({
				type: "POST",
				url: "/Import/RedownloadData",
				data: { id: id, siteType: _siteType },
				beforeSend: function () {
					icon.removeClass('icon download').addClass('spinner loading icon');
				},
				success: function (result) {
					console.log(result);
					icon.removeClass('spinner loading icon').addClass('icon download');

					if (result.success) {
						$("#import-modal").modal("hide");
						tableAccounts.ajax.reload(null, false);
					}
					else if (!result.success && !result.isAirlock) {
						$("#import-modal-account-error-message").show();
						$('#msg').append('<div class="ui negative message">' +
							'<i class="close icon"></i>' +
							'<div class="header">' +
							'An error occured' +
							'</div>' +
							'</div>');
					}
					else if (result.isAirlock) {
					
						if (siteType == "Airbnb") {
							var airlock = result.airlock;
							if (result.hostProfilePic != null) {
								$("#airbnb-host-pic").attr("src", result.hostProfilePic);
							}

							$("#airbnb-host-name").text(result.hostName);
							$("#airbnb-lockId").val(airlock.LockId);
							$("#airbnb-userId").val(airlock.UserId);
							$("#airbnb-phoneId").val(airlock.PhoneId);
							$("#airbnb-isNew").val(airlock.IsNew);
							$("#airbnb-email").val(airlock.Email);
							$("#airbnb-phoneNumber").val(airlock.PhoneNumber);
							$("#import-modal-account-username").val(airlock.Username);
							$("#import-modal-account-password").val(airlock.Password);

							var parent = $("#phone-content");
							parent.empty();
							var phoneIds = airlock.PhoneId.split(',');
							var phoneNumber = airlock.PhoneNumber.split(',');
							for (var q = 0; q < phoneIds.length; q++) {
								if (phoneIds[q] != "") {
									var html = '<div class="ui segment airbnb-airlock-choice" data-airlock-option="1" phoneId="' + phoneIds[q] + '" style="cursor: pointer">' +
										'<p id="airbnb-sms">' + '<span><i class="talk outline icon"></i></span>Text Message   (' + phoneNumber[q] + ')' + '</p>' +
										'</div>' +
										'<div class="ui segment airbnb-airlock-choice" data-airlock-option="2" phoneId="' + phoneIds[q] + '" style="cursor: pointer">' +
										'<p id="airbnb-call">' + '<span><i class="talk outline icon"></i></span>Phone Call   (' + phoneNumber[q] + ')' + '</p>' +
										'</div>';
									parent.append(html);
								}
							}
							if (airlock.Email != null && airlock.Email != "") {
								$("#email").show();
								$("#airbnb-mail").html("<span><i class='mail outline icon'></i></span>E-mail   (" + airlock.Email + ")");
							}
							else {
								$("#email").hide();
							}
						}
						else if (siteType == "Vrbo") {
							$("#import-modal-vrbo-airlock-phone-numbers").html("");

							vrboAirlockViewModel = result.airlock;
							console.log(vrboAirlockViewModel)
							var selectPhoneNumberHtml = "";

							for (var i = 0; i < result.phoneNumbers.length; i++) {
								selectPhoneNumberHtml = selectPhoneNumberHtml +
									'<div class="field">' +
									'<div class="ui radio checkbox">' +
									'<input type="radio" name="example2" checked="checked" selectionId="' + (result.phoneNumbers[i] + '-' + result.countryCodes[i]) + '">' +
									'<label>' + (result.phoneNumbers[i] + '-' + result.countryCodes[i])+ '</label>' +
									'</div>' +
									'</div>' +
									'<br />';
							}

							$("#import-modal-vrbo-airlock-phone-numbers").append(selectPhoneNumberHtml);
							$("#import-modal-vrbo-airlock-action").show();
						}
						else if (siteType == "Booking") {
							$("#import-modal-booking-airlock-phone-numbers").html("");

							bookingAirlockViewModel = result.airlock;
							var selectPhoneNumberHtml = "";

							for (var i = 0; i < result.phoneNumbers.length; i++) {
								selectPhoneNumberHtml = selectPhoneNumberHtml +
									'<div class="field">' +
									'<div class="ui radio checkbox">' +
									'<input type="radio" name="example2" checked="checked" selectionId="' + result.phoneNumberKeys[i] + '">' +
									'<label>' + result.phoneNumbers[i] + '</label>' +
									'</div>' +
									'</div>' +
									'<br />';
							}

							$("#import-modal-booking-airlock-phone-numbers").append(selectPhoneNumberHtml);
							$("#import-modal-booking-airlock-action").show();
						}
						else if (siteType == "Homeaway") {
							$("#import-modal-homeaway-airlock-phone-numbers").html("");
							console.log(result);
							vrboAirlockViewModel = result.airlock;
							var selectPhoneNumberHtml = "";

							for (var i = 0; i < result.phoneNumbers.length; i++) {
								selectPhoneNumberHtml = selectPhoneNumberHtml +
									'<div class="field">' +
									'<div class="ui radio checkbox">' +
									'<input type="radio" name="example2" checked="checked" selectionId="' + (result.phoneNumbers[i] + '-' + result.countryCodes[i]) + '">' +
									'<label>' + (result.phoneNumbers[i] + '-' + result.countryCodes[i]) + '</label>' +
									'</div>' +
									'</div>' +
									'<br />';
							}

							$("#import-modal-homeaway-airlock-phone-numbers").append(selectPhoneNumberHtml);
							$("#import-modal-homeaway-airlock-action").show();
						}
						$("#import-modal-site-type").hide();
						$("#import-modal-account").hide();
						$("#import-modal-account-action").hide();
						$("#import-modal-title").text($("#site-type").val() + " Airlock");
						$("#import-modal-" + siteType.toLowerCase() + "-airlock").show();
						$("#import-modal").modal('show');
					}
				}
			});
		});
	}

	$("#import-modal-account-username, #import-modal-account-password").on('input', function () {
		$("#import-modal-account-error-message").hide();
	});

	//$("#import-modal-vrbo-airlock-verification-code-sms, #import-modal-vrbo-airlock-verification-code-call").click(function () {
	//	var clickedButton = $(this);
	//	clickedButton.addClass("loading");
	//	var selectedChoice = clickedButton.attr("selectedChoice");
	//	var selectedPhoneNumber = $("input[type=radio]:checked").attr("selectionId");

	//	vrboAirlockViewModel.SelectedChoice = selectedChoice;
	//	vrboAirlockViewModel.SelectedPhoneNumber = selectedPhoneNumber;

	//	$.ajax({
	//		type: 'POST',
	//		url: "/Import/ProcessVrboAirlockSelection",
	//		data: vrboAirlockViewModel,
	//		success: function (result) {
	//			vrboAirlockViewModel = result.airlock;
	//			clickedButton.removeClass("loading");

	//			$("#import-modal").modal({ closable: false });
	//			$("#import-modal-vrbo-airlock").hide();
	//			$("#import-modal-vrbo-airlock-action").hide();
	//			$("#import-modal-vrbo-airlock-verification-code").show();
	//			$("#import-modal-vrbo-airlock-verification-code-action").show();
	//		}
	//	});
	//});

	//$("#import-modal-booking-airlock-verification-code-sms, #import-modal-booking-airlock-verification-code-call").click(function () {
	//	var clickedButton = $(this);
	//	clickedButton.addClass("loading");
	//	var selectedChoice = clickedButton.attr("selectedChoice");
	//	var selectedPhoneNumber = $("input[type=radio]:checked").attr("selectionId");
	//	debugger;
	//	bookingAirlockViewModel.SelectedChoice = selectedChoice;
	//	bookingAirlockViewModel.SelectedPhoneNumber = selectedPhoneNumber;

	//	$.ajax({
	//		type: 'POST',
	//		url: "/Import/ProcessBookingAirlockSelection",
	//		data: bookingAirlockViewModel,
	//		success: function (result) {
	//			bookingAirlockViewModel = result.airlock;
	//			clickedButton.removeClass("loading");
	//			$("#import-modal").modal({ closable: false });
	//			$("#import-modal-booking-airlock").hide();
	//			$("#import-modal-booking-airlock-action").hide();
	//			$("#import-modal-vrbo-airlock-verification-code").show();
	//			$("#import-modal-vrbo-airlock-verification-code-action").show();
	//		}
	//	});
	//});

	//$("#import-modal-homeaway-airlock-verification-code-sms, #import-modal-homeaway-airlock-verification-code-call").click(function () {
	//    var clickedButton = $(this);
	//    clickedButton.addClass("loading");
	//    var selectedChoice = clickedButton.attr("selectedChoice");
	//    var selectedPhoneNumber = $("input[type=radio]:checked").attr("selectionId");

	//    vrboAirlockViewModel.SelectedChoice = selectedChoice;
	//    vrboAirlockViewModel.SelectedPhoneNumber = selectedPhoneNumber;

	//    $.ajax({
	//        type: 'POST',
	//        url: "/Import/ProcessHomeawayAirlockSelection",
	//        data: vrboAirlockViewModel,
	//        success: function (result) {
	//            vrboAirlockViewModel = result.airlock;
	//            clickedButton.removeClass("loading");

	//            $("#import-modal").modal({ closable: false });
	//            $("#import-modal-homeaway-airlock").hide();
	//            $("#import-modal-homeaway-airlock-action").hide();
	//            $("#import-modal-vrbo-airlock-verification-code").show();
	//            $("#import-modal-vrbo-airlock-verification-code-action").show();
	//        }
	//    });
	//});

	//$(document).on('click', '.airbnb-airlock-choice', function () {
	//	var thisObj = $(this);
	//	$("#airbnb-choice").val($(this).attr("data-airlock-option"));
	//	var phoneId = $(this).attr("phoneId");
	//	var choice = $("#airbnb-choice").val();
	//	var lockId = $("#airbnb-lockId").val();
	//	var userId = $("#airbnb-userId").val();
	//	$("#airbnb-phoneId").val(phoneId);
	//	var isNew = $("#airbnb-isNew").val();
	//	var username = $("#import-modal-account-username").val();
	//	thisObj.removeClass("loading");
	//	$('.airbnb-airlock-choice').removeAttr("disabled");
	//	$.ajax({
	//		type: 'POST',
	//		url: "/Import/ProcessAirbnbAirlockSelection",
	//		data: {
	//			choice: choice,
	//			lockId: lockId,
	//			userId: userId,
	//			phoneId: phoneId,
	//			isNew: isNew,
	//			username: username
	//		},
	//		success: function (sendCodeAllowed) {
	//			$('.airbnb-airlock-choice').removeClass("loading");
	//			$('.airbnb-airlock-choice').removeAttr("disabled");
	//			if (sendCodeAllowed) {
	//				$("#import-modal").modal({ closable: false });
	//				$("#import-modal-airbnb-airlock").hide();
	//				$("#import-modal-vrbo-airlock-verification-code").show();
	//				$("#import-modal-vrbo-airlock-verification-code-action").show();

	//			}
	//			else {
	//				$("#submit-airlock-choice-error").show();
	//			}
	//		}
	//	});
	//});
	//JM 07/30/19 This is for entering verification code
	$("#import-modal-vrbo-airlock-verification-code-done").click(function () {
		$("#import-modal-vrbo-airlock-verification-code-done").addClass("loading");
		$("#import-modal-vrbo-airlock-verification-code-error-message").hide();
		var choice;
		var lockId;
		var userId;
		var phoneId;
		var isNew;
		var code;
		var dataParameter;

		if (siteType == "Airbnb") {
			choice = $("#airbnb-choice").val();
			lockId = $("#airbnb-lockId").val();
			userId = $("#airbnb-userId").val();
			phoneId = $("#airbnb-phoneId").val();
			isNew = $("#airbnb-isNew").val();
			code = $("#import-modal-vrbo-airlock-verification-code-tb").val();

			var dataParameter = {
				"choice": choice,
				"lockId": lockId,
				"userId": userId,
				"phoneId": phoneId,
				"isNew": isNew,
				"code": code,
				"username": $("#import-modal-account-username").val(),
				"password": $("#import-modal-account-password").val()
			};
		}
		else if (siteType == "Vrbo") {
			vrboAirlockViewModel.Code = $("#import-modal-vrbo-airlock-verification-code-tb").val();

			if (vrboAirlockViewModel.Username == null && vrboAirlockViewModel.Password == null) {
				vrboAirlockViewModel.Username = $("#import-modal-account-username").val();
				vrboAirlockViewModel.Password = $("#import-modal-account-password").val();
			}

			dataParameter = vrboAirlockViewModel;
		}
		else if (siteType == "Booking") {
			bookingAirlockViewModel.Code = $("#import-modal-vrbo-airlock-verification-code-tb").val();

			//if (bookingAirlockViewModel.Email == null && bookingAirlockViewModel.Password == null) {
			//	//bookingAirlockViewModel.Email = $("#import-modal-account-username").val();
			//	//bookingAirlockViewModel.Password = $("#import-modal-account-password").val();
			//}

			
			dataParameter = bookingAirlockViewModel;
		}
		else if (siteType == "Homeaway") {
		    vrboAirlockViewModel.Code = $("#import-modal-vrbo-airlock-verification-code-tb").val();

		    if (vrboAirlockViewModel.Username == null && vrboAirlockViewModel.Password == null) {
		        vrboAirlockViewModel.Username = $("#import-modal-account-username").val();
		        vrboAirlockViewModel.Password = $("#import-modal-account-password").val();
		    }

		    dataParameter = vrboAirlockViewModel;
		}
		$.ajax({
			type: 'POST',
			url: "/Import/Process" + siteType +"Airlock",
			data: dataParameter,
			success: function (result) {
				$("#import-modal-vrbo-airlock-verification-code-done").removeClass("loading");
				if (result.success) {
					tableAccounts.ajax.reload(null, false);
					$("#import-modal-vrbo-airlock-verification-code").hide();
					$("#import-modal-vrbo-airlock-verification-code-action").hide();
					$("#import-modal-account-username").val(result.username);
					$("#import-modal-account-password").val(result.password);
					$("#site-type").val("");
					$("#import-modal-title").text("Choose Site Where to Import");
					$("#import-modal-account").hide();
					$("#import-modal-account-action").hide();
					$("#import-modal-site-type").show();
					if (!isReconnect) {
						download();
					}
					else {
						swal({ title: "Success", text: "Reconnecting Complete!", type: "success" });
					}
					isReconnect = false;
				}
				else {
					if (siteType == "Airbnb") {
						if (result.message == "The code you provided is incorrect. Please try again.") {
							$("#import-modal-vrbo-airlock-verification-code-error-message").show();
						}
						else {
							$("#import-modal").modal('hide');
							swal({ title: "Error", text: "Process Timeout,Please Try again", type: "error" });
						}
					} else {
						$("#import-modal-vrbo-airlock-verification-code-error-message").show();
					}
				}
			}
		});
	});

	$("#import-modal-vrbo-airlock-verification-code-tb").on('input', function () {
		$("#import-modal-vrbo-airlock-verification-code-error-message").hide();
	});

});
