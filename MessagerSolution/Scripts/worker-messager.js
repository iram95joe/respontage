﻿var myHub = $.connection.inboxHub;
$.connection.hub.logging = true;
myHub.client.WorkerMessageReceived = function (inbox, isworker) {
	var threadId = $('.worker-active-thread').attr('data-thread-id');
	var thread = inbox.Thread;
	var date1 = moment(thread.LastMessageAt);
	var date2 = moment();
	differenceInMs = date2.diff(date1);
	duration = moment.duration(differenceInMs);
	differenceInMinutes = duration.asMinutes();
	var html = '<div class="item worker-message-thread" data-thread-id="' + thread.ThreadId + '" data-id="' + thread.WorkerId + '" is-worker="' + isworker + '">' +
		'<div class="avatar">' +
		'<img src="' + thread.ImageUrl + '" height="40" width="40">' +
		'</div>' +
		'<div class="content">' +
		'<div class="author"><strong>' + thread.Name + '</strong></div>' +
		'<br>' +
		'<div class="metadata">' +
		'<div class="date">' + (thread.LastMessageAt != null ? (moment(thread.LastMessageAt).isSame(Date.now(), 'day') ? moment(thread.LastMessageAt).format("h:mm A") : moment(thread.LastMessageAt).format("MMM D, YYYY")) : '') + '</div>' +
		'</div>' +
		'<div class="text">' + (thread.LastMessage != null ? (thread.LastMessage.length > 15 ? (thread.LastMessage.substr(0, 14) + '...') : thread.LastMessage) : "Photo") + '</div>' +
		'</div>' +
		'</div>';
	$('.worker-message-thread[data-thread-id="' + thread.ThreadId + '"]').remove();
	$('#worker-inbox-list').prepend(html);

	if (thread.ThreadId == $('#worker-inbox-list').attr('data-active-thread-id')) {
		var inboxItem = $('.worker-message-thread[data-thread-id="' + $('#worker-inbox-list').attr('data-active-thread-id') + '"]');
		inboxItem.addClass('worker-active-thread');
		inboxItem.css('border-left', '3px solid #00b5ad');
		inboxItem.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
	}
	if (threadId == thread.ThreadId) {

		loadWorkerConversation(inbox.Messages);

	}
};


onButtonSendSMSClick();
function onButtonSendSMSClick() {
	$(document).on('click', '#worker-send-sms', function () {
		$('#worker-send-sms').attr('disabled', true);
		var contact = $('#worker-contact-info').find('.checked');
		let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
		let type = $(contact).parents(".field").attr('data-type');
		var workerId = $('.worker-active-thread').attr('data-id');
		var threadId = $('.worker-active-thread').attr('data-thread-id');
		var isWorker = $('.worker-active-thread').attr('is-worker');
		var formData = new FormData();
		formData.append("workerId", workerId);
		for (var i = 0; i < $('#mms-pic').get(0).files.length; ++i) {
			formData.append("Images[" + i + "]", $('#mms-pic').get(0).files[i]);
		}
		formData.append("to", phoneNumber);
		formData.append("message", $('#txtMsgContentWorker').val());
		formData.append("type", type);
		formData.append("workerId", workerId);
		formData.append("threadId", threadId);
		formData.append("isworker", isWorker);

		$.ajax({
			type: 'POST',
			url: "/SMS/WorkerSend",
			contentType: false,
			cache: false,
			dataType: "json",
			processData: false,
			data: formData,
			success: function (data) {
				if (data.success) {
					$('#worker-send-sms').attr('disabled', false);
					$('#txtMsgContentWorker').val('');
					$('.worker-active-thread').attr('data-thread-id', data.threadId);
					$('#worker-inbox-list').attr('data-active-thread-id', data.threadId);
					$('#mms-image-upload').empty();
					$('#mms-pic').val('');
					toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					//$('.item.worker-message-thread.worker-active-thread').click();
				} else {
					toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			},
			error: function (error) {
				toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
			}
		});

	});
}
function loadWorkerConversation(messages) {
	$("#worker-dv_Conversation").html("");
	$.each(messages, function (index, value) {
		var imageHtml = '<div class="ui tiny images">';
		if (value.Images != null && value.Images!='') {
			for (var x = 0; x < value.Images.length; x++) {
				imageHtml += '<img class="zoom" src="' + value.Images[x] + '">';
			}
		}
		imageHtml += '</div>';
		if (!value.IsMyMessage) {
			if (value.Type == 2) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' +

					(
						value.RecordingUrl == null ?
							'' :
							(
								'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
							)
					) +
					'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
					+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 1) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui  small left pointing label message-label message-incoming">' +
					'<h5>' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
					(value.Images != null ? imageHtml : '') +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 3) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
					'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 4) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
					'<h5>' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
					'</div>');
			} else {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
					'<h5>' +
					'<div class="ui mini circular image">' +
					'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
					'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
			}
		}
		else {
			if (value.Type == 2) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
					(
						value.RecordingUrl == null ?
							'' :
							(
								'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
							)
					) +
					'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
					+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 1) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui  small right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
					(value.Images != null ? imageHtml : '') +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 3) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
					'</div>');
			} else if (value.Type == 4) {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
					'</div>');
			} else {
				$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
					'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
					'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
					'</div>');
			}
		}

	});
	$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
}

$.connection.hub.start();

//JM 07/30/19 Worker Messages Event
function WorkerInbox() {
	loadWorkerInbox();
	onWorkerInboxClick();
	onWorkerSendMessageClick();
}

function onWorkerSendMessageClick() {
	$(document).on('click', '#worker-send-btn', function () {

		var threadId = $('.worker-active-thread').attr('data-thread-id');
		var workerId = $('.worker-active-thread').attr('data-id');
		var isWorker = $('.worker-active-thread').attr('is-worker');
		var msg = $('#txtMsgContentWorker').val();
		if (msg != '') {
			$.ajax({
				type: 'POST',
				url: "/Messages/WorkerSendMessage",
				data: { workerId: workerId, threadId: threadId, messege: msg, isworker: isWorker },
				dataType: "json",
				beforeSend: function () {
					$('#worker-send-btn').addClass('loading');
				},
				success: function (data) {

					if (data.success) {
						$('#txtMsgContentWorker').css('border-color', 'rgba(34,36,38,.15)');
						$('#txtMsgContentWorker').val('');
						$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
						$('#worker-send-btn').removeClass('orange').addClass('teal');
						$('#worker-send-btn').text('Send');
						$('.worker-active-thread').find('.content').find('.text').text(msg);
						$('.worker-active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
					}
					else {
						$('#txtMsgContentWorker').css('border-color', 'orange');
						$('#worker-send-btn').removeClass('teal').addClass('orange');
						$('#worker-send-btn').text('Retry');
					}
					$('#worker-send-btn').removeClass('loading');
					$('.worker-active-thread').attr('data-thread-id', data.threadId);
					$('#worker-inbox-list').attr('data-active-thread-id', data.threadId);
				}
			});
		}

	});
}
function onWorkerInboxClick() {
	//get the message and details of inquiry
	$(document).on('click', '.worker-message-thread', function () {
		var messageThread = $(this);
		//set design to all thread

		$('#worker-inbox-list').children().css('color', 'black', '!important');
		$('#worker-inbox-list').children().css('padding', '10px');
		$('#worker-inbox-list').children().css('background', 'white');
		$('#worker-inbox-list').children().css('border-radius', '10px');
		//load message on mini inbox
		var threadId = messageThread.attr('data-thread-id')
		$('#inbox-modal').attr('threadId', threadId);
		GetInboxMessage(threadId);

		//set message for browser
		//Set design to selected thread
		messageThread.css('color', 'white', '!important');
		messageThread.css('padding', '10px');
		messageThread.css('background', '#277296');
		messageThread.css('border-radius', '10px');
		var id = messageThread.attr('data-thread-id');
		var workerId = messageThread.attr('data-id');
		var isworker = messageThread.attr('is-worker');
		$('#worker-inbox-list').attr('data-active-thread-id', id);

		$("#worker-dv_Conversation").html('');

		$("#worker-img_Loader").show();
		$.ajax({
			type: 'POST',
			url: "/Messages/LoadWorkerInboxMessage",
			data: { threadId: id, workerId: workerId, isworker: isworker },
			dataType: "json",
			async: true,
			success: function (data) {

				$("#worker-img_Loader").hide();
				if (data.success) {
					//display the conversation

					if (data.Messages.length != 0) {
						//$('#worker-dvConvo').html('');
						$.each(data.Messages, function (index, value) {

							var imageHtml = ''
							if (value.Images != null && value.Images != '') {
								var imageHtml = '<div class="ui tiny images">';
								for (var x = 0; x < value.Images.length; x++) {
									imageHtml += '<img class="zoom" src="' + value.Images[x] + '">';
								}
								imageHtml += '</div>';
							}
							if (!value.IsMyMessage) {
								if (value.Type == 2) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
										'<div class="ui mini circular image">' +
										'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
										'</div>' +

										(
											value.RecordingUrl == null ?
												'' : '<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'

										) +
										'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
										+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
										'</div>');
								} else if (value.Type == 1) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui  small left pointing label message-label message-incoming">' +
										'<h5>' +
										'<div class="ui mini circular image">' +
										'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
										'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
										(value.Images != null ? imageHtml : '') +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
										'</div>');
								} else if (value.Type == 3) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
										'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
										'</div>');
								} else if (value.Type == 4) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
										'<h5>' +
										'<div class="ui mini circular image">' +
										'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
										'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
										'</div>');
								} else {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
										'<h5>' +
										'<div class="ui mini circular image">' +
										'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
										'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
										'</div>');
								}
							}
							else {
								if (value.Type == 2) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
										(
											value.RecordingUrl == null ?
												'' :
												(
													'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
												)
										) +
										'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
										+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
										'</div>');
								} else if (value.Type == 1) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui  small right pointing label message-label message-outgoing">' +
										'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
										(value.Images != null ? imageHtml : '') +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
										'</div>');
								} else if (value.Type == 3) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
										'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
										'</div>');
								} else if (value.Type == 4) {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
										'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
										'</div>');
								} else {
									$("#worker-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
										'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
										'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
										'</div>');
								}
							}
						});
					}
					else {
						var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
						$('#worker-dv_Conversation').append(html);
					}
					$('#worker-contact-info').empty();
					if (data.isWorker) {
						var hasDefault = data.hasDefaultWorkerContact;
						$.each(data.contactInfos, function (index, contactInfo) {
							let buttons =
								`<button class="circular tiny ui icon button worker-call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
								<button class="circular tiny ui icon button worker-sms-button" data-tooltip="SMS">
									<i class="icon comments"></i>
								</button>`;

							if (contactInfo.Type == 2) {
								buttons =
									`<button class="circular ui teal icon button worker-sms-button" data-tooltip="WhatsApp">
							                                 <i class="icon whatsapp"></i>
							                             </button>`;
							} else if (contactInfo.Type == 3) {
								buttons =
									`<button class="circular ui blue icon button worker-sms-button" data-tooltip="Messenger">
							                                 <i class="icon facebook messenger"></i>
							                             </button>`;
							}
							var isDefault = "";
							if (hasDefault == false && index == 0) {
								isDefault = "checked";
							}
							else {
								isDefault = contactInfo.IsDefault ? "checked" : "";
							}
							$('#worker-contact-info').append(`
								<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}" >
									<div class="ui radio checkbox ${isDefault}">
									<input type="radio" name="contact" ${isDefault}>
									<label>${contactInfo.Value}</label>
									</div>
								 ${buttons}
								</div>
							`);
						});
					} else {
						var hasDefault = data.hasDefaultVendorContact;
						$.each(data.vendorContactInfos, function (index, contactInfo) {
							let buttons =
								`<button class="circular ui green icon button worker-call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
                                <button class="circular ui orange icon button worker-sms-button" data-tooltip="SMS">
									<i class="icon comments"></i>
								</button>`;
							var isDefault = "";
							if (hasDefault == false && index == 0) {
								isDefault = "checked";
							}
							else {
								isDefault = contactInfo.IsDefault ? "checked" : "";
							}
							$('#worker-contact-info').append(`
								<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="1">
									<div class="ui radio checkbox ${isDefault}">
									<input type="radio" name="contact" ${isDefault}>
									<label>${contactInfo.Value}</label>
                                    </div>
									${buttons}
								</div>
							`);
						});
					}

				}
				$('.ui.radio.checkbox').checkbox();
				$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
			}
		});

		$('.worker-message-thread').removeClass('worker-active-thread');
		$(this).addClass('worker-active-thread');
		document.getElementById('worker-dvConvo').scrollIntoView()
		if ($(window).width() < 700) {
			$('.wrapper').removeAttr('hidden');
			
		}
		else {
			$('.wrapper').attr('hidden', true);
			
		}
	});
}
function loadWorkerInbox() {
	$.ajax({
		type: 'POST',
		url: "/Messages/LoadWorkerInbox",
		//data: { skip: skipNum, take: 10 },
		success: function (data) {

			if (data.result.length > 0) {
				$.each(data.result, function (k, v) {

					var clock = '';
					var date1 = moment(v.LastMessageAt);
					var date2 = moment();
					var differenceInMs = date2.diff(date1); // diff yields milliseconds
					var duration = moment.duration(differenceInMs); // moment.duration accepts ms
					var differenceInMinutes = duration.asMinutes();
					if (differenceInMinutes != 0) {
						if (differenceInMinutes <= 30 && v.IsIncoming) {
							clock = '<i class="wait icon green right floated"></i>';
						}
						else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
							clock = '<i class="wait icon yellow right floated"></i>';
						}
						else if (differenceInMinutes > 60 && v.IsIncoming) {
							clock = '<i class="wait icon red right floated"></i>';
						}
					}
					var html = '<div class="item worker-message-thread" data-thread-id="' + v.ThreadId + '" data-id="' + (v.WorkerId != null ? v.WorkerId : v.VendorId) + '" is-worker="' + (v.WorkerId != null ? true : false) + '">' +
						'<div class="avatar">' +
						'<img src="' + v.ImageUrl + '" height="40" width="40">' +
						'</div>' +
						'<div class="content">' +
						'<div class="author"><strong>' + v.Name + '</strong></div>' +
						clock +
						'<br>' +
						'<div class="metadata">' +
						(v.LastMessageAt != null ? '<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' : '') +
						'</div>' +
						'<div class="text">' + (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage) + '</div>' +
						'</div>' +
						'</div>';
					$('#worker-inbox-list').append(html);

				});
				$('#worker-inbox-list').attr('data-active-thread-id', (data.result.length > 0 ? data.result[0].ThreadId : "0"));
				$("#worker-inbox-list").find('.item[data-thread-id="' + $('#worker-inbox-list').attr('data-active-thread-id') + '"]').first().trigger("click");
			}
			else {
				var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
				$('#worker-dvConvo').append(html);
			}
		}
	});
}
$(document).on('click', '#worker-end-btn', function () {
	var threadId = $('.worker-active-thread').attr('data-thread-id');
	$.ajax({
		type: 'POST',
		url: "/Messages/EndCommunicationThread",
		data: {
			threadId: threadId
		},
		success: function (data) {
			swal("This thread mark as end!", "", "success");
			$('.worker-active-thread').children('.content').children('i').remove();
		}
	});
});
//Worker Call/sms start
$(document).on('click', '.button.worker-sms-button', function () {
	let phoneNumber = $(this).parents(".field").attr('data-phonenumber');
	let type = $(this).parents(".field").attr('data-type');
	var workerId = $('.worker-active-thread').attr('data-id');
	var threadId = $('.worker-active-thread').attr('data-thread-id');
	var isWorker = $('.worker-active-thread').attr('is-worker');

	$('.send-sms-modal [name="phone-number"]').val(phoneNumber);
	$('.send-sms-modal [name="phone-number"]').attr('disabled', true);
	$('.send-sms-modal [name="message"]').val('');

	$('.send-sms-modal.modal')
		.modal({
			closable: true,
			onApprove: function () {
				$.ajax({
					type: 'POST',
					url: "/SMS/WorkerSend",
					data: {
						to: phoneNumber,
						message: $('.send-sms-modal [name="message"]').val(),
						type: type,
						workerId: workerId,
						threadId: threadId,
						isworker: isWorker
					},
					dataType: "json",
					success: function (data) {
						if (data.success) {

							$('.worker-active-thread').attr('data-thread-id', data.threadId);
							$('#worker-inbox-list').attr('data-active-thread-id', data.threadId);
							toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
							//$('.item.worker-message-thread.worker-active-thread').click();
						} else {
							toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						}
					},
					error: function (error) {
						toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				});
			}
		})
		.modal('show');

});

$(document).on('click', '.button.worker-call-button', function () {
	let phoneNumber = $(this).parents(".field").attr('data-phonenumber');

	var threadId = $('.worker-active-thread').attr('data-thread-id');
	var workerId = $('.worker-active-thread').attr('data-id');
	var isworker = $('.worker-active-thread').attr('is-worker');
	$.ajax({
		type: 'GET',
		url: "/Messages/WorkerThreadInfo",
		data: { threadId: threadId, workerId: workerId, isworker: isworker },
		dataType: "json",
		beforeSend: function () {
			$(this).addClass('loading');
		},
		success: function (data) {
			$(this).removeClass('loading');

			var params = {
				From: userId,
				To: phoneNumber
			};

			if (device) {
				$('.mini.modal .phone-number').text(phoneNumber);
				$('.mini.modal .header.name').text(data.isWorker ? data.data.Firstname + ' ' + data.data.Lastname : data.data.Name);
				$('.mini.modal .avatar.image').attr('src', (data.data.ImageUrl != null ? data.data.ImageUrl : '/Images/Worker/default-image.png'));
				$('.mini.modal .avatar.image').show();
				device.connect(params);
				openOutgoingCallModal();
			}
		}
	});
});
	//Worker Call/sms End