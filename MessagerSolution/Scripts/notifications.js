﻿$(document).ready(function () {
    var tableNotifications;
    var tableNotificationEntries;
    CreateNotification();
    LoadNotifications();
    GetNotification();
    CreateNotification();
    SaveNotification();
    LoadNotificationEntries();
    UpdateNotificationStatus();
    $("#notification-variables").on('change', function () {
        var variable = $(this).children(':selected').text();//.replace('[', '').replace(']', '');
        $('#notification-description').append('<i class="icon tags"></i>' + variable);
    });
    function UpdateNotificationStatus() {
        $(document).on('click', '.update-notification-status', function (e) {
            var status = $(this).attr('status-value');
            var id = $(this).parents('tr').attr('data-notification-entry-id');
            $.ajax({
                type: "POST",
                url: "/Notification/UpdateNotificationStatus",
                data: { id: id, status: status },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.success)
                    tableNotificationEntries.ajax.reload(null, false);
                }
            });
        });
    }
    function LoadNotificationEntries() {
        tableNotificationEntries = $('#notification-entries-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "bAutoWidth": false,
            "bLengthChange": false,
            "searching": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Notification/GetNotificationEntries",
                "type": 'GET'
            },
            "columns": [
                { "data": "Description" },
                { "data": "Property" },
                { "data": "Date" },
                { "data": "ScheduleType" },
                { "data": "Status" },
                { "data": "Actions" }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-notification-entry-id', data.Id);
                if (data.Status == 0) {
                    $(row).addClass('negative');
                }
                else if (data.Status == 1) {
                    $(row).addClass('warning');
                }
                else if (data.Status == 2) {
                    $(row).addClass('positive');
                }
                else if (data.Status == 3) {
                    $(row).addClass('active');
                }
            }, columnDefs: [{
                targets: [2], type: "date", render: function (data) {
                    return moment(data).format('MMMM D,YYYY');
                }
            },
                {
                    targets: [4], type: "text", render: function (data) {
                        return data == 0 ? "Pending" : data == 1 ? "Ongoing" : data == 2 ? "Complete" : "Ignore";
                }
                }]
        });
	}
    function LoadNotifications() {
        tableNotifications = $('#notification-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "bAutoWidth": false,
            "bLengthChange": false,
            "searching": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Notification/GetNotifications",
                "type": 'GET'
            },
            "columns": [
                { "data": "Description" },
                { "data": "ScheduleTypeName" },      
                { "data": "Interval" },
                { "data": "FrequencyName" },
                { "data": "Actions" }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-notification-id', data.Id);
            }
        });
    }
    $('.editable').each(function () {
        this.contentEditable = true;
    });
    function CreateNotification() {
        $(document).on('click', '.create-notification', function (e) {
            $('#create-notification-modal').attr('notification-id', '0');
            $('#notification-description').empty();
            $('#notification-property').dropdown('clear');  
            $('#notification-renter').dropdown('clear');  
            $('#notification-frequency').dropdown('clear');  
            $('#notification-interval').val('');
            //$('#recurring-notification').val('');
            $('#notification-schedule-type').dropdown('clear');  
            //$('#notification-event-type').val('');
            $('#create-notification-modal').modal('show').modal('refresh');
            //$('.ui.calendar').calendar({
            //    type: 'date'
            //});
        });
    }
    function SaveNotification() {
        $(document).on('click', '#notification-submit-btn', function (e) {
            var id = $('#create-notification-modal').attr('notification-id');
            var formData = new FormData();
            formData.append("Id", id);
            formData.append("Description", $('#notification-description').text())
            formData.append("Frequency", $('#notification-frequency').dropdown('get value'));
            formData.append("Interval", $('#notification-interval').val());
            formData.append("ScheduleType", $('#notification-schedule-type').dropdown('get value'));
            //formData.append("EventType", $('#notification-event-type').val());
            //formData.append("RecurringDate", $('#recurring-notification').val());
            formData.append("PropertyIds", $('#notification-property').dropdown('get value'));
            //formData.append("RenterIds", $('#notification-renter').dropdown('get value'));
            $.ajax({
                type: "POST",
                url: "/Notification/SaveNotification",
                contentType: false,
                cache: false,
                dataType: "json",
                processData: false,
                data: formData,
                async: false,
                success: function (data) {
                    if (data.success) {
                        tableNotifications.ajax.reload(null, false);
                        toastr.success("Notification Succesfully saved!", null, { timeOut: 3000, positionClass: "toast-top-right" });
                        $('#create-notification-modal').modal('hide');
                    } else {
                        toastr.error("Error while Saving Notification", null, { timeOut: 3000, positionClass: "toast-top-right" });
                    }
                }
            });
        });
    }
    function GetNotification() {
        $(document).on('click', '.edit-notification', function (e) {
            var id = $(this).parents('tr').attr('data-notification-id');
            $.ajax({
                type: "GET",
                url: "/Notification/GetNotification",
                data: { id: id },
                dataType: "json",
                async: false,
                success: function (data) {
                    var renterIds = data.renterIds;
                    var propertyIds = data.propertyIds;
                    var notification = data.notification;
                    $('#create-notification-modal').attr('notification-id', notification.Id);
                    $('#notification-description').empty("");
                    $('#notification-description').append(notification.Description.replace(/\[/g, '<i class="icon tags"></i>['));
                    $('#notification-frequency').dropdown('set selected',notification.Frequency);
                    $('#notification-interval').val(notification.Interval);
                    //$('#notification-event-type').val(notification.EventType)
                    $('#notification-schedule-type').dropdown('set selected',notification.ScheduleType)
                    if (renterIds.length > 0) {
                        for (var x = 0; x < renterIds.length; x++) {
                        $('#notification-renter').dropdown('set selected', renterIds[x]);
                        //alert(renterIds[0]);
                        }
                    }
                    if (propertyIds.length > 0) {
                        for (var x = 0; x < propertyIds.length; x++) {
                        $('#notification-property').dropdown('set selected', propertyIds[x]);
                        //alert(propertyIds[0])
                        }
                    }
                    $('#create-notification-modal').modal('show').modal('refresh');
                }
            });
        });
	}
});