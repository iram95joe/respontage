﻿
// initialization scripts
var tableBooking;
var tableUpcomming;
var bookingForm = document.getElementById("booking-form");

$(document).ready(function () {
    //$('#paragraph').jqte();
    //$('#document-content').jqte();
    //$('#paragraph').jqte({ "status": true });
    //$('#document-content').jqte({ "status": true });
    var tableContract;
    initialize();
    function initialize() {
        setEventListeners();
        loadBookingTable();
        onCreateNewBooking();
        onDeleteBooking();
        onAddFee();
        onRemoveFee();
        onAddTax();
        onRemoveAirbnbTax();
        onRemoveTax();
        onSiteTypeDropdownChange();
        LoadPropertyDetailsById();
        onInquiryPriceChange();
        onFeeAmountChange();
        onEditAirbnbBooking();
        onEditAirbnbBookingSave();
        onAddAirbnbTax();
        onClearFilter();
		onBookingRowClick();
        onConfirmationCheck();
        setupSemanticUI();
        LoadContract();
        SaveDocumentTemplate();
        AddDocumentTemplate();
        DeleteContractTemplate();
        EditContractTemplate();
        loadUpcommingTable();
        ViewContract();
        SaveGeneratedTemplate();
        OnAddTemplateVariable();
    }

    var myHub = $.connection.bookingHub;
    $.connection.hub.logging = true;

    myHub.client.RefreshBookingList = function () {
        tableBooking.ajax.reload(null, false);//RefreshPropertyList();
    };

    $.connection.hub.start();

    var propertyFilter = "All";
    var bookingDateMonthFilter = "";
    var bookingDateYearFilter = "";
    var checkInDateMonthFilter = "";
    var checkInDateYearFilter = "";
    var statusFilter = "All";
    function OnAddTemplateVariable() {
        $("#modal-template-variable").on('change', function () {
            var variable = $(this).children(':selected').val();
            $('#document-template-modal').find('.nicEdit-main').append('<i class="icon tags"></i><span>' + variable + '</span>');
        });
    }

    function ChangeTextToSignRequestTags(content) {
        var elements = ["span", "p", "h1", "h2", "h3", "h4", "h5", "h6"];
        var searchText = ["[Signature]", "[Credit Card Number]", "[Expiry]", "[CVV]"];
        var replaceText = ["[[s|0]]", "[[t|0|n:Credit Card Number|id:creditcard_number]]", "[[t|0|n:Expiry|id:creditcard_expiry]]","[[t|0|n:CVV|id:creditcard_cvv]]"]
        //Replace Text on document to SignRequest Tag
        for (var i = 0; i < elements.length; i++) {
            for (var x = 0; x < searchText.length; x++) {
                content.find(elements[i] + ":contains('" + searchText[x] + "')").css("color", "white");
                content.find(elements[i] + ":contains('" + searchText[x] + "')").text(replaceText[x]);
                content.find(elements[i] + ">:contains('" + searchText[x] + "')").css("color", "white");
                content.find(elements[i] + ">:contains('" + searchText[x] + "')").text(replaceText[x]);
            }
          }
    }

    function VariableAddTags(content, searchText = ["[Checkout Date]", "[Checkin Date]", "[Property Address]", "[Guest]", "[Confirmation Code]", "[Guest Count]"]) {
        var elements = ["span", "h1", "h2", "h3", "h4", "h5", "h6"];
        //Replace Text on document to SignRequest Tag
        for (var i = 0; i < elements.length; i++) {
            for (var x = 0; x < searchText.length; x++) {
                $('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ">:contains('" + searchText[x] + "')"));
                $('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ":contains('" + searchText[x] + "')"));
                content.find(elements[i] + ">:contains('" + searchText[x] + "')").css("background-color", "yellow");
                content.find(elements[i] + ":contains('" + searchText[x] + "')").css("background-color", "yellow");
            }
        }
    }
    function ChangeSignRequestTagsToText(content) {
        var elements = ["span", "p", "h1", "h2", "h3", "h4", "h5", "h6"];
        var replaceText = ["[Signature]", "[Credit Card Number]", "[Expiry]", "[CVV]"];
        var searchText = ["[[s|0]]", "[[t|0|n:Credit Card Number|id:creditcard_number]]", "[[t|0|n:Expiry|id:creditcard_expiry]]", "[[t|0|n:CVV|id:creditcard_cvv]]"]
        //Replace Text on document to SignRequest Tag
        for (var i = 0; i < elements.length; i++) {
            for (var x = 0; x < searchText.length; x++) {
                $('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ">:contains('" + searchText[x] + "')"));
                $('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ":contains('" + searchText[x] + "')"));
                content.find(elements[i] + ":contains('" + searchText[x] + "')").css("color", "black");
                content.find(elements[i] + ":contains('" + searchText[x] + "')").text(replaceText[x]);
                content.find(elements[i] + ">:contains('" + searchText[x] + "')").css("color", "black");
                content.find(elements[i] + ">:contains('" + searchText[x] + "')").text(replaceText[x]);
            }
        }
    }
    function RemoveTag(content) {
        content.find('.icon.tags').next().css("background-color", "");;
        content.find('.icon.tags').remove();
    }
    function SaveDocumentTemplate() {
        $(document).on('click', '#contract-document-template-save', function () {
            ChangeTextToSignRequestTags($('#document-template-modal').find('.nicEdit-main'));
            RemoveTag($('#document-template-modal').find('.nicEdit-main'));
            var id = $('#document-template-modal').attr('document-id');
            var name = $('#document-name').val();
            var propertyId = $('#template-property').val();
            var formData = new FormData();
            for (var i = 0; i < $('#template-upload').get(0).files.length; ++i) {
                formData.append("Files[" + i + "]", $('#template-upload').get(0).files[i]);
            }
            formData.append("Id", id);
            formData.append("PropertyId", propertyId);
            formData.append("DocumentName", name);
            formData.append("Content", $('#document-template-modal').find('.nicEdit-main').html());
            $.ajax({
                type: 'POST',
                url: "/Booking/CreateTemplate",
                contentType: false,
                cache: false,
                dataType: "json",
                processData: false,
                data: formData,
                success: function (data) {
                    if (data.success) {
                        tableContract.ajax.reload(null, false);
                        $('#document-template-modal').modal("hide");

                    }
                    else {
                        swal("Error occured on deleting payment method", "", "error");
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
        });
    }
    function EditContractTemplate() {
        $(document).on('click', '.edit-contract-template', function () {
            var id = $(this).parents('tr').attr('data-contract-id');
            $.ajax({
                type: 'GET',
                url: "/Booking/GetTemplateDetails",
                data: { id: id },
                success: function (data) {
                    if (data.success) {
                        var template = data.template;
                        console.log(data.template)
                        var documentTemplateModal = $('#document-template-modal');
                        documentTemplateModal.attr('document-id', template.Id);
                        documentTemplateModal.find('#document-name').val(template.Name);
                        documentTemplateModal.find
                        $('#template-upload').find('#template-property').val(template.PropertyId);
                        //documentTemplateModal.find('.nicEdit-main').parent().css("width", "100%");
                        documentTemplateModal.find('.nicEdit-main').empty();
                        documentTemplateModal.find('.nicEdit-main').append(template.Html);
                        documentTemplateModal.find('.Section0').children().first().remove();
                        //Conver SignRequest tags to variables
                        ChangeSignRequestTagsToText(documentTemplateModal.find('.nicEdit-main'));
                        VariableAddTags(documentTemplateModal.find('.nicEdit-main'));
                        documentTemplateModal.find('img').each(function () {
                            //var imageObject = $(this);
                            var imgsrc = window.location.origin+"/Documents/DocumentHtml/" + $(this).attr("src");
                            $(this).attr('src', imgsrc);
                            //jQuery.ajax({
                            //    url: imgsrc,
                            //    xhrFields: {
                            //        responseType: 'blob'
                            //    },
                            //    success: function (data) {
                            //        var blobData = data;
                            //        var url = window.URL || window.webkitURL;
                            //        var src = url.createObjectURL(data);
                            //        imageObject.attr("src", src)
                            //    }
                            //});
                        });    
                        documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

                    }
                }
            });
        });
    }
    function DeleteContractTemplate() {
        $(document).on('click', '.delete-contract-template', function () {
            var id = $(this).parents('tr').attr('data-notice-document-id');
            swal({
                title: "Are you sure you want to delete this template?",
                text: "",
                type: "warning",
                confirmButtonColor: "##abdd54",
                confirmButtonText: "Delete",
                showCancelButton: true,
                closeOnConfirm: true,

            },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: "/Booking/DeleteContractTemplate",
                        data: { id: id },
                        success: function (data) {
                            if (data.success) {
                                tableContract.ajax.reload(null, false);
                            }
                        }
                    });
                });
        });
    }
    function AddDocumentTemplate() {
        $(document).on('click', '.create-contract', function (e) {
            var documentTemplateModal = $('#document-template-modal');
            documentTemplateModal.find('#document-name').val('');
            documentTemplateModal.find('.nicEdit-main').empty();
            //documentTemplateModal.find('.nicEdit-main').parent().css("width","100%");
            $('#template-upload').val('');
            documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

        });
    }
    function LoadContract() {
        tableContract = $('#contract-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "bAutoWidth": false,
            "bLengthChange": true,
            "searching": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Booking/GetContract",
                "type": 'GET',
                "async": true
            },
            "columns": [
                { "data": "Name" },
                { "data": "Actions" },
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-contract-id', data.Id);
            }
        });
    }
    $("#property-filter, #status-filter").change(function () {
        propertyFilter = $("#property-filter option[value='" + $("#property-filter").val() + "']").text();
       
		bookingDateMonthFilter = new Date($('#month-year-filter').val()).getMonth() + 1;
        bookingDateYearFilter = new Date($('#month-year-filter').val()).getFullYear();
        checkInDateMonthFilter = new Date($('#check-in-date-filter').val()).getMonth() + 1;
        checkInDateYearFilter = new Date($('#check-in-date-filter').val()).getFullYear();
        switch ($("#status-filter").val()) {
            case "all": statusFilter = "All"; break;
            case "A": statusFilter = "Confirmed"; break;
            case "B": statusFilter = "Booking"; break;
            case "BC": statusFilter = "Booking-Cancelled"; break;
            case "C": statusFilter = "Confirmed-Cancelled"; break;
            case "I": statusFilter = "Inquiry"; break;
            case "IC": statusFilter = "Inquiry-Cancelled"; break;
        }

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
          
                return (
                (propertyFilter == "All" || data[0] == propertyFilter) &&

                ((isNaN(bookingDateMonthFilter) == true && isNaN(bookingDateYearFilter) == true) ||
                    (new Date(data[4]).getMonth() + 1 == bookingDateMonthFilter && new Date(data[4]).getFullYear() == bookingDateYearFilter)) &&

                ((isNaN(checkInDateMonthFilter) == true && isNaN(checkInDateYearFilter) == true) ||
                    (new Date(data[5]).getMonth() + 1 == checkInDateMonthFilter && new Date(data[5]).getFullYear() == checkInDateYearFilter)) &&

                (statusFilter == "All" || data[9] == statusFilter))
                    ? true
                    : false
            }
        );
        tableBooking.draw();
        $.fn.dataTable.ext.search.pop();
    });

    $('#month-year-filter-calendar').calendar({
        type: 'month', onChange: function (date, text, mode) {
            propertyFilter = $("#property-filter option[value='" + $("#property-filter").val() + "']").text();
            bookingDateMonthFilter = new Date(date).getMonth() + 1;
            bookingDateYearFilter = new Date(date).getFullYear();
            checkInDateMonthFilter = new Date($('#check-in-date-filter').val()).getMonth() + 1;
            checkInDateYearFilter = new Date($('#check-in-date-filter').val()).getFullYear();
            switch ($("#status-filter").val()) {
                case "all": statusFilter = "All"; break;
                case "A": statusFilter = "Confirmed"; break;	
                case "B": statusFilter = "Booking"; break;
                case "BC": statusFilter = "Booking-Cancelled"; break;
                case "C": statusFilter = "Confirmed-Cancelled"; break;
                case "I": statusFilter = "Inquiry"; break;
                case "IC": statusFilter = "Inquiry-Cancelled"; break;
            }

            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    return (
                    (propertyFilter == "All" || data[0] == propertyFilter) &&

                    ((isNaN(bookingDateMonthFilter) == true && isNaN(bookingDateYearFilter) == true) ||
                        (new Date(data[4]).getMonth() + 1 == bookingDateMonthFilter && new Date(data[4]).getFullYear() == bookingDateYearFilter)) &&

                    ((isNaN(checkInDateMonthFilter) == true && isNaN(checkInDateYearFilter) == true) ||
                        (new Date(data[5]).getMonth() + 1 == checkInDateMonthFilter && new Date(data[5]).getFullYear() == checkInDateYearFilter)) &&

                    (statusFilter == "All" || data[9] == statusFilter))
                        ? true
                        : false
                }
            );
            tableBooking.draw();
            $.fn.dataTable.ext.search.pop();
        }
    });

    $('#check-in-date-filter-calendar').calendar({
        type: 'month', onChange: function (date, text, mode) {
            propertyFilter = $("#property-filter option[value='" + $("#property-filter").val() + "']").text();
            bookingDateMonthFilter = new Date($('#month-year-filter').val()).getMonth() + 1;
            bookingDateYearFilter = new Date($('#month-year-filter').val()).getFullYear();
            checkInDateMonthFilter = new Date(date).getMonth() + 1;
            checkInDateYearFilter = new Date(date).getFullYear();
            switch ($("#status-filter").val()) {
                case "all": statusFilter = "All"; break;
                case "A": statusFilter = "Confirmed"; break;
                case "B": statusFilter = "Booking"; break;
                case "BC": statusFilter = "Booking-Cancelled"; break;
                case "C": statusFilter = "Confirmed-Cancelled"; break;
                case "I": statusFilter = "Inquiry"; break;
                case "IC": statusFilter = "Inquiry-Cancelled"; break;
            }

            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    return (
                    (propertyFilter == "All" || data[0] == propertyFilter) &&

                    ((isNaN(bookingDateMonthFilter) == true && isNaN(bookingDateYearFilter) == true) ||
                        (new Date(data[4]).getMonth() + 1 == bookingDateMonthFilter && new Date(data[4]).getFullYear() == bookingDateYearFilter)) &&

                    ((isNaN(checkInDateMonthFilter) == true && isNaN(checkInDateYearFilter) == true) ||
                        (new Date(data[5]).getMonth() + 1 == checkInDateMonthFilter && new Date(data[5]).getFullYear() == checkInDateYearFilter)) &&

                    (statusFilter == "All" || data[9] == statusFilter))
                        ? true
                        : false
                }
            );
            tableBooking.draw();
            $.fn.dataTable.ext.search.pop();
        }
    });

    function onBookingRowClick() {
        $(document).on('click', '#booking-table tr td:not(:nth-child(8),:nth-child(1))', function () {
            var threadId = $(this).parents('tr').attr('data-thread-id');
            var hostId = $(this).parents('tr').attr('data-host-id');
            var inquiryId = $(this).parents('tr').attr('data-inquiry-id');
            if (threadId != '' && hostId != '' && threadId != null & hostId != null)
            {
                quickActionModal({
                    hostId: hostId,
                    threadId: threadId,
                    inquiryId: inquiryId,
                    page: 'booking'
                });
            }
        });
    }

    function onClearFilter() {
        $("#clear-filter-btn").on('click', function () {
            $('#month-year-filter').val('');
            $('#check-in-date-filter').val('');
			$('#property-filter').dropdown('set selected', '0');
            $('#status-filter').dropdown('set selected', 'all');
            tableBooking.draw();
        });
    }

    function onEditAirbnbBookingSave() {
        $(document).on('click', '#save-airbnb-booking', function () {
            var id = $('#airbnb-booking-id').val();
            var propertyId = $('#airbnb-property-id').val();
            var guestName = $('#airbnb-guest-name').val();
            var feeId = [];
            var feeAmount = [];
            var checkInTime = $('#airbnb-check-in-time').val();
            var checkOutTime = $('#airbnb-check-out-time').val();
            $('[name=airbnbTaxAmount]').each(function (i, v) {
                feeAmount.push($(v).val());
                feeId.push($(v).parent().parent().children().first().find('select').val());
            });
            $.ajax({
                type: "POST",
                url: '/Booking/EditAirbnbBooking',
                data: {
                    bookingId: id,
                    propertyId: propertyId,
                    guestName: guestName,
                    feeId: feeId,
                    amount: feeAmount,
                    checkInTime: checkInTime,
                    checkOutTime: checkOutTime
                },
                success: function (data) {
                    $('#edit-airbnb-booking-modal').modal('hide');
                    if(data.success)
                    {
                        swal("Booking successfully saved .", "", "success");
                    }
                    else {
                        swal("Error in saving booking", "", "error");
                    }
                }
            });
        });
    }

    function onEditAirbnbBooking() {
        $(document).on('click', '.edit-airbnb-booking', function (e) {
            var id = $(this).parents('tr').attr("data-inquiry-id");
            // clear all values
            $('#airbnb-booking-id').val('');
            $('#airbnb-property-id').val('');
            $('#airbnb-fee-options').val('');
            $('#airbnb-confirmation-code').val('');
            $('#airbnb-guest-name').val('');
            $('#airbnb-guest-count').val('');
            $('#airbnb-nights').val('');
            $('#airbnb-property-name').val('');
            $('#airbnb-reservation-cost').val('');
            $('#airbnb-check-in-date').val('');
            $('#airbnb-check-out-date').val('');
            $('#airbnb-check-in-time').val('');
            $('#airbnb-check-out-time').val('');
            $('#airbnb-payout-amount').val('');
            $('#airbnb-net-revenue').val('');
            $('#airbnb-booked-date').val('');
            $('#airbnb-inquiry-date').val('');
            $('.airbnb-fee-fields-wrapper').empty();
            $('.airbnb-tax-fields-wrapper').empty();

            $.ajax({
                type: "GET",
                url: '/Booking/GetAirbnbBookingDetails',
                data: { id: id },
                success: function (data) {
                    console.log(data);
                    if (data.success)
                    {
                        $('#airbnb-booking-id').val(id);
                        $('#airbnb-property-id').val(data.booking.Property.Id);
                        $('#airbnb-confirmation-code').val(data.booking.Inquiry.ConfirmationCode);
                        $('#airbnb-guest-name').val(data.booking.Guest.Name);
                        $('#airbnb-guest-count').val(data.booking.Inquiry.GuestCount);
                        $('#airbnb-nights').val(data.booking.Inquiry.Nights);
                        $('#airbnb-property-name').val(data.booking.Property.Name);
                        $('#airbnb-reservation-cost').val(data.booking.Inquiry.ReservationCost);
						$('#airbnb-check-in-date').val(moment(data.booking.Inquiry.CheckInDate).tz("America/Vancouver").format('MMM D, YYYY'));
						$('#airbnb-check-out-date').val(moment(data.booking.Inquiry.CheckOutDate).tz("America/Vancouver").format('MMM D, YYYY'));
						$('#airbnb-check-in-time').val(moment(data.booking.Inquiry.CheckInDate).tz("America/Vancouver").format('h:mm A'));
						$('#airbnb-check-out-time').val(moment(data.booking.Inquiry.CheckOutDate).tz("America/Vancouver").format('h:mm A'));
                        $('#airbnb-payout-amount').val(data.booking.Inquiry.TotalPayout);
                        $('#airbnb-net-revenue').val(data.booking.Inquiry.NetRevenueAmount);
						$('#airbnb-booked-date').val(moment(data.booking.Inquiry.BookingDate).tz("America/Vancouver").format('MMM D, YYYY h:mm A'));
						$('#airbnb-inquiry-date').val(moment(data.booking.Inquiry.InquiryDate).tz("America/Vancouver").format('MMM D, YYYY h:mm A'));
                        $('.airbnb-fee-fields-wrapper').append('<div class="fields">' +
                                    '<div class="four wide field">' +
                                    '<input type="text" readonly value="Airbnb Fee"></div>' +
                                    '<div class="four wide field">' +
                                    '<input type="text" readonly value="' + data.booking.Inquiry.ServiceFee + '"></div>' +
                                    '</div>');
                        var feeOptions = '';
                        $(data.propertyFees).each(function (index, item) {
                            feeOptions +=
                                '<option value="' + item.FeeTypes.Id +
                                '" data-compute-basis="' + item.PropertyFees.ComputeBasis +
                                '" data-amount-percent="' + item.PropertyFees.AmountPercent +
                                '" data-method="' + item.PropertyFees.Method +
                                '" data-inclusive-exclusive="' + item.PropertyFees.InclusiveOrExclusive +
                                '" data-show="' + item.PropertyFees.Show + '" data-prededucted="' + item.PropertyFees.IsPreDeducted + '">' +
                                item.FeeTypes.Fee_Type + '</option>';
                        });
                        $('#airbnb-fee-options').val(feeOptions);

                        $(data.expenses).each(function (index, item) {
                            if (!item.Expenses.IsPreDeducted)
                            {
                                $('.airbnb-tax-fields-wrapper').append('<div class="fields">' +
                                '<div class="four wide field">' +
                                '<select class="ui search dropdown">' + $('#airbnb-fee-options').val() +
                                '</select></div>' +
                                '<div class="four wide field"><input type="text" readonly name="airbnbTaxAmount" value="' + item.Expenses.Amount + '" placeholder="0.00"/></div>' +
                                '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                                activateDropdown();
                                $('.airbnb-tax-fields-wrapper select:last').val(item.FeeType.Id).change();
                                calculateAirbnbTax();
                                $(".airbnb-tax-fields-wrapper select:last").change(function () {
                                    $(this).parents('.fields').find('input[name=airbnbTaxAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                                    calculateAirbnbTax();
                                });
                            }
                        });
                        $('#edit-airbnb-booking-modal').modal('show');
                        $('.ui.calendar.time').calendar({ type: 'time' });
                    }
                }
            });
            e.stopPropagation();
        });
    }

    function onFeeAmountChange() {
        $(document).on('keyup', 'input[name="taxAmount"]', function () {
            calculate();
        });
        $(document).on('keyup', 'input[name="feeAmount"]', function () {
            calculate();
        });
    }

	function onInquiryPriceChange() {
		var inquiryPriceElement = bookingForm.querySelector("input[name=inquiryPrice]")
		inquiryPriceElement.addEventListener("keyup", function () {
			calculate()
		})
    }

    

    function calculateAirbnbTax() {
        var inquiryPrice = $("#airbnb-reservation-cost").val();
        var totalNonPrededucted = 0;
        var payoutPrice = $("#airbnb-payout-amount").val();

        $('.airbnb-tax-fields-wrapper select').each(function (index, element) {
            var temp = $(element).children('option:selected');
            var taxamt = temp.attr('data-amount-percent');
            var method = temp.attr('data-method');
            var computeBasis = temp.attr('data-compute-basis');
            var isInclusive = temp.attr('data-inclusive-exclusive');
            if (method == 2) { // percent based
                var calculation = 0;

                if (computeBasis == 1) // if based on booking price
                {
                    if (isInclusive == 1) {
                        calculation = (inquiryPrice - (inquiryPrice / (1 + Number(taxamt) / 100)));
                        calculation = calculation.toFixed(2);
                    }
                    else {
                        calculation = (inquiryPrice * taxamt) / 100;
                        calculation = calculation.toFixed(2);
                    }
                    totalNonPrededucted = totalNonPrededucted + Number(calculation);
                    $('input[name="airbnbTaxAmount"]:eq(' + index + ')').val(calculation);
                }
                else // if based on payout amount price
                {
                    if (isInclusive == 1) {
                        calculation = (payoutPrice - (payoutPrice / (1 + Number(taxamt) / 100)));
                        calculation = calculation.toFixed(2);
                    }
                    else {
                        calculation = (payoutPrice * taxamt) / 100;
                        calculation = calculation.toFixed(2);
                    }
                    totalNonPrededucted = totalNonPrededucted + Number(calculation);
                    $('input[name="airbnbTaxAmount"]:eq(' + index + ')').val(calculation);
                }
            }
            else // amount based
            {
                totalNonPrededucted = (totalNonPrededucted + Number(taxamt));
            }
        });

        var netrevenue = payoutPrice - totalNonPrededucted;
        netrevenue = netrevenue.toFixed(2);
        $('#airbnb-net-revenue').val(netrevenue);
    }

    function onDeleteBooking() {
        $(document).on('click', '.delete-booking', function (e) {
            var id = $(this).parents('tr').attr("data-inquiry-id");
            $.ajax({
                type: "POST",
                url: '/Booking/RemoveBooking',
                data: { Id: id },
                success: function (data) {
                    $(this).parents('tr').remove();
                }
            });
        });
	}

	function getPropertySiteType(sitetype) {
		var properties = null;
		$.ajax({
			type: 'GET',
			url: "/Booking/GetPropertyBySiteType",
			async: false,
			data: { sitetype: sitetype },
			success: function (data) {
				
				if (data.success) {
					properties = data.properties;
				}
			}
		});
		return properties;
	}

	function onConfirmationCheck(){
		$(document).on('focusout', '.confirmation-code-field', function () {
			var code = $('#booking-form input[name=confirmationCode]').val();
			if (IsConfirmationCodeExist(code)) {
				alert("code is existing in current data");
				$('#booking-form input[name=confirmationCode]').val("");

			}
			
		});
		//$('.confirmation-code-field');
	}

	function IsConfirmationCodeExist(code) {
		var result = false;
		$.ajax({
			type: 'GET',
			url: "/Booking/IsConfirmationCodeExist",
			async: false,
			data: { code: code},
			success: function (data) {
				
				if (data.exist) {
					result = data.exist;
				}
			}
		});
		return result;
	}

	function onSiteTypeDropdownChange() {
		$('#booking-form select[name=siteType]').change(function () {
			var type = $(this).val();
			if (type == '0') {
				$('.confirmation-code-field').hide();
				var properties = getPropertySiteType(type);
				$('#booking-form select[name=propertyId]').empty();

				if (properties.length > 0) {
					$('#booking-form select[name=propertyId]').append('<option class="placeholder" selected disabled value="">Select Property</option>');
					for (i = 0; i < properties.length; i++) {
						$('#booking-form select[name=propertyId]').append("<option value =" + properties[i].Id + ">" + properties[i].Name + "</option>");
					}
				}
				//else if(properties.length == 0) {
				//	$('#booking-form select[name=propertyId]').append('<option class="placeholder" selected disabled value="">No properties</option>');
				//}
			}
			else {

				$('.confirmation-code-field').show();
				var properties = getPropertySiteType(type);
				$('#booking-form select[name=propertyId]').empty();

				if (properties.length > 0)
				{
					$('#booking-form select[name=propertyId]').append('<option class="placeholder" selected disabled value="">Select Property</option>');
					for (i = 0; i < properties.length; i++) {
					$('#booking-form select[name=propertyId]').append("<option value =" + properties[i].Id + ">" + properties[i].Name + "</option>");
					}
				}
				//else if (properties.length == 0)
				//{
				//	$('#booking-form select[name=propertyId]').append('<option class="placeholder" selected disabled value="">No properties</option>');
				//}
			}
		});
		$('#edit-booking-form select[name=siteType]').change(function () {
			type = $(this).val();
			if (type == '0') {
				$('.confirmation-code-field').hide();
			}
			else {
				$('.confirmation-code-field').show();
			}
		});
    }

    function LoadPropertyDetailsById() {
		$('#booking-form select[name=propertyId]').change(function () {
			var propertyId = $(this).val()
			if (propertyId != '') {
                $('.fee-fields-wrapper').empty();
				$('.tax-fields-wrapper').empty();

				document.querySelector("#booking-form input[name=payoutAmount]").value = "0.00"
				document.querySelector("#booking-form input[name=netRevenue]").value = "0.00"

                $.ajax({
                    type: "POST",
                    url: '/Booking/GetPropertyDetailsById',
					data: { PropertyId: propertyId },
                    success: function (data)
                    {
                        if (data.success)
                        {
							bookingForm.querySelector("input[name=feeTypeDropdownOptions]").value = ''
                            var feeOptions = '';
                            $('.fee-fields-wrapper').empty();
                            $('.tax-fields-wrapper').empty();

                            $(data.feeList).each(function (index, item) {
                                feeOptions +=
                                    '<option value="' + item.FeeTypeId +
                                    '" data-compute-basis="' + item.ComputeBasis +
                                    '" data-amount-percent="' + item.AmountPercent +
                                    '" data-method="' + item.Method +
                                    '" data-inclusive-exclusive="' + item.InclusiveOrExclusive +
                                    '" data-show="' + item.Show + '" data-prededucted="' + item.PreDeducted + '">' +
                                    item.FeeName + '</option>';
							});
							bookingForm.querySelector("input[name=feeTypeDropdownOptions]").value = feeOptions

                            $(data.feeList).each(function (index, item) {
                                if (item.PreDeducted && item.Show) {
                                    $('.fee-fields-wrapper').append('<div class="fields">' +
                                    '<div class="four wide field">' +
                                    '<select name="vatgrp" class="ui search dropdown">' + feeOptions +
                                    '</select></div>' +
                                    '<div class="four wide field"><input type="text" readonly name="feeAmount" value="' + (item.Method == 1 ? item.AmountPercent : 0.00) + '" placeholder="0.00"/></div>' +
                                    '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                                    activateDropdown();
                                    $('.fee-fields-wrapper select:last').val(item.FeeTypeId).change();
                                    $(".fee-fields-wrapper select:last").change(function () {
                                        $(this).parents('.fields').find('input[name=feeAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                                        calculate();
                                    });
                                }
                                else if (!item.PreDeducted && item.Show) {
                                    $('.tax-fields-wrapper').append('<div class="fields">' +
                                    '<div class="four wide field">' +
                                    '<select name="vatgrp" class="ui search dropdown">' + feeOptions +
                                    '</select></div>' +
                                    '<div class="four wide field"><input type="text" readonly name="taxAmount" value="' + (item.Method == 1 ? item.AmountPercent : 0.00) + '" placeholder="0.00"/></div>' +
                                    '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                                    activateDropdown();
                                    $('.tax-fields-wrapper select:last').val(item.FeeTypeId).change();
                                    $(".tax-fields-wrapper select:last").change(function () {
                                        $(this).parents('.fields').find('input[name=taxAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                                        calculate();
                                    });
                                }
                            });

							var bookingId = bookingForm.querySelector("input[name=bookingId]").value
							if (bookingId != 0 && bookingId != null) {
                                calculate();
                            }
                        }
                    }
                });
            }
        });
    }

	$(document).on('click', '#submitNewBookingBtn', function (e) {
		e.preventDefault();
		$('.error_message').hide();
		$('.error_message').css('color', 'red');
		var bookingId = bookingForm.querySelector("input[name=bookingId]").value
		var siteType = bookingForm.querySelector("select[name=siteType]").value
		var confirmationCode = bookingForm.querySelector("input[name=confirmationCode]").value
		var guestName = bookingForm.querySelector("input[name=guestName]").value
		var guestCount = bookingForm.querySelector("input[name=guestCount]").value
		var propertyId = bookingForm.querySelector("select[name=propertyId]").value
		var checkInDate = bookingForm.querySelector("input[name=checkInDate]").value
		var checkOutDate = bookingForm.querySelector("input[name=checkOutDate]").value
		var checkInTime = bookingForm.querySelector("input[name=checkInTime]").value
		var checkOutTime = bookingForm.querySelector("input[name=checkOutTime]").value
		var inquiryPrice = bookingForm.querySelector("input[name=inquiryPrice]").value
		var payoutAmount = bookingForm.querySelector("input[name=payoutAmount]").value
		var netRevenue = bookingForm.querySelector("input[name=netRevenue]").value
		var payoutDate = bookingForm.querySelector("input[name=payoutDate]").value
		var bookingDate = bookingForm.querySelector("input[name=bookingDate]").value
		var bookingTime = bookingForm.querySelector("input[name=bookingTime]").value
		var inquiryDate = bookingForm.querySelector("input[name=inquiryDate]").value
		var inquiryTime = bookingForm.querySelector("input[name=inquiryTime]").value
		var bookingStatus = bookingForm.querySelector("select[name=bookingStatus]").value

		if (siteType != "" && siteType != "0") {
			if (confirmationCode == "") {
				bookingForm.querySelector("input[name=confirmationCode]").focus()
				bookingForm.querySelector("input[name=confirmationCode]").nextElementSibling.style.display = 'block'
				return false;
			}
		}

		if (siteType == "") {
			bookingForm.querySelector("select[name=siteType]").focus()
			bookingForm.querySelector(".site-type-field").querySelector(".error_message").style.display = 'block'
			return false;
		}

		//else if (GuestName == "") {
		//	$('#GuestName').focus();
		//	$('#errrorGuestName').fadeIn();
		//	return false;
		//}
		//else if (GuestCount == "") {
		//	$('#GuestCount').focus();
		//	$('#errrorGuestCount').fadeIn();
		//	return false;
		//}
		//else if (/\D/g.test(GuestCount)) {
		//	$('#GuestCount').focus();
		//	$('#errrorGuestCountNumaric').fadeIn();
		//	return false;
		//}
		//else if (Property == "") {
		//	$('#PropertyId').focus();
		//	$('#errrorproperty').fadeIn();
		//	return false;
		//}

		//else if (CheckinDate == "") {
		//	$('#checkindate').focus();
		//	$('#errrorcheckindate').fadeIn();
		//	return false;
		//}
		//else if (CheckoutDate == "") {
		//	$('#checkoutdate').focus();
		//	$('#errrorcheckoutdate').fadeIn();
		//	return false;
		//}
		//else if (CheckinTime == "") {
		//	$('#checkintime').focus();
		//	$('#errrorcheckintime').fadeIn();
		//	return false;
		//}
		//else if (CheckoutTime == "") {
		//	$('#checkouttime').focus();
		//	$('#errrorcheckouttime').fadeIn();
		//	return false;
		//}
		//else if (InquiryPrice == "") {
		//	$('#inquiryprice').focus();
		//	$('#errrorinquiryprice').fadeIn();
		//	return false;
		//}
		//else if (/[^0-9\.]/g.test(InquiryPrice)) {
		//	$('#inquiryprice').focus();
		//	$('#errrorinquirypricenumeric').fadeIn();
		//	return false;
		//}
		//else if (PayoutAmt == "") {
		//	$('#payoutamt').focus();
		//	$('#errrorpayoutamt').fadeIn();
		//	return false;
		//}
		//else if (/[^0-9\.]/g.test(PayoutAmt)) {
		//	$('#payoutamt').focus();
		//	$('#errrorpayoutamtnumeric').fadeIn();
		//	return false;
		//}
		//else if (Payoutdate == "") {
		//	$('#payoutdate').focus();
		//	$('#errrorpayoutdate').fadeIn();
		//	return false;
		//}
		//else if (Bookeddate == "") {
		//	$('#bookeddate').focus();
		//	$('#errrorbookeddate').fadeIn();
		//	return false;
		//}
		//else if (Bookedtime == "") {
		//	$('#bokedtime').focus();
		//	$('#errrorbookedtime').fadeIn();
		//	return false;
		//}
		//else if (Inquirydate == "") {
		//	$('#Inquirydate').focus();
		//	$('#errrorInquirydate').fadeIn();
		//	return false;
		//}
		//else if (Status == "") {
		//	$('#status').focus();
		//	$('#errrorstatus').fadeIn();
		//	return false;
		//}
		else {
			if (siteType != "" && guestName != "" && guestCount != "" && propertyId != "" && checkInDate != "" && checkInTime != ""
				&& checkOutDate != "" && checkOutTime != "" && inquiryPrice != "" && payoutAmount != "" && netRevenue != "" && bookingDate != "" && bookingTime != ""
				&& inquiryDate != "" && inquiryTime != "" && bookingStatus != "") {


				var feeFeeId = "";
				var taxFeeId = "";
				var feeValue = "";
				var taxValue = "";

				$('input[name="feeAmount"]').each(function (i, v) {
					feeValue = (feeValue + "," + $(v).val());
					feeFeeId = (feeFeeId + "," + $(v).parent().parent().children().first().find('select').val());
				});

				$('[name=taxAmount]').each(function (i, v) {
					taxValue = (taxValue + "," + $(v).val());
					taxFeeId = (taxFeeId + "," + $(v).parent().parent().children().first().find('select').val());
				});

				$.ajax({
					type: "POST",
					url: '/Booking/AddBooking',
					data: {
						SiteType: siteType,
						ConfirmationCode: confirmationCode,
						GuestName: guestName,
						GuestCount: guestCount,
						PropertyId: propertyId,
						CheckinDate: checkInDate,
						CheckinTime: checkInTime,
						CheckoutDate: checkOutDate,
						CheckoutTime: checkOutTime,
						InquiryPrice: inquiryPrice,
						PayoutAmt: payoutAmount,
						Payoutdate: payoutDate,
						NetRevenue: netRevenue,
						Bookeddate: bookingDate,
						Bookedtime: bookingTime,
						Inquirydate: inquiryDate,
						inquirytime: inquiryTime,
						Status: bookingStatus,
						Id: bookingId,
						feeValue: feeValue,
						taxValue: taxValue,
						feeFeeId: feeFeeId,
						taxFeeId: taxFeeId

					},
					success: function (data) {
						tableBooking.ajax.reload(null, false);
					}
				});
				//$('#myHiddenInput').val(0);
				$('.create-new-booking-modal').modal('hide');
				//$('#SiteType').val("");
				//$('#confirmation-code').val("");
				//$('#GuestName').val("");
				//$('#GuestCount').val("");
				//$('#PropertyId').val("");
				//$('#checkoutdate').val("");
				//$('#checkindate').val("");
				//$('#checkintime').val("");
				//$('#checkouttime').val("");
				//$('#inquiryprice').val("");
				//$('#payoutamt').val("");
				//$('#payoutdate').val("");
				//$('#bookeddate').val("");
				//$('#bokedtime').val("");
				//$('#Inquirydate').val("");
				//$('#status').val("");
				$('.fee-fields-wrapper').empty();
				$('.tax-fields-wrapper').empty();
			}
			else
			{
				alert("Please complete the booking details");
			}
		}
	});

    $(document).on('click', '.edit-booking', function (e) {
        $('.create-new-booking-modal').modal('show');
        $('.ui.calendar').calendar({
            type: 'date'
        });
        $('.ui.calendar.time').calendar({
            type: 'time'
        });
        var id = $(this).parents('tr').attr("data-inquiry-id");
        $('#myHiddenInput').val(id);
        var CopyId = id;
        $.ajax({
            type: "POST",
            url: '/Booking/GetDetailBooks',
            data: { Id: id },
            success: function (data) {
                console.log(data);
                $('#SiteType').dropdown('set selected', data[0].SiteType);
                $('#ConfirmationCode').val(data[0].ConfirmationCode);
                $('#GuestName').val(data[0].GuestName);
                $('#GuestCount').val(data[0].GuestCount);
                $('#PropertyId').off('change');
                $("#PropertyId").val(data[0].PropertyId).change();
                LoadPropertyDetailsById();
                $('#checkoutdate').val(data[0].StayEndDate);
                $('#checkindate').val(data[0].StayStartDate);
                $('#checkintime').val(data[0].StayStartTime);
                $('#checkouttime').val(data[0].StayEndTime);
                $('#inquiryprice').val(data[0].Price);
                $('#payoutamt').val(data[0].ExpensePayout);
                $('#netrevenue').val(data[0].NetRevAmt);
                $('#payoutdate').val(data[0].PayoutDate);
                $('#bookeddate').val(data[0].BookDate);
                $('#bokedtime').val(data[0].BookTime);
                $('#Inquirydate').val(data[0].InquiryDate);
                $('#status').val(data[0].Status).change();
                var feeOptions = '';
                $(data[0].PropertyFees).each(function (index, item) {
                    feeOptions +=
                        '<option value="' + item.FeeTypes.Id +
                        '" data-compute-basis="' + item.PropertyFees.ComputeBasis +
                        '" data-amount-percent="' + item.PropertyFees.AmountPercent +
                        '" data-method="' + item.PropertyFees.Method +
                        '" data-inclusive-exclusive="' + item.PropertyFees.InclusiveOrExclusive +
                        '" data-show="' + item.PropertyFees.Show + '" data-prededucted="' + item.PropertyFees.IsPreDeducted + '">' +
                        item.FeeTypes.Fee_Type + '</option>';
                });
                $('#feeTypeDropdownOptions').val(feeOptions);

                $(data[0].Expenses).each(function (index, item) {
                    if (item.IsPaid) {
                        $('.fee-fields-wrapper').append('<div class="fields">' +
                        '<div class="four wide field">' +
                        '<select name="vatgrp" class="ui search dropdown">' + $('#feeTypeDropdownOptions').val() +
                        '</select></div>' +
                        '<div class="four wide field"><input type="text" readonly name="feeAmount" value="' + item.Amount + '" placeholder="0.00"/></div>' +
                        '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                        activateDropdown();
                        $('.fee-fields-wrapper select:last').val(item.FeeTypeId).change();
                        $(".fee-fields-wrapper select:last").change(function () {
                            $(this).parents('.fields').find('input[name=feeAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                            calculate();
                        });
                    }
                    else {
                        $('.tax-fields-wrapper').append('<div class="fields">' +
                        '<div class="four wide field">' +
                        '<select name="vatgrp" class="ui search dropdown">' + $('#feeTypeDropdownOptions').val() +
                        '</select></div>' +
                        '<div class="four wide field"><input type="text" readonly name="taxAmount" value="' + item.Amount + '" placeholder="0.00"/></div>' +
                        '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                        activateDropdown();
                        $('.tax-fields-wrapper select:last').val(item.FeeTypeId).change();
                        $(".tax-fields-wrapper select:last").change(function () {
                            $(this).parents('.fields').find('input[name=taxAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                            calculate();
                        });
                    }
                });
                //getting selected value from edit
                //calculate();
            }
        });
    });

	function loadBookingTable() {
        tableBooking = $('#booking-table').DataTable({
            //rowReorder: {
            //    selector: 'td:nth-child(2)'
            //},
            responsive: true,
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Booking/GetBookingList",
                "type": 'GET',
                "data": function () {
                    return {
                        monthYearFilter: $('#month-year-filter').val(),
                        checkInDateFilter: $('#check-in-date-filter').val(),
                        propertyFilter: $('#property-filter').val(),
                        statusFilter: $('#status-filter').val()
                    }
                }
            },
            "columns": [
                    { "data": "PropertyName" },
                    { "data": "Site" },
                    { "data": "GuestName" },
                    { "data": "InquiryDate" },
                    { "data": "BookingDate" },
                    { "data": "CheckInDate" },
                    { "data": "CheckOutDate" },
                    { "data": "ReservationCost" },
                    { "data": "TotalPayout" },
                    { "data": "Status" },
                    { "data": "Payments" },
                    { "data": "Balance"},
                    { "data": "Actions"}
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).css('cursor', 'pointer');
                $(row).attr('data-inquiry-id', data.Id);
                $(row).attr('data-host-id', data.HostingAccountId);
                $(row).attr('data-inquiry-code', data.ConfirmationCode);
                $(row).attr('data-guest-id', data.GuestId);
                $(row).attr('data-thread-id', data.ThreadId);
                $(row).find('.ui.accordion').accordion();
            },
            columnDefs: [{
                targets: [3, 4, 5, 6], type:"date", render: function (data) {
                    if (data == null) {
                        return '';
                    } else {
                        return moment(data).format('MMMM D,YYYY');
                    }
                }
            }]
        });
    }
    function loadUpcommingTable() {
        tableUpcomming = $('#upcomming-table').DataTable({
            responsive: true,
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Booking/UpcommingBooking",
                "type": 'GET'
            },
            "columns": [
                { "data": "Property" },
                { "data": "Site" },
                { "data": "GuestName" },
                { "data": "CheckInDate" },
                { "data": "CheckOutDate" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-reservation-id', data.Id);
            },
            columnDefs: [{
                targets: [3, 4], type: "date", render: function (data) {
                    return moment(data).format('MMMM D,YYYY');
                }
            }],
        });
    }
    function ViewContract() {
        $(document).on('click', '.view-contract', function () {
            var bookingId = $(this).parents('tr').attr('data-reservation-id');
            $.ajax({
                type: 'GET',
                url: "/Booking/GetContractTemplateWithData",
                data: { id: bookingId },
                async: true,
                success: function (data) {
                    var document = data.document;
                    var documentModal = $('#uploaded-document-modal');
                    $('#guest-email').val(data.email);
                    documentModal.attr('document-id', document.Id);
                    //documentModal.find('.nicEdit-main').parent().css("width", "100%");
                    documentModal.find('.nicEdit-main').empty();
                    documentModal.find('.nicEdit-main').append(document.Html);
                    documentModal.find('.Section0').children().first().remove(); 
                    ChangeSignRequestTagsToText(documentModal.find('.nicEdit-main'));
                    VariableAddTags(documentModal.find('.nicEdit-main'),data.variables);
                    documentModal.find('img').each(function () {
                        var imgsrc = window.location.origin +"/Documents/ContractHtml/" + $(this).attr("src");
                        $(this).attr('src', imgsrc);
                    });    
                    documentModal.modal({ closable: false }).modal('show').modal('refresh');
                }
            });
        });
    }
  
    function SaveGeneratedTemplate() {
        $(document).on('click', '#contract-save-send', function () {
            var id = $('#uploaded-document-modal').attr('document-id');
            //Replace Text on document to SignRequest Tag
            ChangeTextToSignRequestTags($('#uploaded-document-modal').find('.nicEdit-main'));
            RemoveTag($('#uploaded-document-modal').find('.nicEdit-main'));
            $.ajax({
                type: 'POST',
                url: "/Booking/SaveGeneratedDocument",
                data: { id: id, content: $('#uploaded-document-modal').find('.nicEdit-main').html(), email: $('#guest-email').val() },
                success: function (data) {
                    if (data.success) {
                        $('#uploaded-document-modal').modal('hide');
                    }
                    else {
                        swal("Error occured on deleting payment method", "", "error");
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
        });
    }
    function onAddAirbnbTax() {
        $(document).on('click', '.airbnb_add_tax_button', function () {
            if ($('#airbnb-fee-options').val() == '') {
                alert('No fee set in property');
            }
            else {
                var count = $('.airbnb-tax-fields-wrapper').children().length;
                if (count <= 5) {
                    $('.airbnb-tax-fields-wrapper').append('<div class="fields">' +
                    '<div class="four wide field">' +
                      '<select class="ui search dropdown">' + $('#airbnb-fee-options').val() +
                      '</select></div>' +
                    '<div class="four wide field"><input type="text" readonly name="airbnbTaxAmount" placeholder="0.00"/></div>' +
                    '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                    activateDropdown();
                    calculateAirbnbTax();
                    $(".airbnb-tax-fields-wrapper select:last").change(function () {
                        $(this).parents('.fields').find('input[name=airbnbTaxAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                        calculateAirbnbTax();
                    });
                }
            }
        });
    }

    function onAddTax() {
		$(document).on('click', '.add_tax_button', function () {
			var propertyId = bookingForm.querySelector("select[name=propertyId]").value
			var selectOptions = bookingForm.querySelector("input[name=feeTypeDropdownOptions]").value
			if (propertyId == '')
            {
                alert('Please select property first.');
            }
            else
            {
                var count = $('.tax-fields-wrapper').children().length;
                if (count <= 5) {
                    $('.tax-fields-wrapper').append('<div class="fields">' +
                    '<div class="four wide field">' +
						'<select class="ui search dropdown">' + selectOptions +
						'</select></div>' +
                    '<div class="four wide field"><input type="text" readonly name="taxAmount" placeholder="0.00"/></div>' +
                    '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                    activateDropdown();
                    calculate();
                    $(".tax-fields-wrapper select:last").change(function () {
                        $(this).parents('.fields').find('input[name=taxAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                        calculate();
                    });
                }
            }
        });
    }

    function onAddFee() {
		$(document).on('click', '.add_fee_button', function () {
			var propertyId = bookingForm.querySelector("select[name=propertyId]").value
			var selectOptions = bookingForm.querySelector("input[name=feeTypeDropdownOptions]").value
			if (propertyId == '') {
                alert('Please select property first.');
            }
            else {
                var count = $('.fee-fields-wrapper').children().length;
                if (count <= 5) {
                    $('.fee-fields-wrapper').append('<div class="fields">' +
                    '<div class="four wide field">' +
						'<select class="ui search dropdown">' + selectOptions +
                      '</select></div>' +
                    '<div class="four wide field"><input type="text" readonly name="feeAmount" placeholder="0.00"/></div>' +
                    '<a href="#" class="remove_field" data-tooltip="Delete"><i class="orange minus circle icon remove-field-icon"></i></a></div>');
                    activateDropdown();
                    calculate();
                    $(".fee-fields-wrapper select:last").change(function () {
                        $(this).parents('.fields').find('input[name=feeAmount]').val($(this).children('option:selected').attr('data-amount-percent'));
                        calculate();
                    });
                }
            }
        });
    }

    function onRemoveFee() {
        $('.fee-fields-wrapper').on("click", ".remove_field", function () {
            $(this).parent('div').remove();
            calculate();
        });
    }

    function onRemoveTax() {
        $('.tax-fields-wrapper').on("click", ".remove_field", function () {
            $(this).parent('div').remove();
            calculate();
        });
    }

    function onRemoveAirbnbTax() {
        $('.airbnb-tax-fields-wrapper').on("click", ".remove_field", function () {
            $(this).parent('div').remove();
            calculateAirbnbTax();
        });
    }

    function onCreateNewBooking() {
		$(document).on('click', '.js-btn_create-new-booking', function () {
			bookingForm.querySelector("input[name=bookingId]").value = 0
            $('.error_message').hide();
			$('#booking-form select[name=propertyId]').dropdown('clear');
			$('#booking-form select[name=siteType]').dropdown('clear');
			$('#booking-form select[name=bookingStatus]').dropdown('clear');
			bookingForm.querySelector("input[name=confirmationCode]").value = "";
			bookingForm.querySelector("input[name=guestName]").value = "";
			bookingForm.querySelector("input[name=guestCount]").value = "";
			bookingForm.querySelector("input[name=checkOutDate]").value = "";
			bookingForm.querySelector("input[name=checkInDate]").value = "";
			bookingForm.querySelector("input[name=checkInTime]").value = "";
			bookingForm.querySelector("input[name=checkOutTime]").value = "";
			bookingForm.querySelector("input[name=inquiryPrice]").value = "";
			bookingForm.querySelector("input[name=payoutAmount]").value = "";
			bookingForm.querySelector("input[name=payoutDate]").value = "";
			bookingForm.querySelector("input[name=bookingDate]").value = "";
			bookingForm.querySelector("input[name=bookingTime]").value = "";
			bookingForm.querySelector("input[name=inquiryDate]").value = "";
			bookingForm.querySelector("input[name=netRevenue]").value = "";
            $('.fee-fields-wrapper').empty();
            $('.tax-fields-wrapper').empty();
            $('.create-new-booking-modal').modal('show');
            $('.ui.calendar').calendar({
                type: 'date'
            });

            $('.ui.calendar.time').calendar({
                type: 'time'
            });
        });
    }

    //function RefreshBookingList() {
    //    $.ajax({
    //        type: 'GET',
    //        url: "/Booking/RefreshBookingList",
    //        async: true,
    //        success: function (data) {
    //            console.log(data);
    //            if (data.success) {
    //                tableBooking.ajax.reload(null, false);
    //            }
    //            setTimeout(function () { RefreshBookingList(); }, 15000);
    //        }
    //    });
    //}

    function setupSemanticUI() {
        $('.ui.accordion').accordion();
        $('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
        $('.ui.checkbox').checkbox();
        $('.ui.calendar').calendar({ type: 'date' });
        $('.ui.calendar.month').calendar({ type: 'month' });
        $('.ui.calendar.time').calendar({ type: 'time' });
        $('table td a.table-tooltip').popup();
    }

    function setEventListeners() {
    }

    // ------------------
    // ------------------
    //   Event Listeners
    // ------------------
    // ------------------
    function RefreshSingleBooking(hostId, reservationCode) {
        $.ajax({
            type: 'GET',
            url: "/Booking/RefreshSingleBooking",
            data: { hostId: hostId, reservationCode: reservationCode },
            async: true,
            success: function (data) {
                console.log(data);
                if (data.success) {
                    tableBooking.ajax.reload(null, false);
                }
            }
        });
    }

    function activateDropdown() {
        $('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
    }
});

// external functions

function calculate() {
	var inquiryPrice = bookingForm.querySelector("input[name=inquiryPrice]").value
	var total = 0
	var totalNonPrededucted = 0;
	var payout = inquiryPrice;
	bookingForm.querySelector("input[name=payoutAmount]").value = payout
	var taxtotal = 0
	$('.fee-fields-wrapper select').each(function (index, element) {
		var temp = $(element).children('option:selected');
		var valamt = temp.attr('data-amount-percent');
		var method = temp.attr('data-method');
		var isInclusive = temp.attr('data-inclusive-exclusive');
		var calculation = valamt;
		if (method == 2) {
			if (isInclusive == 1) {
				calculation = (inquiryPrice - (inquiryPrice / (1 + Number(valamt) / 100)));
			}
			else {
				calculation = (inquiryPrice * valamt) / 100;
			}
			total = total + Number(calculation);
			$('input[name="feeAmount"]:eq(' + index + ')').val(calculation.toFixed(2));
		}
		else { total = (total + Number(valamt)); }
	});
	payout = inquiryPrice - total;
	payout = payout.toFixed(2);
	bookingForm.querySelector("input[name=payoutAmount]").value = payout

	$('.tax-fields-wrapper select').each(function (index, element) {
		var temp = $(element).children('option:selected');
		var taxamt = temp.attr('data-amount-percent');
		var method = temp.attr('data-method');
		var computeBasis = temp.attr('data-compute-basis');
		var isInclusive = temp.attr('data-inclusive-exclusive');
		if (method == 2) { // percent based
			var calculation = 0;

			if (computeBasis == 1) // if based on booking price
			{
				if (isInclusive == 1) {
					calculation = (inquiryPrice - (inquiryPrice / (1 + Number(taxamt) / 100)));
					calculation = calculation.toFixed(2);
				}
				else {
					calculation = (inquiryPrice * taxamt) / 100;
					calculation = calculation.toFixed(2);
				}
				totalNonPrededucted = totalNonPrededucted + Number(calculation);
				$('input[name="taxAmount"]:eq(' + index + ')').val(calculation);
			}
			else // if based on payout amount price
			{
				if (isInclusive) {
					calculation = (payout - (payout / (1 + Number(taxamt) / 100)));
					calculation = calculation.toFixed(2);
				}
				else {
					calculation = (payout * taxamt) / 100;
					calculation = calculation.toFixed(2);
				}
				totalNonPrededucted = totalNonPrededucted + Number(calculation);
				$('input[name="taxAmount"]:eq(' + index + ')').val(calculation);
			}
		}
		else // amount based
		{
			totalNonPrededucted = (totalNonPrededucted + Number(taxamt));
		}
	});

	var netrevenue = payout - totalNonPrededucted;
	netrevenue = netrevenue.toFixed(2);
	bookingForm.querySelector("input[name=netRevenue]").value = netrevenue
}