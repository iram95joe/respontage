﻿$(document).ready(function () {
	var siteType = "";
	var deviceRefresh;
	Login();
	LoginSubmit();
	LoadDevices();
	GetPincode();
	DeletePincode();
	AddPincode();
	LockUnlock();
	DeviceAssignPropertySave();
	ImportAction();
	SmatthingsSaveEmail();

	function ImportAction() {
		$("#import-from-vera").click(function () {
			changeSiteType("Vera");
			$("#device-modal-title").text("Log In Your " + siteType + " Account");
			$("#login-modal-account").show();
			$("#device-modal-site-type").hide();
			$('#login-modal-smartthings').hide();
			$('#blink-security').hide();
		});
		$("#import-from-arlo").click(function () {
			changeSiteType("Arlo");
			$("#device-modal-title").text("Log In Your " + siteType + " Account");
			$("#login-modal-account").show();
			$("#device-modal-site-type").hide();
			$('#login-modal-smartthings').hide();
			$('#blink-security').hide();
		});
		$("#import-from-blink").click(function () {
			changeSiteType("Blink");
			$("#device-modal-title").text("Log In Your " + siteType + " Account");
			$("#login-modal-account").show();
			$("#device-modal-site-type").hide();
			$('#login-modal-smartthings').hide();
			$('#blink-security').hide();
		});
		$("#import-from-wyze").click(function () {
			changeSiteType("Wyze");
			$("#device-modal-title").text("Log In Your " + siteType + " Account");
			$("#login-modal-account").show();
			$("#device-modal-site-type").hide();
			$('#login-modal-smartthings').hide();
			$('#blink-security').hide();
		});
		$("#import-from-smartthings").click(function () {
			changeSiteType("Smartthings");
			$("#device-modal-title").text("Log In Your " + siteType + " Account");
			$("#login-modal-account").hide();
			$("#device-modal-site-type").hide();
			$('#login-modal-smartthings').show();

		});
		$("#back-device-login").click(function () {
			changeSiteType("");
			$("#login-modal-account").hide();
			$("#device-modal-site-type").show();
			$('#device-modal-title').text('Device Login');
		});
	}
	function changeSiteType(type) {
		siteType = type;
		$("#site-type").val(type);
	}

	function SmatthingsSaveEmail() {
		$(document).on('click', '#smartthings-login', function () {
			$.ajax({
				type: 'POST',
				url: "/Devices/SmartthingsSaveEmail",
				async: false,
				data: {
					email: $("#email-smartthings").val()
				},
				dataType: "json",
				success: function (data) {
					if (data.success) {
						$('#device-login-modal').modal('hide');
					}
				}
			});
		});
	}
	function Login() {
		$(document).on('click', '#add-account-button', function () {
			$("#login-modal-account").hide();
			$("#device-modal-site-type").show();
			$('#login-modal-smartthings').hide();
			$('#blink-security').hide();
			$('#device-modal-title').text('Device Login');
			$('#device-login-modal').modal({ closable: false }).modal('show').modal('refresh');
		});
	}
	function LoginSubmit() {
		$(document).on('click', '#login-btn', function () {
			var siteController = siteType == "Devices" ? "Devices" : "Video";
			$.ajax({
				type: 'POST',
				url: "/" + siteController + "/Login",
				async: false,
				data: {
					username: $('#username').val(),
					password: $('#password').val(),
					site: siteType
				},
				dataType: "json",
				success: function (data) {
					isSuccess = data.success;
					if (data.hasSecurity) {
						$("#login-modal-account").hide();
						$('#blink-security').show();
						$('#blink-security').attr('accountId', data.localId);
					}
					else if (data.success) {
						toastr.success("Downloading Devices", null, { timeOut: 3000, positionClass: "toast-bottom-right" });
						LoadDevices();
						$('#device-login-modal').modal("hide");
					} else {
						toastr.error("Invalid Credentials", null, { timeOut: 3000, positionClass: "toast-bottom-right" });
					}
				},
				error: function (error) {
					toastr.error("Invalid Credentials", null, { timeOut: 3000, positionClass: "toast-bottom-right" });
				}
			});
		});
	}

	function DeletePincode() {
		$(document).on('click', '.pincode-delete', function () {
			var pincodeId = $(this).closest('tr').attr('pincode-id');
			var lockId = $(this).closest('tr').attr('lock-id');
			$.ajax({
				type: "POST",
				data: {
					pincodeId: pincodeId,
					lockId: lockId
				},
				url: '/Devices/DeletePincode',
				success: function (data) {
					if (data.success) {
						var pincode = data.pincodes;
						$('#pincode-table').children('tbody').empty();
						for (var x = 0; x < pincode.length; x++) {
							$('#pincode-table').children('tbody').append('<tr pincode-id="' + pincode[x].PinId + '" lock-id="' + lockId + '">' +
								'<td>' + pincode[x].PinName + '</td>' +
								'<td>' + pincode[x].PinCode + '</td>' +
								'<td><button class="ui mini button pincode-delete" data-tooltip="Delete"><i class="icon trash"></i></button></td>' +
								'</tr>');
						}
						$('#pincode-modal').modal('show').modal('refresh');
					}
				}
			});
		});
	}
	function LockUnlock() {
		$(document).on('click', '.lock-unlock', function () {
			var isLock = $(this).attr('islock');
			var lockId = $(this).closest('tr').attr('lock-id');
			var site = $(this).closest('tr').attr('site');
			$('#pincode-modal').attr('lock-id', lockId);
			$.ajax({
				type: "GET",
				data: {
					lockId: lockId,
					islock: isLock,
					siteType: site
				},
				url: '/Devices/LockUnlock',
				success: function (data) {
					if (data.success) {
						alert(isLock ? "Succesfully Lock!" : "Succesfully Unlock!");
					}
				}
			});
		});
	}
	function AddPincode() {
		$(document).on('click', '.pincode-add', function () {
			var lockId = $('#pincode-modal').attr('lock-id');
			$('#pincode-modal').attr('lock-id', lockId);
			$.ajax({
				type: "GET",
				data: {
					name: $('#pincode-name').val(),
					code: $('#pincode').val(),
					lockId: lockId
				},
				url: '/Devices/AddPincode',
				success: function (data) {
					if (data.success) {
						var pincode = data.pincodes;
						$('#pincode-table').children('tbody').empty();
						for (var x = 0; x < pincode.length; x++) {
							$('#pincode-table').children('tbody').append('<tr pincode-id="' + pincode[x].PinId + '" lock-id="' + lockId + '">' +
								'<td>' + pincode[x].PinName + '</td>' +
								'<td>' + pincode[x].PinCode + '</td>' +
								'<td><button class="ui mini button pincode-delete" data-tooltip="Delete"><i class="icon trash"></i></button></td>' +
								'</tr>');
						}
						$('#pincode-name').val('');
						$('#pincode').val('');
						$('#pincode-modal').modal('refresh');
					}
				}
			});
		});
	}
	function GetPincode() {
		$(document).on('click', '.pincode-btn', function () {
			var lockId = $(this).closest('tr').attr('lock-id');
			$('#pincode-modal').attr('lock-id', lockId);
			$.ajax({
				type: "GET",
				data: {
					lockId: lockId
				},
				url: '/Devices/GetPincodes',
				success: function (data) {
					if (data.success) {
						$('#pincode-name').val('');
						$('#pincode').val('');
						var pincode = data.pincodes;
						$('#pincode-table').children('tbody').empty();
						for (var x = 0; x < pincode.length; x++) {
							$('#pincode-table').children('tbody').append('<tr pincode-id="' + pincode[x].PinId + '" lock-id="' + lockId + '">' +
								'<td>' + pincode[x].PinName + '</td>' +
								'<td>' + pincode[x].PinCode + '</td>' +
								'<td><button class="ui mini button pincode-delete" data-tooltip="Delete"><i class="icon trash"></i></button></td>' +
								'</tr>');
						}
						$('#pincode-modal').modal('show').modal('refresh');
					}
				}
			});
		});
	}

	function DeviceAssignPropertySave() {
		$(document).on('change', '.property-assign', function () {
			var divtitle = $(this).closest('.title');
			var deviceId = divtitle.attr('deviceId');
			var propertyId = $(this).dropdown('get value');
			$.ajax({
				type: "POST",
				data: {
					propertyId: propertyId,
					deviceId: deviceId
				},
				url: '/Devices/DeviceAssignProperty',
				success: function (data) {
					if (data.success) {
						//swal("Device assign to property.", "", "success");
						toastr.success("Device assign to property.", null, { timeOut: 5000, positionClass: "toast-bottom-right" });
					}
				}
			});
		});

	} function GetSite(property) {
		if (property.IsParentProperty) {
			return "(Parent)";
		}
		else if (property.SiteType == 0) { return "(Local)"; }
		else if (property.SiteType == 1) { return "(Airbnb)"; }
		else if (property.SiteType == 2) { return "(Vrbo)"; }
		else if (property.SiteType == 3) { return "(Booking.com)"; }
		else if (property.SiteType == 4) { return "(Homeaway)"; }
	}
	function GetDeviceSite(siteType) {
		switch (siteType) {
			case 0: return "Arlo";
			case 1: return "Blink";
			case 2: return "Vera";
			case 3: return "Smartthings";
			case 4: return "Wyze";
		}
	}

	function SetDeviceRefresh() {

		deviceRefresh = setTimeout(function () { LoadDevices(); }, 10000);
		clearTimeout(deviceRefresh);
	}
	function LoadDevices() {
		$.ajax({
			type: "GET",
			url: '/Devices/GetDevices',
			success: function (data) {
				if (data.success) {
					$('#device-div').empty();
					var accounts = data.accounts;
					var properties = data.properties;
					var propertiesHtml = '';
					for (var i = 0; i < properties.length; i++) {
						propertiesHtml += '<option value="' + properties[i].ListingId + '">' + properties[i].Name + GetSite(properties[i]) + '</option>';
					}
					for (var x = 0; x < accounts.length; x++) {
						var deviceHtml = '';
						for (var d = 0; d < accounts[x].Devices.length; d++) {
							var lockHtml = '';
							for (var i = 0; i < accounts[x].Devices[d].Locks.length; i++) {
								lockHtml += '<tr lock-id="' + accounts[x].Devices[d].Locks[i].Id + '">' +
									'<td>' + accounts[x].Devices[d].Locks[i].Name + '</td>' +
									'<td>' + (accounts[x].Devices[d].Locks[i].IsLock ? '<button class="ui mini button pincode-btn" data-tooltip="Pin Code List"><i class="icon clipboard list"></i></button>' +
										'<button class="ui mini button lock-unlock" site=' + accounts[x].Account.Site + ' data-tooltip="Unlock" islock="false"><i class="icon unlock open"></i></button>' +
										'<button class="ui mini button lock-unlock" site=' + accounts[x].Account.Site + ' data-tooltip="Lock" islock="true"><i class="icon lock"></i></button>' : '') + '</td>' +
									'</tr>';
							}

							deviceHtml += '<div class="accordion" style="width:100%">' +
								'<div class="title" deviceId="' + accounts[x].Devices[d].Id + '">' +
								'<div class="ui form">' +
								'<div class="fields">' +
								'<div class="two wide field">' +
								(accounts[x].Devices[d].Locks.length > 0 ? '<i class="dropdown icon"></i>' : '') +
								accounts[x].Devices[d].Name + ' ' + accounts[x].Devices[d].DeviceId +
								'</div>' +
								'<div class="six wide field">' +
								'<select class="ui search dropdown property-assign">' +
								(accounts[x].Devices[d].Property != null ? '<option selected value="' + accounts[x].Devices[d].Property.ListingId + '">' + accounts[x].Devices[d].Property.Name + GetSite(accounts[x].Devices[d].Property) + '</option>' : '<option value="">Select Property</option>') +
								propertiesHtml +
								'</select>' +
								'</div>' +
								'</div>' +
								'</div>' +
								'</div>' +
								(accounts[x].Devices[d].Locks.length > 0 ? '<div class="content" >' +
									'<table class="ui celled red table lock-table" cellspacing="0" style="width: 100% !important">' +
									'<thead>' +
									'<tr>' +
									'<th>Name</th>' +
									'<th>Action</th>' +
									'</tr>' +
									'</thead>' +
									'<tbody>' +
									lockHtml +
									'</tbody>' +
									'</table>' +
									'</div>' : '') +
								'</div>';
						}
						$('#device-div').append('<div class="ui styled accordion" style="width:100%">' +
							'<div class="title">' +
							'<i class="dropdown icon"></i>' +
							(accounts[x].Account.Username + ' - ' + GetDeviceSite(accounts[x].Account.SiteType)) +
							'</div>' +
							'<div class="content">' +
							deviceHtml +
							'</div>' +
							'</div>');
					}

					//var devices = data.devices;
					//var properties = data.properties;
					//var propertiesHtml = '';
					//for (var i = 0; i < properties.length; i++) {
					//	propertiesHtml += '<option value="' + properties[i].ListingId + '">' + properties[i].Name + '</option>';
					//}
					//for (var x = 0; x < devices.length; x++) {

					//	var lockHtml = '';
					//	for (var i = 0; i < devices[x].Locks.length; i++) {
					//		lockHtml += '<tr lock-id="' + devices[x].Locks[i].Id +'">'+
					//			'<td>' + devices[x].Locks[i].Name + '</td>' +
					//			'<td>' + (devices[x].Locks[i].IsLock ? '<button class="ui mini button pincode-btn" data-tooltip="Pin Code List"><i class="icon clipboard list"></i></button>': '') + '</td>' +
					//				   '</tr>';
					//	}

					//}

					//$('.ui.dropdown').dropdown();
					$('.ui.accordion').accordion();
				}
			}
		});
	}
});
