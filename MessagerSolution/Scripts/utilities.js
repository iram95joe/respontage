﻿var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];


function GetBoolValue(value)
{
    if (value == "on") {
        return true;
    }
    return false;
}

function GetBoolNumber(value) {
    if (value) {
        return 1;
    }
    return 0;
}

function GetBoolSyntax(value) {
    if (value == 1) {
        return true;
    }
    return false;
}

function GetFrequencyValue(value) {
    switch (value) {
        case "daily": return 0;
        case "weekly": return 1;
        case "monthly": return 2;
        case "quarterly": return 3;
        case "year": return 4;
        default: return 0;
    }
}

function GetFrequencyText(frequencyValue) {
    switch (frequencyValue) {
        case 0: return "daily";
        case 1: return "weekly";
        case 2: return "monthly";
        case 3: return "quarterly";
        case 4: return "year";
        default: return "daily";
    }
}

function GetMonthValue(value) {
    switch(value) {
        case "jan": return 1;
        case "feb": return 2;
        case "mar": return 3;
        case "apr": return 4;
        case "may": return 5;
        case "june": return 6;
        case "jul": return 7;
        case "aug": return 8;
        case "sep": return 9;
        case "oct": return 10;
        case "nov": return 11;
        case "dec": return 12;
        default : return 0;
    }
}

function GetMonthText(value) {
    switch (value) {
        case 1: return "jan";
        case 2: return "feb";
        case 3: return "mar";
        case 4: return "apr";
        case 5: return "may";
        case 6: return "june";
        case 7: return "jul";
        case 8: return "aug";
        case 9: return "sep";
        case 10: return "oct";
        case 11: return "nov";
        case 12: return "dec";
        default : return "jan";
    }
}

//function formatDateTimeDisplay(dateTimeValue) {
//    var validDateTime = new Date(parseInt(dateTimeValue.substr(6)));

//    return monthNames[validDateTime.getMonth()] + " " + validDateTime.getDate() + ", " + validDateTime.getFullYear();
//}

function GetMonthDigit(dateValue) {
    var date = new Date(dateValue);
    return date.getMonth() + 1;
}

function GetPropertyValue(value) {
    var number;
    switch (value) {
        case "yaletown-park": number = 1; break;
        case "firenze": number = 2; break;
        case "false-creekside": number = 3; break;
        case "chinatown-one": number = 4; break;
        case "woodrose-manor": number = 5; break;
        case "the-citadel": number = 6; break;
        case "one-granville": number = 7; break;
        case "tv-towers": number = 8; break;
        default: number = 1; break;
    }
return number - 1;
}

function isValueNull(value) {
    if (value == "" || value == null || value == undefined) {
        return true;
    }

    return false;
}

function GetStatusNumber(value) {
    if (value == "new") {
        return 0;
    }
    return 1;
}


//var monthNames = ["January", "February", "March", "April", "May", "June",
//  "July", "August", "September", "October", "November", "December"
//];

function formatDateTimeDisplay(dateTimeValue) {
    var validDateTime = new Date(parseInt(dateTimeValue.substr(6)));
    return (validDateTime.getMonth() + 1) + "-" + validDateTime.getDate() + "-" + validDateTime.getFullYear();
}

function LaunchEditPopup() {
    toUpdateCashModal = true;
    $('.cash-flow-modal').modal('show');

    //initialize calendar on modal show
    $('.ui.calendar').calendar({
        type: 'date'
    });
}

function formatDateTimeDisplayText(dateTimeValue) {
    var validDateTime = new Date(parseInt(dateTimeValue.substr(6)));
    return monthNames[validDateTime.getMonth()] + " " + validDateTime.getDate() + ", " + validDateTime.getFullYear();
}

