﻿$(document).ready(function () {
	var tableEmail;
	LoadEmailTemplates();
	AddEmailTemplate();
	SaveTemplate();
	EditTemplate();
	function AddEmailTemplate() {
		$(document).on('click', '#create-email-template', function (e) {
			var documentTemplateModal = $('#email-template-modal');
			documentTemplateModal.find('#email-template-name').val('');
			documentTemplateModal.attr('email-template-id', '0');
			documentTemplateModal.find('#email-template-content').empty();
			$('#email-template-content').val('');
			documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

		});
	}
	function LoadEmailTemplates() {
		tableEmail = $('#email-template-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Payment/GetEmailTemplates",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Description" },
				{ "data": "Actions" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-email-template-id', data.Id);
			}
		});
	}
	function SaveTemplate() {
		$(document).on('click', '#save-email-template', function (e) {
			var formData = new FormData();

			formData.append("Id", $('#email-template-modal').attr('email-template-id'));
			formData.append("Name", $('#email-template-name').val());
			formData.append("Description", $('#email-template-content').text());
			$.ajax({
				type: 'POST',
				url: "/Payment/SaveEmailTemplate",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						tableEmail.ajax.reload(null, false);
						$('#email-template-modal').modal("hide");

					}
					else {
						swal("Error occured on deleting payment method", "", "error");
					}
				},
				error: (error) => {
					console.log(JSON.stringify(error));
				}
			});
		});
	}
	function EditTemplate() {
		$(document).on('click', '.edit-email-template', function (e) {
			var id = $(this).parents('tr').attr('data-email-template-id');
			$.ajax({
				type: 'GET',
				url: "/Payment/GetEmailTemplate",
				dataType: "json",
				data: { id: id },
				success: function (data) {
					var template = data.template
					var documentTemplateModal = $('#email-template-modal');
					documentTemplateModal.find('#email-template-name').val(template.Name);
					documentTemplateModal.attr('email-template-id', template.Id);
					documentTemplateModal.find('#email-template-content').text("");
					documentTemplateModal.find('#email-template-content').append(template.Description.replace(/\[/g, '<i class="icon tags"></i>['));
					documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');
				}
			})
		});
		
	}

	$("#email-template-variable").on('change', function () {
		var variable = $(this).children(':selected').val();//.replace('[', '').replace(']', '');
		$('#email-template-content').append('<i class="icon tags"></i>' + variable);
	});

})