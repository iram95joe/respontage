﻿function changePrice(e) {

    var id = e.value;
    var date = e.getAttribute("data-date");
    var price = document.getElementById("price-" + id).getAttribute("value");
    var latitude = document.getElementById("latitude").value;
    var longitude = document.getElementById("longitude").value;
    var bedrooms = document.getElementById("bedrooms").value;
    var priceRuleBasis = document.getElementById("priceRuleBasis").value;
    var priceRuleCondition = document.getElementById("priceRuleCondition").value;
    var priceRuleConditionPercentage = document.getElementById("priceRuleConditionPercentage").value;
    var priceRuleAmount = document.getElementById("priceRuleAmount").value;
    var priceRuleSign = document.getElementById("priceRuleSign").value;
    var priceRuleAveragePriceBasis = document.getElementById("priceRuleAveragePriceBasis").value;

    var oldPrice = price;
    var averagePriceUsed = 0;
    var computedPriceRuleAmount = priceRuleAmount;

    document.getElementById("price-" + id).innerHTML = "Getting Analytics...";

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var json = JSON.parse(this.responseText);

            var result =
                "Availability Rate By Date : " + json.analytics.AvailabilityRateByDate + " %\n\n" +
                "Availability Rate By Date for x Rooms : " + json.analytics.AvailabilityRateByDateForXRooms + " %\n\n" +
                "Availability Rate By Month : " + json.analytics.AvailabilityRateByMonth + " %\n\n" +
                "Availability Rate By Month for x Rooms : " + json.analytics.AvailabilityRateByMonthForXRooms + " %\n\n" +
                "Average Price By Date : " + json.analytics.AveragePriceByDate + "\n\n" +
                "Average Price By Date for x Rooms : " + json.analytics.AveragePriceByDateForXRooms + "\n\n" +
                "Average Price By Month : " + json.analytics.AveragePriceByMonth + "\n\n" +
                "Average Price By Month for x Rooms : " + json.analytics.AveragePriceByMonthForXRooms + "\n\n" +
                "Occupancy Rate By Date : " + json.analytics.OccupancyRateByDate + " %\n\n" +
                "Occupancy Rate By Date for x Rooms : " + json.analytics.OccupancyRateByDateForXRooms + " %\n\n" +
                "Occupancy Rate By Month: " + json.analytics.OccupancyRateByMonth + " %\n\n" +
                "Occupancy Rate By Month for x Rooms : " + json.analytics.OccupancyRateByMonthForXRooms + " %\n\n" +
                "Property Count By Date : " + json.analytics.PropertyCountByDate + "\n\n" +
                "Property Count By Date for x Rooms : " + json.analytics.PropertyCountByDateForXRooms + "\n\n" +
                "Property Count By Rooms : " + json.analytics.PropertyCountByRooms;

            alert(result);

            document.getElementById("price-" + id).innerHTML = "Saving to database...";

            var rate = 0;

            switch (parseInt(priceRuleBasis)) {
                case 0:
                    rate = json.analytics.OccupancyRateByDate;
                    break;

                case 1:
                    rate = json.analytics.OccupancyRateByDateForXRooms;
                    break;

                case 2:
                    rate = json.analytics.OccupancyRateByMonth;
                    break;

                case 3:
                    rate = json.analytics.OccupancyRateByMonthForXRooms;
                    break;

                case 4:
                    rate = json.analytics.AvailabilityRateByDate;
                    break;

                case 5:
                    rate = json.analytics.AvailabilityRateByDateForXRooms;
                    break;

                case 6:
                    rate = json.analytics.AvailabilityRateByMonth;
                    break;

                case 7:
                    rate = json.analytics.AvailabilityRateByMonthForXRooms;
                    break;

                default:
                    rate = rate;
            }

            switch (parseInt(priceRuleAveragePriceBasis)) {
                case 0:
                    price = json.analytics.AveragePriceByDate;
                    break;

                case 1:
                    price = json.analytics.AveragePriceByDateForXRooms;
                    break;

                case 2:
                    price = json.analytics.AveragePriceByMonth;
                    break;

                case 3:
                    price = json.analytics.AveragePriceByMonthForXRooms;
                    break;

                default:
                    price = price;
            }

            averagePriceUsed = price;

            if (priceRuleCondition == 0) {
                if (rate >= priceRuleConditionPercentage) {
                    if (priceRuleSign == 0) {
                        computedPriceRuleAmount = parseFloat(parseFloat((price / 100) * priceRuleAmount).toFixed(2));
                        price = parseFloat(price) + computedPriceRuleAmount;
                    }
                    else {
                        computedPriceRuleAmount = parseFloat(parseFloat(priceRuleAmount).toFixed(2));
                        price = parseFloat(price) + computedPriceRuleAmount;
                    }
                }
                else {
                    computedPriceRuleAmount = parseFloat(price).toFixed(2);
                    price = parseFloat(parseFloat(price).toFixed(2));
                }
            }
            else if (priceRuleCondition == 1) {
                if (rate <= priceRuleConditionPercentage) {
                    if (priceRuleSign == 0) {
                        computedPriceRuleAmount = parseFloat(parseFloat((price / 100) * priceRuleAmount).toFixed(2));
                        price = parseFloat(price) + computedPriceRuleAmount;
                    }
                    else {
                        computedPriceRuleAmount = parseFloat(parseFloat(priceRuleAmount).toFixed(2));
                        price = parseFloat(price) + computedPriceRuleAmount;
                    }
                }
                else {
                    computedPriceRuleAmount = parseFloat(parseFloat(price).toFixed(2));
                    price = parseFloat(parseFloat(price).toFixed(2));
                }
            }

            $.ajax({
                type: "POST",
                url: "/Calendar/UpdateSpecificDate",
                data: {
                    id: parseInt(id),
                    price: price
                },
                success: function (result) {

                    if (result.success = true) {
                        if (parseFloat(parseFloat(oldPrice).toFixed(2)) != parseFloat(parseFloat(result.price).toFixed(2))) {
                            document.getElementById("computation-" + id).innerHTML =
                                "<div style='margin-left: 50px;'>" +
                                "Old Price : " + oldPrice + "<br /><br />" +
                                "Rate (Occupancy or Availability) : " + rate + " %<br /><br />" +
                                "Average Price : " + averagePriceUsed + "<br /><br />" +
                                "Plus or Minus : " + (priceRuleAmount.includes("-") ? "MINUS" : "PLUS") + "<br /><br />" +
                                "Price Rule Amount : " + priceRuleAmount + " " + (priceRuleSign == 0 ? "%" : "$") + "<br /><br />" +
                                "Price Rule Amount (Computed): " + computedPriceRuleAmount + "<br /><br />" +
                                "Computation : " + averagePriceUsed + (priceRuleAmount.includes("-") ? " - " : " + ") + (String(computedPriceRuleAmount).replace("-", "")) + " = " + parseFloat(result.price).toFixed(2) + "<br /><br />"
                            "</div>";
                        }

                        document.getElementById("price-" + id).innerHTML = parseFloat(parseFloat(result.price).toFixed(2));
                    }
                    else {
                        document.getElementById("price-" + id).innerHTML = "Failed on saving to database...";
                    }

                },
                error: function (errormessage) {
                    alert(errormessage.responseText);
                }
            });
        }
    };

    var url = "http://149.28.59.242/api/airbnb/getanalytics?" +
        "latitude=" + latitude + "&longitude=" + longitude + "&date=" + date + "&propertycountgetavailable=true" +
        "&averagepriceonlyavailable=false&rooms=" + bedrooms;

    xhttp.open("GET", url, true);
    xhttp.send();
}