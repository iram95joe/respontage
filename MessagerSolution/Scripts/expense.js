﻿// initialization scripts

$(document).ready(function () {
    var tableExpense;
	var tableExpenseCategory;
	var tablePayment;
    var postType = 0;
	var expenseCategoryPostType = 0;
	var feeTypePostType = 0;
	var paymentAction = 0;
	var paymentId = 0;
	var tablePaymentMethod;
    initialize();

    function initialize() {
        setupSemanticUI();
        setEventListeners();
        LoadExpenseTable();
        LoadExpenseCategoryTable();
		//UpdateExpenseFromAirbnb();
		onAddFeeType();
		LoadFeeTypeTable();
		onFilterByFeeType();
		onFeeTypeSubmit();
		onEditFeeType();
		editPayment();
		onDeleteFeeType();
		//batch payment
		BatchPaymentButton();
		LoadPaymentTable();
		AllocatePayment();
		//PaymentMethod
		LoadPaymentMethodTable();
		CreatePaymentMethodClick();
		CreatePaymentMethodSubmit();
		DeletePaymentMethod();
		EditPaymentMethod();
		OnchangeExpenseImage();
		OnchangeExpensePaymentImage();
		RemoveFile();
	}

	function RemoveFile() {
		$(document).on('click', '#expense-payment-proof-upload .icon.close', function () {
			$(this).parent().remove();
		});
		$(document).on('click', '#expense-proof-upload .icon.close', function () {
			$(this).parent().remove();
		});
	}
	function OnchangeExpenseImage() {
		$(document).on('change', '#expense-proof-files', function (e) {
			$('#expense-proof-upload').empty();
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				var type = this.files[i]['type'].split('/')[0];
				if (type == 'image') {
					$('#expense-proof-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"></i></i><img src="' + fileUrl + '"><div>');
				}
				else {
					$('#expense-proof-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"><a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a></div>');
				}
			}
		});
	}
	function OnchangeExpensePaymentImage() {
		$(document).on('change', '#expense-payment-proof-files', function (e) {
			$('#expense-payment-proof-upload').empty();
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				var type = this.files[i]['type'].split('/')[0];
				if (type == 'image') {
					$('#expense-payment-proof-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"><img src="' + fileUrl + '"><div>');
				}
				else {
					$('#expense-payment-proof-upload').append('<div file-id="0" class="ui small image"><i class="close icon" style="cursor: pointer;"><a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a></div>');
				}
			}
		});
	}
	function AllocatePayment() {
		$(document).on('click', '.allocate-btn', function () {
			var id = $(this).parents('tr').attr("batch-payment-id");
			$.ajax({
				type: "GET",
				url: '/Expense/GetBatchPayment',
				data: { id: id },
				dataType: "json",
				success: function (data) {
					$('#batch-payment-id').text('Receipt Number: ' + data.batchpayment.Id);
					$('#batch-payment-id').show();
					$('#batch-id').val(data.batchpayment.Id);
					$('#batch-payment-amount').val(data.batchpayment.Amount).keyup();
					$('#batch-payment-description').val(data.batchpayment.Description);
					$('#batch-payment-date').val(moment.utc(data.batchpayment.PaymentDate).format("MMMM D, YYYY"));
					$('.batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
					$('.ui.calendar.date').calendar({
						type: 'date',
					});
					$('#paid-expense').empty();
					$('#unpaid-expense').empty();
					$('#batch-image-upload').empty();
					var images = data.images;
					for (var x = 0; x < images.length; x++) {
						var image = images[x];
						$('#batch-image-upload').append((ImageExist(image) ? '<div class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
					}
					var incomingpayment = data.incomingpayment;
					for (var x = 0; x < incomingpayment.length; x++) {
						$('#paid-expense').append('<div class="fields expense"><div class="five wide field"><p class="expense-id" expense-id="' + incomingpayment[x].ExpenseId + '">' + incomingpayment[x].Description + " " + moment.utc(incomingpayment[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + incomingpayment[x].Balance + '</b></p></div>' +
							'<div class="six wide field">' +
							'<input type="number" min="0" max="' + incomingpayment[x].Balance + '"  payment-id="' + incomingpayment[x].ExpensePaymentId + '" value="' + incomingpayment[x].Amount + '" class="expense-payment-amount" placeholder="Payment Amount " />' +
							'</div></div>');
					}
					//for (var x = 0; x < data.monthlyrent.length; x++) {
					//	if (!$('[income-id="' + data.monthlyrent[x].Id + '"]').length > 0) {
					//		$('#unpaid-expense').append('<div class="fields monthly-rent"><div class="five wide field"><p class="expense-id" expense-id="' + data.monthlyrent[x].Id + '">' + data.monthlyrent[x].Description + " " + moment.utc(data.monthlyrent[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + data.monthlyrent[x].Amount + '</b></p></div>' +
					//			'<div class="six wide field">' +
					//			'<input type="number"  min="0" max="' + data.monthlyrent[x].Balance + '" payment-id="0" class="income-payment-amount" required placeholder="Payment Amount " />' +
					//			'</div></div>');
					//	}
					//}
				}
			});
		});
	}

	function EditPaymentMethod() {
		$(document).on('click', '.edit-payment-method', function () {
			var id = $(this).parents('tr').attr('payment-method-id');
			$.ajax({
				type: 'GET',
				url: "/Expense/GetPaymentMethod",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						$('#payment-method-id').val(data.paymentMethod.Id);
						$('#payment-method-name').val(data.paymentMethod.Name);
						$('#create-payment-method-modal').modal({ closable: false }).modal('show').modal('refresh');
					}
				}
			});
		});
	}
	function DeletePaymentMethod() {
		$(document).on('click', '.delete-payment-method', function () {
			var id = $(this).parents('tr').attr('payment-method-id');
			swal({
				title: "Are you sure you want to remove this Payment Method?",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Remove",
				showCancelButton: true,
				closeOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Expense/DeletePaymentMethod",
						data: { id: id },
						success: function (data) {
							if (data.success) {
								tablePaymentMethod.ajax.reload(null, false);
							}
						}
					});
				});
		
		});
	}
	function CreatePaymentMethodSubmit() {
		$(document).on('click', '#payment-method-submit-btn', function () {
			var id = $('#payment-method-id').val();
			var name = $('#payment-method-name').val();
			$.ajax({
				type: 'POST',
				url: "/Expense/CreatePaymentMethod",
				data: { id: id, name: name },
				success: function (data) {
					if (data.success) {
						swal({ title: "Success", text: "Payment Method Saved!", type: "success" });
						tablePaymentMethod.ajax.reload(null, false);
						$('#create-payment-method-modal').modal('hide');
						GetPaymentMthod()
					}
					else {
						swal("Error occured on deleting fee type", "", "error");
					}
				}
			});
		});
	}
	function CreatePaymentMethodClick() {
		$(document).on('click', '#payment-method-btn', function () {
			$('#payment-method-id').val('0');
			$('#payment-method-name').val('');
			$('#create-payment-method-modal').modal({ closable: false }).modal('show').modal('refresh');
		});
	}
	function BatchPaymentButton() {
		$(document).on('click', '.batch-payment-btn', function () {
			$('#proof-pic').val('');
			$('#batch-image-upload').empty();
			$('#batch-payment-description').val('');
			$('#batch-payment-amount').val('');
			$('#batch-payment-date').val('');
			$('#batch-id').val('0');
			$('#batch-payment-remaining').text('0');
			$('#unpaid-expense').empty();
			$('#paid-expense').empty();
			$('.batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
			$('.ui.calendar.date').calendar({
				type: 'date'
			});
		});
	}

	function onDeleteFeeType() {
		$(document).on('click', '.delete-fee-type-btn', function () {
			var id = $(this).parents('tr').attr('data-fee-type-id');
			if (id != '') {
				swal({
					title: "Are you sure you want to delete this fee type ?",
					text: "",
					type: "warning",
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes",
					showCancelButton: true,
					closeOnConfirm: false,
					showLoaderOnConfirm: true,
				},
					function () {
						$.ajax({
							type: 'POST',
							url: "/Expense/DeleteFeeType",
							data: { id: id },
							success: function (data) {
								if (data.success) {
									swal({ title: "Success", text: "Payment successfully delete.", type: "success" },
										function () {
											RefreshFeeTypeList();
										});
								}
								else
								{
									swal("Error occured on deleting fee type", "", "error");
								}
							}
						});
					});
			}
		});
	}
	function onEditFeeType() {
		$(document).on('click', '.edit-fee-type-btn', function () {
			$('#expense-category-id').val('');
			$('#category-name').val('');
			$('#type').val(1);

			var id = $(this).parents('tr').attr("data-fee-type-id");
			$.ajax({
				type: "POST",
				url: "/Expense/FindFeeTypeById",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						$('#fee-type-id').val(data.fee_type.Id);
						$('#fee-type').val(data.fee_type.Fee_Type);
						$('#type').val(data.fee_type.Type);
						$('#create-fee-type-modal').modal({ closable: false }).modal('show').modal('refresh');
						feeTypePostType = 2;
					}
				}
			});
		});
	}
	function onFeeTypeSubmit() {
		$(document).on('click', '#fee-type-submit-btn', function () {
			var Id = $('#fee-type-id').val();
			var feeType = $('#fee-type').val();
			var type = $('#type').val();

		
			if (feeType != '') {
				if (feeTypePostType == 1) // add
				{
					$.ajax({
						type: "POST",
						url: "/Expense/CreateFeeType",
						data: {
							feeType: feeType,
							type: type,
						},
						success: function (data) {
							if (data.success) {
								$('#create-fee-type-modal').modal('hide');
								swal({ title: "Success", text: "Fee type has been saved.", type: "success" },
									function () {
										$('#fee-type').val('');
										RefreshFeeTypeList();
									});
							}
							else {
								$('#create-fee-type-modal').modal('hide');
								swal("Error on add fee type", "", "error");
							}
						}
					});
				}
				else if (feeTypePostType == 2) // edit
				{
					if (Id != '') {
						$.ajax({
							type: "POST",
							url: "/Expense/EditFeeType",
							data: {
								Id: Id,
								Fee_Type: feeType,
								Type: type,
							},
							success: function (data) {
								if (data.success) {
									$('#create-fee-type-modal').modal('hide');
									swal({ title: "Success", text: "Changes to fee type has been saved.", type: "success" },
										function () {
											$('#fee-type').val('');
											RefreshFeeTypeList();
										});
								}
								else {
									$('#create-fee-type-modal').modal('hide');
									swal("Error on edit fee type", "", "error");
								}
							}
						});
					}
				}
			}
		});
	}
	function onFilterByFeeType() {
		$("#fee-type-filter").on('change', function () {
			RefreshFeeTypeList();
		});
	}

	function RefreshFeeTypeList() {
		tableFeeType.ajax.reload(null, false);
	}
	function LoadPaymentTable() {
		tablePayment = $('#payment-table').DataTable({
			"aaSorting": [],
			"order": [],
			"responsive": true,
			"ajax": {
				"url": "/Expense/GetExpensePayments",
				"type": 'GET',
				"data": function () {
					return {
						type: $('#fee-type-filter').val()
					};
				}
			},
			"columns": [
				{ "data": "Description" },
				{ "data": "Amount"},
				{ "data": "ExpensesPaid" },
				{ "data": "Balance" },
				{ "data": "Status"},
				{ "data": "Date"},
				{ "data": "Action" }
			],
			columnDefs: [{
				targets: 5, type: "date", render: function (data) {
					if (data == null) {
						return '';
					} else {
						return moment(data).format('MMMM D,YYYY');
					}

				}
			}],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('batch-payment-id', data.Id);
			}
		});
	}
	function LoadPaymentMethodTable() {
		tablePaymentMethod = $('#payment-method-table').DataTable({
			//initComplete: function () {
			//	$(".dataTables_wrapper").find('.ui.stackable.grid').css("margin", 0);
			//},
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Expense/GetPaymentMethodList",
				"type": 'GET'
				
			},
			"columns": [
				{ "data": "Name" },
				{"data":"Type"},
				{ "data": "Actions", "orderable": false }


			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('payment-method-id', data.Id);
			}
		});
	}
	function LoadFeeTypeTable() {
		tableFeeType = $('#fee-type-table').DataTable({
			initComplete: function () {
				$(".dataTables_wrapper").find('.ui.stackable.grid').css("margin", 0);
			},
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Expense/GetFeeTypeList",
				"type": 'POST',
				"data": function () {
					return {
						type: $('#fee-type-filter').val()
					}
				}
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Type", "orderable": false },
				{ "data": "Actions", "orderable": false }


			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-fee-type-id', data.Id);
			}
		});
	}
	function onAddFeeType() {
		$('#create-new-fee-type').on('click', function () {
			$('#fee-type-id').val('');
			$('#fee-type').val('');
			$('#type').val(1);
			$('#create-fee-type-modal').modal('show');
			feeTypePostType = 1;
		});
	}
    //function UpdateExpenseFromAirbnb() {
    //    $.ajax({
    //        type: 'POST',
    //        url: "/Expense/UpdateAirbnbExpenseStatus",
    //        success: function (data) {
    //            if (data.success) {
    //                RefreshExpenseList();
    //                setTimeout(function () { UpdateExpenseFromAirbnb(); }, 15000);
    //            }
    //        }
    //    });
    //}

	function setupSemanticUI() {
		$('.ui.accordion').accordion();
		$('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
		$('.ui.checkbox').checkbox();
		$('.ui.calendar.date').calendar({ type: 'date' });
		$('.ui.calendar.month').calendar({
			type: 'month', onChange: function (date, text, mode) {
				$(this).find('input').val(text);
				//alert($(this).val());
				RefreshExpenseList();
			}
		});
        $('.ui.calendar.time').calendar({ type: 'time' });
        $('table td a.table-tooltip').popup();
		$('.tabular.menu .item').tab();
    }

    function setEventListeners() {
        onDisplaySidebar();
        onAddExpense();
        //onApplyFilter();
        onExpenseSubmit();
        onEditExpense();
        onDeleteExpense();
        onFilterByCategoryType();
        onAddExpenseCategory();
        onEditExpenseCategory();
        onDeleteExpenseCategory();
        onExpenseCategorySubmit();
        onGenerateExpense();
        onGenerateExpenseTimeFrameChange();
        onGenerateExpenseSubmit();
        onDownloadSampleExcelFormat();
        onExpensePayment();
        onSubmitExpensePayment();
		//onUpdateStatusExpense();
		onClearFilter();
		deletePayment();
		OnExpenseFilterChange();
		//batch payment
		OnChangeBatchFilter();
		OnchangePaymentImage();
		OnchangeBatchPaymentImage();
		CreateBatchPayment();
		//BatchBalanceKeyup();
		ExpensePaymentAmountKeyup();
		BatchPaymentAmountKeyup();
	}

	function ExpensePaymentAmountKeyup() {
		$(document).on('keyup', '.expense-payment-amount', function () {

		
			var totalAmountToPay = 0;
			$('.expense-payment-amount').each(function (index, element) {
				if (!isNaN(parseFloat($(this).val()))){
					totalAmountToPay = totalAmountToPay + parseFloat($(this).val());
				}
			});

			ValidateTotalPayment($('#batch-payment-amount'), totalAmountToPay);
			var maxValue = $(this).attr('max');
			ValidatePayment($(this), maxValue);
			$('#batch-payment-remaining').text(parseFloat($('#batch-payment-amount').val()) - parseFloat(totalAmountToPay));

		});
	}
	function BatchPaymentAmountKeyup() {
		$(document).on('keyup', '#batch-payment-amount', function () {

			var totalAmountToPay = 0;
			$('.expense-payment-amount').each(function (index, element) {
				if (!isNaN(parseFloat($(this).val()))) {
					totalAmountToPay = totalAmountToPay + parseFloat($(this).val());
				}
			});
			ValidateTotalPayment($(this), totalAmountToPay);
			
			$('#batch-payment-remaining').text(parseFloat($(this).val()) - parseFloat(totalAmountToPay));

		});
	}
	//function BatchBalanceKeyup() {
	//	$(document).on('keyup', '#batch-payment-amount, .expense-payment-amount', function () {
	//		var max = $(this).attr('max');
	//		var total = $('#batch-payment-amount').val();
	//		alert(max + " " + total);
	//		$('.expense-payment-amount').each(function (index, element) {
	//			total = total - $(this).val();
	//		});
	//		if ((parseFloat($(this).val()) > parseFloat(max)) || (parseFloat($(this).val()) > parseFloat(total))) {
	//			ValidatePayment($(this), max);
	//			ValidatePayment($(this), total);
	//		}
	//		else {
	//			ValidatePayment($(this), max);
	//			$('#batch-payment-remaining').text(total);
	//		}
	//	});
	//}
	//function BatchPaymentKeyup() {
	//	$(document).on('keyup', '#batch-payment-amount', function () {
	//		var total = $('#batch-payment-amount').val();
	//		var totalPaid = 0;
	//		$('.expense-payment-amount').each(function (index, element) {
	//			total = total - $(this).val();
	//			totalPaid = totalPaid + $(this).val();
	//			$(this).keyup();
	//		});
	//		ValidateTotalPayment($(this), totalPaid);
	//		$('#batch-payment-remaining').text(total);

	//	});
	//}
	function ValidatePayment(obj, max) {
		if (parseFloat(obj.val()) > parseFloat(max)) {
			obj.next().remove();
			obj.after('<div class="ui basic red pointing prompt label transition visible">Payment must not greater than balance and a number</div>');
			obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		} else {
			obj.next().remove();
		}

	}
	function ValidateTotalPayment(obj, max) {
		if (parseFloat(obj.val()) < parseFloat(max)) {
			obj.next().remove();
			obj.after('<div class="ui basic red pointing prompt label transition visible">Payment must greater than allocated amount</div>');
		} else {
			obj.next().remove();
		}

	}
	function CreateBatchPayment() {
		$(document).on('click', '#save-batch-payment', function (e) {
		
			var Payment = [];
			var isOverpaid = false;
			$('.expense').each(function (index, element) {

				var amount = $(this).find('.expense-payment-amount').val();
				var id = $(this).find('.expense-id').attr('expense-id');
				var paymentId = $(this).find('.expense-payment-amount').attr('payment-id');
				//JM 10/09/19 Check if payment is greater than balance if not don`t save
				//if (parseFloat($(this).find('.expense-payment-amount').attr('max')) < parseFloat(amount)) {
				//	isOverpaid = true;
				//}
				var jsonPayment = {
					ExpenseId: id,
					Amount: amount,
					PaymentId: paymentId
				};
				Payment.push(jsonPayment);

			});
			//if (isOverpaid == false) {
				var formData = new FormData();
				for (var i = 0; i < $('#batch-proof-pic').get(0).files.length; ++i) {
					formData.append("Images[" + i + "]", $('#batch-proof-pic').get(0).files[i]);
				}
				formData.append("batchid", $('#batch-id').val());
				formData.append("description", $('#batch-payment-description').val());
				formData.append("paymentDate", $('#batch-payment-date').val());
				formData.append("amount", $('#batch-payment-amount').val());
				formData.append("payment", JSON.stringify(Payment));
				$.ajax({
					type: "POST",
					url: '/Expense/CreateBatchPayment',
					contentType: false,
					cache: false,
					dataType: "json",
					processData: false,
					data: formData,
					success: function (data) {
						console.log(data);
						if (data.success) {
							$('.batch-payment-modal').modal('hide');
							swal("Payment created!", "", "success");
							tableExpense.ajax.reload(null, false);
							tablePayment.ajax.reload(null, false);
						}
					}
				});
			//}
		});
	}
	function OnExpenseFilterChange() {
		$(document).on('change', '#property-filter,#status-filter,#isIncludeChild', function (e) {
			RefreshExpenseList();
		});
	}
	function OnChangeBatchFilter() {
		$(document).on('change', '#property-expense-filter', function (e) {

			$.ajax({
				type: "GET",
				url: '/Expense/GetUnpaidExpensesByProperty',
					data: { propertyId: $('#property-expense-filter').val()},
				dataType: "json",
				async: false,
				success: function (data) {
					//$('#batch-payment-id').hide();
					//$('#proof-pic').val('');
					//$('#batch-image-upload').empty();
					//$('#batch-payment-description').val('');
					//$('#batch-payment-amount').val('');
					//$('#batch-payment-date').val('');
					//$('#batch-id').val('0');
					$('.batch-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
					$('#unpaid-expense').empty();
					for (var x = 0; x < data.expenses.length; x++) {
						$('#unpaid-expense').append('<div class="fields expense"><div class="five wide field"><p class="expense-id" expense-id="' + data.expenses[x].Id + '">' + data.expenses[x].Description + " " + moment.utc(data.expenses[x].DueDate).format("MMMM D, YYYY") + '<br><b>Balance:' + data.expenses[x].Amount + '</b></p></div>' +
							'<div class="six wide field">' +
							'<input type="number" min="0" max="' + data.expenses[x].Amount + '" payment-id="0" class="expense-payment-amount" placeholder="Payment Amount " />' +
							'</div></div>');
					}
					$('.batch-payment-modal').modal('refresh');
				}
			});
		});
	}
	function OnchangePaymentImage() {
		$(document).on('change', '#proof-pic', function (e) {
			$('#image-upload').empty();
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				var type = this.files[i]['type'].split('/')[0];
				if (type == 'image') {
					$('#image-upload').append('<div class="ui small image"><i class="close icon" style="cursor: pointer;"></i><img src="' + fileUrl + '"><div>');
				}
				else {
					$('#image-upload').append('<a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a>');
				}
			}
		});
	}
	function OnchangeBatchPaymentImage() {
		$(document).on('change', '#batch-proof-pic', function (e) {
			$('#batch-image-upload').empty();
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				var type = this.files[i]['type'].split('/')[0];
				if (type == 'image') {
					$('#batch-image-upload').append('<div class="ui small image"><i class="close icon" style="cursor: pointer;"></i><img src="' + fileUrl + '"><div>');
				}
				else {
					$('#batch-image-upload').append('<div class="ui small image"><i class="close icon" style="cursor: pointer;"><a target="_blank" href="' + fileUrl + '"><i class="large icon file" ></i></a><div>');
				}
			}
		});
	}
	function deletePayment() {
		$(document).on('click', '.delete-payment-btn', function (e) {
			var id = $(this).parents('tr').attr('payment-id');
					$.ajax({
						type: 'POST',
						url: "Expense/DeletePayment",
						data: {
							Id: id
						},
						success: function (data) {
							if (data.success) {
								$('.expense-payment-modal').modal('hide');
								swal({ title: "Success", text: "Payment successfully deleted.", type: "success" },
									function () {
										RefreshFeeTypeList();
									});
							} else
							{
								swal("Error occured on deleting payment", "", "error");
							}
						}
					});
		});
	
	}
	function ImageExist(url) {
		var result = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(url);
		return result;
	}
	function editPayment() {
		$(document).on('click', '.edit-payment-btn', function (e) {
			var id = $(this).parents('tr').attr('payment-id');
			paymentId = id;
			$.ajax({
				type: 'POST',
				url: "Expense/EditPayment",
				data: {
					Id: id
				},
				success: function (data) {
					paymentAction = 1;
					$('.edit-payment-btn').closest('tr').remove();
					$('#expense-payment-amount-name').val(data.payment.Amount);
					$('#ExpensePaymentId').dropdown("set selected",data.payment.Type);
					$('#expense-payment-description-name').val(data.payment.Description);
					$('#payment-date').val(moment(data.payment.CreatedAt).format("MMMM D, YYYY"));
					$('#expense-payment-proof-upload').empty();
					$('#expense-payment-proof-files').val('');
					var files = data.Files;
					for (var x = 0; x < files.length; x++) {
						var image = files[x].Fileurl;
						$('#expense-payment-proof-upload').append((ImageExist(image) ? '<div file-id="' + files[x].Id + '" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
					}
					var remaining = $('.remaining-balance').text();
					$('.remaining-balance').text(parseFloat(remaining) + data.payment.Amount);  

				}
			});




		});

	}

    function onExpensePayment() {
        $(document).on('click', '.add-expense-payment-btn', function (e) {

            $('.payment-table tbody').empty();
            $('.payment-table').hide()
            $('#expense-payment-id').val('');
            $('#expense-payment-amount-name').val('');
            $('#ExpensePaymentId').dropdown('clear');
            $('#expense-payment-description-name').val('');
			$('#expense-payment-proof-upload').empty();
			$('#expense-payment-proof-files').val('');
            var id = $(this).parents('tr').attr('data-expense-id'); 
            $('#expense-payment-id').val(id);
            $('.expense-table').show();
            $.ajax({
                type: 'POST',
                url: "Expense/GetExpenseDetails",
                data: {
                    Id: id
                },

                success: function (data) {
                    var totalPaid = 0;
                    var total = 0;
                    if(data.expense.length != 0)
                    {
						$('.payment-table').show()
						$.each(data.expense_paid, function (index, value) {
							$('.payment-table tbody').append('<tr payment-id="' + value.Id + '"><td>' + value.Description + '</td><td>' + value.Amount + '</td><td>' + moment(value.CreatedAt).format('MMMM, D YYYY') + '</td><td><a class=\"ui mini button delete-payment-btn\" title=\"Delete Payment\"><i class=\"trash icon\"></i></a><a class=\"ui mini button edit-payment-btn\" title=\"Edit Payment\"><i class=\"edit icon\"></i></a></td></tr>'),
                            totalPaid += value.Amount;  
                        });
                        $('.expense-amount').text(data.expense.Amount);
                        total = data.expense.Amount - totalPaid;
                        $('.remaining-balance').text(total);  
                    }        
                }
			});
			$('#payment-date').val(moment(new Date()).format("MMMM D, YYYY"));
			$('.expense-payment-modal').modal({ closable: false }).modal('show').modal('refresh');
			$('.ui.calendar.date').calendar({ type: 'date'});

          
        });     
    }
    function onSubmitExpensePayment() {
        $('#expense-payment-submit-btn').on('click', function () {
            var id = $('#expense-payment-id').val();
            var amount = $('#expense-payment-amount-name').val();
            var type = $('#ExpensePaymentId').val();
            var desc = $('#expense-payment-description-name').val();
			var balance = $('.remaining-balance').val();
			var paymentDate = $('#payment-date').val()

			var formData = new FormData();
			for (var i = 0; i < $('#expense-payment-proof-files').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#expense-payment-proof-files').get(0).files[i]);
			}
			formData.append("ExpenseId", id);
			formData.append("Amount", amount);
			formData.append("Type", type);
			formData.append("Description", desc);
			formData.append("PaymentDate", paymentDate);
			if (paymentAction == 0) {
				$.ajax({
					type: 'POST',
					url: "Expense/AddExpensePayment",
					contentType: false,
					cache: false,
					dataType: "json",
					processData: false,
					data: formData,
					success: function (data) {
						if (data.success) {

							$('.expense-payment-modal').modal('hide');
							swal({ title: "Success", text: "Payment has been successfully recorded.", type: "success" },
								function () {
									paymentAction = 0;
									RefreshExpenseList();
								});



						}
						else {
							swal({ title: "Warning", text: "Payment is more than balance.", type: "error" },
								function () {
									RefreshExpenseList();
								});
						}
					}
				});
			}
			else if (paymentAction == 1) {
				var fileIds = [];
				$('#expense-payment-proof-upload').find('[file-id]').each(function (index, element) {
					fileIds.push($(this).attr('file-id'));
				});
				if (fileIds.length == 0) {
					fileIds.push(0);
				}
				formData.append("fileIds", JSON.stringify(fileIds));
				$.ajax({
					type: 'POST',
					url: "Expense/EditExpensePayment",
					contentType: false,
					cache: false,
					dataType: "json",
					processData: false,
					data: formData,
					success: function (data) {
						if (data.success) {
							$('.expense-payment-modal').modal('hide');
							swal({ title: "Success", text: "Payment has been successfully updated.", type: "success" },
								function () {
									paymentAction = 0;
									RefreshExpenseList();
								});
						}
						else {
							swal({ title: "Warning", text: "Payment is more than balance.", type: "error" },
								function () {
									RefreshExpenseList();
								});
						}
					}

				});
			}
        });
    }
  



    function onDownloadSampleExcelFormat() {
        $(document).on('click', '#excelDownloadFile', function (e) {
            var url = $(this).attr('data-url-link');
            var link = document.createElement("a");
            link.download = "SampleFormat.xlsx";
            link.href = url;
            link.click();
        });
    }

    function onGenerateExpenseSubmit() {
		$('#generate-expense-submit-btn').on('click', function () {
            $('#generate-expense-error-message').hide();
            $('#generate-expense-loading').hide();
            var hasError = false;
            var selectedTimeFrame = $('#time-frame').val();
            var propertyId = $('#property-id-generate-expense').val();
            var daily = $('#daily-date').val();
            var monthly = $('#monthly-month').val();
            var yearly = $('#yearly-year').val();
            var from = $('#range-from').val();
            var to = $('#range-to').val();

            if (selectedTimeFrame != '' && propertyId != '')
            {
                // daily
                if(selectedTimeFrame == 1)
                {
                    if (daily != '')
                    {
                        $('#generate-expense-loading').show();
                    }
                    else { hasError = true }
                }
                // monthly
                else if(selectedTimeFrame == 2)
                {
                    if(monthly != '')
                    {
                        $('#generate-expense-loading').show();
                    }
                    else { hasError = true }
                }
                // yearly
                else if(selectedTimeFrame == 3)
                {
                    if(yearly != '')
                    {
                        $('#generate-expense-loading').show();
                    }
                    else { hasError = true }
                }
                //range
                else if(selectedTimeFrame == 4)
                {
                    if(from != '' && to != '')
                    {
                        $('#generate-expense-loading').show();
                    }
                    else { hasError = true }
                }
            }
            else
            {
                hasError = true;
            }
            if (hasError) $('#generate-expense-error-message').show();
            else
            {
                $.ajax({
                    type: "POST",
                    url: "/Expense/GenerateExpense",
                    data: {
                        type: selectedTimeFrame,
                        propertyFilter: propertyId,
                        dt: daily,
                        month: monthly,
                        year: yearly,
                        from: from,
                        to: to
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success) {
                            if (data.count > 0)
                            {
                                $('#generate-expense-loading').hide();
                                $('#generate-expense-category-modal').modal('hide');
                                swal({ title: "Expense is successfully generated.", text: (data.count + " new records has been added."), type: "success" },
                                function () {
                                    RefreshExpenseList();
                                });
                            }
                            else
                            {
                                $('#generate-expense-loading').hide();
                                swal("No records added from the selected time frame.");
							} expense - submit - btn
                        }
                        else {
                            $('#generate-expense-loading').hide();
                            swal("Error on generate expense", "", "error");
                        }
                    }
                });
            }
        });
    }

    function onGenerateExpenseTimeFrameChange() {
        $('#time-frame').on('change', function () {
            var selectedOption = $(this).val();

            $('#generate-expense-loading').hide();
            $('.time-frame-daily').hide();
            $('.time-frame-monthly').hide();
            $('.time-frame-yearly').hide();
            $('.time-frame-range').hide();

            if (selectedOption == 1) {
                $('#daily-date').val('');
                $('.time-frame-daily').show();
            }
            else if (selectedOption == 2) {
                $('#monthly-month').val('');
                $('.time-frame-monthly').show();
            }
            else if (selectedOption == 3) {
                $('#yearly-year').val('');
                $('.time-frame-yearly').show();
            }
            else if (selectedOption == 4) {
                $('#range-from').val('');
                $('#range-to').val('');
                $('.time-frame-range').show();
            }
        });
    }

    function onGenerateExpense() {
        $('#generate-expense-btn').on('click', function () {
            $('#time-frame').dropdown('clear');
            $('#property-id-generate-expense').dropdown('set selected', 'all');
            $('.time-frame-daily').hide();
            $('.time-frame-monthly').hide();
            $('.time-frame-yearly').hide();
            $('.time-frame-range').hide();
            $('#generate-expense-error-message').hide();
            $('#generate-expense-loading').hide();
            $('#generate-expense-category-modal').modal('show');
            $('.ui.calendar.date').calendar({ type: 'date' });
			$('.ui.calendar.month:not(.filter)').calendar({ type: 'month' });
			$('.ui.calendar.year').calendar({ type: 'year' });
        });
    }
	$('#property-filter').on('change', function () {
		document.getElementById("property-hidden-ids").value = $("#property-filter").val().join();
	});
    function LoadExpenseTable() {
        tableExpense = $('#expense-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Expense/GetExpenseList",
                "type": 'POST',
                "data": function () {
                    return {
						propertyFilter: document.getElementById("property-hidden-ids").value,
                        dueDateFilter: $('#due-date-month-filter').val(),
                        paymentDateFilter: $('#payment-date-month-filter').val(),
						statusFilter: $('#status-filter').val(),
						includeChild: $('#isIncludeChild').is(":checked")
                    }
                }
            },
            "columns": [
                    { "data": "Description" },
                    { "data": "Category" },
                    { "data": "BookingCode" },
                    { "data": "StayDate", "orderable": false },
                    { "data": "FeeName" },
                    { "data": "Amount" },
                    { "data": "PropertyName" },
                    { "data": "Status" },
                    { "data": "DueDate" },
                    { "data": "PaymentDate" },
                    { "data": "CreatedDate" },
                    { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-expense-id', data.Id);
            }
        });
    }

    function LoadExpenseCategoryTable() {
        tableExpenseCategory = $('#expense-category-table').DataTable({
            initComplete: function () {
                $(".dataTables_wrapper").find('.ui.stackable.grid').css("margin", 0);
            },
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Expense/GetExpenseCategoryList",
                "type": 'POST',
                "data": function () {
                    return {
                        type: $('#expense-category-type-filter').val()
                    }
                }
            },
            "columns": [
                    { "data": "Name" },
                    { "data": "Type", "orderable": false },
                    { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-expense-category-id', data.Id);
            }
        });
    }

 //   function onApplyFilter() {
 //       $('#filter-btn').on('click', function () {
 //           RefreshExpenseList();
 //       });
	//}
	function onClearFilter() {
		$('#clear-filter-btn').on('click', function () {
			$('#due-date-month-filter').val('');
			$('#payment-date-month-filter').val('');
			$('#property-filter').dropdown('set selected', 'All');
			$('#status-filter').dropdown('set selected', 'All');
			RefreshExpenseList();
		});
	}
    function onAddExpense() {
        $('.js-btn_create-expenses').on('click', function () {
            ClearModalForm();
            $('#create-expenses-modal').modal('show');
            $('.ui.calendar.date').calendar({ type: 'date' });
            postType = 1;
        });
    }
	//Expense Validation
	$(document).on('keyup', '[required]', function () {

		if ($(this).val().length == 0) {
			$(this).next().remove();
			$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).next().remove();
		}
	});
	function GetPaymentMthod() {
		$.ajax({
			type: "GET",
			url: "/Expense/GetPaymentMethods",

			success: function (data) {
				var paymentMethods = data.paymentMethods;
				$('#schedule-payment-method').empty();
				var optionData = '<option value="">Select Payment Method</option>';
				for (var i = 0; i < paymentMethods.length; i++) {
					optionData += '<option value="' + paymentMethods[i].Id + '">' + paymentMethods[i].Name + '</option>';
				}

				$('#schedule-payment-method').append(optionData);


			}
		});
	}
	function GetExpenseCategory() {
		$.ajax({
			type: "GET",
			url: "/Expense/GetExpenseCategory",

			success: function (data) {
				var expenseCategory = data.expenseCategory;
				$('#ExpenseCategoryId').empty();
				var optionData = '<option value="">Select Category</option>';
				for (var i = 0; i < expenseCategory.length; i++) {
					optionData += '<option value="' + expenseCategory[i].Id + '">' + expenseCategory[i].ExpenseCategoryType + '</option>';
				}

				$('#ExpenseCategoryId').append(optionData);

			}
		});
	}
	$(document).on('change', '#IsPaid, #ExpenseCategoryId, #PropertyId', function () {

		if ($(this).val().length == 0) {
			$(this).parent().next().remove();
			$(this).parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).parent().next().remove();
		}
	});
	function ValidateRequiredFields() {
		var hasBlankField = false;
		var hasfirstFocus = false;
		var property = $('#PropertyId');
		if (property.val() == "" || property.val() == null) {
			property.parent().next().remove();
			property.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				property.focus();
			}
			hasBlankField = true;
		}
		else {
			property.parent().next().remove();
		}
		var expenseCategory = $('#ExpenseCategoryId');
		if (expenseCategory.val() == "" || expenseCategory.val() == null) {
			expenseCategory .parent().next().remove();
			expenseCategory .parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				expenseCategory .focus();
			}
			hasBlankField = true;
		}
		else {
			expenseCategory.parent().next().remove();
		}
		var status = $('#IsPaid');
		if (status.val() == "" || status.val() == null) {
			status.parent().next().remove();
			status.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				status.focus();
			}
			hasBlankField = true;
		}
		else {
			status.parent().next().remove();
		}
		$('#create-expenses-modal').find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.next().remove();
				obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.next().remove();
			}

		});
		return hasBlankField;
	}
    function onExpenseSubmit() {
		$('#expense-submit-btn').on('click', function () {
			var hasBlankField = ValidateRequiredFields();
			if (hasBlankField) {
				return;
			}
            var Id = $('#ExpenseId').val();
            var Description = $('#Description').val();
            var ExpenseCategoryId = $('#ExpenseCategoryId').val();
            var PropertyId = $('#PropertyId').val();
            var Amount = $('#Amount').val();
            var DueDate = $('#DueDate').val();
            var PaymentDate = $('#PaymentDate').val();
            var IsPaid = $('#IsPaid').val();
            var PaymentMethodId = $('#PaymentMethodId').val();
            var IsRecurring = $('#IsRecurring').prop('checked');
            var Frequency = $('#Frequency').val() == '' ? 0 : $('#Frequency').val();
            var IsStartupCost = $('#IsStartupCost').prop('checked');
			var IsCapitalExpense = $('#IsCapitalExpense').prop('checked');

			var formData = new FormData();
			for (var i = 0; i < $('#expense-proof-files').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#expense-proof-files').get(0).files[i]);
			}
			formData.append("Id", Id);
			formData.append("Description", Description);
			formData.append("ExpenseCategoryId", ExpenseCategoryId);
			formData.append("PropertyId", PropertyId);
			formData.append("Amount", Amount);
			formData.append("DueDate", DueDate);
			formData.append("PaymentDate", PaymentDate);
			formData.append("IsPaid", IsPaid);
			formData.append("PaymentMethodId", PaymentMethodId);
			formData.append("IsRecurring", IsRecurring);
			formData.append("Frequency", Frequency);
			formData.append("IsStartupCost", IsStartupCost);
			formData.append("IsCapitalExpense", IsCapitalExpense);
	

            if (Description != '' && ExpenseCategoryId != '' && PropertyId != '' && Amount != '' && DueDate != ''  && IsPaid != '')
			{
                if (postType == 1) // add
                {
                    $.ajax({
                        type: "POST",
                        url: "/Expense/Create",
						contentType: false,
						cache: false,
						dataType: "json",
						processData: false,
						data: formData,
                        success: function (data) {
                            if (data.success) {
                                $('#create-expenses-modal').modal('hide');
                                swal({ title: "Success", text: "Expense has been saved.", type: "success" },
                                function () {
                                    RefreshExpenseList();
                                    ClearModalForm();
                                });
                            }
                            else {
                                $('#create-expenses-modal').modal('hide');
                                swal("Error on add expense", "", "error");
                            }
                        }
                    });
                }
                else if(postType == 2) // edit
                {
                    if(Id != '')
					{
						var fileIds = [];
						$('#expense-proof-upload').find('[file-id]').each(function (index, element) {
							fileIds.push($(this).attr('file-id'));
						});
						if (fileIds.length == 0) {
							fileIds.push(0);
						}
						formData.append("fileIds", JSON.stringify(fileIds));
                        $.ajax({
                            type: "POST",
                            url: "/Expense/Edit",
							contentType: false,
							cache: false,
							dataType: "json",
							processData: false,
							data: formData,
                            success: function (data) {
                                if (data.success) {
                                    $('#create-expenses-modal').modal('hide');
                                    swal({ title: "Success", text: "Changes to expense has been saved.", type: "success" },
                                    function () {
                                        RefreshExpenseList();
                                        ClearModalForm();
                                    });
                                }
                                else {
                                    $('#create-expenses-modal').modal('hide');
                                    swal("Error on edit expense", "", "error");
                                }
                            }
                        });
                    }
                }
            }
            else
            {
                swal("Please fill all fields", "", "error");
            }
            
        });
    }

    function onEditExpense() {
        $(document).on('click', '.edit-expense-btn', function () {
            var id = $(this).parents('tr').attr("data-expense-id");
            $.ajax({
                type: "POST",
                url: "/Expense/Details",
                data: { id: id },
                success: function (data) {
                    if (data.success) {
                        ClearModalForm();
                        $('#ExpenseId').val(data.expense.Id);
                        $('#Description').val(data.expense.Description);
                        $('#ExpenseCategoryId').dropdown('set selected', data.expense.ExpenseCategoryId);
                        $('#PropertyId').dropdown('set selected', data.expense.PropertyId);
                        $('#Amount').val(data.expense.Amount);
                        $('#DueDate').val(moment(data.expense.DueDate).format('MMMM, D YYYY'));
                        $('#PaymentDate').val(moment(data.expense.PaymentDate).format('MMMM, D YYYY'));
                        $('#IsPaid').dropdown('set selected', data.expense.IsPaid);
                        $('#PaymentMethodId').dropdown('set selected', data.expense.PaymentMethodId);
                        $('#IsRecurring').prop('checked', data.expense.IsRecurring);
                        if (data.expense.Frequency != 0) {
                            $('#Frequency').dropdown('set selected', data.expense.Frequency);
                        }
                        $('#IsStartupCost').prop('checked', data.expense.IsStartupCost);
						$('#IsCapitalExpense').prop('checked', data.expense.IsCapitalExpense);
						var files = data.Files;
						for (var x = 0; x < files.length; x++) {
							var image = files[x].FileUrl;
							$('#expense-proof-upload').append((ImageExist(image) ? '<div file-id="' + files[x].Id + '" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><img src="' + image + '"></a></div>' : '<div file-id="' + files[x].Id + '" class="ui small image"><i class="close icon" style="cursor: pointer;"></i><a target="_blank" href="' + image + '"><i class="large icon file"></i></a></div>'));
						}
                        $('#create-expenses-modal').modal('show');
                        $('.ui.calendar.date').calendar({ type: 'date' });
                        postType = 2;
                    }
                }
            });
        });
    }

    function onDeleteExpense() {
        $(document).on('click', '.delete-expense-btn', function () {
            var id = $(this).parents('tr').attr('data-expense-id');
            if (id != '') {
                swal({
                    title: "Are you sure you want to delete this expense ?",
                    text: "",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: "/Expense/Delete",
                        data: { id: id },
                        success: function (data) {
                            if (data.success) {
                                swal({ title: "Success", text: "Expense is successfully deleted.", type: "success" },
                                function () {
                                    RefreshExpenseList();
                                });
                            }
                            else {
                                swal("Error occured on deleting expense", "", "error");
                            }
                        }
                    });
                });
            }
        });
    }

    function onAddExpenseCategory() {
        $('#create-expense-category-btn').on('click', function () {
            $('#expense-category-id').val('');
			$('#category-name').val('');
			$('#companyTermId').val('0');

            $('#create-expense-category-modal').modal('show');
            expenseCategoryPostType = 1;
        });
    }

    function onEditExpenseCategory() {
        $(document).on('click', '.edit-expense-category-btn', function () {
            var id = $(this).parents('tr').attr("data-expense-category-id");
            $.ajax({
                type: "POST",
                url: "/Expense/FindExpenseCategoryById",
                data: { id: id },
                success: function (data) {
					if (data.success) {
                        $('#expense-category-id').val(data.expense_category.Id);
						$('#category-name').val(data.expense_category.ExpenseCategoryType);
						$('#companyTermId').val(data.expense_category.CompanyTerm);
                        $('#create-expense-category-modal').modal('show');
                        expenseCategoryPostType = 2;
                    }
                }
            });
        });
    }

    function onDeleteExpenseCategory() {
        $(document).on('click', '.delete-expense-category-btn', function () {
            var id = $(this).parents('tr').attr('data-expense-category-id');
            if (id != '') {
                swal({
                    title: "Are you sure you want to delete this expense category ?",
                    text: "",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: "/Expense/DeleteExpenseCategory",
                        data: { id: id },
                        success: function (data) {
                            if (data.success) {
                                swal({ title: "Success", text: "Expense category is successfully deleted.", type: "success" },
                                function () {
                                    RefreshExpenseCategoryList();
                                });
                            }
                            else {
                                swal("Error occured on deleting expense category", "", "error");
                            }
                        }
                    });
                });
            }
        });
    }

    function onExpenseCategorySubmit() {
        $(document).on('click', '#expense-category-submit-btn', function () {
            var Id = $('#expense-category-id').val();
            var categoryName = $('#category-name').val();
            if (categoryName != '')
            {
                if (expenseCategoryPostType == 1) // add
                {
                    $.ajax({
                        type: "POST",
                        url: "/Expense/CreateExpenseCategory",
                        data: {
                            categoryName: categoryName,
							type: 1,
							companyTerm: $('#companyTermId').val()

                        },
                        success: function (data) {
                            if (data.success) {
                                $('#create-expense-category-modal').modal('hide');
                                swal({ title: "Success", text: "Expense category has been saved.", type: "success" },
                                function () {
                                    $('#category-name').val('');
									RefreshExpenseCategoryList();
									GetExpenseCategory();
                                });
                            }
                            else {
                                $('#create-expense-category-modal').modal('hide');
                                swal("Error on add expense category", "", "error");
                            }
                        }
                    });
                }
                else if(expenseCategoryPostType == 2) // edit
                {
                    if (Id != '') {
                        $.ajax({
                            type: "POST",
                            url: "/Expense/EditExpenseCategory",
                            data: {
                                Id: Id,
								ExpenseCategoryType: categoryName,
								CompanyTerm: $('#companyTermId').val()

                            },
                            success: function (data) {
                                if (data.success) {
                                    $('#create-expense-category-modal').modal('hide');
                                    swal({ title: "Success", text: "Changes to expense category has been saved.", type: "success" },
                                    function () {
                                        $('#category-name').val('');
                                        RefreshExpenseCategoryList();
                                    });
                                }
                                else {
                                    $('#create-expense-category-modal').modal('hide');
                                    swal("Error on edit expense category", "", "error");
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    function onFilterByCategoryType() {
        $("#expense-category-type-filter").on('change', function () {
            RefreshExpenseCategoryList();
        });
    }

    function RefreshExpenseList() {
        tableExpense.ajax.reload(null, false);
    }

    function RefreshExpenseCategoryList() {
        tableExpenseCategory.ajax.reload(null, false);
    }

    function ClearModalForm() {
		$('#expense-form').form('clear');
		$('#expense-proof-files').val('');
		$('#expense-proof-upload').empty();
        $('#MyTitle').removeClass('active');
        $('#MyContent').removeClass('active');
    }

    // ------------------
    // ------------------
    //   Event Listeners
    // ------------------
    // ------------------

    function onDisplaySidebar() {
        $('#js-btn_sidebar').on('click', function () {
            $('.ui.sidebar').sidebar('toggle');
        });
    }
});