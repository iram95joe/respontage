﻿var tableSubUsers;
// initialization scripts
$(document).ready(function () {
    try {
        if ($("#userRoleId").val().toLowerCase() == "admin") {
            GetSubUserList();
            loadTableData();
        }
    } catch (err) { }


		$(document).on('click', '#profile-image', function (e) {
			$('#profile-pic').click();
			//$(this).parent().find('input[class="profile-pic"]').click();
		});
	
	$(document).on('change', '#profile-pic', function () {
			var imageUrl = window.URL.createObjectURL(this.files[0]);
			$('#profile-image').attr("src", imageUrl);
			//alert(imageUrl);
			//$('#profile-image').attr("src", imageUrl);


	});

	$(document).on('click', '#sub-profile-image', function (e) {
		$('#sub-profile-pic').click();
		//$(this).parent().find('input[class="profile-pic"]').click();
	});

	$(document).on('change', '#sub-profile-pic', function () {
		var imageUrl = window.URL.createObjectURL(this.files[0])
		//$('.sub-image').attr("src", imageUrl);
		//alert(imageUrl);
		$('#sub-profile-image').attr("src", imageUrl);


	});
	$(document).on('click', '#add-profile-image', function (e) {
		$('#add-profile-pic').click();
	});

	$(document).on('change', '#add-profile-pic', function () {
		var imageUrl = window.URL.createObjectURL(this.files[0])
		//$('.sub-image').attr("src", imageUrl);
		//alert(imageUrl);
		$('#add-profile-image').attr("src", imageUrl);
	});

    //getting profile information
	GetProfileDetail();

    $(document).on('click', '#update-profile-btn', function (e) {
        UpdateProfileDetail();
    });
    $(document).on('click', '#change-password-btn', function (e) {
        ChangePasswordOfCurrentLogin();
    });
    $(document).on('click', '#SaveRegisterSubUser', function (e) {
        var flag = true;
        if ($("#AddFirstName").val().trim() == "") {
            flag = false;
            toastr["error"]("please fill the firstname field.");
        }
        if ($("#AddLastName").val().trim() == "") {
            flag = false;
            toastr["error"]("please fill the lastname field.");
        }
        if ($("#AddUserName").val().trim() == "") {
            flag = false;
            toastr["error"]("please fill the email field.");
        }
        //if ($("#AddTimeZone").val().trim() == "") {
        //    flag = false;
        //    toastr["error"]("please select a timezone.");
        //}
        if ($("#AddPassword").val().trim() == "") {
            flag = false;
            toastr["error"]("please fill the password field.");
        }
        if ($("#AddConfirmPasswordModel").val().trim() != $("#AddPassword").val().trim()) {
            flag = false;
            toastr["error"]("Password and confirm password does not match.");
        }
        if ($('#AddRoleId option:selected').length == 0) {
            flag = false;
            toastr["error"]("Please select a role.");
        }

        $("#AddDvGetRoleList").html("");
        $('#AddRoleId :selected').each(function (i, selected) {
            //debugger;
            $("#AddDvGetRoleList").append('<input type="hidden" id="AddRoleId" name="RoleId[]" value="' + $(selected).val() + '" />');
        });

		if (flag == true) {
			var formData = new FormData($('#add-sub-user-form')[0]);
			formData.append("Image", $('#add-sub-user-form input[id="add-profile-pic"]').get(0).files[0]);
            $.ajax({
                type: "POST",
				url: "/Profile/GetRegister/",
				data: formData,

				contentType: false,
				processData: false,
                success: function (html) {
                    if (html == "S1") {
                        $('#add-sub-user-modal').modal('hide');
                        toastr["success"]("Congratulation, your account has been created, an email will be sent to your register account for email validation.");
                        tableSubUsers.ajax.reload(null, false);
                    }
                    else if (html == "A1") {
                        toastr["warning"]("user name is already existing.");
                    }
                    else if (html == "E1") {
                        toastr["error"]("error is coming, please refresh the form.");
                    }
                }
            });
        }
        return false;
    });
    $(document).on('click', '.edit-sub-user', function (e) {
        var id = $(this).parents('tr').attr("data-sub-user-id");
        $('#EditRoleId').dropdown('clear');
        $.ajax({
            type: "GET",
            url: '/Profile/GetSubUserDetails',
            data: { id: id },
            success: function (data) {
                $('#sub-user-id').val(data.sub_user.Id);
                $('#EditFirstName').val(data.sub_user.FirstName);
                $('#EditLastName').val(data.sub_user.LastName);
				$('#EditUserName').val(data.sub_user.UserName);
				$('#sub-profile-image').attr("src", (data.sub_user.ImageUrl != null ? data.sub_user.ImageUrl : "/Images/User/default-image.png"))
                $(data.roles).each(function (index, item) {
                    $('#EditRoleId').dropdown('set selected', item);
                });
                $('#edit-sub-user-modal').modal('show');
            }
        })
    });
    $(document).on('click', '#SaveUpdateSubUser', function (e) {
        UpdateSubUser();
    });
    $(document).on('click', '#add-sub-user-btn', function (e) {
        $('#AddFirstName').val('');
        $('#AddLastName').val('');
        $('#AddUserName').val('');
        $('#AddPassword').val('');
        $('#AddConfirmPasswordModel').val('');
        $('#AddRoleId').dropdown('clear');
        //$('#AddTimeZone').dropdown('clear');
        $('#AddDvGetRoleList').empty();
        $('#add-sub-user-modal').modal('show');
    });
    $(document).on('click', '.delete-sub-user', function (e) {
        var id = $(this).parents('tr').attr("data-sub-user-id");
        swal({
            title: "Are you sure you want to delete this user ?",
            text: "",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function () {
            $.ajax({
                type: 'POST',
                url: "/Profile/DeleteUser",
                data: { id: id },
                async: true,
                success: function (data) {
                    if (data.success) {
                        tableSubUsers.ajax.reload(null, false);
                        swal("Successfully deleted user.", "", "success");
                    }
                    else {
                        swal("An error occured on deleting user", "", "error");
                    }
                }
            });
        });
    });
    $(document).on('click', '.block-sub-user', function (e) {
        var id = $(this).parents('tr').attr("data-sub-user-id");
        swal({
            title: "Are you sure you want to block this user ?",
            text: "",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function () {
            $.ajax({
                type: 'POST',
                url: "/Profile/Block",
                data: { id: id },
                async: true,
                success: function (data) {
                    if (data.success) {
                        tableSubUsers.ajax.reload(null, false);
                        swal("Successfully blocked user.", "", "success");
                    }
                    else {
                        swal("An error occured on block user", "", "error");
                    }
                }
            });
        });
    });
    $(document).on('click', '.unblock-sub-user', function (e) {
        var id = $(this).parents('tr').attr("data-sub-user-id");
        swal({
            title: "Are you sure you want to unblock this user ?",
            text: "",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function () {
            $.ajax({
                type: 'POST',
                url: "/Profile/Unblock",
                data: { id: id },
                async: true,
                success: function (data) {
                    if (data.success) {
                        tableSubUsers.ajax.reload(null, false);
                        swal("Successfully unblocked user.", "", "success");
                    }
                    else {
                        swal("An error occured on unblock user", "", "error");
                    }
                }
            });
        });
    });
});

function loadTableData() {
    tableSubUsers = $('#profile-list-table').DataTable({
        "aaSorting": [],
        "responsive": true,
        "search": { "caseInsensitive": true },
        "ajax": {
            "url": "/Profile/GetSubUserList",
            "type": 'GET'
        },
        "columns": [
                { "data": "FirstName" },
                { "data": "LastName" },
                { "data": "Email" },
                { "data": "Status" },
                { "data": "Actions", "orderable": false }
        ],
        "createdRow": function (row, data, rowIndex) {
            $(row).attr('data-sub-user-id', data.Id);
        }
    });
}

function GetSubUserList() {
    $.ajax({
        type: "GET",
        url: "/Profile/GetSubUserList/",
        data: {},
        success: function (model) {
            $("#tb_SubUserList").html("");
            $.each(model, function (k, v) {
                if (v.Status == false) {
                    $("#tb_SubUserList").append('<tr style="background-color: rgba(255, 0, 0, 0.88) !important; color: #fff;"> <td> ' + v.FirstName + ' </td> <td> ' + v.LastName + ' </td> <td> ' + v.UserName + ' </td> <td> ' + v.Status + ' </td> <td> <a class="ui mini button edit-SubUser" name="' + v.Id + '" title="Edit"> <i class="icon edit"></i> </a> <a class="ui mini button delete-SubUser" name="' + v.Id + '" title="Delete"> <i class="icon ban"></i> </a> <a class="ui mini button Unblock-SubUser" name="' + v.Id + '" title="Block"> <i class="icon ban"></i> </a> </td> </tr>');
                }
                else {
                    $("#tb_SubUserList").append('<tr> <td> ' + v.FirstName + ' </td> <td> ' + v.LastName + ' </td> <td> ' + v.UserName + ' </td> <td> ' + v.Status + ' </td> <td> <a class="ui mini button edit-SubUser" name="' + v.Id + '" title="Edit"> <i class="icon edit"></i> </a> <a class="ui mini button delete-SubUser" name="' + v.Id + '" title="Delete"> <i class="icon ban"></i> </a> <a class="ui mini button block-SubUser" name="' + v.Id + '" title="UnBlock"> <i class="icon ban"></i> </a> </td> </tr>');
                }
            });
        }
    });
}

function GetProfileDetail() {
    $.ajax({
        type: "GET",
        url: "/Profile/GetProfileDetails/",
        success: function (data) {
            if (data.success) {
                $("#FirstName").val(data.result.FirstName);
                $("#LastName").val(data.result.LastName);
				$("#UserName").val(data.result.UserName);
				$("#profile-image").attr("src", (data.result.ImageUrl != null ? data.result.ImageUrl :'/Images/User/default-image.png'));
                $("#TimeZone option[value='" + data.result.TimeZone + "']").prop("selected", true);
            }
        }
    });
}

function UpdateProfileDetail() {

	var formData = new FormData($("#profile-form")[0]);
	formData.append("Image", $('#profile-form input[name="profile-pic"]').get(0).files[0]);
    $.ajax({
        type: "POST",
		url: "/Profile/UpdateProfile/",
		data: formData,
		contentType: false,
		processData: false,
        success: function (data) {
            if (data.success) {
                toastr["success"]("Profile details updated successfully.");
            }
            else {
                toastr["error"]("Error on update profile.");
            }
        }
    });
}

function UnBlockSubUserProfile(id) {
    $.ajax({
        type: "GET",
        url: "/Home/UnBlockUser/",
        data: { subUserId: id },
        success: function (html) {
            if (html == "S1") {
                toastr["success"]("sub-user profile unblocked successfully.");
                GetSubUserList();
            }
            else {
                toastr["error"]("error is coming, please refresh the form.");
            }
        }
    });
}

function UpdateSubUser() {
    var flag = true;
    if ($("#EditFirstName").val().trim() == "") {
        flag = false;
        toastr["error"]("please fill the firstname field.");
    }
    if ($("#EditLastName").val().trim() == "") {
        flag = false;
        toastr["error"]("please fill the lastname field.");
    }
    if (validateForm($("#EditUserName").val().trim()) == false) {
        flag = false;
        toastr["error"]("please fill the valid email.");
    }
    if ($('#EditRoleId option:selected').length == 0) {
        flag = false;
        toastr["error"]("Please select a role.");
    }
    $("#EditDvGetRoleList").html("");
    $('#EditRoleId :selected').each(function (i, selected) {
        $("#EditDvGetRoleList").append('<input type="hidden" name="RoleId[]" value="' + $(selected).val() + '" />');
	});

	var formData = new FormData($("#edit-sub-user-form")[0]);
	formData.append("Image", $('#edit-sub-user-form input[id="sub-profile-pic"]').get(0).files[0]);
    if (flag == true) {
        $.ajax({
            type: "POST",
			url: "/Profile/UpdateSubUserDetail/",
			data: formData,
			contentType: false,
			processData: false,
            success: function (html) {
                if (html == "S1") {
                    toastr["success"]("Profile details of sub-user updated successfully.");
                    $('#edit-sub-user-modal').modal('hide');
                    tableSubUsers.ajax.reload(null, false);
                }
                else {
                    toastr["error"]("error is coming, please refresh the form.");
                }
            }
        });
    }
}


function ChangePasswordOfCurrentLogin() {
    var flag = true;
    if ($("#OldPassword").val().trim() == "") {
        flag = false;
        toastr["error"]("please fill the old password field.");
    }
    if ($("#NewPassword").val().trim() == "") {
        flag = false;
        toastr["error"]("please fill the new password field.");
    }
    if ($("#ConfirmPassword").val().trim() != $("#NewPassword").val().trim()) {
        flag = false;
        toastr["error"]("confirm password is not matching with new password field.");
    }
    if (flag == true) {
        $.ajax({
            type: "POST",
            url: "/Profile/ChangePassword/",
            data: $("#change-password-form").serialize(),
            success: function (data) {
                if (data.success) {
                    toastr["success"]("Password has been successfully changed.");
                    $("#OldPassword").val('');
                    $("#NewPassword").val('');
                    $("#ConfirmPassword").val('');
                }
                else {
                    toastr["error"]("Error in changing password.");
                }
            }
        });
    }
}

function validateForm(valEmail) {
    var x = valEmail;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        return false;
    }
}

var lastForm = null;

function paymentMethodChanged() {
	var paymentMethod = $("#payment-method").val();

	if (lastForm != null) {
		lastForm.transition("fade");
	}

	if (paymentMethod == "paypal") {
		lastForm = $("#paypal-form");
		lastForm.transition("fade left");
	}
	else if (paymentMethod == "square") {
		lastForm = $("#square-form");
		lastForm.transition("fade left");
	}
	else if (paymentMethod == "stripe") {
		lastForm = $("#stripe-form");
		lastForm.transition("fade left");
	}

	$("#save-information-btn").prop("disabled", false);
}

function paymentMethodChanged() {
    var paymentMethod = $("#payment-method").val();
    $.ajax({
        type: "GET",
        url: "/Profile/GetPaymentAccountInformation/",
        data: { paymentMethod: paymentMethod },
        success: function (result) {
            var data = result.data;
            $('#PaypalClientId').val(data.PaypalClientId == null ? '' : data.PaypalClientId);
            $('#PaypalClientSecret').val(data.PaypalClientSecret == null ? '' : data.PaypalClientSecret);
            $('#SquareAccessToken').val(data.SquareAccessToken == null ? '' : data.SquareAccessToken);
            $('#SquareApplicationId').val(data.SquareApplicationId == null ? '' : data.SquareApplicationId);
            $('#SquareLocationId').val(data.SquareLocationId == null ? '' : data.SquareLocationId);
            $('#StripePublishableKey').val(data.StripePublishableKey == null ? '' : data.StripePublishableKey);
            $('#StripeSecretKey').val(data.StripeSecretKey == null ? '' : data.StripeSecretKey);
        }
    });
    if (lastForm != null) {
        lastForm.transition("fade");
    }

    if (paymentMethod == 1) {
        lastForm = $("#paypal-form");
        lastForm.transition("fade left");
    }
    else if (paymentMethod == 2) {
        lastForm = $("#square-form");
        lastForm.transition("fade left");
    }
    else if (paymentMethod == 3) {
        lastForm = $("#stripe-form");
        lastForm.transition("fade left");
    }

    $("#save-information-btn").prop("disabled", false);
}
$("#save-information-btn").click(function () {
	debugger;
	$.ajax({
		type: "POST",
		url: "/Profile/SavePaymentAccountInformation/",
		data: $("#payment-account-form").serialize(),
		success: function (result) {
			if (result.success) {
				toastr["success"]("Payment account information has been successfully saved.");
			}
			else {
				toastr["error"]("Error in saving payment account information.");
			}
		}
	});
});