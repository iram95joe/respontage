﻿$(document).ready(function () {

    function ValidateRequiredFields(parent) {
        var hasBlankField = false;
        var hasfirstFocus = false;
        parent.find('[required]').each(function (index, element) {
            var obj = $(this);
            if (obj.val().length == 0) {
                obj.attr("style", "border-color:red");
                if (hasfirstFocus == false) {
                    hasfirstFocus = true;
                    obj.focus();
                }
                hasBlankField = true;
            } else {
                obj.removeAttr("style");
            }

        });
        return hasBlankField;
    }
    $(document).on('click', '.wizard', function (e) {

        $('#wizard-modal').modal({ closable: false }).modal('show').modal('refresh');
    });
    //Save Details of all property
    $(document).on('click', '#save-wizard', function (e) {

        var formData = new FormData();
        var multiUnit = $('#property-name-wizard').parents('.field').next();
        var isMultiUnit = multiUnit.find('.ui.buttons').attr('is-multi-unit');
        formData.append("PropertyName", $('#property-name-wizard').val());
        formData.append("IsMultiUnit", isMultiUnit);
        if (isMultiUnit == 'true') {
            var propertyCount = 0;
            $('.property-unit').each(function (index) {
                var propertyUnit = $(this);
                formData.append("Child[" + propertyCount + "].PropertyName", propertyUnit.find('.property-name').val());
                var hasRenter = propertyUnit.find('.property-name').parents('.field').next().find('.ui.buttons').attr('has-renter');
                if (hasRenter == 'true') {
                    var renterCount = 0;
                    propertyUnit.find('.property-name').parents('.field').siblings('.renter-container').find('.renter').each(function (index) {
                        formData.append("Child[" + propertyCount + "].Renters[" + renterCount + "].Firstname", $(this).find('.firstname').val());
                        formData.append("Child[" + propertyCount + "].Renters[" + renterCount + "].Lastname", $(this).find('.lastname').val());
                        formData.append("Child[" + propertyCount + "].Renters[" + renterCount + "].Email", $(this).find('.email').val());
                        formData.append("Child[" + propertyCount + "].Renters[" + renterCount + "].PhoneNumber", $(this).find('.contact').val());
                        renterCount++;
                    });
                    formData.append("Child[" + propertyCount + "].StartDate", $(this).find('.start-date').val());
                    formData.append("Child[" + propertyCount + "].EndDate", $(this).find('.end-date').val());
                    formData.append("Child[" + propertyCount + "].RecurringDate", $(this).find('.recurring-date').val());
                    formData.append("Child[" + propertyCount + "].Amount", $(this).find('.amount').val());

                }
                propertyCount++;
            });

        }
        else {
            var hasRenter = isMultiUnit == true ? "false" : multiUnit.next().find('.ui.buttons').attr('has-renter');
            if (hasRenter=='true') {
                var renterCount = 0;
                $(multiUnit).siblings('.renter-container').find('.renter').each(function (index) {
                    formData.append("Renters[" + renterCount + "].Firstname", $(this).find('.firstname').val());
                    formData.append("Renters[" + renterCount + "].Lastname", $(this).find('.lastname').val());
                    formData.append("Renters[" + renterCount + "].Email", $(this).find('.email').val());
                    formData.append("Renters[" + renterCount + "].PhoneNumber", $(this).find('.contact').val());
                    renterCount++;
                });
                formData.append("StartDate", $(multiUnit).siblings('.contract-container').find('.start-date').val());
                formData.append("EndDate", $(multiUnit).siblings('.contract-container').find('.end-date').val());
                formData.append("RecurringDate", $(multiUnit).siblings('.contract-container').find('.recurring-date').val());
                formData.append("Amount", $(multiUnit).siblings('.contract-container').find('.amount').val());
            }
        }
        $.ajax({
            type: "POST",
            url: '/Property/SaveWizardProperty',
            data: formData,
            beforeSend: function () {
                $('#save-wizard').addClass('loading');
            },
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.success) {
                    $('#save-wizard').removeClass('loading');
                    swal("Successfully Save!", "", "success");
                    $('#wizard-modal').modal('hide');
                }

            }
        });
    });

    $(document).on('click', '.yes-multi-unit', function (e) {
        $(this).parents('.field').nextAll().remove();
        $('<div class="ui field property-hidden" hidden>' +
            '<label>How many unit(s)?</label>' +
            '<input class="no-of-unit" required placeholder="Number of unit(s)" />' +
            '</div>').insertAfter($(this).parents('.field'));
        $('#wizard-modal').modal('refresh');
        $(this).parent().attr('is-multi-unit', true);
        ShowNextHiddenFields();
    });

    //add question if there is a renter
    $(document).on('click', '.no-multi-unit', function (e) {
        $(this).parents('.field').nextAll().remove();
        $('<div class="ui field choice">' +
            '<label>Do you have Renter</label>' +
            '<div class="ui buttons">' +
            '<button class="ui blue button yes-renter">Yes</button>' +
            '<div class="or"></div>' +
            '<button class="ui blue button no-renter">No</button>' +
            '</div>' +
            '</div>' +
            '<div class="details-container">' +

            '</div>').insertAfter($(this).parents('.field'));
        $('#wizard-modal').modal('refresh');
        $(this).parent().attr('is-multi-unit', false);
        //ShowNextHiddenFields();
    });
    $(document).on('keyup', '.no-of-unit', function (e) {
        var count = $(this).val();
        if (count > 0) {
            var html = '';
            for (var x = 1; x <= count; x++) {
                html += '<div class="property-hidden property-unit" hidden>' +
                    '<div class="ui field">' +
                    '<label>Unit ' + x + '</label>' +
                    '<input placeholder="Property Name" required class="property-name" />' +
                    '</div>' +
                    '<div class="ui field choice">' +
                    '<label>Do you have Renter</label>' +
                    '<div class="ui buttons">' +
                    '<button class="ui blue button yes-renter">Yes</button>' +
                    '<div class="or"></div>' +
                    '<button class="ui blue button no-renter">No</button>' +
                    '</div>' +
                    '</div>' +
                    '<div class="details-container">' +

                    '</div>' +
                    '</div>';
            }
            //html += '</div>';
            $(this).parents('.field').nextAll().remove();
            $(html).insertAfter($(this).parents('.field'));
            ShowNextHiddenFields();
        }
    });

    //$(document).on('click', '.prev-unit', function (e) {
    //    var currentRenter = $(this).siblings('.property-container').find('.property-unit:not([hidden])');
    //    if (currentRenter.prev().length > 0) {
    //        currentRenter.attr('hidden', 'hidden');
    //        currentRenter.prev().removeAttr('hidden');
    //    }
    //});

    //$(document).on('click', '.next-unit', function (e) {
    //    var currentRenter = $(this).siblings('.property-container').find('.property-unit:not([hidden])');
    //    if (currentRenter.next().length > 0) {
    //        currentRenter.attr('hidden', 'hidden');
    //        currentRenter.next().removeAttr('hidden');
    //    }
    //});

    //Add question how many renter
    $(document).on('click', '.yes-renter', function (e) {
        $(this).parents('.field').nextAll().remove();
        $('<div class="ui field">' +
            '<label>How many renter?</label>' +
            '<input class="no-of-renter" required placeholder="Number of renter" />' +
            '</div>').insertAfter($(this).parents('.field'));
        $(this).parent().attr('has-renter', true);

        //ShowNextHiddenFields()
    });


    $(document).on('click', '.no-renter', function (e) {
        $(this).parent().attr('has-renter', false);
        $(this).parents('.field').nextAll().remove();
        ShowNextHiddenFields()
    });

    //Create renter fields base on number input
    $(document).on('keyup', '.no-of-renter', function (e) {
        $(this).parents('.field').nextAll().remove();
        var count = $(this).val();
        if (count > 0) {
            var html = '<hr><div class="renter-container">';
            for (var x = 1; x <= count; x++) {
                html += '<div class="renter">' +
                    '<h3>Renter ' + x + '</h3>' +
                    '<div class="ui fields">' +
                    '<div class="ui field">' +
                    '<label>Firstname</label>' +
                    '<input placeholder="Firstname" required class="firstname" />' +
                    '</div>' +
                    '<div class="ui field">' +
                    '<label>Lastname</label>' +
                    '<input placeholder="Lastname" required class="lastname" />' +
                    '</div>' +
                    '<div class="ui field">' +
                    '<label>Email</label>' +
                    '<input placeholder="Email Address" required class="email" />' +
                    '</div>' +
                    '<div class="ui field">' +
                    '<label>Contact Number</label>' +
                    '<input placeholder="Contact Number" required class="contact" />' +
                    '</div>' +
                    '</div>'+
                    '</div>';
            }
            html += '</div>' +
                '<div class="contract-container">' +
                '<div class="ui field">' +
                '<label>What is your contract type?</label>' +
                '<div class="ui buttons">' +
                '<button class="ui blue button contract" contract="fix-term">Fix term</button>' +
                '<div class="or"></div>' +
                '<button class="ui blue button contract" contract="month-to-month">Month to Month</button>' +
                '</div><div class="contract-details"></div>'+'</div>';
            
            $(html).insertAfter($(this).parents('.field'));
            $('#wizard-modal').modal('refresh');
        }
    });

    $(document).on('click', '.contract', function (e) {
        var contract = $(this).attr('contract')
        $(this).parent().siblings('.contract-details').empty();
        if (contract == "month-to-month") {
            $(this).parent().siblings('.contract-details').append('<div class="ui fields">' +
                '<div class="ui field">' +
                '<label>Start Date</label>' +
                '<div class="ui calendar">' +
                '<div class="ui input left icon">' +
                '<i class="calendar icon"></i>' +
                '<input type="text" required class="start-date">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="ui fields">' +
                '<div class="ui field">' +
                '<label>Recurring</label>' +
                '<div class="ui calendar">' +
                '<div class="ui input left icon">' +
                '<i class="calendar icon"></i>' +
                '<input type="text" required class="recurring-date">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="ui field">' +
                '<label>Amount</label>' +
                '<input placeholder="Amount" required class="amount"/>' +
                '</div>' +
                '</div>');
        } else if (contract == "fix-term") {
            $(this).parent().siblings('.contract-details').append('<div class="ui fields">' +
                '<div class="ui field">' +
                '<label>Start Date</label>' +
                '<div class="ui calendar">' +
                '<div class="ui input left icon">' +
                '<i class="calendar icon"></i>' +
                '<input type="text" required class="start-date">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="ui field">' +
                '<label>End Date</label>' +
                '<div class="ui calendar">' +
                '<div class="ui input left icon">' +
                '<i class="calendar icon"></i>' +
                '<input type="text" required class="end-date">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="ui fields">' +
                '<div class="ui field">' +
                '<label>Recurring</label>' +
                '<div class="ui calendar">' +
                '<div class="ui input left icon">' +
                '<i class="calendar icon"></i>' +
                '<input type="text" required class="recurring-date">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="ui field">' +
                '<label>Amount</label>' +
                '<input placeholder="Amount" required class="amount" />' +
                '</div>' +
                '</div>');
        }
        $('.ui.calendar').calendar({
            type: 'date'
        });
    });


    $(document).on('click', '.prev-hidden', function (e) {
        ShowPreviousHiddenFields();
    });

    function ShowNextHiddenFields() {
        var currentRenter = $('#wizard-modal').find('.property-hidden:not([hidden])');
        var hasBlank = ValidateRequiredFields(currentRenter);
        if (!hasBlank) {
            if (currentRenter.next('.property-hidden').length > 0) {
                currentRenter.attr('hidden', 'hidden');
                currentRenter.next('.property-hidden').removeAttr('hidden');
                //$('.prev-button').removeAttr('hidden');
            }
            else {
                $('.save-button').removeAttr('hidden');
                $('#wizard-modal').find('.property-hidden').removeAttr('hidden');
                $('.choice').attr('hidden', 'hidden');
                $('.prev-next-button').attr('hidden', 'hidden');
            }
            $('#wizard-modal').modal('refresh');
        }
    }
    function ShowPreviousHiddenFields() {
        var currentRenter = $('#wizard-modal').find('.property-hidden:not([hidden])');
        if (currentRenter.prev('.property-hidden').length > 0) {
            currentRenter.attr('hidden', 'hidden');
            currentRenter.prev('.property-hidden').removeAttr('hidden');
        }
        else {
            //$('.next-button').removeAttr('hidden');
            $('.save-button').attr('hidden', 'hidden');
		}
        $('#wizard-modal').modal('refresh');
    }
    $(document).on('click', '.next-hidden', function (e) {
        ShowNextHiddenFields()
    });
});