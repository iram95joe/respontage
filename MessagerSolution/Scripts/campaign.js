﻿$(document).ready(function () {
    var advertisementTable;
    var campaignTable;
    var accountTable;
    LoadadvertisementTable();
    Createadvertisement();
    GetPropertyDetailsForAdvertisement();
    UploadSaveAdvertisement();
    EditAdvertismentTemplate();
    GetAdvertismentDropdown();
    CreateCampaign();
    GetAdvertismenTemplateDetails();
    SaveCampaign();
    LoadCampaignTable();
    ShowSiteContent();
    AddCampaignAdvertisment();
    GetLamudiPropertySubType();
    LoadAccountTable();
    SaveLamudiAccount();
    AddAccount();
    EditAdvertisement();
    function LoadadvertisementTable() {
        advertisementTable = $('#advertisement-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Campaign/AdvertisementList",
                "type": 'GET',
            },
            "columns": [
                { "data": "PropertyName" },
                { "data": "Title" },
                { "data": "Site" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-advertisement-id', data.Id);
            }
        });
    }
    function LoadCampaignTable() {
        campaignTable = $('#campaign-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Campaign/GetCampaignList",
                "type": 'GET',
            },
            "columns": [
                { "data": "Name" },
                { "data": "Advertisements" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-campaign-id', data.Id);
                $(row).find('.ui.accordion').accordion();
            }
        });
    }
    function LoadAccountTable() {
        accountTable = $('#account-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Campaign/GetAccounts",
                "type": 'GET',
            },
            "columns": [
                { "data": "Username" },
                { "data": "Name" },
                { "data": "Site" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-account-id', data.Id);
            }
        });
    }
    function AddAccount() {
        $(document).on('click', '#add-account', function () {
            $('#advertisement-account-modal').modal('show');
        });
    }
    function SaveLamudiAccount() {
        $(document).on('click', '#lamudi-save-account', function () {
            var username = $('#lamudi-username').val();
            var password = $('#lamudi-password').val();
            $.ajax({
                type: "POST",
                url: '/Campaign/SaveLamudiAccount',
                dataType: "json",
                data: { username: username, password: password },
                success: function (data) {
                    if (data.success) {
                        $('#advertisement-account-modal').modal('hide');
                        accountTable.ajax.reload(null, false);
                    }
                    else {
                        swal("Invalid Credentials!", "", "error");
					}
                }
            });
        });
    }
    function Createadvertisement() {
        $(document).on('click', '#create-advertisement', function () {
            $('#advertisement-modal').modal('show');
        });
    }
    function CreateCampaign() {
        $(document).on('click', '#create-campaign', function () {
            $('#campaign-modal').modal('show');
        });
    }

    function ShowSiteContent() {
        $(document).on('change', '.advertisement-site', function () {

            var value = $(this).dropdown('get value');
            var parent = $(this).parents('.ui.modal');
            var isMultiple = $(this).hasClass('multiple')
            if (isMultiple) {
                var site = value.toString().split(',');
                parent.find('.lamudi-content').attr('hidden','');
                parent.find('.craiglist-content').attr('hidden','');
                if (jQuery.inArray("2", site) !== -1) {
                    parent.find('.craiglist-content').removeAttr('hidden');
                }
                if (jQuery.inArray("3", site) !== -1) {
                    parent.find('.lamudi-content').removeAttr('hidden');
                }
            }
            else {
                if (value == '1') {
                    parent.find('.lamudi-content').attr('hidden', '');
                    parent.find('.craiglist-content').attr('hidden', '');
                }
                else if (value == '2') {
                    parent.find('.lamudi-content').attr('hidden', '');
                    parent.find('.craiglist-content').removeAttr('hidden');
                }
                else if (value == '3') {
                    parent.find('.lamudi-content').removeAttr('hidden');
                    parent.find('.craiglist-content').attr('hidden', '');
                }
            }

            parent.modal('refresh')
        });
        
	}
    function GetPropertyDetailsForAdvertisement() {
        $(document).on('change', '.advertisement-property', function () {
            var id = $(this).dropdown('get value');
            $.ajax({
                type: "GET",
                url: '/Campaign/GetPropertyDetails',
                data: { id: id },
                success: function (data) {
                    if (data.success) {
                        var property = data.property;
                        var modal = $('#advertisement-modal');
                        modal.find('.title').val(property.Title);
                        modal.find('.description').val(property.Description);
                        modal.find('.bedrooms').val(property.Bedrooms);
                        modal.find('.bathrooms').val(property.Bathrooms);
                        modal.find('.housing-type-id').dropdown("set selected",property.HousingTypeId);
                        modal.find('.laundry-id').dropdown("set selected",property.LaundryId)
                        modal.find('.parking-id').dropdown("set selected",property.ParkingId)
                        modal.find('.rent-period-id').dropdown("set selected",property.RentPeriodId)
                        modal.find('.geographic-area').val(property.GeographicArea);
                        modal.find('.postal-code').val(property.PostalCode);
                        modal.find('.surface-area').val(property.SurfaceArea);
                        modal.find('.price').val(property.Price);
                        modal.find('.longitude').val(property.Longitude);
                        modal.find('.latitude').val(property.Latitude);
                        $('.ui.calendar').calendar({
                            type: 'date'
                        });
                    }
                }
            });
        });
    }
    function GetAdvertismentDropdown() {
        $.ajax({
            type: "GET",
            url: "/Campaign/GetAdvertisementTemplates",
            success: function (data) {
                var advertisments = data.advertisments;
                $('.advertisement-template').find('.menu').empty();
                $('.advertisement-template').find('select').empty();
                var optionData = '';
                var selectOption = '';
                for (var i = 0; i < advertisments.length; i++) {
                    selectOption+= '<option value="' + advertisments[i].Id + '">' + advertisments[i].Title + '</option>';
                    optionData += '<div class="item" data-value="' + advertisments[i].Id + '">' + advertisments[i].Title + '</div>';
                }
                $('.advertisement-template').find('select').append(selectOption);
                $('.advertisement-template').find('.menu').append(optionData);
                $('.advertisement-template').dropdown('clear');

            }
        });
    }

    function GetLamudiPropertySubType() {
        $(document).on('change', '.lamudi-property-type', function () { 
            var subTypeDropdown = $(this).parents('.fields').find('.lamudi-property-sub-type');
        $.ajax({
            type: "GET",
            url: "/Campaign/GetLamudiPropertySubType",
            data: { id: $(this).dropdown('get value') },
            success: function (data) {
                var subtypes = data.subtypes;
                subTypeDropdown.find('.menu').empty();
                subTypeDropdown.find('select').empty();
                var optionData = '';
                var selectOption = '';
                for (var i = 0; i < subtypes.length; i++) {
                    selectOption += '<option value="' + subtypes[i].Id + '">' + subtypes[i].Label + '</option>';
                    optionData += '<div class="item" data-value="' + subtypes[i].Id + '">' + subtypes[i].Label+ '</div>';
                }
                subTypeDropdown.find('select').append(selectOption);
                subTypeDropdown.find('.menu').append(optionData);
                subTypeDropdown.dropdown('clear');

            }
        });
        });
    }
    function FormatDate(date) {
        return moment(date).format('MMMM D,YYYY');
	}
    function GetAdvertismenTemplateDetails() {
        $(document).on('change', '.advertisement-template', function () {
            var id = $(this).dropdown('get value');
            $.ajax({
                type: "GET",
                url: '/Campaign/GetAdvertisement',
                data: { id: id },
                success: function (data) {
                    var advertisement = data.advertisement;
                    var modal = $('#campaign-modal');
                    modal.find('.title').val(advertisement.Title);
                    modal.find('.description').val(advertisement.Description);
                    modal.find('.bedrooms').val(advertisement.Bedrooms);
                    modal.find('.bathrooms').val(advertisement.Bathrooms);
                    modal.find('.housing-type-id').dropdown("set selected",advertisement.HousingTypeId);
                    modal.find('.laundry-id').dropdown("set selected",advertisement.LaundryId)
                    modal.find('.parking-id').dropdown("set selected",advertisement.ParkingId)
                    modal.find('.rent-period-id').dropdown("set selected",advertisement.RentPeriodId)
                    modal.find('.floor-id').val(advertisement.FlooringId)
                    modal.find('.geographic-area').val(advertisement.GeographicArea);
                    modal.find('.postal-code').val(advertisement.PostalCode);
                    modal.find('.surface-area').val(advertisement.SurfaceArea);
                    modal.find('.advertisement-property').dropdown('set selected', advertisement.PropertyId);
                    modal.find('.advertisement-site').dropdown('set selected', advertisement.AdvertismentSite);
                    modal.find('.email').val(advertisement.FromEMail);
                    modal.find('.price').val(advertisement.Price);
                    modal.find('.movein-date').val(FormatDate(advertisement.MoveinDate));
                    modal.find('.sale-date1').val(FormatDate(advertisement.SaleDate1));
                    modal.find('.sale-date2').val(FormatDate(advertisement.SaleDate2));
                    modal.find('.sale-date3').val(FormatDate(advertisement.SaleDate3));
                    modal.find('.longitude').val(advertisement.Longitude);
                    modal.find('.latitude').val(advertisement.Latitude);
                    $('.ui.calendar').calendar({
                        type: 'date'
                    });
                }
            });
        });
	}
    function UploadSaveAdvertisement() {
        $(document).on('click', '#save-advertisment-btn', function (e) {
            var formData = new FormData();
            var modal = $('#advertisement-modal');
            formData.append("Title", modal.find('.title').val());
            formData.append("Description", modal.find('.description').val());
            formData.append("Bedrooms", modal.find('.bedrooms').val());
            formData.append("Bathrooms", modal.find('.bathrooms').val());
            formData.append("HousingTypeId", modal.find('.housing-type-id').dropdown('get value'));
            formData.append("LaundryId", modal.find('.laundry-id').dropdown('get value'));
            formData.append("ParkingId", modal.find('.parking-id').dropdown('get value'));
            formData.append("RentPeriodId", modal.find('.rent-period-id').dropdown('get value'));
            formData.append("GeographicArea", modal.find('.geographic-area').val());
            formData.append("PostalCode", modal.find('.postal-code').val());
            formData.append("FlooringId", modal.find('.floor-id').val());
            formData.append("SurfaceArea", modal.find('.surface-area').val());
            formData.append("FromEMail", modal.find('.email').val());
            formData.append("MoveinDate", modal.find('.movein-date').val());
            formData.append("SaleDate1", modal.find('.sale-date1').val());
            formData.append("SaleDate2", modal.find('.sale-date2').val());
            formData.append("SaleDate3", modal.find('.sale-date3').val());
            formData.append("Price", modal.find('.price').val());
            formData.append("AdvertismentSite", modal.find('.advertisement-site').dropdown('get value'));
            formData.append("PropertyId", modal.find('.advertisement-property').dropdown('get value'));
            formData.append("Longitude", modal.find('.longitude').val());
            formData.append("Latitude", modal.find('.latitude').val())
            formData.append("PropertyType", modal.find('.lamudi-property-type').dropdown('get value'));
            formData.append("PropertySubType", modal.find('.lamudi-property-sub-type').dropdown('get value'));
            formData.append("State", modal.find('.lamudi-state').val());
            formData.append("City", modal.find('.lamudi-city').val());
            formData.append("Brgy", modal.find('.lamudi-brgy').val());
            formData.append("Address", modal.find('.lamudi-address').val());
            formData.append("YoutubeUrl", modal.find('.lamudi-youtube').val());
            formData.append("TourUrl", modal.find('.lamudi-tour').val());
            formData.append("Id", modal.attr('advertisment-id'));
            $.ajax({
                type: "POST",
                url: '/Campaign/SaveAdvertisementTemplate',
                contentType: false,
                cache: false,
                dataType: "json",
                processData: false,
                data: formData,
                success: function (data) {
                    if (data.success) {
                        $('#advertisement-modal').modal('hide');
                        advertisementTable.ajax.reload(null, false);
                        GetAdvertismentDropdown();
                    }
                }
            });
        });
    }
    function EditAdvertismentTemplate() {
        $(document).on('click', '.edit-advertisment-template', function () {
            var id = $(this).parents('tr').attr('data-advertisement-id');
            $.ajax({
                type: "GET",
                url: "/Campaign/GetAdvertisementTemplate",
                data: { id: id },
                dataType: "json",
                async: true,
                success: function (data) {
                    var advertisement = data.advertisement;
                    var modal = $('#advertisement-modal');
                    modal.find('.title').val(advertisement.Title);
                    modal.find('.description').val(advertisement.Description);
                    modal.find('.bedrooms').val(advertisement.Bedrooms);
                    modal.find('.bathrooms').val(advertisement.Bathrooms);
                    modal.find('.housing-type-id').dropdown("set selected",advertisement.HousingTypeId);
                    modal.find('.laundry-id').dropdown("set selected",advertisement.LaundryId)
                    modal.find('.parking-id').dropdown("set selected",advertisement.ParkingId)
                    modal.find('.rent-period-id').dropdown("set selected",advertisement.RentPeriodId)
                    modal.find('.floor-id').dropdown("set selected",advertisement.FlooringId);
                    modal.find('.geographic-area').val(advertisement.GeographicArea);
                    modal.find('.postal-code').val(advertisement.PostalCode);
                    modal.find('.surface-area').val(advertisement.SurfaceArea);
                    modal.find('.advertisement-site').dropdown('set selected', advertisement.AdvertismentSite);
                    modal.find('.geographic-area').val(advertisement.GeographicArea);
                    modal.find('.postal-code').val(advertisement.PostalCode);
                    modal.find('.surface-area').val(advertisement.SurfaceArea);
                    modal.find('.advertisement-property').dropdown('set selected', advertisement.PropertyId);
                    modal.find('.email').val(advertisement.FromEMail);
                    modal.find('.price').val(advertisement.Price);
                    modal.find('.movein-date').val(FormatDate(advertisement.MoveinDate));
                    modal.find('.sale-date1').val(FormatDate(advertisement.SaleDate1));
                    modal.find('.sale-date2').val(FormatDate(advertisement.SaleDate2));
                    modal.find('.sale-date3').val(FormatDate(advertisement.SaleDate3));
                    modal.find('.longitude', advertisement.Longitude);
                    modal.find('.latitude', advertisement.Latitude);
                    modal.attr('advertisment-id', advertisement.Id);
                    modal.find('.lamudi-property-type').dropdown('set selected', advertisement.PropertyType);
                    modal.find('.lamudi-property-sub-type').dropdown('set selected', advertisement.PropertySubType);
                    modal.find('.lamudi-state').val(advertisement.State);
                    modal.find('.lamudi-city').val(advertisement.City);
                    modal.find('.lamudi-brgy').val(advertisement.Brgy);
                    modal.find('.lamudi-address').val(advertisement.Address);
                    modal.find('.lamudi-youtube').val(advertisement.YoutubeUrl);
                    modal.find('.lamudi-tour').val(advertisement.TourUrl);
                    modal.modal('show');
                    $('.ui.calendar').calendar({
                        type: 'date'
                    });
                }
            });
        });
        
    }
    StateSearch()
    function StateSearch() {
        $(document).on('keyup', '.lamudi-state', function (e) {
            var text = $(this).val();
            var datalist = $(this).parents('.field').find('.lamudi-state-datalist');
            if (text.length > 2) {
                $.ajax({
                    type: "GET",
                    url: "/Campaign/GetLamudiState",
                    data: { text:text},
                    success: function (data) {
                        var states = data.states
                        datalist.empty();
                        var optionData = '';
                        for (var i = 0; i < states.length; i++) {
                            optionData += '<option value="' + states[i].Name + '">';
                        }
                        datalist.append(optionData);

                    }
                });
            }
        });
	}

    function AddCampaignAdvertisment() {
        $(document).on('click', '.add-campaign-advertisment', function (e) {

            var id = $(this).parents('tr').attr('data-campaign-id');

            $('#campaign-modal').attr('campaign-id', id);
            $('#campaign-modal').find('.campaign-content').attr('hidden', '');
            $('#campaign-modal').modal('show');

        });
    }
    function EditAdvertisement() {
        $(document).on('click', '.edit-advertisement', function (e) {
            var advertisementId = $(this).parents('.accordion').attr('advertisementId');
            var campaignId = $(this).parents('tr').attr('data-campaign-id');
            var modal = $('#campaign-modal');
            modal.attr('campaign-id', campaignId);
            modal.attr('advertisement-id', advertisementId)
            $.ajax({
                type: "GET",
                url: "/Campaign/GetAdvertisement",
                data: { id: advertisementId  },
                dataType: "json",
                async: true,
                success: function (data) {
                    var advertisement = data.advertisement;
                    modal.find('.title').val(advertisement.Title);
                    modal.find('.description').val(advertisement.Description);
                    modal.find('.bedrooms').val(advertisement.Bedrooms);
                    modal.find('.bathrooms').val(advertisement.Bathrooms);
                    modal.find('.housing-type-id').dropdown("set selected",advertisement.HousingTypeId);
                    modal.find('.laundry-id').dropdown("set selected",advertisement.LaundryId)
                    modal.find('.parking-id').dropdown("set selected",advertisement.ParkingId)
                    modal.find('.rent-period-id').dropdown("set selected",advertisement.RentPeriodId)
                    modal.find('.floor-id').dropdown("set selected",advertisement.FlooringId);
                    modal.find('.geographic-area').val(advertisement.GeographicArea);
                    modal.find('.postal-code').val(advertisement.PostalCode);
                    modal.find('.surface-area').val(advertisement.SurfaceArea);
                    modal.find('.advertisement-site').dropdown('set selected', [advertisement.StringPostToSite]);
                    modal.find('.geographic-area').val(advertisement.GeographicArea);
                    modal.find('.postal-code').val(advertisement.PostalCode);
                    modal.find('.surface-area').val(advertisement.SurfaceArea);
                    modal.find('.advertisement-property').dropdown('set selected', advertisement.PropertyId);
                    modal.find('.email').val(advertisement.FromEMail);
                    modal.find('.price').val(advertisement.Price);
                    modal.find('.movein-date').val(FormatDate(advertisement.MoveinDate));
                    modal.find('.sale-date1').val(FormatDate(advertisement.SaleDate1));
                    modal.find('.sale-date2').val(FormatDate(advertisement.SaleDate2));
                    modal.find('.sale-date3').val(FormatDate(advertisement.SaleDate3));
                    modal.find('.longitude', advertisement.Longitude);
                    modal.find('.latitude', advertisement.Latitude);
                    modal.attr('advertisment-id', advertisement.Id);
                    modal.find('.lamudi-property-type').dropdown('set selected', advertisement.PropertyType);
                    modal.find('.lamudi-property-sub-type').dropdown('set selected', advertisement.PropertySubType);
                    modal.find('.lamudi-state').val(advertisement.State);
                    modal.find('.lamudi-city').val(advertisement.City);
                    modal.find('.lamudi-brgy').val(advertisement.Brgy);
                    modal.find('.lamudi-address').val(advertisement.Address);
                    modal.find('.lamudi-youtube').val(advertisement.YoutubeUrl);
                    modal.find('.lamudi-tour').val(advertisement.TourUrl);
                    modal.modal('show');
                    $('.ui.calendar').calendar({
                        type: 'date'
                    });
                }
            });
            modal.find('.campaign-content').attr('hidden', '');
            modal.modal('show');
            
        });
        
	}

    function SaveCampaign() {
        $(document).on('click', '#save-campaign-btn', function (e) {
            $('#save-campaign-btn').addClass('loading');
            var formData = new FormData();
            var modal = $('#campaign-modal');
            formData.append("Id", modal.attr('campaign-id'));
            formData.append("Name", modal.find('.campaign-name').val());
            //Data of adds
            formData.append("Advertisement.Id", modal.attr('advertisement-id'));
            formData.append("Advertisement.StringPostToSite", modal.find('.advertisement-site').dropdown("get value"))
            formData.append("Advertisement.Title", modal.find('.title').val());
            formData.append("Advertisement.Description", modal.find('.description').val());
            formData.append("Advertisement.Bedrooms", modal.find('.bedrooms').val());
            formData.append("Advertisement.Bathrooms", modal.find('.bathrooms').val());
            formData.append("Advertisement.HousingTypeId", modal.find('.housing-type-id').dropdown('get value'));
            formData.append("Advertisement.LaundryId", modal.find('.laundry-id').dropdown('get value'));
            formData.append("Advertisement.FlooringId", modal.find('.floor-id').dropdown('get value'));
            formData.append("Advertisement.ParkingId", modal.find('.parking-id').dropdown('get value'));
            formData.append("Advertisement.RentPeriodId", modal.find('.rent-period-id').dropdown('get value'));
            formData.append("Advertisement.GeographicArea", modal.find('.geographic-area').val());
            formData.append("Advertisement.PostalCode", modal.find('.postal-code').val());
            formData.append("Advertisement.SurfaceArea", modal.find('.surface-area').val());
            formData.append("Advertisement.FromEMail", modal.find('.email').val());
            formData.append("Advertisement.MoveinDate", modal.find('.movein-date').val());
            formData.append("Advertisement.SaleDate1", modal.find('.sale-date1').val());
            formData.append("Advertisement.SaleDate2", modal.find('.sale-date2').val());
            formData.append("Advertisement.SaleDate3", modal.find('.sale-date3').val());
            formData.append("Advertisement.Price", modal.find('.price').val());
            formData.append("Advertisement.PropertyType", modal.find('.lamudi-property-type').dropdown('get value'));
            formData.append("Advertisement.PropertySubType", modal.find('.lamudi-property-sub-type').dropdown('get value'));
            formData.append("Advertisement.State", modal.find('.lamudi-state').val());
            formData.append("Advertisement.City", modal.find('.lamudi-city').val());
            formData.append("Advertisement.Brgy", modal.find('.lamudi-brgy').val());
            formData.append("Advertisement.Address", modal.find('.lamudi-address').val());
            formData.append("Advertisement.YoutubeUrl", modal.find('.lamudi-youtube').val());
            formData.append("Advertisement.TourUrl", modal.find('.lamudi-tour').val());
            formData.append("Advertisement.PropertyId", modal.find('.advertisement-property').dropdown('get value'))
            $.ajax({
                type: "POST",
                url: '/Campaign/SaveCampaign',
                contentType: false,
                cache: false,
                dataType: "json",
                processData: false,
                data: formData,
                success: function (data) {
                    $('#save-campaign-btn').removeClass('loading');
                    if (data.success) {
                        $('#campaign-modal').modal('hide');
                        campaignTable.ajax.reload(null, false);
                    }
                }
            });
        });
    }
});