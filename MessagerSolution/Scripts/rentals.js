﻿
$(document).ready(function () {

	//var editor = new Quill('#paragraph', {
	//	theme: 'snow'
	//});
	//$('#paragraph').jqte();
	//$('#notice-content').jqte();
	var tableProperty;
	var tableNotices;
	var tableNoticeDocuments;
	var tableRequestBooking;
	$('.ui.accordion').accordion({
		animateChildren: false
	});
	$('.menu .item').tab({
		onVisible: function (tabPath) {
			if (tabPath == "tab-messages") {
				$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
			}
		}
	});
	$('.ui.dropdown').dropdown({
		"clearable": true
	});
	PropertyFilterChange();
	LoadPropertyTable();
	LoadPropertiesForMaintenanceSchedules();
	ShowMaintenanceModal();
	ShowScheduleExpenseModal();
	ScheduleMaintenanceSubmit();
	ScheduleExpenseSubmit();
	OnEditScheduleMaintenance();
	OnEditScheduleExpense();
	onEditIncome();
	onIncomeSubmit();
	onExpenseSubmit();
	onEditExpense();
	//Maintenance entry
	onEditMaintenanceEntry();
	MaintenanceEntrySubmit();
	OnchangeMaintenanceEntryImage();
	OnDeleteMaintenanceEntry();
	AddNote();
	RemoveNote();
	GetExpenseCategory();
	GetPaymentMthod();
	IncomeExpenseStatusChange();
	AddPaymentMethod();
	Notices();
	LoadRequestBooking();
	ViewRequestBooking();
	RejectContractRequest();
	//CreateContractFromRequest()
	function ViewRequestBooking() {
		$(document).on('click', '.view-request-booking', function (e) {
			var id = $(this).parents('tr').attr('data-request-id');
			var propertyId = $(this).parents('tr').attr('data-property-id');
			$.ajax({
				type: 'GET',
				url: "/Property/GetRequestContract",
				data: { id: id },
				dataType: "json",
				async: false,
				success: function (data) {
					var contract = data.contract;
					var renters = data.renters;
					$('#contract-start').text("Start Date:" + (contract.StartDate == null ? '' : moment(contract.StartDate).format('MMMM D,YYYY')));
					$('#contract-end').text("End Date:" + (contract.EndDate == null ? '' : moment(contract.EndDate).format('MMMM D,YYYY')));
					$('#renter-request-list').empty();

					for (var x = 0; x < renters.length; x++) {
						var job = '';
						var address = '';
						for (var y = 0; y < renters[x].JobList.length; y++) {
							job += renters[x].JobList[y] + '<br>';
						}
						for (var y = 0; y < renters[x].AddressList.length; y++) {
							address += renters[x].AddressList[y] + '<br>';
						}
						var html = '';
						for (var i = 0; i < renters[x].Files.length; i++) {
							if ($.trim(renters[x].Files[i].Description) != null && $.trim(renters[x].Files[i].Description) != '') {
								html += '<div class="ui fields"><div class="ui field">Description: ' + renters[x].Files[i].Description + '</div></div>';
								html += "<div class='ui fields'>";
								for (var y = 0; y < renters[x].Files[i].Files.length; y++) {
									var file = renters[x].Files[i].Files[y];
									html += "<div class='ui field'>" + (ImageExist(file) ? '<img class="zoom ui tiny image" src="' + file + '">' : '<a target="_blank" href="' + file + '"><i class="huge icon ' + FileIcon(file) + '"></i></a>') + "</div>";
								}
								html += '</div>';
							}
						}
						$('#renter-request-list').append('<hr /><label>Name:' + renters[x].Name + '</label><br><label>Address:' + renters[x].Address + '</label><br><label>Email:' + renters[x].Email + '</label><br><label>Phone#:' + renters[x].PhoneNumber + '</label><br><label>Email:' + renters[x].Email + '</label><br><label>Phone#:' + renters[x].PhoneNumber + '</label><br><h4>Job(s):</h4>' + job + '<br><h4>Address:</h4>' + address + '<h4>Attachment(s)</h4>' + html);
					}
					$('#request-booking-modal').attr('request-booking-id', contract.Id);
					//$('#request-booking-modal').attr('request-property-id', contract[x].PropertyId);
					$('#create-contract-submit').attr('href', 'https://localhost:44301/Home/PropertyPayment?pid=' + contract.PropertyId + '&requestid=' + contract.Id + '')
					$('#request-booking-modal').modal('show').modal('refresh');
				}
			});
		});

	}
	function RejectContractRequest() {
		$(document).on('click', '#reject-request-contract', function (e) {
			var id = $('#request-booking-modal').attr('request-booking-id');
			$.ajax({
				type: 'POST',
				url: "/Property/RejectRequestContract",
				data: { id: id },
				dataType: "json",
				async: false,
				success: function (data) {
					tableRequestBooking.ajax.reload(null, false);
				}
			});
		});
	}

	function LoadRequestBooking() {
		tableRequestBooking = $('#table-request-booking').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Property/GetRequestContracts",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "PropertyName" },
				{ "data": "RenterName" },
				{ "data": "StartDate" },
				{ "data": "EndDate" },
				{ "data": "RequestDate" },
				{ "data": "Status" },
				{ "data": "Actions" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-request-id', data.Id);
				$(row).attr('data-property-id', data.PropertyId);
			}, columnDefs: [{
				targets: [4], type: "date", render: function (data) {
					if (data == null) {
						return '';
					} else {
						return moment(data).format('MMMM D,YYYY hh:mm a');
					}
				}
			},
			{
				targets: [2, 3], type: "date", render: function (data) {
					if (data == null) {
						return '';
					} else {
						return moment(data).format('MMMM D,YYYY');
					}
				}
			}, {
				targets: [5], type: "text", render: function (data) {
					if (data == 0) {
						return 'Pending';
					} else if (data == 1) {
						return 'Accepted';
					}
					else { return 'Rejected' }
				}
			}]
		});
	}

	function Notices() {
		LoadNotices();
		AddNotice();
		AddNoticeSubmit();
		GetNoticeDetails();
		OnChangeNoticeDocument();
		AddNoticeType()
		AddDeliveryMethod();
		GetNoticeTypes();
		GetNoticeDeliveryMethod();
		GetRenters();
		AddDocumentTemplate();
		SaveDocumentTemplate();
		LoadNoticeDocuments();
		GetDocumentTemplates();
		OnAddTemplateVariable();
		DeleteNoticeTemplate();
		DeleteNotice();
		EditNoticeTemplate();
		SaveGeneratedTemplate();
	}
	//JM 07/30/19 Renter Messages Event
	function OnChangeNoticeDocument() {
		$(document).on('change', '#notice-document', function (e) {
			$('#notice-file-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				$('#notice-file-upload').append('<a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a>');
			}
		});
	}


	//JM For future purpose
	//function onRenterSendMessageClick() {
	//	$(document).on('click', '#send-btn', function () {
	//		var threadId = $('.active-thread').attr('data-thread-id');
	//		var workerId = $('.active-thread').attr('data-id');
	//		var isWorker = $('.active-thread').attr('is-worker');
	//		var msg = $('#txtMsgContent').val();
	//		if (msg != '') {
	//			$.ajax({
	//				type: 'POST',
	//				url: "/Messages/WorkerSendMessage",
	//				data: { workerId: workerId, threadId: threadId, messege: msg, isworker: isWorker },
	//				dataType: "json",
	//				beforeSend: function () {
	//					$('#worker-send-btn').addClass('loading');
	//				},
	//				success: function (data) {

	//					if (data.success) {
	//						$('#txtMsgContentWorker').css('border-color', 'rgba(34,36,38,.15)');
	//						$('#txtMsgContentWorker').val('');
	//						$('#worker-dvConvo').scrollTop($('#worker-dvConvo')[0].scrollHeight);
	//						$('#worker-send-btn').removeClass('orange').addClass('teal');
	//						$('#worker-send-btn').text('Send');
	//						$('.worker-active-thread').find('.content').find('.text').text(msg);
	//						$('.worker-active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
	//					}
	//					else {
	//						$('#txtMsgContentWorker').css('border-color', 'orange');
	//						$('#worker-send-btn').removeClass('teal').addClass('orange');
	//						$('#worker-send-btn').text('Retry');
	//					}
	//					$('#worker-send-btn').removeClass('loading');
	//					$('.worker-active-thread').attr('data-thread-id', data.threadId);
	//					$('#worker-inbox-list').attr('data-active-thread-id', data.threadId);
	//				}
	//			});
	//		}

	//	});
	//}

	function GetPaymentMthod() {
		$.ajax({
			type: "GET",
			url: "/Expense/GetPaymentMethods",

			success: function (data) {
				var paymentMethods = data.paymentMethods;
				$('#schedule-payment-method').empty();
				$('#PaymentMethodId').empty();
				$('#maintenance-payment-method').empty();
				var optionData = '<option value="">Select Payment Method</option>';
				for (var i = 0; i < paymentMethods.length; i++) {
					optionData += '<option value="' + paymentMethods[i].Id + '">' + paymentMethods[i].Name + '</option>';
				}
				optionData += '<option class="add-payment-method">New Category...</option>';
				$('#schedule-payment-method').append(optionData);
				$('#PaymentMethodId').append(optionData);
				$('#maintenance-payment-method').append(optionData);
				$('#schedule-payment-method').dropdown('clear')
				$('#PaymentMethodId').dropdown('clear')
				$('#maintenance-payment-method').dropdown('clear')


			}
		});
	}
	function AddPaymentMethod() {
		$(document).on('change', '#schedule-payment-method,#PaymentMethodId,#maintenance-payment-method,#maintenance-entry-payment-method', function () {
			if ($(this).find('option:selected').attr('class') == 'add-payment-method') {
				swal({
					title: "Add Payment Method",
					type: "input",
					confirmButtonColor: "##abdd54",
					confirmButtonText: "Add",
					showCancelButton: true,
					closeOnConfirm: true,
					showLoaderOnConfirm: true
				},
					function (message) {
						if (message) {
							$.ajax({
								type: 'POST',
								url: "/Expense/CreatePaymentMethod",
								data: { id: 0, name: message },
								success: function (data) {
									if (data.success) {
										GetPaymentMthod();
									}
									else {
										swal("Error occured on deleting payment method", "", "error");
									}
								}
							});
						}
					});
			}
		});
	}
	function AddNoticeType() {
		$(document).on('change', '#notice-type', function () {
			if ($(this).find('option:selected').attr('class') == 'add-notice-type') {
				swal({
					title: "Add Notice Type",
					type: "input",
					confirmButtonColor: "##abdd54",
					confirmButtonText: "Add",
					showCancelButton: true,
					closeOnConfirm: true,
					showLoaderOnConfirm: true
				},
					function (message) {
						if (message) {
							$.ajax({
								type: 'POST',
								url: "/Rentals/CreateNoticeType",
								data: { id: 0, name: message },
								success: function (data) {
									if (data.success) {
										GetNoticeTypes();
									}
								}
							});
						}
					});
			}
		});
	}
	function AddDeliveryMethod() {
		$(document).on('change', '#notice-delivery-method', function () {
			if ($(this).find('option:selected').attr('class') == 'add-delivery-method') {
				swal({
					title: "Add Delivery Method",
					type: "input",
					confirmButtonColor: "##abdd54",
					confirmButtonText: "Add",
					showCancelButton: true,
					closeOnConfirm: true,
					showLoaderOnConfirm: true
				},
					function (message) {
						if (message) {
							$.ajax({
								type: 'POST',
								url: "/Rentals/CreateNoticeDeliveryMethod",
								data: { id: 0, name: message },
								success: function (data) {
									if (data.success) {
										GetNoticeDeliveryMethod();
									}
								}
							});
						}
					});
			}
		});
	}
	function GetExpenseCategory() {
		$.ajax({
			type: "GET",
			url: "/Expense/GetExpenseCategory",

			success: function (data) {
				var expenseCategory = data.expenseCategory;
				$('#schedule-expense-category').empty();
				$('#ExpenseCategoryId').empty();
				$('#maintenance-expense-category').empty();
				var optionData = '<option value="">Select Category</option>';
				for (var i = 0; i < expenseCategory.length; i++) {
					optionData += '<option value="' + expenseCategory[i].Id + '">' + expenseCategory[i].ExpenseCategoryType + '</option>';
				}
				optionData += '<option class="add-expense-category">New Category...</option>';
				$('#schedule-expense-category').append(optionData);
				$('#maintenance-expense-category').append(optionData);
				$('#ExpenseCategoryId').append(optionData);
				$('#ExpenseCategoryId').dropdown('clear');
				$('#schedule-expense-category').dropdown('clear');
				$('#maintenance-expense-category').dropdown('clear');
				AddExpenseCategory();

			}
		});
	}
	function AddExpenseCategory() {
		$(document).on('change', '#schedule-expense-category,#maintenance-entry-expense-category,#maintenance-expense-category', function () {
			if ($(this).find('option:selected').attr('class') == 'add-expense-category') {
				swal({
					title: "Add Expense Category",
					type: "input",
					confirmButtonColor: "##abdd54",
					confirmButtonText: "Add",
					showCancelButton: true,
					closeOnConfirm: true,
					showLoaderOnConfirm: true
				},
					function (message) {
						if (message) {
							$.ajax({
								type: "POST",
								url: "/Expense/CreateExpenseCategory",
								data: {
									categoryName: message,
									type: 1,
									companyTerm: 1
								},
								success: function (data) {
									if (data.success) {
										GetExpenseCategory();
										swal({ title: "Success", text: "Expense category has been saved.", type: "success" },
											function () {
												$('#category-name').val('');
											});
									}
									else {
										$('#create-expense-category-modal').modal('hide');
										swal("Error on add expense category", "", "error");
									}
								}
							});
						}
					});
			}
		});
	}
	function FileIcon(url) {
		var extention = url.split('.').pop();
		if (extention == "pdf") {
			return "file pdf outline";
		}
		else if (extention == "docx" || extention == "docm" || extention == "dot") {
			return "file word outline";
		}
		else if (extention == "xlsx" || extention == "xlsm" || extention == "xlsb") {
			return "file excel outline";
		}
		else {
			return "file alternate";
		}
	}
	function ImageExist(url) {
		var imageExtentions = ['jpg', 'png', 'gif'];
		var extention = url.split('.').pop();
		if (jQuery.inArray(extention, imageExtentions) !== -1) {
			return true;
		}
		else {
			return false;
		}

	}

	//OnChangeFrequency();
	function IncomeExpenseStatusChange() {
		$(document).on('change', '.income-expense-status', function (e) {
			var search = $(this).val();
			var content = $(this).parent('.content');
			var expenseTable = content.find('[expense-table-propert-id]').DataTable();
			var incomeTable = content.find('[income-table-propert-id]').DataTable();
			//Income search
			$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					return (search == "All" ? true : data[9] == search ? true : false);
				});
			incomeTable.draw();
			$.fn.dataTable.ext.search.pop();
			//Expense Search
			$.fn.dataTable.ext.search.push(
				function (settings, data, dataIndex) {
					return (search == "All" ? true : data[10] == search ? true : false);
				});
			expenseTable.draw();
			$.fn.dataTable.ext.search.pop();

		});
	}
	function MaintenanceSchedulesAccordionClick() {
		$('.accordion-title-schedules').on('click', function () {
			var parent = $(this).parent();
			parent.accordion({
				animateChildren: false
			});
			var propertyId = $(this).attr('property-Id');
			GetExpense(propertyId);
			GetMaintenanceEntries(propertyId);
			GetIncome(propertyId);
			GetScheduledMaintenanceEvent(propertyId);
			GetScheduledExpensesEvent(propertyId);
			parent.find('.menu .item').tab();
			if ($.fn.dataTable.isDataTable(parent.find('.datatable')) == false) {
				parent.find('.datatable').DataTable({ "ordering": false });
			}
		});
	}
	function MaintenanceEntriesAccordionClick() {
		$('.accordion-title-entries').on('click', function () {
			var parent = $(this).parent();
			parent.accordion({
				animateChildren: false
			});
			var propertyId = $(this).attr('property-Id');
			GetExpense(propertyId);
			GetMaintenanceEntries(propertyId);
			GetIncome(propertyId);
			parent.find('.menu .item').tab();
			if ($.fn.dataTable.isDataTable(parent.find('.datatable')) == false) {
				parent.find('.datatable').DataTable({ "ordering": false });
			}
			IncomeExpenseStatusChange();
		});
	}
	function ValidateExpenseEntryRequiredFields() {
		var hasBlankField = false;
		var hasfirstFocus = false;
		$('#create-expenses-modal').find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				//obj.next().remove();
				obj.attr("style", "border-color:red");
				//obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.removeAttr("style");
			}

		});
		return hasBlankField;
	}
	function onExpenseSubmit() {
		$('#expense-submit-btn').on('click', function () {

			var hasBlankField = ValidateExpenseEntryRequiredFields();
			if (hasBlankField) {
				return;
			}
			var Id = $('#ExpenseId').val();
			var Description = $('#Expense-Description').val();
			var ExpenseCategoryId = $('#ExpenseCategoryId').val();
			var PropertyId = $('#Expense-PropertyId').val();
			var Amount = $('#Expense-Amount').val();
			var DueDate = $('#Expense-DueDate').val();
			var PaymentDate = $('#PaymentDate').val();
			var IsPaid = $('#IsPaid').val();
			var PaymentMethodId = $('#PaymentMethodId').val();
			if (Description != '' && ExpenseCategoryId != '' && PropertyId != '' && Amount != '' && DueDate != '' /*&& PaymentDate != ''*/ && IsPaid != '') {
				$.ajax({
					type: "POST",
					url: "/Expense/Edit",
					data: {
						Id: Id,
						Description: Description,
						ExpenseCategoryId: ExpenseCategoryId,
						PropertyId: PropertyId,
						Amount: Amount,
						DueDate: DueDate,
						PaymentDate: PaymentDate,
						IsPaid: IsPaid,
						PaymentMethodId: PaymentMethodId
					},
					success: function (data) {
						if (data.success) {
							$('#create-expenses-modal').modal('hide');
							swal({ title: "Success", text: "Changes to expense has been saved.", type: "success" },
								function () {
									ClearModalForm();
									GetMaintenanceEntries(PropertyId);
									GetExpense(PropertyId);
								});
						}
						else {
							$('#create-expenses-modal').modal('hide');
							swal("Error on edit expense", "", "error");
						}
					}
				});

			}
			else {
				swal("Please fill all fields", "", "error");
			}

		});
	}
	function onEditExpense() {
		$(document).on('click', '.edit-expense-btn', function () {
			var id = $(this).parents('tr').attr("data-expense-id");
			$.ajax({
				type: "POST",
				url: "/Expense/Details",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						ClearModalForm();
						$('#ExpenseId').val(data.expense.Id);
						$('#Expense-Description').val(data.expense.Description);
						$('#ExpenseCategoryId').dropdown('set selected', data.expense.ExpenseCategoryId);
						$('#Expense-PropertyId').dropdown('set selected', data.expense.PropertyId);
						$('#Expense-Amount').val(data.expense.Amount);
						$('#Expense-DueDate').val(moment.utc(data.expense.DueDate).format('MMMM, D YYYY'));
						//$('#PaymentDate').val(moment.utc(data.expense.PaymentDate).format('MMMM, D YYYY'));
						$('#IsPaid').dropdown('set selected', data.expense.IsPaid);
						$('#PaymentMethodId').dropdown('set selected', data.expense.PaymentMethodId);
						if (data.expense.Frequency != 0) {
							$('#Frequency').dropdown('set selected', data.expense.Frequency);
						}
						$('#IsStartupCost').prop('checked', data.expense.IsStartupCost);
						$('#IsCapitalExpense').prop('checked', data.expense.IsCapitalExpense);
						$('#create-expenses-modal').modal('show').modal('refresh');
						$('.ui.calendar').calendar({ type: 'date' });
					}
				}
			});
		});
	}
	function OnchangeMaintenanceEntryImage() {
		$(document).on('change', '#proof-pic', function (e) {
			$('#image-upload').empty();
			for (var i = 0; i < e.target.files.length; i++) {
				var fileUrl = window.URL.createObjectURL(this.files[i]);
				var type = this.files[i]['type'].split('/')[0];
				if (type == 'image') {
					$('#image-upload').append('<div class="ui small image"><i class="close icon"></i><img src="' + fileUrl + '"><div>');
				}
				else {
					$('#image-upload').append('<a target="_blank" href="' + fileUrl + '"><i class="large icon file"></i></a>');
				}
			}
		});
	}

	function ValidateMaintenanceEntryRequiredFields() {
		var hasBlankField = false;
		var hasfirstFocus = false;

		$('#maintenance-entry-modal').find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.attr("style", "border-color:red");
				//obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.removeAttr("style");
			}

		});
		return hasBlankField;
	}
	function MaintenanceEntrySubmit() {
		$(document).on("click", "#maintenance-entry-submit-btn", function () {
			var hasBlankField = ValidateMaintenanceEntryRequiredFields();
			if (hasBlankField) {
				return;
			}
			var id = $('#maintenance-entry-modal').attr('maintenance-entry-id');
			var formdata = new FormData();
			formdata.append("Id", id);
			formdata.append("Description", $('#maintenance-entry-description').val());
			formdata.append("DueDate", $('#maintenance-entry-date').val());
			formdata.append("Amount", $('#maintenance-entry-amount').val());
			formdata.append("WorkerId", $('#maintenance-entry-worker').val());
			formdata.append("JobTypeId", $('#maintenance-entry-jobtype').val());
			formdata.append("VendorId", $('#maintenance-entry-vendor').val());
			formdata.append("DateStart", $('#maintenance-entry-date-start').val());
			formdata.append("DateEnd", $('#maintenance-entry-date-end').val());
			formdata.append("Status", $('#maintenance-entry-status').val());
			for (var i = 0; i < $('#proof-pic').get(0).files.length; ++i) {
				formdata.append("Images[" + i + "]", $('#proof-pic').get(0).files[i]);
			}

			var notes = [];
			$('.note').each(function (index, element) {
				var note = $(this).find('.note-description').val();
				if (note != null && note != "") {
					var jsonFee = {
						Id: $(this).find('.fields').attr('note-id'),
						Note: note,
						MaintenanceEntryId: 0
					};
					notes.push(jsonFee);
				}
			});
			formdata.append('NoteString', JSON.stringify(notes));
			for (var pair of formdata.entries()) {
				console.log(pair[0] + ', ' + pair[1]);
			}
			$.ajax({
				type: "POST",
				url: "/Rentals/EditMaintenanceEntry",
				data: formdata,
				cache: false,
				processData: false,
				contentType: false,
				success: function (data) {
					swal("Data Saved.", "", "success");
					$('#maintenance-entry-modal').modal('hide');
					GetExpense(data.maintenance.PropertyId);
					GetMaintenanceEntries(data.maintenance.PropertyId);

				}
			});
		});
	}

	function OnDeleteMaintenanceEntry() {
		$(document).on("click", ".delete-maintenance-entry", function () {
			var id = $(this).parents('tr').attr("maintenance-entry-id");
			swal({
				title: "Are you sure you want to delete maintenance entry?",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showCancelButton: true,
				closeOnConfirm: true,
				showLoaderOnConfirm: true
			},
				function () {
					$.ajax({
						type: "POST",
						url: "/Rentals/DeleteMaintenanceEntry",
						data: { id: id },
						success: function (data) {
							GetExpense(data.PropertyId);
							GetMaintenanceEntries(data.PropertyId);
						}
					});
				});
		});

	}
	function onEditMaintenanceEntry() {
		$(document).on('click', '.edit-maintenance-entry-btn', function () {
			var id = $(this).parents('tr').attr("maintenance-entry-id");
			$.ajax({
				type: "GET",
				url: "/Rentals/MaintenanceEntryDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						var maintenance = data.maintenance;
						var property = data.property;
						var images = data.images;
						var notes = data.notes;
						$('#note-div').empty();
						if (notes.length == 0) {
							$('#note-div').append(noteTemplate);
							$('.remove-note').parent().remove();
						}
						else {
							for (var x = 0; x < notes.length; x++) {
								$('#note-div').append('<div class="note"><div class="fields" note-id="' + notes[x].Id + '">' +
									'<div class="five wide field">' +
									'<label>Note</label>' +
									'<input autocomplete="off" type="text" name="note" class="note-description" placeholder="Note" value="' + notes[x].Note + '"/>' +
									'</div>' +
									'<div class="one wide field">' +
									'<label>&nbsp;</label>' +
									'<button type="button" class="circular ui icon blue mini button add-note" data-tooltip="Add Note">' +
									'<i class="plus icon"></i>' +
									'</button>' +
									'</div>' +
									(x != 0 ? '<div class="one wide field">' +
										'<label>&nbsp;</label>' +
										'<button type="button" class="circular ui icon orange mini button remove-note" data-tooltip="Remove">' +
										'<i class="minus icon"></i>' +
										'</button>' +
										'</div>' : '') +
									'</div></div>');
							}
						}
						$('#image-upload').empty();
						for (var x = 0; x < images.length; x++) {
							$('#image-upload').append('<div class="ui small image"><i class="close icon"></i><a target="_blank" href="' + images[x] + '"><img src="' + images[x] + '"></a></div>');
						}
						$('#maintenance-entry-modal').attr('maintenance-entry-id', maintenance.Id);
						$('#maintenance-entry-propertyId').val(property.Name);
						$('#maintenance-entry-description').val(maintenance.Description);
						$('#maintenance-entry-amount').val(maintenance.Amount);
						$('#maintenance-entry-date').val(moment.utc(maintenance.DueDate).format('MMMM D,YYYY'));
						//$('#maintenance-entry-expense-category').dropdown('set selected', maintenance.);
						//$('#maintenance-entry-payment-method').dropdown('set selected', maintenance.);
						$('#maintenance-entry-worker').dropdown('set selected', maintenance.WorkerId);
						$('#maintenance-entry-jobtype').dropdown('set selected', maintenance.JobTypeId);
						$('#maintenance-entry-vendor').dropdown('set selected', maintenance.VendorId);
						$('#maintenance-entry-date-start').val(moment.utc(maintenance.DateStart).format('MMMM D,YYYY'));
						$('#maintenance-entry-date-end').val(moment.utc(maintenance.DateEnd).format('MMMM D,YYYY'));
						$('#maintenance-entry-status').val(maintenance.Status);
						$('#maintenance-entry-modal').modal('show').modal('refresh');
						$('.ui.calendar.maintenance-entry-dates').calendar({ type: 'date' });

					}
				}
			});
		});
	}

	function onEditIncome() {
		$(document).on('click', '.edit-income-btn', function () {
			var id = $(this).parents('tr').attr("data-income-id");
			$.ajax({
				type: "GET",
				url: "/Income/Details",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						$('#income-id').val(data.income.Id);
						$('#PropertyId').dropdown('set selected', data.income.PropertyId);
						$('#PropertyId').attr('disabled', true);
						$('#IncomeTypeId').dropdown('set selected', data.income.IncomeTypeId);
						$('#Status').dropdown('set selected', data.income.Status);
						$('#Description').val(data.income.Description);
						$('#Amount').val(data.income.Amount);
						$('#DueDate').val(moment.utc(data.income.DueDate).format('MMMM, D YYYY'));
						$('#income-modal').modal('show').modal('refresh');
						$('.ui.calendar.income').calendar({ type: 'date' });
					}
				}
			});
		});
	}

	//Income Entry validation
	$(document).on('keyup', '[required]', function () {

		if ($(this).val().length == 0) {
			$(this).attr("style", "border-color:red");
			$(this).next().remove();
			$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).next().remove();
			$(this).removeAttr("style");
		}
	});
	$(document).on('change', '#Status, #IncomeTypeId, #PropertyId', function () {

		if ($(this).val().length == 0) {
			$(this).parent().next().remove();
			$(this).parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).parent().next().remove();
		}
	});
	function ValidateIncomeEntryRequiredFields() {
		var hasBlankField = false;
		var hasfirstFocus = false;
		property = $('#PropertyId');
		if (property.val() == "" || property.val() == null) {
			property.parent().next().remove();
			property.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				property.focus();
			}
			hasBlankField = true;
		}
		else {
			property.parent().next().remove();
		}
		var incomeType = $('#IncomeTypeId');
		if (incomeType.val() == "" || incomeType.val() == null) {
			incomeType.parent().next().remove();
			incomeType.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				incomeType.focus();
			}
			hasBlankField = true;
		}
		else {
			incomeType.parent().next().remove();
		}
		var status = $('#Status');
		if (status.val() == "" || status.val() == null) {
			status.parent().next().remove();
			status.parent().after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
			if (hasfirstFocus == false) {
				hasfirstFocus = true;
				status.focus();
			}
			hasBlankField = true;
		}
		else {
			status.parent().next().remove();
		}
		$('#income-modal').find('[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.next().remove();
				obj.after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.next().remove();
			}

		});
		return hasBlankField;
	}
	function onIncomeSubmit() {
		$('#income-save-btn').on('click', function () {
			var hasBlankField = ValidateIncomeEntryRequiredFields();
			if (hasBlankField) {
				return;
			}
			var id = $('#income-id').val();
			var propertyId = $('#PropertyId').val();
			var incomeTypeId = $('#IncomeTypeId').val();
			var description = $('#Description').val();
			var amount = $('#Amount').val();
			var dueDate = $('#DueDate').val();
			//var status = $('#Status').val();

			if (id == '') // Add
			{
				if (propertyId != '' && incomeTypeId != '' && description != '' && amount != '' && dueDate != '') {
					$.ajax({
						type: "POST",
						url: "/Income/Create",
						data: {
							IncomeTypeId: incomeTypeId,
							BookingId: 0,
							PropertyId: propertyId,
							Description: description,
							Amount: amount,
							Status: status,
							DueDate: dueDate
						},
						success: function (data) {
							if (data.success) {
								$('#income-modal').modal('hide');
								swal({ title: "Success", text: "Income has been saved.", type: "success" },
									function () {
										clearIncomeModalForm();
										GetIncome(propertyId);
									});
							}
							else {
								$('#income-modal').modal('hide');
								swal("Error in saving new income", "", "error");
							}
						}
					});
				}
				else {
					swal("Please fill all fields", "", "error");
				}
			}
			else // Edit
			{
				if (id != '') {
					$.ajax({
						type: "POST",
						url: "/Income/Edit",
						data: {
							Id: id,
							IncomeTypeId: incomeTypeId,
							BookingId: 0,
							PropertyId: propertyId,
							Description: description,
							Amount: amount,
							Status: status,
							DueDate: dueDate
						},
						success: function (data) {
							if (data.success) {
								$('#income-modal').modal('hide');
								swal({ title: "Success", text: "Income has been saved.", type: "success" },
									function () {
										clearIncomeModalForm();
										GetIncome(propertyId);
									});
							}
							else {
								$('#income-modal').modal('hide');
								swal("Error on edit income", "", "error");
							}
						}
					});
				}
			}
		});
	}
	function clearIncomeModalForm() {
		$('#PropertyId').dropdown('clear');
		$('#IncomeTypeId').dropdown('clear');
		$('#Status').dropdown('clear');
		$('#Description').val('');
		$('#Amount').val('');
		$('#PaymentDate').val('');
		$('#income-id').val('');
	}

	var noteTemplate = '<div class="note"><div class="fields" note-id="0">' +
		'<div class="five wide field">' +
		'<label>Note</label>' +
		'<input autocomplete="off" type="text" name="note" class="note-description" placeholder="Note"/>' +
		'</div>' +
		'<div class="one wide field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-note" data-tooltip="Add Note">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'</div>' +
		'<div class="one wide field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon orange mini button remove-note" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div></div>';
	function AddNote() {
		$(document).on('click', '.add-note', function () {
			$('#note-div').append(noteTemplate);
		});
	}
	function RemoveNote() {
		$(document).on('click', '.remove-note', function () {
			$(this).parents('.fields').remove();
		});
	}

	function OnEditScheduleMaintenance() {
		$(document).on("click", ".edit-maintenance", function () {
			var id = $(this).parents('tr').attr('maintenance-id');
			$.ajax({
				type: "GET",
				url: "/Rentals/GetScheduledExpense",
				data: { Id: id },
				success: function (data) {
					var details = data.details;
					var property = data.property;
					$('#maintenance-id').val(details.Id);
					$('#maintenance-description').val(details.Description);
					$('#maintenance-interval').val(details.Interval);
					$('#maintenance-frequency').val(details.Frequency).change();
					$('#maintenance-propertyId').val(property.Name);
					$('#maintenance-propertyId').attr('propertyId', property.Id);
					$('#maintenance-recurring-start-date').val(moment.utc(details.RecurringDate).format("MMMM D,YYYY"));
					$('#maintenance-recurring-end-date').val(moment.utc(details.RecurringEndDate).format("MMMM D,YYYY"));
					$('#maintenance-amount').val(details.Amount);
					$('#maintenance-expense-category').dropdown("set selected", details.ExpenseCategoryId);
					$('#maintenance-payment-method').dropdown("set selected", details.PaymentMethodId);
					$('#worker').dropdown("set selected", details.WorkerId);
					$('#jobtype').dropdown("set selected", details.JobTypeId);
					$('#vendor').dropdown("set selected", details.VendorId);
					$('#schedule-maintenance-modal').modal('show').modal('refresh');
					$('.ui.calendar').calendar({ type: 'date' });
				}
			});
		});
		$(document).on("click", ".delete-maintenance", function (e) {
			var id = $(this).parents('tr').attr('maintenance-id');
			swal({
				title: "Are you sure you want to delete this scheduled maintenace?",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete",
				showCancelButton: true,
				closeOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Rentals/DeleteScheduledExpense",
						data: { Id: id },
						success: function (data) {
							if (data.success) {
								GetScheduledMaintenanceEvent(data.PropertyId);
							}
						}
					});
				});

		});
	}
	function OnEditScheduleExpense() {

		$(document).on("click", ".edit-expense", function () {
			var id = $(this).parents('tr').attr('maintenance-id');
			$.ajax({
				type: "GET",
				url: "/Rentals/GetScheduledExpense",
				data: { Id: id },
				success: function (data) {
					var details = data.details;
					var property = data.property;
					$('#schedule-expense-Id').val(details.Id);
					$('#schedule-expense-description').val(details.Description);
					$('#schedule-expense-frequency').val(details.Frequency).change();
					$('#schedule-expense-propertyId').val(property.Name);
					$('#schedule-expense-propertyId').attr('propertyId', property.Id);
					$('#schedule-expense-recurring-start-date').val(moment.utc(details.RecurringDate).format("MMMM D,YYYY"));
					$('#schedule-expense-recurring-end-date').val(moment.utc(details.RecurringEndDate).format("MMMM D,YYYY"));
					$('#schedule-expense-amount').val(details.Amount);
					$('#schedule-expense-category').dropdown('set selected', details.ExpenseCategoryId);
					$('#schedule-payment-method').dropdown('set selected', details.PaymentMethodId);
					$('#schedule-expense-interval').val(details.Interval);
					$('#schedule-monthly-expense-modal').modal('show').modal('refresh');
					$('.ui.calendar').calendar({ type: 'date' });


				}
			});
		});
		$(document).on("click", ".delete-expense", function (e) {
			var id = $(this).parents('tr').attr('maintenance-id');
			swal({
				title: "Are you sure you want to delete this scheduled expense?",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete",
				showCancelButton: true,
				closeOnConfirm: true,
			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Rentals/DeleteScheduledExpense",
						data: { Id: id },
						success: function (data) {
							if (data.success) {
								GetScheduledExpensesEvent(data.PropertyId);
							}
						}
					});
				});

		});
	}
	function ValidateMaintenanceScheduleRequiredFields() {

		var hasBlankField = false;
		var hasfirstFocus = false;
		if ($('#maintenance-frequency').val() != 5) {
			$('#maintenance-interval').attr('required', '');
		}
		else {
			$('#maintenance-interval').removeAttr('required');
			$('#maintenance-interval').removeAttr("style");
		}

		$('#schedule-maintenance-modal').find('input[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.attr("style", "border-color:red");
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {
				obj.removeAttr("style")
			}

		});
		$('#schedule-maintenance-modal').find('select[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.parents('.ui.dropdown').attr("style", "border-color:red");
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {

				obj.parents('.ui.dropdown').removeAttr("style");
			}

		});
		return hasBlankField;
	}
	function ScheduleMaintenanceSubmit() {
		$(document).on("click", "#maintenance-submit-btn", function () {
			var hasBlankField = ValidateMaintenanceScheduleRequiredFields();
			if (hasBlankField) {
				return;
			}
			var formdata = new FormData();
			var propertyId = $('#maintenance-propertyId').attr('propertyId');
			formdata.append("Id", $('#maintenance-id').val());
			formdata.append("Description", $('#maintenance-description').val());
			formdata.append("Frequency", $('#maintenance-frequency').val());
			formdata.append("Interval", $('#maintenance-interval').val());
			formdata.append("PropertyId", propertyId);
			formdata.append("RecurringDate", $('#maintenance-recurring-start-date').val());
			formdata.append("RecurringEndDate", $('#maintenance-recurring-end-date').val());
			formdata.append("Amount", $('#maintenance-amount').val());
			formdata.append("ExpenseCategoryId", $('#maintenance-expense-category').val());
			formdata.append("PaymentMethodId", $('#maintenance-payment-method').val());
			formdata.append("WorkerId", $('#worker').val());
			formdata.append("JobTypeId", $('#jobtype').val());
			formdata.append("VendorId", $('#vendor').val());
			formdata.append("Type", 1);
			$.ajax({
				type: "POST",
				url: "/Rentals/SaveScheduleMaintenanceExpense",
				data: formdata,
				cache: false,
				processData: false,
				contentType: false,
				success: function (data) {
					swal("Data Saved.", "", "success");
					$('#schedule-maintenance-modal').modal('hide');
					GetScheduledMaintenanceEvent(propertyId);
					if ($('#maintenance-frequency').val() == '5') {
						GetExpense(propertyId);
						GetMaintenanceEntries(propertyId);
					}
				}
			});
		});
	}
	$(document).on('keyup', '[required]', function () {

		if ($(this).val().length == 0) {
			$(this).next().remove();
			$(this).after('<div class="ui basic red pointing prompt label transition visible">Required field!</div>');
		}
		else {
			$(this).next().remove();
		}
	});
	function ScheduleExpenseValidation() {
		var hasBlankField = false;
		var hasfirstFocus = false;

		if ($('#schedule-expense-frequency').val() != 5) {
			$('#schedule-expense-interval').attr('required', '');
		}
		else {
			$('#schedule-expense-interval').removeAttr('required');
			$('#schedule-expense-interval').removeAttr("style");
		}
		$('#schedule-monthly-expense-modal').find('input[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.attr("style", "border-color:red");
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {

				obj.removeAttr("style");
			}

		});
		$('#schedule-monthly-expense-modal').find('select[required]').each(function (index, element) {
			var obj = $(this);
			if (obj.val().length == 0) {
				obj.parents('.ui.dropdown').attr("style", "border-color:red");
				if (hasfirstFocus == false) {
					hasfirstFocus = true;
					obj.focus();
				}
				hasBlankField = true;
			} else {

				obj.parents('.ui.dropdown').removeAttr("style");
			}

		});
		return hasBlankField;
	}
	function ScheduleExpenseSubmit() {
		$(document).on("click", "#monthly-expense-submit-btn", function () {
			var hasBlankField = ScheduleExpenseValidation();
			if (hasBlankField) {
				return;
			}
			var formdata = new FormData();
			var propertyId = $('#schedule-expense-propertyId').attr('propertyId');
			formdata.append("Id", $('#schedule-expense-Id').val());
			formdata.append("Description", $('#schedule-expense-description').val());
			formdata.append("Frequency", $('#schedule-expense-frequency').val());
			formdata.append("Interval", $('#schedule-expense-interval').val());
			formdata.append("PropertyId", propertyId);
			formdata.append("RecurringDate", $('#schedule-expense-recurring-start-date').val());
			formdata.append("RecurringEndDate", $('#schedule-expense-recurring-end-date').val());
			formdata.append("Amount", $('#schedule-expense-amount').val());
			formdata.append("ExpenseCategoryId", $('#schedule-expense-category').val());
			formdata.append("PaymentMethodId", $('#schedule-payment-method').val());
			formdata.append("Type", 0);
			$.ajax({
				type: "POST",
				url: "/Rentals/SaveScheduleMaintenanceExpense",
				data: formdata,
				cache: false,
				processData: false,
				contentType: false,
				success: function (data) {
					swal("Data Saved.", "", "success");
					$('#schedule-monthly-expense-modal').modal('hide');
					if ($('#schedule-expense-frequency').val() == '5') {
						GetExpense(propertyId);
					}
					GetScheduledExpensesEvent(propertyId);
				}
			});
		});
	}
	function ClearModalForm() {
		$('#expense-form').form('clear');
		$('#MyTitle').removeClass('active');
		$('#MyContent').removeClass('active');
	}
	function GetPropertyForMaintenanceAndExpense(propertyId) {
		var property = '';
		$.ajax({
			type: "GET",
			url: "/Property/GetPropertyForMaintenanceAndExpense",
			data: { propertyId: propertyId },
			dataType: "json",
			async: false,
			success: function (data) {
				property = data.property;
			}
		});
		return property;
	}
	function ShowMaintenanceModal() {
		$(document).on("click", ".add-maintenance", function () {
			ClearModalForm();
			var property = GetPropertyForMaintenanceAndExpense($(this).attr('propertyId'));
			$('#maintenance-id').val('0');
			$('#maintenance-propertyId').val(property.Name);
			$('#maintenance-propertyId').attr('propertyId', property.Id);
			$('#maintenance-description').val('');
			$('#maintenance-frequency').dropdown('clear');
			$('#maintenance-interval').val('');
			$('#maintenance-frequency-day').val('');
			$('#maintenance-recurring-start-date').val('');
			$('#maintenance-recurring-end-date').val('');
			$('#maintenance-amount').val('');
			$('#maintenance-expense-category').dropdown('clear');
			$('#maintenance-payment-method').dropdown('clear');
			$('#worker').dropdown('clear');
			$('#jobtype').dropdown('clear');
			$('#vendor').dropdown('clear');
			$('#schedule-maintenance-modal').modal('show').modal('refresh');
			$('.ui.calendar').calendar({ type: 'date' });
		});
	}
	function ShowScheduleExpenseModal() {
		$(document).on("click", ".add-expense", function () {
			ClearModalForm();
			var property = GetPropertyForMaintenanceAndExpense($(this).attr('propertyId'));
			$('#schedule-expense-propertyId').val(property.Name);
			$('#schedule-expense-propertyId').attr('propertyId', property.Id);
			$('#schedule-expense-Id').val('0');
			$('#schedule-expense-description').val('');
			$('#schedule-expense-frequency').dropdown('clear');
			$('#schedule-expense-frequency-day').val('');
			$('#schedule-expense-recurring-start-date').val('');
			$('#schedule-expense-recurring-end-date').val('');
			$('#schedule-expense-amount').val('');
			$('#schedule-expense-category').dropdown('clear');
			$('#schedule-payment-method').dropdown('clear');
			$('#schedule-expense-interval').val('');
			$('#schedule-monthly-expense-modal').modal('show').modal('refresh');
			$('#schedule-monthly-expense-modal').find('.ui.calendar').calendar({ type: 'date' });
		});
	}
	//Load all properties for rentals
	function LoadPropertyTable() {
		tableProperty = $('#properties-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"ajax": "/Rentals/GetRentals",
			"columns": [
				{ "data": "Property" },
				{ "data": "Guest" },
				{ "data": "Status" },
				{ "data": "Actions" }

			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-property-id', data.Id);
			}
		});
	}

	function GetSchedulerFrequency(id) {
		switch (id) {
			case 1: return "Daily"
			case 2: return "Weekly"
			case 3: return "Monthly"
			case 4: return "Yearly"
			case 5: return "One time"
		}
	}
	function GetScheduledExpensesEvent(propertyId) {
		$.ajax({
			type: "GET",
			url: "/Rentals/GetScheduledExpensesEvent/",
			data: { propertyId: propertyId },
			success: function (data) {
				var table = $('[scheduled-expense-event-table-property-id="' + data.propertyId + '"]');
				var monthlyExpenses = data.ExpensePaymentData;
				var MonthlyExpensesOption = '';
				if (monthlyExpenses.length > 0) {
					for (var y = 0; y < monthlyExpenses.length; y++) {
						MonthlyExpensesOption +=
							'<tr maintenance-id="' + monthlyExpenses[y].Maintenance.Id + '">' +
							'<td>' + monthlyExpenses[y].Maintenance.Amount + '</td>' +
							'<td>' + monthlyExpenses[y].Maintenance.Description + '</td>' +
							'<td>' + moment.utc(monthlyExpenses[y].Maintenance.RecurringDate).format("MMMM D,YYYY") + '</td>' +
							'<td>' + (monthlyExpenses[y].Maintenance.RecurringEndDate != null ? moment.utc(monthlyExpenses[y].Maintenance.RecurringEndDate).format("MMMM D,YYYY") : '-') + '</td>' +
							'<td>' + GetSchedulerFrequency(monthlyExpenses[y].Maintenance.Frequency) + '</td>' +
							'<td>' + monthlyExpenses[y].ExpenseCategory.ExpenseCategoryType + '</td>' +
							'<td>' + (monthlyExpenses[y].PaymentMethod != null ? monthlyExpenses[y].PaymentMethod.Name : '-') + '</td>' +
							'<td><a class="ui mini button edit-expense" title="Details"><i class=\"icon edit\"></i></a><a class="ui mini button delete-expense" title="Delete"><i class=\"icon trash\"></i></a></td>' +
							'</tr>';
					}
				}
				table.DataTable().clear().destroy();
				table.find('tbody').empty();
				table.find('tbody').append(MonthlyExpensesOption);
				table.DataTable({ ordering: false });

			}
		});
	}
	function GetScheduledMaintenanceEvent(propertyId) {
		$.ajax({
			type: "GET",
			url: "/Rentals/GetScheduledMaintenanceEvent/",
			data: { propertyId: propertyId },
			success: function (data) {
				var table = $('[scheduled-maintenance-event-table-property-id="' + data.propertyId + '"]');
				var maintenances = data.Maintenances;
				var MaintenanceOption = '';
				if (maintenances.length > 0) {
					for (var y = 0; y < maintenances.length; y++) {
						MaintenanceOption +=
							'<tr maintenance-id="' + maintenances[y].Maintenance.Id + '">' +
							'<td>' + maintenances[y].Maintenance.Amount + '</td>' +
							'<td>' + maintenances[y].Maintenance.Description + '</td>' +
							'<td>' + moment.utc(maintenances[y].Maintenance.RecurringDate).format("MMMM D,YYYY") + '</td>' +
							'<td>' + (maintenances[y].Maintenance.RecurringEndDate != null ? moment.utc(maintenances[y].Maintenance.RecurringEndDate).format("MMMM D,YYYY") : '-') + '</td>' +
							'<td>' + GetSchedulerFrequency(maintenances[y].Maintenance.Frequency) + '</td>' +
							'<td>' + maintenances[y].ExpenseCategory.ExpenseCategoryType + '</td>' +
							'<td>' + (maintenances[y].PaymentMethod != null ? maintenances[y].PaymentMethod.Name : '-') + '</td>' +
							'<td>' + (maintenances[y].Worker != null ? maintenances[y].Worker.Firstname + ' ' + maintenances[y].Worker.Lastname : maintenances[y].Vendor != null ? maintenances[y].Vendor.Name : '-') + '</td>' +
							'<td>' + (maintenances[y].JobType != null ? maintenances[y].JobType.Name : '-') + '</td>' +
							'<td><a class="ui mini button edit-maintenance" title="Details"><i class=\"icon edit\"></i></a><a class="ui mini button delete-maintenance" title="delete"><i class=\"icon trash\"></i</a></td>' +
							'</tr>';
					}
				}
				table.DataTable().clear().destroy();
				table.find('tbody').empty();
				table.find('tbody').append(MaintenanceOption);
				table.DataTable({ ordering: false });

			}
		});
	}

	function GetIncome(propertyId) {
		$.ajax({
			type: "GET",
			url: "/Rentals/GetIncome/",
			data: { propertyId: propertyId },
			success: function (data) {
				var table = $('[income-table-propert-id="' + data.propertyId + '"]');
				var IncomeData = '';
				var Incomes = data.Incomes;
				for (var q = 0; q < Incomes.length; q++) {
					IncomeData +=
						'<tr  data-income-id="' + Incomes[q].Id + '">' +
						'<td>' + moment.utc(Incomes[q].DateRecorded).format("MMM D, YYYY") + '</td>' +
						'<td>' + Incomes[q].Description + '</td>' +
						'<td>' + Incomes[q].SiteType + '</td>' +
						'<td>' + Incomes[q].PropertyName + '</td>' +
						'<td>' + Incomes[q].BookingCode + '</td>' +
						'<td>' + Incomes[q].GuestName + '</td>' +

						'<td>' + Incomes[q].Amount + '</td>' +
						'<td>' + Incomes[q].PaymentDetails + '</td>' +
						//'<td>' + Incomes[q].Balance + '</td>' +
						'<td>' + ('<p style="color:' + (Incomes[q].Status == "Over Paid" ? "#008000" : "#FF0000") + ';">' + (Incomes[q].IncomeTypeId == 5 ? "0" : Incomes[q].Balance) + '</p>'  /*(parseFloat(Incomes[q].Amount) - parseFloat(Incomes[q].Payments))*/) + '</td>' +
						'<td>' + Incomes[q].Status + '</td>' +
						'<td>' + moment.utc(Incomes[q].DueDate).format("MMM D, YYYY") + '</td>' +
						'<td>' + Incomes[q].Actions + '</td>' +
						'</tr>';
				}
				table.DataTable().clear().destroy();
				table.find('tbody').empty();
				table.find('tbody').append(IncomeData);
				table.DataTable({ ordering: false });

			}
		});
	}
	function GetExpense(propertyId) {
		$.ajax({
			type: "GET",
			url: "/Rentals/GetExpense/",
			data: { propertyId: propertyId },
			success: function (data) {
				var table = $('[expense-table-propert-id="' + data.propertyId + '"]');
				var ExpenseData = '';
				var Expenses = data.Expenses;
				for (var q = 0; q < Expenses.length; q++) {

					//var status = Expenses[q].IsPaid ? "Paid" : "Unpaid";
					ExpenseData +=
						'<tr data-expense-id="' + Expenses[q].Id + '">' +
						'<td>' + (moment.utc(Expenses[q].CreatedDate).format("MMM D, YYYY")) + '</td>' +
						'<td>' + Expenses[q].Description + '</td>' +
						'<td>' + Expenses[q].PropertyName + '</td>' +
						'<td>' + Expenses[q].Category + '</td>' +
						'<td>' + Expenses[q].BookingCode + '</td>' +
						'<td>' + Expenses[q].FeeName + '</td>' +
						'<td>' + Expenses[q].Amount + '</td>' +
						'<td>' + Expenses[q].Balance + '</td>' +
						'<td>' + moment.utc(Expenses[q].DueDate).format("MMM D, YYYY") + '</td>' +
						'<td>' + Expenses[q].PaymentDate + '</td>' +
						'<td>' + Expenses[q].Status + '</td>' +
						'<td><a class="ui mini button edit-expense-btn" title="Edit"><i class="icon edit"></i></a></td>' +
						'</tr>';
				}
				table.DataTable().clear().destroy();
				table.find('tbody').empty();
				table.find('tbody').append(ExpenseData);
				table.DataTable({ ordering: false });

			}
		});
	}
	function GetMaintenanceEntries(propertyId) {
		$.ajax({
			type: "GET",
			url: "/Rentals/GetMaintenanceEntries/",
			data: { propertyId: propertyId },
			success: function (data) {
				var table = $('[maintenance-entry-table-propert-id="' + data.propertyId + '"]');
				var MaintenanceEntryData = '';
				var MaintenanceEntries = data.MaintenanceEntries;
				for (var q = 0; q < MaintenanceEntries.length; q++) {
					//var status = (Incomes[q].Amount == Incomes[q].Payments ? 'Paid' : 'New');
					var status = MaintenanceEntries[q].Status;
					var color = (status == "Not yet start" ? "#EC7063" : status == "Started" ? "#F5B041 " : "#82E0AA");
					MaintenanceEntryData +=
						'<tr bgcolor="' + color + '" maintenance-entry-id="' + MaintenanceEntries[q].Id + '">' +
						'<td>' + MaintenanceEntries[q].Name + '</td>' +
						'<td>' + (moment.utc(MaintenanceEntries[q].DueDate).format("MMM D, YYYY")) + '</td>' +
						'<td>' + MaintenanceEntries[q].Description + '</td>' +
						'<td>' + MaintenanceEntries[q].Amount + '</td>' +
						'<td>' + MaintenanceEntries[q].DateStart + '</td>' +
						'<td>' + MaintenanceEntries[q].DateEnd + '</td>' +
						'<td>' + MaintenanceEntries[q].Status + '</td>' +
						'<td>' + MaintenanceEntries[q].Actions + '</td>' +
						'</tr>';
				}
				table.DataTable().clear().destroy();
				table.find('tbody').empty();
				table.find('tbody').append(MaintenanceEntryData);
				table.DataTable({ ordering: false });

			}
		});
	}

	function PropertyFilterChange() {
		//JM 4/30/2020 Filter for maintenance schedules
		$(document).on('change', '#property-filter', function () {
			LoadPropertiesForMaintenanceSchedules();
		});
	}
	function LoadPropertiesForMaintenanceSchedules() {
		$.ajax({
			type: "GET",
			url: "/Rentals/GetPropertyMaintenance",
			data: { propertyIds: JSON.stringify($("#property-filter").val()) },
			async: true,
			success: function (result) {
				if (result.success) {
					var html = '';
					var properties = result.Properties;
					var childId = result.LocalChildIds;
					for (var x = 0; x < properties.length; x++) {
						html += '<div class="ui styled accordion property-accordion-header">' +
							'<div class="title accordion-title-schedules"  property-Id="' + (childId[x] != 0 ? childId[x] : properties[x].Id) + '">' +
							'<table class="ui celled table">' +
							'<thead>' +
							'<tr></tr>' +
							'</thead>' +
							'<tbody>' +
							'<tr>' +
							'<td >' +
							'<label>' + properties[x].Name + '</label>' +
							'</td>' +
							'</tr>' +
							'</tbody>' +
							'</table>' +
							'</div>' +
							'<div class="content">' +
							'<div class="ui tabular menu property-setting">' +
							'<div class="item active" data-tab="property-setting-tab">Scheduling</div>' +
							'<div class="item" data-tab="payment-tab">Income,Expense,Maintenance</div>' +
							'</div>' +
							'<div class="ui tab active" data-tab="property-setting-tab">' +
							'<div class="ui form">' +
							'<h3>Scheduled Expenses Event</h3>' +
							'<button class="ui primary button add-expense" propertyId="' + properties[x].Id + '">' +
							'Add Expenses' +
							'</button>' +
							'<table class="display responsive datatable property-accordion-table" style="width:100%" scheduled-expense-event-table-property-id="' + properties[x].Id + '">' +
							'<thead>' +
							'<tr>' +
							'<th>Amount</th>' +
							'<th>Desctiption</th>' +
							'<th>Recurring Start</th>' +
							'<th>Recurring End</th>' +
							'<th>Frequency</th>' +
							'<th>Category</th>' +
							'<th>Payment</th>' +
							'<th>Action</th>' +
							'</tr>' +
							'</thead>' +
							'<tbody>' +
							//MonthlyExpensesOption +
							'</tbody>' +
							'</table>' +
							'<h3>Scheduled Maintenance Event</h3>' +
							'<button class="ui primary button add-maintenance" propertyId="' + properties[x].Id + '">' +
							'Add Maintenance' +
							'</button>' +
							'<table class="display responsive datatable property-accordion-table" style="width:100%" scheduled-maintenance-event-table-property-id="' + properties[x].Id + '">' +
							'<thead>' +
							'<tr>' +
							'<th>Amount</th>' +
							'<th>Desctiption</th>' +
							'<th>Recurring Start</th>' +
							'<th>Recurring End</th>' +
							'<th>Frequency</th>' +
							'<th>Category</th>' +
							'<th>Payment</th>' +
							'<th>Worker/Vendor</th>' +
							'<th>JobType</th>' +
							'<th>Action</th>' +
							'</tr>' +
							'</thead>' +
							'<tbody>' +
							//MaintenanceOption +
							'</tbody>' +
							'</table>' +

							'</div>' +
							'</div>' +
							'<div class="ui tab" data-tab="payment-tab">' +
							//Not scheduled Income and Expense
							'<h3>Income</h3>' +
							'<table class="datatable display responsive" style="width:100%" income-table-propert-id="' + properties[x].Id + '">' +
							'<thead>' +
							'<tr><th>Date Recorded</th>' +
							'<th>Description</th>' +
							'<th>SiteType</th>' +
							'<th>Property</th>' +
							'<th>Booking Code</th>' +
							'<th>Guest</th>' +
							'<th>Amount</th>' +
							'<th>Payment</th>' +
							'<th>Balance</th>' +
							'<th>Status</th>' +
							'<th>Due Date</th>' +
							'<th>Actions</th>' +
							'</tr></thead>' +
							'<tbody>' +
							//IncomeData +
							'</tbody>' +
							'</table>' +
							'<h3>Expense</h3>' +
							'<table class="datatable display responsive" style="width:100%" expense-table-propert-id="' + properties[x].Id + '">' +
							'<thead>' +
							'<tr><th>Date Recorded</th>' +
							'<th>Description</th>' +
							'<th>Property</th>' +
							'<th>Category</th>' +
							'<th>Booking Code</th>' +
							'<th>Fee</th>' +
							'<th>Amount</th>' +
							'<th>Balance</th>' +
							'<th>Due Date</th>' +
							'<th>Payment Date</th>' +
							'<th>Status</th>' +
							'<th>Actions</th>' +
							'</tr></thead>' +
							'<tbody>' +
							//ExpenseData +
							'</tbody>' +
							'</table>' +

							'<h3>Maintenance Entries</h3>' +
							'<table class="datatable display responsive" style="width:100%" maintenance-entry-table-propert-id="' + properties[x].Id + '">' +
							'<thead>' +
							'<tr>' +
							'<th>Worker/Vendor</th>' +
							'<th>Date</th>' +
							'<th>Description</th>' +
							'<th>Amount</th>' +
							'<th>Start Date</th>' +
							'<th>End Date</th>' +
							'<th>Status</th>' +
							'<th>Actions</th>' +
							'</tr></thead>' +
							'<tbody>' +
							//MaintenanceEntryData +
							'</tbody>' +
							'</table>' +
							'</div>' +
							'</div>' +
							'</div>';

						//$('.property-accordion-header').last().find('.datatable').DataTable({ "ordering": false });							
					}
					$('.property-maintenance-div').empty();
					$('.property-maintenance-div').append(html);
					MaintenanceSchedulesAccordionClick()
				}
			}
		});
	}


	function LoadNotices() {
		tableNotices = $('#notices-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Rentals/GetNotices",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "Description" },
				{ "data": "NoticeType" },
				{ "data": "NoticeDeliveryMethod" },
				{ "data": "Properties" },
				{ "data": "Renters" },
				{ "data": "AttachmentPaths" },
				{ "data": "DateOfNotice" },
				{ "data": "Actions" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-notice-id', data.Id);
			}
		});
	}
	function AddNotice() {
		$(document).on('click', '.add-notice', function (e) {
			var noticeModal = $('#notice-modal');
			noticeModal.attr('notice-id', 0);
			noticeModal.find('.description').val('');
			$('#notice-property').dropdown('clear');
			$('#notice-renter').dropdown('clear');
			noticeModal.find('.date-of-notice').val('');
			noticeModal.find('.date-of-incident').val('');
			noticeModal.find('#notice-type').dropdown('clear');
			noticeModal.find('#notice-delivery-method').dropdown('clear');
			noticeModal.find('#document-template').dropdown('clear');
			$('#notice-file-upload').html('');
			$('#notice-document').val('');
			$('#notice-modal').modal({ closable: false }).modal('show').modal('refresh');
			$('.ui.calendar').calendar({ type: 'date' });
			$('.ui.calendar.time').calendar();
		});
	}
	function GetNoticeTypes() {
		$.ajax({
			type: 'GET',
			url: "/Rentals/GetNoticeType",
			dataType: "json",
			success: function (data) {
				var noticeTypes = data.noticeTypes;
				$('#notice-type').empty();
				$('#notice-type').append('<option value="0" selected>Notice Type</option>');
				$('#notice-type').append('<option class="add-notice-type">New Type...</option>');
				for (var x = 0; x < noticeTypes.length; x++) {
					$('#notice-type').append('<option value="' + noticeTypes[x].Id + '">' + noticeTypes[x].Name + '</option>')
				}
			}
		});
	}
	function GetNoticeDeliveryMethod() {
		$.ajax({
			type: 'GET',
			url: "/Rentals/GetNoticeDeliveryMethod",
			dataType: "json",
			success: function (data) {
				var noticeDeliveryMethods = data.noticeDeliveryMethods;
				$('#notice-delivery-method').empty();
				$('#notice-delivery-method').append('<option value="0" selected>Delivery Method</option>');
				$('#notice-delivery-method').append('<option class="add-delivery-method">New Method...</option>');
				for (var x = 0; x < noticeDeliveryMethods.length; x++) {
					$('#notice-delivery-method').append('<option value="' + noticeDeliveryMethods[x].Id + '">' + noticeDeliveryMethods[x].Name + '</option>')
				}
			}
		});
	}
	function GetDocumentTemplates() {
		$.ajax({
			type: 'GET',
			url: "/Rentals/GetNoticeDocuments",
			dataType: "json",
			success: function (data) {
				var documents = data.data;
				$('#document-template').empty();
				$('#document-template').append('<option value="0" selected>Document Templates</option>');
				for (var x = 0; x < documents.length; x++) {
					$('#document-template').append('<option value="' + documents[x].Id + '">' + documents[x].Name + '</option>')
				}
			}
		});
	}
	function AddNoticeSubmit() {
		$(document).on('click', '#notice-save-btn', function (e) {
			var noticeModal = $('#notice-modal');
			var formData = new FormData();
			for (var i = 0; i < $('#notice-document').get(0).files.length; ++i) {
				formData.append("Files[" + i + "]", $('#notice-document').get(0).files[i]);
			}
			var propertyIds = [];
			//for (var e = 0; e < $('#notice-property').val().length; e++) {
			//propertyIds.push($('#notice-property').val()[e]);
			propertyIds.push($('#notice-property').val());
			//}
			var renterIds = [];
			renterIds.push($('#notice-renter').val());
			//}
			formData.append("RenterIds", JSON.stringify(renterIds));
			formData.append("PropertyIds", JSON.stringify(propertyIds));
			formData.append("Id", noticeModal.attr('notice-id'));
			formData.append("Description", noticeModal.find('.description').val())
			formData.append("DateOfIncident", noticeModal.find('.date-of-incident').val())
			formData.append("DateOfNotice", noticeModal.find('.date-of-notice').val())
			formData.append("NoticeDeliveryMethodId", noticeModal.find('#notice-delivery-method').dropdown('get value'))
			formData.append("NoticeTypeId", noticeModal.find('#notice-type').dropdown('get value'))
			formData.append("IsForRenter", $('#send-by').dropdown('get value'));
			formData.append('documentId', noticeModal.find('#document-template').val())
			$.ajax({
				type: 'POST',
				url: "/Rentals/CreateNotice",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#notice-modal').modal('hide');
						ShowGeneratedNotice(data.generatedId, data.variables);
						tableNotices.ajax.reload(null, false);
					}
				}
			});
		});
	}

	function GetNoticeDetails() {
		$(document).on('click', '.edit-notice', function (e) {
			var id = $(this).parents('tr').attr('data-notice-id');
			$.ajax({
				type: 'GET',
				url: "/Rentals/GetNoticeDetails",
				dataType: "json",
				data: { id: id },
				success: function (data) {
					$('#notice-property').dropdown('clear');
					$('#notice-renter').dropdown('clear');
					$('#notice-file-upload').html('');
					$('#notice-document').val('');
					var notice = data.notice;
					var renterIds = data.renterIds;
					var propertyIds = data.propertyIds;
					var attachmentPaths = data.attachmentPaths;
					for (var x = 0; x < attachmentPaths.length; x++) {
						$('#notice-file-upload').append(attachmentPaths[x]);
					}
					if (propertyIds.length > 0) {
						for (var x = 0; x < propertyIds.length; x++) {
							$('#notice-property').dropdown('set selected', [propertyIds[x]]);
						}
					}
					if (renterIds.length > 0) {
						for (var x = 0; x < renterIds.length; x++) {
							$('#notice-renter').dropdown('set selected', [renterIds[x]]);
						}
					}
					var noticeModal = $('#notice-modal');

					$('#notice-modal').modal({ closable: false }).modal('show').modal('refresh');
					noticeModal.attr('notice-id', notice.Id);
					noticeModal.find('.description').val(notice.Description);
					noticeModal.find('.date-of-notice').val(moment(notice.DateOfNotice).format("h:mm A MMM D, YYYY"));
					noticeModal.find('.date-of-incident').val(moment(notice.DateOfIncident).format("h:mm A MMM D, YYYY"));
					$('#notice-type').dropdown('set selected', notice.NoticeTypeId);
					$('#notice-delivery-method').dropdown('set selected', notice.NoticeDeliveryMethodId);
					$('#send-by').dropdown('set selected', notice.IsForRenter);;
					$('.ui.calendar').calendar({ type: 'date' });
					$('.ui.calendar.time').calendar();
				}
			});
		});

	}
	function GetRenters() {
		$.ajax({
			type: 'GET',
			url: "/Rentals/GetRenters",
			dataType: "json",
			async: false,
			success: function (data) {
				var renters = data.renters;
				$('#notice-renter').append('<option value="">Select Renter</option>');
				$('#notification-renter').append('<option value="">Select Renter</option>');
				$('#notice-renter').empty();
				for (var x = 0; x < renters.length; x++) {
					$('#notice-renter').append('<option value="' + renters[x].Id + '">' + renters[x].Firstname + " " + renters[x].Lastname + '</option>');
					$('#notification-renter').append('<option value="' + renters[x].Id + '">' + renters[x].Firstname + " " + renters[x].Lastname + '</option>');
				}
			}
		});
	}
	function AddDocumentTemplate() {
		$(document).on('click', '.create-document-template', function (e) {
			var documentTemplateModal = $('#document-template-modal');
			documentTemplateModal.find('#document-name').val('');
			documentTemplateModal.find('.nicEdit-main').empty();
			documentTemplateModal.attr('document-id', '0');
			$('#template-upload').val('');
			documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

		});
	}
	function SaveGeneratedTemplate() {
		$(document).on('click', '#notice-document-save', function () {
			var id = $('#uploaded-notice-modal').attr('notice-document-id');
			RemoveTag($('#uploaded-notice-modal').find('.nicEdit-main'));
			var content = $('#uploaded-notice-modal').find('.nicEdit-main').html();
			$.ajax({
				type: 'POST',
				url: "/Rentals/SaveGeneratedDocument",
				data: { id: id, content: content },
				success: function (data) {
					if (data.success) {
						tableNotices.ajax.reload(null, false);
						$('#uploaded-notice-modal').modal('hide');
					}
					else {
						swal("Error occured on deleting payment method", "", "error");
					}
				},
				error: (error) => {
					console.log(JSON.stringify(error));
				}
			});
		});
	}
	function RemoveTag(content) {
		content.find('.icon.tag').remove();
	}
	function SaveDocumentTemplate() {
		$(document).on('click', '#notice-document-template-save', function () {
			var id = $('#document-template-modal').attr('document-id');
			var name = $('#document-name').val();
			RemoveTag($('#document-template-modal').find('.nicEdit-main'));
			var formData = new FormData();
			for (var i = 0; i < $('#template-upload').get(0).files.length; ++i) {
				formData.append("Files[" + i + "]", $('#template-upload').get(0).files[i]);
			}
			formData.append("Id", id);
			formData.append("DocumentName", name);
			formData.append("Content", $('#document-template-modal').find('.nicEdit-main').html());

			$.ajax({
				type: 'POST',
				url: "/Rentals/CreateTemplate",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						tableNoticeDocuments.ajax.reload(null, false);
						GetDocumentTemplates();
						$('#document-template-modal').modal("hide");

					}
					else {
						swal("Error occured on deleting payment method", "", "error");
					}
				},
				error: (error) => {
					console.log(JSON.stringify(error));
				}
			});
		});
	}
	function ShowGeneratedNotice(id, variables) {
		$.ajax({
			type: 'GET',
			url: "/Rentals/GetNoticeDocumentDetails",
			data: { id: id },
			success: function (data) {
				if (data.success) {
					var document = data.document;
					console.log(data.template)
					var documentModal = $('#uploaded-notice-modal');
					documentModal.attr('notice-document-id', document.Id);
					//documentModal.find('.nicEdit-main').empty();
					documentModal.find('.nicEdit-main').html(document.Html);
					documentModal.find('.Section0').children().first().remove();
					VariableAddTags(documentModal.find('.nicEdit-main'), variables);
					documentModal.modal({ closable: false }).modal('show').modal('refresh');

				}
			}
		});
	}
	function LoadNoticeDocuments() {
		tableNoticeDocuments = $('#notice-documents-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Rentals/GetNoticeDocuments",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Actions" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-notice-document-id', data.Id);
			}
		});
	}
	function OnAddTemplateVariable() {
		$("#modal-template-variable").on('change', function () {
			var variable = $(this).children(':selected').val();
			$('#document-template-modal').find('.nicEdit-main').append('<i class="icon tags"></i><span>' + variable + '</span>');
		});
	}
	function VariableAddTags(content, searchText = ["[Firstname]", "[Lastname]", "[Property address]", "[Date of notice]", "[Date of incident]"]) {
		var elements = ["span", "h1", "h2", "h3", "h4", "h5", "h6"];
		//Replace Text on document to SignRequest Tag
		for (var i = 0; i < elements.length; i++) {
			for (var x = 0; x < searchText.length; x++) {
				$('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ">:contains('" + searchText[x] + "')"));
				$('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ":contains('" + searchText[x] + "')"));
			}
		}
	}
	function DeleteNotice() {
		$(document).on('click', '.delete-notice', function () {
			var id = $(this).parents('tr').attr('data-notice-id');
			swal({
				title: "Are you sure you want to delete this notice?",
				text: "",
				type: "warning",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Delete",
				showCancelButton: true,
				closeOnConfirm: true,

			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Rentals/DeleteNotice",
						data: { id: id },
						success: function (data) {
							if (data.success) {
								tableNotices.ajax.reload(null, false);
							}
						}
					});
				});
		});
	}
	function DeleteNoticeTemplate() {
		$(document).on('click', '.delete-notice-template', function () {
			var id = $(this).parents('tr').attr('data-notice-document-id');
			swal({
				title: "Are you sure you want to delete this template?",
				text: "",
				type: "warning",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Delete",
				showCancelButton: true,
				closeOnConfirm: true,

			},
				function () {
					$.ajax({
						type: 'POST',
						url: "/Rentals/DeleteNoticeTemplate",
						data: { id: id },
						success: function (data) {
							if (data.success) {
								GetDocumentTemplates();
								tableNoticeDocuments.ajax.reload(null, false);
							}
						}
					});
				});
		});
	}

	function EditNoticeTemplate() {
		$(document).on('click', '.edit-notice-template', function () {
			var id = $(this).parents('tr').attr('data-notice-document-id');
			$.ajax({
				type: 'GET',
				url: "/Rentals/GetTemplateDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						var template = data.template;
						console.log(data.template)
						var documentTemplateModal = $('#document-template-modal');
						documentTemplateModal.attr('document-id', template.Id);
						documentTemplateModal.find('#document-name').val(template.Name);
						$('#template-upload').val('');
						documentTemplateModal.find('.nicEdit-main').html(template.Html);
						VariableAddTags(documentTemplateModal.find('.nicEdit-main'));
						documentTemplateModal.find('.Section0').children().first().remove();
						documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

					}
				}
			});
		});
	}

});