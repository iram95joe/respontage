﻿var arrivalTravelType = 1;
var departureTravelType = 2;
var isSubmitToEdit = false;
var currentEditViewTravelEventRecordId = 0;

var addNewTravellerInfoUIRecord =
                '<div class="form-group">' +
                '<label class="control-label col-md-2" for="travellerName">Name:</label>' +
                '<div class="col-md-2">' +
                    '<input type="text" class="form-control" id="travellerName" placeholder="Traveller Name" name="travellerName">' +
                '</div>' +
                '<label class="control-label col-md-2" for="travellerContactNumber">Contact Number:</label>' +
                '<div class="col-md-2">' +
                    '<input type="text" class="form-control" id="travellerContactNumber" placeholder="Contact Number" name="travellerContactNumber">' +
                '</div>' +
                '<label class="control-label col-md-2" for="travellerLagguageCount">Total Lagguage:</label>' +
                '<div class="col-md-2">' +
                    '<input type="text" class="form-control" id="travellerLagguageCount" placeholder=Total Lagguage" name="travellerLagguageCount">' +
                '</div>' +
                '</div>';

$(document).ready(function () {
    setupSemanticUI();
    HideAllTravelInfoUISettings();
    setupArrivalTypeDropDownList();
    setupDepartureTypeDropDownList();
    onAddNewTravellerInfo();
    onClearAllTravellerInfo();
    onCreateNewTravelGroup();
    setTravelTypeForTravelEventForm(arrivalTravelType);
    $("#FlightDepartureInfoSection").show();
    $("#FlightArrivalInfoSection").show();
    onNewTravelEventFormSubmit();
});

function onAddNewTravellerInfo() {
    $(".add_new_traveller_button").on('click', function (e) {
        e.preventDefault();
        $("#travellersList").append(addNewTravellerInfoUIRecord);
        updateTravelGroupInfoTravellerCount(false);
    });
}

function onClearAllTravellerInfo() {
    $(".clear_all_traveller_button").on('click', function (e) {
        e.preventDefault();
        $("#travellersList").html("");
        updateTravelGroupInfoTravellerCount(true);

    });
}

function setupSemanticUI() {
    $('.ui.calendar').calendar();
    //$('.ui.calendar.month').calendar({ type: 'month' });
    //$('.ui.calendar.time').calendar({ type: 'time' });
}

function updateTravelGroupInfoTravellerCount(isClearAll) {
    var totalTravellerCount = 0;
    if (!isClearAll) {
        totalTravellerCount = $("#travellersList > .form-group").length;
        $("#totalPerson").val(totalTravellerCount);
    } else {
        $("#totalPerson").val(totalTravellerCount);
    }
}

function setupArrivalTypeDropDownList() {
    $('#arrival-type-filter').dropdown({
        on: 'hover', showOnFocus: false,
        onChange: function () {
            var selectedTravelType = this.value;

            HideArrivalTravelInfoUISettings();

            if (selectedTravelType == 2) {
                $("#FlightArrivalInfoSection").show();
            }
            else if (selectedTravelType == 5) {
                $("#DriveArrivalInfoSection").show();
            }
            else if (selectedTravelType == 7) {
                $("#CruiseArrivalInfoSection").show();
            }
            else if (selectedTravelType == 9) {
                $("#TrainArrivalInfoSection").show();
            }
            else if (selectedTravelType == 11) {
                $("#BusArrivalInfoSection").show();
            }
        }
    });
}

function setupDepartureTypeDropDownList() {
    $('#departure-type-filter').dropdown({
        on: 'hover', showOnFocus: false,
        onChange: function () {
            var selectedTravelType = this.value;

            HideDepartureTravelInfoUISettings();

            if (selectedTravelType == 3) {
                $("#FlightDepartureInfoSection").show();
            }
            else if (selectedTravelType == 6) {
                $("#DriveDepartureInfoSection").show();
            }
            else if (selectedTravelType == 8) {
                $("#CruiseDepartureInfoSection").show();
            }
            else if (selectedTravelType == 10) {
                $("#TrainDepartureInfoSection").show();
            }
            else if (selectedTravelType == 12) {
                $("#BusDepartureInfoSection").show();
            }
        }
    });
}

function HideDepartureTravelInfoUISettings() {
    $("#FlightDepartureInfoSection").hide();
    $("#DriveDepartureInfoSection").hide();
    $("#CruiseDepartureInfoSection").hide();
    $("#TrainDepartureInfoSection").hide();
    $("#BusDepartureInfoSection").hide();
}

function HideArrivalTravelInfoUISettings() {
    $("#FlightArrivalInfoSection").hide();
    $("#DriveArrivalInfoSection").hide();
    $("#CruiseArrivalInfoSection").hide();
    $("#TrainArrivalInfoSection").hide();
    $("#BusArrivalInfoSection").hide();
}

function HideAllTravelInfoUISettings() {
    HideArrivalTravelInfoUISettings();
    HideDepartureTravelInfoUISettings();
}

function onNewTravelEventFormSubmit() {
    $("#form-submit-btn").click(function () {
        var totalPersonInGroup = $("input[name=totalPerson]").val();
        var contactPerson = $("input[name=contactPerson]").val();
		var contactNumber = $("input[name=contactNumber]").val();
		var checkin = $("input[name=Checkin]").val();
		var checkout = $("input[name=Checkout]").val();
        var people = $("#people").val();

        var travelType = $('input[name=travelType]:checked').val();

        var requestObj;
        var hashValue = getUrlParameter("enc");
        var inquiryId = getUrlParameter("id");
        var travelDateTime;
        var noteType;
        var airportCodeArrival;
        var airportCodeDeparture;
        var airline;
        var flightNumber;
        var carType;
        var carPlate;
        var carColor;
        var departureCity = "";
        var cruiseName;
        var cruiseArrivalPort;
        var cruiseDeparturePort;
        var name = "";
        var terminalStation = "";

        //need to check on travelType to get value
        if (travelType == 1) //Arrival Type
        {
            noteType = $("#arrival-type-filter").val();

            travelDateTime = $('input[name=arrivalDate]').val();

            airportCodeArrival = $('#FlightArrivalInfoSection input[name=airportCodeArrival]').val();
            airportCodeDeparture = $('#FlightArrivalInfoSection input[name=airportCodeDeparture]').val();

            airline = $('input[name=arrivalAirline]').val();
            flightNumber = $('input[name=arrivalFlightNumber]').val();

            carType = $('input[name=driveArrivalCarType]').val();
            carPlate = $('input[name=driveArrivalCarPlate]').val();
            carColor = $('input[name=driveArrivalCarColor]').val();

            cruiseName = $('input[name=cruiseArrivalName]').val();
            cruiseArrivalPort = $('#CruiseArrivalInfoSection input[name=cruiseArrivalPort]').val();
            cruiseDeparturePort = $('#CruiseArrivalInfoSection input[name=cruiseDeparturePort]').val();


            if (noteType == 9) {
                name = $('input[name=trainArrivalName]').val();
                terminalStation = $('input[name=trainArrivalTerminalStation]').val();
            } else if (noteType == 11) {
                name = $('input[name=busArrivalName]').val();
                terminalStation = $('input[name=busArrivalTerminalStation]').val();
            }
        } else { //Departure Type
            noteType = $("#departure-type-filter").val();

            travelDateTime = $('input[name=departureDate]').val();

            airportCodeArrival = $('#FlightDepartureInfoSection input[name=airportCodeArrival]').val();
            airportCodeDeparture = $('#FlightDepartureInfoSection input[name=airportCodeDeparture]').val();

            airline = $('input[name=departureAirline]').val();
            flightNumber = $('input[name=departureFlightNumber]').val();

            carType = $('input[name=driveDepartureCarType]').val();
            carPlate = $('input[name=driveDepartureCarPlate]').val();
            carColor = $('input[name=driveDepartureCarColor]').val();
            departureCity = $('input[name=driveDepartureCity]').val();

            cruiseName = $('input[name=cruiseDepartureName]').val();
            cruiseArrivalPort = $('#CruiseDepartureInfoSection input[name=cruiseArrivalPort]').val();
            cruiseDeparturePort = $('#CruiseDepartureInfoSection input[name=cruiseDeparturePort]').val();

            if (noteType == 10) {
                name = $('input[name=trainDepartureName]').val();
                terminalStation = $('input[name=trainDepartureTerminalStation]').val();
            } else if (noteType == 12) {
                name = $('input[name=busDepartureName]').val();
                terminalStation = $('input[name=busDepartureTerminalStation]').val();
            }
        }

        if (!isSubmitToEdit && currentEditViewTravelEventRecordId == 0) {

            requestObj = {
                InquiryId: inquiryId,
                HashValue: hashValue,
                TravelType: travelType,
                TotalPersonInGroup: totalPersonInGroup,
                ContactPerson: contactPerson,
                ContactNumber: contactNumber,
                NoteType: noteType,
                People: people,
                TravelDateTime: travelDateTime,
                AirportCodeArrival: airportCodeArrival,
                AirportCodeDeparture: airportCodeDeparture,
                Airline: airline,
                FlightNumber: flightNumber,
                CarType: carType,
                CarPlateNumber: carPlate,
                CarColor: carColor,
                DepartureCity: departureCity,
                CruiseName: cruiseName,
                CruiseArrivalPort: cruiseArrivalPort,
                CruiseDeparturePort: cruiseDeparturePort,
                Name: name,
				TerminalStation: terminalStation,
				Checkin :checkin,
				Checkout:checkout
            }

            $.ajax({
                type: 'POST',
                url: "/Form/RegisterTravelGroup",
                contentType: "application/json",
                data: JSON.stringify(requestObj),
                async: true,
                success: function(data) {
                    if (data.Status) {
                        swal("New Travel Group Info has been added .", "", "success");
                        ClearFormValues();
                        $('.create-new-travel-group-modal').modal('hide');
                        UpdateTravelEventFormDashboard(data);
                    } else {
                        swal("New Travel Group Info fail to add", "", "error");
                    }
                },
                error: function(errorCode) {

                }
            });
        }
        else {
            requestObj = {
                Id: currentEditViewTravelEventRecordId,
                InquiryId: inquiryId,
                TotalPersonInGroup: totalPersonInGroup,
                ContactPerson: contactPerson,
                ContactNumber: contactNumber,
                NoteType: noteType,
                People: people,
                TravelDateTime: travelDateTime,
                AirportCodeArrival: airportCodeArrival,
                AirportCodeDeparture: airportCodeDeparture,
                Airline: airline,
                FlightNumber: flightNumber,
                CarType: carType,
                CarPlateNumber: carPlate,
                CarColor: carColor,
                DepartureCity: departureCity,
                CruiseName: cruiseName,
                CruiseArrivalPort: cruiseArrivalPort,
                CruiseDeparturePort: cruiseDeparturePort,
                Name: name,
				TerminalStation: terminalStation,
				Checkin: checkin,
				Checkout: checkout
            }

            $.ajax({
                type: 'POST',
                url: "/Form/UpdateTravelEventRecord",
                contentType: "application/json",
                data: JSON.stringify(requestObj),
                async: true,
                success: function (data) {
                    if (data.Status) {
                        swal("New Travel Group Info has been updated .", "", "success");
                        ClearFormValues();
                        $('.create-new-travel-group-modal').modal('hide');
                        UpdateTravelEventFormDashboard(data);
                    } else {
                        swal("New Travel Group Info fail to update", "", "error");
                    }
                },
                error: function (errorCode) {

                }
            });
        }
    });

    $("#form-cancel-btn").click(function () {
        ClearFormValues();
    });
}

function onCreateNewTravelGroup() {
    $(".create-new-travel-group-btn").click(function () {
        isSubmitToEdit = false;
        currentEditViewTravelEventRecordId = 0;
        $('.create-new-travel-group-modal')
            .modal('show').modal('refresh').modal('refresh');
        setupSemanticUI();
    });
}


function setTravelTypeForTravelEventForm(value) {
    $("input[name='travelType'][value='" + value + "']").prop('checked', true);
    return false; // Returning false would not submit the form
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function UpdateTravelEventFormDashboard(data) {
    var html = "";

    var records = data.TravelGroupInfoList;

    if (records != null && records.length > 0) {
        for (var e = 0; e < records.length; e++) {
            html += "<tr>";
            html += "<td>" + (e + 1) + "</td>";
            html += "<td>" + records[e].ContactPerson + "</td>";
            html += "<td>" + records[e].ContactNumber + "</td>";
			if (records[e].TravelType == 0) {
				html += "<td>Arrival</td>";
			} else if (records[e].TravelType == 13) {
				html += "<td>Self Checkin</td>";
			}
			else if (records[e].TravelType == 14) {
				html += "<td>Self CheckOut</td>";
			}
			else {
				html += "<td>Arrival</td>";
			}
            html += "<td>" + records[e].TravelDateTime + "</td>";
            html += "<td>" + records[e].People + "</td>";
            html += "<td><div><a class='ui mini button edit-form-travel-record-btn' data-tooltip='Edit' onclick='EditViewFormTravelEvent(" + records[e].Id + ")'><i class='icon edit'></i></a><a href='#' class='remove_field' data-tooltip='Delete' onclick='DeleteTravelEventRecord(" + records[e].Id + "," + records[e].InquiryId + ")'><i class='orange minus circle icon remove-field-icon'></i></a></div></td>";
            html += "</tr>";
        }
    }
    else {
        html += "<tr><td colspan='7' style='text-align: center;'>No Records Available</td></tr>";
    }
    $("#travel-event-form-table > tbody").html(html);
}

function ClearFormValues() {
    $("input[name=totalPerson]").val("");
    $("input[name=contactPerson]").val("");
    $("input[name=contactNumber]").val("");
    $("#people").val("");
    $('input[name=arrivalDate]').val("");

    $('input[name=airportCodeArrival]').val("");
    $('input[name=arrivalAirline]').val("");

    $('input[name=arrivalFlightNumber]').val("");

    $('input[name=driveArrivalCarType]').val("");
    $('input[name=driveArrivalCarPlate]').val("");
    $('input[name=driveArrivalCarColor]').val("");

    $('input[name=cruiseArrivalName]').val("");
    $('input[name=cruiseArrivalPort]').val("");

    $('input[name=trainArrivalName]').val("");
    $('input[name=trainArrivalTerminalStation]').val("");
    $('input[name=busArrivalName]').val("");
    $('input[name=busArrivalTerminalStation]').val("");

    $('input[name=departureDate]').val("");

    $('input[name=airportCodeDeparture]').val("");
    $('input[name=departureAirline]').val("");
    $('input[name=departureFlightNumber]').val("");

    $('input[name=driveDepartureCarType]').val("");
    $('input[name=driveDepartureCarPlate]').val("");
    $('input[name=driveDepartureCarColor]').val("");

    $('input[name=cruiseDepartureName]').val("");
    $('input[name=cruiseDeparturePort]').val("");
    $('input[name=trainDepartureName]').val("");
    $('input[name=trainDepartureTerminalStation]').val("");
    $('input[name=busDepartureName]').val("");
    $('input[name=busDepartureTerminalStation]').val("");

    //Reset Dropdown Selections
    $("#arrival-type-filter").val("2").change();   //Change back to flight arrival option
    $("#departure-type-filter").val("3").change(); //Change back to flight departure option
    $("input[name='travelType'][value=1]").prop('checked', true);

    HideAllTravelInfoUISettings();
    $("#FlightArrivalInfoSection").show();
    $("#FlightDepartureInfoSection").show();
}

function DeleteTravelEventRecord(travelEventRecordId, inquiryId) {
    $.ajax({
        type: 'POST',
        url: "/Form/DeleteTravelEventRecord",
        data: { travelEventRecordId: travelEventRecordId, inquiryId: inquiryId },
        async: true,
        success: function (data) {
            if (data.Status) {
                swal("Record Deleted Successfully .", "", "success");
                UpdateTravelEventFormDashboard(data);
            } else {
                swal("Record Fail to Delete", "", "error");
            }
        },
        error: function (errorCode) {

        }
    });
}

function EditViewFormTravelEvent(recordId)
{
    $.ajax({
        type: 'GET',
        url: "/Form/GetTravelEventRecord",
        data: { travelEventRecordId: recordId },
        async: true,
        success: function (data) {
            if (data.Status) {
                var response = data.TravelGroupInfo;

                if (response != null) {
                    isSubmitToEdit = true;
                    currentEditViewTravelEventRecordId = response.Id;
                    HideAllTravelInfoUISettings();
                    $("input[name=totalPerson]").val(response.TotalPersonInGroup);
                    $("input[name=contactPerson]").val(response.ContactPerson);
                    $("input[name=contactNumber]").val(response.ContactNumber);
					$("#people").val(response.People);
					$("input[name=Checkin]").val(response.Checkin);
					$("input[name=Checkout]").val(response.Checkout);
                    if (response.TravelType == 0) {
                        $("input[name='travelType'][value=1]").prop('checked', true);
                        $('input[name=arrivalDate]').val(response.TravelDateTime);
                        $("#arrival-type-filter").val(response.NoteType).change();


                        if (response.NoteType == 2) { //FLIGHT ARRIVAL
                            $('#FlightArrivalInfoSection').show();
                            $('#FlightArrivalInfoSection input[name=airportCodeArrival]').val(response.AirportCodeArrival);
                            $('#FlightArrivalInfoSection input[name=airportCodeDeparture]').val(response.AirportCodeDeparture);
                            $('input[name=arrivalAirline]').val(response.Airline);
                            $('input[name=arrivalFlightNumber]').val(response.FlightNumber);
                        } else if (response.NoteType == 5) { // DRIVE ARRIVAL
                            $('#DriveArrivalInfoSection').show();
                            $('input[name=driveArrivalCarType]').val(response.CarType);
                            $('input[name=driveArrivalCarPlate]').val(response.CarPlateNumber);
                            $('input[name=driveArrivalCarColor]').val(response.CarColor);
                        } else if (response.NoteType == 7) { // CRUISE ARRIVAL
                            $('#CruiseArrivalInfoSection').show();
                            $('input[name=cruiseArrivalName]').val(response.CruiseName);
                            $('#CruiseArrivalInfoSection input[name=cruiseArrivalPort]').val(response.CruiseArrivalPort);
                            $('#CruiseArrivalInfoSection input[name=cruiseDeparturePort]').val(response.CruiseDeparturePort);
                        } else if (response.NoteType == 9) { // TRAIN ARRIVAL
                            $('#TrainArrivalInfoSection').show();
                            $('input[name=trainArrivalName]').val(response.Name);
                            $('input[name=trainArrivalTerminalStation]').val(response.TerminalStation);
                        } else if (response.NoteType == 11) { // BUS ARRIVAL
                            $('#BusArrivalInfoSection').show();
                            $('input[name=busArrivalName]').val(response.Name);
                            $('input[name=busArrivalTerminalStation]').val(response.TerminalStation);
                        }
                    } else if (response.TravelType == 1) {
                        $("input[name='travelType'][value=2]").prop('checked', true);
                        $('input[name=departureDate]').val(response.TravelDateTime);
                        $("#departure-type-filter").val(response.NoteType).change();

                        if (response.NoteType == 3) { //FLIGHT DEPARTURE
                            $('#FlightDepartureInfoSection').show();
                            $('#FlightDepartureInfoSection input[name=airportCodeArrival]').val(response.AirportCodeArrival);
                            $('#FlightDepartureInfoSection input[name=airportCodeDeparture]').val(response.AirportCodeDeparture);
                            $('input[name=departureAirline]').val(response.Airline);
                            $('input[name=departureFlightNumber]').val(response.FlightNumber);
                        } else if (response.NoteType == 6) { // DRIVE DEPARTURE
                            $('#DriveDepartureInfoSection').show();
                            $('input[name=drivedepartureCarType]').val(response.CarType);
                            $('input[name=drivedepartureCarPlate]').val(response.CarPlateNumber);
                            $('input[name=drivedepartureCarColor]').val(response.CarColor);
                        } else if (response.NoteType == 8) { // CRUISE DEPARTURE
                            $('#CruiseDepartureInfoSection').show();
                            $('input[name=cruisedepartureName]').val(response.CruiseName);
                            $('#CruiseDepartureInfoSection input[name=cruiseArrivalPort]').val(response.CruiseArrivalPort);
                            $('#CruiseDepartureInfoSection input[name=cruiseDeparturePort]').val(response.CruiseDeparturePort);
                        } else if (response.NoteType == 10) { // TRAIN DEPARTURE
                            $('#TrainDepartureInfoSection').show();
                            $('input[name=trainDepartureName]').val(response.Name);
                            $('input[name=trainDepartureTerminalStation]').val(response.TerminalStation);
                        } else if (response.NoteType == 12) { // BUS DEPARTURE
                            $('#BusDepartureInfoSection').show();
                            $('input[name=busDepartureName]').val(response.Name);
                            $('input[name=busDepartureTerminalStation]').val(response.TerminalStation);
                        }
                    }

                    $('.create-new-travel-group-modal').modal('show').modal('refresh').modal('refresh');
                    setupSemanticUI();
                } else {
                    isSubmitToEdit = false;
                }
            } else {
                isSubmitToEdit = false;
                swal("Record Fail to Search", "", "error");
            }
        },
        error: function (errorCode) {

        }
    });
}
