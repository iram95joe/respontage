﻿$(document).ready(function () {
    var tableCashFlow;
    initialize();

    function initialize() {
        LoadCashFlowTable();
        SetEventListener();
        onPropertyFilterChange();
    }

    function SetEventListener()
    {
        onClickAddCashFlow();
        onAddCashFlow();
        onDeleteCashFlow();
        onEditCashFlow();
    }

    function LoadCashFlowTable() {
        tableCashFlow = $('#cashflow-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Cashflow/GetAllCashFlowRecord",
                "type": 'POST',
                "data": function () {
                    return {
                        propertyFilter: $('#property-filter').val(),
                        // monthYearFilter: $('#month-year-filter').val()
                    }
                }
            },
            "columns": [
                    { "data": "PropertyName" },
                    { "data": "CashFlowTypeName" },
                    { "data": "Amount" },
                    { "data": "Description" },
                    { "data": "PaymentDate" },
                    { "data": "Owner" },
                    { "data": "Actions", "orderable": false }
            ],

            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-cashflow-id', data.Id);
            }

        });
    }
    function onPropertyFilterChange() {
        $("#property-filter").on('change', function () {
            RefreshCashFlowList();
           
        });
    }
    function RefreshCashFlowList() {
        tableCashFlow.ajax.reload(null, false);
    }
    function clearModal()
    {
        $('#PropertyId').dropdown('clear');
        $('#CashFlowTypeId').dropdown('clear');
        $('#Amount').val('');
        $('#PaymentDate').val('');
        $('#cashflow-id').val('');
        $('#OwnerId').dropdown('clear');

    }

    function onClickAddCashFlow()
    {
        $('#add-cashflow-button').on('click', function () {
            clearModal()
            $('.cash-flow-modal').modal('show');
            $('.ui.calendar').calendar({ type: 'date' });


        });
    }
    function onDeleteCashFlow() {
        $(document).on('click', '.delete-cashflow-btn', function () {
            var id = $(this).parents('tr').attr('data-cashflow-id');
            if (id != '') {
                swal({
                    title: "Are you sure you want to delete this income ?",
                    text: "",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: "/Cashflow/Delete",
                        data: { id: id },
                        success: function (data) {
                            if (data.success) {
                                swal({ title: "Success", text: "Successfully deleted cashflow.", type: "success" },
                                function () {
                                    RefreshCashFlowList();
                                });
                            }
                            else {
                                swal("Error occured on deleting cashflow", "", "error");
                            }
                        }
                    });
                });
            }
        });
    }
    function onEditCashFlow() {
        $(document).on('click', '.edit-cashflow-btn', function () {
            var id = $(this).parents('tr').attr("data-cashflow-id");

            $.ajax({
                type: 'GET',
                url: "/Cashflow/Details",
                data: { id: id },
                success: function (data) {
                    if (data.success) {
                        console.log(data);
                        clearModal();
                        $('#cashflow-id').val(data.cashflow.Id);
                        $('#PropertyId').dropdown('set selected', data.cashflow.PropertyId);
                        $('#CashFlowTypeId').dropdown('set selected', data.cashflow.CashFlowTypeId);
                        $('#Amount').val(data.cashflow.Amount);
                        $('#PaymentDate').val(moment(data.cashflow.PaymentDate).format('MMMM, D YYYY'));
                        $('#Description').val(data.cashflow.Description)
                        $('#OwnerId').dropdown('set selected',data.cashflow.OwnerId);
                        $('.cash-flow-modal').modal('show');
                        $('.ui.calendar').calendar({ type: 'date' });
                    }
                }
            });
        });
    }
    function onAddCashFlow()
    {
        $('#submit-button').on('click', function () {
            var propertyId = $('#PropertyId').val();
            var cashTypeId = $('#CashFlowTypeId').val();
            var paymentDate = $('#PaymentDate').val();
            var amount = $('#Amount').val();
            var id = $('#cashflow-id').val();
            var description = $('#Description').val();
            var ownerId = $('#OwnerId').val() == "" ? 0 : $('#OwnerId').val();
            if (id == '') {
                if (propertyId != '' && cashTypeId != '' && paymentDate != '' && amount != '') {

                    $.ajax({
                        type: 'POST',
                        url: "/Cashflow/Add",
                        data: {
                            PropertyId: propertyId,
                            CashFlowTypeId: cashTypeId,
                            PaymentDate: paymentDate,
                            Amount: amount,
                            description: description,
                            ownerId: ownerId


                        },
                        success: function (data) {
                            if (data.success) {

                                swal({ title: "Success", text: "Cashflow has been saved.", type: "success" },
                                        function () {
                                            RefreshCashFlowList();
                                            clearModal();

                                        });
                                $('.cash-flow-modal').modal('hide');

                            }
                            else {
                                $('.cash-flow-modal').modal('hide');
                                swal(data.message, "", "error");
                            }
                        }

                    });
                }
                else {
                    swal("Please fill all fields", "", "error");
                }
            }
            else
            {
                if(id != '')
                {
                    $.ajax({
                        type: 'POST',
                        url: "/Cashflow/Update",
                        data: {
                            Id: id,
                            cashtype: cashTypeId,
                            property: propertyId,
                            amount: amount,
                            payment: paymentDate,
                            description: description,
                            ownerId: ownerId
                        },
                        success: function (data) {
                            if (data.success) {
                                $('.cash-flow-modal').modal('hide');
                                swal({ title: "Success", text: "Cashflow has been saved.", type: "success" },
                                function () {
                                    RefreshCashFlowList();
                                    clearModal();
                                });
                            }
                            else {
                                $('.cash-flow-modal').modal('hide');
                                swal({ title: "Error", text: "Error on edit cashflow.", type: "error" },
                                function () {
                                    RefreshCashFlowList();
                                    clearModal();
                                });

                            }
                        }
                    });
                }
            }
        });
    }
});