﻿$(document).ready(function () {
	var monthNames = ["Start", "January", "Feb", "March", "April", "May", "June",
		"July", "August", "Sept", "Oct", "Nov", "Dec"
	];

	var dps = [];
	var dpsMonth2 = [];
	//loadCalendar();
    var airbnbAccounts = null;
    loadMessages();
	onMessageClick();
	//initializeChart();


	//function initializeChart() {

	//	$.ajax({
	//		type: 'POST',
	//		url: "/Home/GetGraphList",
	//		data: { status: 1, indexPage: 0 },
	//		dataType: "html",
	//		success: function (msg) {
	//			if (JSON.parse(msg) != "S1") {
	//				dps = JSON.parse(msg);
	//				GetGraphList();
	//			} else {
	//				alert('Error is coming due to unauthorized permission');
	//			}
	//		}
	//	});

	//	$.ajax({
	//		type: 'POST',
	//		url: "/Home/GetTotalMonth",
	//		data: { status: 1, indexPage: 0 },
	//		dataType: "html",
	//		success: function (msg) {
	//			if (JSON.parse(msg) != "S1") {
	//				dpsMonth2 = JSON.parse(msg);
	//				GetSecondGraphList();
	//			} else {
	//				alert('Error is coming due to unauthorized permission');
	//			}
	//		}
	//	});
	//}

	//function GetGraphList() {
	//	$("#buttonclick").html("");
	//	if (dps.length > 0) {
	//		$("#divCheck").show();
	//		$("#divCheck2").show();

	//		for (var x = 0; x < (dps[0].TotalMonth / 6); x++) {
	//			$("#buttonclick").append("<a class='ui primary button' style='margin-right:5px;' onclick='GetGraphTotalPayout(" + x + ")'>" + (x + 1) + "</a>");
	//		}
	//		for (var i = 0; i < dps.length; i++) {
	//			var arrayList = dps[i].MonthName.split("-");
	//			var labels = monthNames[arrayList[0].replace(/^0+/, '')] + ' ' + arrayList[1];
	//			var yVal = dps[i].ResultAmount;
	//			var boilerColor;
	//			boilerColor =
	//				(dps[i].ResultAmount) < 0 ? "Red" : "Green";
	//			dps[i] = { label: labels, y: yVal, color: boilerColor, type: dps[i].MonthName };
	//		};
	//		var chart = new CanvasJS.Chart("chartContainer", {
	//			title: {
	//				text: "Company's Revenue", fontFamily: "arial"
	//			},
	//			data: [
	//				{
	//					type: "column",
	//					bevelEnabled: true,
	//					dataPoints: dps,
	//					click: function (e) {
	//						//alert(e.dataPoint.type);
	//					}
	//				}
	//			]
	//		});
	//		chart.render();
	//	}
	//};

	//function GetGraphTotalPayout(pageIndex) {
	//	var statusFlag = 1;
	//	if ($('#chkBoxValid').is(':checked')) {
	//		statusFlag = 0;
	//	}
	//	$.ajax({
	//		type: 'POST',
	//		url: "/Home/GetGraphList",
	//		data: {
	//			status: statusFlag, indexPage: pageIndex
	//		},
	//		dataType: "html",
	//		success: function (msg) {
	//			if (JSON.parse(msg) != "S1") {
	//				dps = JSON.parse(msg);
	//				GetGraphList();
	//			} else {
	//				alert('Error is coming due to unauthorized permission');
	//			}
	//		}
	//	});
	//}
	//function GetGraphViewDetail(pageIndex) {
	//	dpsMonth2 = [];
	//	var statusFlag = 1;
	//	if ($('#chkBoxValidForSecond').is(':checked')) {
	//		statusFlag = 0;
	//	}
	//	$.ajax({
	//		type: 'POST',
	//		url: "/Home/GetTotalMonth",
	//		data: { status: statusFlag, indexPage: pageIndex },
	//		dataType: "html",
	//		success: function (msg) {
	//			if (JSON.parse(msg) != "S1") {
	//				dpsMonth2 = JSON.parse(msg);
	//				console.log(dpsMonth2);
	//				GetSecondGraphList();
	//			} else {
	//				alert('Error is coming due to unauthorized permission');
	//			}
	//		}
	//	});
	//}
	

	//$(document).on('change', '#chkBoxValid', function (e) {
	//	GetGraphTotalPayout(0);
	//});
	//$(document).on('change', '#chkBoxValidForSecond', function (e) {
	//	GetGraphViewDetail(0);
	//});


    //function RefreshSingleBooking(hostId, reservationCode) {
    //    $.ajax({
    //        type: 'GET',
    //        url: "/Booking/RefreshSingleBooking",
    //        data: { hostId: hostId, reservationCode: reservationCode },
    //        success: function (data) {

    //        }
    //    });
    //}

    function onMessageClick() {
        $(document).on('click', '.dashboard-message', function () {
            var threadId = $(this).attr('data-thread-id');
            var hostId = $(this).attr('data-host-id');
			var guestId = $(this).attr('data-guest-id');

            quickActionModal({
                hostId: hostId,
				threadId: threadId,
				page: 'booking'
            });
        });
    }

	function checkInbox() {
        $.ajax({
            type: "GET",
            url: "/Home/CheckNewMessage",
            success: function (data) {
                if (data.success)
                {
                    console.log('success refresh message');
                    $('#inquiry-message-table').find('tbody').empty().append(data.inquiry);
                    $('#booking-message-table').find('tbody').empty().append(data.booking);
                    $('#trip-coordination-message-table').find('tbody').empty().append(data.active);
                    setTimeout(function () { checkInbox(); }, 10000);
                }
            }
        });
    }

    
	//function loadCalendar() {
	//	$('#dashboard-calendar').fullCalendar({
	//	    now: new moment().format("YYYY-MM-DD"),
	//	    nowIndicator: true,
	//		aspectRatio: 2.2,
	//		eventOverlap: false,
	//		eventLimit: true,
	//		scrollTime: Date.now(),
	//		timeFormat: 'H:mm',
	//		header: {
	//			left: 'today prev,next',
	//			center: 'title',
	//			right: ''
	//		},
	//		defaultView: 'timelineDay',
	//		views: {
	//			timelineDay: {
	//				type: 'timeline',
	//				duration: { days: 1 },
	//				buttonText: 'day'
	//			}
	//		},
	//		//defaultView: 'timelineWeek',
	//		//views: {
	//		//    timelineWeek: {
	//		//        type: 'timeline',
	//		//        duration: { weeks: 1 },
	//		//        slotDuration: { days: 1 },
	//		//        buttonText: 'week'
	//		//    }
	//		//},
	//		resourceLabelText: 'Properties',
	//		resources: function (callback) {
	//			var view = $("#dashboard-calendar").fullCalendar("getView");
	//			$.ajax({
	//				url: $('#dashboard-calendar').attr('data-get-resources-url'),
	//				dataType: "json",
	//				cache: false,
	//				data: {
	//					propertyId: "0",
	//					showPricer: false,
	//					showWorker: true,
	//					showTravelEvents: true
	//				}
	//			}).then(function (resources) {
	//				callback(resources);
	//			})
	//		},
	//		events: {
	//			url: $('#dashboard-calendar').attr('data-get-events-url'),
	//			data: function () {
	//				var dateToDisplay = $('#dashboard-calendar').fullCalendar('getDate');

	//				return {
	//					propertyId: "0",
	//					workerId: "0",
	//					showBooking: true,
	//					showWorker: true,
	//					dateToDisplay: dateToDisplay.format()
	//				};
	//			}
	//		},
	//		eventRender: function (event, element, view) {
	//			//var isMonthView = document.getElementById("calendar-hidden-isMonthView").value == "true" ? true : false;

	//			//if (isMonthView) {
	//			//    $('[highlighted="true"]').hide();
	//			//    $('[highlighted="false"]').hide();
	//			//}

	//			if (event.type == 1) {
	//				if (event.noteType == 1) {
	//					element.prepend('<a><i style="font-size : 20px;" class="yellow privacy icon"></i></a>');
	//					element.attr('data-id', event.id)
	//					element.attr('data-title', event.title);
	//					element.attr('data-content', event.note);
	//					element.attr('data-position', 'top left');
	//					element.css('background-color', 'transparent');
	//					element.css('border-color', 'transparent');
	//					element.find('.fc-content').remove();
	//				}
	//				else if (event.noteType == 2) {
	//					element.prepend('<a><i style="font-size : 20px;" class="yellow plane icon"></i></a>');
	//					element.attr('data-title', event.title);
	//					element.attr('data-content', event.note);
	//					element.attr('data-position', 'top left');
	//					element.css('background-color', 'transparent');
	//					element.css('border-color', 'transparent');
	//					element.find('.fc-content').remove();
	//				}
	//				// ship
	//				else if (event.noteType == 3) {
	//					element.prepend('<a><i style="font-size : 20px;" class="yellow ship icon"></i></a>');
	//					element.attr('data-title', event.title);
	//					element.attr('data-content', event.note);
	//					element.attr('data-position', 'top left');
	//					element.css('background-color', 'transparent');
	//					element.css('border-color', 'transparent');
	//					element.find('.fc-content').remove();
	//				}
	//				// car
	//				else if (event.noteType == 4) {
	//					element.prepend('<a><i style="font-size : 20px;" class="yellow car icon"></i></a>');
	//					element.attr('data-title', event.title);
	//					element.attr('data-content', event.note);
	//					element.attr('data-position', 'top left');
	//					element.css('background-color', 'transparent');
	//					element.css('border-color', 'transparent');
	//					element.find('.fc-content').remove();
	//				}
	//				element.popup();
	//			}
	//			else if (event.type == 2 || event.type == 3 || event.type == 4) {
	//				if (event.image != '' && event.image != null && event.image != undefined) {
	//					element.prepend('<img class="ui avatar image" src=' + event.image + '>');
	//				}

	//				if (event.siteType == 1) {
	//					element.prepend('<a class="ui pink empty circular label"></a>');
	//				}
	//				else if (event.siteType == 2) {
	//					element.prepend('<a class="ui green empty circular label"></a>');
	//				}
	//				else if (event.siteType == 3) {
	//					element.prepend('<a class="ui blue empty circular label"></a>');
	//				}

	//				element.find('.fc-content').css('display', 'inline');
	//				//element.attr('data-title', event.title)
	//				//element.attr('data-content',
	//				//  $.fullCalendar.formatDate(event.start, 'h:mm A') + ' - ' 
	//				//  + $.fullCalendar.formatDate(event.end, 'h:mm A'));
	//				//element.attr('data-position', 'top center');
	//				element.attr('data-event-id', event.eventId);
	//				element.attr('data-host-id', event.hostId);
	//				element.attr('data-thread-id', event.threadId);

	//				if (event.type == 2) {
	//					element.addClass('edit-event');
	//				}
	//				if (event.type == 3) {
	//					element.addClass('edit-worker-schedule');
	//					element.attr('data-schedule-id', event.scheduleId);
	//					//element.append('<i style="float: right; font-size: 14px; margin: 0;" class="remove icon"></i>');
	//				}
	//				if (event.type == 4) {
	//					//console.log(event.date + "\n" + event.isAvailable + "\n" + event.price + "\n" + event.propertyId);
	//					element.addClass('edit-set-price');
	//					//element.addClass('ui button');
	//					//element.addClass('os card');
	//					element.attr("style", "background-color:#FFFFFF; border-color:#FFFFFF; color:#000000;");
	//					element.attr("highlighted", false);
	//					element.attr("data-date", event.date);
	//					element.attr("data-isAvailable", event.isAvailable);
	//					element.attr("data-price", event.price);
	//					element.attr("data-property-id", event.propertyId);
	//					element.attr("data-property-name", event.propertyName);
	//					element.attr("data-siteType", event.siteType);
	//					element.attr("data-vrboResId", event.vrboResId);
	//					element.attr("data-roomId", event.roomId);
	//					element.attr("data-hostId", event.hostId);

	//					//element.attr("os-confirmation", "");
	//					//element.attr("content", "");
	//					//element.attr("options", "ctrl.topPosition");
	//					//element.attr("data-roomCount", event.roomCount);
	//					//element.attr("data-occupancy", event.occupancy);
	//					//element.attr("data-averagePrice", event.averagePrice);
	//					//element.attr("data-averagePriceForDate", event.averagePriceForDate);
	//					//element.attr("data-propertiesCountByDate", event.propertiesCountByDate);
	//					//element.attr("data-percentageAvailability", event.percentageAvailability);

	//					//var analytics = 
	//					//    "\nRoom Count : " + event.roomCount +
	//					//    "\nOccupancy : " + event.occupancy +
	//					//    "\nAverage Price : " + event.averagePrice +
	//					//    "\nAverage Price for this Date : " + event.averagePriceForDate +
	//					//    "\nProperties Count for this Date : " + event.propertiesCountByDate +
	//					//    "\nPercentage Availability : " + event.percentageAvailability + "%";


	//					//element.attr("data-tooltip", analytics.toString());
	//					//element.attr("data-position", "top center");
	//				}
	//			}
	//			else {
	//				element.addClass("edit-set-price");
	//				element.attr("style", "background-color:#FFFFFF; border-color:#FFFFFF; color:#000000;");
	//				element.attr("highlighted", false);
	//				element.attr("data-date", event.date);
	//				element.attr("data-isAvailable", event.isAvailable);
	//				element.attr("data-price", event.price);
	//				element.attr("data-property-id", event.propertyId);
	//				element.attr("data-property-name", event.propertyName);
	//				element.attr("data-siteType", event.siteType);
	//				element.attr("data-vrboResId", event.vrboResId);
	//				element.attr("data-roomId", event.roomId);
	//				element.attr("data-hostId", event.hostId);
	//			}
	//			element.css('cursor', 'pointer');
	//			//element.attr("data-tooltip", event.propertyName);
	//		},
	//		displayEventTime: false
	//	});
	//}

    function loadMessages() {
        $.ajax({
            type: 'GET',
            url: "/Home/LoadDashboardMessages",
            success: function (data) {
                if (data.success) {
                    $('.message-loader').remove();
                    $('.dashboard-message-spacer').remove();
                    $('#inquiry-message-table').find('tbody').append(data.inquiry);
                    $('#booking-message-table').find('tbody').append(data.booking);
					$('#trip-coordination-message-table').find('tbody').append(data.active);

                    checkInbox();
                }
            }
        });
	}

	//function GetGraphViewDetail(pageIndex) {
	//	dpsMonth2 = [];
	//	var statusFlag = 1;
	//	if ($('#chkBoxValidForSecond').is(':checked')) {
	//		statusFlag = 0;
	//	}
	//	$.ajax({
	//		type: 'POST',
	//		url: "/Home/GetTotalMonth",
	//		data: { status: statusFlag, indexPage: pageIndex },
	//		dataType: "html",
	//		success: function (msg) {
	//			if (JSON.parse(msg) != "S1") {
	//				dpsMonth2 = JSON.parse(msg);
	//				console.log(dpsMonth2);
	//				GetSecondGraphList();
	//			} else {
	//				alert('Error is coming due to unauthorized permission');
	//			}
	//		}
	//	});
	//}


	//var dps = [];
	//var dpsMonth2 = [];

	//var monthNames = ["Start", "January", "Feb", "March", "April", "May", "June",
	//	"July", "August", "Sept", "Oct", "Nov", "Dec"
	//];
	//Joem Remove this bacause it don`t have use
	//function GetSecondGraphList() {

	//	if (dpsMonth2.length > 0) {
	//		$("#divSecondCheck").show();
	//		$("#divSecondCheck2").show();
	//		var chart2 = new CanvasJS.Chart("chartSecondContainer");
	//		chart2.options.title = {
	//			text: "Profit and lost forcast by property",
	//			fontFamily: "arial"};
	//		chart2.options.data = [];
	//		$("#buttonclickForSecondGraph").html("");

	//		for (var i = 0; i < (dpsMonth2[0].TotalMonth / 6); i++) {
	//			$("#buttonclickForSecondGraph").append("<a class='ui primary button' style='margin-right:5px;' onclick='GetGraphViewDetail(" + i + ")'>" + (i + 1) + "</a>");
	//		}
	//		for (var a = 0; a < dpsMonth2.length; a++) {
	//			var dpsBar1 = [];
	//			var series1 = { type: "column", name: dpsMonth2[a].PropertyName, showInLegend: true };
	//			for (var x = 0; x < dpsMonth2[a].Result.length; x++) {
	//				var arrayList = dpsMonth2[a].Result[x].MonthName.split("-");
	//				var labels = monthNames[arrayList[0].replace(/^0+/, '')] + ' ' + arrayList[1];

	//				var yVal = dpsMonth2[a].Result[x].ResultAmount;
	//				dpsBar1[x] = { label: labels, y: yVal, name: dpsMonth2[a].PropertyName, type: "/" };
	//			}
	//			chart2.options.data.push(series1);
	//			chart2.options.toolTip = { content: "{name}<hr/>{label}: {y}" };
	//			series1.dataPoints = dpsBar1;
	//		}
	//		chart2.render();
	//	}
	//}
});



//var dps = [];
//var dpsMonth2 = [];
//var monthNames = ["Start", "January", "Feb", "March", "April", "May", "June",
//	"July", "August", "Sept", "Oct", "Nov", "Dec"
//];
//function GetGraphViewDetail(pageIndex) {
//	dpsMonth2 = [];
//	var statusFlag = 1;
//	if ($('#chkBoxValidForSecond').is(':checked')) {
//		statusFlag = 0;
//	}
//	$.ajax({
//		type: 'POST',
//		url: "/Home/GetTotalMonth",
//		data: { status: statusFlag, indexPage: pageIndex },
//		dataType: "html",
//		success: function (msg) {
//			if (JSON.parse(msg) != "S1") {
//				dpsMonth2 = JSON.parse(msg);
//				console.log(dpsMonth2);
//				GetSecondGraphList();
//			} else {
//				alert('Error is coming due to unauthorized permission');
//			}
//		}
//	});
//}


//function GetSecondGraphList() {

//	if (dpsMonth2.length > 0) {
//		$("#divSecondCheck").show();
//		$("#divSecondCheck2").show();
//		var chart2 = new CanvasJS.Chart("chartSecondContainer");
//		chart2.options.title = {
//			text: "Profit and lost forcast by property",
//		fontFamily:"tahoma"};
//		chart2.options.data = [];
//		$("#buttonclickForSecondGraph").html("");

//		for (var i = 0; i < (dpsMonth2[0].TotalMonth / 6); i++) {
//			$("#buttonclickForSecondGraph").append("<a class='ui primary button' style='margin-right:5px;' onclick='GetGraphViewDetail(" + i + ")'>" + (i + 1) + "</a>");
//		}
//		for (var a = 0; a < dpsMonth2.length; a++) {
//			var dpsBar1 = [];
//			var series1 = { type: "column", name: dpsMonth2[a].PropertyName, showInLegend: true };
//			for (var x = 0; x < dpsMonth2[a].Result.length; x++) {
//				var arrayList = dpsMonth2[a].Result[x].MonthName.split("-");
//				var labels = monthNames[arrayList[0].replace(/^0+/, '')] + ' ' + arrayList[1];

//				var yVal = dpsMonth2[a].Result[x].ResultAmount;
//				dpsBar1[x] = { label: labels, y: yVal, name: dpsMonth2[a].PropertyName, type: "/" };
//			}
//			chart2.options.data.push(series1);
//			chart2.options.toolTip = { content: "{name}<hr/>{label}: {y}" };
//			series1.dataPoints = dpsBar1;
//		}
//		chart2.render();
//	}
//}

//function GetGraphTotalPayout(pageIndex) {
//	var statusFlag = 1;
//	if ($('#chkBoxValid').is(':checked')) {
//		statusFlag = 0;
//	}
//	$.ajax({
//		type: 'POST',
//		url: "/Home/GetGraphList",
//		data: {
//			status: statusFlag, indexPage: pageIndex
//		},
//		dataType: "html",
//		success: function (msg) {
//			if (JSON.parse(msg) != "S1") {
//				dps = JSON.parse(msg);
//				GetGraphList();
//			} else {
//				alert('Error is coming due to unauthorized permission');
//			}
//		}
//	});
//}
//function GetGraphList() {
//	$("#buttonclick").html("");
//	if (dps.length > 0) {
//		$("#divCheck").show();
//		$("#divCheck2").show();

//		for (var x = 0; x < (dps[0].TotalMonth / 6); x++) {
//			$("#buttonclick").append("<a class='ui primary button' style='margin-right:5px;' onclick='GetGraphTotalPayout(" + x + ")'>" + (x + 1) + "</a>");
//		}

//		for (var i = 0; i < dps.length; i++) {
//			var arrayList = dps[i].MonthName.split("-");
//			var labels = monthNames[arrayList[0].replace(/^0+/, '')] + ' ' + arrayList[1];
//			var yVal = dps[i].ResultAmount;
//			var boilerColor;
//			boilerColor =
//				(dps[i].ResultAmount) < 0 ? "Red" : "Green";
//			dps[i] = { label: labels, y: yVal, color: boilerColor, type: dps[i].MonthName };
//		};
//		var chart = new CanvasJS.Chart("chartContainer", {
//			title: {
//				text: "Company's Revenue", fontFamily: "arial"
//			},
//			data: [
//				{
//					type: "column",
//					bevelEnabled: true,
//					dataPoints: dps,
//					click: function (e) {
//						//alert(e.dataPoint.type);
//					}
//				}
//			]
//		});
//		chart.render();
//	}
//};