﻿$(document).ready(function () {
    var tableOwner;
    GetOwners();
    Image();
    OwnerBtnClick();
    SaveOwner();
    GetOwnerProperties();
    function GetOwners() {
        tableOwner = $('#owner-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Owners/GetOwnerList",
                "type": 'GET',
            },
            "columns": [
                { "data": "Username" },
                { "data": "Name" },
                { "data": "Properties" },
                { "data": "Actions", "orderable": false }
            ],
            "createdRow": function (row, data, rowIndex) {
                $(row).attr('user-id', data.OwnerId);
            }
        });
    }
    function Image() {
        $(document).on('click', '#profile-image', function (e) {
            $('#profile-pic').click();
        });

        $(document).on('change', '#profile-pic', function () {
            var imageUrl = window.URL.createObjectURL(this.files[0])
            //$('.sub-image').attr("src", imageUrl);
            //alert(imageUrl);
            $('#profile-image').attr("src", imageUrl);
        });
    }
    function OwnerBtnClick() {
        $(document).on('click', '.owner-btn', function (e) {
            $.ajax({
                type: "GET",
                url: '/Owners/GetProperties',
                success: function (data) {
                    $('#property-table tbody').empty();
                    $.each(data.properties, function (index, value) {
                        var rowTemplate = '<tr propertyId="' + value.Id + '">' +
                            '<td>' +
                            '<div class="ui toggle checkbox">' +
                            '<input type="checkbox" class="property-chk"> <label></label>' +
                            '</div>' +
                            '</td>' +
                            '<td>' + value.Name + '</td>' +
                            '<td> ' + GetSiteType(value.SiteType) + '</td>' +
                            '<td>' + value.Host + '<td>' +
                            '</tr>';
                        $('#property-table tbody').append(rowTemplate);
                    });
                }
            });
            $('#owner-modal').modal('show');
        });
    }
    function GetSiteType(site) {
        if (site == 1)
            return "Airbnb";
        else if (site == 2)
            return "Vrbo";
        else if (site == 3)
            return "Booking";
        else if (site == 4)
            return "Homeaway";
        else if (site == 0)
            return "Local";
    }
    function SaveOwner() {
        $(document).on('click', '#save-owner', function (e) {
            var propertyIds = [];
            $('.property-chk').each(function (index, element) {
                var row = $(this).parents('tr');
                if ($(this).prop("checked")) {
                    propertyIds.push(row.attr('propertyId'));
                }
            });
            var formData = new FormData();
            formData.append("Id", $('#owner-modal').attr('userid'));
            formData.append("Image", $('#owner-profile-pic').get(0).files[0]);
            formData.append("UserName", $('#owner-username').val());
            formData.append("Password", $('#owner-password').val());
            formData.append("FirstName", $('#owner-firstname').val());
            formData.append("LastName", $('#owner-lastname').val());
            //formData.append("TimeZone", $('#timezone').val());
            formData.append("PropertyIds", propertyIds);
            $.ajax({
                type: "POST",
                url: "/Owners/RegisterOwner/",
                data: formData,
                beforeSend: function () { $('#save-owner').addClass('loading') },
                contentType: false,
                processData: false,
                success: function (data) {

                    if (data.success) {
                        toastr.success("Owner succesfully saved!", null, { timeOut: 3000, positionClass: "toast-top-right" });
                        $('#save-owner').removeClass('loading')
                        tableOwner.ajax.reload(null, false);
                        $('#owner-modal').modal('hide');
                    }
                }
            });
        });

    }

    function GetOwnerProperties() {
        $(document).on('click', '.edit-owner', function (e) {
            var id = $(this).parents('tr').attr('user-id');
            $.ajax({
                type: "GET",
                url: '/Owners/GetOwnerProperties',
                data: { id: id },
                success: function (data) {
                    $('#property-table tbody').empty();
                    var owner = data.owner;
                    $('#owner-modal').attr('userid', owner.Id);
                    $('#owner-username').val(owner.UserName);
                    $('#owner-firstname').val(owner.FirstName);
                    $('#owner-lastname').val(owner.LastName);
                    $('#profile-image').attr("src", (owner.ImageUrl != null ? owner.ImageUrl : "/Images/User/default-image.png"))
                    $.each(data.properties, function (index, value) {
                        var rowTemplate = '<tr propertyId="' + value.Id + '">' +
                            '<td>' +
                            '<div class="ui toggle checkbox">' +
                            '<input type="checkbox" checked class="property-chk"> <label></label>' +
                            '</div>' +
                            '</td>' +
                            '<td>' + value.Name + '</td>' +
                            '<td> ' + GetSiteType(value.SiteType) + '</td>' +
                            '<td>' + value.Host + '<td>' +
                            '</tr>';
                        $('#property-table tbody').append(rowTemplate);
                    });
                    $.each(data.notConnectedProperties, function (index, value) {
                        var rowTemplate = '<tr propertyId="' + value.Id + '">' +
                            '<td>' +
                            '<div class="ui toggle checkbox">' +
                            '<input type="checkbox" class="property-chk"> <label></label>' +
                            '</div>' +
                            '</td>' +
                            '<td>' + value.Name + '</td>' +
                            '<td> ' + GetSiteType(value.SiteType) + '</td>' +
                            '<td>' + value.Host + '<td>' +
                            '</tr>';
                        $('#property-table tbody').append(rowTemplate);
                    });
                    $('#owner-modal').modal('show');
                }
            });
        });
    }

});