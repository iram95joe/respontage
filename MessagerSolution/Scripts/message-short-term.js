﻿var myHub = $.connection.inboxHub;
$.connection.hub.logging = true;
myHub.client.LockThread = function (lockThreadId, user) {
	var threadId = $('.active-thread').attr('data-thread-id');
	if (lockThreadId == threadId) {
		$('#txtMsgContent').attr("disabled", "disabled");
		$('#btn_Send').attr("disabled", "disabled");
		$('#btn_Clear').attr("disabled", "disabled");
		$('#txtMsgContent').val(user);
	}
};
myHub.client.UnLockThread = function (lockThreadId) {
	var threadId = $('.active-thread').attr('data-thread-id');
	if (lockThreadId == threadId) {
		$('#btn_Send').removeAttr("disabled");
		$('#btn_Clear').removeAttr("disabled");
		$('#txtMsgContent').removeAttr("disabled");
		$('#txtMsgContent').val('');
	}
};

myHub.client.MessageReceived = function (inboxes) {
	var threadId = $('.active-thread').attr('data-thread-id');

	$.each(inboxes, function (key, value) {

		//$.each(value, function (key, value) {
		//	alert(value);
		//});
		var clock = '';
		var date1 = moment(value.LastMessageAt);
		var date2 = moment();
		differenceInMs = date2.diff(date1); // diff yields milliseconds
		duration = moment.duration(differenceInMs); // moment.duration accepts ms
		differenceInMinutes = duration.asMinutes();
		if (differenceInMinutes <= 30 && value.IsIncoming) {
			clock = '<div class="message-indicator ui wait icon green right floated"></div>';
		}
		else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && value.IsIncoming) {
			clock = '<div class="message-indicator ui wait icon yellow rightfloated"></div>';
		}
		else if (differenceInMinutes > 60 && value.IsIncoming) {
			clock = '<div class="message-indicator ui wait icon red right floated"></div>';
		}
		var html = '<div class="item message-thread" data-host-id="' + value.HostAccountId + '" data-thread-id="' + value.ThreadId + '" data-guest-id="' + value.GuestId + '" data-site-type="' + value.SiteType + '" data-reservation-id="">' +
			'<div class="avatar">' +
			'<img src="' + value.GuestProfilePictureUrl + '" height="40" width="40">' +
			'</div>' +
			'<div class="content">' +
			'<div class="author"><strong>' + value.GuestName + '</strong></div>' +
			v.GuestName 
			clock +
			'<br>' +
			'<div class="metadata">' +
			'<div class="date">' + (moment(value.LastMessageAt).isSame(Date.now(), 'day') ? moment(value.LastMessageAt).format("h:mm A") : moment(value.LastMessageAt).format("MMM D, YYYY")) + '</div>' +
			'</div>' +
			'<div class="text">' + (value.LastMessage.length > 15 ? (value.LastMessage.substr(0, 14) + '...') : value.LastMessage) + '</div>' +
			'</div>' +
			'</div>';
		$('.message-thread[data-thread-id="' + value.ThreadId + '"]').remove();
		$('#inbox-list').prepend(html);

		if (value.ThreadId == $('#inbox-list').attr('data-active-thread-id')) {
			var inboxItem = $('.message-thread[data-thread-id="' + $('#inbox-list').attr('data-active-thread-id') + '"]');
			inboxItem.addClass('active-thread');
			inboxItem.css('border-left', '3px solid #00b5ad');
			inboxItem.css('background-color', 'rgba(0, 0, 0, .06)', '!important');
		}

		if (threadId == value.ThreadId) {
			loadConversation(value.Messages);
		}

	});
};

myHub.client.MessageSent = function (threadId, messages) {
	loadConversation(messages);
};
function loadConversation(messages) {
	$("#dv_Conversation").html("");
	$.each(messages, function (index, value) {

		if (!value.IsMyMessage) {
			$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
				'<h5>' + value.MessageContent + '</h5>' +
				'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
				'</div>');
		}
		else {
			$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
				'<h5>' + value.MessageContent + '</h5>' +
				'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
				'</div>');
		}

	});
	$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
}

$.connection.hub.start();

$(document).ready(function () {
	var skipNum = 0;
	loadInbox();
	onInboxClick();
	onButtonSendMessageClick();
	onMessageEndSubmit();
	onButtonSendSMSClick();
	OnChangeMMS();
	ontxtMsgContentClick();
	ontxtMsgContentFocusOut();
	AddContact();
	saveContact();
	LazyLoadInbox();
	onUploadImage();
	GetGuests();
	DetailsClick();
	resizeWindows();
	function onUploadImage() {
		$(document).on('click', '#add-mms', function (e) {
			$('#mms-pic').click();
		});
		
		$(document).on('click', '#short-term-add-mms-btn', function (e) {
			$('#short-term-file-pic').click();
		});
	}
	function GetGuests() {
		$.ajax({
			type: 'GET',
			url: "/Messages/GetGuests",
			async: true,
			success: function (data) {
				$('#guest').siblings('.menu').empty();
				$('#guest').siblings('.menu').append('<div class="item" data-value="0"></div>');
				$.each(data.guests, function (index, guest) {
					$('#guest').siblings('.menu').append
						('<div class="item" data-value="'+guest.Id+'">' +
						'<img class="ui avatar image" src="' + guest.ProfilePictureUrl + '">' +
							guest.Name +
							'</div>');
                                                
				});
			}
		});
	}
	function saveContact() {
		var ContactList = [];

		$(document).on('click', '#save-phone-number', function () {
			var guestId = $('.active-thread').attr('data-guest-id');

			$('.contact-field').each(function (index, element) {
				ContactList.push($(this).find('input[name="contact-num"]').val());
				//var input = $(this).find('input[name="contact-num"]');
				//var iti = intlTelInput(input);
				//alert(window.intlTelInputGlobals.getInstance(iti));
			});
			$.ajax({
				type: "POST",
				url: "/Messages/SaveContact",
				data: { guestId: guestId, ContactList: ContactList/*JSON.stringify(ContactList)*/ },
				success: function (data) {
					$('#contact-info').empty();
					$('#contact-wrapper').empty();
					$('#contact-save').addClass("hidden");
					$.each(data.contactInfos, function (index, contactInfo) {
						let buttons =
							`<button class="circular tiny ui icon button call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
								<button class="circular tiny ui icon button sms-button" data-tooltip="SMS">
																	<i class="icon comments"></i>
																</button>
								<button class="circular tiny ui icon button reassign-button" data-tooltip="Re-assign Contact Info">
																	<i class="icon exchange"></i>
																</button>`;

						if (contactInfo.Type == 2) {
							buttons =
								`<button class="circular ui teal icon button sms-button" data-tooltip="WhatsApp" data-position="left center">
                                        <i class="icon whatsapp"></i>
                                    </button>`;
						} else if (contactInfo.Type == 3) {
							buttons =
								`<button class="circular ui blue icon button sms-button" data-tooltip="Messenger" data-position="left center">
                                        <i class="icon facebook messenger"></i>
                                    </button>`;
						}
						var isDefault = contactInfo.IsDefault ? "checked" : "";
						$('#contact-info').append(`<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}">
															<div class="ui radio checkbox ${isDefault}">
															 <input type="radio" name="contact" ${isDefault}>
															<label>${contactInfo.Value} (${getSiteName(contactInfo.Source)})</label>
															</div><br/>
															${buttons}
														</div>`);
					});
				}
			});
		});


	}
	function AddContact() {
		$(document).on('click', '.add-contact', function () {
			var fields = '<div class="fields">' +
				'<div class="three wide field">' +
				'<div class="contact-field"><div class"ui input">' +
				'<input type="tel" name="contact-num" class="contact-num" placeholder="Phone Number"/></div>' +
				'<button type="button" class="circular ui icon orange mini button remove-contact" data-tooltip="Remove">' +
				'<i class="minus icon"></i>' +
				'</button>' +
				'</div>' +
				'</div>' +
				'</div>';
			$('#contact-wrapper').append(fields);
			//var input = document.querySelector(".contact-num");
			//window.intlTelInput(input, { separateDialCode: true, utilsScript: "~/Content/country-code/utils.js" });
			$('#contact-save').removeClass("hidden");
		});
	}
	function ontxtMsgContentFocusOut() {
		$('#txtMsgContent').focusout(function () {
			$.ajax({
				type: 'POST',
				url: "/Messages/UnLockThread",
				data: {
					threadId: $('.active-thread').attr('data-thread-id')
				},
				success: function (data) {
				}
			});
		});
	}
	function ontxtMsgContentClick() {
		$(document).on('click', '#txtMsgContent', function () {
			if ($('#txtMsgContent').attr('disabled') == undefined) {
				$.ajax({
					type: 'POST',
					url: "/Messages/LockThread",
					data: {
						threadId: $('.active-thread').attr('data-thread-id')
					},
					success: function (data) {
						//if (data.success) {
						//}
						//else {
						//}
					}
				});
			}
		});
	}
	function OnChangeMMS() {
		$(document).on('change', '#mms-pic', function (e) {
			$('#mms-image-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#mms-image-upload').append('<img class="zoom" src="' + imageUrl + '">');
			}
		});
		$(document).on('change', '#short-term-file-pic', function (e) {
			$('#short-term-file-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#short-term-file-upload').append('<img class="zoom" src="' + imageUrl + '">');
			}
		});
	}


	function onMessageEndSubmit() {
		$(document).off('click', '#end-message-btn,#short-term-end-message-btn');
		$(document).on('click', '#end-message-btn,#short-term-end-message-btn', function () {
			$.ajax({
				type: 'POST',
				url: "/Messages/EndThread",
				data: {
					threadId: $('#inbox-list').attr('data-active-thread-id')
				},
				success: function (data) {
					swal("This thread mark as end!", "", "success");
				}
			});
		});
	}
	function onButtonSendSMSClick() {
		$(document).on('click', '#btn_Send_SMS', function () {
			$('#btn_Send_SMS').attr('disabled', true);
			var contact = $('#contact-info').find('.checked');
			let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
			let type = $(contact).parents(".field").attr('data-type');
			var guestId = $('.active-thread').attr('data-guest-id');
			var formData = new FormData();
			for (var i = 0; i < $('#mms-pic').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#mms-pic').get(0).files[i]);
			}
			formData.append("to", phoneNumber);
			formData.append("message", $('#txtMsgContent').val());
			formData.append("type", type);
			formData.append("guestId", guestId);
			$.ajax({
				type: 'POST',
				url: "/SMS/Send",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#btn_Send_SMS').attr('disabled', false);
						toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						$('.item.message-thread.active-thread').click();
					} else {
						toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});
		});
		$(document).on('click', '#short-term-send-sms-message-btn', function () {
			$('#short-term-send-sms-message-btn').attr('disabled', true);
			var contact = $('#short-term-contact-info').find('.checked');
			let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
			let type = $(contact).parents(".field").attr('data-type');
			var guestId = $('.active-thread').attr('data-guest-id');
			var formData = new FormData();
			for (var i = 0; i < $('#short-term-file-pic').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#short-term-file-pic').get(0).files[i]);
			}
			formData.append("to", phoneNumber);
			formData.append("message", $('#short-term-message-box').val());
			formData.append("type", type);
			formData.append("guestId", guestId);
			$.ajax({
				type: 'POST',
				url: "/SMS/Send",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#short-term-message-box').val('')
						$('#short-term-send-sms-message-btn').attr('disabled', false);
						toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
						$('.item.message-thread.active-thread').click();
					} else {
						toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});
		});
	}
	function onButtonSendMessageClick() {
		$(document).on('click', '#btn_Send', function () {
			var siteType = $('.active-thread').attr('data-site-type');
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var msg = $('#txtMsgContent').val();
			if (msg != '') {
				$.ajax({
					type: 'POST',
					url: "/Messages/SendMessageToInbox",
					data: { siteType: siteType, hostId: hostId, threadId: threadId, message: msg },
					dataType: "json",
					beforeSend: function () {
						$('#btn_Send').addClass('loading');
					},
					success: function (data) {

						if (data.success) {
							$('#txtMsgContent').css('border-color', 'rgba(34,36,38,.15)');
							$('#txtMsgContent').val('');
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' + msg + '</h5>' +
								'<p>' + moment().format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
							$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
							$('#btn_Send').removeClass('orange').addClass('teal');
							$('#btn_Send').text('Send');
							$('.active-thread').find('.content').find('.text').text(msg);
							$('.active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
						}
						//else {
						//	$('#txtMsgContent').css('border-color', 'orange');
						//	$('#btn_Send').removeClass('teal').addClass('orange');
						//	$('#btn_Send').text('Retry');
						//}
						$('#btn_Send').removeClass('loading');
					}
				});
			}

		});
		$(document).on('click', '#short-term-send-message-btn', function () {
			var siteType = $('.active-thread').attr('data-site-type');
			var threadId = $('.active-thread').attr('data-thread-id');
			var hostId = $('.active-thread').attr('data-host-id');
			var msg = $('#short-term-message-box').val();
			if (msg != '') {
				$.ajax({
					type: 'POST',
					url: "/Messages/SendMessageToInbox",
					data: { siteType: siteType, hostId: hostId, threadId: threadId, message: msg },
					dataType: "json",
					beforeSend: function () {
						$('#short-term-message-box').addClass('loading');
					},
					success: function (data) {

						if (data.success) {
							$('#short-term-message-box').css('border-color', 'rgba(34,36,38,.15)');
							$('#short-term-message-box').val('');
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' + msg + '</h5>' +
								'<p>' + moment().format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
							$('#short-term-dvConvo').scrollTop($('#short-term-dvConvo')[0].scrollHeight);
							$('#short-term-message-box').removeClass('orange').addClass('teal');
							$('#short-term-message-box').text('Send');
							$('.active-thread').find('.content').find('.text').text(msg);
							$('.active-thread').find('.message-indicator').removeClass('green').removeClass('yellow').removeClass('red');
						}
						//else {
						//	$('#txtMsgContent').css('border-color', 'orange');
						//	$('#btn_Send').removeClass('teal').addClass('orange');
						//	$('#btn_Send').text('Retry');
						//}
						$('#short-term-message-box').removeClass('loading');
					}
				});
			}

		});
	}
	function getVariableList(data) {
		$('#template-variable').empty();

		$('#template-variable').append('<option class="placeholder" selected disabled value="">Select Variable</option>');
		var variables = data.threadDetails.Inquiries[0];
		if (data.threadDetails.Inquiries.length == 1) {
			//vaiables in messages page
			$('#template-variable').append('<option value="' + data.threadDetails.GuestName + '">[Guest Name]</option>');
			$('#template-variable').append('<option value="' + variables.GuestCount + '">[Guest Count]</option>');
			if (variables.Status != 'I' && variables.Status != 'NP') {
				$('#template-variable').append('<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>');
			}
			$('#template-variable').append('<option value="' + moment(variables.CheckInDate).tz("America/Vancouver").format("MMM D, YYYY") + '">[Check In Date]</option>');
			$('#template-variable').append('<option value="' + moment(variables.CheckOutDate).tz("America/Vancouver").format("MMM D, YYYY") + '">[Check Out Date]</option>');

			$('#template-variable').append('<option value="' + variables.PropertyName + '">[Listing Name]</option>');
			$('#template-variable').append('<option value="' + variables.CleaningCost + '">[Cleaning Fee]</option>');
			$('#template-variable').append('<option value="' + (variables.ReservationCost - variables.CleaningCost) + '">[SubTotal]</option>');
			$('#template-variable').append('<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>');
			$('#template-variable').append('<option value="' + variables.PropertyAddress + '">[Listing Address]</option>');
			(variables.PhoneNumber != null && variables.PhoneNumber != "") ? $('#template-variable').append('<option value="' + variables.PhoneNumber.slice(variables.PhoneNumber.length - 4) + '">[Lock Pin Code]</option>') : "";
		}
		else if (data.threadDetails.Inquiries.length > 1) {

			//console.log(data.threadDetails.GuestName);
			$('#template-variable').append('<option value="' + data.threadDetails.GuestName + '">[Guest Name]</option>');
			//$('#template-variable').append('<option value="' + variables.GuestCount + '">[Guest Count]</option>');
			//if (variables.Status != 'I' && variables.Status != 'NP') {
			//	$('#template-variable').append('<option value="' + variables.ConfirmationCode + '">[Confirmation Code]</option>');
		}
		//$('#template-variable').append('<option value="' + moment(variables.CheckInDate).format("MMM D, YYYY") + '">[Check In Date]</option>');
		//$('#template-variable').append('<option value="' + moment(variables.CheckOutDate).format("MMM D, YYYY") + '">[Check Out Date]</option>');
		//$('#template-variable').append('<option value="' + variables.PropertyName + '">[Listing Name]</option>');
		//$('#template-variable').append('<option value="' + variables.CleaningCost + '">[Cleaning Cost]</option>');
		//$('#template-variable').append('<option value="' + variables.ReservationCost + '">[Reservation Cost]</option>');
		//$('#template-variable').append('<option value="' + variables.PropertyAddress + '">[Listing Address]</option>');
		//$('#template-variable').append('<option value="' + variables.PropertyDescription + '">[Listing Description]</option>');
		//$('#template-variable').append('<option value="' + variables.PropertyType + '">[Listing Type]</option>');
	}
	function MobileInbox(data) {
		if (data.success) {
			if (data.user != "") {
				$('#short-term-txtMsgContent').attr("disabled", "disabled");
				$('#short-term-btn_Send').attr("disabled", "disabled");
				$('#short-term-btn_Clear').attr("disabled", "disabled");

				$('#short-term-txtMsgContent').val(data.user);
			}
			else {
				$('#short-term-txtMsgContent').removeAttr("disabled");
				$('#short-term-btn_Send').removeAttr("disabled");
				$('#short-term-btn_Clear').removeAttr("disabled");
				$('#short-term-txtMsgContent').val('');
			}
			$("#short-term-img_Loader").hide();
			getVariableList(data);

			//getTemplateMessage();

			//display the conversation
			if (data.messageThread.length != 0) {
				$.each(data.messageThread, function (index, value) {
					var imageHtml = '<div class="ui tiny images">';
					if (value.Images != null) {
						for (var x = 0; x < value.Images.length; x++) {
							imageHtml += '<img class="zoom" src="' + value.Images[x] + '">';
						}
					}
					imageHtml += '</div>';
					if (!value.IsMyMessage) {
						if (value.Type == 2) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
								(
									value.RecordingUrl == null ?
										'' :
										(
											'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
										)
								) +
								(
									value.MessageContent == null ?
										'' :
										(
											'<p>Transcription: ' + value.MessageContent + '</p>'
										)
								)
								+
								'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
								+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 1) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui  small left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 3) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 4) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>');
						} else {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
						}
					}
					else {
						if (value.Type == 2) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +

								(
									value.RecordingUrl == null ?
										'' :
										(
											'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
										)
								) +
								(
									value.MessageContent == null ?
										'' :
										(
											'<p>Transcription: ' + value.MessageContent + '</p>'
										)
								)
								+
								'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
								+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 1) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui  small right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 3) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 4) {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>');
						} else {
							$("#short-term-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
						}
					}
				});
			}

			else {
				var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #short-term-00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
				$('#short-term-dv_Conversation').append(html);
			}
			//$('#short-term-reservation-detail-property-name').text(data.threadDetails.PropertyName);
			$('#short-term-reservation-detail-guest-image').attr('src', data.threadDetails.GuestProfilePictureUrl);
			$('#short-term-reservation-detail-guest-name').text(data.threadDetails.GuestName);
			$('#short-term-reservation-detail-host-name').text(data.threadDetails.HostName);
			$('#short-term-reservation-detail-host-image').attr('src', data.threadDetails.HostPicture);

			$('#short-term-reservation-detail-guest-image').show();
			$('#short-term-reservation-detail-guest-name').show();
			$('#short-term-reservation-detail-host-name').show();


			$('#short-term-reservation-detail-host-image').show();
			var hasDefault = data.hasDefaultContact;
			$('#short-term-contact-info').empty();
			$.each(data.contactInfos, function (index, contactInfo) {
				let buttons =
					`<button class="circular tiny ui icon button call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
								<button class="circular tiny ui icon button sms-button" data-tooltip="SMS">
																	<i class="icon comments"></i>
																</button>
								<button class="circular tiny ui icon button reassign-button" data-tooltip="Re-assign Contact Info">
																	<i class="icon exchange"></i>
																</button>`;

				if (contactInfo.Type == 2) {
					buttons =
						`<button class="circular mini ui teal icon button sms-button" data-tooltip="WhatsApp">
                                        <i class="icon whatsapp"></i>
                                    </button>`;
				} else if (contactInfo.Type == 3) {
					buttons =
						`<button class="circular mini ui blue icon button sms-button" data-tooltip="Messenger">
                                        <i class="icon facebook messenger"></i>
                                    </button>`;
				}
				var isDefault = "";
				if (hasDefault == false && index == 0) {
					isDefault = "checked";
				}
				else {
					isDefault = contactInfo.IsDefault ? "checked" : "";
				}
				$('#short-term-contact-info').append(`<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}">
															<div class="ui radio checkbox ${isDefault}">
															 <input type="radio" name="contact" ${isDefault}>
															<label>${contactInfo.Value} (${getSiteName(contactInfo.Source)})</label>
															</div><br/>
															${buttons}
															
														</div>`);
			});


			//start
			$('#short-term-reservations').html('');

			$.each(data.threadDetails.Inquiries, function (index, value) {
				//if (value.Status == 'I' || (value.Status == 'A' && value.IsActive) || value.Status == 'B') {				//var data-reservation - id  =(siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.ReservationId : '');
				var reservationId = siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.InquiryId : '';
				$('#short-term-reservation-detail-property-name').text(value.PropertyName);
				$('#short-term-reservation-detail-guest-image').attr('src', data.threadDetails.GuestProfilePictureUrl);
				$('#short-term-reservation-detail-guest-name').text(data.threadDetails.GuestName);
				$('#short-term-reservation-detail-host-name').text(data.threadDetails.HostName);
				$('#short-term-reservation-detail-host-image').attr('src', data.threadDetails.HostPicture);



				//$('#short-term-reservation-detail-status').text(reservation_status);
				//$('#short-term-reservation-detail-check-in').text(moment(value.CheckInDate).format("h:mm A MMM D,YYYY"));
				//$('#short-term-reservation-detail-check-out').text(moment(value.CheckOutDate).format("h:mm A MMM D, YYYY"));
				//$('#short-term-reservation-detail-confirmation-code').text(value.ConfirmationCode);
				//$('#short-term-reservation-details-value-id').val(value.valueId);
				//$('#short-term-reservation-detail-guest-count').text(value.GuestCount == 1 ? (value.GuestCount + ' Guest') : value.GuestCount > 1 ? (value.GuestCount + ' Guests') : '')

				//if (value.CheckInDate != null && value.CheckInDate != null) {
				//	$('#short-term-reservation-details').removeClass('hidden');
				//}
				var action = null;
				if (value.ConfirmationCode != null) {
					$('#short-term-confirmation-code-wrapper').show();
				}
				if (value.Status == "I" && value.CanWithdrawPreApprovalInquiry && !value.CanPreApproveInquiry && !value.CanDeclineInquiry) {
					//$('#short-term-booking-actions-withdraw-pre-approve').show();
					action = '<div><button class="ui blue button js-btn_withdraw-pre-approve ' + reservationId + '">Withdraw Pre-approve</button> </div>'
				}
				else if (value.Status == 'I' && !value.IsSpecialOfferSent) {
					//$('#short-term-booking-actions-inquiry').show();
					if (value.CheckInDate != null && value.CheckOutDate != null) {
						action = '<div>' +
							'<div class="three ui mini buttons">' +
							'<button style="margin: 1%" class="ui blue button js-btn_pre-approve ' + reservationId + '">Pre-Approve</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_decline-inquiry ' + reservationId + '">Decline</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer ' + reservationId + '">Send Special Offer</button>' +
							'</div>' +
							'</div>';
					}

				}

				else if (value.IsSpecialOfferSent) {
					//$('#short-term-booking-actions-withdraw-special-offer').show();
					action = '<div><button class="ui blue button js-btn_withdraw-special-offer ' + reservationId + '" > Withdraw Special Offer</button ></div >';

				}

				if (value.Status == 'A' && value.IsActive) {
					//$('#short-term-note-section').show();
					//$('#short-term-note-save').show();

					//GetNotes(data.threadDetails.valueId);

					if (value.IsAltered) {
						action = '<div class="three ui mini buttons">' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration-booking ' + reservationId + '">Cancel Alteration</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationId + '" >Cancel reservation</button>' +
							((value.Status == 'A' && value.IsActive) ? '<button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button>' : '');


						'</div>'
					}
					else {
						action = '<div class="three ui mini buttons">' +
							'<button style="margin: 1%" class="ui blue button js-btn_alter-booking ' + reservationId + '">Alter Reservation</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationId + '">Cancel Reservation</button>' +
							((value.Status == 'A' && value.IsActive) ? '<div class="one ui mini buttons"><button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button></div>' : '');

						'</div>'

						//$('.js-btn_cancel-alter-booking').hide();
						//$('.js-btn_alter-booking').show();
					}
					//$('#short-term-booking-actions-active').show();
				}
				else if (siteType == 2 && value.Status == 'A') {
					action = '<div class="three ui mini buttons">' +
						'<button style="margin: 1%" class="ui blue button js-btn_alter-booking-vrbo ' + reservationId + '">Alter Reservation</button>' +
						'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking-vrbo ' + reservationId + '">Cancel Reservation</button>' +
						((value.Status == 'A') ? '<div class="one ui mini buttons"><button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button></div>' : '');
					'</div>';
				}

				if (value.Status == 'B') {
					action = '<div class="two ui mini buttons">' +
						'<button style="margin: 1%" class="ui blue button js-btn_accept-booking ' + reservationId + '">Accept</button>' +
						'<button style="margin: 1%" class="ui blue button js-btn_decline-booking ' + reservationId + '">Decline</button>' +
						'</div>';
				}





				var reservation_status = value.Status;
				//Status of value
				if (reservation_status == 'I') { reservation_status = 'Inquiry'; }
				else if (reservation_status == 'SO') { reservation_status = 'Inquiry'; }
				else if (reservation_status == 'IC') { reservation_status = 'Inquiry-Cancelled'; }
				else if (reservation_status == 'A') { reservation_status = 'Confirmed'; }
				else if (reservation_status == 'B') { reservation_status = 'Booking'; }
				else if (reservation_status == 'BC') { reservation_status = 'Booking-Cancelled'; }
				else if (reservation_status == 'C') { reservation_status = 'Confirmed-Cancel'; }
				else if (reservation_status == 'NP') { reservation_status = 'Not Possible'; }

				var item = '<div style="border-bottom: solid;" class="inquiry-item" data-reservation-id="' + reservationId + '">' +
					'<h3 class="color-gray">' + value.PropertyName + '</h3>' +
					'<div class="ui three column grid">' +
					'<div id="reservation-details" class="row">' +
					'<div class="six wide column">' +
					'<h5 class="margin-0">Check in</h5>' +
					'<h6 id="reservation-detail-check-in" class="margin-0 gray">' + (value.CheckInDate == null ? 'Not Specified' : moment(value.CheckInDate).tz("America/Vancouver").format('MMMM D, YYYY')) + '</h6>' +
					'</div>' +
					'<div class="two wide column">' +
					'<h5 class="margin-0">&nbsp;</h5>' +
					'<h6 class="margin-0 gray">' +
					'<i class="arrow right icon"></i>' +
					'</h6>' +
					'</div>' +
					'<div class="six wide column">' +
					'<h5 class="margin-0">Check out</h5>' +
					'<h6 id="reservation-detail-check-out" class="margin-0 gray">' + (value.CheckOutDate == null ? 'Not Specified' : moment(value.CheckOutDate).tz("America/Vancouver").format('MMMM D, YYYY')) + '</h6>' +
					'</div>' +
					'</div>' +
					(value.ConfirmationCode == '' || value.ConfirmationCode == null ? '' : '<h5 style="margin-top: 0">Confirmation Code:<span id="reservation-detail-confirmation-id" class="margin-left-10-px gray">' + value.ConfirmationCode + '</span></h5>') +
					'</div>' +
					'<h5>Number of guest:<span class="margin-left-10-px gray">' + value.GuestCount + '</span></h5>' +
					'<h5 style="margin-top: 0">Status:<span class="margin-left-10-px gray">' + reservation_status + '</span></h5>' +
					'<div id="' + reservationId + '">' +
					(action != null ? action : '') +
					'</div>' +
					'</div></div>';

				$('#short-term-reservations').append(item);
				//if (value.IsActive && value.Status == 'A') {
				//	$('#short-term-reservations').append(item);
				//}
				//else if (value.Status != 'A' && value.Status != 'B') {
				//	$('#short-term-reservations').append(item);
				//}

				//$('#short-term-booking-actions-withdraw-pre-approve').hide();

				$('.ui.radio.checkbox').checkbox();
				$('#short-term-reservation-detail-host-image').show();
				$('#short-term-reservation-details-wrapper').removeClass('hidden');
				$('#short-term-dvConvo').scrollTop($('#short-term-dvConvo')[0].scrollHeight);

			});
			//end
		}
	}

	function BrowserInbox(data) {
		if (data.success) {
			if (data.user != "") {
				$('#txtMsgContent').attr("disabled", "disabled");
				$('#btn_Send').attr("disabled", "disabled");
				$('#btn_Clear').attr("disabled", "disabled");

				$('#txtMsgContent').val(data.user);
			}
			else {
				$('#txtMsgContent').removeAttr("disabled");
				$('#btn_Send').removeAttr("disabled");
				$('#btn_Clear').removeAttr("disabled");
				$('#txtMsgContent').val('');
			}
			$("#img_Loader").hide();
			getVariableList(data);

			//getTemplateMessage();

			//display the conversation
			if (data.messageThread.length != 0) {
				$.each(data.messageThread, function (index, value) {
					var imageHtml = '<div class="ui tiny images">';
					if (value.Images != null) {
						for (var x = 0; x < value.Images.length; x++) {
							imageHtml += '<img class="zoom" src="' + value.Images[x] + '">';
						}
					}
					imageHtml += '</div>';
					if (!value.IsMyMessage) {
						if (value.Type == 2) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
								(
									value.RecordingUrl == null ?
										'' :
										(
											'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
										)
								) +
								(
									value.MessageContent == null ?
										'' :
										(
											'<p>Transcription: ' + value.MessageContent + '</p>'
										)
								)
								+
								'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
								+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 1) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui  small left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 3) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 4) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>');
						} else {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
								'<h5>' + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
						}
					}
					else {
						if (value.Type == 2) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +

								(
									value.RecordingUrl == null ?
										'' :
										(
											'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
										)
								) +
								(
									value.MessageContent == null ?
										'' :
										(
											'<p>Transcription: ' + value.MessageContent + '</p>'
										)
								)
								+
								'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
								+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 1) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui  small right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 3) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>');
						} else if (value.Type == 4) {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>');
						} else {
							$("#dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' +
								(value.ImageUrl != null && value.ImageUrl != "" ? '<div class="ui mini circular image">' +
									'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
									'</div>' : '') + value.MessageContent + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>');
						}
					}
				});
			}

			else {
				var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
				$('#dv_Conversation').append(html);
			}

		
			//$('#reservation-detail-property-name').text(data.threadDetails.PropertyName);
			$('#reservation-detail-guest-image').attr('src', data.threadDetails.GuestProfilePictureUrl);
			$('#reservation-detail-guest-name').text(data.threadDetails.GuestName);
			$('#reservation-detail-host-name').text(data.threadDetails.HostName);
			$('#reservation-detail-host-image').attr('src', data.threadDetails.HostPicture);

			$('#reservation-detail-guest-image').show();
			$('#reservation-detail-guest-name').show();
			$('#reservation-detail-host-name').show();


			$('#reservation-detail-host-image').show();
			var hasDefault = data.hasDefaultContact;
			$('#contact-info').empty();
			$.each(data.contactInfos, function (index, contactInfo) {
				let buttons =
					`<button class="circular tiny ui icon button call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>
								<button class="circular tiny ui icon button sms-button" data-tooltip="SMS">
																	<i class="icon comments"></i>
																</button>
								<button class="circular tiny ui icon button reassign-button" data-tooltip="Re-assign Contact Info">
																	<i class="icon exchange"></i>
																</button>`;

				if (contactInfo.Type == 2) {
					buttons =
						`<button class="circular mini ui teal icon button sms-button" data-tooltip="WhatsApp">
                                        <i class="icon whatsapp"></i>
                                    </button>`;
				} else if (contactInfo.Type == 3) {
					buttons =
						`<button class="circular mini ui blue icon button sms-button" data-tooltip="Messenger">
                                        <i class="icon facebook messenger"></i>
                                    </button>`;
				}
				var isDefault = "";
				if (hasDefault == false && index == 0) {
					isDefault = "checked";
				}
				else {
					isDefault = contactInfo.IsDefault ? "checked" : "";
				}
				$('#contact-info').append(`<div class="field" contact-id="${contactInfo.Id}" data-phonenumber="${contactInfo.Value}" data-type="${contactInfo.Type}">
															<div class="ui radio checkbox ${isDefault}">
															 <input type="radio" name="contact" ${isDefault}>
															<label>${contactInfo.Value} (${getSiteName(contactInfo.Source)})</label>
															</div><br/>
															${buttons}
															
														</div>`);
			});


			//start
			$('#reservations').html('');

			$.each(data.threadDetails.Inquiries, function (index, value) {
				//if (value.Status == 'I' || (value.Status == 'A' && value.IsActive) || value.Status == 'B') {
				var siteType = data.threadDetails.SiteType;
				//var data-reservation - id  =(siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.ReservationId : '');
				var reservationId = siteType == 1 ? value.ConfirmationCode : siteType == 2 ? value.InquiryId : '';
				$('#reservation-detail-property-name').text(value.PropertyName);
				$('#reservation-detail-guest-image').attr('src', data.threadDetails.GuestProfilePictureUrl);
				$('#reservation-detail-guest-name').text(data.threadDetails.GuestName);
				$('#reservation-detail-host-name').text(data.threadDetails.HostName);
				$('#reservation-detail-host-image').attr('src', data.threadDetails.HostPicture);



				//$('#reservation-detail-status').text(reservation_status);
				//$('#reservation-detail-check-in').text(moment(value.CheckInDate).format("h:mm A MMM D,YYYY"));
				//$('#reservation-detail-check-out').text(moment(value.CheckOutDate).format("h:mm A MMM D, YYYY"));
				//$('#reservation-detail-confirmation-code').text(value.ConfirmationCode);
				//$('#reservation-details-value-id').val(value.valueId);
				//$('#reservation-detail-guest-count').text(value.GuestCount == 1 ? (value.GuestCount + ' Guest') : value.GuestCount > 1 ? (value.GuestCount + ' Guests') : '')

				//if (value.CheckInDate != null && value.CheckInDate != null) {
				//	$('#reservation-details').removeClass('hidden');
				//}
				var action = null;
				if (value.ConfirmationCode != null) {
					$('#confirmation-code-wrapper').show();
				}
				if (value.Status == "I" && value.CanWithdrawPreApprovalInquiry && !value.CanPreApproveInquiry && !value.CanDeclineInquiry) {
					//$('#booking-actions-withdraw-pre-approve').show();
					action = '<div><button class="ui blue button js-btn_withdraw-pre-approve ' + reservationId + '">Withdraw Pre-approve</button> </div>'
				}
				else if (value.Status == 'I' && !value.IsSpecialOfferSent) {
					//$('#booking-actions-inquiry').show();
					if (value.CheckInDate != null && value.CheckOutDate != null) {
						action = '<div>' +
							'<div class="three ui mini buttons">' +
							'<button style="margin: 1%" class="ui blue button js-btn_pre-approve ' + reservationId + '">Pre-Approve</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_decline-inquiry ' + reservationId + '">Decline</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_send-special-offer ' + reservationId + '">Send Special Offer</button>' +
							'</div>' +
							'</div>';
					}

				}

				else if (value.IsSpecialOfferSent) {
					//$('#booking-actions-withdraw-special-offer').show();
					action = '<div><button class="ui blue button js-btn_withdraw-special-offer ' + reservationId + '" > Withdraw Special Offer</button ></div >';

				}

				if (value.Status == 'A' && value.IsActive) {
					//$('#note-section').show();
					//$('#note-save').show();

					//GetNotes(data.threadDetails.valueId);

					if (value.IsAltered) {
						action = '<div class="three ui mini buttons">' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-alteration-booking ' + reservationId + '">Cancel Alteration</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationId + '" >Cancel reservation</button>' +
							((value.Status == 'A' && value.IsActive) ? '<button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button>' : '');


						'</div>'
					}
					else {
						action = '<div class="three ui mini buttons">' +
							'<button style="margin: 1%" class="ui blue button js-btn_alter-booking ' + reservationId + '">Alter Reservation</button>' +
							'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking ' + reservationId + '">Cancel Reservation</button>' +
							((value.Status == 'A' && value.IsActive) ? '<div class="one ui mini buttons"><button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button></div>' : '');

						'</div>'

						//$('.js-btn_cancel-alter-booking').hide();
						//$('.js-btn_alter-booking').show();
					}
					//$('#booking-actions-active').show();
				}
				else if (siteType == 2 && value.Status == 'A') {
					action = '<div class="three ui mini buttons">' +
						'<button style="margin: 1%" class="ui blue button js-btn_alter-booking-vrbo ' + reservationId + '">Alter Reservation</button>' +
						'<button style="margin: 1%" class="ui blue button js-btn_cancel-booking-vrbo ' + reservationId + '">Cancel Reservation</button>' +
						((value.Status == 'A') ? '<div class="one ui mini buttons"><button id="trip-note-id" style="margin: 1%" class="ui blue button" data-inquiry-id="' + value.Id + '">Trip Details</button></div>' : '');
					'</div>';
				}

				if (value.Status == 'B') {
					action = '<div class="two ui mini buttons">' +
						'<button style="margin: 1%" class="ui blue button js-btn_accept-booking ' + reservationId + '">Accept</button>' +
						'<button style="margin: 1%" class="ui blue button js-btn_decline-booking ' + reservationId + '">Decline</button>' +
						'</div>';
				}





				var reservation_status = value.Status;
				//Status of value
				if (reservation_status == 'I') { reservation_status = 'Inquiry'; }
				else if (reservation_status == 'SO') { reservation_status = 'Inquiry'; }
				else if (reservation_status == 'IC') { reservation_status = 'Inquiry-Cancelled'; }
				else if (reservation_status == 'A') { reservation_status = 'Confirmed'; }
				else if (reservation_status == 'B') { reservation_status = 'Booking'; }
				else if (reservation_status == 'BC') { reservation_status = 'Booking-Cancelled'; }
				else if (reservation_status == 'C') { reservation_status = 'Confirmed-Cancel'; }
				else if (reservation_status == 'NP') { reservation_status = 'Not Possible'; }

				var item = '<div style="border-bottom: solid;" class="inquiry-item" data-reservation-id="' + reservationId + '">' +
					'<h3 class="color-gray">' + value.PropertyName + '</h3>' +
					'<div class="ui three column grid">' +
					'<div id="reservation-details" class="row">' +
					'<div class="six wide column">' +
					'<h5 class="margin-0">Check in</h5>' +
					'<h6 id="reservation-detail-check-in" class="margin-0 gray">' + (value.CheckInDate == null ? 'Not Specified' : moment(value.CheckInDate).tz("America/Vancouver").format('MMMM D, YYYY')) + '</h6>' +
					'</div>' +
					'<div class="two wide column">' +
					'<h5 class="margin-0">&nbsp;</h5>' +
					'<h6 class="margin-0 gray">' +
					'<i class="arrow right icon"></i>' +
					'</h6>' +
					'</div>' +
					'<div class="six wide column">' +
					'<h5 class="margin-0">Check out</h5>' +
					'<h6 id="reservation-detail-check-out" class="margin-0 gray">' + (value.CheckOutDate == null ? 'Not Specified' : moment(value.CheckOutDate).tz("America/Vancouver").format('MMMM D, YYYY')) + '</h6>' +
					'</div>' +
					'</div>' +
					(value.ConfirmationCode == '' || value.ConfirmationCode == null ? '' : '<h5 style="margin-top: 0">Confirmation Code:<span id="reservation-detail-confirmation-id" class="margin-left-10-px gray">' + value.ConfirmationCode + '</span></h5>') +
					'</div>' +
					'<h5>Number of guest:<span class="margin-left-10-px gray">' + value.GuestCount + '</span></h5>' +
					'<h5 style="margin-top: 0">Status:<span class="margin-left-10-px gray">' + reservation_status + '</span></h5>' +
					'<div id="' + reservationId + '">' +
					(action != null ? action : '') +
					'</div>' +
					'</div></div>';

				$('#reservations').append(item);
				//if (value.IsActive && value.Status == 'A') {
				//	$('#reservations').append(item);
				//}
				//else if (value.Status != 'A' && value.Status != 'B') {
				//	$('#reservations').append(item);
				//}

				//$('#booking-actions-withdraw-pre-approve').hide();

				$('.ui.radio.checkbox').checkbox();
				$('#reservation-detail-host-image').show();
				$('#reservation-details-wrapper').removeClass('hidden');
				$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);

			});
			//end
		}
	}


	function DetailsClick() {
		$(document).on('click', '#details-label', function (e) {
			$(this).popup({
				inline: true,
				hoverable: true,
				position: 'bottom right',
				lastResort: 'bottom right',
				delay: {
					show: 50,
					hide: 0
				},
				on: 'click'
			});
		});
	}


	function resizeWindows() {
		$(window).resize(function () {
			var panel = $('.message-panel');
			if ($(window).width() < 1000) {
				panel.removeClass('grid');
				panel.find('.column:nth-child(2)').css("display", "none");
				panel.find('.column:nth-child(3)').css("display", "none");
				//$('#short-term-inbox').removeAttr('hidden');
			}
			else {
				panel.addClass('grid');
				panel.find('.column:nth-child(2)').css("display", "");
				panel.find('.column:nth-child(3)').css("display", "");
				$('#short-term-inbox').attr('hidden', 'true');
			}
		});
	}
	function onInboxClick() {
		//get the message and details of inquiry
		$(document).on('click', '.message-thread', function () {
			var messageThread = $(this);

			$('#inbox-list').children().css('color', 'black', '!important');
			$('#inbox-list').children().css('padding', '10px');
			$('#inbox-list').children().css('background', 'white');
			$('#inbox-list').children().css('border-radius', '10px');

			messageThread.css('color', 'white', '!important');
			messageThread.css('padding', '10px');
			messageThread.css('background', '#277296');
			messageThread.css('border-radius', '10px');
		
			var id = messageThread.attr('data-thread-id');
			window.history.pushState(null, null, '?threadId=' + id);
			$('#inbox-list').attr('data-active-thread-id', id);
			$('#reservation-details-wrapper').removeClass('hidden').addClass('hidden');
			$('#reservation-details').removeClass('hidden').addClass('hidden');
			$('#booking-actions-active').hide();
			$('#confirmation-code-wrapper').hide();
			$('#booking-actions-pending').hide();
			$('#booking-actions-inquiry').hide();
			$('#booking-actions-withdraw-pre-approve').hide();
			$('#booking-actions-withdraw-special-offer').hide();
			$('#inquiry-note-wrapper').html('');
			$('#reservation-details-inquiry-id').val('');
			$('#dv_Conversation').html('');
			$("#img_Loader").show();
			$('#dvConvo').scrollTop($('#dvConvo')[0].scrollHeight);
			$('#note-section').hide();
			$('#note-save').hide();
			$('#reservation-detail-host-image').hide();

			$.ajax({
				type: 'GET',
				url: "/Messages/GetMessageThread",
				data: { threadId: id },
				dataType: "json",
				async: true,
				success: function (data) {
					$("#dv_Conversation").html("");
					$("#short-term-dv_Conversation").html("");
					var siteType = data.threadDetails.SiteType;
					messageThread.attr('data-reservation-id', siteType == 1 ? data.threadDetails.ConfirmationCode : siteType == 2 ? data.threadDetails.ReservationId : '');

					if ($(window).width() < 1000) {
						$('#short-term-inbox').removeAttr('hidden');
						var threadId = messageThread.attr('data-thread-id')
						$('#short-term-inbox').attr('threadId', threadId);
					}
					else {
						//Set design to selected thread
						$('#short-term-inbox').attr('hidden', true);
					}
					MobileInbox(data);
					BrowserInbox(data)
				}
			});
			firstRun = false;
			$('.message-thread').removeClass('active-thread');
			$('.message-thread[data-thread-id="' + id + '"]').addClass('active-thread');
			document.getElementById('dvConvo').scrollIntoView()
			$("#btn_Send").removeAttr("disabled");
		});
	}
	//Joem 07/27/18 It triggered the Lazy load
	function LazyLoadInbox() {
		$('.message-list').scroll(function () {

			var elem = $('.message-list');
			if (elem[0].scrollHeight - elem.scrollTop() - elem.innerHeight() < 1) {
				skipNum = skipNum + 10;
				$.ajax({
					type: 'POST',
					url: "/Messages/LoadInbox",
					async: true,
					data: { skip: skipNum, take: 10 },
					success: function (data) {
						var html = '';
						$.each(data.result, function (k, v) {
							var clock = '';
							var date1 = moment(v.LastMessageAt).utc();
							var date2 = moment();
							var differenceInMs = date2.diff(date1); // diff yields milliseconds
							var duration = moment.duration(differenceInMs); // moment.duration accepts ms
							var differenceInMinutes = duration.asMinutes();

							if (differenceInMinutes != 0) {
								if (differenceInMinutes <= 30 && v.IsIncoming) {
									clock = '<i class="wait icon green right floated"></i>';
								}
								else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
									clock = '<i class="wait icon yellow right floated"></i>';
								}
								else if (differenceInMinutes > 60 && v.IsIncoming) {
									clock = '<i class="wait icon red right floated"></i>';
								}
							}

							html += '<div class="item message-thread" data-host-id="' + v.HostAccountId + '" data-thread-id="' + v.ThreadId + '" data-guest-id="' + v.GuestId + '" data-site-type="' + v.SiteType + '" data-reservation-id="">' +
								'<div class="avatar">' +
								'<img src="' + v.GuestProfilePictureUrl + '" height="40" width="40">' +
								'</div>' +
								'<div class="content">' +
							    '<div class="author"><strong>' + v.GuestName + '</strong></div>' +
								clock +
								'<br>' +
								'<div class="metadata">' +
								'<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' +
								'</div>' +
								'<div class="text">' + (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage) + '</div>' +
								'</div>' +
								'</div>';
						});
						$('#inbox-list').append(html);
					}
				});

			}
		});
	}
	function loadInbox() {
		$.ajax({
			type: 'POST',
			url: "/Messages/LoadInbox",
			async: true,
			data: { skip: skipNum, take: 10 },
			success: function (data) {
				if (data.result.length > 0) {
					var html = '';
					$.each(data.result, function (k, v) {
						var clock = '';
						var date1 = moment(v.LastMessageAt);
						var date2 = moment();
						var differenceInMs = date2.diff(date1); // diff yields milliseconds
						var duration = moment.duration(differenceInMs); // moment.duration accepts ms
						var differenceInMinutes = duration.asMinutes();

						if (differenceInMinutes != 0) {
							if (differenceInMinutes <= 30 && v.IsIncoming) {
								clock = '<i class="wait icon green right floated"></i>';
							}
							else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
								clock = '<i class="wait icon yellow right floated"></i>';
							}
							else if (differenceInMinutes > 60 && v.IsIncoming) {
								clock = '<i class="wait icon red right floated"></i>';
							}
						}

						html += '<div class="item message-thread" data-host-id="' + v.HostAccountId + '" data-thread-id="' + v.ThreadId + '" data-guest-id="' + v.GuestId + '" data-site-type="' + v.SiteType + '" data-reservation-id="">' +
							'<div class="avatar">' +
							'<img src="' + v.GuestProfilePictureUrl + '" height="40" width="40">' +
							'</div>' +
							'<div class="content">' +
							'<div class="author"><strong>' + v.GuestName  + '</strong></div>' +
							clock +
							'<br>' +
							'<div class="metadata">' +
							'<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' +
							'</div>' +
							'<div class="text">' + (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage) + '</div>' +
							'</div>' +
							'</div>';
					});
					$('#inbox-list').append(html);

					var threadId = $("meta[name=initial_thread_id]").attr('content');

					$('#inbox-list').attr('data-active-thread-id', (threadId != "0" ? threadId : (data.result.length > 0 ? data.result[0].ThreadId : "0")));
					$("#inbox-list").find('.item[data-thread-id="' + $('#inbox-list').attr('data-active-thread-id') + '"]').first().trigger("click");
					$('#short-term-inbox').attr('hidden', 'true');
				}
				else {
					var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
					$('#dvConvo').append(html);
				}
			}
		});
	}
});