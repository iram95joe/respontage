﻿$(document).ready(function () {
	$('.ui.accordion').accordion({
		animateChildren: false
	});
	$('.menu .item').tab({
		onVisible: function (tabPath) {
			if (tabPath == "renter-messages-tab") {
				$('#renter-dvConvo').scrollTop($('#renter-dvConvo')[0].scrollHeight);
			}
		}
	});

	loadRenterInbox();
	onRenterInboxClick();
	onButtonSendSMSClick();
	OnChangeMMS();
	onUploadImage();
	AddContact();
	AddEmail();
	EndCommunicationThread();
	SMSClick();
	CallClick();
	SendEmailClick();
	UploadEmailAttachment();
	function SendEmailClick() {
		$(document).on('click', '#renter-send-btn-email', function () {
			$('#renter-send-btn-email').attr('disabled', true);
			var threadId = $('.renter-active-thread').attr('data-thread-id');
			var formData = new FormData();
			for (var i = 0; i < $('#file-pic').get(0).files.length; ++i) {
				formData.append("Attachments[" + i + "]", $('#file-pic').get(0).files[i]);
			}
			var contact = $('#renter-contact-email').find('.checked');
			let email = $(contact).parents(".field").attr('data-email');
			formData.append("renterEmail", email)
			formData.append("threadId", threadId);
			formData.append("message", $('#renter-txtMsgContent').val());
			$.ajax({
				type: 'POST',
				url: "/Email/Send",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#send-btn-email').attr('disabled', false);
						$('#renter-txtMsgContent').val('');
						$('.renter-active-thread').attr('data-thread-id', data.threadId);
						$('.renter-active-thread').click();
						$('#inbox-list').attr('data-active-thread-id', data.threadId);
						$('#file-upload').empty();
						$('#file-pic').val('');
						toastr.success("Email sent!", null, { timeOut: 3000, positionClass: "toast-top-right" });
					} else {
						toastr.error("Failed sending email!", null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error("Failed sending email!", null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});
			$('#renter-send-btn-email').attr('disabled', false);
		});
	}
	function UploadEmailAttachment() {
		$(document).on('click', '#renter-add-attachment', function (e) {
			$('#file-pic').attr('accept', "");
			$('#file-pic').click();
		});
	}
	function onButtonSendSMSClick() {
		$(document).on('click', '#renter-send-btn-sms', function () {
			$('#renter-send-btn-sms').attr('disabled', true);
			var contact = $('#renter-contact-info').find('.checked');
			let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
			let type = $(contact).parents(".field").attr('data-type');
			var renterId = $('.renter-active-thread').attr('data-id');
			var threadId = $('.renter-active-thread').attr('data-thread-id');
			var formData = new FormData();
			for (var i = 0; i < $('#file-pic').get(0).files.length; ++i) {
				formData.append("Images[" + i + "]", $('#file-pic').get(0).files[i]);
			}
			formData.append("to", phoneNumber);
			formData.append("message", $('#renter-txtMsgContent').val());
			formData.append("type", type);
			formData.append("renterId", renterId);
			formData.append("threadId", threadId);
			$.ajax({
				type: 'POST',
				url: "/SMS/RenterSend",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					$('#renter-send-btn-sms').attr('disabled', false);
					if (data.success) {
						$('#renter-txtMsgContent').val('');
						$('.renter-active-thread').attr('data-thread-id', data.threadId);
						$('.renter-active-thread').click();
						$('#renter-inbox-list').attr('data-active-thread-id', data.threadId);
						$('#file-upload').empty();
						$('#file-pic').val('');
						toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					} else {
						toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
					}
				},
				error: function (error) {
					toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			});

		});
	}
	function EndCommunicationThread() {
		$(document).on('click', '#renter-end-btn', function () {
			var threadId = $('.renter-active-thread').attr('data-thread-id');
			$.ajax({
				type: 'POST',
				url: "/Messages/EndCommunicationThread",
				data: {
					threadId: threadId
				},
				success: function (data) {
					swal("This thread mark as end!", "", "success");
					$('.renter-active-thread').children('.content').children('i').remove();
				}
			});
		});
	}
	function SMSClick(){
		$(document).on('click', '.button.renter-sms-button', function () {
			let phoneNumber = $(this).parents(".field").attr('data-phonenumber');
			let type = $(this).parents(".field").attr('data-type');
			var renterId = $('.renter-active-thread').attr('data-id');
			var threadId = $('.renter-active-thread').attr('data-thread-id');

			$('.send-sms-modal [name="phone-number"]').val(phoneNumber);
			$('.send-sms-modal [name="phone-number"]').attr('disabled', true);
			$('.send-sms-modal [name="message"]').val('');

			$('.send-sms-modal.modal')
				.modal({
					closable: true,
					onApprove: function () {
						$.ajax({
							type: 'POST',
							url: "/SMS/RenterSend",
							data: {
								to: phoneNumber,
								message: $('.send-sms-modal [name="message"]').val(),
								type: type,
								threadId: threadId,
								renterId: renterId
							},
							dataType: "json",
							success: function (data) {
								if (data.success) {

									$('.renter-active-thread').attr('data-thread-id', data.threadId);
									$('.renter-active-thread').click();
									$('#renter-inbox-list').attr('data-active-thread-id', data.threadId);
									toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
									//$('.item.worker-message-thread.worker-active-thread').click();
								} else {
									toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
								}
							},
							error: function (error) {
								toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
							}
						});
					}
				})
				.modal('show').modal('refresh');

		});
	}
	function CallClick(){
		$(document).on('click', '.button.renter-call-button', function () {
			let phoneNumber = $(this).parents(".field").attr('data-phonenumber');

			var threadId = $('.renter-active-thread').attr('data-thread-id');
			var renterId = $('.renter-active-thread').attr('data-id');
			$.ajax({
				type: 'GET',
				url: "/Rentals/RenterThreadInfo",
				data: { threadId: threadId, renterId: renterId },
				dataType: "json",
				beforeSend: function () {
					$(this).addClass('loading');
				},
				success: function (data) {
					$(this).removeClass('loading');
					var params = {
						From: userId,
						To: phoneNumber
					};
					if (device) {
						$('.mini.modal .phone-number').text(phoneNumber);
						$('.mini.modal .header.name').text(data.renter.Name);
						$('.mini.modal .avatar.image').attr('src', '/Images/Worker/default-image.png');
						$('.mini.modal .avatar.image').show();
						device.connect(params);
						openOutgoingCallModal();
					}
				}
			});
		});
	}

	function OnChangeMMS() {
		$(document).on('change', '#file-pic', function (e) {
			$('#file-upload').html('');
			for (var i = 0; i < e.target.files.length; i++) {
				var imageUrl = window.URL.createObjectURL(this.files[i]);
				$('#file-upload').append("<div class='ui field'>" + (this.files[i].type == 'image/jpeg' ? '<img class="zoom ui tiny image" src="' + imageUrl + '">' : '<a target="_blank" href="' + imageUrl + '"><i class="huge icon file alternate"></i></a>') + "</div>");
			}
		});
	}
	function onUploadImage() {
		$(document).on('click', '#renter-add-mms', function (e) {
			$('#file-pic').attr('accept', "image/*");
			$('#file-pic').click();
		});
	}
	function FileIcon(url) {
		var extention = url.split('.').pop();
		if (extention == "pdf") {
			return "file pdf outline";
		}
		else if (extention == "docx" || extention == "docm" || extention == "dot") {
			return "file word outline";
		}
		else if (extention == "xlsx" || extention == "xlsm" || extention == "xlsb") {
			return "file excel outline";
		}
		else {
			return "file alternate";
		}
	}
	function ImageExist(url) {
		var imageExtentions = ['jpg', 'png', 'gif'];
		var extention = url.split('.').pop();
		if (jQuery.inArray(extention, imageExtentions) !== -1) {
			return true;
		}
		else {
			return false;
		}

	}
	function onRenterInboxClick() {
		//get the message and details of inquiry
		$(document).on('click', '.renter-message-thread', function () {
			var messageThread = $(this);
			//set design to all thread
			$('#renter-inbox-list').children().css('color', 'black', '!important');
			$('#renter-inbox-list').children().css('background', 'white');
			$('#renter-inbox-list').children().css('border-radius', '10px');
			$('#renter-inbox-list').children().css('padding', '10px');
			//load message on mini inbox
			var threadId = messageThread.attr('data-thread-id')
			$('#inbox-modal').attr('threadId', threadId);
			GetInboxMessage(threadId);

			//Set design to selected thread
			messageThread.css('color', 'white', '!important');
			messageThread.css('background', '#277296');
			messageThread.css('border-radius', '10px');
			messageThread.css('padding', '10px');
			var id = messageThread.attr('data-thread-id');
			var renterId = messageThread.attr('data-id');
			$('#renter-inbox-list').attr('data-active-thread-id', id);
			$("#renter-dv_Conversation").html('');
			$("#renter-img_Loader").show();
			$('.renter-reservation-list .content').removeAttr('hidden')
			$.ajax({
				type: 'POST',
				url: "/Rentals/LoadRenterInboxMessage",
				data: { threadId: id, renterId: renterId },
				dataType: "json",
				async: true,
				success: function (data) {
					$("#renter-img_Loader").hide();
					if (data.success) {
						//display the conversation
						if (data.Messages.length != 0) {
							$.each(data.Messages, function (index, value) {
								var imageHtml = ''
								if (value.Images != null && value.Images != '') {
									imageHtml = '<div class="ui form"><div class="ui fields">';
									for (var x = 0; x < value.Images.length; x++) {
										imageHtml += "<div class='ui field'>" + (ImageExist(value.Images[x]) ? '<img class="zoom ui tiny image" src="' + value.Images[x] + '">' : '<a target="_blank" href="' + value.Images[x] + '"><i class="huge icon file alternate"></i></a>') + "</div>";
									}
									imageHtml += '</div></div>';
								}

								if (!value.IsMyMessage) {
									if (value.Type == 2) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' +

											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
											'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 5) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui pink small left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
											'</div>');
									}
									else {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
								else {
									if (value.Type == 2) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
											(
												value.RecordingUrl == null ?
													'' :
													(
														'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
													)
											) +
											'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
											+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 1) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui small right pointing label message-label message-outgoing">' +
											'<h5>' +
											'<div class="ui mini circular image">' +
											'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
											'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 3) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									} else if (value.Type == 4) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									}
									else if (value.Type == 5) {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui blue pink right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
											(value.Images != null ? imageHtml : '') +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
											'</div>');
									}
									else {
										$("#renter-dv_Conversation").append('<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
											'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
											'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
											'</div>');
									}
								}
							});
						}
						else {
							var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
							$('#renter-dv_Conversation').append(html);
						}
						$('#renter-contact-info').empty();
						$.each(data.contactInfos, function (index, contactInfo) {

							let buttons =
								`<button class="circular tiny ui icon button renter-call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>`;
							$('#renter-contact-info').append(`
								<div class="field" data-phonenumber="${contactInfo.Contact}" data-type="1" >
									<div class="ui radio checkbox ${(contactInfo.IsDefault ? "checked" : "")}">
									<input type="radio" name="contact" contact-id="${contactInfo.Contact}" checked>
									<label>${contactInfo.Contact}</label>
									</div>
                                        ${buttons}
								</div>
							`);
						});
						$('#renter-contact-email').empty();
						$.each(data.emails, function (index, email) {
							$('#renter-contact-email').append(`
								<div class="field" data-email="${email.Email}">
									<div class="ui radio checkbox ${(email.IsDefault ? "checked" : "")}">
									<input type="radio" name="email" email-id="${email.Email}" checked>
									<label>${email.Email}</label>
									</div>
								</div>
							`);
						});
					}
					$('.ui.radio.checkbox').checkbox();
					$('#renter-dvConvo').scrollTop($('#renter-dvConvo')[0].scrollHeight);
				}
			});

			$('.renter-message-thread').removeClass('renter-active-thread');
			$(this).addClass('renter-active-thread');
			document.getElementById('renter-dvConvo').scrollIntoView();
			if ($(window).width() < 1000) {
				$('.wrapper').removeAttr('hidden');
				
			}
			else {
				$('.wrapper').attr('hidden',true);

			}
		});
	}
	function AddContact() {
		$(document).on('click', '#renter-add-contact-btn', function (e) {
			var renterId = $('.renter-active-thread').attr('data-id');
			swal({
				title: "Add Contact Number",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true
			},
				function (message) {
					if (message) {
						$.ajax({
							type: 'POST',
							url: "/Rentals/AddContact",
							data: { renterId: renterId, phoneNumber: message },
							success: function (data) {
								if (data.success) {
									swal("New contact has been added.", "", "success");
									$('#renter-contact-info').append(`
												<div class="field" data-phonenumber="${message}" data-type="1" >
													<div class="ui radio checkbox checked">
													<input type="radio" name="contact" contact-id="${message}" checked>
													<label>${message}</label>
													</div>
													  <button class="circular tiny ui icon button renter-call-button" data-tooltip="Call">
														<i class="icon phone"></i>
													  </button>
												</div>`);

								}
								else {
									swal("Failed to add new contact", "", "error");
								}
							}
						});
					}
				});
		});

	}
	function AddEmail() {
		$(document).on('click', '#renter-add-email-btn', function (e) {
			var renterId = $('.renter-active-thread').attr('data-id');
			swal({
				title: "Add Email Address",
				type: "input",
				confirmButtonColor: "##abdd54",
				confirmButtonText: "Yes",
				showCancelButton: true,
				closeOnConfirm: false,
				showLoaderOnConfirm: true
			},
				function (message) {
					if (message) {
						$.ajax({
							type: 'POST',
							url: "/Email/AddEmail",
							data: { renterId: renterId, email: message },
							success: function (data) {
								if (data.success) {
									swal("New email has been added.", "", "success");
									$('#renter-contact-email').append(`
								<div class="field" data-email="${emai.Email}">
									<div class="ui radio checkbox checked">
									<input type="radio" name="email" email-id="${email.Email}" checked>
									<label>${email.Email}</label>
									</div>
								</div>
							`);
									
								}
								else {
									swal("Failed to add new email", "", "error");
								}
							}
						});
					}
				});
		});

	}
	$(document).on('keyup', '#search', function () {
		loadRenterInbox();
	});
	function loadRenterInbox() {
		$.ajax({
			type: 'POST',
			url: "/Rentals/LoadRenterInbox",
			data: { search: $('#search').val(), bookingId: $('#renter-inbox-list').attr('bookingId') },
			success: function (data) {

				if (data.result.length > 0) {
					$("#renter-inbox-list").empty();
					$.each(data.result, function (k, v) {

						var clock = '';
						var date1 = moment(v.LastMessageAt);
						var date2 = moment();
						var differenceInMs = date2.diff(date1); // diff yields milliseconds
						var duration = moment.duration(differenceInMs); // moment.duration accepts ms
						var differenceInMinutes = duration.asMinutes();
						if (differenceInMinutes != 0) {
							if (differenceInMinutes <= 30 && v.IsIncoming) {
								clock = '<i class="wait icon green right floated"></i>';
							}
							else if (differenceInMinutes <= 60 && differenceInMinutes > 30 && v.IsIncoming) {
								clock = '<i class="wait icon yellow right floated"></i>';
							}
							else if (differenceInMinutes > 60 && v.IsIncoming) {
								clock = '<i class="wait icon red right floated"></i>';
							}
						}
						var html = '<div class="item renter-message-thread" data-thread-id="' + v.ThreadId + '" data-id="' + v.RenterId + '">' +
							'<div class="avatar">' +
							'<img src="' + v.ImageUrl + '" height="40" width="40">' +
							'</div>' +
							'<div class="content">' +
							'<div class="author"><strong>' + v.Name + '</strong></div>' +
							clock +
							'<div class="metadata">' +
							(v.LastMessageAt != null ? '<div class="date">' + (moment(v.LastMessageAt).isSame(Date.now(), 'day') ? moment(v.LastMessageAt).format("h:mm A") : moment(v.LastMessageAt).format("MMM D, YYYY")) + '</div>' : '') +
							'</div>' +
							'<div class="text">' + (v.LastMessage != null ? (v.LastMessage.length > 15 ? (v.LastMessage.substr(0, 14) + '...') : v.LastMessage) : "Photo") + '</div>' +
							'</div>' +
							'</div>';
						$('#renter-inbox-list').append(html);

					});
					$('#renter-inbox-list').attr('data-active-thread-id', (data.result.length > 0 ? data.result[0].ThreadId : "0"));
					$("#renter-inbox-list").find('.item[data-thread-id="' + $('#renter-inbox-list').attr('data-active-thread-id') + '"]').first().trigger("click");
				}
				else {
					if ($('#search').val() == "") {
						var html = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
						$('#renter-dvConvo').append(html);
					}
				}
			}
		});
	}
});
