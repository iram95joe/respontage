﻿var calendar;
var previousElement = null;
var dateFrom = new moment().format("YYYY-MM-DD");
var dateTo = new moment().format("YYYY-MM-DD");
var price = 10000.00;
var site = "";
var site2 = "";
var cHub = $.connection.calendarHub;
$.connection.hub.logging = true;
cHub.client.ReloadCalendar = function (/*message*/) {
	//document.write(message);
	$('#calendar').fullCalendar('refetchEvents');
};
$.connection.hub.start();

$(document).ready(function () {
	calendar = $('#calendar').fullCalendar({
		now: new moment().format("YYYY-MM-DD"),
		aspectRatio: 2,
		nowIndicator: false,
		handleWindowResize: true,
		height: 500,
		eventOverlap: false,
		eventLimit: false,
		scrollTime: moment().format("HH") + ":00:00",//undo default 6am scrollTime
		header: {
			left: 'today prev,next',
			center: 'title',
			right: 'timelineDay,timelineWeek,timelineTwoWeeks,month'
		},
		defaultView: 'timelineTwoWeeks',
		views: {
			timelineWeek: {
				type: 'timeline',
				duration: { weeks: 1 },
				slotDuration: { days: 1 },
				buttonText: 'week'
			},
			timelineTwoWeeks: {
				type: 'timeline',
				duration: { weeks: 4 },
				slotDuration: { days: 1 },
				buttonText: 'four weeks'
			}
		},
		resourceLabelText: 'Properties',
		resources: function (callback) {

			var view = $("#calendar").fullCalendar("getView");
			$.ajax({
				url: $('#calendar').attr('data-get-resources-url'),
				dataType: "json",
				cache: false,
				async: true,
				data: {
					propertyId: document.getElementById("calendar-hidden-booking").value,
					showPricer: ($("#calendar-show-pricer").val() == undefined ? false : ($("#calendar-show-pricer").is(':checked') ? true : false)),
					showWorker: ($("#calendar-show-worker").val() == undefined ? true : ($("#calendar-show-worker").is(':checked') ? true : false)),
					showTravelEvents: ($("#calendar-show-travel-events").val() == undefined ? true : ($("#calendar-show-travel-events").is(':checked') ? true : false))
				}
			}).then(function (resources) {
				callback(resources);
			});
		},
		resourceGroupField: 'title',
		events: {
			url: $('#calendar').attr('data-get-events-url'),
			data: function () {
				var dateToDisplay = $('#calendar').fullCalendar('getDate');
				return {
					propertyId: document.getElementById("calendar-hidden-booking").value,
					workerId: ($("#calendar-hidden-worker").val() == undefined ? "0" : $("#calendar-hidden-worker").val()),
					showBooking: ($("#calendar-show-booking").val() == undefined ? true : ($("#calendar-show-booking").is(':checked') ? true : false)),
					showWorker: ($("#calendar-show-worker").val() == undefined ? true : ($("#calendar-show-worker").is(':checked') ? true : false)),
					dateToDisplay: dateToDisplay.format()
				};
			}
		},
		eventRender: function (event, element, view) {
			var isMonthView = document.getElementById("calendar-hidden-isMonthView").value == "true" ? true : false;

			if (event.type == 1) {
				element.addClass('edit-travel-event');
				element.attr('data-event-id', event.eventId);
				element.attr('data-host-id', event.hostId);
				element.attr('data-thread-id', event.threadId);

				if (event.noteType == 1) {
					if (event.BookingStatus == 'A') {
						element.prepend('<a><i style="font-size : 20px;" class="yellow privacy icon"></i></a>');
					}
					else {
						element.prepend('<a><i style="font-size : 20px;" class="red privacy icon"></i></a>');
					}
					element.qtip({
						content: event.title + " : " + event.note
					});
					element.attr('data-id', event.id)
					//element.attr('data-tooltip', event.title + " : " + event.note);
					element.attr('data-position', 'top left');
					element.css('background-color', 'transparent');
					element.css('border-color', 'transparent');
					//element.find('.fc-content').remove();
				}
				else if (event.noteType == 2) {
					if (event.BookingStatus == 'A') {
						element.prepend('<a><i style="font-size : 20px;" class="yellow plane icon"></i></a>');
					}
					else {
						element.prepend('<a><i style="font-size : 20px;" class="red plane icon"></i></a>');
					}
					element.qtip({
						content: event.title + " : " + event.note
					});
					//element.attr('data-tooltip', event.title + " : " + event.note);
					element.attr('data-position', 'top left');
					element.css('background-color', 'transparent');
					element.css('border-color', 'transparent');
					//element.find('.fc-content').remove();
				}
				// ship
				else if (event.noteType == 3) {
					if (event.BookingStatus == 'A') {
						element.prepend('<a><i style="font-size : 20px;" class="yellow ship icon"></i></a>');

					}
					else {
						element.prepend('<a><i style="font-size : 20px;" class="red ship icon"></i></a>');
					}
					element.qtip({
						content: event.title + " : " + event.note
					});
					//element.attr('data-tooltip', event.title + " : " + event.note);
					element.attr('data-position', 'top left');
					element.css('background-color', 'transparent');
					element.css('border-color', 'transparent');
					//element.find('.fc-content').remove();
				}
				// car
				else if (event.noteType == 4) {
					if (event.BookingStatus == 'A') {
						element.prepend('<a><i style="font-size : 20px;" class="yellow taxi icon"></i></a>');
					}
					else {
						element.prepend('<a><i style="font-size : 20px;" class="red taxi icon"></i></a>');
					}

					element.qtip({
						content: event.title + " : " + event.note
					});
					//element.attr('data-tooltip', event.title + " : " + event.note);
					element.attr('data-position', 'top center');
					element.css('background-color', 'transparent');
					element.css('border-color', 'transparent');
					//element.find('.fc-content').remove();
				}
				else if (event.noteType == 5) {
					if (event.BookingStatus == 'A') {
						element.prepend('<a><i style="font-size : 20px;" class="yellow clock icon"></i></a>');
					}
					else {
						element.prepend('<a><i style="font-size : 20px;" class="red clock icon"></i></a>');
					}
					element.qtip({
						content: event.title + " : " + event.note
					});
					//element.attr('data-tooltip', event.title + " : " + event.note);
					element.attr('data-position', 'top center');
					element.css('background-color', 'transparent');
					element.css('border-color', 'transparent');
					//element.find('.fc-content').remove();
				}
				//element.popup();
			}
			else if (event.type == 2 || event.type == 3 || event.type == 4 || event.type == 5) {
				if (event.image != '' && event.image != null && event.image != undefined) {

					if (event.siteType == 1) {
						element.prepend('<img class="ui mini avatar image" style="border:2px solid DeepPink " src=' + event.image + '>');
					}
					else if (event.siteType == 2) {
						element.prepend('<img class="ui mini avatar image" style="border:2px solid LimeGreen" src=' + event.image + '>');
					}
					else if (event.siteType == 3) {
						element.prepend('<img class="ui mini avatar image" style="border:2px solid MediumBlue" src=' + event.image + '>');
					}
					else if (event.siteType == 4) {
						element.prepend('<img class="ui mini avatar image" style="border:2px solid MediumBlue" src=' + event.image + '>');
					}
					else {
						element.prepend('<img class="ui mini avatar image" src=' + event.image + '>');
					}
				}

				element.find('.fc-content').css('display', 'inline');
				element.attr('data-event-id', event.eventId);
				element.attr('data-host-id', event.hostId);
				element.attr('data-thread-id', event.threadId);

				if (event.type == 2) {

					element.addClass('edit-event');

					if (event.BookingStatus == 'B') {
						element.attr("style", "border-color:#FF0000; color:#000000;");
					}
				}
				if (event.type == 3) {
					element.addClass('edit-worker-schedule');
					element.attr('data-schedule-id', event.scheduleId);
					if (event.BookingStatus != 'A') {
						element.attr("style", "background-color:#FF0000; border-color:#FF0000; color:#000000;");
					}
					else {
						if (event.AssignmentStatus == 1) {
							element.attr("style", "background-color:#00FF00; border-color:#00FF00; color:#000000;");
						}
						else if (event.AssignmentStatus == 2 || event.AssignmentStatus == 3) {
							element.attr("style", "background-color:#FF0000; border-color:#FF0000; color:#000000;");
						}
						else {
							element.attr("style", "background-color:orange; border-color:orange; color:#000000;");
						}
					}
				}
				if (event.type == 4) {
					element.addClass('edit-set-price');
					if (event.title == "") {
						element.attr("style", "background-color:#8c857b; border-color:#8c857b; color:#8c857b;");
					}
					else {
						element.attr("style", "background-color:#FFFFFF; border-color:#FFFFFF; color:#000000;");
					}
					element.attr("highlighted", false);
					element.attr("data-date", event.date);
					element.attr("data-isAvailable", event.isAvailable);
					element.attr("data-price", event.price);
					element.attr("data-property-id", event.propertyId);
					element.attr("data-property-name", event.propertyName);
					element.attr("data-latitude", event.latitude);
					element.attr("data-longitude", event.longitude);
					element.attr("data-bedrooms", event.bedrooms);
					element.attr("data-siteType", event.siteType);
					element.attr("data-vrboResId", event.vrboResId);
					element.attr("data-roomId", event.roomId);
					element.attr("data-hostId", event.hostId);

				}
				if (event.type == 5) {
					element.addClass('cctv-videos');
					element.attr("style", "background-color:#FFFFFF; border-color:#FFFFFF; color:#000000;");
					if (event.hasFootage) {
						element.children().children().html("<i class='ui icon video' />");
						element.attr("style", "background-color:#FFFFFF; border-color:#FFFFFF; color:#000000;");
						element.attr("data-date", event.date);
						element.attr("data-property-id", event.propertyId);
						element.attr("data-timezone-id",event.timezoneId);
						element.attr("data-property-name", event.propertyName);
						element.attr("data-hostId", event.hostId);
						//element.attr("data-arlo-footage", event.arloCctvFootageUrls);
						//element.attr("data-blink-footage", event.blinkCctvFootageUrls);
					}
					
				}
			}
			else {
				element.addClass("edit-set-price");
				element.attr("style", "background-color:#FFFFFF; border-color:#FFFFFF; color:#000000;");
				element.attr("highlighted", false);
				element.attr("data-date", event.date);
				element.attr("data-isAvailable", event.isAvailable);
				element.attr("data-price", event.price);
				element.attr("data-property-id", event.propertyId);
				element.attr("data-property-name", event.propertyName);
				element.attr("data-latitude", event.latitude);
				element.attr("data-longitude", event.longitude);
				element.attr("data-bedrooms", event.bedrooms);
				element.attr("data-siteType", event.siteType);
				element.attr("data-vrboResId", event.vrboResId);
				element.attr("data-roomId", event.roomId);
				element.attr("data-hostId", event.hostId);
			}
			element.css('cursor', 'pointer');
			//element.attr("data-tooltip", event.propertyName);
		},
		displayEventTime: false,
		loading: function (isLoading, view) {
			if (isLoading) {
				$("#calendar-dimmer").addClass("active").removeClass("inactive");
				//$("#calendar-loader").removeClass("disabled");
				//$("#calendar-loader").text("Initializing Data");
			}
			else {
				$("#calendar-dimmer").addClass("inactive").removeClass("active");
				//$("#calendar-loader").addClass("disabled");
				//$("#calendar-loader").text("Initializing Data");
			}
		},
		eventAfterAllRender: function () {
			$("#calendar-dimmer").addClass("inactive").removeClass("active");
			//$("#calendar-loader").addClass("disabled");
			//$("#calendar-loader").text("Initializing Data");

			if (previousElement == null) {
				previousElement = $('[data-date="' + new moment().format("YYYY-MM-DD") + ' 00:00:00"]').first();
				previousElement.attr("style", previousElement.attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
				previousElement.attr("highlighted", true);
				document.getElementById("hidden-isAvailable").value == previousElement.attr("data-isAvailable") ? "1" : "0";
			}

			var isMonthView = document.getElementById("calendar-hidden-isMonthView").value == "true" ? true : false;

			if (isMonthView) {
				$('[highlighted="true"]').hide();
				$('[highlighted="false"]').hide();
			}
			$(".fc-divider").each(function (index) {
				$(this).children().html("");
				$(this).children().first().attr("style", "height: 0x;");
			});
		}
	});

	onBookingClick();
	onTravelEventClick();
	onAddNote();
	onDeleteNote();
	onSelectedNoteChange();
	onSaveNewBookingTime();
	onAssignWorker();
	onViewWorkers();
	onUnassignWorker();
	workerFilterChange();
	siteFilterChangeExport();
	propertyFilterChangeExport();
	siteFilterChangeImport();

	function workerFilterChange() {
		$('#calendar-worker-id').on('change', function () {
			document.getElementById("calendar-hidden-worker").value = $("#calendar-worker-id").val().join();
			if (document.getElementById("calendar-show-worker").checked) {
				$('#calendar').fullCalendar('refetchEvents');
			}
		});
	}

	$('#calendar-property-id').on('change', function () {
		document.getElementById("calendar-hidden-booking").value = $("#calendar-property-id").val().join();
		$('#calendar').fullCalendar('refetchResources');
		$('#calendar').fullCalendar('refetchEvents');
		$('#calendar').fullCalendar('getDate');

	});




	function onUnassignWorker() {
		$(document).on('click', '#edit-unassign-worker', function () {
			$('#edit-assign-worker-modal').modal('hide');
			var assignmentId = $('#edit-worker-schedule-id').val();
			swal({
				title: "Are you sure you want to unassign this worker ? ",
				text: "",
				type: "warning",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				showCancelButton: true,
				closeOnConfirm: false,
				closeOnCancel: false,
				showLoaderOnConfirm: true
			},
				function (isConfirm) {
					if (isConfirm) {
						$.ajax({
							type: "POST",
							url: "/Home/UnassignWorker/",
							data: { assignmentId: assignmentId },
							success: function () {
								swal("Worker assignment has been successfully removed.", "", "success");
								calendar.fullCalendar('refetchEvents');

							}
						});
					} else {
						swal.close();
						$('#edit-assign-worker-modal').modal('setting', { autofocus: false, detachable: false }).modal("show").modal("refresh");
					}
				});
		});
	}

	function onSaveNewBookingTime() {
		$('#save-new-booking-time-btn').on('click', function () {
			$('#change-time-error-message').hide();
			$('#change-time-success-message').hide();
			$.ajax({
				type: "POST",
				url: "/Booking/ChangeBookingTime",
				data: {
					inquiryId: $('#inquiry-id').val(),
					checkInTime: $('#reservation-check-in-time').val(),
					checkOutTime: $('#reservation-check-out-time').val()
				},
				success: function (data) {
					if (data.success) { $('#change-time-success-message').show(); }
					else { $('#change-time-error-message').show(); }
					calendar.fullCalendar('refetchEvents');

				}
			});
		});
	}

	function onBookingClick() {
		$(document).on('click', '.edit-event', function () {
			var hostId = $(this).attr('data-host-id');
			var threadId = $(this).attr('data-thread-id');
			var inquiryId = $(this).attr('data-event-id');

			quickActionModal({
				hostId: hostId,
				threadId: threadId,
				inquiryId: inquiryId,
				page: 'booking'
			});
		});
	}

	function onTravelEventClick() {
		$(document).on('click', '.edit-travel-event', function () {
			var hostId = $(this).attr('data-host-id');
			var threadId = $(this).attr('data-thread-id');
			var inquiryId = $(this).attr('data-event-id');

			quickActionModal({
				hostId: hostId,
				threadId: threadId,
				inquiryId: inquiryId,
				page: 'travel-events'
			});
		});
	}

	// start assign worker section
	function onViewWorkers() {
		$('#js-btn_view-workers').on('click', function (e) {
			var start_date = moment($('#AssignWorkerStartDate').val()).format('MM/DD/YYYY hh:mm:ss');
			var end_date = moment($('#AssignWorkerEndDate').val()).format('MM/DD/YYYY hh:mm:ss');
			var property_id = $('#AssignWorkerPropertyId').val();
			var job_type_id = $('#AssignWorkerJobTypeId').val();
			$.ajax({
				type: "GET",
				url: "/Home/LoadAvailableWorkers",
				data: { PropertyId: property_id, jobTypeId: job_type_id, StartDate: start_date, EndDate: end_date },
				success: function (data) {
					$('#available-worker-table > tbody').find('tr').remove();
					$.each(data.workers, function (i, item) {
						var html =
							'<tr data-worker-id="' + item.Id + '">' +
							'<td>' + (item.Firstname + ' ' + item.Lastname) + '</td>' +
							'<td>' + (item.Job) + '</td>' +
							'<td>' + (item.PaymentType == 1 ? 'Hourly' : item.PaymentType == 2 ? 'Daily' : item.PaymentType == 3 ? 'Per Appointment' : item.PaymentType == 4 ? 'Fixed' : '') + '</td>' +
							'<td>' + (item.PaymentRate) + '</td>' +
							'<td><div class="ui toggle checkbox">' +
							'<input class="select-worker" type="checkbox" name="assign">' +
							'</div></td>' +
							'</tr>';
						$("#available-worker-table > tbody").append(html);
						$('.ui.checkbox').checkbox();
					});
					if (data.workers.length == 0) {
						var html = '<tr><td colspan="5" id="no-worker-tr" style="text-align: center">No worker available</td></tr>';
						$("#available-worker-table > tbody").append(html);
					}
				},
				error: function (error) {
					$('#assign-worker-modal').modal("hide");
					swal("Error on loading of worker.", "", "error");
				}
			});
		});
	}

	function onAssignWorker() {
		$('.js-btn_assign-worker').on('click', function () {
			$('#AssignWorkerPropertyId').dropdown('clear');
			$('#AssignWorkerJobTypeId').dropdown('clear');
			$('#AssignWorkerStartDate').val('');
			$('#AssignWorkerEndDate').val('');
			$('.assign-worker-modal').modal('show').modal("refresh");
			$('.ui.calendar.dt').calendar({
				type: 'datetime'
			});
			$('.ui.calendar.time').calendar({
				type: 'time'
			});
		});
	}

	$('.ui.checkbox').checkbox();

	$(document).on('click', '.edit-worker-schedule', function () {

		var hostId = $(this).attr('data-host-id');
		var threadId = $(this).attr('data-thread-id');
		var inquiryId = $(this).attr('data-event-id');

		quickActionModal({
			hostId: hostId,
			threadId: threadId,
			inquiryId: inquiryId,
			page: 'workers'
		});
		//var assignmentId = $(this).attr('data-schedule-id');
		//$('#edit-available-worker-table > tbody').find('tr').remove();

		//$.ajax({
		//	type: "GET",
		//	url: "/Home/GetWorkerAssignment",
		//	data: { assignmentId: assignmentId },
		//	success: function (data) {
		//		$('#EditAssignWorkerPropertyId').dropdown('set selected', data.workerAssignment.PropertyId);
		//		$('#EditAssignWorkerJobTypeId').dropdown('set selected', data.workerAssignment.JobTypeId);
		//		$('#edit-worker-schedule-id').val(assignmentId);
		//		$('#EditStartDate').val(moment(data.workerAssignment.StartDate).format('MMMM D, YYYY h:mm A'));
		//		$('#EditEndDate').val(moment(data.workerAssignment.EndDate).format('MMMM D, YYYY h:mm A'));
		//		$('.ui.calendar.dt').calendar({ type: 'datetime' });
		//		$('#edit-available-worker-table > tbody').find('tr').remove();

		//		$.each(data.availableWorker, function (i, item) {
		//			var html =
		//				'<tr data-edit-worker-id="' + item.Id + '">' +
		//				'<td>' + (item.Firstname + ' ' + item.Lastname) + '</td>' +
		//				'<td>' + (item.Job) + '</td>' +
		//				'<td>' + (item.PaymentType == 1 ? 'Hourly' : item.PaymentType == 2 ? 'Daily' : item.PaymentType == 3 ? 'Per Appointment' : item.PaymentType == 4 ? 'Fixed' : '') + '</td>' +
		//				'<td>' + (item.PaymentRate) + '</td>' +
		//				'<td><div class="ui toggle checkbox">' +
		//				'<input class="select-worker" type="checkbox" name="assign" ' + (data.workerAssignment.WorkerId == item.Id ? 'checked' : '') + '>' +
		//				'</div></td>' +
		//				'</tr>';
		//			$("#edit-available-worker-table > tbody").append(html);
		//			$('.ui.checkbox').checkbox();
		//		});
		//		$('.ui.checkbox').checkbox();
		//		$('.ui.dropdown').dropdown({ 'allowTab': false });
		//		$('#edit-assign-worker-modal').modal('setting', { autofocus: false, detachable: false }).modal("show");
		//		$('.ui.calendar.dt').calendar({
		//			type: 'datetime'
		//		});
		//	},
		//	error: function (error) {
		//		console.log(error)
		//		$('#edit-assign-worker-modal').modal("hide");
		//		swal("An error has occured.", "", "error");
		//	},
		//});
	});

	$(document).on('change', 'input.select-worker', function (e) {
		$('input.select-worker').not(this).prop('checked', false);
		$('.assign-worker').removeClass('disabled');
		$('.edit-assign-worker').removeClass('disabled');
		if ($('input.select-worker:checked').length == 0) {
			$('.assign-worker').addClass('disabled');
			$('.edit-assign-worker').addClass('disabled');
		}
	});
	$(document).on('click', '.export-calendar', function (e) {
		var flag = true;
		if ($('#ExportCalendarPropertyId').val() == 0) {
			toastr.warning("Please select property. ", "Warning");
			flag = false;

		}

		if ($("#ExportCalendarRoomId").val() == 0 && site == 3) {
			toastr.warning("Please select the room. ", "Warning");
			flag = false;

		}

		if (site == "") {
			toastr.warning("Please select a site. ", "Warning");
			flag = false;

		}

		if (flag) {
			$.ajax({
				type: "POST",
				url: "/Calendar/	",
				data: {
					propertyId: $('#ExportCalendarPropertyId').val(),
					roomId: $("#ExportCalendarRoomId").val(),
					site: site,

				},
				success: function (data) {

					if (data.success = true) {
						//var filename = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
						var uri = "data:text/calendar;charset=utf8," + escape(data.data);
						var link = document.createElement("a");
						link.href = uri;
						link.style = "display:none";
						link.download = data.property + ".ics";
						document.body.appendChild(link);
						link.click();
						document.body.removeChild(link);

						$('#property-modal-export').modal("hide");
						swal({ title: "Success", text: "Calendar has been successfully exported.", type: "success" },
							function () {


								$.ajax({
									type: "POST",
									url: "/Calendar/ExportCalendarToSite",
									data: {
										propertyId: $('#ExportCalendarPropertyId').val(),

									},
									success: function (data) {

										if (data.success = true) {
											$("#calendarUrl").val(data.data);
											$('#show-calendar-url-modal').modal("show").modal("refresh");
										}

									},
									error: function (response) {
										$('#show-calendar-url-modal').modal("hide");
										swal("Error submitting form.", "", "error");
									}
								});

							});
					}

				},
				error: function (response) {
					$('#property-modal').modal("hide");
					swal("Error submitting form.", "", "error");
				}
			});
		}
	});

	$(document).on('click', '.import-calendar', function (e) {
		//alert('');
		var flag = true;
		if ($('#ImportCalendarPropertyId').val() == 0) {
			toastr.warning("Please select property. ", "Warning");
			flag = false;

		}
		if (site2 == "") {
			toastr.warning("Please select a site. ", "Warning");
			flag = false;

		}

		if (flag) {
			$.ajax({
				type: "POST",
				url: "/Calendar/ScrapeCalendarFromIcal",
				data: {
					propertyId: $('#ImportCalendarPropertyId').val(),

				},
				success: function (data) {

					if (data.success = true) {

						$("#property-modal-import").modal("hide");
						swal({ title: "Success", text: "Calendar has been successfully imported.", type: "success" });

					}

				},
				error: function (response) {
					$("#property-modal-import").modal("hide");
					swal({ title: "Success", text: "Calendar has been successfully imported.", type: "success" });
				}
			});
		}
	});

	$(document).on('click', '.assign-worker', function (e) {
		$.ajax({
			type: "POST",
			url: "/Home/AssignWorker",
			data: {
				PropertyId: $('#AssignWorkerPropertyId').val(),
				WorkerId: $('input.select-worker:checked').parents('tr').attr('data-worker-id'),
				JobTypeId: $('#AssignWorkerJobTypeId').val(),
				StartDate: $('#AssignWorkerStartDate').val(),
				EndDate: $('#AssignWorkerEndDate').val()
			},
			success: function (data) {
				$('#available-worker-table > tbody').find('tr').remove();
				var html = '<tr><td colspan="6" id="no-worker-tr" style="text-align: center">No worker available</td></tr>';
				$("#available-worker-table > tbody").append(html);
				$('#assign-worker-modal').modal("hide");
				swal({ title: "Success", text: "Worker has been successfully assigned.", type: "success" },
					function () {
						$('#calendar').fullCalendar('refetchEvents');

						$('#AssignWorkerStartDate').val('');
						$('#AssignWorkerEndDate').val('');
					});
			},
			error: function (response) {
				$('#assign-worker-modal').modal("hide");
				swal("Error submitting form.", "", "error");
			}
		});
	});

	$(document).on('click', '#edit-assign-worker', function (e) {
		$.ajax({
			type: "POST",
			url: "/Home/EditAssignWorker",
			data: {
				Id: $('#edit-worker-schedule-id').val(),
				PropertyId: $('#EditAssignWorkerPropertyId').val(),
				WorkerId: $('input.select-worker:checked').parents('tr').attr('data-edit-worker-id'),
				JobTypeId: $('#EditAssignWorkerJobTypeId').val(),
				StartDate: $('#EditStartDate').val(),
				EndDate: $('#EditEndDate').val()
			},
			success: function (data) {
				$('#edit-available-worker-table > tbody').find('tr').remove();
				var html = '<tr><td colspan="6" id="no-worker-tr" style="text-align: center">No worker available</td></tr>';
				$("#edit-available-worker-table > tbody").append(html);
				$('#edit-assign-worker-modal').modal("hide");
				swal({ title: "Success", text: "Worker schedule has been successfully updated.", type: "success" },
					function () {
						$('#calendar').fullCalendar('refetchEvents');

					});
			},
			error: function (response) {
				$('#edit-assign-worker-modal').modal("hide");
				swal("An error has occured.", "", "error");
			},
		});
	});
	// end assign worker section



	//setInterval(function () {
	//    $('#calendar').fullCalendar('refetchEvents');
	//}, 10000);




	//end calendar page calendar
});

$(".js-btn_export-calendar").on("click", function () {
	$("#property-modal-export").modal("show").modal("refresh");

});

$(".js-btn_import-calendar").on("click", function () {
	$("#property-modal-import").modal("show").modal("refresh");

});

$(".js-btn_set-price").on("click", function () {
	$("#SetPriceStartDate").val("");
	$("#SetPriceEndDate").val("");

	if (new Date(dateFrom) > new Date(dateTo)) {
		var temp = dateTo;
		dateTo = dateFrom;
		dateFrom = temp;
	}

	if (dateFrom != "" && dateTo != "") {
		$("#SetPriceStartDate").val(dateFrom);
		$("#SetPriceEndDate").val(dateTo);
	}
	else {
		$("#SetPriceStartDate").val(dateFrom);
		$("#SetPriceEndDate").val(dateFrom);
	}

	$("#set-price-block-date").text(document.getElementById("hidden-isAvailable").value == "1" ? "Block Date" : "Unblock Date");
	$("#set-price-modal").modal("show").modal("refresh");
	$('.ui.calendar.dt').calendar({ type: 'date' });
});


var dragging = false;

$(document).on("mouseenter", ".edit-set-price", function (e) {
	bookingDateId = $(this).attr("data-event-id");

	if (dragging) {
		previousElement = $(this);
		$(this).attr("style", $(this).attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
		$(this).attr("highlighted", true);
	}
	else if ($(this).attr("highlighted") == "false") {
		$(this).attr("style", $(this).attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
	}
});

$(document).on("mouseleave", ".edit-set-price", function (e) {
	bookingDateId = $(this).attr("data-event-id");

	if ($(this).attr("highlighted") == "false") {
		$(this).attr("style", $(this).attr("style").replace("background-color: #99ffff;", "background-color: rgb(255, 255, 255);"));
	}
});

Number.prototype.pad = function (size) {
	var s = String(this);
	while (s.length < (size || 2)) { s = "0" + s; }
	return s;
}

$("#analytics-done").click(function () {
	$("#analytics-modal").modal("hide");
});


$(document).on("click", ".delete-video", function (e) {
	var videoId = $(this).attr('videoId');
	var parent = $(this).closest('.div-video');
	swal({
		title: "Are you sure you want to delete video?",
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Delete",
		showCancelButton: true,
		closeOnConfirm: true,
	},
		function () {
			$.ajax({
				type: 'POST',
				url: "/Video/DeleteVideo",
				data: { videoId: videoId },
				success: function (data) {
					if (data.success) {
						parent.remove();
					}
				}
			});
		});
			
});
$(document).on("click", ".save-video", function (e) {
	var videoId = $(this).attr('videoId');
	var isSave = $(this).prop("checked")
	$.ajax({
		type: 'POST',
		url: "/Video/SaveVideo",
		data: { videoId: videoId, isSave: isSave },
		success: function (data) {
		}
	});

});

$(document).on("click", ".multiple-video-delete", function (e) {
	swal({
		title: "Are you sure you want to delete video?",
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Delete",
		showCancelButton: true,
		closeOnConfirm: true,
	},
		function () {
			var videoIds =[]
			$('.select-video').each(function (index, element) {
				var parent = $(this).closest('.div-video');
				var videoId = $(this).attr('videoId');
				if ($(this).prop("checked")) {
					videoIds.push(videoId);
					parent.remove();
				}
			});
			$.ajax({
				type: 'POST',
				url: "/Video/DeleteVideos",
				data: { videoIds: JSON.stringify(videoIds) },
				async: true,
				success: function (data) {
					if (data.success) {
					}
				}
			});
		});
});

$(document).on("click", ".cctv-videos", function (e) {
	var listingId = String($(this).attr('data-property-id'));
	var listingName = String($(this).attr('data-property-name'));
	var date = String($(this).attr('data-date'));
	var timezoneId = $(this).attr('data-timezone-id') != undefined ? $(this).attr('data-timezone-id'):"America/Los_Angeles";
	$.ajax({
		type: "GET",
		url: "/Video/GetVideos",
		data: { date: date, listingId: listingId },
		success: function (data) {
			if (data.success) {
				var videos = data.videos;
				if (videos.length > 0) {
					var videoHtml = "<center><div class='ui divider'></div>";
					for (var i = 0; i < videos.length; i++) {
							videoHtml = videoHtml +
								'<div class="div-video">' +
								'<div>' +
								'<video width="400" controls>' +
								'<source src="' + videos[i].Video.VideoUrl+ '" type="video/mp4">' +
								'Your browser does not support HTML5 video.' +
								'</video>' +
								'</div>' +
								'<div><h5 ' + (videos[i].Video.IsWatched ? 'data-tooltip="Watch"' : 'data-tooltip="Unwatch"') + '>' + (videos[i].Video.IsWatched ? '<i class="play circle icon"></i>' : '<i class="play circle outline icon"></i>') + DeviceSiteType(videos[i].Account.SiteType) + ' : Camera "' + videos[i].Device.Name + '", ' + moment(videos[i].Video.DateCreated).tz(timezoneId).format('hh:mm A') + '</h5><div class="ui checked checkbox"><input type="checkbox" videoId="' + videos[i].Video.Id + '" class="select-video"><label>Select</label></div><a href="' + videos[i].Video.VideoUrl + '" download="' + videos[i].Device.Name + " " + moment(videos[i].Video.DateCreated).format('MMM D YYYY hh mm A') + '"><button class="ui mini button" data-tooltip="Download"><i class="icon download"></i></button></a><button class="ui mini red button delete-video" videoId="' + videos[i].Video.Id + '" data-tooltip="Delete"><i class="icon trash alternate outline"></i></button><div class="ui checked checkbox"><input type="checkbox" videoId="' + videos[i].Video.Id + '" ' + (videos[i].Video.IsSaved ? 'checked' : '') + ' class="save-video"><label>Save</label></div></div>' +
								'<div class="ui divider"></div>' +
								'</div>';
						}

					videoHtml = videoHtml + "</center>";
					$(".video-div").html(videoHtml);
					$(".video-title").html("<h4>" + listingName + " (" + listingId + ") - " + moment(date).format('MMMM DD,YYYY') + "</h4>");
					$('.ui.longer.modal').modal('show').modal("refresh");
				}
			}
		},
		error: function (error) {
		}
	});


});

$(document).on("click", ".edit-set-price", function (e) {
	bookingDateId = $(this).attr("data-event-id");
	propertyId = $(this).attr("data-property-id");
	propertyName = $(this).attr("data-property-name");
	date = $(this).attr("data-date");
	$("#set-price-propertyName").text("Set Price for Property \"" + propertyName + "\"");

	if ($(this).attr("highlighted") == "true") {
		var thisElement = $(this);

		//if ($("#calendar-show-analytics").is(':checked')) {
		//    $.ajax({
		//        type: "POST",
		//        url: "/Calendar/ShowAnalytics",
		//        data: { propertyId: propertyId, date: date },
		//        success: function (analytics) {
		//            $("#analytics-property-name").text("   " + propertyName);
		//            $("#analytics-date").text("   " + moment(date).format("MMMM DD, YYYY"));

		//            $("#analytics-property-count-month").text("   " + analytics.PropertyCountByMonth);
		//            $("#analytics-property-count-month-room").text("   " + analytics.PropertyCountByMonthAndRoomCount);
		//            $("#analytics-property-count-date-room").text("   " + analytics.PropertyCountByDateAndRoomCount);

		//            $("#analytics-occupancy-rate-month").text("   " + parseFloat(analytics.OccupancyByMonth).toFixed(2) + "%");
		//            $("#analytics-occupancy-rate-month-room").text("   " + parseFloat(analytics.OccupancyByMonthAndRoomCount).toFixed(2) + "%");
		//            $("#analytics-occupancy-rate-date-room").text("   " + parseFloat(analytics.OccupancyByDateAndRoomCount).toFixed(2) + "%");

		//            $("#analytics-availability-rate-month").text("   " + parseFloat(analytics.AvailabilityByMonth).toFixed(2) + "%");
		//            $("#analytics-availability-rate-month-room").text("   " + parseFloat(analytics.AvailabilityByMonthAndRoomCount).toFixed(2) + "%");
		//            $("#analytics-availability-rate-date-room").text("   " + parseFloat(analytics.AvailabilityByDateAndRoomCount).toFixed(2) + "%");

		//            $("#analytics-average-price-month").text("   " + parseFloat(analytics.AveragePriceByMonth).toFixed(2));
		//            $("#analytics-average-price-month-room").text("   " + parseFloat(analytics.AveragePriceByMonthAndRoomCount).toFixed(2));
		//            $("#analytics-average-price-date-room").text("   " + parseFloat(analytics.AveragePriceByDateAndRoomCount).toFixed(2));

		//            $("#analytics-modal").modal("show");
		//        }
		//    });
		//}

		if (dateTo != "") {
			dateFrom = $(this).attr("data-date");
			dateTo = "";
			price = $(this).attr("data-price");

			$('[highlighted="true"]').each(function () {
				$(this).attr("style", $(this).attr("style").replace("background-color: #99ffff;", "background-color: rgb(255, 255, 255);"));
				$(this).attr("highlighted", false);
			});

			$(this).attr("style", $(this).attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
			previousElement = $(this);
			$(this).attr("highlighted", true);
		}
		else {
			$(this).attr("style", $(this).attr("style").replace("background-color: #99ffff;", "background-color: rgb(255, 255, 255);"));
			dateFrom = "";
			$(this).attr("highlighted", false);
		}
	}
	else {
		$(this).attr("style", $(this).attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
		document.getElementById("hidden-propertyId").value = $(this).attr("data-property-id");
		document.getElementById("hidden-isAvailable").value = $(this).attr("data-isAvailable") == "true" ? "1" : "0";
		document.getElementById("hidden-siteType").value = $(this).attr("data-siteType");
		document.getElementById("hidden-hostId").value = $(this).attr("data-hostId");
		document.getElementById("hidden-vrboResId").value = $(this).attr("data-vrboResId");
		document.getElementById("hidden-roomId").value = $(this).attr("data-roomId");
		document.getElementById("SetPriceTb").value = $(this).attr("data-price");
		$(".js-btn_block-date").text(document.getElementById("hidden-isAvailable").value == "1" ? "Block Date" : "Unblock Date");
		price = $(this).attr("data-price");

		if (e.shiftKey && $(this).attr("data-property-id") == previousElement.attr("data-property-id")) {
			dateTo = $(this).attr("data-date");

			for (var date = new Date(dateFrom); date <= new Date(dateTo); date.setDate(date.getDate() + 1)) {
				var targetElement = $('[data-date="' + date.getFullYear() + "-" + (date.getMonth() + 1).pad() + "-" + date.getDate().pad() +
					" 00:00:00" + '"][data-property-id="' + $(this).attr("data-property-id") + '"]');
				targetElement.attr("style", targetElement.attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
				targetElement.attr("highlighted", true);
			}
		}
		else if (previousElement != null) {
			dateFrom = $(this).attr("data-date");
			dateTo = $(this).attr("data-date");

			$('[highlighted="true"]').each(function () {
				$(this).attr("style", $(this).attr("style").replace("background-color: #99ffff;", "background-color: rgb(255, 255, 255);"));
				$(this).attr("highlighted", false);
			});

			previousElement.attr("style", previousElement.attr("style").
				replace("background-color: #99ffff;", "background-color: rgb(255, 255, 255);"));
			previousElement.attr("highlighted", false);
		}
		previousElement = $(this);
		$(this).attr("highlighted", true);
	}

	if ($("#calendar-show-analytics").is(':checked')) {

		var propertyName = $(this).attr("data-property-name");
		var latitude = $(this).attr("data-latitude");
		var longitude = $(this).attr("data-longitude");
		var date = $(this).attr("data-date");
		var bedrooms = $(this).attr("data-bedrooms");

		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				var json = JSON.parse(this.responseText);

				var result =
					"Availability Rate By Date : " + json.analytics.AvailabilityRateByDate + " %\n\n" +
					"Availability Rate By Date for x Rooms : " + json.analytics.AvailabilityRateByDateForXRooms + " %\n\n" +
					"Availability Rate By Month : " + json.analytics.AvailabilityRateByMonth + " %\n\n" +
					"Availability Rate By Month for x Rooms : " + json.analytics.AvailabilityRateByMonthForXRooms + " %\n\n" +
					"Average Price By Date : " + json.analytics.AveragePriceByDate + "\n\n" +
					"Average Price By Date for x Rooms : " + json.analytics.AveragePriceByDateForXRooms + "\n\n" +
					"Average Price By Month : " + json.analytics.AveragePriceByMonth + "\n\n" +
					"Average Price By Month for x Rooms : " + json.analytics.AveragePriceByMonthForXRooms + "\n\n" +
					"Occupancy Rate By Date : " + json.analytics.OccupancyRateByDate + " %\n\n" +
					"Occupancy Rate By Date for x Rooms : " + json.analytics.OccupancyRateByDateForXRooms + " %\n\n" +
					"Occupancy Rate By Month: " + json.analytics.OccupancyRateByMonth + " %\n\n" +
					"Occupancy Rate By Month for x Rooms : " + json.analytics.OccupancyRateByMonthForXRooms + " %\n\n" +
					"Property Count By Date : " + json.analytics.PropertyCountByDate + "\n\n" +
					"Property Count By Date for x Rooms : " + json.analytics.PropertyCountByDateForXRooms + "\n\n" +
					"Property Count By Rooms : " + json.analytics.PropertyCountByRooms;

				toastr.info(result, "Price Analysis for " + propertyName + " - " + moment(date).format('MMMM DD,YYYY'));
			}
		};

		var url = "http://149.28.59.242/api/airbnb/getanalytics?" +
			"latitude=" + latitude + "&longitude=" + longitude + "&date=" + date + "&propertycountgetavailable=true" +
			"&averagepriceonlyavailable=false&rooms=" + bedrooms;

		xhttp.open("GET", url, true);
		xhttp.send();
		toastr.info("Calculating analytics...", "");
	}
});

$(document).on("mousedown", ".edit-set-price", function () {
	previousElement = $(this);
	$(this).attr("style", $(this).attr("style").replace("background-color: rgb(255, 255, 255);", "background-color: #99ffff;"));
	$(this).attr("highlighted", true);
	document.getElementById("hidden-siteType").value = $(this).attr("data-siteType");
	document.getElementById("hidden-hostId").value = $(this).attr("data-hostId");
	document.getElementById("hidden-vrboResId").value = $(this).attr("data-vrboResId");
	document.getElementById("hidden-roomId").value = $(this).attr("data-roomId");
	price = $(this).attr("data-price");
	dateFrom = $(this).attr("data-date");
	dragging = true;
});

$(document).on("mouseup", ".edit-set-price", function () {
	document.getElementById("hidden-propertyId").value = $(this).attr("data-property-id");
	document.getElementById("hidden-isAvailable").value = $(this).attr("data-isAvailable") == "true" ? "1" : "0";
	document.getElementById("hidden-siteType").value = $(this).attr("data-siteType");
	document.getElementById("hidden-hostId").value = $(this).attr("data-hostId");
	document.getElementById("hidden-vrboResId").value = $(this).attr("data-vrboResId");
	document.getElementById("hidden-roomId").value = $(this).attr("data-roomId");
	$("#set-price-propertyName").text("Set Price for Property \"" + $(this).attr("data-property-name") + "\"");
	document.getElementById("SetPriceTb").value = $(this).attr("data-price");
	$(".js-btn_block-date").text(document.getElementById("hidden-isAvailable").value == "1" ? "Block Date" : "Unblock Date");
	price = $(this).attr("data-price");
	dateTo = $(this).attr("data-date");

	$('[highlighted="true"]').each(function () {
		if ($(this).attr("data-property-id") != document.getElementById("hidden-propertyId").value) {
			$(this).attr("style", $(this).attr("style").replace("background-color: #99ffff;", "background-color: rgb(255, 255, 255);"));
			$(this).attr("highlighted", false);
		}
	});

	dragging = false;
});

$(document).on("mouseup", function () {
	dragging = false;
});


$(document).on("click", ".fc-timelineDay-button", function () {
	$('[highlighted="true"]').hide();
	$('[highlighted="false"]').hide();
	document.getElementById("calendar-hidden-isMonthView").value = false;
});

$(document).on("click", ".fc-timelineWeek-button", function () {
	$('[highlighted="true"]').show();
	$('[highlighted="false"]').show();
	document.getElementById("calendar-hidden-isMonthView").value = false;
});

$(document).on("click", ".fc-month-button", function () {
	$('[highlighted="true"]').hide();
	$('[highlighted="false"]').hide();
	document.getElementById("calendar-hidden-isMonthView").value = true;
});

function getRefreshResourceUrl() {
	return $('#calendar').attr('data-get-resources-url') + '?propertyId=1';
}

function setPrice() {
	var sync = $("#calendar-sync-changes").is(':checked') ? true : false;
	var propertyId = document.getElementById("hidden-propertyId").value;
	var from = dateFrom;
	var to = dateTo;
	var setPrice = $("#SetPriceTb").val();
	var isAvailable = (document.getElementById("hidden-isAvailable").value == "1" ? "true" : "false");
	var siteType = document.getElementById("hidden-siteType").value;
	var hostId = document.getElementById("hidden-hostId").value;
	var roomId = siteType == 3 ? document.getElementById("hidden-roomId").value : null;
	debugger;

	$("#set-price-done").addClass("loading");

	$.ajax({
		type: "POST",
		url: "/Calendar/SetPrice",
		data: {
			propertyId: propertyId,
			from: dateFrom,
			to: dateTo,
			price: setPrice,
			isAvailable: isAvailable,
			siteType: siteType,
			syncChangesToOtherChild: sync,
			hostId: hostId,
			roomId: roomId
		},
		dataType: "json",
		success: function (result) {
			$("#set-price-done").removeClass("loading");
			$("#set-price-modal").modal("hide");

			if (result.success) {
				swal({ title: "Success", text: result.message, type: "success" });
				$('#calendar').fullCalendar('refetchEvents');

			}
			else {
				swal({ title: "Failed", text: result.message, type: "error" });
			}
		}
	});
}

function blockDate() {
	$(".js-btn_block-date").addClass("loading");

	if (new Date(dateFrom) > new Date(dateTo)) {
		var temp = dateTo;
		dateTo = dateFrom;
		dateFrom = temp;
	}

	var sync = $("#calendar-sync-changes").is(':checked') ? true : false;

	var propertyId = document.getElementById("hidden-propertyId").value;
	var from = dateFrom;
	var to = dateTo;
	var setPrice = price;
	var isAvailable = (document.getElementById("hidden-isAvailable").value == "1" ? "false" : "true");
	var siteType = document.getElementById("hidden-siteType").value;
	var hostId = document.getElementById("hidden-hostId").value;
	var vrboResId = document.getElementById("hidden-vrboResId").value;
	var roomId = siteType == 3 ? document.getElementById("hidden-roomId").value : null;
	debugger;

	$.ajax({
		type: "POST",
		url: "/Calendar/BlockDate",
		data: {
			propertyId: propertyId,
			from: dateFrom,
			to: dateTo,
			price: setPrice,
			notBlocked: isAvailable,
			siteType: siteType,
			syncChangesToOtherChild: sync,
			resId: vrboResId,
			hostId: hostId,
			roomId: roomId
		},
		dataType: "json",
		//"propertyId=" + document.getElementById("hidden-propertyId").value +
		//"&from=" + dateFrom +
		//"&to=" + dateTo +
		//"&price=" + price +
		//"&isAvailable=" + (document.getElementById("hidden-isAvailable").value == "1" ? "true" : "false") + 
		//"&syncChanges=" + sync,
		success: function (result) {
			$(".js-btn_block-date").removeClass("loading");
			debugger;
			if (result.success) {

				swal({ title: "Success", text: result.message, type: "success" });
				$('#calendar').fullCalendar('refetchEvents');

			}
			else {
				swal({ title: "Failed", text: result.message, type: "error" });
			}
		}
	});
}

function updateStandardRate() {
	$("#update-standard-rate").addClass("loading");

	$.ajax({
		type: "POST",
		url: "/Calendar/UpdateStandardRate",
		data: {
			rate: document.getElementById("standard-rate").value
		},
		dataType: "json",
		success: function (successful) {
			$("#update-standard-rate").removeClass("loading");

			if (!successful) {
				document.getElementById("standard-rate").value = 0;
			}
		}
	});
}

function toggleDateBlocking() {
	var isAvailable = document.getElementById("hidden-isAvailable").value;

	if (isAvailable == "1") {
		document.getElementById("hidden-isAvailable").value = "0";
		document.getElementById("set-price-block-date").innerText = "Unblock Date"
	}
	else {
		document.getElementById("hidden-isAvailable").value = "1";
		document.getElementById("set-price-block-date").innerText = "Block Date";
	}
}

function showBookingToggled(e) {
	//if (!e.checked) {
	//    document.getElementById("calendar-hidden-booking").value = "-1";
	//}
	//else {
	//    document.getElementById("calendar-hidden-booking").value = "0";
	//}
	$('#calendar').fullCalendar('refetchResources');
	$('#calendar').fullCalendar('refetchEvents');
	$('#calendar').fullCalendar('getDate');

}

function showWorkerToggled(e) {
	//if (!e.checked) {
	//    document.getElementById("calendar-hidden-worker").value = "-1";
	//}
	//else {
	//    document.getElementById("calendar-hidden-worker").value = 0;
	//}
	//$('#calendar').fullCalendar('refetchEvents');

	$('#calendar').fullCalendar('refetchResources');
	$('#calendar').fullCalendar('refetchEvents');
	$('#calendar').fullCalendar('getDate');
}

function showTravelEventsToggled(e) {
	//if (!e.checked) {
	//    document.getElementById("calendar-hidden-worker").value = "-1";
	//}
	//else {
	//    document.getElementById("calendar-hidden-worker").value = 0;
	//}
	//$('#calendar').fullCalendar('refetchEvents');

	$('#calendar').fullCalendar('refetchResources');
	$('#calendar').fullCalendar('refetchEvents');
	$('#calendar').fullCalendar('getDate');
}
function showPricerToggled(e) {
	//if (!e.checked) {
	//    document.getElementById("calendar-hidden-worker").value = "-1";
	//}
	//else {
	//    document.getElementById("calendar-hidden-worker").value = 0;
	//}
	//$('#calendar').fullCalendar('refetchEvents');

	$('#calendar').fullCalendar('refetchResources');
	$('#calendar').fullCalendar('refetchEvents');
	$('#calendar').fullCalendar('getDate');
}

$(".js-btn_sample-fetch").click(function () {
	$(this).addClass("loading");
	$.ajax({
		type: "POST",
		url: "/Calendar/FetchCalendarFromAirbnb",
		success: function (successful) {
			$(this).removeClass("loading");

			alert(successful);
			$('#calendar').fullCalendar('refetchEvents');
		}
	});
});

function siteFilterChangeExport() {
	$('.roomDiv').hide();

	$('#SiteType').on('change', function () {
		site = $(this).val();
		if ($(this).val() == 3) {
			$('.roomDiv').show();
		}
		else {
			$('.roomDiv').hide();
		}

		$.ajax({
			type: "POST",
			url: "/Calendar/ChooseProperty",
			data: {
				siteType: $(this).val(),

			},

			success: function (data) {
				console.log(data);
				var vhtml = "<option value='99'>Select Property</option>";
				var array = data.data;
				array.forEach(function (key, i) {
					vhtml += "<option value='" + array[i].ListingId + "' name='" + array[i].ListingId + "'>" + array[i].Name + "</option>";
				});

				$("#ExportCalendarPropertyId").html(vhtml);


			},
			error: function (errormessage) {
				console.log(errormessage.responseText);
			}
		});
	});

}

function siteFilterChangeImport() {

	$('#SiteType2').on('change', function () {
		site2 = $(this).val();


		$.ajax({
			type: "POST",
			url: "/Calendar/ChooseProperty",
			data: {
				siteType: $(this).val(),

			},

			success: function (data) {
				console.log(data);
				var vhtml = "<option value='99'>Select Property</option>";
				var array = data.data;
				array.forEach(function (key, i) {
					vhtml += "<option value='" + array[i].ListingId + "' name='" + array[i].ListingId + "'>" + array[i].Name + "</option>";
				});

				$("#ImportCalendarPropertyId").html(vhtml);


			},
			error: function (errormessage) {
				console.log(errormessage.responseText);
			}
		});
	});

}

function propertyFilterChangeExport() {



	$('#ExportCalendarPropertyId').on('change', function () {



		$.ajax({
			type: "POST",
			url: "/Calendar/ChooseRoom",
			data: {
				property: $(this).val(),

			},

			success: function (data) {
				console.log(data);
				var vhtml = "<option value='0'>Select Room</option>";
				var array = data.data;
				array.forEach(function (key, i) {
					vhtml += "<option value='" + array[i].RoomId + "' name='" + array[i].RoomId + "'>" + array[i].RoomName + "</option>";
				});

				$("#ExportCalendarRoomId").html(vhtml);


			},
			error: function (errormessage) {
				console.log(errormessage.responseText);
			}
		});
	});

}