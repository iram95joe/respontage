﻿$(document).ready(function () {
    $('#js-btn_sidebar').on('click', function () {
        $('.ui.sidebar').sidebar('toggle');
    });
    $('#update-batch-price').hide();
    var excludeComparableTable;
    var comparableTable;
    var bedroomsJson = null;
    var comparablesJson = null;
    var excludedComparablesJson = null;
    var unfilteredComparablesJson = null;
    var selectedSiteType = null;
    var selectedDate = null;
    var selectedListingId = null;
    var selectedBedroom = null;
    var selectedHostId = null;
    var milesFilter = 20;
	var tableAnalytics;
	LoadDateAnalytics();
    ShowAnalytics();
    ShowComparable();
    filterByMiles();
    filterByBedroom();
    IncludeBooked();
    IncludeComparableBySiteType();
    ChangePropertyType();
    filterByDate();
    includeComparable();
    excludeComparable();
    SelectAllPricerRow();
    RequestBatchAnalytics();
    UpdatePricePerDate();
    UpdateBatchPrice();
	function LoadDateAnalytics() {
		tableAnalytics = $('#booking-date-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Analytics/GetProperyDate",
				"type": 'GET',
				"data": function () {
					return {
						date: $('#date-filter').val(),
						listingId: $('#listingId').val()
					};
				}
			},
            "columns": [
                { "data":"Checkbox"},
				{ "data": "Date" },
				{ "data": "Price" },
                { "data": "Availability" },
                { "data": "RulesApplied" },
                { "data": "SuggestedAmount" },
				{ "data": "Actions"}
			],
			"createdRow": function (row, data, rowIndex) {
                $(row).attr('date-id', data.Id);
                if (data.IsAvailable == false) {
                    $(row).addClass('active');
                }
			}
		});
	}

	function ShowAnalytics() {
        $(document).on('click', '.show-analytics', function () {
            var thisbtn = $(this);
            thisbtn.addClass('loading');
            $('.show-analytics').attr('disabled', true);
            var id = $(this).parents('tr').attr('date-id');
           var site = "";
			$.ajax({
				type: 'GET',
                url: "/Analytics/ShowAnalytics",
                data: {
                    dateId: id,
                    includeSiteByOnly: $('#sitetype-only').find('input').is(':checked'),
                    bedroom: $('#analytics-bedroom-filter').val(),
                    siteType: $('#siteType').val()
                },
                dataType: 'json',
                success: function (data) {
                    thisbtn.removeClass('loading');
                    $('.show-analytics').attr('disabled', false);
                    if (data.success) {
                        var analytics = data.analytics;
                        var property = data.property;
                        var date = moment(data.Date).format('MMMM D,YYYY');
                        var month = moment(data.Date).format('MMMM YYYY');
                        var bedroom = $('#analytics-bedroom-filter').val();
                        switch (data.site) {
                            case 1:
                                site = "Airbnb";
                                break;

                            case 2:
                                site = "Vrbo";
                                break;

                            case 3:
                                site = "Booking.com";
                                break;

                            case 4:
                                site = "Homeaway";
                                break;
                        }
                        var result =
                            "<h4>" +
                            "<div class='ui grid'>" +
                            "<div class='eight wide column'>" +
                            "<table class='ui celled striped table' style='width: 100%; background: transparent; border-color: white;'>" +
                            "<tbody style='color: white;'>" +
                            "<tr>" +
                            "<td>" +
                            "Occupancy Rate " + date +
                            "</td>" +
                            "<td>" +
                            analytics.OccupancyRateByDate + " %" +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Occupancy Rate " + date + " for " +bedroom + " Rooms" +
                            "</td>" +
                            "<td>" +
                            analytics.OccupancyRateByDateForXRooms + " %" +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Occupancy Rate " + month +
                            "</td>" +
                            "<td>" +
                            analytics.OccupancyRateByMonth + " %" +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Occupancy Rate " + month + " for " +bedroom + " Rooms" +
                            "</td>" +
                            "<td>" +
                            analytics.OccupancyRateByMonthForXRooms + " %" +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Average Price " + date +
                            "</td>" +
                            "<td>" +
                            analytics.AveragePriceByDate +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Average Price " + date + " for " +bedroom+ " Rooms" +
                            "</td>" +
                            "<td>" +
                            analytics.AveragePriceByDateForXRooms +
                            "</td>" +
                            "</tr>" +
                            "</tbody>" +
                            "</table>" +
                            "</div>" +
                            "<div class='eight wide column'>" +
                            "<table class='ui celled striped table' style='width: 100%; background: transparent; border-color: white;'>" +
                            "<tbody style='color: white;'>" +
                            "<tr>" +
                            "<td>" +
                            "Average Price " + month +
                            "</td>" +
                            "<td>" +
                            analytics.AveragePriceByMonth +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Average Price " + month + " for " +bedroom + " Rooms" +
                            "</td>" +
                            "<td>" +
                            analytics.AveragePriceByMonthForXRooms +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Active Property Count "+
                            "</td>" +
                            //"<td>" +
                            //"Available Property Count " + date +
                            //"</td>" +
                            "<td>" +
                            analytics.PropertyCountByDate +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Active Property Count for " + bedroom + " Rooms" +
                            "</td>" +
                            //"<td>" +
                            //"Available Property Count " + date + " for " +bedroom+ " Rooms" +
                            //"</td>" +
                            "<td>" +
                            analytics.PropertyCountByDateForXRooms +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Total Property Count"+
                            "</td>" +
                            //"<td>" +
                            //"Total Property Count " + date +
                            //"</td>" +
                            "<td>" +
                            analytics.TotalPropertyCountByDate +
                            "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>" +
                            "Total Property Count for " + bedroom + " Rooms" +
                            "</td>" +
                            //"<td>" +
                            //"Total Property Count " + date + " for " +bedroom + " Rooms" +
                            //"</td>" +
                            "<td>" +
                            analytics.TotalPropertyCountByDateForXRooms +
                            "</td>" +
                            "</tr>" +

                            "<tr>" +
                            "<td style='white-space: pre-line'>" +

                            "<div class='ui accordion'>" +
                                "<div class='title' style='color: white;'>" +
                                "<i class='dropdown icon'></i>Property Count By Room(s)" +
                                "</div>" +
                            "<div class='content'>" +

                            "<div class='ui items'>"+
                                  "<div class='item'>"+
                                    "<div class='content'>"+
                            "<div class='meta' style='display:flex;color: white;'>" +
                            "<div>" + analytics.NearPropertyCountByBedrooms +"</div>" +
                            "<div>" + analytics.PropertyCountByBedrooms +"</div>" +
                                                
                                                
                                  "</div>"+
                                "</div>"+
                              "</div>"+

                                "</div>" +
                                "</div>" +
                            //"</td>" +
                            //"<td>" +
                            "</td>" +
                            "</tr>" +

                            "</tbody>" +
                            "</table>" +
                            "</div>" +
                            "</div>" +
                            "</h4>"+
                            "<h3>" + data.ruleUsedAndComputations+"</h3>";
                        $('#analytics-modal-title').text("PROPERTIES ON " + site.toUpperCase() + " WITHIN 100 KM. \\ 62.5 MILES\n\n");
                        $('#analytics-modal-content').empty();
                        $('#analytics-modal-content').append(result);
                        $("#analytics-modal").modal('show').modal('refresh');
                        $("#analytics-modal").find('.ui.accordion').accordion();
					}
                }

			});

		});
	}
	function ShowComparable() {
        $(document).on('click', '.show-comparable', function () {
            var thisbtn = $(this);
            thisbtn.addClass('loading');
            $('.show-comparable').attr('disabled', true);
            //Show comparable properties on specific date
            var id = $(this).parents('tr').attr('date-id');
            var site = "";
            $.ajax({
                type: 'GET',
                url: "/Analytics/ShowComparables",
                data: {
                    dateId: id,
                    includeSiteByOnly: $('.comparable-sitetype-only').find('input').is(':checked'),
                    siteType: $('#siteType').val(),
                },
                dataType: 'json',
                success: function (data) {
                    thisbtn.removeClass('loading');
                    $('.show-comparable').attr('disabled', false);
                    if (data.success) {
                        var json = data.comparables;
                        var property = data.property;
                        var date = data.Date;
                        bedroomsJson = json.Bedrooms;
                        comparablesJson = json.Comparables;
                        excludedComparablesJson = json.ExcludedComparables;
                        unfilteredComparablesJson = json.UnfilteredComparables;
                        selectedDate = moment(date).format("MMMM D,YYYY");//e.getAttribute("data-date");
                        selectedListingId = property.ListingId;//e.getAttribute("data-listing-id");
                        selectedSiteType = property.SiteType;//parseInt(e.getAttribute("data-site-type"));
                        selectedBedroom = property.Bedrooms;//parseInt(e.getAttribute("data-bedroom"));
                        selectedHostId = property.HostId;//parseInt(e.getAttribute("data-host-id"));
                        milesFilter = 20;
                        var site = "";

                        switch (selectedSiteType) {
                            case 1:
                                site = "Airbnb";
                                break;

                            case 2:
                                site = "Vrbo";
                                break;

                            case 3:
                                site = "Booking.com";
                                break;

                            case 4:
                                site = "Homeaway";
                                break;
                            default:
                                site = "Local";
                        }

                        var bedroomList = "";

                        for (var i = 1; i <= 20; i++) {
                            bedroomList = bedroomList +
                                '<option value="' + i + '" ' + (i == selectedBedroom?"selected":"") + '>' + i+ ' bedroom' +'</option>';
                        }
                        var milesOption = '';
                        for (var x = 1; x <= 40; x++) {
                            milesOption +='<option '+(x==40?'selected':'')+' value="'+x+'">' + x + ' mile away</option>'
                        }
                        var html =
                            '<i class="close icon"></i>' +
                            '<div class="header">' +
                            'Comparables' +
                            '</div>' +
                            '<div class="content">' +
                            '<div class="ui top attached tabular menu">' +
                            '<div class="item active" data-tab="tab-comparables">Available Comparables on ' + site + ' for ' + selectedDate + '</div>' +
                            '<div class="item" data-tab="tab-excluded-comparables">Excluded Comparables</div>' +
                            '</div>' +
                            '<div class="ui tab active" data-tab="tab-comparables">' +
                            '<div class="header" style="margin-top: 20px;">' +
                            '<div class="ui form">' +
                            '<div class="fields">' +
                            '<div class="field"  style="margin-top: 12px;">' +
                            '</div>' +
                            '<div id="comparables-average-price" class="two wide column"  style="margin-top: 12px;">' +
                            '</div>' +
                            '<div class="field">' +
                            '<label>Less Than  </label>' +
                            '<select id="miles-filter" class="ui dropdown comparables-dropdown">' +
                            '<option value="1">1 mile away</option>' +
                            milesOption+
                            '</select>' +
                            '</div>' +
                            '<div class="field">' +
                            '<label>Room Count  </label>' +
                            '<select id="bedroom-filter" class="ui dropdown comparables-dropdown">' +
                            bedroomList +
                            '</select>' +
                            '</div>' +
                            '<div class="field">' +
                            '<label>Type</label>' +
                            '<select id="property-type-filter" class="ui dropdown comparables-dropdown">' +
                            '<option value="false">Entire House</option>' +
                            '<option value="true">Private Room</option>' +
                            '</select>' +
                            '</div>' +
                            '<div class="field">' +
                            '<br>'+
                            '<div class="ui toggle checkbox include-booked"><input type="checkbox" name="include-booked"><label>Include booked</label></div>' +
                            '</div>' +
                            '<div class="field">' +
                            '<br>' +
                            '<div class="ui toggle checkbox comparable-sitetype-only"><input type="checkbox" name="comparable-sitetype-only"><label id="comparable-sitetype-only-label">' + site + ' Only</label></div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div id="comparables-content">' +
                            '</div>' +
                            '</div>' +

                            '<div class="ui tab" data-tab="tab-excluded-comparables">' +
                            '<div id="excluded-comparables-content" style="margin-top: 20px;">' +
                            'TESTING...' +
                            '</div>' +
                            '</div>'
                        '</div>';

                        $("#comparables-modal").html(html);
                        $(".comparables-dropdown").dropdown();
                        
                        showComparableFilter($('.comparable-sitetype-only').find('input').is(':checked'), selectedBedroom);

                        $("#comparables-modal").modal('show');
                        $('.menu .item').tab();
                    }
                }

            });
        });
    }

    function showComparableFilter(showPropertySiteTypeOnly, bedroom) {
        var html = "" +
            '<table id="comparable-table" class="ui fixed celled table" style="margin-top: 10px;">' +
            '<thead>' +
            '<tr>' +
            '<th>Photo</th>' +
            '<th>Name</th>' +
            '<th>Url</th>' +
            '<th>Distance</th>' +
            '<th id="price-th">Price</th>' +
            '<th>Fees</th>' +
            '<th>Bedrooms</th>' +
            '<th>Bathrooms</th>' +
            //'<th>Size</th>' +
            '<th>Site</th>' +
            '<th>Status</th>' +
            '<th>Exclude</th>' +
            '<th>Occupancy Rates</th>' +
            '<th>Ratings</th>' +
            '<th>Type</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';

        var comparables = comparablesJson;
        var siteType = 0;
        for (var i = 0; i < comparables.length; i++) {
            switch (comparables[i].Site) {
                case "Airbnb":
                    siteType = 1;
                    break;

                case "Vrbo":
                    siteType = 2;
                    break;

                case "Booking.com":
                    siteType = 3;
                    break;

                case "Homeaway":
                    siteType = 4;
                    break;
            }
            //Include All Property
                html = html +
                    '<tr class="' + (comparables[i].IsAvailable ? "" : "active") + '">' +
                    '<td><img style="width: 70px; height: 60px; object-fit: cover;" src="' + (comparables[i].ListingImages != null ? comparables[i].ListingImages[0] : "/Content/img/default-avatar/P.png") + '" /></td>' +
                    '<td>' + comparables[i].ListingName + '</td>' +
                    '<td><u><a href="' + comparables[i].ListingUrl + '" style="color: gray;">' + comparables[i].ListingUrl + '</a></u></td>' +
                    '<td>' + comparables[i].Distance + '</td>' +
                    '<td>' + (comparables[i].Price == 0 ? 'Not Available' : (comparables[i].IsAvailable ? "" : 'Price Booked <br/>') + comparables[i].Price + ' $') + '<br/><h6>(' + comparables[i].DateUpdated +')</h6></td>' +
                    '<td>' + (comparables[i].Fees == null || comparables[i].Fees =="" ? "Not Available" : comparables[i].Fees) + '</td>' +
                    '<td>' + comparables[i].Bedrooms + '</td>' +
                    '<td>' + comparables[i].Bathrooms + '</td>' +
                    '<td>' + comparables[i].Site + '</td>' +
                    '<td>' + (comparables[i].IsAvailable ? "Available" : "Booked") + '</td>' +
                    '<td>' +
                    '<center><p><u><a style="cursor: pointer; color: gray;" class="exclude-comparable" listingId="' + comparables[i].ListingId + '">Exclude</a></u>  <i class="arrow alternate circle right icon"></i></p></center>' +
                    '</td>' +
                    '<td>' + comparables[i].AverageOccupancies + '</td>' +
                    '<td>' + (comparables[i].Ratings != "" && comparables[i].Ratings != null ? comparables[i].Ratings : "Not Available") + '</td>' +
                    '<td>' + (comparables[i].IsPrivateRoom?"Private Room":"EntireHouse") + '</td>' +
                    '</tr>';
        }
        html = html + '</tbody>' +
            '</table>';
        $("#comparables-content").html(html);

        var html = "" +
            '<table id="exclude-comparable-table" class="ui fixed celled striped table" style="margin-top: 10px;">' +
            '<thead>' +
            '<tr>' +
            '<th></th>' +
            '<th>Name</th>' +
            '<th>Url</th>' +
            '<th>Distance</th>' +
            '<th>Price</th>' +
            '<th>Fees</th>' +
            '<th>Bedrooms</th>' +
            '<th>Bathrooms</th>' +
            '<th>Site</th>' +
            '<th>Include</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';

        var excludedComparables = excludedComparablesJson;
        comparables = unfilteredComparablesJson;
        for (var x = 0; x < excludedComparables.length; x++) {
            for (var i = 0; i < comparables.length; i++) {
                if (excludedComparables[x].ListingId == comparables[i].ListingId) {
                    switch (comparables[i].Site) {
                        case "Airbnb":
                            siteType = 1;
                            break;

                        case "Vrbo":
                            siteType = 2;
                            break;

                        case "Booking.com":
                            siteType = 3;
                            break;

                        case "Homeaway":
                            siteType = 4;
                            break;
                    }

                    html = html +
                        '<tr>' +
                        '<td><img style="width: 70px; height: 60px; object-fit: cover;" src="' + (comparables[i].ListingImages != null ? comparables[i].ListingImages[0] : "/Content/img/default-avatar/P.png") + '" /></td>' +
                        '<td>' + comparables[i].ListingName + '</td>' +
                        '<td><u><a href="' + comparables[i].ListingUrl + '" style="color: gray;">' + comparables[i].ListingUrl + '</a></u></td>' +
                        '<td>' + comparables[i].Distance + '</td>' +
                        '<td>' + comparables[i].Price + ' $</td>' +
                        //'<td class="top aligned">Cleaning fee: 50 $<br />Misc fee: 100 $</td>' +
                        '<td class="top aligned">' + (comparables[i].Fees == null || comparables[i].Fees == "" ? "Not Available" : comparables[i].Fees) + '</td>' +
                        '<td>' + comparables[i].Bedrooms + '</td>' +
                        '<td>' + comparables[i].Bathrooms + '</td>' +
                        //'<td>120 sq.ft.</td>' +
                        '<td>' + comparables[i].Site + '</td>' +
                        '<td>' +
                        '<center><p><u><a style="cursor: pointer; color: gray;" class="include-comparable" listingId="' + comparables[i].ListingId + '">Include</a></u>  <i class="arrow alternate circle right icon"></i></p></center>' +
                        '</td>' +
                        '</tr>';
                }
            }
        }

        html = html + '</tbody>' +
            '</table>';

        $("#excluded-comparables-content").html(html);
        comparableTable=$('#comparable-table').DataTable();
        excludeComparableTable = $('#exclude-comparable-table').DataTable();
        $('.include-booked').change();
    }
    function IncludeComparableBySiteType() {
        $(document).on('change', '.comparable-sitetype-only', function () {
            selectedBedroom= $('#bedroom-filter').val();
            var miles = $('#miles-filter').val();
            milesFilter = parseInt(miles);
            filterComparable(selectedBedroom);
        });
    }
    function ChangePropertyType() {
        $(document).on('change', '#property-type-filter', function () {
            selectedBedroom = $('#bedroom-filter').val();
            var miles = $('#miles-filter').val();
            milesFilter = parseInt(miles);
            filterComparable(selectedBedroom);
        });
    }
    function IncludeBooked() {
        $(document).on('change', '.include-booked', function () {
            var bedroom = $('#bedroom-filter').val();
            var miles = $('#miles-filter').val();
            var includeBooked = $(this).find('input').is(':checked');
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    return (
                        (includeBooked ? true: data[9]=="Available") && parseInt(data[6]) == parseInt(bedroom) && parseFloat(data[3].replace(' miles away', '').replace(' mile away', '')) <= parseFloat(miles)
                            ? true
                            : false);
                });
            comparableTable.draw();
            $.fn.dataTable.ext.search.pop();
            //This will compute average price of property by filter
            var comparables = comparablesJson;
            var average = 0;
            var count = 0;
            for (var i = 0; i < comparables.length; i++) {

                if ((includeBooked ? true : comparables[i].IsAvailable) && parseInt(bedroom) == parseInt(comparables[i].Bedrooms) && parseFloat(comparables[i].Distance.replace(' miles away', '').replace(' mile away', '')) <= parseFloat(miles)) {
                    if (comparables[i].Price != 0) {
                        average = average + comparables[i].Price;
                        count++;
                    }
                }
            }
            $("#comparables-average-price").html("Average Price : " + (average / count).toFixed(2));
            $("#comparables-modal").modal('refresh');
        });
    }
    //filter by miles of near property
    function filterByMiles() {
        $(document).on('change', '#miles-filter', function () {
            var miles = $(this).val();
            milesFilter = parseInt(miles);
            filterComparable(selectedBedroom);
            $("#comparables-modal").modal('refresh');
        });
    }
    //Filter by bedroom of property
    function filterByBedroom() {
        $(document).on('change', '#bedroom-filter', function () {
            selectedBedroom = $(this).val();
            var miles = $('#miles-filter').val();
            milesFilter = parseInt(miles);
            filterComparable(selectedBedroom);
            $("#comparables-modal").modal('refresh');
        });
    }

    //filter by miles of near property
    //function filterByMiles() {
    //    $(document).on('change', '#miles-filter', function () {
    //        var bedroom = $('#bedroom-filter').val();
    //        var miles = $(this).val();
    //        var includeBooked = $('.include-booked').find('input').is(':checked');
    //        $.fn.dataTable.ext.search.push(
    //            function (settings, data, dataIndex) {
    //                return (
    //                    (includeBooked ? true : data[10] == "Available") && parseInt(data[6]) == parseInt(bedroom) && parseFloat(data[3].replace(' miles away', '').replace(' mile away', '')) <= parseFloat(miles)
    //                        ? true
    //                        : false);
    //            });
    //        comparableTable.draw();
    //        $.fn.dataTable.ext.search.pop();
    //        //This will compute average price of property by filter
    //        var comparables = comparablesJson;
    //        var average = 0;
    //        var count = 0;
    //        for (var i = 0; i < comparables.length; i++) {

    //            if ((includeBooked ? true : comparables[i].IsAvailable) && parseInt(bedroom) == parseInt(comparables[i].Bedrooms) && parseFloat(comparables[i].Distance.replace(' miles away', '').replace(' mile away', '')) <= parseFloat(miles)) {
    //                if (comparables[i].Price != 0) {
    //                    average = average + comparables[i].Price;
    //                    count++;
    //                }
    //            }
    //        }
    //        $("#comparables-average-price").html("Average Price : " + (average / count).toFixed(2) + ", Count : " + count);
        
    //    });
    //}
   
    //Filter by bedroom of property
    //function filterByBedroom() {
    //    $(document).on('change', '#bedroom-filter', function () {
    //        var bedroom = $(this).val();
    //        var miles = $('#miles-filter').val();
    //        var includeBooked = $('.include-booked').find('input').is(':checked');
    //        $.fn.dataTable.ext.search.push(
    //            function (settings, data, dataIndex) {
    //                return (
    //                    (includeBooked ? true : data[10] == "Available") &&parseInt(data[6]) == parseInt(bedroom) && parseFloat(data[3].replace(' miles away', '').replace(' mile away', '')) <= parseFloat(miles)
    //                    ? true
    //                    : false);
    //            });
    //        comparableTable.draw();
    //        $.fn.dataTable.ext.search.pop();
    //        var comparables = comparablesJson;
    //        //This will compute average price of property by filter
    //        var average = 0;
    //        var count = 0;
    //        for (var i = 0; i < comparables.length; i++) {

    //            if ((includeBooked ? true : comparables[i].IsAvailable) && parseInt(bedroom) == parseInt(comparables[i].Bedrooms) && parseFloat(comparables[i].Distance.replace(' miles away', '').replace(' mile away', '')) <= parseFloat(miles)) {
    //                if (comparables[i].Price != 0) {
    //                    average = average + comparables[i].Price;
    //                    count++;
    //                }
    //            }
    //        }
    //        $("#comparables-average-price").html("Average Price : " + (average / count).toFixed(2) + ", Count : " + count);
    //    });
    //}
    //Filter by calendar date
    function filterByDate() {
        $(document).on('change', '#date-filter', function () {
            tableAnalytics.ajax.reload(null, false);
        });
    }
    //function filterByAnalyticsBedroom() {
    //    $(document).on('change', '#bedroom-filter', function () {

    //    });
    //}
    //Include comparable 
    function includeComparable() {
        $(document).on('click', '.include-comparable', function () {
            var listingId = $(this).attr('listingId');
            $("#excluded-comparables-content").html(
                '<div class="ui active inverted dimmer">' +
                '<div class="ui massive text loader">Including comparable, please wait...</div>' +
                '</div>'
            );

            $.ajax({
                type: "GET",
                url: window.location.origin + "/test/property/comparable/include",
                data: {
                    hostId: selectedHostId,
                    listingId: listingId
                },
                success: function (result) {
                    if (result.success) {
                        filterComparable(selectedBedroom);
                    }
                },
                error: function (error) {
                    filterComparable(selectedBedroom);
                }
            });
        });
    }

    function excludeComparable() {
        $(document).on('click', '.exclude-comparable', function () {
            var listingId = $(this).attr('listingId');
            $("#comparables-content").html(
                '<div class="ui active inverted dimmer">' +
                '<div class="ui massive text loader">Excluding comparable, please wait...</div>' +
                '</div>'
            );
            $.ajax({
                type: "GET",
                url: window.location.origin + "/test/property/comparable/exclude",
                data: {
                    hostId: selectedHostId,
                    listingId: listingId
                },
                success: function (result) {
                    if (result.success) {
                        filterComparable(selectedBedroom);
                    }
                },
                error: function (error) {

                }
            });
        });
    }
    function filterComparable(bedroom) {
        $("#comparables-content").html(
            '<div class="ui active inverted dimmer">' +
            '<div class="ui massive text loader">Getting comparables...</div>' +
            '</div>'
        );
        $.ajax({
            type: "GET",
            url: window.location.origin + "/test/property/comparable",
            data: {
                listingId: selectedListingId,
                bedrooms: bedroom,
                date: selectedDate,
                miles: milesFilter,
                includeSiteByOnly: $('.comparable-sitetype-only').find('input').is(':checked'),
                siteType: $('#siteType').val(),
                isPrivateRoomOnly: $('#property-type-filter').val()

            },
            success: function (result) {
                bedroomsJson = result.comparables.Bedrooms;
                comparablesJson = result.comparables.Comparables;
                excludedComparablesJson = result.comparables.ExcludedComparables;
                unfilteredComparablesJson = result.comparables.UnfilteredComparables;
                var id = "#show-comparable-" + selectedDate.replace(/\s/g, "");
                $("#show-comparable-" + selectedDate.replace(/\s/g, "")).val(result.comparables);
                showComparableFilter(true, bedroom);
            },
            error: function (error) {
                filterComparable(selectedBedroom);
            }
        });
    }

    function SelectAllPricerRow() {
        $(document).on('change', '.select-all-date-row', function () {
            var check = $(this).find('input').is(':checked');
            if (check) {
                $('.request-price-check').prop("checked", true);
            }
            else {
                $('.request-price-check').prop("checked", false);
            }
        });
        $(document).on('change', '.select-available-date-row', function () {
            var check = $(this).find('input').is(':checked');
            if (check) {
                //$('#booking-date-table').find('tr').not('.active')
                $('.request-price-check').parents('tr').not('.active').find('.request-price-check').prop("checked", true);
            }
            else {
                $('.request-price-check').parents('tr').not('.active').find('.request-price-check').prop("checked", false);
            }
        });
    }
    function RequestBatchAnalytics() {
        $(document).on('click', '#request-batch-analytics', function () {
            $('#request-batch-analytics').addClass('disabled');
            $('#update-batch-price').hide();
            $('.request-price-check').each(function (index, element) {
             var row = $(this).parents('tr');
                if ($(this).prop("checked")) {
                    var id = row.attr('date-id');
                    row.addClass("disabled");
                    row.find("td:eq(5)").html('<div class="price-loader"></div><p>Calculating</p>');
                    $.ajax({
                        type: 'GET',
                        url: "/Analytics/GetAnalyticsPrice",
                        data: {
                            dateId: id,
                            includeSiteByOnly: $('#sitetype-only').find('input').is(':checked'),
                            bedroom: $('#analytics-bedroom-filter').val(),
                            siteType: $('#siteType').val()
                        },
                        dataType: 'json',
                        success: function (data) {
                            row.removeClass("disabled");
                            $('#request-batch-analytics').removeClass('disabled');
                            $('#update-batch-price').show();
                            if (data.finalPrice == 0 || data.finalPrice == "0") {
                                row.find("td:eq(4)").text("No Recommended Price");
                                row.find("td:eq(5)").text("No Recommended Price");
                            }
                            else {
                                var texboxHtml = '<div class="update-date-price"><div class="ui input"><input type="text" placeholder="Suggestion" value="' + data.finalPrice + '"></div><button class="ui button blue update-price">Update price</button></div>';
                                row.find("td:eq(4)").html(data.ruleUsedAndComputations);
                                row.find("td:eq(5)").html(texboxHtml);
                            }
                        }

                    });
                }
            });
        });

    }

    function UpdatePricePerDate() {
        $(document).on('click', '.update-price', function () {
            var amount = $(this).parent().find('input').val();
            var dateId = $(this).parents('tr').attr('date-id');
            if (amount === undefined || amount == "0" || isNaN(amount)) {
                swal("Invalid price!", "", "error");
            }
            else {
                $.ajax({
                    type: 'POST',
                    url: "/Analytics/UpdatePrice",
                    data: {
                        dateId: dateId,
                        amount: amount
                    },
                    dataType: 'json',
                    success: function (data) {

                    }

                });
            }
        });
    }
    function UpdateBatchPrice() {
        $(document).on('click', '#update-batch-price', function () {
            $('.request-price-check').each(function (index, element) {
                var row = $(this).parents('tr');

                if ($(this).prop("checked")) {
                    var dateId = row.attr('date-id');
                    var amount = parseFloat(row.find("td:eq(5)").find('input').val());
                    if (amount === undefined || amount == "0" || isNaN(amount)) {

                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            url: "/Analytics/UpdatePrice",
                            data: {
                                dateId: dateId,
                                amount: amount
                            },
                            dataType: 'json',
                            success: function (data) {

                            }

                        });
                    }
                }
            });
        });

    }
});