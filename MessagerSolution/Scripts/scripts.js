// initialization scripts

$(document).ready(function () {

    initialize();

    function initialize() {
        setupSemanticUI();
        setEventListeners();
        getSessionState();
    }

    function setupSemanticUI() {
        $('.ui.accordion').accordion();
        $('.ui.dropdown').dropdown({ on: 'hover', showOnFocus: false });
        $('.ui.checkbox').checkbox();
        $('.ui.calendar').calendar({ type: 'date' });
        $('.ui.calendar.month').calendar({ type: 'month' });
        $('.ui.calendar.time').calendar({ type: 'time' });
        $('table td a.table-tooltip').popup();
        $('.menu .item').tab();
        $('.display.responsive').addClass('cell-border');
    }

    function setEventListeners() {
        onDisplayContentSidebar();
        onDisplaySidebar();
        onHeaderSearch();
        getLogin();
        postLogin();
        onAddAccount();
		onForgotPassword();
		onForgotPasswordSubmit();
		onResetPasswordSubmit();

    }

    // ------------------
    // ------------------
    //   Event Listeners
    // ------------------
    // ------------------

    function getLogin() {
        $(document).on('click', '.userLogin', function (e) {
            $('.messager-detail-modal').modal('show');
        });
    }

    function postLogin() {
        $(document).on('click', '#SaveLoginInfo', function (e) {
            var flag = true;
            if ($("#UserName").val().trim() == "") {
                flag = false;
                toastr["error"]("please fill the username field.");
            }
            if ($("#Password").val().trim() == "") {
                flag = false;
                toastr["error"]("please fill the password field.");
            }
            if (validateForm($("#UserName").val().trim()) == false) {
                flag = false;
                toastr["error"]("please fill the valid email.");
            }

            if (flag == true) {
                $.ajax({
                    type: "POST",
                    url: "/Home/GetLogin/",
                    data: $("#form_GetLogin").serialize(),
                    beforeSend: function () {
                        $('#SaveLoginInfo').addClass('loading');
                    },
                    success: function (html) {
                        $('#SaveLoginInfo').removeClass('loading');
                        $('.messager-detail-modal').modal('hide');
                        if (html == "S1") {
                            toastr["success"]("You have login successfully.");
                            window.location.reload();
                        }
                        else if (html == "V1") {
                            toastr["warning"]("please click the link to validate your email id.");
                        }
                        else if (html == "A1") {
                            toastr["warning"]("sorry, login detail is not valid or your account has been blocked.");
                        }
                        else if (html == "E1") {
                            toastr["success"]("error is coming, please refresh the form.");
                        }
                    }
                });
            }
            return false;
        });
    }

	function onForgotPassword() {
		$('.forgot-password').on('click', function () {
			$('.messager-detail-modal').modal('hide');
			$('#forgot-password-modal').modal('show');
		});

	}

 //   function onRegisterUserSubmit() {
 //       $(document).on('click', '#SaveRegister', function (e) {
 //           var flag = true;
 //           if ($("#FirstNameRegister").val().trim() == "") {
 //               flag = false;
 //               toastr["error"]("please fill the firstname field.");
 //           }
 //           if ($("#LastNameRegister").val().trim() == "") {
 //               flag = false;
 //               toastr["error"]("please fill the lastname field.");
 //           }
 //           if ($("#UserNameRegister").val().trim() == "") {
 //               flag = false;
 //               toastr["error"]("please fill the email field.");
 //           }
 //           //if ($("#TimeZoneRegister").val().trim() == "") {
 //           //    flag = false;
 //           //    toastr["error"]("please select a timezone.");
 //           //}
 //           if ($("#PasswordRegister").val().trim() == "") {
 //               flag = false;
 //               toastr["error"]("please fill the password field.");
 //           }
 //           if ($("#ConfirmPasswordRegister").val().trim() != $("#PasswordRegister").val().trim()) {
 //               flag = false;
 //               toastr["error"]("Password and confirm password does not match.");
 //           }
 //           if ($("#CompanyType").val().trim() == "") {
 //               flag = false;
 //               toastr["error"]("please fill the subscription.");
 //           }
            
 //           if ($("#CompanyName").val().trim() == "") {
 //               flag = false;
 //               toastr["error"]("please fill the subscription.");
 //           }
	//		if (flag == true) {
	//			var formData = new FormData($("#register-user-form")[0]);
	//			formData.append("Image", null);
 //               $.ajax({
 //                   type: "POST",
 //                   url: "/Profile/GetRegister/",
	//				data: formData,
	//				contentType: false,
	//				processData: false,
 //                   success: function (html) {
 //                       if (html == "S1") {
 //                           $('#register-user-modal').modal('hide');
 //                           toastr["success"]("Congratulation, your account has been created, an email will be sent to your register account for email validation.");
 //                       }
 //                       else if (html == "A1") {
 //                           toastr["warning"]("user name is already existing.");
 //                       }
 //                       else if (html == "E1") {
 //                           toastr["error"]("error is coming, please refresh the form.");
 //                       }
 //                   }
 //               });
 //           }
 //       });
	//}

	function onForgotPasswordSubmit(){
		$(document).on('click', '#forgotBtn', function (e) {
			alert($('#Email').val());
			if ($('#Email').val().trim() != "") {
				$.ajax({
					type: "POST",
					url: "/Profile/ForgotPassword/",
					data: { Email: $('#Email').val() },
					success: function (data) {
						if (data == "S1") {
							$('#forgot-password-modal').modal('hide');
							toastr["success"]("Check your Email to reset your password");
						}
					}
				});
			}
		});
	}
	function onResetPasswordSubmit() {
		$(document).on('click', '#resetBtn', function (e) {
			if ($('newPassword').val() == $('#re-newPassword').val() && $('#email').val() !="")
			if ($('#Email').val().trim() != "") {
				$.ajax({
					type: "POST",
					url: "/Profile/ResetPassword/",
					data: { Email: $('#Email').val(), Password: $('newPassword').val() },
					success: function (data) {
						if (data == "S1") {
							toastr["success"]("Your password succesfully change!");
							window.location.href = '/Home/Index/';
						}
					}
				});
			}
		});
	}
    function getSessionState() {
        try {
            var state = $("meta[name=session_state]").attr('content');
            if (state) {
                toastr["error"]("Session has expired.");
            }
        }
        catch (Exception) { }
    }

    function validateForm(valEmail) {
        var x = valEmail;
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            return false;
        }
    }

    function onAddAccount() {
        $('.js-btn_add-new-account').on('click', function () {
            $('#import-form-fields').css('display', 'flex');
            $('#import-error-msg').css('display', 'none');
            $('#add-new-account-modal').modal('show');
        });
    }

    function onHeaderSearch() {
        $('#js-btn_menu-search').dropdown({
            action: 'nothing'
        });
    }

    function onDisplaySidebar() {
        $('#js-btn_sidebar').on('click', function () {
            $('#pages-sidebar').sidebar('toggle');
        });
    }
    function onDisplayContentSidebar() {
        $('.content-mobile-sidebar').sidebar({
            context: $('.ui.pushable'),
            transition: 'toggle'
        }).sidebar('attach events', '#mobile_item');
	}
});






