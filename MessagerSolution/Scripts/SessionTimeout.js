﻿$(document).ready(function () {
	GetUnRepliedThreadCount()
	checkInbox();
	InboxClick();
	SendMessage();
	onUploadImage();
	OnChangeMMS();
	EndCommunicationThread();
	CloseInboxModal();
	resizeWindows();
	CheckInboxLayout();
	DetailsClick();
	CallClick();
	AddContact();
	AddEmail();
	SendEmailClick();
	UploadEmailAttachment();
});
function UploadEmailAttachment() {
	$(document).on('click', '#communication-add-attachment', function (e) {
		$('#communication-file-pic').attr('accept', "");
		$('#communication-file-pic').click();
	});
}

function SendEmailClick() {
	$(document).on('click', '#communication-send-btn-email', function () {
		$('#communication-send-btn-email').attr('disabled', true);
		var threadId = $('#inbox-modal').attr('threadId');
		var formData = new FormData();
		for (var i = 0; i < $('#communication-file-pic').get(0).files.length; ++i) {
			formData.append("Attachments[" + i + "]", $('#communication-file-pic').get(0).files[i]);
		}
		var contact = $('#communication-contact-email').find('.checked');
		let email = $(contact).parents(".field").attr('data-email');
		formData.append("renterEmail", email);
		formData.append("threadId", threadId);
		formData.append("message", $('#communication-message-box').val());
		$.ajax({
			type: 'POST',
			url: "/Email/Send",
			contentType: false,
			cache: false,
			dataType: "json",
			processData: false,
			data: formData,
			success: function (data) {
				if (data.success) {
					$('#communication-send-btn-email').attr('disabled', false);
					$('#communication-message-box').val('');
					GetInboxMessage(threadId);
					$('#communication-file-upload').empty();
					$('#communication-file-pic').val('');
					toastr.success("Email sent!", null, { timeOut: 3000, positionClass: "toast-top-right" });
				} else {
					toastr.error("Failed sending email!", null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			},
			error: function (error) {
				toastr.error("Failed sending email!", null, { timeOut: 3000, positionClass: "toast-top-right" });
			}
		});
		$('#communication-send-btn-email').attr('disabled', false);
	});
}
function resizeWindows() {
	$(window).resize(function () {
		CheckInboxLayout();
	});
}
function CheckInboxLayout() {
	var panel = $('.message-panel');
	if ($(window).width() < 1000) {
		panel.removeClass('grid');
		panel.find('.column:nth-child(2)').css("display", "none");
		panel.find('.column:nth-child(3)').css("display", "none");
		//$('#inbox-modal').removeAttr('hidden');
	}
	else {
		panel.addClass('grid');
		panel.find('.column:nth-child(2)').css("display", "");
		panel.find('.column:nth-child(3)').css("display", "");
		$('#inbox-modal').attr('hidden', true);
	}
}
function checkInbox() {
	$.ajax({
		type: "GET",
		url: "/Home/CheckSession",
		success: function (data) {
			if (data.isTimeout) {
				window.location.href = data.Url;
			}
			setTimeout(function () { checkInbox(); }, 10000);
		}
	});
}
function GetUnRepliedThreadCount() {
	$.ajax({
		type: "GET",
		url: "/Messages/GetUnRepliedThreadCount",
		success: function (data) {
			$('#unreply-count').html(data.count);
			var inboxes = data.inboxes;
			var menu = $('.inbox-dropdown').find('.menu');
			menu.empty();
			for (var x = 0; x < inboxes.length; x++) {
				menu.append('<div class="item" data-value="' + inboxes[x].ThreadId + '">' +
					'<img class="ui avatar image" style="border:4px solid ' + (inboxes[x].Type == 1 ? "DeepPink" : inboxes[x].Type == 2 ? "LimeGreen" : inboxes[x].Type == 3 ? "MediumBlue" : "") + '" src="' + inboxes[x].Image + '">' +
					(inboxes[x].LastMessageAt == null ? '' : inboxes[x].IsReply == false ?
						`1<i class="envelope outline icon"></i>`
						: '') +
					inboxes[x].Name +
					'</div>');
			}
			$('.ui.dropdown').dropdown();

			setTimeout(function () { GetUnRepliedThreadCount(); }, 10000);
		}
	});
}
function ImageExist(url) {
	var imageExtentions = ['jpg', 'png', 'gif'];
	var extention = url.split('.').pop();
	if (jQuery.inArray(extention, imageExtentions) !== -1) {
		return true;
	}
	else {
		return false;
	}

}

function DetailsClick() {
	$(document).on('click', '#details', function (e) {
		$(this).popup({
			inline: true,
			hoverable: true,
			position: 'bottom right',
			lastResort: 'bottom right',
			delay: {
				show: 50,
				hide: 0
			},
			on: 'click'
		});
	});
}

function CallClick() {
	$(document).on('click', '.button.communication-call-button', function () {
		let phoneNumber = $(this).parents(".field").attr('data-phonenumber');
		var threadId = $('#inbox-modal').attr('threadId');
		$.ajax({
			type: 'GET',
			url: "/Communication/ThreadInfo",
			data: { threadId: threadId },
			dataType: "json",
			beforeSend: function () {
				$(this).addClass('loading');
			},
			success: function (data) {
				$(this).removeClass('loading');
				var params = {
					From: userId,
					To: phoneNumber
				};
				if (device) {
					$('.mini.modal .phone-number').text(phoneNumber);
					$('.mini.modal .header.name').text(data.Name);
					$('.mini.modal .avatar.image').attr('src', '/Images/Worker/default-image.png');
					$('.mini.modal .avatar.image').show();
					device.connect(params);
					openOutgoingCallModal();
				}
			}
		});
	});
}

function AddContact() {
	$(document).on('click', '#communication-add-contact-btn', function (e) {
		var threadId = $('#inbox-modal').attr('threadId');
		swal({
			title: "Add Contact Number",
			type: "input",
			confirmButtonColor: "##abdd54",
			confirmButtonText: "Yes",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true
		},
			function (message) {
				if (message) {
					$.ajax({
						type: 'POST',
						url: "/Communication/AddContact",
						data: { threadId: threadId, phoneNumber: message },
						success: function (data) {
							if (data.success) {
								swal("New contact has been added.", "", "success");
								$('#communication-contact-info').append(`
												<div class="field" data-phonenumber="${message}" data-type="1" >
													<div class="ui radio checkbox checked">
													<input type="radio" name="communication-contact" contact-id="${message}" checked>
													<label>${message}</label>
													</div>
													  <button class="circular tiny ui icon button communication-call-button" data-tooltip="Call">
														<i class="icon phone"></i>
													  </button>
												</div>`);

							}
							else {
								swal("Failed to add new contact", "", "error");
							}
						}
					});
				}
			});
	});

}
function AddEmail() {
	$(document).on('click', '#communication-add-email-btn', function (e) {
		var threadId = $('#inbox-modal').attr('threadId');
		swal({
			title: "Add Email Address",
			type: "input",
			confirmButtonColor: "##abdd54",
			confirmButtonText: "Yes",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true
		},
			function (message) {
				if (message) {
					$.ajax({
						type: 'POST',
						url: "/Communication/AddEmail",
						data: { threadId: threadId, email: message },
						success: function (data) {
							if (data.success) {
								swal("New email has been added.", "", "success");
								$('#communication-contact-email').append(`
								<div class="field" data-email="${email.Email}">
									<div class="ui radio checkbox checked">
									<input type="radio" name="communication-email" email-id="${message}" checked>
									<label>${messagel}</label>
									</div>
								</div>
							`);
							}
							else {
								swal("Failed to add new email", "", "error");
							}
						}
					});
				}
			});
	});

}
function GetInboxMessage(threadId) {
	$.ajax({
		type: 'GET',
		url: "/Messages/GetCommunicationThreadMessages",
		data: { threadId: threadId },
		dataType: "json",
		async: true,
		success: function (data) {
			if (data.HasEmail) {
				$('#email-actions').removeAttr('hidden');
			}
			else {
				$('#email-actions').attr('hidden', true);
			}
			var messages = '';
			/*					$("#inbox-modal").find('communication-dv_Conversation').empty();*/
			if (data.Messages.length != 0) {
				$.each(data.Messages, function (index, value) {
					var imageHtml = ''
					if (value.Images != null && value.Images != '') {
						imageHtml = '<div class="ui form"><div class="ui fields">';
						for (var x = 0; x < value.Images.length; x++) {
							imageHtml += "<div class='ui field'>" + (ImageExist(value.Images[x]) ? '<img class="zoom ui tiny image" src="' + value.Images[x] + '">' : '<a target="_blank" href="' + value.Images[x] + '"><i class="huge icon file alternate"></i></a>') + "</div>";
						}
						imageHtml += '</div></div>';
					}

					if (!value.IsMyMessage) {
						if (value.Type == 2) {
							messages += '<div style="width: 80%;" class="ui green small left pointing label message-label message-incoming">' +
								'<div class="ui mini circular image">' +
								'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
								'</div>' +
								(
									value.RecordingUrl == null ?
										'' :
										(
											'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
										)
								) +
								'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
								+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call From ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 1) {
							messages += '<div style="width: 80%;" class="ui small left pointing label message-label message-incoming">' +
								'<h5>' +
								'<div class="ui mini circular image">' +
								'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
								'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS From ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 3) {
							messages += '<div style="width: 80%;" class="ui teal small left pointing label message-label message-incoming">' +
								'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 4) {
							messages += '<div style="width: 80%;" class="ui blue small left pointing label message-label message-incoming">' +
								'<h5>' +
								'<div class="ui mini circular image">' +
								'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
								'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 5) {
							messages += '<div style="width: 80%;" class="ui pink small left pointing label message-label message-incoming">' +
								'<h5>' +
								'<div class="ui mini circular image">' +
								'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
								'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (From ' + value.Source + ')</p>' +
								'</div>';
						}
						else {
							messages += '<div style="width: 80%;" class="ui left pointing label message-label message-incoming">' +
								'<h5>' +
								'<div class="ui mini circular image">' +
								'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
								'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>';
						}
					}
					else {
						if (value.Type == 2) {
							messages += '<div style="width: 80%;" class="ui green small right pointing label message-label message-outgoing">' +
								(
									value.RecordingUrl == null ?
										'' :
										(
											'<button class="circular ui icon button recording" data-recording-url="' + value.RecordingUrl + '"><i class="file audio outline icon"></i></button>'
										)
								) +
								'<p><b>' + moment().startOf('d').add(value.Duration, 's').format("mm:ss") + '<b>, '
								+ moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (Call To ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 1) {
							messages += '<div style="width: 80%;" class="ui small right pointing label message-label message-outgoing">' +
								'<h5>' +
								'<div class="ui mini circular image">' +
								'<img src="' + value.ImageUrl + '" height="30" width="30" title="' + value.Name + '">' +
								'</div>' + (value.Message != null ? value.Message : "") + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (SMS To ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 3) {
							messages += '<div style="width: 80%;" class="ui teal small right pointing label message-label message-outgoing">' +
								'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>';
						} else if (value.Type == 4) {
							messages += '<div style="width: 80%;" class="ui blue small right pointing label message-label message-outgoing">' +
								'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>';
						}
						else if (value.Type == 5) {
							messages += '<div style="width: 80%;" class="ui blue pink right pointing label message-label message-outgoing">' +
								'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
								(value.Images != null ? imageHtml : '') +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + ' (To ' + value.Source + ')</p>' +
								'</div>';
						}
						else {
							'<div style="width: 80%;" class="ui right pointing label message-label message-outgoing">' +
								'<h5>' + (value.Message != null ? value.Message : "") + '</h5>' +
								'<p>' + moment(value.CreatedDate).format("h:mm A MMM D, YYYY") + '</p>' +
								'</div>';
						}
					}
				});

				$('#communication-contact-info').empty();
				console.log(data.contactInfos);
				$.each(data.contactInfos, function (index, contactInfo) {

					let buttons =
						`<button class="circular tiny ui icon button communication-call-button" data-tooltip="Call">
									<i class="icon phone"></i>
								</button>`;
					$('#communication-contact-info').append(`
								<div class="field" data-phonenumber="${contactInfo.Contact}" data-type="1" >
									<div class="ui radio checkbox ${(contactInfo.IsDefault ? "checked" : "")}">
									<input type="radio" name="communication-contact" contact-id="${contactInfo.Contact}" checked>
									<label>${contactInfo.Contact}</label>
									</div>
                                        ${buttons}
								</div>
							`);
				});
				$('#communication-contact-email').empty();
				$.each(data.emails, function (index, email) {
					$('#communication-contact-email').append(`
								<div class="field" data-email="${email.Email}">
									<div class="ui radio checkbox ${(email.IsDefault ? "checked" : "")}">
									<input type="radio" name="communication-email" email-id="${email.Email}" checked>
									<label>${email.Email}</label>
									</div>
								</div>
							`);
				});
			}
			else {
				messages = '<div class="ui one column stackable center aligned page grid"><div class="column sixteen wide" ><br/><br/><br/><h1 class="ui header"><i class="open envelope icon" style="color: #00b5ad !important;"></i></h1><h2>No Messages Yet</h2><h4 class=text-center>You haven`t receive any <br/><br />message</h4><br/><br/></div></div>';
				/*$("#inbox-modal").find('communication-dv_Conversation').append(html);*/
			}
			$("#inbox-modal").find('.communication-dv_Conversation').html(messages);
			$("#inbox-modal").find('.communication-dvConvo').scrollTop($("#inbox-modal").find('.communication-dvConvo')[0].scrollHeight);
		}
	});
}
function InboxClick() {
	$(document).on('click', '.inbox-dropdown > .menu >.item', function () {
		$('#inbox-modal').removeAttr('hidden');
		//$('.wrapper').css('bottom','85px');
		//$('.wrapper').css('pointer-events','auto');
		var threadId = $('.inbox-new-message').val();
		$('#inbox-modal').attr('threadId', threadId);
		GetInboxMessage(threadId);
		$('.inbox-dropdown').dropdown('clear');
		//$("#inbox-modal").modal("show");
	});
}
function CloseInboxModal() {
	$(document).on('click', '.hide-inbox', function (e) {
		$('.wrapper').attr('hidden', 'true');
	});
}
function onUploadImage() {
	$(document).on('click', '#communication-add-mms-btn', function (e) {
		$('#communication-file-pic').attr('accept', "image/*");
		$('#communication-file-pic').click();
	});
}
function OnChangeMMS() {
	$(document).on('change', '#communication-file-pic', function (e) {
		$('#communication-file-upload').html('');
		for (var i = 0; i < e.target.files.length; i++) {
			var imageUrl = window.URL.createObjectURL(this.files[i]);
			$('#communication-file-upload').append("<div class='ui field'>" + (this.files[i].type == 'image/jpeg' ? '<img class="zoom ui tiny image" src="' + imageUrl + '">' : '<a target="_blank" href="' + imageUrl + '"><i class="huge icon file alternate"></i></a>') + "</div>");
		}
	});
}
function SendMessage() {
	$(document).on('click', '#communication-send-message-btn', function () {
		$('#communication-send-message-btn').attr('disabled', true);
		$('#communication-send-message-btn').addClass('loading');
		var threadId = $('#inbox-modal').attr('threadId');
		var contact = $('#communication-contact-info').find('.checked');
		let phoneNumber = $(contact).parents(".field").attr('data-phonenumber');
		var formData = new FormData();
		formData.append("to", phoneNumber);
		for (var i = 0; i < $('#communication-file-pic').get(0).files.length; ++i) {
			formData.append("Images[" + i + "]", $('#communication-file-pic').get(0).files[i]);
		}
		formData.append("message", $('#communication-message-box').val());
		formData.append("threadId", threadId);
		$.ajax({
			type: 'POST',
			url: "/SMS/CommunicationSend",
			contentType: false,
			cache: false,
			dataType: "json",
			processData: false,
			data: formData,
			success: function (data) {
				$('#communication-send-message-btn').attr('disabled', false);
				$('#communication-send-message-btn').removeClass('loading');
				if (data.success) {
					$('#communication-message-box').val('');
					GetInboxMessage(threadId);
					$('#communication-file-upload').empty();
					$('#communication-file-pic').val('');
					toastr.success(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				} else {
					toastr.error(data.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
				}
			},
			error: function (error) {
				toastr.error(error.message, null, { timeOut: 3000, positionClass: "toast-top-right" });
			}
		});

	});
}

function EndCommunicationThread() {
	$(document).on('click', '#communication-end-message-btn', function () {
		var threadId = $('#inbox-modal').attr('threadId');
		$.ajax({
			type: 'POST',
			url: "/Messages/EndCommunicationThread",
			data: {
				threadId: threadId
			},
			success: function (data) {
				toastr.error("This thread mark as end!", null, { timeOut: 3000, positionClass: "toast-bottom-right" });
			}
		});
	});
}
