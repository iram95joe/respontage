﻿$(document).ready(function () {
	var tableVendor;
	LoadVendorTable();
	CreateVendor();
	AddContact();
	RemoveContact();
	AddEmail();
	RemoveEmail();
	SaveVendor();
	EditVendor();
	//Load all vendors
	function LoadVendorTable() {
		tableVendor = $('#vendor-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": "/Vendors/GetVendors",
			"columns": [
				{ "data": "Name" },
				{ "data": "Address" },
				{ "data": "ContactNumbers" },
				{ "data": "Emails" },
				{ "data": "Actions" }
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('vendor-id', data.Id);
			}
		});
	}
	function CreateVendor() {
		$(document).on('click', '#create-vendor', function () {
			$('#vendor-name').val('');
			$('#vendor-address').val('');
			$('#create-vendor-modal').attr('vendor-id', '0');
			$('#contact-div').empty();
			$('#email-div').empty();
			$('#contact-div').append(contactTemplate);
			$('#email-div').append(emailTemplate);
			$('.remove-contact').parent().remove();
			$('.remove-email').parent().remove();
			$('#create-vendor-modal').modal("show");
		});
	}
	function EditVendor() {
		$(document).on('click', '.edit-vendor', function () {
			var id = $(this).parents('tr').attr("vendor-id");
			$.ajax({
				type: 'GET',
				url: "/Vendors/GetVendorDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						var contacts = data.contacts;
						var emails = data.emails;
						if (contacts.length > 0) {
							$('#contact-div').empty();
						}
						if (emails.length > 0) {
							$('#email-div').empty();
						}
						for (var x = 0; x < contacts.length; x++) {
							$('#contact-div').append('<div class="contact"><div class="fields" contact-id="' + contacts[x].Id + '">' +
								'<div class="five wide field">' +
								'<label>Phone Number</label>' +
								'<input autocomplete="off" type="text" name="vendor-mobile" class="vendor-mobile" placeholder="Phone Number" value="' + contacts[x].Value + '"/>' +
								'</div>' +
								'<div class="one wide field">' +
								'<label>&nbsp;</label>' +
								'<button type="button" class="circular ui icon blue mini button add-contact" data-tooltip="Add Contact Number">' +
								'<i class="plus icon"></i>' +
								'</button>' +
								'</div>' +
								(x!=0?'<div class="one wide field">' +
								'<label>&nbsp;</label>' +
								'<button type="button" class="circular ui icon orange mini button remove-contact" data-tooltip="Remove">' +
								'<i class="minus icon"></i>' +
								'</button>' +
								'</div>':'') +
								'</div></div>');
						}
						for (var x = 0; x < emails.length; x++) {
							$('#email-div').append('<div class="email"><div class="fields" email-id="' + emails[x].Id + '">' +
								'<div class="five wide field">' +
								'<label>Email</label>' +
								'<input autocomplete="off" type="text" name="vendor-email" class="vendor-email" placeholder="Email" value="' + emails[x].Email + '"/>' +
								'</div>' +
								'<div class="one wide field">' +
								'<label>&nbsp;</label>' +
								'<button type="button" class="circular ui icon blue mini button add-email" data-tooltip="Add Email">' +
								'<i class="plus icon"></i>' +
								'</button>' +
								'</div>' +
								(x!=0?'<div class="one wide field">' +
								'<label>&nbsp;</label>' +
								'<button type="button" class="circular ui icon orange mini button remove-email" data-tooltip="Remove">' +
								'<i class="minus icon"></i>' +
								'</button>' +
								'</div>':'') +
								'</div></div>');
						}
						$('#vendor-name').val(data.vendor.Name);
						$('#vendor-address').val(data.vendor.Address);
						$('#create-vendor-modal').attr('vendor-id', data.vendor.Id);
						$('#create-vendor-modal').modal("show");
					}
				}
			});
		});
	}
	function SaveVendor() {
		$(document).on('click', '#save-vendor', function () {
			var formData = new FormData();
			formData.append("Name", $('#vendor-name').val());
			formData.append("Address", $('#vendor-address').val());
			var contacts = [];
			$('.contact').each(function (index, element) {
				var contactNumber = $(this).find('.vendor-mobile').val();
				if (contactNumber != null && contactNumber != "") {
					var jsonFee = {
						Id: $(this).find('.fields').attr('contact-id'),
						Value: contactNumber,
						VendorId: 0
					};
					contacts.push(jsonFee);
				}
			});
			var emails = [];
			$('.email').each(function (index, element) {
				var email = $(this).find('.vendor-email').val();
				if (email != null && email != "") {
					var jsonFee = {
						Id: $(this).find('.fields').attr('email-id'),
						Email: email,
						VendorId:0
					};
					emails.push(jsonFee);
				}
			});
			formData.append('ContactString', JSON.stringify(contacts));
			formData.append('EmailString', JSON.stringify(emails));
			var vendorId = $('#create-vendor-modal').attr('vendor-id');
			if (vendorId == "0") {
				$.ajax({
					type: "POST",
					url: "/Vendors/Create/",
					data: formData,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data.success) {
							$('#create-vendor-modal').attr('vendor-id', data.vendorId);
							tableVendor.ajax.reload(null, false);
							$('#create-vendor-modal').modal("hide");
							swal("Data saved!", "", "success");
						}
						else {
							swal("Error in adding worker.", "", "error");
						}
					}
				});
			}
			else {
				formData.append('Id', vendorId);
				$.ajax({
					type: "POST",
					url: "/Vendors/Edit/",
					data: formData,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data.success) {
							$('#create-vendor-modal').attr('vendor-id', data.vendorId);
							tableVendor.ajax.reload(null, false);
							$('#create-vendor-modal').modal("hide");
							swal("Data saved!", "", "success");
						}
						else {
							swal("Error in adding worker.", "", "error");
						}
					}
				});
			}
		});
	}
	//Contact Settings and template
	var contactTemplate = '<div class="contact"><div class="fields" contact-id="0">' +
		'<div class="five wide field">' +
		'<label>Phone Number</label>' +
		'<input autocomplete="off" type="text" name="vendor-mobile" class="vendor-mobile" placeholder="Phone Number"/>' +
		'</div>' +
		'<div class="one wide field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-contact" data-tooltip="Add Contact Number">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'</div>' +
		'<div class="one wide field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon orange mini button remove-contact" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div></div>';
	function AddContact() {
		$(document).on('click', '.add-contact', function () {
			$('#contact-div').append(contactTemplate);
		});
	}
	function RemoveContact() {
		$(document).on('click', '.remove-contact', function () {
			$(this).parents('.fields').remove();
		});
	}

	//Email Template and settings
	var emailTemplate = '<div class="email"><div class="fields" email-id="0">' +
		'<div class="five wide field">' +
		'<label>Email</label>' +
		'<input autocomplete="off" type="text" name="vendor-email" class="vendor-email" placeholder="Email"/>' +
		'</div>' +
		'<div class="one wide field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon blue mini button add-email" data-tooltip="Add Email">' +
		'<i class="plus icon"></i>' +
		'</button>' +
		'</div>' +
		'<div class="one wide field">' +
		'<label>&nbsp;</label>' +
		'<button type="button" class="circular ui icon orange mini button remove-email" data-tooltip="Remove">' +
		'<i class="minus icon"></i>' +
		'</button>' +
		'</div>' +
		'</div></div>';
	function AddEmail() {
		$(document).on('click', '.add-email', function () {
			$('#email-div').append(emailTemplate);
		});
	}
	function RemoveEmail() {
		$(document).on('click', '.remove-email', function () {
			$(this).parents('.fields').remove();
		});
	}
});