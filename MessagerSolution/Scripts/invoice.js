﻿$(document).ready(function () {
	var tableInvoiceDocuments;
	var tableInvoiceSchedule;
	var invoiceTable;
	LoadInvoiceTemplates();
	OnAddTemplateVariable();
	AddInvoiceTemplate();
	SaveDocumentTemplate();
	EditInvoiceTemplate();
	LoadInvoiceSchedules();
	GetInvoiceTemplate();
	GetEmailTemplate();
	CreateInvoiceSchedule();
	SaveInvoiceSchedule();
	EditInvoiceSchedule();
	LoadInvoiceTable();
	function RemoveTag(content) {
		content.find('.icon.tag').remove();
	}
	function VariableAddTags(content, searchText = ["[Renter Firstname]", "[Renter Lastname]", "[Renter Address]", "[Property Address]", "[Company]", "[Total Amount]", "[Renter Phone]","[Transaction Details]","[Date]"]) {
		var elements = ["span", "h1", "h2", "h3", "h4", "h5", "h6"];
		//Replace Text on document to SignRequest Tag
		for (var i = 0; i < elements.length; i++) {
			for (var x = 0; x < searchText.length; x++) {
				$('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ">:contains('" + searchText[x] + "')"));
				$('<i class="icon tags"></i>').insertBefore(content.find(elements[i] + ":contains('" + searchText[x] + "')"));
			}
		}
	}
	function SaveDocumentTemplate() {
		$(document).on('click', '#invoice-template-save', function () {
			var id = $('#invoice-template-modal').attr('document-id');
			var name = $('#invoice-document-name').val();
			RemoveTag($('#invoice-template-modal').find('.nicEdit-main'));
			var formData = new FormData();
			for (var i = 0; i < $('#invoice-template-upload').get(0).files.length; ++i) {
				formData.append("Files[" + i + "]", $('#invoice-template-upload').get(0).files[i]);
			}
			formData.append("Id", id);
			formData.append("DocumentName", name);
			formData.append("Content", $('#invoice-template-modal').find('.nicEdit-main').html());

			$.ajax({
				type: 'POST',
				url: "/Payment/CreateInvoiceTemplate",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						tableInvoiceDocuments.ajax.reload(null, false);
						$('#invoice-template-modal').modal("hide");

					}
					else {
						swal("Error occured on deleting payment method", "", "error");
					}
				},
				error: (error) => {
					console.log(JSON.stringify(error));
				}
			});
		});
	}
	function AddInvoiceTemplate() {
		$(document).on('click', '#create-invoice-template', function (e) {
			var documentTemplateModal = $('#invoice-template-modal');
			documentTemplateModal.find('#document-name').val('');
			documentTemplateModal.find('.nicEdit-main').empty();
			documentTemplateModal.attr('document-id', '0');
			$('#template-upload').val('');
			documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

		});
	}
	function LoadInvoiceTemplates() {
		tableInvoiceDocuments = $('#invoice-template-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Payment/GetInvoiceTemplates",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "Name" },
				{ "data": "Actions" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-invoice-template-id', data.Id);
			}
		});
	}

	function OnAddTemplateVariable() {
		$("#invoice-template-variable").on('change', function () {
			var variable = $(this).children(':selected').val();
			$('#invoice-template-modal').find('.nicEdit-main').append('<i class="icon tags"></i><span>' + variable + '</span>');
		});
	}

	function EditInvoiceTemplate() {
		$(document).on('click', '.edit-invoice-template', function () {
			var id = $(this).parents('tr').attr('data-invoice-template-id');
			$.ajax({
				type: 'GET',
				url: "/Payment/GetTemplateDetails",
				data: { id: id },
				success: function (data) {
					if (data.success) {
						var template = data.template;
						console.log(data.template)
						var documentTemplateModal = $('#invoice-template-modal');
						documentTemplateModal.attr('document-id', template.Id);
						documentTemplateModal.find('#invoice-document-name').val(template.Name);
						$('#invoice-template-upload').val('');
						documentTemplateModal.find('.nicEdit-main').html(template.Html);
						VariableAddTags(documentTemplateModal.find('.nicEdit-main'));
						documentTemplateModal.find('.Section0').children().first().remove();
						documentTemplateModal.modal({ closable: false }).modal('show').modal('refresh');

					}
				}
			});
		});
	}

	function LoadInvoiceSchedules() {
		tableInvoiceSchedule = $('#invoice-schedule-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"bAutoWidth": false,
			"bLengthChange": false,
			"searching": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Invoice/GetInvoiceSchedules",
				"type": 'GET',
				"async": true
			},
			"columns": [
				{ "data": "Description" },
				{ "data": "EmailTemplate" },
				{ "data": "InvoiceTemplate" },
				{ "data": "PaymentMethod" },
				{ "data": "Properties" },
				{ "data": "Actions" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-invoice-schedule-id', data.Id);
			}
		});
	}
	function GetInvoiceTemplate() {
		$.ajax({
			type: "GET",
			url: "/Payment/GetTemplates",
			success: function (data) {
				var documents = data.documents;
				var dropdown = $('.invoice-template');
				var optionData = '';
				var selectOption = '';

				dropdown.find('.menu').empty();
				dropdown.find('select').empty();
				var optionData = '<option value="">Select Template</option>';
				for (var i = 0; i < documents.length; i++) {
					selectOption += '<option value="' + documents[i].Id + '">' + documents[i].Name+ '</option>';
					optionData += '<div class="item" data-value="' + documents[i].Id + '">' + documents[i].Name + '</div>';
				}

				dropdown.find('select').append(selectOption);
				dropdown.find('.menu').append(optionData);
				dropdown.dropdown('clear');
			}
		});
	}
	function GetEmailTemplate() {
		$.ajax({
			type: "GET",
			url: "/Payment/GetEmailTemplates",
			success: function (data) {
				var dropdown = $('.email-template');
				var template = data.data;

				var optionData = '';
				var selectOption = '';

				dropdown.find('.menu').empty();
				dropdown.find('select').empty();
				var optionData = '<option value="">Select Template</option>';
				for (var i = 0; i < template.length; i++) {
					selectOption += '<option value="' + template[i].Id + '">' + template[i].Name + '</option>';
					optionData += '<div class="item" data-value="' + template[i].Id + '">' + template[i].Name + '</div>';
				}
			
				dropdown.find('select').append(selectOption);
				dropdown.find('.menu').append(optionData);
				dropdown.dropdown('clear');
			}
		});
	}

	function CreateInvoiceSchedule() {
		$(document).on('click', '#create-invoice-schedule', function (e) {
			var scheduleModal = $('#invoice-schedule-modal');
			scheduleModal.attr('invoice-schedule-id', '0');
			scheduleModal.find('.day-of-month').dropdown('set selected','1');
			scheduleModal.find('.invoice-schedule-property').dropdown('clear');
			scheduleModal.find('.email-template').dropdown('clear');
			scheduleModal.find('.invoice-template').dropdown('clear');
			//scheduleModal.find('.payment-method').dropdown('clear');
			scheduleModal.find('.invoice-schedule-description').val('');
			scheduleModal.modal({ closable: false }).modal('show').modal('refresh');
			
		});
	}
	function SaveInvoiceSchedule() {
		$(document).on('click', '#save-invoice-schedule', function (e) {
			var scheduleModal = $('#invoice-schedule-modal');
			var id = scheduleModal.attr('invoice-schedule-id');
			alert(scheduleModal.find('.payment-method').dropdown('get value'));
			return;
			var formData = new FormData();
			formData.append("Id", id);
			formData.append("DayOfMonth", scheduleModal.find('.day-of-month').dropdown('get value'));
			formData.append("PropertyIds", scheduleModal.find('.invoice-schedule-property').dropdown('get value'));
			formData.append("EmailTemplateId", scheduleModal.find('.email-template').dropdown('get value'));
			formData.append("InvoiceTemplateId", scheduleModal.find('.invoice-template').dropdown('get value'));
			formData.append("PaymentMethod", scheduleModal.find('.payment-method').dropdown('get value'));
			formData.append("Description", scheduleModal.find('.invoice-schedule-description').val());
			$.ajax({
				type: 'POST',
				url: "/Invoice/SaveInvoiceSchedule",
				contentType: false,
				cache: false,
				dataType: "json",
				processData: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						tableInvoiceSchedule.ajax.reload(null, false);
						scheduleModal.modal("hide");

					}
					else {
						swal("Error occured on deleting payment method", "", "error");
					}
				},
				error: (error) => {
					console.log(JSON.stringify(error));
				}
			});
		});
		
	}

	function EditInvoiceSchedule() {
		$(document).on('click', '.edit-invoice-schedule', function (e) {
			var id = $(this).parents('tr').attr('data-invoice-schedule-id');
			$.ajax({
				type: 'GET',
				url: "/Invoice/GetInvoiceSchedule",
				data: { id: id },
				success: function (data) {
					var schedule = data.schedule;
					var scheduleModal = $('#invoice-schedule-modal');
					scheduleModal.attr('invoice-schedule-id', schedule.Id);
					scheduleModal.find('.day-of-month').dropdown("set selected", schedule.DayOfMonth);
					var propertyIds = schedule.PropertyIds.split(',');
					for (var x = 0; x < propertyIds.length; x++) {
						scheduleModal.find('.invoice-schedule-property').dropdown("set selected", propertyIds[x]);
					}
					scheduleModal.find('.email-template').dropdown("set selected",schedule.EmailTemplateId);
					scheduleModal.find('.invoice-template').dropdown("set selected", schedule.InvoiceTemplateId);
					scheduleModal.find('.payment-method').val(schedule.PaymentMethod);
					scheduleModal.find('.invoice-schedule-description').val(schedule.Description);
					scheduleModal.modal({ closable: false }).modal('show').modal('refresh');
					$('.ui.dropdown').dropdown();
				}
			});
		});
	}
	function LoadInvoiceTable() {
		invoiceTable = $('#invoice-table').DataTable({
			"aaSorting": [],
			"responsive": true,
			"search": { "caseInsensitive": true },
			"ajax": {
				"url": "/Invoice/GetInvoices",
				"type": 'GET'
			},
			"columns": [
				{ "data": "DateCreated" },
				{ "data": "Renter" },
				{ "data": "Property" },
				{ "data": "PaymentMethod" },
				{ "data": "FileUrl" },
				{ "data": "Status" },
			],
			"createdRow": function (row, data, rowIndex) {
				$(row).attr('data-invoice-id', data.Id);
			},
			columnDefs: [{
				targets: [0], type: "date", render: function (data) {
					return moment(data).format('MMMM D,YYYY');
				}
			},
			{
				targets: [4], type: "text", render: function (data) {
					return '<a target ="_blank" href="' + data + '"><i class="huge icon file alternate"></i></a>';
				}
			}
			],

		});
	}
});