﻿
var device;
var userId;
var openOutgoingCallModal = function () { };

$(function () {

    //#region
    $("body").append(`
	<div class="ui ongoing-call mini modal">
		<div class="content">
			<img class="right floated ui avatar image" src="https://semantic-ui.com/images/avatar/large/elliot.jpg">
			<div class="header name"></div>
			<div class="meta phone-number"></div>
			<div class="ui clearing divider"></div>
			<div class="ui grid">
				<div class="row ui centered">
					<h1 class="ui header time">
						<span class="minutes">00</span>:<span class="seconds">00</span>
					</h1>
				</div>
				<div class="two column row">
					<div class="column">
						<div class="ui tiny progress indicating input-volume">
							<div class="bar"></div>
							<div class="label">
								<i class="icon headphones volume"></i>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="ui tiny progress indicating output-volume">
							<div class="bar">
								<div class="progress"></div>
							</div>
							<div class="label">
								<i class="icon microphone volume"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="one column row">
					<div class="column">
						<button class="fluid ui negative button">Hangup</button>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="ui incoming-call mini modal">
		<div class="content">
			<img class="right floated ui avatar image" src="https://semantic-ui.com/images/avatar/large/elliot.jpg">
			<div class="header name"></div>
			<div class="meta phone-number"></div>
			<div class="ui clearing divider"></div>
			<div class="actions">
				<div class="ui three buttons">
					<button class="ui positive button answer">Answer</button>
					<div class="or"></div>
					<button class="ui negative button">Decline</button>
				</div>
			</div>
		</div>
	</div>
    <div class="ui play-recording modal">
        <div class="content">
            <div class="ui fluid">
                <div class="audio green-audio-player">
                    <div class="loading">
                    <i class="spinner loading icon"></i>
                    </div>
                    <div class="play-pause-btn">
                    <i class="play icon" class="play-pause-icon" id="playPause"></i>
                    </div>
                    
                    <div class="controls">
                    <span class="current-time">0:00</span>
                        <div class="slider-bar" data-direction="horizontal">
                        <div class="progress-bar">
                        <div class="pin" id="progress-pin" data-method="rewind"></div>
                        </div>
                    </div>
                    <span class="total-time">0:00</span>
                    </div>
                    
                    <div class="volume">
                    <div class="volume-btn">
                        <i class="volume up icon" id="speaker"></i>
                    </div>
                    <div class="volume-controls hidden">
                        <div class="slider-bar" data-direction="vertical">
                        <div class="progress-bar">
                            <div class="pin" id="volume-pin" data-method="changeVolume"></div>
                        </div>
                        </div>
                    </div>
                    </div>
                    
                    <audio crossorigin>
                    <source id="recordingSource" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/355309/Swing_Jazz_Drum.mp3" type="audio/wav">
                    </audio>
                </div>
            </div>
    
            <div class="actions">
                <button class="fluid ui button cancel">Close</button>
            </div>
        </div>
    </div>
    `);
    //#endregion

  //  $('.form')
		//.form({
		//    on: 'blur',
		//    fields: {
		//        number: {}
		//    }
		//})
  //  ;

    $('.button.manual-call').on('click', () => {
        var params = {
            From: userId,
            To: $('.form.manual-form [name="phone-number"]').val()
        };

        if (device) {
            $('.ongoing-call .phone-number').text($('.form.manual-form [name="phone-number"]').val());
            device.connect(params);
            openOutgoingCallModal();
        }
    });

    $(document).on('click', '.button.recording', function() { 
        let url = $(this).attr("data-recording-url");

        console.log(url);
        $('#recordingSource').attr('src', url);
        player.load();
        
        $('.play-recording.modal')
        .modal({
            onDeny: function () {
                playPause.classList.remove('pause');
                playPause.classList.add('play');
                player.pause();
                player.currentTime = 0;
                totalTime.textContent = formatTime(0);
            },
            closable: false
        })
        .modal('show');
   });

    var minutesLabel = $(".ongoing-call .minutes");
    var secondsLabel = $(".ongoing-call .seconds");
    var totalSeconds = 0;
    var callInterval;

    function setTime() {
        ++totalSeconds;
        secondsLabel.text(pad(totalSeconds % 60));
        minutesLabel.text(pad(parseInt(totalSeconds / 60)));
    }

    function clearTimeInterval() {
        clearInterval(callInterval);
        secondsLabel.text(pad(0));
        minutesLabel.text(pad(0));
    }

    function pad(val) {
        var valString = val + "";
        if (valString.length < 2) {
            return "0" + valString;
        } else {
            return valString;
        }
    }

    $(".input-volume.ui.progress, .output-volume.ui.progress").progress({
        percent: 1,
        label: ""
    });

    $(".ongoing-call .negative").on("click", () => {
        console.log('Hanging up...');
        if (device) {
            device.disconnectAll();
        }
        $('.ongoing-call.mini.modal')
			.modal('hide');
    });


    console.log('Requesting Capability Token...');
    $.ajax({
        type: 'POST',
        dataType: "json",
        cache: false,
        url: "/Call/Token",
        success: function (data) {
            console.log(data.token);
            userId = data.userId;

            device = new Twilio.Device(data.token);

            device.ready(function (device) {
                $('.button.call-button').prop('disabled', false);
                console.log('Twilio.Device Ready!');
            });

            device.error(function (error) {
                $('.ongoing-call.mini.modal')
					.modal('hide');
                $('.incoming-call.mini.modal')
                    .modal('hide');
                toastr.error(error.message, null, { timeOut: 5000, positionClass: "toast-top-right" });
                console.log('Twilio.Device Error: ' + error.message);
            });

            device.cancel(function (conn) {
                console.log('Device cancelled');
                console.log(conn);
                $('.incoming-call.mini.modal')
                    .modal('hide');
                toastr.info('Call Cancelled', null, { timeOut: 3000, positionClass: "toast-top-right" });
            });

            device.connect(function (conn) {
                console.log(conn.parameters);
				if (conn.parameters && conn.parameters.From) {
					$.ajax({
						type: 'GET',
						url: "/Call/GetContactInfo",
						data:{ ContactNumber: conn.parameters.From},
						success: function (data) {
							console.log(data)
							$('.mini.modal .header.name').text("");
							if (data.Image == "")
								$('.mini.modal .avatar.image').attr("src", (data.Image != "" ? data.Image : '/Images/Worker/default-image.png'));
							else {
								$('.mini.modal .avatar.image').hide();
							}
							$('.mini.modal .phone-number').text((data.Name != "" ? data.Name : conn.parameters.From));
						}
					});
                
                }
                console.log('Successfully established call!');
                totalSeconds = 0;
                callInterval = setInterval(setTime, 1000);
                conn.volume(function (inputVolume, outputVolume) {
                    $(".input-volume.ui.progress").progress({
                        percent: (inputVolume * 100) + 20,
                        label: ""
                    });
                    $(".output-volume.ui.progress").progress({
                        percent: (outputVolume * 100) + 20,
                        label: ""
                    });
                });
            });

            device.disconnect(function (conn) {
                console.log('Call ended.');
                clearTimeInterval();
                $('.ongoing-call.mini.modal')
					.modal('hide');
                $('.incoming-call.mini.modal')
                    .modal('hide');
            });

            device.incoming(function (conn) {
                console.log(conn.parameters);
                if (conn.parameters && conn.parameters.From) {
					$.ajax({
						type: 'GET',
						url: "/Call/GetContactInfo",
						data: { ContactNumber: conn.parameters.From },
						success: function (data) {
							console.log(data)
							$('.mini.modal .header.name').text("");
							if (data.Image == "")
								$('.mini.modal .avatar.image').attr("src", (data.Image != "" ? data.Image : '/Images/Worker/default-image.png'));
							else {
								$('.mini.modal .avatar.image').hide();
							}
							$('.mini.modal .phone-number').text((data.Name != "" ? data.Name : conn.parameters.From));
						}
					});
                }
                $('.incoming-call.mini.modal')
					.modal({
					    closable: false,
					    onDeny: function () {
					        conn.reject();
					    },
					    onApprove: function () {
					        conn.accept();
					        openOutgoingCallModal();
					    }
					})
					.modal('show');
            });
        },
        error: function () {
            console.log('Could not get a token from server!');
        }
    });

    openOutgoingCallModal = function() {
        clearTimeInterval();
        $('.ongoing-call.mini.modal')
			.modal('setting', 'closable', false)
			.modal('show')
        ;
    }

    var audioPlayer = document.querySelector(".green-audio-player");
    var playPause = audioPlayer.querySelector("#playPause");
    var playpauseBtn = audioPlayer.querySelector(".play-pause-btn");
    var loading = audioPlayer.querySelector(".loading");
    var progress = audioPlayer.querySelector(".progress-bar");
    var sliders = audioPlayer.querySelectorAll(".slider-bar");
    var volumeBtn = audioPlayer.querySelector(".volume-btn");
    var volumeControls = audioPlayer.querySelector(".volume-controls");
    var volumeProgress = volumeControls.querySelector(".slider-bar .progress-bar");
    var player = audioPlayer.querySelector("audio");
    var currentTime = audioPlayer.querySelector(".current-time");
    var totalTime = audioPlayer.querySelector(".total-time");
    var speaker = audioPlayer.querySelector("#speaker");

    var draggableClasses = ["pin"];
    var currentlyDragged = null;

    window.addEventListener("mousedown", function (event) {
        if (!isDraggable(event.target)) return false;

        currentlyDragged = event.target;
        var handleMethod = currentlyDragged.dataset.method;

        this.addEventListener("mousemove", window[handleMethod], false);

        window.addEventListener(
            "mouseup",
            function () {
                currentlyDragged = false;
                window.removeEventListener("mousemove", window[handleMethod], false);
            },
            false
        );
    });

    playpauseBtn.addEventListener("click", togglePlay);
    player.addEventListener("timeupdate", updateProgress);
    player.addEventListener("volumechange", updateVolume);
    player.addEventListener("loadedmetadata", function () {
        totalTime.textContent = formatTime(player.duration);
    });
    player.addEventListener("canplay", makePlay);
    player.addEventListener("ended", function () {
        playPause.classList.remove('pause');
        playPause.classList.add('add');
        player.currentTime = 0;
    });

    volumeBtn.addEventListener("click", function () {
        volumeBtn.classList.toggle("open");
        volumeControls.classList.toggle("hidden");
    });

    window.addEventListener("resize", directionAware);

    sliders.forEach(function (slider) {
        var pin = slider.querySelector(".pin");
        slider.addEventListener("click", window[pin.dataset.method]);
    });

    directionAware();

    function isDraggable(el) {
        var canDrag = false;
        var classes = Array.from(el.classList);
        draggableClasses.forEach(function (draggable) {
            if (classes.indexOf(draggable) !== -1) canDrag = true;
        });
        return canDrag;
    }

    function inRange(event) {
        var rangeBox = getRangeBox(event);
        var rect = rangeBox.getBoundingClientRect();
        var direction = rangeBox.dataset.direction;
        if (direction == "horizontal") {
            var min = rangeBox.offsetLeft;
            var max = min + rangeBox.offsetWidth;
            if (event.clientX < min || event.clientX > max) return false;
        } else {
            var min = rect.top;
            var max = min + rangeBox.offsetHeight;
            if (event.clientY < min || event.clientY > max) return false;
        }
        return true;
    }

    function updateProgress() {
        var current = player.currentTime;
        var percent = current / player.duration * 100;
        progress.style.width = percent + "%";

        currentTime.textContent = formatTime(current);
    }

    function updateVolume() {
        volumeProgress.style.height = player.volume * 100 + "%";
        if (player.volume >= 0.5) {
            speaker.attributes.d.value =
                "M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z";
        } else if (player.volume < 0.5 && player.volume > 0.05) {
            speaker.attributes.d.value =
                "M0 7.667v8h5.333L12 22.333V1L5.333 7.667M17.333 11.373C17.333 9.013 16 6.987 14 6v10.707c2-.947 3.333-2.987 3.333-5.334z";
        } else if (player.volume <= 0.05) {
            speaker.attributes.d.value = "M0 7.667v8h5.333L12 22.333V1L5.333 7.667";
        }
    }

    function getRangeBox(event) {
        var rangeBox = event.target;
        var el = currentlyDragged;
        if (event.type == "click" && isDraggable(event.target)) {
            rangeBox = event.target.parentElement.parentElement;
        }
        if (event.type == "mousemove") {
            rangeBox = el.parentElement.parentElement;
        }
        return rangeBox;
    }

    function getCoefficient(event) {
        var slider = getRangeBox(event);
        var rect = slider.getBoundingClientRect();
        var K = 0;
        if (slider.dataset.direction == "horizontal") {
            var offsetX = event.clientX - slider.offsetLeft;
            var width = slider.clientWidth;
            K = offsetX / width;
        } else if (slider.dataset.direction == "vertical") {
            var height = slider.clientHeight;
            var offsetY = event.clientY - rect.top;
            K = 1 - offsetY / height;
        }
        return K;
    }

    function rewind(event) {
        if (inRange(event)) {
            player.currentTime = player.duration * getCoefficient(event);
        }
    }

    function changeVolume(event) {
        if (inRange(event)) {
            player.volume = getCoefficient(event);
        }
    }

    function formatTime(time) {
        var min = Math.floor(time / 60);
        var sec = Math.floor(time % 60);
        return min + ":" + (sec < 10 ? "0" + sec : sec);
    }

    function togglePlay() {
        if (player.paused) {
            playPause.classList.remove('play');
            playPause.classList.add('pause');
            player.play();
        } else {
            playPause.classList.remove('pause');
            playPause.classList.add('play');
            player.pause();
        }
    }

    function makePlay() {
        playpauseBtn.style.display = "block";
        loading.style.display = "none";
    }

    function directionAware() {
        if (window.innerHeight < 250) {
            volumeControls.style.bottom = "-54px";
            volumeControls.style.left = "54px";
        } else if (audioPlayer.offsetTop < 154) {
            volumeControls.style.bottom = "-164px";
            volumeControls.style.left = "-3px";
        } else {
            volumeControls.style.bottom = "52px";
            volumeControls.style.left = "-3px";
        }
    }

});