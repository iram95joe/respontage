﻿$(document).ready(function () {
    var tableDocuments;
    LoadDocumentTable()
    $(document).on('click', '#save-document', function () {
        var formData = new FormData();
        formData.append("bookingId", $('#BookingId').val());
        formData.append("description", $('#document-description').val());
        for (var i = 0; i < $('#document-file-upload').get(0).files.length; ++i) {
            formData.append("Files[" + i + "]", $('#document-file-upload').get(0).files[i]);
        }
        $.ajax({
            type: "POST",
            url: '/Rentals/SaveDocuments',
            data: formData,
            beforeSend: function () {
                $('#save-document').addClass('loading');
            },
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.success) {
                    $('#save-document').removeClass('loading');
                    swal("Successfully Save Document(s)!", "", "success");
                    tableDocuments.ajax.reload(null, false);
                    $('#renter-document-modal').modal('hide');
                }

            }
        });
    });


    $(document).on('click', '#add-document', function () {
        $('#document-description').val('');
        $('#document-file-upload').val('');
        $('#file-display').empty();
		$('#renter-document-modal').modal('show');
    });
    $(document).on('click', '#file-upload-btn', function () {
        $('#document-file-upload').click()
    });
    $(document).on('change', '#document-file-upload', function (e) {
        var attachments = $('#file-display');
        attachments.empty();
        for (var i = 0; i < e.target.files.length; i++) {
            var fileUrl = window.URL.createObjectURL(this.files[i]);
            var type = this.files[i]['type'].split('/')[0];
            if (type == 'image') {
                attachments.append('<div class="ui small image"><i class="close icon"></i><img src="' + fileUrl + '"><div>');
            }
            else {
                attachments.append('<a target="_blank" href="' + fileUrl + '"><i class="huge icon ' + FileIcon(fileUrl) + '"></i></a>');
            }
        }
    });

    function LoadDocumentTable(){
        tableDocuments = $('#document-table').DataTable({
            "aaSorting": [],
            "responsive": true,
            "search": { "caseInsensitive": true },
            "ajax": {
                "url": "/Rentals/GetDocuments",
                "type": 'GET',
                "data": function () {
                    return {
                        bookingId: $('#BookingId').val()
                    }
                }
            },
            "columns": [
                { "data": "Description" },
                { "data": "Files"},
                { "data": "Actions", "orderable": false }
            ],

            "createdRow": function (row, data, rowIndex) {
                $(row).attr('data-document-id', data.Id);
            }

        });
    }
    function FileIcon(url) {
        var extention = url.split('.').pop();
        if (extention == "pdf") {
            return "file pdf outline";
        }
        else if (extention == "docx" || extention == "docm" || extention == "dot") {
            return "file word outline";
        }
        else if (extention == "xlsx" || extention == "xlsm" || extention == "xlsb") {
            return "file excel outline";
        }
        else {
            return "file alternate";
        }
    }
});