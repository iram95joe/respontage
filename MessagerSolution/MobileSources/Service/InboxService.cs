﻿using Core.Database.Context;
using Core.Models;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using MessagerSolution.MobileSources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Web;

namespace MessagerSolution.MobileSources.Service
{
    public class InboxService
    {
 
        public static List<Inbox> GetInboxes(int companyId,int userId)
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            using (var db = new ApplicationDbContext())
            {
                List<Inbox> List = new List<Inbox>();
               var temp = (from r in db.Renters join t in db.CommunicationThreads on r.Id equals t.RenterId where t.CompanyId == companyId && t.IsRenter select new { Renter = r, Thread = t }).ToList();
                foreach(var item in temp)
                {
                    var listingId = db.Inquiries.Where(x => x.Id == item.Renter.BookingId).FirstOrDefault().PropertyId;
                    var propertyname = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault().Name;
                    Inbox inbox = new Inbox();
                    inbox.PropertyName = propertyname;
                    inbox.ThreadId = item.Thread.ThreadId;
                    inbox.Name = item.Renter.Firstname +" "+item.Renter.Lastname;
                    inbox.RenterId = item.Renter.Id;
                    inbox.ImageUrl = Url+Core.Helper.Utilities.GetDefaultAvatar(item.Renter.Firstname);
                    if (item.Thread != null)
                    {
                        var lastMessage = db.CommunicationMessages.Where(x => x.ThreadId == item.Thread.ThreadId).OrderByDescending(x => x.DateCreated).Select(x => x).FirstOrDefault();
                        if (lastMessage != null)
                        {
                            inbox.Message = lastMessage.Message;
                            inbox.MessageDate = lastMessage.DateCreated.ToString("MMM dd,yyyy hh:mm tt");
                        }
                        else if (lastMessage == null)
                        {
                            var lastSMS = db.CommunicationSMS.Where(x => x.ThreadId == item.Thread.ThreadId).OrderByDescending(x => x.CreatedDate).Select(x => x).FirstOrDefault();
                            if (lastSMS != null)
                            {
                                inbox.Message = lastSMS.Message;
                                inbox.MessageDate = lastSMS.CreatedDate.ToString("MMM dd,yyyy hh:mm tt");
                            }
                            else
                            {
                                inbox.Message = "";
                            }
                        }

                    }
                    else
                    {
                        inbox.Message = "No Message";
                    }
                    inbox.Name = item.Renter.Firstname + " " + item.Renter.Lastname;
                    List.Add(inbox);
                }

                return List;

            }
        }
        public static List<Message> GetMessages(string threadId,int userId,int skip)
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            List<Message> MessageList = new List<Message>();
            using (var db = new ApplicationDbContext())
            {
                var renterSMS = (from user in db.Renters
                                 join t in db.CommunicationThreads on user.Id equals t.RenterId
                                 join m in db.CommunicationSMS on t.ThreadId equals m.ThreadId
                                 where t.ThreadId == threadId && m.UserId == 0
                                 select new { Id = m.Id, Message = m.Message, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.Firstname + " " + user.Lastname, From = m.From }).ToList();

                var temp = (from user in db.Users
                            join m in db.CommunicationMessages on user.Id equals m.UserId
                            join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                            where t.ThreadId == threadId
                            select new { Message = m.Message, CreatedDate = m.DateCreated, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();

                var sms = (from user in db.Users
                           join m in db.CommunicationSMS on user.Id equals m.UserId
                           join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                           where t.ThreadId == threadId
                           select new { Id = m.Id, ResourceSid = m.MessageSid, To = m.To, From = m.From, Message = m.Message, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();

                var call = (from user in db.Users
                            join m in db.CommunicationCalls on user.Id equals m.UserId
                            join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                            where t.ThreadId == threadId
                            select new { CallSid = m.CallSid, ResourceSid = m.ResourceSid, To = m.To, From = m.From, RecordingUrl = m.RecordingUrl, Duration = m.CallDuration == null ? m.RecordingDuration : m.CallDuration, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();
                var Emails = (from m in db.CommunicationEmails
                              join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                              where t.ThreadId == threadId
                              select new { IsMyMessage = m.IsMyMessage, Id = m.Id, To = m.To, From = m.From, Message = m.Message, CreatedDate = m.CreatedDate }).ToList();

                foreach (var item in Emails)
                {
                    Message message = new Message();
                    message.MessageContent = item.Message;
                    message.IsMyMessage = item.IsMyMessage;
                    message.CreatedDate = item.CreatedDate;
                    message.Source = message.IsMyMessage ? item.To : item.From;
                    message.Name = "Email";
                    message.ImageUrl = Url + Core.Helper.Utilities.GetDefaultAvatar(message.Name);
                    message.Type = 5;
                    message.ImageUrl = string.IsNullOrEmpty(message.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(message.Name) : message.ImageUrl;
                    db.EmailAttachments.Where(x => x.CommunicationEmailId == item.Id).ToList().ForEach(x =>
                    message.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                    MessageList.Add(message);
                }
                foreach (var item in renterSMS)
                {
                    Message message = new Message();
                    message.MessageContent = item.Message;
                    message.IsMyMessage = false;
                    message.CreatedDate = item.CreatedDate;
                    message.Source = item.From;
                    message.Name = item.Name;
                    //Get files with Id
                    (from file in db.MMSImages where file.CommunicationSMSId == item.Id select new { Id = file.Id, FileUrl =Url+ file.ImageUrl }).ToList().ForEach(x =>
                    message.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));

                    if (message.Source.Substring(0, 8) == "whatsapp")
                    {
                        message.Type = 3;
                    }
                    else if (message.Source.Substring(0, 9) == "messenger")
                    {
                        message.Type = 4;
                    }
                    else
                    {
                        message.Type = 1;
                    }
                    message.ImageUrl = Url+ Core.Helper.Utilities.GetDefaultAvatar(item.Name);

                    MessageList.Add(message);
                }
                foreach (var item in temp)
                {

                    Message message = new Message();
                    message.MessageContent = item.Message;
                    message.IsMyMessage = (item.UserId == userId? true : false);
                    message.CreatedDate = item.CreatedDate;
                    message.Name = item.Name;
                    message.ImageUrl = Url+(string.IsNullOrEmpty(item.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl);
                    message.Type = 0;
                    MessageList.Add(message);
                }
                foreach (var item in sms)
                {
                    Message message = new Message();
                    message.MessageContent = item.Message;
                    message.IsMyMessage = (item.UserId == userId ? true : false);
                    message.CreatedDate = item.CreatedDate;
                    message.Source = message.IsMyMessage ? item.To : item.From;
                    message.Name = item.Name;
                    //Get files with Id
                    (from file in db.MMSImages where file.CommunicationSMSId == item.Id select new { Id = file.Id, FileUrl = Url+file.ImageUrl }).ToList().ForEach(x =>
                    message.Files.Add(new FileModel() {Id=x.Id,FileUrl=x.FileUrl }));

                    if (message.Source.Substring(0, 8) == "whatsapp")
                    {
                        message.Type = 3;
                    }
                    else if (message.Source.Substring(0, 9) == "messenger")
                    {
                        message.Type = 4;
                    }
                    else
                    {
                        message.Type = 1;
                    }
                    message.ResourceSid = item.ResourceSid;
                    message.ImageUrl = Url+( string.IsNullOrEmpty(item.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl);

                    MessageList.Add(message);
                }


                foreach (var item in call)
                {
                    Message message = new Message();
                    message.IsMyMessage = (item.UserId == userId ? true : false);
                    message.Source = message.IsMyMessage ? item.To : item.From;
                    message.CreatedDate = item.CreatedDate;
                    message.ResourceSid = item.CallSid;
                    message.RecordingUrl = item.RecordingUrl;
                    message.Duration = item.Duration;

                    message.Name = item.Name;
                    message.Type = 2;
                    message.ImageUrl = Url+( string.IsNullOrEmpty(item.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl);

                    MessageList.Add(message);
                }
            }
            MessageList = MessageList.OrderByDescending(x => x.CreatedDate).Skip(skip).Take(10).ToList();
            return MessageList;
        } 

    }
}