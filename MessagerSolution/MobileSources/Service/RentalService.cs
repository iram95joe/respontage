﻿using Core.Database.Context;
using MessagerSolution.MobileSources.Models;
using MessagerSolution.Models.Rentals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.MobileSources.Service
{
    public class RentalService
    {
        public static List<Rental> GetRental(int companyId)
        {
            using(var db= new ApplicationDbContext())
            {
                List<Rental> rentals = new List<Rental>();
                var properties =db.Properties.Where(x => x.SiteType == 0 && x.IsParentProperty == false && x.CompanyId == companyId).ToList();
                foreach (var property in properties)
                {
                    Rental model = new Rental();
                    var status = "Available";
                    decimal remainingBalance = 0.0M;
                    var reservation = db.Inquiries.Where(x => x.PropertyId == property.ListingId).OrderByDescending(x => x.Id).FirstOrDefault();
                    if (reservation != null)
                    {
                        var dt = DateTime.Now;
                        var lastDayMonth = DateTime.DaysInMonth(dt.Year, dt.Month);
                        dt = new DateTime(dt.Year, dt.Month, lastDayMonth);
                        var incomes = db.Income.Where(x => x.BookingId == reservation.Id && x.DueDate <= dt).ToList();
                        foreach (var income in incomes)
                        {
                            var payment = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();

                            remainingBalance += income.Amount.HasValue ? (income.Amount.Value - payment.Sum(x => x.Amount)) : 0;

                        }
                        if (remainingBalance != 0)
                        {
                            status = "Unpaid - " + remainingBalance;
                        }

                        else if (reservation.CheckOutDate != null && reservation.CheckOutDate.Value.Year == dt.Year && reservation.CheckOutDate.Value.Month == dt.Month)
                        {
                            status = "Ending Contract";
                        }

                        else
                        {
                            status = "Current";
                        }
                        if (reservation != null)
                        {
                            var renters = db.Renters.Where(x => x.BookingId == reservation.Id && x.IsActive).ToList();
                            foreach (var renter in renters)
                            {
                                model.Guest += renter.Firstname + " " + renter.Lastname + Environment.NewLine;
                            }
                            model.ReservationId = reservation.Id;
                        }
                    }


                    model.Property = property.Name;
                    model.PropertyId = property.Id;
                    model.Status = status;
                    Dictionary<string, string> contracts = new Dictionary<string, string>();
                    var rentalContracts = (from r in db.Inquiries join renter in db.Renters on r.Id equals renter.BookingId where r.PropertyId == property.ListingId select new { BookingId = r.Id, RenterName = renter.Firstname + " " + renter.Lastname, Checkin = r.CheckInDate }).ToList();
                    foreach (var rental in rentalContracts)
                    {
                        contracts.Add(rental.BookingId.ToString(), rental.RenterName + " - " + rental.Checkin.Value.ToString("MMM dd,yyyy"));
                    }
                    model.RentalContracts = contracts;
                    rentals.Add(model);
                }
                return rentals;
            }
        }

    }
}