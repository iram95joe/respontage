﻿using Core.Database.Entity;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.MobileSources.Models
{
    public class IncomeDetails
    {
        public int Id { get; set; }
        public int IncomeTypeId { get; set; }
        public int BookingId { get; set; }
        public int PropertyId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal Payments { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateRecorded { get; set; }
        public bool IsBatchLoad { get; set; }
        public int CompanyId { get; set; }
        public bool IsActive { get; set; }
        public decimal RunningBalance { get; set; }
        public List<IncomePayment> PaymentList { get; set; } = new List<IncomePayment>();
        public string Actions { get; set; }
        public string SiteType { get; set; }
        public string BookingCode { get; set; }
        public string PropertyName { get; set; }
        public string GuestName { get; set; }
        public string PaymentDate { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}