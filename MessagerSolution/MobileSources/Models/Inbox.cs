﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.MobileSources.Models
{
    public class Inbox
    {
        public string ThreadId { get; set; }
        public int RenterId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Message { get; set; }
        public string MessageDate { get; set; }
        public string PropertyName { get; set; }
    }
}