﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.MobileSources.Models
{
    public class Message
    {
        public string MessageContent { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsMyMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Source { get; set; }
        public string RecordingUrl { get; set; }
        public string ResourceSid { get; set; }
        public int? Duration { get; set; }
        public int Type { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}