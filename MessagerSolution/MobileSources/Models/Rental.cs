﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.MobileSources.Models
{
    public class Rental
    {
        public int PropertyId { get; set; }
        public int ReservationId { get; set; }
        public string Property { get; set; }
        public string Guest { get; set; }
        public string Status { get; set; }
        public Dictionary<string, string> RentalContracts { get; set; }
    }
}