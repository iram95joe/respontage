﻿using Core.Database.Context;
using Core.Database.Entity;
//using Core.Database.Entity;
using Core.Helper;
using MessagerSolution.JobScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Services;

namespace MessagerSolution.WebServices
{
    /// <summary>
    /// Summary description for Scheduler
    /// </summary>
    [WebService(Namespace = "https://respontage.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Scheduler : System.Web.Services.WebService
    {

        [WebMethod]
        public void Run(int companyId)
        {
            using (var db = new ApplicationDbContext())
            {
                #region Run Video Scheduler
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                List<DeviceAccount> accounts = db.DeviceAccounts.Where(x => x.CompanyId == companyId).ToList();
                foreach (var account in accounts)
                {
                    var site = (Core.Enumerations.DevicesSiteType)account.SiteType;
                    switch (site)
                    {
                        //case Core.Enumerations.DevicesSiteType.Arlo:
                        //    JobScheduler.ArloJobScheduler.Start("arlo-" + account.Id,
                        //        account.Username,
                        //        account.Password,
                        //        account.Id, Url, companyId);
                        //    break;
                        //case Core.Enumerations.DevicesSiteType.Blink:
                        //    JobScheduler.BlinkJobScheduler.Start("blink-" + account.Id,
                        //        account.Username,
                        //        account.Password,
                        //        account.Id, Url, companyId, User.Identity.Name);
                        //    break;
                        case Core.Enumerations.DevicesSiteType.Wyze:
                            JobScheduler.WyzeJobScheduler.Start("wyze-" + account.Id,
                                account.Username,
                                account.Password,
                                account.Id, Url, companyId);
                            break;
                    }
                }
                #endregion

                #region Maintenance and Expense Scheduler
                var WorkerMessageRules = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId join r in db.WorkerMessageRules on p.Id equals r.PropertyId where r.Type == 1 && hc.CompanyId == companyId select r).ToList();
                if (WorkerMessageRules != null)
                {
                    foreach (var rule in WorkerMessageRules)
                    {
                        WorkerNotificationJobScheduler.Start(rule);
                    }
                }
                var emailIds = db.EmailAccounts.Where(x => x.CompanyId == companyId).Select(x => x.Id).ToList();
                foreach (var id in emailIds)
                {
                    EmailJobScheduler.Start(id, companyId);
                }

                MaintenanceJobScheduler.Start(companyId);
                MonthlyRentJobScheduler.Start(companyId);
                NotificationJobScheduler.Start(companyId);
                DisputeJobScheduler.Start(companyId);
                GmailJobScheduler.Start(companyId);
                InvoiceJobScheduler.Start(companyId, System.Web.HttpContext.Current);
                #endregion
            }
        }
        [WebMethod]
        public async System.Threading.Tasks.Task RunScrapper()
        {
            using (HttpClient client = new HttpClient())
            {
                if (SessionHandler.CompanyId != 0)
                {
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(1/*siteType*/));
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("userId", User.Identity.Name);
                    reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                    var reqContent = new FormUrlEncodedContent(reqParams);
                    var result = await client.PostAsync("Home/Index", reqContent);
                    //if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    //{

                    //}
                    GlobalVariables.CompanyId = SessionHandler.CompanyId;
                    GlobalVariables.AirlockIsTriggered = false;
                }
            }
        }
        public async System.Threading.Tasks.Task RunAnalytics()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiAnalyticsUrl);
                var reqParams = new Dictionary<string, string>();
                var reqContent = new FormUrlEncodedContent(reqParams);
                var result = await client.PostAsync("Home/Index", reqContent);
                //if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                //{
                //}
            }
        }
    }
}
