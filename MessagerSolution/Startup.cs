﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MessagerSolution.Startup))]
namespace MessagerSolution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
