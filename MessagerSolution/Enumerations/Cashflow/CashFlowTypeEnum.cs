﻿namespace MessagerSolution.Enumerations.Cashflow
{
    public enum CashFlowTypeEnum
    {
        StartingCash = 1,
        NewInvestment = 2,
        PartnerInvestment = 3,
        WriteOff = 4,
        CashOut = 5,
        OwnerDraw = 6,
        PartnerInvestmentDraw = 7,
        LessOwnerDraw = 8
    }
}