﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MessagerSolution.Enumerations.Commission
{
    public enum CommissionTypeEnum
    {
        [Description("Per Booking")]
        FixedPricePerBooking = 1,
        [Description("Per Night")]
        FixedPricePerNight = 2,
        [Description("% of Revenue")]
        PercentofTotalRevenuefortheMonth = 3,
        [Description("% of Profit")]
        PercentofTotalProfitfortheMonth = 4,
        [Description("Amount after X amount of Total Revenue")]
        AllAmountAfterXAmountofTotalRevenue = 5,
        [Description("Amount after X amount of Total Net Profit")]
        AllAmountAfterXAmountofTotalNetProfit = 6
    }
}