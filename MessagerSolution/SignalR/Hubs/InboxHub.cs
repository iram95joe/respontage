﻿using System;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;

namespace MessagerSolution.SignalR.Hubs
{
    public class OldInboxHub : Hub
    {
        public OldInboxHub()
        {
            //Create a Long running task to do an infinite loop which will sync airbnb/ vrbo inbox message
            // to the clients every 3 seconds.
            var taskTimer = Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    string timeNow = DateTime.Now.ToString();
                    //Sending the server time to all the connected clients on the client method SendServerTime()
                    Clients.All.NewMessages(timeNow);
                    await Task.Delay(40 * 1000);
                }
            }, TaskCreationOptions.LongRunning);
        }
    }
}