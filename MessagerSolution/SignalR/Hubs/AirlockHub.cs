﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.SignalR.Hubs
{
    public class UserHandler
    {
        public static HashSet<string> ConnectedIds = new HashSet<string>();
    }

    [HubName("airlockHub")]
    public class AirlockHub : Hub
    {
        static string userId;
        static VRBO.Models.AirlockViewModel airlockModel;
        VRBO.ScrapeManager sm = new VRBO.ScrapeManager();

        #region DisplayAirlock

        public static void DisplayAirlock(string userId, VRBO.Models.AirlockViewModel airlockModel)
        {
            AirlockHub.userId = userId;
            AirlockHub.airlockModel = airlockModel;

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<AirlockHub>();

            try { hubContext.Clients.User(userId).DisplayAirlock(airlockModel.PhoneNumbers.Values); }
            catch (NullReferenceException err) { }
        }

        #endregion

        #region ProcessAirlock

        public void ProcessAirlock(int selectedPhoneNumber, byte selectedChoice)
        {
            airlockModel.SelectedChoice = (VRBO.Enumerations.AirlockChoice)selectedChoice;
            airlockModel.SelectedPhoneNumber = selectedPhoneNumber.ToString();
            airlockModel = sm.ProcessAirlockSelection(airlockModel);

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<AirlockHub>();
            hubContext.Clients.User(userId).ShowEnterCode();
        }

        #endregion

        #region VerifyCode

        public void VerifyCode(string code)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<AirlockHub>();
            airlockModel.Code = code;
            Tuple<bool, string> result = sm.ProcessAirlock(airlockModel);
            bool success = result.Item1;

            if (success)
            {
                //JobScheduler.ScrapeJobScheduler.Start(userId);
                hubContext.Clients.User(userId).VRBOAirlockResult(true);
            }
            else
                hubContext.Clients.User(userId).VRBOAirlockResult(false);
        }

        #endregion
    }
}