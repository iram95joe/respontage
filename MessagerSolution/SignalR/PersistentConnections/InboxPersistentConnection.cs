﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace MessagerSolution.SignalR.PersistentConnections
{
    public class InboxPersistentConnection : PersistentConnection
    {
        protected override Task OnConnected(IRequest request, string connectionId)
        {
            return Connection.Send(connectionId, "Welcome!");
        }

        protected override Task OnReceived(IRequest request, string connectionId, string data)
        {
            return Connection.Broadcast(data);
        }
    }
}