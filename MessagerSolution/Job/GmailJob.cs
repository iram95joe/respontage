﻿using Core.Database.Context;
using MessagerSolution.Helper;
using MessagerSolution.Service.Gmail;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MessagerSolution.Job
{
    public class GmailJob : IJob
    {
        IJobExecutionContext jobContext;

        #region Execute

        public async Task Execute(IJobExecutionContext context)
        {
            int companyId = context.JobDetail.JobDataMap["companyId"].ToInt();
            jobContext = context;

            await Task.Factory.StartNew(() =>
            {
                using (var db = new ApplicationDbContext())
                {
                    GmailApi.Login(companyId);
                    //var emails = db.EmailAccounts.Where(x => x.CompanyId == companyId).ToList();
                    //foreach (var email in emails)
                    //{
                    //    //Helper.EmailHandler.SyncEmail(email.Id);
                    //    GmailApi.Login();
                    //}
                }
            });
        }

        #endregion
    }
}