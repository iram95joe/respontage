﻿using Core.Helpers;
using Core.Helpers.Devices;
using Core.Models;
using Core.Models.MultiMedia;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class ArloJob : IJob
    {
        IJobExecutionContext jobContext;

        #region Execute

        public async Task Execute(IJobExecutionContext context)
        {
            jobContext = context;

            string email = context.JobDetail.JobDataMap["email"].ToString();
            string password = context.JobDetail.JobDataMap["password"].ToString();
            int accountId = context.JobDetail.JobDataMap["accountId"].ToInt();
            string Url = context.JobDetail.JobDataMap["Url"].ToString();
            int companyId = context.JobDetail.JobDataMap["companyId"].ToInt();
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    using (var sm = new MultiMedia.Arlo.ScrapeManager())
                    {
                        Logger.WriteLog(string.Format("Scheduled scraping on Arlo with username '{0}' has been started, logging in...", email));
                        var result = sm.LogIn(email, password,Url,companyId);
}
                }
                catch (Exception e)
                {

                }
            });
        }

        #endregion
    }
}
