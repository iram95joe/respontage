﻿using Core.Helpers;
using Core.Helpers.Devices;
using Core.Models;
using Core.Models.MultiMedia;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class BlinkJob : IJob
    {
        IJobExecutionContext jobContext;

        #region Execute

        public async Task Execute(IJobExecutionContext context)
        {
            jobContext = context;

            string email = context.JobDetail.JobDataMap["email"].ToString();
            string password = context.JobDetail.JobDataMap["password"].ToString();
            int accountId = context.JobDetail.JobDataMap["accountId"].ToInt();
            string Url = context.JobDetail.JobDataMap["Url"].ToString();
            int companyId = context.JobDetail.JobDataMap["companyId"].ToInt();
            string userId= context.JobDetail.JobDataMap["userId"].ToString();
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    using (var sm = new Blink.ScrapeManager())
                    {
                        sm.Test();
                        //Logger.WriteLog(string.Format("Scheduled scraping on Blink with username '{0}' has been started, logging in...", email));
                        //var result = sm.LogIn(email, password,Url,companyId,true,userId);

                        //if (result.Success)
                        //{  
                        //    sm.ScrapeVideos(result, listingId,accountId,Url);
                        //}
                    }
                }
                catch (Exception e)
                {

                }
            });
        }

        #endregion
    }
}
