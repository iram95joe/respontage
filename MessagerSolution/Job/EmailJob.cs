﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MlkPwgen;
using Quartz;
using S22.Imap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;

namespace MessagerSolution.JobScheduler
{
    public class EmailJob : IJob
    {

        string Content = null;
        string emailFrom = null;
        string emailTo = null;
        string ReplyToEmail = null;
        IJobExecutionContext jobContext;

        ICommonRepository objIList = new CommonRepository();
        ImapClient client;
        AutoResetEvent reconnectEvent = new AutoResetEvent(false);
        int emailId;
        int companyId;
        #region Execute
        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            jobContext = context;
            emailId = context.JobDetail.JobDataMap["emailId"].ToInt();
            companyId = context.JobDetail.JobDataMap["companyId"].ToInt();

            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    while (true)
                    {
                        InitializeClient();
                        reconnectEvent.WaitOne();
                    }
                }
                finally
                {
                    if (client != null)
                        client.Dispose();
                }
            });
        }

        #endregion
        void InitializeClient()
        {
            try
            {
                if (client != null)
                    client.Dispose();
                using(var db = new ApplicationDbContext())
                {
                    var account =db.EmailAccounts.Where(x => x.Id == emailId).FirstOrDefault();
                    if (account != null)
                    {
                        client = new ImapClient("imap.gmail.com", 993, account.Email, Security.Decrypt(account.Password), AuthMethod.Login, true);
                        // Setup event handlers.
                        client.NewMessage += new EventHandler<IdleMessageEventArgs>(client_NewMessage);
                        client.IdleError += new EventHandler<IdleErrorEventArgs>(client_IdleError);
                    }
                }
             
            }
            catch (Exception e)
            {
            }
            //}

        }
        void client_IdleError(object sender, IdleErrorEventArgs e)
        {
            Console.Write("An error occurred while idling: ");
            Console.WriteLine(e.Exception.Message);
            reconnectEvent.Set();
        }

        void client_NewMessage(object sender, IdleMessageEventArgs e)
        {
            MailMessage msg = client.GetMessage(e.MessageUID);
            ParseMessage(msg);
        }

        #region Record Email
        #endregion



        public void ParseMessage(MailMessage Message)
        {
            emailFrom = Message.From.Address.ToSafeString();
            emailTo = Message.To.ToSafeString();
            var renter = GetRenter(emailFrom);
            if (renter != null)
            {
                if (Message.ReplyToList.Count > 0)
                {
                    ReplyToEmail = Message.ReplyToList[0].ToSafeString();
                }
                var subject = Message.Subject.Replace("Fwd:", "").Trim();
                var date = Message.Headers["Date"].ToDateTime();
                //Console.WriteLine("Received at : {0}", date);
                //var dataStream = Message.AlternateViews[0].ContentStream;
                //byte[] byteBuffer = new byte[dataStream.Length];
                //string altbody = System.Text.Encoding.UTF8.GetString(byteBuffer, 0, dataStream.Read(byteBuffer, 0, byteBuffer.Length));
                //altbody = Regex.Replace(altbody, @"m_\-[\d]*", string.Empty);
                //altbody = Regex.Replace(altbody, @"m_[0-9]*", string.Empty);

                //XmlDocument xd = Core.Sites.Gmail.ScrapeManager.ConvertToXml(ref altbody);
                //Content = altbody;
                //var message = xd.SelectSingleNode("//div[contains(@dir,'ltr')]").GetInnerTextFromNode();
                var message = Message.Body;
                var attachments = new List<string>();
                if (Message.Attachments.Count > 0)
                {
                    foreach (var attachment in Message.Attachments)
                    {
                        string filename = "/Images/EmailAttachment/" + attachment.Name;
                        var path = System.AppDomain.CurrentDomain.BaseDirectory + "/Images/EmailAttachment/";
                        System.IO.Directory.CreateDirectory(path);
                        var copyPath = path + attachment.Name;
                        using (var destination = File.Create(copyPath))
                            attachment.ContentStream.CopyTo(destination);

                        attachments.Add(filename);
                    }
                }
                SaveEmail(renter,emailFrom, emailTo, message,date, attachments);
            }
        }

        void SaveEmail(Renter renter, string from, string to, string message, DateTime date, List<string> attachments)
        {
            using (var db = new ApplicationDbContext())
            {
                var thread = db.CommunicationThreads.Where(x => x.CompanyId == companyId && renter.Id == x.RenterId && x.IsRenter).FirstOrDefault();
                CommunicationThread workerThread;
                if (thread == null)
                {
                    workerThread = new CommunicationThread();
                    workerThread.RenterId = renter.Id;
                    workerThread.ThreadId = PasswordGenerator.Generate(20);
                    workerThread.CompanyId = companyId;
                    db.CommunicationThreads.Add(workerThread);
                    db.SaveChanges();
                }
                else
                {
                    workerThread = thread;
                    workerThread.IsEnd = false;
                    db.Entry(workerThread).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                var Email = new CommunicationEmail()
                {
                    From = from,
                    To = to,
                    IsMyMessage = false,
                    Message = message,
                    ThreadId = workerThread.ThreadId,
                    CreatedDate = date
                };
                db.CommunicationEmails.Add(Email);
                db.SaveChanges();
                if (attachments.Count > 0)
                {
                    foreach (var attachment in attachments)
                    {
                        db.EmailAttachments.Add(new CommunicationEmailAttachment()
                        {
                            CommunicationEmailId = Email.Id,
                            FileUrl = attachment
                        });
                        db.SaveChanges();
                    }
                }
            }
        }
        Renter GetRenter(string email)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var renter = (from r in db.Renters join reservation in db.Inquiries on r.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where property.CompanyId == companyId && !property.IsParentProperty && property.SiteType == 0 && r.Email == email select r).FirstOrDefault();
                    return renter;
                }
            }
            catch
            {
                return null;
            }

        }
    }
}