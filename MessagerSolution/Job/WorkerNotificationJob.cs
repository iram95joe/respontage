﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MlkPwgen;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class WorkerNotificationJob : IJob
    {
        IJobExecutionContext jobContext;
        ICommonRepository commonRepo = new CommonRepository();
        #region Execute
        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            jobContext = context;
            int ruleId = context.JobDetail.JobDataMap["ruleId"].ToInt();
            int templateId = context.JobDetail.JobDataMap["templateId"].ToInt();
            int propertyId = context.JobDetail.JobDataMap["propertyId"].ToInt();
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                        string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                        TwilioClient.Init(accountSid, authToken);
                        var property = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault();
                        var parentproperty = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ChildPropertyId == property.Id && pc.CompanyId == SessionHandler.CompanyId && p.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p).FirstOrDefault();
                        //db.Properties.Where(x => x.Id == property.ParentPropertyId).FirstOrDefault();
                        var template = db.WorkerTemplateMessages.Where(x => x.Id == templateId).FirstOrDefault();

                        var assignments = db.WorkerAssignments.Where(x => x.PropertyId == propertyId && DbFunctions.TruncateTime(x.StartDate) == DateTime.Today).ToList();
                        var phoneNumber = "";
                        if (assignments != null)
                        {
                            foreach (var assignment in assignments)
                            {

                                var reservation = db.Inquiries.Where(x => x.Id == assignment.ReservationId).FirstOrDefault();
                                var worker = db.Workers.Where(x => x.Id == assignment.WorkerId).FirstOrDefault();
                                var workerContact = db.WorkerContactInfos.Where(x => x.WorkerId == worker.Id).FirstOrDefault();
                                var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == property.TwilioNumberId).FirstOrDefault();
                                var guest = db.Guests.Where(x => x.Id == reservation.GuestId).FirstOrDefault();
                                if (twilioNumber != null)
                                {
                                    phoneNumber = twilioNumber.PhoneNumber;
                                }
                                else
                                {
                                    var host = db.Hosts.Where(x => x.Id == property.HostId).FirstOrDefault();
                                    twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == host.TwilioNumberId).FirstOrDefault();
                                    if (twilioNumber != null)
                                    {
                                        phoneNumber = twilioNumber.PhoneNumber;
                                    }
                                }
                                var workerThread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && x.WorkerId == assignment.WorkerId).FirstOrDefault();
                                if (workerThread == null)
                                {
                                    workerThread = new CommunicationThread();
                                    db.CommunicationThreads.Add(workerThread);
                                    workerThread.WorkerId = worker.Id;
                                    workerThread.ThreadId = PasswordGenerator.Generate(20);
                                    workerThread.CompanyId = SessionHandler.CompanyId;
                                    db.CommunicationThreads.Add(workerThread);
                                    db.SaveChanges();
                                }
                                var message = template.Message;
                                var replacements = new Dictionary<string, string>
                            {
                                { "[Guest Name]", guest.Name },
                                { "[Worker Name]", worker.Firstname },
                                { "[Parent Property]", parentproperty!=null?parentproperty.Name:property.Name },
                                { "[Listing Name]", property.Name },
                                { "[Guest Count]", reservation.GuestCount.ToString()},
                                { "[Check In Date]", reservation.CheckInDate.ToDateTime().ToString("MMM dd,yyyy") },
                                { "[Check Out Date]", reservation.CheckOutDate.ToDateTime().ToString("MMM dd,yyyy") },
                                { "[Assignment Start]", assignment.StartDate.ToDateTime().ToString("MMM dd,yyyy hhtt")},
                                { "[Assignment End]", assignment.EndDate.ToDateTime().ToString("MMM dd,yyyy hhtt") }
                            };
                                foreach (var replacement in replacements)
                                {
                                    message = message.Replace(replacement.Key, replacement.Value);
                                }

                                var smsMessage = MessageResource.Create(
                                 from: new Twilio.Types.PhoneNumber(phoneNumber),
                                 to: new Twilio.Types.PhoneNumber(workerContact.Value),
                                 body: message);

                                CommunicationSMS sms = new CommunicationSMS
                                {
                                    ThreadId = workerThread.ThreadId,
                                    Message = message,
                                    From = phoneNumber,
                                    To = workerContact.Value,
                                    CreatedDate = DateTime.Now,
                                    UserId = 1

                                };
                                db.CommunicationSMS.Add(sms);
                                db.SaveChanges();

                            }
                        }
                    }
                }
                catch (Exception err)
                {
                }
            });
        }

        #endregion
    }
}