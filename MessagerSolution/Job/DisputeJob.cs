﻿using Core.Database.Context;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Job
{
    public class DisputeJob : IJob
    {
        IJobExecutionContext jobContext;
        private readonly Service.PayPalService _payPalService = new Service.PayPalService();
        private readonly Service.SquareService _squarePaymentService = new Service.SquareService();
        private readonly Service.Stripe _stripePaymentService = new Service.Stripe();

        #region Execute

        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            jobContext = context;

            var companyId = context.JobDetail.JobDataMap["CompanyId"].ToInt();
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                using (var db = new ApplicationDbContext())
                {
                    List<string> paymentIds = new List<string>();
                    var paymentAccounts = db.PaymentAccount.Where(x => x.CompanyId == companyId).ToList();
                    foreach (var account in paymentAccounts)
                    {
                        var accType = (Core.Enumerations.PaymentRequestMethod)account.PaymentMethod;
                        switch (accType)
                        {
                            case Core.Enumerations.PaymentRequestMethod.Paypal:
                                //Paypal Not working as of now.
                                _payPalService.ListOfDispute(account.Id);
                                break;
                            case Core.Enumerations.PaymentRequestMethod.Stripe:
                                _stripePaymentService.ListOfDispute(account.Id);
                                break;
                            case Core.Enumerations.PaymentRequestMethod.Square:
                                paymentIds = _squarePaymentService.ListOfDispute(account.Id);
                                break;
                        }
                        foreach (var paymentId in paymentIds)
                        {
                            var invoice = db.Invoices.Where(x => x.SitePaymentId == paymentId).FirstOrDefault();
                            var batch = db.IncomeBatchPayments.Where(x => x.InvoiceId == invoice.Id).FirstOrDefault();
                            batch.IsRejected = true;
                            var payments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == batch.Id).ToList();
                            payments.ForEach(x => x.IsRejected = true);
                            db.SaveChanges();
                        }
                    }

                }
            });
        }
        #endregion
    }
}