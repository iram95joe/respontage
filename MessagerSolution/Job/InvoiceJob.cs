﻿using Core.Database.Context;
using MessagerSolution.Helper;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class InvoiceJob: IJob
    {
        IJobExecutionContext jobContext;
        #region Execute
        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {      
            jobContext = context;
            var companyId = context.JobDetail.JobDataMap["CompanyId"].ToInt();
            var httpContext = (System.Web.HttpContext)context.JobDetail.JobDataMap["HttpContext"];
            InvoiceSchedule invoiceSchedule = new InvoiceSchedule(httpContext);
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {

                using (var db = new ApplicationDbContext())
                {
                    var schedules = db.InvoiceSchedules.Where(x => x.CompanyId == companyId).ToList();
                    foreach(var schedule in schedules)
                    { var now = DateTime.Now;
                        var dayOfMonth = schedule.DayOfMonth;
                        var lastDay = DateTime.DaysInMonth(now.Year, now.Month);
                        if (lastDay < dayOfMonth)
                            dayOfMonth = lastDay;

                        if (dayOfMonth == DateTime.Now.Day)
                        {
                            var propertyIds = schedule.PropertyIds.Split(',').Select(Int32.Parse).ToList();
                            foreach (var propertyId in propertyIds)
                            {
                                var reservation = invoiceSchedule.GetCurrentReservation(propertyId);
                                if (reservation != null)
                                {var invoice =db.Invoices.Where(x => x.BookingId==reservation.Id&& x.InvoiceScheduleId == schedule.Id && x.DateCreated.Month == now.Month && x.DateCreated.Year == now.Year).FirstOrDefault();
                                    if(invoice==null)
                                    invoiceSchedule.CreateInvoice(new Models.Invoice.InvoiceViewModel() {InvoiceScheduleId =schedule.Id, RequestPaymentJsonString = invoiceSchedule.GetUnpaidTransaction(reservation.Id), PaymentMethod = (Core.Enumerations.PaymentRequestMethod)schedule.PaymentMethod, BookingId = reservation.Id, EmailTemplateId = schedule.EmailTemplateId, InvoiceTemplateId = schedule.InvoiceTemplateId });
                                }
                            }
                        }
                    }

                }

            });
        }

        #endregion
    }
}