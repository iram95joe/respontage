﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class WyzeJob : IJob
    {
        IJobExecutionContext jobContext;

        #region Execute

        public async Task Execute(IJobExecutionContext context)
        {
            jobContext = context;

            string email = context.JobDetail.JobDataMap["email"].ToString();
            string password = context.JobDetail.JobDataMap["password"].ToString();
            int accountId = context.JobDetail.JobDataMap["accountId"].ToInt();
            string Url = context.JobDetail.JobDataMap["Url"].ToString();
            int companyId = context.JobDetail.JobDataMap["companyId"].ToInt();
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    using (var sm = new Wyze.ScrapeManager())
                    {
                        var result = sm.LogIn(email, password, Url,companyId);
                    }
                }
                catch (Exception e)
                {

                }
            });
        }

        #endregion
    }
}