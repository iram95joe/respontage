﻿using Core.Database.Context;
using Core.Enumerations;
using MessagerSolution.Helper.Scheduler;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Job
{
    public class NotificationJob : IJob
    {
        IJobExecutionContext jobContext;
        NotificationSchedule Schedule;
        #region Execute

        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            jobContext = context;
            var companyId = context.JobDetail.JobDataMap["CompanyId"].ToInt();
            Schedule = new NotificationSchedule();
            Schedule.Run(companyId);

        }
        #endregion
    }
}