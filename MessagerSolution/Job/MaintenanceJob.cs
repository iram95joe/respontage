﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using MessagerSolution.Helper;
using MessagerSolution.Helper.Scheduler;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class MaintenanceJob : IJob
    {
        IJobExecutionContext jobContext;
        ExpenseMaintenanceSchedule Schedule;
        #region Execute

        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            Schedule = new ExpenseMaintenanceSchedule();
            jobContext = context;

            var companyId = context.JobDetail.JobDataMap["CompanyId"].ToInt();
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {

                using (var db = new ApplicationDbContext())
                {
                    //TwilioSMS.Send(string.Format("Maintenance Scheduler Start {0}", DateTime.Now.ToString("MMMM dd,yyyy hh:mm tt")));
                    db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    db.Database.ExecuteSqlCommand("SET DEADLOCK_PRIORITY NORMAL;");
                    var maintenances = db.PropertyScheduledExpenses.Where(x => x.IsDataCreated == false && x.CompanyId == companyId && x.Frequency != (int)MaintenanceFrequency.Onetime).OrderBy(x => x.Interval).ToList();
                    foreach (var entry in maintenances)
                    {
                        try
                        {
                            var maintenance = db.PropertyScheduledExpenses.Where(x => x.Id == entry.Id).FirstOrDefault();
                            var frequency = (MaintenanceFrequency)maintenance.Frequency;
                            var duedate = DateTime.Now;
                            var feeday = maintenance.RecurringDate.Day;
                            var date = DateTime.Now;
                            var lastdayMonth = DateTime.DaysInMonth(date.Year, date.Month);
                            if (lastdayMonth < feeday)
                            {
                                feeday = lastdayMonth;
                            }
                            Expense temp = null;

                            #region Daily
                            if (frequency == MaintenanceFrequency.Daily)
                            {
                                var start = date.Date;
                                var end = start.AddDays(30);
                                if (start >= maintenance.RecurringDate)
                                {
                                    for (DateTime counter = start; counter <= end; counter = counter.AddDays(maintenance.Interval))
                                    {
                                        if (counter >= maintenance.RecurringDate && (maintenance.RecurringEndDate != null ? counter <= maintenance.RecurringEndDate : true))
                                        {
                                            temp = db.Expenses.Where(x =>
                                           x.DueDate == counter &&
                                          x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                                            //For Testing
                                            Schedule.CreateExpense(maintenance, temp, counter);
                                        }
                                    }
                                }

                            }
                            #endregion

                            #region Weekly
                            else if (frequency == MaintenanceFrequency.Weekly)
                            {
                                //Due date is recurring date
                                duedate = maintenance.RecurringDate;

                                double diff = (date.Date - duedate.Date).TotalDays / 7;
                                var numberOf = Math.Floor(diff);
                                List<DateTime> daysInWeek = new List<DateTime>();
                                DayOfWeek Day = numberOf > -1 ? date.DayOfWeek : duedate.DayOfWeek;
                                int Days = Day - DayOfWeek.Sunday; //here you can set your Week Start Day

                                DateTime WeekStartDate = numberOf > -1 ? date.AddDays(-Days) : duedate.AddDays(-Days);
                                for (int i = 0; i < 7; i++)
                                {
                                    daysInWeek.Add(WeekStartDate.AddDays(i).Date);
                                }

                                var dayofWeeK = maintenance.RecurringDate.DayOfWeek;
                                duedate = daysInWeek.Where(x => x.DayOfWeek == dayofWeeK).Select(x => x.Date).FirstOrDefault();
                                //Check if the current week is within interval of weeks
                                diff = (duedate.Date - maintenance.RecurringDate.Date).TotalDays / 7;
                                numberOf = Math.Floor(diff);
                                var result = numberOf % maintenance.Interval;
                                if (result == 0 || numberOf == -1)
                                {
                                    temp = db.Expenses.Where(x =>
                                    daysInWeek.Contains(x.DueDate) &&
                                    x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                                    //Data must be create 1 week before recurring date
                                    var diffday = (duedate.Date - date.Date).Days;
                                    if (duedate.Date >= maintenance.RecurringDate.Date && diffday <= 7)
                                    {
                                        Schedule.CreateExpense(maintenance, temp, duedate);

                                        #region Weekly Advance Data
                                        if (frequency == MaintenanceFrequency.Weekly && (maintenance.RecurringEndDate != null ? duedate.AddDays(7 * maintenance.Interval) <= maintenance.RecurringEndDate : true))
                                        {
                                            duedate = duedate.AddDays(7 * maintenance.Interval);
                                            daysInWeek = new List<DateTime>();
                                            Day = date.DayOfWeek;
                                            Days = Day - DayOfWeek.Sunday; //here you can set your Week Start Day

                                            WeekStartDate = duedate.AddDays(-Days);
                                            for (int i = 0; i < 7; i++)
                                            {
                                                daysInWeek.Add(WeekStartDate.AddDays(i).Date);
                                            }
                                            dayofWeeK = maintenance.RecurringDate.DayOfWeek;
                                            duedate = daysInWeek.Where(x => x.DayOfWeek == dayofWeeK).Select(x => x.Date).FirstOrDefault();
                                            var advanceTemp = db.Expenses.Where(x =>
                                            daysInWeek.Contains(x.DueDate) &&
                                            x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                                            //For Testing
                                            Schedule.CreateExpense(maintenance, advanceTemp, duedate);

                                        }

                                        #endregion
                                    }
                                }
                            }
                            #endregion

                            #region Monthly
                            else if (frequency == MaintenanceFrequency.Monthly)
                            {
                                var intervalMonth = maintenance.Interval;
                                double dtspan = 0;
                                //Due date is recurring date
                                duedate = maintenance.RecurringDate;
                                if (date > duedate)
                                {
                                    if (intervalMonth != 1)
                                    {
                                        do
                                        {
                                            dtspan = Math.Abs(new DateTime(duedate.Year, duedate.Month, 1).Subtract(new DateTime(date.Year, date.Month, 1)).Days / (365.25 / 12));

                                            if (Math.Floor(dtspan) >= (intervalMonth - 1))
                                            {
                                                //
                                                duedate = duedate.AddMonths(intervalMonth);
                                            }
                                        } while (dtspan >= (intervalMonth - 1));
                                        //It will set the last day of month for duedate.ie when recurring day is 31 then month is february then last day will be 28 or 29
                                        var recurringday = maintenance.RecurringDate.Day;
                                        var lastDayMonth = DateTime.DaysInMonth(duedate.Year, duedate.Month);
                                        if (lastDayMonth < recurringday)
                                        {
                                            recurringday = lastDayMonth;
                                        }
                                        duedate = new DateTime(duedate.Year, duedate.Month, recurringday);
                                    }
                                    else
                                    {
                                        if (date > duedate)
                                        {
                                            var recurringday = maintenance.RecurringDate.Day;
                                            var lastDayMonth = DateTime.DaysInMonth(date.Year, date.Month);
                                            if (lastDayMonth < recurringday)
                                            {
                                                recurringday = lastDayMonth;
                                            }
                                            duedate = new DateTime(date.Year, date.Month, recurringday);
                                        }
                                    }

                                }
                                temp = db.Expenses.Where(x =>
                               (x.DueDate.Month == duedate.Month && x.DueDate.Year == duedate.Year) &&
                               x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                            }
                            #endregion

                            #region Ontime
                            else if (frequency == MaintenanceFrequency.Onetime)
                            {
                                duedate = maintenance.RecurringDate;
                                temp = db.Expenses.Where(x =>
                                x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                                maintenance.IsDataCreated = true;
                                db.Entry(maintenance).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            #endregion
                            //Get due date on year
                            #region Yearly
                            else if (frequency == MaintenanceFrequency.Yearly)
                            {
                                duedate = maintenance.RecurringDate;
                                var result = (date.AddMonths(1).Year - duedate.Year) % maintenance.Interval;
                                if (result == 0)
                                {
                                    int year = date.Year;
                                    if (date.AddMonths(1).Year > date.Year && date.Month != maintenance.RecurringDate.Month)
                                    {
                                        year = date.AddMonths(1).Year;
                                    }
                                    var lastdayMonthyearly = DateTime.DaysInMonth(year, maintenance.RecurringDate.Month);
                                    var feedayyearly = maintenance.RecurringDate.Day;
                                    if (lastdayMonthyearly < feedayyearly)
                                    {
                                        feedayyearly = lastdayMonthyearly;
                                    }
                                    duedate = new DateTime(year, maintenance.RecurringDate.Month, feedayyearly);
                                }
                                temp = db.Expenses.Where(x =>
                                  (x.DueDate.Year == duedate.Date.Year) &&
                                  x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                            }
                            #endregion

                            #region Create Expenses entry
                            var daydiff = (new DateTime(duedate.Year, duedate.Month, 1) - new DateTime(date.Year, date.Month, 1)).Days;
                            if (/*temp == null &&*/
                             (((((duedate.Date >= maintenance.RecurringDate.Date && frequency == MaintenanceFrequency.Monthly))
                             || (duedate.Year >= maintenance.RecurringDate.Year && frequency == MaintenanceFrequency.Yearly))
                             && (daydiff <= 31))
                             && (maintenance.RecurringEndDate != null ? duedate <= maintenance.RecurringEndDate : true)))
                            {
                                //For Testing
                                Schedule.CreateExpense(maintenance, temp, duedate);
                                #region Monthly Advance Data
                                //Create advance 1 month
                                var advancediff = (new DateTime(duedate.AddMonths(maintenance.Interval).Year, duedate.AddMonths(maintenance.Interval).Month, 1) - new DateTime(date.Year, date.Month, 1)).Days;
                                if (frequency == MaintenanceFrequency.Monthly && advancediff <= 31 && (maintenance.RecurringEndDate != null ? duedate.AddMonths(1) <= maintenance.RecurringEndDate : true))
                                {
                                    var recurringday = maintenance.RecurringDate.Day;
                                    var tempduedate = duedate.AddMonths(maintenance.Interval);
                                    var lastDayMonth = DateTime.DaysInMonth(tempduedate.Year, tempduedate.Month);
                                    if (lastDayMonth < recurringday)
                                    {
                                        recurringday = lastDayMonth;
                                    }
                                    duedate = new DateTime(tempduedate.Year, tempduedate.Month, recurringday);
                                    var advenceTemp = db.Expenses.Where(x =>
                                                    (x.DueDate.Month == duedate.Month && x.DueDate.Year == duedate.Year) &&
                                                      x.PropertyScheduledExpenseId == maintenance.Id).FirstOrDefault();
                                    //For Testing
                                    Schedule.CreateExpense(maintenance, advenceTemp, duedate);
                                }
                                #endregion
                            }

                            #endregion
                            //}
                        }
                        catch (Exception err)
                        {
                        }

                    }

                }

            });
        }

        #endregion
    }
}