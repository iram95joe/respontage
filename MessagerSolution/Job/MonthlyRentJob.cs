﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using MessagerSolution.SignalR.Hubs;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using System.Data.Entity;

namespace MessagerSolution.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class MonthlyRentJob : IJob
    {
        VRBO.Models.ScrapeResult sr;
        IJobExecutionContext jobContext;

        #region Execute

        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            jobContext = context;

            var companyId = context.JobDetail.JobDataMap["CompanyId"].ToInt();
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    db.Database.ExecuteSqlCommand("SET DEADLOCK_PRIORITY NORMAL;");
                    var reservations = (from r in db.Inquiries join p in db.Properties on r.PropertyId equals p.ListingId where r.IsActive && p.SiteType == 0 && p.CompanyId == companyId select r).ToList();
                    foreach (var reservation in reservations)
                    {
                        try
                        {
                            var date = DateTime.Now;
                            var startDate = reservation.RecurringRentStartDate.ToDateTime();
                            var endDate = DateTime.Now.AddMonths(1);
                            var property = db.Properties.Where(x => x.ListingId == reservation.PropertyId).FirstOrDefault();
                            //Check if all rent is create base on date range
                            for (var start = startDate; start <= endDate; start = start.AddMonths(1))
                            {
                                var recurringday = reservation.RecurringRentStartDate.Value.Day;
                                var lastDayMonth = DateTime.DaysInMonth(start.Year, start.Month);
                                if (lastDayMonth < recurringday)
                                {
                                    recurringday = lastDayMonth;
                                }
                                if ((reservation.CheckOutDate == null && reservation.IsMonthToMonth) || (reservation.CheckOutDate != null && reservation.IsFixedMonthToMonth))
                                {
                                    Income income = new Income();
                                    income.Description = "Rent";
                                    income.PropertyId = Convert.ToInt32(property.Id);
                                    income.DueDate = new DateTime(start.Year, start.Month, recurringday);
                                    var tempAmount = db.MonthlyRents.Where(x => x.EffectiveDate <= income.DueDate && x.BookingId == reservation.Id).OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
                                    income.Amount = db.MonthlyRents.Where(x => x.EffectiveDate <= income.DueDate && x.BookingId == reservation.Id).OrderByDescending(x => x.EffectiveDate).FirstOrDefault().Amount;
                                    income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                                    income.Status = 1;
                                    income.CompanyId = companyId;
                                    income.DateRecorded = DateTime.UtcNow;
                                    income.IsBatchLoad = false;
                                    income.BookingId = reservation.Id;
                                    income.IsActive = true;
                                    var temp = db.Income.Where(x => (x.Description == "Rent" || x.Description.Contains("Post Dated") || x.Description == "Advance Payment") && (x.DueDate.Month == income.DueDate.Month && x.DueDate.Year == income.DueDate.Year) && x.BookingId == reservation.Id).FirstOrDefault();
                                    if (((reservation.CheckOutDate != null ? income.DueDate <= reservation.CheckOutDate : true) || (reservation.IsFixedMonthToMonth || reservation.IsMonthToMonth)))
                                    {
                                        if (temp == null)
                                        {
                                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), reservation.Id);
                                            db.Income.Add(income);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            if (temp.Description != income.Description || temp.DueDate != income.DueDate || temp.Amount != income.Amount)
                                            {
                                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction changes Description: {0} to {1} Due Date:{2} to {3} Amount:{4} to {5} ", temp.Description, income.Description, temp.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount, income.Amount), reservation.Id);
                                                temp.Amount = income.Amount;
                                                //temp.Description = income.Description;
                                                temp.DueDate = income.DueDate;
                                                temp.IsActive = true;
                                                db.Entry(temp).State = EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                                //Create late fee entry when unpaid or partially paid
                                var thisMonthIncome = db.Income.Where(x => (x.DueDate.Month == date.Month && x.DueDate.Year == date.Year) && (x.Description == "Rent") && x.BookingId == reservation.Id).FirstOrDefault();
                                if (thisMonthIncome != null)
                                {
                                    var payment = db.IncomePayment.Where(x => x.IncomeId == thisMonthIncome.Id).ToList();
                                    decimal totalPayment = 0;
                                    if (payment != null)
                                    {
                                        totalPayment = payment.Sum(x => x.Amount);
                                    }
                                    if (reservation.IsLateFees)
                                    {
                                        if ((Math.Abs(date.Day - recurringday) >= reservation.LateFeeApplyDays.Value) && (totalPayment != thisMonthIncome.Amount))
                                        {
                                            Income income = new Income();
                                            income.Description = thisMonthIncome.DueDate.ToString("MMMM yyyy") + " Late Fee";
                                            income.PropertyId = Convert.ToInt32(property.Id);
                                            income.Amount = decimal.Parse(reservation.LateFeeAmount.ToString());
                                            income.DueDate = thisMonthIncome.DueDate.AddDays(reservation.LateFeeApplyDays.Value);
                                            income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                                            income.Status = 1;
                                            income.CompanyId = companyId;
                                            income.DateRecorded = DateTime.UtcNow;
                                            income.IsBatchLoad = false;
                                            income.BookingId = reservation.Id;
                                            income.IsActive = true;
                                            var temp = db.Income.Where(x => x.Description == income.Description && x.BookingId == reservation.Id).FirstOrDefault();
                                            if (temp == null)
                                            {
                                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), reservation.Id);
                                                db.Income.Add(income);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }



                            var additionalFees = db.BookingAdditionalFees.Where(x => x.BookingId == reservation.Id && x.Type == 2).ToList();
                            foreach (var fee in additionalFees)
                            {
                                Income temp = null;
                                var duedate = DateTime.Now;
                                var dt = DateTime.Now;
                                var frequency = (AdditionalFeeFrequency)fee.Frequency;
                                if (frequency == AdditionalFeeFrequency.Daily)
                                {
                                    temp = db.Income.Where(x =>
                                   x.DueDate == duedate
                                    && x.BookingAdditionalFeeId == fee.Id).FirstOrDefault();
                                }
                                //else if (frequency == AdditionalFeeFrequency.Weekly) { }
                                else if (frequency == AdditionalFeeFrequency.Monthly)
                                {
                                    duedate = fee.DueOn;
                                    if (date > duedate)
                                    {
                                        var feeday = fee.DueOn.Day;
                                        var lastDayMonth = DateTime.DaysInMonth(date.Year, date.Month);
                                        if (lastDayMonth < feeday)
                                        {
                                            feeday = lastDayMonth;
                                        }
                                        duedate = new DateTime(date.Year, date.Month, feeday);
                                    }
                                    temp = db.Income.Where(x =>
                                   (x.DueDate.Month == duedate.Month && x.DueDate.Year == duedate.Year)
                                   && x.BookingAdditionalFeeId == fee.Id
                                   ).FirstOrDefault();
                                }
                                //else if (frequency == AdditionalFeeFrequency.Onetime) {
                                // duedate = fee.DueOn;
                                // temp = db.Income.Where(x =>
                                // x.DueDate == duedate &&
                                // x.Description == fee.Description &&
                                // x.PropertyId == property.Id).FirstOrDefault();
                                //}
                                else if (frequency == AdditionalFeeFrequency.Yearly)
                                {
                                    duedate = fee.DueOn;
                                    if (dt.Year >= fee.DueOn.Year)
                                    {
                                        int year = dt.Year;
                                        if (dt.AddMonths(1).Year > dt.Year && dt.Month != fee.DueOn.Month)
                                        {
                                            year = dt.AddMonths(1).Year;
                                        }
                                        var lastdayMonthyearly = DateTime.DaysInMonth(year, fee.DueOn.Month);
                                        var feedayyearly = fee.DueOn.Day;
                                        if (lastdayMonthyearly < feedayyearly)
                                        {
                                            feedayyearly = lastdayMonthyearly;
                                        }
                                        duedate = new DateTime(year, fee.DueOn.Month, feedayyearly);
                                    }
                                    temp = db.Income.Where(x =>
                                   (x.DueDate.Year == duedate.Year) &&
                                   x.BookingAdditionalFeeId == fee.Id).FirstOrDefault();
                                }
                                var daydiff = (new DateTime(duedate.Year, duedate.Month, 1) - new DateTime(dt.Year, dt.Month, 1)).Days;
                                if (temp == null &&
                                    (((frequency == AdditionalFeeFrequency.Monthly && (reservation.IsFixedMonthToMonth || reservation.IsMonthToMonth) && (daydiff <= 31))
                                    || (frequency == AdditionalFeeFrequency.Yearly && (reservation.IsFixedMonthToMonth || reservation.IsMonthToMonth)) && (daydiff <= 31))
                                    && ((reservation.IsFixedMonthToMonth == false && reservation.IsMonthToMonth == false) ? (duedate <= reservation.CheckOutDate) : true)
                                    ))
                                {
                                    Income income = new Income();
                                    income.Description = fee.Description;
                                    income.PropertyId = Convert.ToInt32(property.Id);
                                    income.Amount = fee.Amount;
                                    income.DueDate = duedate;
                                    income.IncomeTypeId = (int)Core.Enumerations.IncomeType.TenantBill;
                                    income.Status = 1;
                                    income.CompanyId = companyId;
                                    income.DateRecorded = DateTime.UtcNow;
                                    income.IsBatchLoad = false;
                                    income.BookingId = reservation.Id;
                                    income.IsActive = true;
                                    income.BookingAdditionalFeeId = fee.Id;
                                    db.Income.Add(income);
                                    db.SaveChanges();
                                    Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), reservation.Id);

                                    //Create advance bills
                                    var advancediff = (new DateTime(duedate.AddMonths(1).Year, duedate.AddMonths(1).Month, 1) - new DateTime(dt.Year, dt.Month, 1)).Days;
                                    if ((frequency == AdditionalFeeFrequency.Monthly && (reservation.IsFixedMonthToMonth || reservation.IsMonthToMonth) && advancediff <= 31)
                                    && ((reservation.IsFixedMonthToMonth == false && reservation.IsMonthToMonth == false) ? (duedate.AddMonths(1) <= reservation.CheckOutDate) : true))
                                    {
                                        //Create 1 month advance utilities
                                        duedate = duedate.AddMonths(1);
                                        var recurringday = fee.DueOn.Day;
                                        var tempduedate = duedate;
                                        var lastDayMonth = DateTime.DaysInMonth(tempduedate.Year, tempduedate.Month);
                                        if (lastDayMonth < recurringday)
                                        {
                                            recurringday = lastDayMonth;
                                        }
                                        var advanceTemp = db.Income.Where(x =>
                                       (x.DueDate.Month == duedate.Month && x.DueDate.Year == duedate.Year)
                                       && x.BookingAdditionalFeeId == fee.Id
                                         ).FirstOrDefault();
                                        if (advanceTemp == null)
                                        {
                                            duedate = new DateTime(tempduedate.Year, tempduedate.Month, recurringday);
                                            Income advanceIncome = new Income();
                                            advanceIncome.Description = fee.Description;
                                            advanceIncome.PropertyId = Convert.ToInt32(property.Id);
                                            advanceIncome.Amount = fee.Amount;
                                            advanceIncome.DueDate = duedate;
                                            advanceIncome.IncomeTypeId = (int)Core.Enumerations.IncomeType.TenantBill;
                                            advanceIncome.Status = 1;
                                            advanceIncome.CompanyId = companyId;
                                            advanceIncome.DateRecorded = DateTime.UtcNow;
                                            advanceIncome.IsBatchLoad = false;
                                            advanceIncome.BookingId = reservation.Id;
                                            advanceIncome.IsActive = true;
                                            advanceIncome.BookingAdditionalFeeId = fee.Id;
                                            db.Income.Add(advanceIncome);
                                            db.SaveChanges();
                                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", advanceIncome.Description, advanceIncome.Amount, advanceIncome.DueDate.ToDateTime().ToString("MMM dd,yyyy")), reservation.Id);
                                        }
                                        else
                                        {
                                            if (advanceTemp.Amount != fee.Amount || advanceTemp.Description != fee.Description)
                                            {
                                                advanceTemp.Amount = fee.Amount;
                                                advanceTemp.Description = fee.Description;
                                                db.Entry(advanceTemp).State = EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                                else if (temp != null)
                                {
                                    //temp.Description = fee.Description;
                                    temp.DueDate = duedate;
                                    //temp.Amount = fee.Amount;
                                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                    var advancediff = (new DateTime(duedate.AddMonths(1).Year, duedate.AddMonths(1).Month, 1) - new DateTime(dt.Year, dt.Month, 1)).Days;
                                    if ((frequency == AdditionalFeeFrequency.Monthly && (reservation.IsFixedMonthToMonth || reservation.IsMonthToMonth) && advancediff <= 31)
                                    && ((reservation.IsFixedMonthToMonth == false && reservation.IsMonthToMonth == false) ? (duedate.AddMonths(1) <= reservation.CheckOutDate) : true))
                                    {
                                        duedate = duedate.AddMonths(1);
                                        //Create 1 month advance utilities
                                        var recurringday = fee.DueOn.Day;
                                        var tempduedate = duedate;
                                        var lastDayMonth = DateTime.DaysInMonth(tempduedate.Year, tempduedate.Month);
                                        if (lastDayMonth < recurringday)
                                        {
                                            recurringday = lastDayMonth;
                                        }
                                        duedate = new DateTime(tempduedate.Year, tempduedate.Month, recurringday);
                                        var advanceTemp = db.Income.Where(x =>
                                       (x.DueDate.Month == duedate.Month && x.DueDate.Year == duedate.Year)
                                       && x.BookingAdditionalFeeId == fee.Id
                                         ).FirstOrDefault();
                                        if (advanceTemp == null)
                                        {
                                            Income advanceIncome = new Income();
                                            advanceIncome.Description = fee.Description;
                                            advanceIncome.PropertyId = Convert.ToInt32(property.Id);
                                            advanceIncome.Amount = fee.Amount;
                                            advanceIncome.DueDate = duedate;
                                            advanceIncome.IncomeTypeId = (int)Core.Enumerations.IncomeType.TenantBill;
                                            advanceIncome.Status = 1;
                                            advanceIncome.CompanyId = companyId;
                                            advanceIncome.DateRecorded = DateTime.UtcNow;
                                            advanceIncome.IsBatchLoad = false;
                                            advanceIncome.BookingId = reservation.Id;
                                            advanceIncome.IsActive = true;
                                            advanceIncome.BookingAdditionalFeeId = fee.Id;
                                            db.Income.Add(advanceIncome);
                                            db.SaveChanges();
                                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", advanceIncome.Description, advanceIncome.Amount, advanceIncome.DueDate.ToDateTime().ToString("MMM dd,yyyy")), reservation.Id);
                                        }
                                        else
                                        {
                                            if (advanceTemp.Amount != fee.Amount || advanceTemp.Description != fee.Description)
                                            {
                                                advanceTemp.Amount = fee.Amount;
                                                advanceTemp.Description = fee.Description;
                                                db.Entry(advanceTemp).State = EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            });
        }

        #endregion
    }
}
