(function ($) {
    "use strict";

    //    Preloader  ======================
    var preloader = $('#preloader');
    $(window).on('load', function () {
        preloader.fadeOut('slow', function () {
            $(this).remove();
        });
    });


    //  slicknav ====================

    $('ul#navigation').slicknav({
        prependTo: ".mobile_menu"
    });

    // ======== equiment-worp  slider

    $('.active-map-slider-1').owlCarousel({
        slideSpeed: 2000,
        loop: true,
        autoplay: false,
        dots: false,
        nav: true,
        margin: 0,
        items: 1,
        navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
    });


    // ======== equiment-worp  slider



    // ======== equiment-worp  slider
    // ======== equiment-worp  slider

    //   ==============     quantity


    $(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below 1
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });
    // ======== equiment-worp  slider



    //=======  reservation-slider
    //=======  scrollTop

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 2000);
        return false;
    });


    //Search Pafe slider


    var slider = $(".carousel-full");
    var thumbnailSlider = $(".carousel-thumbs");
    var duration = 1000;
    var syncedSecondary = true;

    setTimeout(function () {
        $(".cloned .item-slider-model a").attr("data-fancybox", "group-2");
    }, 500);

    // carousel function for main slider
    slider
        .owlCarousel({
            loop: true,
            nav: true,
            navText: ["", ""],
            items: 1,
            lazyLoad: true,
            autoplay: false,
            smartSpeed: 1000,
            navText: ['<i class="icofont-thin-left"></i>', '<i class="icofont-thin-right"></i>'],
        })
        .on("changed.owl.carousel", syncPosition);

    // carousel function for thumbnail slider
    thumbnailSlider
        .on("initialized.owl.carousel", function () {
            thumbnailSlider
                .find(".owl-item")
                .eq(0)
                .addClass("current");
        })
        .owlCarousel({
            loop: false,
            nav: false,
            margin: 2,
            smartSpeed: 1000,
            responsive: {
                0: {
                    items: 12
                },
                600: {
                    items: 12
                },
                1200: {
                    items: 12,
                    margin: 5
                }
            }
        })
        .on("changed.owl.carousel", syncPosition2);

    // on click thumbnaisl
    thumbnailSlider.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        slider.data("owl.carousel").to(number, 300, true);
    });

    function syncPosition(el) {
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        thumbnailSlider
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = thumbnailSlider.find(".owl-item.active").length - 1;
        var start = thumbnailSlider
            .find(".owl-item.active")
            .first()
            .index();
        var end = thumbnailSlider
            .find(".owl-item.active")
            .last()
            .index();

        if (current > end) {
            thumbnailSlider.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
            thumbnailSlider.data("owl.carousel").to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            slider.data("owl.carousel").to(number, 100, true);
        }
    }
    
    

            $("#rangeDate").flatpickr({
            mode: 'range',
            dateFormat: "Y-m-d"
        });
    
    
            $(document).ready(function() {

            $(".price-range-tab").click(function() {
                $(".price-range-block").toggle();
            });

            $(".price-save , .clear , .place-type , .place-type1").click(function() {
                $(".price-range-block").hide();
            });
            
            $(".place-type").click(function() {
                $(".off-on-toggle").toggle();

            });
            $(".clear , .price-range-tab , .place-type1").click(function() {
                $(".off-on-toggle").hide();

            });
            
            $(".place-type1").click(function() {
                $(".off-on-toggle1").toggle();

            });
            
            $(".clear , .price-range-tab ,.place-type").click(function() {
                $(".off-on-toggle1").hide();

            });


        });
    
    
    
    
})(jQuery);
