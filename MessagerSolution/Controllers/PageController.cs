﻿using Core.Database.Context;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Models.Logs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class PageController : Controller
    {
        // GET: Page
        public ActionResult Index()
        {
            if (SessionHandler.CompanyId == 1 && User.Identity.Name.ToInt() == 1)
            {
                using (var db = new ApplicationDbContext()) {
                    ViewBag.Companies = db.Companies.ToList();
                return View(); }

            }
            else
            {
                return new HttpNotFoundResult();
            }
        }
        [HttpGet]
        public ActionResult GetLogs(string date)
        {
            if (SessionHandler.CompanyId == 1 && User.Identity.Name.ToInt() == 1)
            {
                var filter = date.ToDateTime();
                using (var db = new ApplicationDbContext())
                {
                    var data = db.Logs.Where(x => filter == DateTime.MinValue ? true : DbFunctions.TruncateTime(x.DateRecorded) == DbFunctions.TruncateTime(filter))
                    .GroupBy(p => p.Message)
                    .Select(g => new { Name = g.Key, Count = g.Count() * 100 / db.Logs.Count() }).ToList();
                    var logs = new List<LogModel>();
                    data.ForEach(x => {
                        logs.Add(new LogModel(x.Name, x.Count.ToDouble()));
                    });
                    return Json(new { Logs = logs }, JsonRequestBehavior.AllowGet);
                }
            }
            return new HttpNotFoundResult();
        }

        [HttpGet]
        public ActionResult GetUserLogs(string date)
        {
            if (SessionHandler.CompanyId == 1 && User.Identity.Name.ToInt() == 1)
            {
                using (var db = new ApplicationDbContext())
                {
                    var filter = date.ToDateTime();
                    var userData = db.Logs.Where(x=>filter== DateTime.MinValue?true: DbFunctions.TruncateTime(x.DateRecorded) == DbFunctions.TruncateTime(filter)).GroupBy(x => x.UserId).ToList();
                    var UserLogs = new List<UserLog>();
                    userData.ForEach(x => {
                        List<PageDetails> pages = new List<PageDetails>();
                         var pagesVisited = db.Logs.Where(l => l.UserId == x.Key).Where(z => filter == DateTime.MinValue ? true : DbFunctions.TruncateTime(z.DateRecorded) == DbFunctions.TruncateTime(filter)).GroupBy(p => p.Message).Select(i=>i.Key).ToList();
                        foreach(var page in pagesVisited)
                        {
                            pages.Add(new PageDetails() { 
                                Page=page,
                                DateAccess = db.Logs.Where(l => l.Message == page && filter == DateTime.MinValue ? true : DbFunctions.TruncateTime(l.DateRecorded) == DbFunctions.TruncateTime(filter)).Select(o => o.DateRecorded).ToList()

                            });
                                                    }

                        UserLogs.Add(new UserLog()
                        {

                            Username = db.Users.Where(u => u.Id == x.Key).Select(k => new { Name = k.FirstName + " " + k.LastName + "(" + k.UserName + ")" }).FirstOrDefault().Name,
                            PagesVisited =pages


                        });
                    });
                    return Json(new { data = UserLogs }, JsonRequestBehavior.AllowGet);
                }
            }
            return new HttpNotFoundResult();
        }
    }
}