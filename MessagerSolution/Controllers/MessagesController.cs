﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Core.Enumerations;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Core.SignalR.Hubs;
using MessagerSolution.Models.Message;
using System.Data.Entity;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using System.Configuration;
using Twilio.Rest.Pricing.V1.PhoneNumber;
using Newtonsoft.Json;
using System.Text;
using Twilio.Rest.Api.V2010.Account.AvailablePhoneNumberCountry;
using Twilio.Types;
using System.Globalization;
using MlkPwgen;
using System.Data.Entity.Core.Objects;

namespace MessagerSolution.Controllers
{
    public class MessagesController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        IMessageRepository messageRepo = new MessageRepository();
        //
        // GET: /Messages/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int threadId = 0)
        {

            if (User.Identity.IsAuthenticated)
            {
                FormsIdentity fid = User.Identity as FormsIdentity;
                string plainTextUserData = fid.Ticket.UserData;
                ViewBag.UserRole = plainTextUserData.ToLower();
                ViewBag.UserId = User.Identity.Name;
                ViewBag.ThreadId = threadId;
                ViewBag.Renters = objIList.GetRenters();
                ViewBag.Properties = objIList.GetProperties();
                ViewBag.TemplateList = objIList.GetTemplateMessages();
                ViewBag.IsUserWorker = GlobalVariables.UserWorkerId != 0 ? true : false;
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult GetGuests()
        {
            var guests= objIList.GetGuestsList();
            return Json(new {guests }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetMessageRulesList(string propertyFilter, int eventFilter)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {

                    var eventType = "";
                    string actions = "<a class=\"ui mini button edit-message-rule-btn\" title=\"Edit\">" +
                                         "<i class=\"icon edit\"></i></a>" +
                                         "<a class=\"ui mini button delete-message-rule-btn\" title=\"Delete\">" +
                                         "<i class=\"icon delete\"></i></a>";
                    List<MessageRuleTableData> mr = new List<MessageRuleTableData>();
                    var messageRules = db.MessageRules.Join(db.TemplateMessages, x => x.TemplateMessageId, y => y.Id, (x, y) => new { MessageRules = x, TemplateMessage = y })
                        .Join(db.Properties, x => x.MessageRules.PropertyId, y => y.Id, (x, y) => new { MessageRules = x.MessageRules, TemplateMessage = x.TemplateMessage, Property = y })
                        .Where(x=>x.TemplateMessage.CompanyId==SessionHandler.CompanyId)
                        .ToList();

                    if (propertyFilter != "all")
                    {
                        int propertyId = int.Parse(propertyFilter);
                        messageRules = messageRules.Where(x => x.Property.Id == propertyId /*|| x.Property.ParentPropertyId == propertyId*/).ToList();
                    }
                    if (eventFilter != 0)
                    {
                        messageRules = messageRules.Where(x => x.MessageRules.Type == eventFilter).ToList();
                    }
                    messageRules.ForEach(x =>
                    {
                        if (x.MessageRules.Type == 1)
                        {
                            eventType = "First Message";
                        }
                        else if (x.MessageRules.Type == 2)
                        {
                            eventType = "Before Check-in";
                        }
                        else if (x.MessageRules.Type == 3)
                        {
                            eventType = "After Check-in";
                        }
                        else if (x.MessageRules.Type == 4)
                        {
                            eventType = "Before Check-out";
                        }
                        else if (x.MessageRules.Type == 5)
                        {
                            eventType = "After Check-out";
                        }
                        else if (x.MessageRules.Type == 6)
                        {
                            eventType = "Upon Inquiry";
                        }
                        else if (x.MessageRules.Type == 7)
                        {
                            eventType = "Upon Confirmed Reservation";
                        }
                        else if (x.MessageRules.Type == 8)
                        {
                            eventType = "Upon Cancellation";
                        }
                        else if (x.MessageRules.Type == 9)
                        {
                            eventType = "Upon Alteration";
                        }
                        else if (x.MessageRules.Type == 10)
                        {
                            eventType = "Pre-approval";
                        }

                        MessageRuleTableData temp = new MessageRuleTableData();
                        temp.Id = x.MessageRules.Id;
                        temp.PropertyName = x.Property.Name;
                        temp.EventType = eventType;
                        temp.Title = x.TemplateMessage.Title;
                        temp.Message = x.TemplateMessage.Message;
                        temp.Actions = actions;
                        mr.Add(temp);
                    });
                    return Json(new { data = mr }, JsonRequestBehavior.AllowGet);

                }
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ThreadInfo(string threadId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                    var guest = db.Guests.Where(x => x.Id == inbox.GuestId).FirstOrDefault();
                    return Json(new { data = guest }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult WorkerThreadInfo(string threadId, int workerId ,bool isworker)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    if (isworker)
                    {
                        var worker = db.Workers.Where(x => x.Id == workerId).FirstOrDefault();
                        return Json(new { data = worker,isWorker = isworker }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var vendor = db.Vendors.Where(x => x.Id == workerId).FirstOrDefault();
                        return Json(new { data = vendor, isWorker = isworker }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetSMSList(string propertyFilter)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var numbers = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId).Select(x => x.PhoneNumber).ToList();

                    string actions = "<a class=\"ui mini circular primary icon button reply-sms-btn\" title=\"Reply\">" +
                                         "<i class=\"icon reply\"></i></a>" +
                                         "<a class=\"ui mini circular red icon button delete-sms-btn\" title=\"Delete\">" +
                                         "<i class=\"icon trash\"></i></a>" +
                                         "<a class=\"ui mini circular icon button assign-sms-btn\" title=\"Assign\">" +
                                         "<i class=\"icon exchange\"></i></a>";

                    string callActions = "<a class=\"ui mini circular primary icon button reply-sms-btn\" title=\"Reply\">" +
                                         "<i class=\"icon reply\"></i></a>" +
                                         "<a class=\"ui mini circular red icon button delete-call-btn\" title=\"Delete\">" +
                                         "<i class=\"icon trash\"></i></a>" +
                                         "<a class=\"ui mini circular icon button assign-sms-btn\" title=\"Assign\">" +
                                         "<i class=\"icon exchange\"></i></a>";
                    List<MessageSMSTableData> smsTableData = new List<MessageSMSTableData>();
                    var smsList = db.SMS
                        .Where(x => x.GuestId == 0 && x.IsMyMessage == false && numbers.Contains(x.To))
                        .ToList();
                 
                    smsList.ForEach(x =>
                    {
                        MessageSMSTableData temp = new MessageSMSTableData
                        {
                            Id = x.Id,
                            From = x.From,
                            Message = x.Body,
                            CreatedDate = x.CreatedDate,
                            Actions = actions,
                            Type = CommunicationType.SMS
                        };
                        smsTableData.Add(temp);
                    });

                    var callList = db.Calls.Where(x => x.GuestId == 0 && x.IsMyMessage == false && numbers.Contains(x.To)).ToList();
                    callList.ForEach(x =>
                    {
                        MessageSMSTableData temp = new MessageSMSTableData
                        {
                            Id = x.Id,
                            From = x.From,
                            Message = "missed call",
                            CreatedDate = x.CreatedDate,
                            Actions = callActions,
                            Type = CommunicationType.Call
                        };
                        smsTableData.Add(temp);
                    });

                    smsTableData = smsTableData.OrderByDescending(x => x.CreatedDate).ToList();
                    return Json(new { data = smsTableData }, JsonRequestBehavior.AllowGet);

                }
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult MessageRuleDetails(int id)
        {
            bool isSuccess = false;
            MessageRule rules = new MessageRule();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    rules = db.MessageRules.FirstOrDefault(i => i.Id == id);
                    if (rules != null) isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, messageRules = rules }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddMessageRules(int propertyId, int templateMessageId, int type, int days, string title, string message, bool isSendToSms, bool isSendToSite, bool isSendToEmail, bool isActive)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    if (templateMessageId == 0)
                    {
                        TemplateMessage temp = new TemplateMessage();
                        temp.Title = title;
                        temp.Message = message;
                        temp.CompanyId = SessionHandler.CompanyId;
                        temp.CreatedDate = DateTime.Now;
                        db.TemplateMessages.Add(temp);
                        db.SaveChanges();

                        MessageRule rule = new MessageRule();
                        rule.PropertyId = propertyId;
                        rule.TemplateMessageId = temp.Id;
                        rule.Type = type;
                        rule.IsActive = isActive;
                        rule.IsSendToSMS = isSendToSms;
                        rule.IsSendToSite = isSendToSite;
                        rule.IsSendToEmail = isSendToEmail;
                        rule.DateDifference = days;
                        db.MessageRules.Add(rule);
                        db.SaveChanges();

                        return Json(new { success = true, message = "Successfully Added Message Rule" }, JsonRequestBehavior.AllowGet);
                    }

                    else
                    {
                        MessageRule rul = new MessageRule();
                        rul.PropertyId = propertyId;
                        rul.TemplateMessageId = templateMessageId;
                        rul.Type = type;
                        rul.IsActive = isActive;
                        rul.IsSendToSMS = isSendToSms;
                        rul.IsSendToSite = isSendToSite;
                        rul.IsSendToEmail = isSendToEmail;
                        rul.DateDifference = days;
                        db.MessageRules.Add(rul);
                        db.SaveChanges();

                        return Json(new { success = true, message = "Successfully Added Message Rule" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMessageRule(int id, string title, string message, int propertyId, int templateMessageId, int type, int days,bool isSendToSms,bool isSendToSite,bool isSendToEmail,bool isActive)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var rules = db.MessageRules.Find(id);
                    if (templateMessageId == 0)
                    {
                        TemplateMessage temp = new TemplateMessage();
                        temp.Message = message;
                        temp.Title = title;
                        temp.CompanyId = SessionHandler.CompanyId;
                        temp.CreatedDate = DateTime.Now;
                        db.TemplateMessages.Add(temp);
                        db.SaveChanges();

                        rules.PropertyId = propertyId;
                        rules.TemplateMessageId = temp.Id;
                        rules.DateDifference = days;
                        rules.IsActive = isActive;
                        rules.IsSendToSMS = isSendToSms;
                        rules.IsSendToSite = isSendToSite;
                        rules.IsSendToEmail = isSendToEmail;
                        db.Entry(rules).State = EntityState.Modified;
                        db.SaveChanges();
                        return Json(new { success = true, message = "Message Rule details has been updated" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        rules.PropertyId = propertyId;
                        rules.TemplateMessageId = templateMessageId;
                        rules.Type = type;
                        rules.DateDifference = days;
                        db.Entry(rules).State = EntityState.Modified;
                        db.SaveChanges();
                        return Json(new { success = true, message = "Message Rule details has been updated" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteMessageRule(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var b = db.MessageRules.Where(x => x.Id == id).FirstOrDefault();
                db.MessageRules.Remove(b);
                db.SaveChanges();
                return Json(new { success = true, message = "Message Rule has been deleted" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AssignSMSPhoneNumber(string phoneNumber, int? guestId, int? renterId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var smsList = db.SMS.Where(x => (x.From == phoneNumber || x.To == phoneNumber) && x.GuestId == 0).ToList();
                    var callList = db.Calls.Where(x => (x.From == phoneNumber || x.To == phoneNumber) && x.GuestId == 0).ToList();
                    var contactInfos = db.GuestContactInfos.Where(x => x.Value == phoneNumber).ToList();
                    if (contactInfos.Count > 0)
                    {
                        db.GuestContactInfos.RemoveRange(contactInfos);
                    }
                    //Connect unknown number to short term guest
                    if (guestId != 0 && guestId!=null)
                    {
                        var guestContact = new GuestContactInfo
                        {
                            Type = 1,
                            Value = phoneNumber,
                            GuestId = guestId.GetValueOrDefault()
                        };
                        if (phoneNumber.Substring(0, 8) == "whatsapp")
                        {
                            guestContact.Type = 2;
                        }
                        else if (phoneNumber.Substring(0, 9) == "messenger")
                        {
                            guestContact.Type = 3;
                        }

                        db.GuestContactInfos.Add(guestContact);
                        smsList.ForEach(x =>
                        {
                            x.GuestId = guestId.GetValueOrDefault();
                        });
                        callList.ForEach(x =>
                        {
                            x.GuestId = guestId.GetValueOrDefault();
                        });
                        db.SaveChanges();
                    }
                    else if (renterId != 0 && renterId!=null)
                    {
                        var contacts = db.RenterContacts.Where(x => x.RenterId == renterId).ToList();
                        contacts.ForEach(x => x.IsDefault = false);
                        db.SaveChanges();
                        var thread = db.CommunicationThreads.Where(x => x.RenterId == renterId && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                        db.RenterContacts.Add(new RenterContact() { RenterId = renterId.GetValueOrDefault(), Contact = phoneNumber });
                        db.SaveChanges();
                        foreach(var sms in smsList)
                        {
                            db.CommunicationSMS.Add(new CommunicationSMS() {To =sms.To,From = sms.From,ThreadId =thread.ThreadId,CreatedDate=sms.CreatedDate,Message=sms.Body});
                            db.SaveChanges();
                        }
                        foreach (var call in callList)
                        {
                            db.CommunicationCalls.Add(new CommunicationCall() { To = call.To, From = call.From, ThreadId = thread.ThreadId, CreatedDate = call.CreatedDate,CallDuration =call.CallDuration,CallSid=call.CallSid,FromCountry=call.FromCountry,Length=call.Length,RecordingDuration=call.RecordingDuration,RecordingSid=call.RecordingSid,RecordingStartTime=call.RecordingStartTime,RecordingUrl=call.RecordingUrl});
                            db.SaveChanges();
                        }
                        db.SMS.RemoveRange(smsList);
                        db.Calls.RemoveRange(callList);
                        db.SaveChanges();
                    }

                   

                    return Json(new { success = true, message = "Phone Number has been Assigned" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteMessageSMS(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var b = db.SMS.Where(x => x.Id == id).FirstOrDefault();
                db.SMS.Remove(b);
                db.SaveChanges();
                return Json(new { success = true, message = "SMS has been deleted" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteMessageCall(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var b = db.Calls.Where(x => x.Id == id).FirstOrDefault();
                db.Calls.Remove(b);
                db.SaveChanges();
                return Json(new { success = true, message = "Call has been deleted" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Remove by Joem 02/24/19 because Paulo change logic of sending sms
        //[HttpPost]
        //[CheckSessionTimeout]
        //public ActionResult ReplyMessageSMSMessage(string threadId, string message)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
        //        var guest = db.Guests.Where(x => x.Id == inbox.GuestId).FirstOrDefault();
        //        var property = db.Properties.Where(x => x.ListingId == inbox.PropertyId).FirstOrDefault();
        //        var twilioNumber = new TwilioNumber();

        //        if (property != null)
        //        {
        //            twilioNumber = db.TwilioNumbers.Where(x => x.Id == property.TwilioPhoneId).FirstOrDefault();
        //        }

        //        string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
        //        string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

        //        TwilioClient.Init(accountSid, authToken);

        //        if (guest.ContactNumber != null && twilioNumber.PhoneNumber != null)
        //        {
        //            var smsMessage = MessageResource.Create(
        //                from: new Twilio.Types.PhoneNumber(twilioNumber.PhoneNumber),
        //                body: message,
        //                to: new Twilio.Types.PhoneNumber(guest.ContactNumber)
        //            );

        //            var sms = new SMS
        //            {
        //                From = twilioNumber.PhoneNumber,
        //                To = guest.ContactNumber,
        //                GuestId = guest.Id,
        //                MessageSid = smsMessage.Sid,
        //                Body = message,
        //                IsMyMessage = true,
        //                CreatedDate = DateTime.Now
        //            };
        //            db.SMS.Add(sms);
        //            db.SaveChanges();

        //            return Json(new { success = true, message = "SMS sent", from = property.TwilioPhoneNumber, to = guest.ContactNumber }, JsonRequestBehavior.AllowGet);

        //        }

        //        return Json(new { success = false }, JsonRequestBehavior.AllowGet);

        //    }
        //}

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ReplyMessageSMS(int id, string message)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var smsData = db.SMS.Where(x => x.Id == id).FirstOrDefault();

                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                    string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                    TwilioClient.Init(accountSid, authToken);

                    var smsMessage = MessageResource.Create(
                        from: new Twilio.Types.PhoneNumber(smsData.To),
                        body: message,
                        to: new Twilio.Types.PhoneNumber(smsData.From)
                    );
                    var sms = new SMS();
                    sms.From = smsData.To;
                    sms.To = smsData.From;
                    sms.GuestId = smsData.GuestId;
                    sms.MessageSid = smsMessage.Sid;
                    sms.Body = message;
                    sms.IsMyMessage = true;
                    sms.CreatedDate = DateTime.Now;
                    db.SMS.Add(sms);
                    db.SaveChanges();

                    return Json(new { success = true, message = "SMS sent" }, JsonRequestBehavior.AllowGet);
                }
            } catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult LoadInbox(int skip, int take)
        {
            return Json(new { result = messageRepo.GetInboxList(skip, take) }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MessageReceived(string userId) {

            InboxHub.MessageReceived(userId, messageRepo.GetNewInbox());
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetTemplateList()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        string actions = "<a class=\"ui mini button edit-template-btn\" title=\"Edit\">" +
                                         "<i class=\"icon edit\"></i></a>" +
                                         "<a class=\"ui mini button delete-template-btn\" title=\"Delete\">" +
                                         "<i class=\"icon delete\"></i></a>";

                        List<TemplateMessageTableData> td = new List<TemplateMessageTableData>();
                        var accounts = db.TemplateMessages.Where(x => x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.Title).ToList();
                        accounts.ForEach(x =>
                        {
                            TemplateMessageTableData d = new TemplateMessageTableData();
                            d.Id = x.Id;
                            d.Title = x.Title;
                            d.Message = x.Message;
                            d.Actions = actions;
                            td.Add(d);
                        });
                        return Json(new { data = td }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult SaveContact(int guestId,string[] ContactList)
        {

            using (var db = new ApplicationDbContext())
            {
                for(int x=0;x < ContactList.Length;x++)
                {
                    var valtemp= ContactList[x].Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");

                    var temp = db.GuestContactInfos.Where(y => y.Value == valtemp && y.GuestId == guestId).FirstOrDefault();
                    if(temp == null)
                    {
                        GuestContactInfo contactInfo = new GuestContactInfo()
                        {
                            GuestId = guestId,
                            Value = valtemp,
                            Source=0,
                            Type=1
                        };
                        db.GuestContactInfos.Add(contactInfo);
                        db.SaveChanges();
                    }
                }
                var ContactInfos = db.GuestContactInfos.Where(x => x.GuestId == guestId).ToList();
                return Json(new { contactInfos = ContactInfos }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpGet]
        public ActionResult GetProperty(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var availableParentproperties = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where hc.CompanyId == SessionHandler.CompanyId && p.TwilioNumberId == null && p.IsParentProperty select p).ToList();
                var assignedParentproperties = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where hc.CompanyId == SessionHandler.CompanyId && p.TwilioNumberId == id && p.IsParentProperty select p).ToList();
                var availableproperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId && (/*p.ParentPropertyId==0&&*/ p.TwilioNumberId == null) select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();
                var assignedproperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId && (/*p.ParentPropertyId == 0 &&*/ p.TwilioNumberId == id) select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();

                var assignedHost = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId && h.TwilioNumberId==id select h).ToList();
                var availableHost = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId && h.TwilioNumberId ==null select h).ToList();
                return Json(new { assignedHost= assignedHost, availableHost= availableHost, assignedParentproperties = assignedParentproperties,availableParentproperties = availableParentproperties, assignedproperties =assignedproperties, availableproperties = availableproperties }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetUnRepliedThreadCount()
        {
            using (var db = new ApplicationDbContext())
            {

                var threads = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                List<NewMessageModel> inboxes = new List<NewMessageModel>();
                foreach (var item in threads)
                {
                    var temp = (from m in db.CommunicationSMS where m.ThreadId == item.ThreadId orderby m.CreatedDate descending select m).FirstOrDefault();
                    //var tempMessage = (from m in db.CommunicationMessages where m.ThreadId == item.ThreadId orderby m.DateCreated descending select m).FirstOrDefault();
                    //var userId = temp != null ? temp.UserId : tempMessage != null ? tempMessage.UserId : 0;
                    var userId = temp != null ? temp.UserId : 0;
                    var name = item.VendorId.HasValue ? db.Vendors.Where(x => x.Id == item.VendorId).Select(x => x.Name).FirstOrDefault() :
                                item.WorkerId.HasValue ? db.Workers.Where(x => x.Id == item.WorkerId).Select(x => new { Name = x.Firstname + " " + x.Lastname }).FirstOrDefault().Name :
                                item.RenterId.HasValue ? db.Renters.Where(x => x.Id == item.RenterId).Select(x => new { Name = x.Firstname + " " + x.Lastname }).FirstOrDefault().Name : "";
                    inboxes.Add(new NewMessageModel()
                    {
                        Name = name,
                        Image = Core.Helper.Utilities.GetDefaultAvatar(name),
                        LastMessage = temp != null ? temp.Message : "No Message(s)",
                        ThreadId = item.ThreadId,
                        LastMessageAt = temp != null ? temp.CreatedDate : (DateTime?)null,
                        IsReply = userId == User.Identity.Name.ToInt(),
                        Type = item.RenterId != null ? 1 : item.WorkerId != null ? 2 : item.VendorId != null ? 3 : 0
                    });
                }
                var count = inboxes.Where(x => x.IsEnd == false && x.IsReply == false).Count();
                inboxes = inboxes.OrderByDescending(x => x.LastMessageAt).ThenByDescending(x => x.IsEnd == false && x.IsReply == false).ToList();
                return Json(new { count, inboxes }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EndCommunicationThread(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var thread= db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                thread.IsEnd = true;
                db.Entry(thread).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true, }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EndThread(string threadId)
        {
            using (var db= new ApplicationDbContext())
            {
                var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                inbox.IsEnd = true;
                db.Entry(inbox).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true, }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LockThread(string threadId){
            using (var db = new ApplicationDbContext())
            {
                if (threadId != null)
                {
                    var lockinbox = new LockInbox()
                    {
                        UserId = User.Identity.Name.ToInt(),
                        ThreadId = threadId,
                        DateCreated = DateTime.Now
                    };
                    db.LockInboxes.Add(lockinbox);
                    db.SaveChanges();
                    InboxHub.LockInbox(lockinbox.UserId.ToString(), lockinbox);
                }
                return Json(new { success = true, }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UnLockThread(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {

                var temp = db.LockInboxes.Where(x => x.ThreadId == threadId).ToList();
                if (temp.Count > 0)
                {
                    db.LockInboxes.RemoveRange(temp);
                    db.SaveChanges();

                    InboxHub.UnLockInbox(User.Identity.Name, threadId);
                }
                return Json(new { success = true, }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetMessageThread(string threadId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {   var userTyping = "";
                    IList<MessageEntry> messages = new List<MessageEntry>();

                    ICommonRepository objIList = new CommonRepository();
                    var messageThread = objIList.GetMessageThread(threadId);
                    var threadDetails = objIList.GetThreadDetail(threadId);

                    var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                    var sms = db.SMS.Where(x => x.GuestId == inbox.GuestId).ToList();
                    var calls = db.Calls.Where(x => x.GuestId == inbox.GuestId).ToList();
                    var hasDefaultContact = db.GuestContactInfos.Where(x => x.GuestId == inbox.GuestId && x.IsDefault).Select(x => x.IsDefault).FirstOrDefault();
                    var contactInfos = db.GuestContactInfos.Where(x => x.GuestId == inbox.GuestId).ToList();
                    var lockthread = db.LockInboxes.Where(x => x.ThreadId == threadId && EntityFunctions.DiffSeconds(x.DateCreated, DateTime.Now)  <= 180).FirstOrDefault();
                    if (lockthread != null)
                    {
                        if (User.Identity.Name.ToInt() != lockthread.UserId)
                        {

                            var user = db.Users.Where(x => x.Id == lockthread.UserId).FirstOrDefault();

                            userTyping = user.FirstName + " " + user.LastName + " Replying...";
                        }
                    }
                    messageThread.ToList().ForEach(x =>
                    {  
                        var message = new MessageEntry();
                        message.MessageContent = x.MessageContent;
                        message.IsMyMessage = x.IsMyMessage;
                        message.CreatedDate = x.CreatedDate;
                        message.Id = x.Id;
                        message.Type = 0;
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if(x.UserId ==0){
                            message.Name = "Automated";
                            message.ImageUrl = "/Content/img/robot.jpg";
                        }
                        else if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        messages.Add(message);
                    });
                    sms.ForEach(x =>
                    {
                        var message = new MessageEntry();
                        message.Id = x.Id;
                        message.MessageContent = x.Body;
                        message.IsMyMessage = x.IsMyMessage;
                        message.Source = x.IsMyMessage ? x.To : x.From;
                        message.Images = db.MMSImages.Where(y => y.CommunicationSMSId == x.Id).Select(y => y.ImageUrl).ToList();
                        if (message.Source.Substring(0, 8) == "whatsapp")
                        {
                            message.Type = 3;
                        } else if (message.Source.Substring(0, 9) == "messenger")
                        {
                            message.Type = 4;
                        }
                        else
                        {
                            message.Type = 1;
                        }
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (x.UserId == 0)
                        {
                            message.Name = "Automated";
                            message.ImageUrl = "/Content/img/robot.jpg";
                        }
                        else if(user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        message.CreatedDate = x.CreatedDate;
                        message.ResourceSid = x.MessageSid;
                        messages.Add(message);
                    }
                    );
                    calls.ForEach(x =>
                    {
                        var message = new MessageEntry();
                        message.Id = x.Id;
                        message.IsMyMessage = x.IsMyMessage;
                        message.Type = 2;
                        message.Source = x.IsMyMessage ? x.To : x.From;
                        message.CreatedDate = x.CreatedDate;
                        message.ResourceSid = x.CallSid;
                        message.RecordingUrl = x.RecordingUrl;
                        message.Duration = x.CallDuration == null ? x.RecordingDuration : x.CallDuration;
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        messages.Add(message);
                    }
                    );
                    threadDetails.Inquiries = threadDetails.Inquiries.OrderBy(x => x.CheckInDate).Reverse().ToList();
                    var variables = "";

                    return Json(new { hasDefaultContact, success = true, messageThread = messages.OrderBy(f => f.CreatedDate), threadDetails, variables, contactInfos,user= userTyping }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> SendMessageToInbox(int siteType, string hostId, string threadId, string message)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Sending message...", NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                //set the parameter for API request
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId);
                reqParams.Add("threadId", threadId);
                reqParams.Add("message", message);
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Messages/SendMessageToInbox", reqContent);
                if (result != null && result.Content != null)
                {  //get and parse the result into Object to get the content
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Successfully sent!", NotificationType.SUCCESS);
                    //    using (var db = new ApplicationDbContext())
                    //    {

                    //        var temp = db.LockInboxes.Where(x => x.ThreadId == threadId).ToList();
                    //        if (temp.Count > 0)
                    //        {
                    //            db.LockInboxes.RemoveRange(temp);
                    //            db.SaveChanges();
                    //        }
                    //        InboxHub.UnLockInbox(User.Identity.Name, threadId);
                    //    }
                    InboxHub.MessageReceived(User.Identity.Name, messageRepo.GetNewInbox());
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while sending your message.", NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UpdateInboxStatus(string threadId)
        {
            try
            {
                objIList.UpdateInboxStatus(threadId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpGet]
        //[CheckSessionTimeout]
        //public ActionResult CheckNewHostMessage(long threadId, int hostId)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        Trace.WriteLine("Thread has start checking for new message");

        //        try
        //        {
        //            IList<InboxMessageModel> newInbox = new List<InboxMessageModel>();
        //            SiteConstants.accounts.ToList().ForEach(item =>
        //            {
        //                Task.Factory.StartNew(() => { 
        //                    GetAirbnbNewMessagesFromHost(item);
        //                }).ContinueWith(x => { if (x.IsCompleted) x.Dispose(); });
        //            });
        //            Task.WaitAll();
        //            newInbox = objIList.GetNewInbox();

        //            return Json(new { success = true, hasNew = objIList.HasNewMessage(), newInbox = newInbox, messages = objIList.GetMessageThread(threadId) }, JsonRequestBehavior.AllowGet);
        //        }
        //        catch { return Json(new { success = false, message = "Error on refresh messages" }, JsonRequestBehavior.AllowGet); }
        //    }
        //    return Json(new { success = false, message = "Unauthenticated User" }, JsonRequestBehavior.AllowGet);
        //}

        //public AirbnbScrapeTask GetAirbnbNewMessagesFromHost(KeyValuePair<string, AirbnbAccount> kvp)
        //{
        //    AirbnbScrapeTask st = new AirbnbScrapeTask();
        //    Trace.WriteLine(string.Format("{0}'s host new message scrapping start", kvp.Value.Username));
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var ownerEntity = db.Owners.Find(kvp.Value.Id.ToLong());
        //        if (ownerEntity != null && ownerEntity.AutoSync)
        //        {
        //            st.IsAutoSync = true;
        //            using (ScrapeManager sm = new ScrapeManager())
        //            {
        //                if (!string.IsNullOrEmpty(kvp.Value.SessionToken))
        //                {
        //                    var res = sm.ScrapeInbox(kvp.Value.SessionToken, true, false);
        //                    var res2 = 
        //                    st.IsSuccess = res.Success;
        //                    if (res.Success)
        //                        Trace.WriteLine("Success on scrape new inbox");
        //                    else
        //                        Trace.WriteLine("Error on scrape new inbox");
        //                }
        //            }
        //        }
        //        else
        //        {
        //            Trace.WriteLine(string.Format("{0}'s account sync is off", ownerEntity.Name));
        //        }
        //    }
        //    Trace.WriteLine(string.Format("{0}'s host new message scrapping finish", kvp.Value.Username));
        //    return st;
        //}

        //[HttpGet]
        //[CheckSessionTimeout]
        //public ActionResult RefreshThreadMessages(long threadId, long hostId)
        //{
        //    try
        //    {
        //        Task.Factory.StartNew(() =>
        //        {
        //            string token = "";
        //            using (ScrapeManager sm = new ScrapeManager())
        //            {
        //                using(var db = new ApplicationDbContext())
        //                {
        //                    var dbmessages = db.Messages.Where(x => x.ThreadId == threadId).ToList();
        //                    var hostAccount = SiteConstants.accounts.Where(x => x.Key == hostId.ToString()).FirstOrDefault().Value;
        //                    if (hostAccount != null) { token = hostAccount.SessionToken; }
        //                    if(!string.IsNullOrEmpty(token))
        //                    {
        //                        sm.LoadCookies(token);
        //                        var messages = sm.ScrapeMessageDetails(threadId);
        //                        foreach (var item in messages)
        //                        {
        //                            if (dbmessages.Where(x => x.MessageId == item.MessageId).ToList().Count == 0)
        //                            {
        //                                db.Messages.Add(item);
        //                            }
        //                        }
        //                        db.BulkSaveChanges();
        //                    }
        //                }
        //            }
        //        }).ContinueWith(x => { if (x.IsCompleted) x.Dispose(); });
        //        return Json(new { success = true, messages = objIList.GetMessageThread(threadId) }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch { return Json(new { success = true }, JsonRequestBehavior.AllowGet); }
        //}

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteAllNoteForInquiry(long inquiryId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.tbNote.RemoveRange(db.tbNote.Where(x => x.ReservationId == inquiryId));
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception) { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult SaveNote(int InquiryId, string list)
        {
            using (var db = new ApplicationDbContext())
            {
                var js = new JavaScriptSerializer();
                var NoteList = js.Deserialize<List<Note>>(list);

                db.tbNote.RemoveRange(db.tbNote.Where(x => x.ReservationId == InquiryId));
                db.SaveChanges();

                foreach (Note n in NoteList.ToList())
                {
                    n.ReservationId = InquiryId;
                    db.tbNote.Add(n);
                }
                db.SaveChanges();

                return Json(new { success = true, model = NoteList }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetNotes(int inquiryId)
        {
            using (var db = new ApplicationDbContext())
            {
                //var temp = db.tbNote
                //    .Join(db.Inquiries, n => n.ReservationId, i => i.ReservationId, (n, i) => new { Notes = n, Inquiry = i })
                //    .Where(x => x.Notes.ReservationId == inquiryId)
                //    .ToList();

                var notes = (from note in db.tbNote where note.ReservationId == inquiryId select note).ToList();

                return Json(new { success = true, result = notes }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddTemplate(string title, string message)
        {
            bool isSuccess = false;
            bool isDuplicate = false;
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    if (db.TemplateMessages.FirstOrDefault(x => x.Title == title) == null)
                    {
                        TemplateMessage tm = new TemplateMessage();
                        tm.Title = title;
                        tm.Message = message;
                        tm.CompanyId = SessionHandler.CompanyId;
                        tm.CreatedDate = DateTime.Now;
                        db.TemplateMessages.Add(tm);
                        isSuccess = db.SaveChanges() > 0 ? true : false;
                    }
                    else
                    {
                        isDuplicate = true;
                    }
                }
            }
            return Json(new { success = isSuccess, is_duplicate = isDuplicate }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult EditTemplate(TemplateMessage model)
        {
            bool isDuplicate = false;
            bool isSuccess = false;
            if (User.Identity.IsAuthenticated)
            {
                if (objIList.IsDuplicateTemplate(model))
                {
                    isDuplicate = true;
                }
                else
                {
                    isSuccess = objIList.EditTemplate(model);
                }
            }
            return Json(new { success = isSuccess, is_duplicate = isDuplicate }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetTemplate(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var temp = objIList.GetTemplate(id);
                if (temp != null)
                {
                    return Json(new { success = true, template = temp }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteTemplate(int id)
        {
            bool isSuccess = false;
            if (User.Identity.IsAuthenticated)
            {
                isSuccess = objIList.DeleteTemplate(id);
            }
            return Json(new { success = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetTemplates()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    var templates = db.TemplateMessages.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    return Json(new { success = true, templates }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveForwardingNumber(int Id,string ForwardingNumber, string CallLength,string VoicemailMessage,string CallforwadingMessage,int [] PropertyIds,int [] ToRemovePropertyIds,int [] ToRemoveHostIds,int [] HostIds  )
        { bool success = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.TwilioNumbers.Where(x => x.Id == Id).FirstOrDefault();
                    temp.CallForwardingNumber = ForwardingNumber;
                    temp.CallLength = CallLength.ToInt();
                    temp.VoicemailMessage = VoicemailMessage ==""? "Hi. Please leave a message after the beep.": VoicemailMessage;
                    temp.CallforwardingMessage = CallforwadingMessage ==""? "Please wait while we forward you to the local number":CallforwadingMessage;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    if (ToRemoveHostIds != null)
                    {
                        foreach (var hId in ToRemoveHostIds)
                        {
                            var host = db.Hosts.Where(x => x.Id == hId).FirstOrDefault();
                            host.TwilioNumberId = null;
                            db.Entry(host).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    if (HostIds != null)
                    {
                        foreach (var hId in HostIds)
                        {
                            var host = db.Hosts.Where(x => x.Id == hId).FirstOrDefault();
                            host.TwilioNumberId = Id;
                            db.Entry(host).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    if (ToRemovePropertyIds!=null) {
                        foreach (var pId in ToRemovePropertyIds)
                        {
                            var property = db.Properties.Where(x => x.Id == pId).FirstOrDefault();
                            //JM  02/24/19 if parent property, get all child and remove link Twiio number
                            if (property.IsParentProperty)
                            {
                                property.TwilioNumberId = null;
                                db.Entry(property).State = EntityState.Modified;
                                db.SaveChanges();
                                var childs = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == property.Id && pc.CompanyId == SessionHandler.CompanyId && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p).ToList();
                                //var child = db.Properties.Where(x => x.ParentPropertyId == property.Id).ToList();
                                foreach (var c in childs)
                                {
                                    c.TwilioNumberId = null;
                                    db.Entry(c).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                property.TwilioNumberId = null;
                                db.Entry(property).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    //JM  02/24/19 Link Twilio number to property
                    if (PropertyIds != null)
                    {
                        foreach (var pId in PropertyIds)
                        {
                            var property = db.Properties.Where(x => x.Id == pId).FirstOrDefault();
                            //JM  02/24/19 if parent property, get all child and link to Twiio number
                            if (property.IsParentProperty)
                            {
                                property.TwilioNumberId = Id;
                                db.Entry(property).State = EntityState.Modified;
                                db.SaveChanges();
                                var childs = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == property.Id && pc.CompanyId == SessionHandler.CompanyId && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p).ToList();
                                //var child = db.Properties.Where(x => x.ParentPropertyId == property.Id).ToList();
                                foreach (var c in childs)
                                {
                                    c.TwilioNumberId = Id;
                                    db.Entry(c).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                property.TwilioNumberId = Id;
                                db.Entry(property).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    success = true;
                }
            }
            catch (Exception e)
            {
                success = false;
            }
                return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetOwnedNumbers()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    string actions = "<a class=\"ui mini button edit-forwarding-number\" title=\"Setting\"><i class=\"sun outline icon\"></a>";
                        //"<a class=\"ui mini button edit\" title=\"Edit\">" +
                        //"<i class=\"icon edit\"></i></a>" +
                        //"<a class=\"ui mini button delete-template-btn\" title=\"Delete\">" +
                        //"<i class=\"icon delete\"></i></a>";
                    List<TwilioNumberTable> data = new List<TwilioNumberTable>();

                    var twilioNumbers = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    twilioNumbers.ForEach(x =>
                    {
                        TwilioNumberTable row = new TwilioNumberTable
                        {
                            Id = x.Id,
                            PhoneNumber = x.PhoneNumber,
                            FriendlyName = x.FriendlyName,
                            Actions = actions,
                            CallForwardingNumber = x.CallForwardingNumber,
                            CallLength =  x.CallLength.ToString(),
                            CallforwardingMessage = x.CallforwardingMessage,
                            VoicemailMessage =x.VoicemailMessage

                        };
                        switch (x.Type)
                        {
                            case 2:
                                row.Capabilities = "<span data-tooltip='Facebook Messenger Capable'><i class='icon blue facebook messenger'></i></span>";
                                break;
                            case 3:
                                row.Capabilities = "<span data-tooltip='WhatsApp Capable'><i class='icon teal whatsapp'></i></span>";
                                break;
                            default:
                                row.Capabilities =
                                    "<span data-tooltip='Voice Capable'><i class='icon green phone'></i></span>" +
                                    "<span data-tooltip='SMS Capable'><i class='icon orange comments'></i></span>" +
                                    "<span data-tooltip='Fax Capable'><i class='icon grey fax'></i></span>" +
                                    "<span data-tooltip='MMS Capable'><i class='icon brown comment alternate'></i></span>";
                                break;
                        }
                        row.Actions = actions;
                        data.Add(row);
                    });
                    return Json(new { success = true, data }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult SearchNumber(
            string country = "US",
            bool? smsEnabled = null,
            bool? mmsEnabled = null,
            bool? voiceEnabled = null,
            bool? faxEnabled = null,
            string type = "local",
            string contains = null,
            int? areaCode = null,
            int? pageSize = null
            )
        {

            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

            TwilioClient.Init(accountSid, authToken);

            try
            {
                var data = new List<AvailableTwilioNumberTable>();
                var countryInfo = CountryResource.Fetch(country);
                CultureInfo ci = new CultureInfo("en-us");
                string actions = "<button class='ui tiny button buy blue'>Buy</button>";


                double localBasePrice = 0;
                double localCurrentPrice = 0;
                double tfBasePrice = 0;
                double tfCurrentPrice = 0;

                countryInfo.PhoneNumberPrices.ToList().ForEach(x =>
                {
                    if (x.NumberType == PhoneNumberPrice.TypeEnum.Local)
                    {
                        localBasePrice = x.BasePrice.Value;
                        localCurrentPrice = x.CurrentPrice.Value;
                    }
                    else if (x.NumberType == PhoneNumberPrice.TypeEnum.TollFree)
                    {
                        tfBasePrice = x.BasePrice.Value;
                        tfCurrentPrice = x.CurrentPrice.Value;
                    }
                });

                if (type == "local")
                {

                    var localAvailableNumbers = LocalResource.Read(
                        country,
                        smsEnabled: smsEnabled,
                        mmsEnabled: mmsEnabled,
                        voiceEnabled: voiceEnabled,
                        faxEnabled: faxEnabled,
                        contains: contains,
                        areaCode: areaCode,
                        pageSize: pageSize
                    );

                    localAvailableNumbers.ToList().ForEach(x =>
                    {
                        var row = new AvailableTwilioNumberTable();
                        row.PhoneNumber = x.PhoneNumber.ToString();
                        row.FriendlyName = "<strong>" + x.FriendlyName.ToString() + "</strong><br>" +
                            (string.IsNullOrEmpty(x.Locality) ? "" : x.Locality + ", ") + (string.IsNullOrEmpty(x.Region) ? "" : x.Region);
                        row.Type = "Local";
                        row.BasePrice = localBasePrice.ToString("C", ci);
                        row.CurrentPrice = localCurrentPrice.ToString("C", ci) + "/monthly";
                        row.VoiceEnabled = x.Capabilities.Voice ? "<span data-tooltip='Voice Capable'><i class='icon green phone'></i></span>" : "";
                        row.SMSEnabled = x.Capabilities.Sms ? "<span data-tooltip='SMS Capable'><i class='icon orange comments'></i></span>" : "";
                        //row.FaxEnabled = x.Capabilities.Fax ? "<span data-tooltip='Fax Capable'><i class='icon grey fax'></i></span>" : "";
                        row.MMSEnabled = x.Capabilities.Mms ? "<span data-tooltip='MMS Capable'><i class='icon brown comment alternate'></i></span>" : "";
                        row.Actions = actions;
                        data.Add(row);
                    });

                } else if(type == "tf")
                {
                    
                    var tfAvailableNumbers = TollFreeResource.Read(
                        country,
                        smsEnabled: smsEnabled,
                        mmsEnabled: mmsEnabled,
                        voiceEnabled: voiceEnabled,
                        faxEnabled: faxEnabled,
                        contains: contains,
                        areaCode: areaCode,
                        pageSize: pageSize
                    );


                    tfAvailableNumbers.ToList().ForEach(x =>
                    {
                        var row = new AvailableTwilioNumberTable();
                        row.PhoneNumber = x.PhoneNumber.ToString();
                        row.FriendlyName = "<strong>" + x.FriendlyName.ToString() + "</strong><br>" +
                            (string.IsNullOrEmpty(x.Locality) ? "" : x.Locality + ", ") + (string.IsNullOrEmpty(x.Region) ? "" : x.Region);
                        row.Type = "Toll-Free";
                        row.BasePrice = tfBasePrice.ToString("C", ci);
                        row.CurrentPrice = tfCurrentPrice.ToString("C", ci) + "/monthly";
                        row.VoiceEnabled = x.Capabilities.Voice ? "<span data-tooltip='Voice Capable'><i class='icon green phone'></i></span>" : "";
                        row.SMSEnabled = x.Capabilities.Sms ? "<span data-tooltip='SMS Capable'><i class='icon orange comments'></i></span>" : "";
                        //row.FaxEnabled = x.Capabilities.Fax ? "<span data-tooltip='Fax Capable'><i class='icon grey fax'></i></span>" : "";
                        row.MMSEnabled = x.Capabilities.Mms ? "<span data-tooltip='MMS Capable'><i class='icon brown comment alternate'></i></span>" : "";
                        row.Actions = actions;
                        data.Add(row);
                    });

                }


                return Json(new { success = true, data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message, data = new List<string>() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult BuyNumber(string phoneNumber, string country)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                    string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();
                    string hostEndpoint = ConfigurationManager.AppSettings["HostEndpoint"].ToString();

                    TwilioClient.Init(accountSid, authToken);

                    var incomingPhoneNumber = IncomingPhoneNumberResource.Create(
                        phoneNumber: new PhoneNumber(phoneNumber),
                        voiceMethod: Twilio.Http.HttpMethod.Post,
                        smsMethod: Twilio.Http.HttpMethod.Post,
                        voiceFallbackMethod: Twilio.Http.HttpMethod.Post,
                        smsFallbackMethod: Twilio.Http.HttpMethod.Post,
                        voiceUrl: new Uri(new Uri(hostEndpoint), "/call"),
                        smsUrl: new Uri(new Uri(hostEndpoint), "/sms"),
                        voiceFallbackUrl: new Uri("https://twimlets.com/message?Message%5B0%5D=Internal%20Server%20Occurred.%20Please%20Try%20Again%20Later.&"),
                        smsFallbackUrl: new Uri("https://twimlets.com/message?Message%5B0%5D=Internal%20Server%20Occurred.%20Please%20Try%20Again%20Later.&")
                    );

                    var twilioNumber = new TwilioNumber
                    {
                        CompanyId = SessionHandler.CompanyId,
                        Type = 1,
                        PhoneNumber = phoneNumber,
                        VoiceEnabled = incomingPhoneNumber.Capabilities.Voice,
                        SmsEnabled = incomingPhoneNumber.Capabilities.Sms,
                        MmsEnabled = incomingPhoneNumber.Capabilities.Mms,
                        CreatedDate = DateTime.Now,
                        Country = country
                    };

                    db.TwilioNumbers.Add(twilioNumber);
                    db.SaveChanges();
                    //twilioNumber.FaxEnabled = incomingPhoneNumber.Capabilities.Fax;


                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ReleaseNumber(string phoneNumber)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                        string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                        TwilioClient.Init(accountSid, authToken);

                        var twilioNumber = db.TwilioNumbers.Where(x => x.PhoneNumber == phoneNumber).FirstOrDefault();

                        if (twilioNumber != null)
                        {
                            IncomingPhoneNumberResource.Delete(pathSid: twilioNumber.Sid);
                            db.TwilioNumbers.Remove(twilioNumber);
                            db.SaveChanges();
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, message = "Phone Number doesn't exist" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (Exception err)
                {
                    return Json(new { success = false, message = err.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, message = "An error has occured. Please try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetBlockedPhoneNumbers()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        string actions = "<button class='ui button remove red'>Remove</button>";
                        List<BlockedPhoneNumberTable> data = new List<BlockedPhoneNumberTable>();

                        var blockedNumbers = db.BlockedPhoneNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                        blockedNumbers.ForEach(x =>
                        {
                            BlockedPhoneNumberTable row = new BlockedPhoneNumberTable
                            {
                                Id = x.Id,
                                PhoneNumber = x.PhoneNumber,
                                BlockedDate = x.BlockedDate,
                                Actions = actions
                            };
                            data.Add(row);
                        });
                        return Json(new { success = true, data }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception err)
                {
                    return Json(new { success = false, data = new List<string>(), message = err.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, data = new List<string>() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult BlockNumber(string phoneNumber)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        BlockedPhoneNumber number = new BlockedPhoneNumber
                        {
                            PhoneNumber = phoneNumber,
                            BlockedDate = DateTime.Now,
                            CompanyId = SessionHandler.CompanyId
                        };
                        db.BlockedPhoneNumbers.Add(number);
                        db.SaveChanges();
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception err)
                {
                    return Json(new { success = false, message = err.Message }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, message = "An error has occured. Please try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult RemoveBlockedNumber(string phoneNumber)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var number = db.BlockedPhoneNumbers.Where(x => x.PhoneNumber == phoneNumber && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                        if (number != null)
                        {
                            db.BlockedPhoneNumbers.Remove(number);
                            db.SaveChanges();
                            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = true, message = "Phone Number doesn't exist" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (Exception err)
                {
                    return Json(new { success = false, message = err.Message }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, message = "An error has occured. Please try again later" }, JsonRequestBehavior.AllowGet);
        }

        #region WorkerMessages
        //WorkerMessages
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult LoadWorkerInbox()
        {
            return Json(new { result = messageRepo.GetWorkerVendorInboxList() }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult LoadWorkerInboxMessage(string threadId,int workerId,bool isworker)
        {
            using (var db = new ApplicationDbContext())
            {
               var contactInfos = db.WorkerContactInfos.Where(x => x.WorkerId == workerId).ToList();
                var vendorContact = db.VendorContactNumbers.Where(x => x.VendorId == workerId).ToList();
                var hasDefaultWorkerContact = db.WorkerContactInfos.Where(x => x.WorkerId == workerId &&x.IsDefault).Select(x => x.IsDefault).FirstOrDefault();
                var hasDefaultVendorContact = db.VendorContactNumbers.Where(x => x.VendorId == workerId && x.IsDefault).Select(x => x.IsDefault).FirstOrDefault();
                return Json(new { hasDefaultVendorContact, hasDefaultWorkerContact, success = true, Messages = messageRepo.GetCommunicationInboxMessages(threadId), contactInfos =(GlobalVariables.UserWorkerId==0 ?(contactInfos):null), vendorContactInfos = vendorContact,isWorker=isworker }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult WorkerSendMessage(string threadId, int workerId,string messege,bool isworker)
        {
            using (var db = new ApplicationDbContext())
            {
                CommunicationThread workerThread;
                if (threadId == "0")
                {
                     workerThread= new CommunicationThread();
                    db.CommunicationThreads.Add(workerThread);
                    if (isworker)
                    {
                        workerThread.WorkerId = workerId;
                    }
                    else
                    {
                        workerThread.VendorId = workerId;
                    }
                    workerThread.ThreadId = PasswordGenerator.Generate(20);
                    workerThread.CompanyId = SessionHandler.CompanyId;
                    db.CommunicationThreads.Add(workerThread);
                    db.SaveChanges();
                }
                else
                {
                    workerThread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                }

                CommunicationMessage workerMessage = new CommunicationMessage();
                workerMessage.Message = messege;
                workerMessage.ThreadId = workerThread.ThreadId;
                workerMessage.DateCreated = DateTime.Now;
                workerMessage.UserId = GlobalVariables.UserId;
                db.CommunicationMessages.Add(workerMessage);
                db.SaveChanges();

                InboxHub.WorkerMessageReceived(workerThread.ThreadId,isworker);
                return Json(new { success=true,threadId = workerThread.ThreadId }, JsonRequestBehavior.AllowGet);

            }

        }

        //End of WorkerMessages


        [HttpGet]
        public ActionResult GetCommunicationThreadMessages(string threadId)
        {
            using (var db = new ApplicationDbContext()) {
                var thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                var HasEmail = thread.RenterId != null ? true : false;
                var contactInfos = thread.RenterId != null ? db.RenterContacts.Where(x => x.RenterId == thread.RenterId).Select(x=>new {Contact= x.Contact,Id=x.Id,IsDefault=x.IsDefault }).ToList() : thread.VendorId != null ? db.VendorContactNumbers.Where(x => x.VendorId == thread.VendorId).Select(x => new { Contact = x.Value, Id = x.Id, IsDefault = x.IsDefault }).ToList() : db.WorkerContactInfos.Where(x => x.WorkerId == thread.WorkerId).Select(x => new { Contact = x.Value, Id = x.Id, IsDefault = x.IsDefault }).ToList();
                var emails = thread.RenterId != null ? db.RenterEmails.Where(x => x.RenterId == thread.RenterId).Select(x => new { Email = x.Email, Id = x.Id, IsDefault = x.IsDefault }).ToList() : thread.VendorId != null ? db.VendorEmails.Where(x => x.VendorId == thread.VendorId).Select(x => new { Email = x.Email, Id = x.Id, IsDefault = x.IsDefault }).ToList() : db.WorkerEmails.Where(x => x.WorkerId == thread.WorkerId).Select(x => new { Email = x.Email, Id = x.Id, IsDefault = x.IsDefault }).ToList();
                var Messages = messageRepo.GetCommunicationInboxMessages(threadId);
                return Json(new { Messages = messageRepo.GetCommunicationInboxMessages(threadId),contactInfos,emails,HasEmail }, JsonRequestBehavior.AllowGet); 
            }

        }
        #endregion

    }
}