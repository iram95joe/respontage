﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using System.Diagnostics;
using Core.Database.Context;
using Core.Helper;

namespace MessagerSolution.Controllers
{
    public class AirlockController : Controller
    {
        //
        // GET: /Airlock/

        [HttpPost]
        public ActionResult GetHostAccountList()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return Json(new { success = true, host_accounts = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select h).ToList() }, JsonRequestBehavior.AllowGet);
                }
            }
            catch 
            {
                return Json(new { success = false, message = "Unable to access database" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AirLockChoice(string lockId, string userId, string phoneId, string phone, string email, string name, string imageUrl)
        {
            AirlockViewModel model = new AirlockViewModel();
            model.LockId = lockId;
            model.UserId = userId;
            model.PhoneId = phoneId;
            model.Email = email;
            model.PhoneNumber = phone;
            model.Name = name;
            model.ProfileImageUrl = imageUrl;
            return View(model);
        }

        [HttpGet]
        public ActionResult VRBOAirlock()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Index(string lockId, string userId, string phoneId, string username = "vancouverbb98@gmail.com", bool hasError = false)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where h.Username == username&& hc.CompanyId == SessionHandler.CompanyId select h).FirstOrDefault();
                TempData["Image"] = temp.ProfilePictureUrl;
                TempData["Name"] = temp.Firstname + " " + temp.Lastname;
                TempData["LockId"] = int.Parse(lockId);
                TempData["UserId"] = int.Parse(userId);
                TempData["PhoneId"] = int.Parse(phoneId);
                TempData["HasError"] = hasError;
                return View();
            }
        }

        //[HttpPost]
        //public ActionResult Check()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        List<Owner> owners = new List<Owner>();
        //        using(var db = new ApplicationDbContext())
        //        {
        //            owners = db.Owners.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
        //        }
        //        foreach (var account in owners)
        //        {
        //            ScrapeManager sm = SiteConstants.Scraper;
        //            var res = sm.Login(account.Username, Security.Decrypt(account.Password), true, true);
        //            if(!res.Success && res.AirLock != null)
        //            {
        //                return Json(new { success = true, has_airlock = true, airlock = res.AirLock, message = "Operation Successful" }, JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        var message = owners.Count > 0 ? "Operation Successful" : "No accounts in the database";
        //        return Json(new { success = true, has_airlock = false, message = message }, JsonRequestBehavior.AllowGet);
        //    }
        //    return Json(new { success = true, message = "Unauthenticated User" }, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public ActionResult AirLockChoice(string lockId, string userId, string phoneId, int choice, bool isNew)
        //{
        //    ScrapeManager sm = SiteConstants.Scraper;
        //    var result = sm.ProcessAirlockSelection(lockId,userId,phoneId,choice,isNew);
        //    return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public ActionResult Verify(string code, string lockId, string userId, string phoneId, int choice, bool isNew)
        //{
        //    ScrapeManager s = SiteConstants.Scraper;
        //    var result = s.ProcessAirlock(code, lockId, userId, phoneId, choice, isNew);
        //    if (result.Item1) { GetAirbnbHostAccountDetails(); }
        //    return Json(new { success = result.Item1 }, JsonRequestBehavior.AllowGet);
        //}

        //public void GetAirbnbHostAccountDetails()
        //{
        //    try
        //    {
        //        Trace.WriteLine("Get airbnb host account started..");
        //        SiteConstants.accounts.Clear();
        //        using (var db = new ApplicationDbContext())
        //        {
        //            db.Owners.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList().ForEach(item =>
        //            {
        //                AirbnbAccount a = new AirbnbAccount();
        //                a.Id = item.Id;
        //                a.Username = item.Username;
        //                a.Password = Security.Decrypt(item.Password);

        //                var entity = db.UserSessions.Where(x => x.OwnerId == item.Id).Where(x => x.IsActive == true).FirstOrDefault();
        //                if (entity != null)
        //                {
        //                    using (ScrapeManager sm = new ScrapeManager())
        //                    {
        //                        var isloaded = sm.ValidateTokenAndLoadCookies(entity.SessionId);
        //                        if (isloaded)
        //                        {
        //                            a.SessionToken = entity.SessionId;
        //                            SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                        }
        //                        else
        //                        {
        //                            db.UserSessions.RemoveRange(db.UserSessions.Where(x => x.OwnerId == entity.OwnerId).ToList());
        //                            db.SaveChanges();
        //                            var result = sm.Login(a.Username, a.Password, true);
        //                            a.SessionToken = result.SessionToken;
        //                            SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    using (ScrapeManager sm = new ScrapeManager())
        //                    {
        //                        var result = sm.Login(a.Username, a.Password, true);
        //                        a.SessionToken = result.SessionToken;
        //                        SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                    }
        //                }
        //            });
        //        }
        //        Trace.WriteLine("Get airbnb host account done..");
        //    }
        //    catch (Exception e)
        //    {
        //        Trace.WriteLine(e.ToString());
        //    }
        //}
	}
}