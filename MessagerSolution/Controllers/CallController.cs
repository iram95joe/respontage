﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.Jwt;
using Twilio.Jwt.Client;
using Twilio.TwiML;
using Twilio.TwiML.Voice;

namespace MessagerSolution.Controllers
{
    public class CallController : Controller
    {

        [HttpPost]
        public ActionResult Index(VoiceRequest request)
        {
            var response = new VoiceResponse();

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var TwilioNumber = db.TwilioNumbers.Where(x => x.PhoneNumber == request.To).FirstOrDefault();

                    if (TwilioNumber == null)
                    {
                        response.Hangup();
                        return new TwiMLResult(response);
                    }
                    List<string> formats = new List<string>();
                    formats.Add(request.From);
                    var phoneNumber = request.From;
                    if (phoneNumber[0] == '1')
                    {
                        formats.Add("+" + phoneNumber.Substring(1));
                    }
                    if (phoneNumber[0] == '0')
                    {
                        formats.Add(phoneNumber.Substring(1));
                    }
                    if (phoneNumber.Contains('(') || phoneNumber.Contains(')') || phoneNumber.Contains(' ') || phoneNumber.Contains('-'))
                    {
                        formats.Add(phoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                    }
                    var worker = db.Workers.Join(db.WorkerContactInfos, x => x.Id, y => y.WorkerId, (x, y) => new { Worker = x, WorkerContactInfo = y })
                    .Where(x => formats.Contains(x.WorkerContactInfo.Value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();


                    var vendor = db.Vendors.Join(db.VendorContactNumbers, x => x.Id, y => y.VendorId, (x, y) => new { Vendor = x, VendorContactInfo = y })
                        .Where(x => formats.Any(y => x.VendorContactInfo.Value.Contains(y))).FirstOrDefault();
                    
                    var renter = db.Renters.Join(db.RenterContacts, x => x.Id, y => y.RenterId, (x, y) => new { Renter = x, RenterContact = y })
                         .Where(x => formats.Contains(x.RenterContact.Contact.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();

                    //var renter = db.Renters.Where(x => formats.Contains(x.PhoneNumber)).FirstOrDefault();
                    if (renter!=null)
                    {
                        //var u = db.Users.Where(x => x.WorkerId == worker.Worker.Id).FirstOrDefault();
                        var thread = db.CommunicationThreads.Where(x => x.RenterId == renter.Renter.Id && x.CompanyId == TwilioNumber.CompanyId).FirstOrDefault();
                        CommunicationThread workerThread;
                        if (thread == null)
                        {
                            workerThread = new CommunicationThread();
                            workerThread.RenterId = renter.Renter.Id;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = TwilioNumber.CompanyId;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                        else
                        {
                            workerThread = db.CommunicationThreads.Where(x => x.ThreadId == thread.ThreadId).FirstOrDefault();
                        }
                        CommunicationCall call = new CommunicationCall();
                        call.From = request.From;
                        call.To = request.To;
                        call.FromCountry = request.FromCountry;
                        call.CallSid = request.CallSid;
                        call.CreatedDate = DateTime.Now;
                        //call.UserId = u.Id;
                        call.ThreadId = workerThread.ThreadId;
                        db.CommunicationCalls.Add(call);
                        db.SaveChanges();
                    }
                    else if (worker != null && worker.Worker != null)
                    {
                        var u = db.Users.Where(x => x.WorkerId == worker.Worker.Id).FirstOrDefault();
                        var thread = db.CommunicationThreads.Where(x => x.WorkerId == worker.Worker.Id && x.CompanyId == TwilioNumber.CompanyId).FirstOrDefault();
                        CommunicationThread workerThread;
                        if (thread == null)
                        {
                            workerThread = new CommunicationThread();
                            workerThread.WorkerId = worker.Worker.Id;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = TwilioNumber.CompanyId;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                        else
                        {
                            workerThread = db.CommunicationThreads.Where(x => x.ThreadId == thread.ThreadId).FirstOrDefault();
                        }
                        CommunicationCall call = new CommunicationCall();
                        call.From = request.From;
                        call.To = request.To;
                        call.FromCountry = request.FromCountry;
                        call.CallSid = request.CallSid;
                        call.CreatedDate = DateTime.Now;
                        call.UserId = u.Id;
                        call.ThreadId = workerThread.ThreadId;
                        db.CommunicationCalls.Add(call);
                        db.SaveChanges();

                    }
                    else
                    {
                        Call call = new Call();
                        call.From = request.From;
                        call.To = request.To;
                        call.FromCountry = request.FromCountry;
                        call.CallSid = request.CallSid;
                        call.CreatedDate = DateTime.Now;
                        call.IsMyMessage = false;

                        var guest = db.Guests.Join(db.GuestContactInfos, x => x.Id, y => y.GuestId, (x, y) => new { Guest = x, GuestContactInfo = y })
                            .Where(x => formats.Contains(x.GuestContactInfo.Value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();
                        if (guest != null)
                        {
                            call.GuestId = guest.GuestContactInfo.GuestId;
                        }

                        db.Calls.Add(call);
                        db.SaveChanges();
                    }


                    var checkIfBlocked = db.BlockedPhoneNumbers.Where(x => x.PhoneNumber == request.From && x.CompanyId == TwilioNumber.CompanyId).FirstOrDefault();

                    if (checkIfBlocked != null)
                    {
                        response.Hangup();
                        return new TwiMLResult(response);
                    }
                    var dial = new Dial(
                        action: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Events"),
                        method: Twilio.Http.HttpMethod.Post,
                        recordingStatusCallbackMethod: Twilio.Http.HttpMethod.Post,
                        recordingStatusCallback: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Recording"),
                        record: Dial.RecordEnum.RecordFromAnswer,
                        callerId: request.From,
                        timeout: TwilioNumber.CallLength
                    );

                    var users = db.Users.Where(x => x.CompanyId == TwilioNumber.CompanyId).ToList();

                    users.ForEach(x =>
                    {
                        dial.Client(x.Id.ToString());
                    });

                    response.Append(dial);
                    return new TwiMLResult(response);
                }
            }
            catch { }

            return new TwiMLResult();
        }

        [HttpPost]
        public ActionResult Outgoing(VoiceRequest request)
        {
            var response = new VoiceResponse();
            if (string.IsNullOrEmpty(request.To))
            {
                response.Say("Invalid Phone Number");
                return new TwiMLResult(response);
            }

            try
            {
                using (var db = new ApplicationDbContext())
                {

                    int userId;
                    int.TryParse(request.From, out userId);

                    var user = db.Users.Find(userId);
                    string phoneNumber = null;
                    
                    if (user != null)
                    {
                        //JM Get twilio number assign on property to use in call
                        var contact = db.GuestContactInfos.Where(x => x.Value == request.To).FirstOrDefault();
                        var tempInq = db.Inquiries.Where(x => x.GuestId == contact.GuestId).FirstOrDefault();
                        var tempProperty = db.Properties.Where(x => x.ListingId == tempInq.PropertyId).FirstOrDefault();

                        var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId && x.Id == tempProperty.TwilioNumberId).FirstOrDefault();
                        if (twilioNumber != null)
                        {
                            phoneNumber = twilioNumber.PhoneNumber;
                        }
                        else
                        {  //JM Get twilio number assign on host to use in call
                            var host = db.Hosts.Where(x => x.Id == tempProperty.HostId).FirstOrDefault();
                            twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId && x.Id == host.TwilioNumberId).FirstOrDefault();
                            if (twilioNumber != null)
                            {
                                phoneNumber = twilioNumber.PhoneNumber;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(phoneNumber))
                    {
                        response.Say("You have no registered Number");
                        return new TwiMLResult(response);
                    }

                    Call call = new Call
                    {
                        From = phoneNumber,
                        To = request.To,
                        CallSid = request.CallSid,
                        CreatedDate = DateTime.Now,
                        IsMyMessage = true
                    };

                    var guestContact = db.GuestContactInfos.Where(x => x.Value == call.To).FirstOrDefault();
                    if (guestContact != null)
                    {
                        call.GuestId = guestContact.GuestId;
                    }

                    db.Calls.Add(call);
                    db.SaveChanges();

                    response.Dial(
                        request.To,
                        callerId: phoneNumber,
                        action: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Events"),
                        method: Twilio.Http.HttpMethod.Post,
                        recordingStatusCallbackMethod: Twilio.Http.HttpMethod.Post,
                        recordingStatusCallback: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Recording"),
                        record: Dial.RecordEnum.RecordFromAnswer
                     );

                }
            }
            catch { }
            return new TwiMLResult(response);
        }

        [HttpPost]
        public ActionResult VoiceMail(VoiceRequest request)
        {
            var response = new VoiceResponse();

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    response.Say("Hello. Please leave a message after the beep.");
                    response.Record(
                        action: new Uri("http://twimlets.com/message?Message%5B0%5D=Thank%20you%20for%20calling.&"),
                        recordingStatusCallbackMethod: Twilio.Http.HttpMethod.Post,
                        recordingStatusCallback: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Recording"),
                        transcribe: true,
                        playBeep: true
                    );

                    return new TwiMLResult(response);
                }
            }
            catch { }


            return new TwiMLResult();
        }



        [HttpPost]
        public ActionResult Recording(VoiceRequest request)
        {
            var response = new VoiceResponse();
            var parameters = Request.Params;
            var recordingStartTime = parameters["RecordingStartTime"];

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var call = db.Calls.Where(x => x.CallSid == request.CallSid).FirstOrDefault();
                    if (call != null)
                    {
                        call.RecordingDuration = request.RecordingDuration.ToInt();
                        call.RecordingSid = request.RecordingSid;
                        call.RecordingStartTime = recordingStartTime.ToDateTime();
                        call.RecordingUrl = request.RecordingUrl;
                        db.Entry(call).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        var CommunicationCalls = db.CommunicationCalls.Where(x => x.CallSid == request.CallSid).FirstOrDefault();
                        CommunicationCalls.RecordingDuration = request.RecordingDuration.ToInt();
                        CommunicationCalls.RecordingSid = request.RecordingSid;
                        CommunicationCalls.RecordingStartTime = recordingStartTime.ToDateTime();
                        CommunicationCalls.RecordingUrl = request.RecordingUrl;
                        db.Entry(CommunicationCalls).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            catch(Exception e){

            }


            return new TwiMLResult();
        }



        [HttpPost]
        public ActionResult Events(VoiceRequest request)
        {
            var response = new VoiceResponse();

         

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var call = db.Calls.Where(x => x.CallSid == request.CallSid).FirstOrDefault();
                    if (call != null)
                    {
                        call.CallDuration = request.DialCallDuration.ToInt();
                        db.Entry(call).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        var CommunicationCalls = db.CommunicationCalls.Where(x => x.CallSid == request.CallSid).FirstOrDefault();
                        CommunicationCalls.CallDuration = request.DialCallDuration.ToInt();
                        db.Entry(CommunicationCalls).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        Core.Helper.Company.Communication.ChargeUsage(CommunicationCalls.Id, false);
                    }
                    string voiceMessage = "Hello. Please leave a message after the beep.";
                    string forwardMessage = "Please wait while we forward you to the local number";
                    if (request.CallStatus != "completed" && request.CallStatus != "in-progress")
                    {
                            var TwilioNumber = db.TwilioNumbers.Where(x => x.PhoneNumber == request.To).FirstOrDefault();
                            if (TwilioNumber.VoicemailMessage != "" && TwilioNumber.VoicemailMessage != null)
                            {
                                voiceMessage = TwilioNumber.VoicemailMessage;
                            }
                            if (TwilioNumber.CallforwardingMessage != "" && TwilioNumber.CallforwardingMessage != null)
                            {
                                forwardMessage = TwilioNumber.CallforwardingMessage;
                            }
                            if (TwilioNumber.CallForwardingNumber == null)
                            {
                                response.Say(voiceMessage);
                                response.Record(
                                    action: new Uri("http://twimlets.com/message?Message%5B0%5D=Thank%20you%20for%20calling.&"),
                                    recordingStatusCallbackMethod: Twilio.Http.HttpMethod.Post,
                                    recordingStatusCallback: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Recording"),
                                    transcribe: true,
                                    playBeep: true
                                );
                                return new TwiMLResult(response);
                            }
                            else
                            {
                                response.Say(forwardMessage);
                                response.Dial(
                                    TwilioNumber.CallForwardingNumber,
                                    method: Twilio.Http.HttpMethod.Post,
                                    recordingStatusCallbackMethod: Twilio.Http.HttpMethod.Post,
                                    recordingStatusCallback: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Recording"),
                                    action: new Uri(new Uri(ConfigurationManager.AppSettings["HostEndpoint"].ToString()), "/call/Events"),
                                    record: Dial.RecordEnum.RecordFromAnswer
                                 );
                                return new TwiMLResult(response);
                            }
                        
                    }

                }
            }
            catch { }


            return new TwiMLResult();
        }

         [HttpGet]
         public ActionResult GetContactInfo(string ContactNumber)
         { string Name = "";
          string Image = "";

            using (var db = new ApplicationDbContext())
            {
                List<string> formats = new List<string>();
                formats.Add(ContactNumber);
                var phoneNumber = ContactNumber;
                if (phoneNumber[0] == '1')
                {
                    formats.Add("+" + phoneNumber.Substring(1));
                }
                if (phoneNumber[0] == '0')
                {
                    formats.Add(phoneNumber.Substring(1));
                }
                if (phoneNumber.Contains('(') || phoneNumber.Contains(')') || phoneNumber.Contains(' ') || phoneNumber.Contains('-'))
                {
                    formats.Add(phoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                }

                var guest = db.Guests.Join(db.GuestContactInfos, x => x.Id, y => y.GuestId, (x, y) => new { Guest = x,GuestContactInfo = y })
                    .Where(x => formats.Any(y => x.GuestContactInfo.Value.Contains(y))).FirstOrDefault();

                var worker = db.Workers.Join(db.WorkerContactInfos, x => x.Id, y => y.WorkerId, (x, y) => new { Worker = x, WorkerContactInfo = y })
                     .Where(x => formats.Any(y => x.WorkerContactInfo.Value.Contains(y))).FirstOrDefault();

                var vendor = db.Vendors.Join(db.VendorContactNumbers, x => x.Id, y => y.VendorId, (x, y) => new { Vendor = x, VendorContactInfo = y })
                    .Where(x => formats.Any(y => x.VendorContactInfo.Value.Contains(y))).FirstOrDefault();
                var renter = db.Renters.Where(x => formats.Contains(x.PhoneNumber)).FirstOrDefault();
                if (renter != null)
                {
                    Name = renter.Firstname + " " + renter.Lastname;
                }
                else if (worker != null)
                {
                    Name = worker.Worker.Firstname + " " + worker.Worker.Lastname;
                    Image = worker.Worker.ImageUrl;
                }
                else if (guest != null)
                {
                    Name = guest.Guest.Name;
                    Image = guest.Guest.ProfilePictureUrl;
                }
                else if (vendor != null)
                {
                    Name = vendor.Vendor.Name;
                }

            }
            return Json(new { Name = Name, Image = Image }, JsonRequestBehavior.AllowGet);
         
        }
        [HttpPost]
        public ActionResult Token()
        {
            string userId = "";
            if (User.Identity.IsAuthenticated)
            {
                var result = Core.Repository.CommonRepository.GetCurrentUserDetail(Convert.ToInt32(User.Identity.Name));
                userId = result.Id.ToString();
            } else
            {
                userId = "dev";
            }


            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();
            string appSid = ConfigurationManager.AppSettings["TwilioAppSid"].ToString();

            var scopes = new HashSet<IScope>
            {
                new OutgoingClientScope(appSid),
                new IncomingClientScope(userId)
            };
            var capability = new ClientCapability(accountSid, authToken, scopes: scopes);

            return Json(new { token = capability.ToJwt(), userId });
        }
    }
}