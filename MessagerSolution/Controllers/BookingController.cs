﻿using Airbnb;
using Core;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using MessagerSolution.Models.Contract;
using MessagerSolution.Models.Message;
using MessagerSolution.Models.Notices;
using MlkPwgen;
using MoreLinq;
using Newtonsoft.Json.Linq;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Spire.Doc;
using Spire.Doc.Documents;
using DocumentFormat.OpenXml.ExtendedProperties;
using OpenXmlPowerTools;
using Spire.Doc.Fields;
using MessagerSolution.Helper.SignRequestSettings;

namespace MessagerSolution.Controllers
{
    public class BookingController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        private static Airbnb.ScrapeManager _AirbnbScrapeManager = null;
        public static Airbnb.ScrapeManager AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb.ScrapeManager();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }
        private readonly Service.EmailService _emailService = new Service.EmailService();
        private readonly Service.PayPalService _payPalService = new Service.PayPalService();
        private readonly Service.SquareService _squarePaymentService = new Service.SquareService();
        //
        // GET: /Booking/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int companyId)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Booking") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //ViewBag.InquiryList = objIList.GetInquiryList();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            //ViewBag.FeeType = ExpenseGetFeeTypeList
            ViewBag.property = new SelectList(GetChildPropertyList(), "Id", "Name");
            ViewBag.PropertyList = objIList.GetProperties();//GetPropertyList();   
            ViewBag.StatusList = GetStatusList();
            ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();
            ViewBag.BookingStatus = new SelectList(objIList.GetAllBookingStatus(), "Code", "Description");
            ViewBag.SiteTypeList = new SelectList(objIList.GetAllSiteType(), "Code", "Description");
            return View();
        }
        [HttpGet]
        public ActionResult GetContract()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<ContractViewModel>();
                var documents =db.ContractTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach (var document in documents)
                {
                    list.Add(new ContractViewModel()
                    {
                        Id = document.Id,
                        Name = document.Name,
                        Actions =
                                        "<button class=\"ui mini button edit-contract-template\" data-tooltip=\"Edit\">" +
                                        "<i class=\"icon edit outline\"></i></button>"
                                        + "<a href=\"" + document.Path + "\"><button class=\"ui mini button\" data-tooltip=\"Download\">" +
                                        "<i class=\"icon download\"></i></button></a>" +
                                        "<button class=\"ui mini button delete-contract-template\" data-tooltip=\"Delete\">" +
                                        "<i class=\"icon trash\"></i></button>"
                    });
                }
               return Json(new { data = list }, JsonRequestBehavior.AllowGet);
               

            }
        }
        public JsonResult GetTemplateDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var template = db.ContractTemplates.Where(x => x.Id == id).FirstOrDefault();
                Document doc = new Document(Server.MapPath("~" + template.Path));
                var filename = "/Documents/DocumentHtml/" + template.Name + ".html";
                var htmlpath = Server.MapPath("~" + filename);
                doc.SaveToFile(htmlpath);
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority + filename;

                template.Html = AirbnbScrapper.Test(url);
                return Json(new { success = true, template }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CreateTemplate(CreateTemplateModel Template, HttpPostedFileWrapper[] Files)
        {

            if (Files != null)
            {
                foreach (var File in Files)
                {
                    if (File != null)
                    {
                        var fileName = Path.GetFileName(File.FileName);
                        string url = "~/Documents/Contract/" + fileName;
                        File.SaveAs(Server.MapPath(url));
                        url = url.Replace("~", "");
                        using (var db = new ApplicationDbContext())
                        {
                            db.ContractTemplates.Add(new ContractTemplate() { Name = Path.GetFileNameWithoutExtension(File.FileName), Path = url, CompanyId = SessionHandler.CompanyId,ListingId=Template.PropertyId });
                            db.SaveChanges();

                        }
                    }
                }
            }
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ContractTemplates.Where(x => x.Id == Template.Id).FirstOrDefault();
                if (Template.DocumentName != "" && Template.DocumentName != null)
                {

                    var file = "";
                    var path = "";
                    Document doc = new Document();
                    file = "/Documents/Contract/" + Template.DocumentName + ".docx";
                    path = Server.MapPath("~" + file);
                    Section s = doc.AddSection();
                    Paragraph para = s.AddParagraph();
                    para.AppendBookmarkStart("content");
                    para.AppendHTML(Template.Content.Trim());
                    para.AppendBookmarkEnd("content");
                    doc.SaveToFile(path, FileFormat.Docx);



                    if (Template.Id == 0)
                    {
                        db.ContractTemplates.Add(new ContractTemplate() { Name = Template.DocumentName, Path = file, CompanyId = SessionHandler.CompanyId, ListingId = Template.PropertyId });
                        db.SaveChanges();
                    }
                    else
                    {

                        temp.Name = Template.DocumentName;
                        temp.Path = file;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteContractTemplate(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                db.ContractTemplates.Remove(db.ContractTemplates.Where(x => x.Id == id).FirstOrDefault());
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetAirbnbBookingDetails(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var result = db.Inquiries
                        .Join(db.Properties, x => x.PropertyId, y => y.ListingId, (x, y) => new { Inquiry = x, Property = y })
                        .Join(db.Guests, x => x.Inquiry.GuestId, y => y.Id, (x, y) => new { Inquiry = x.Inquiry, Property = x.Property, Guest = y })
                        .Where(x => x.Inquiry.Id == id)
                        .FirstOrDefault();

                    if (result != null)
                    {
                        var expenses = db.Expenses
                            .Join(db.FeeTypes, x => x.FeeTypeId, y => y.Id, (x, y) => new { Expenses = x, FeeType = y })
                            .Where(x => x.Expenses.BookingId == result.Inquiry.Id)
                            .ToList();

                        var propertyFees = db.PropertyFees.Join(db.FeeTypes, x => x.FeeTypeId, y => y.Id, (x, y) => new { PropertyFees = x, FeeTypes = y }).Where(x => x.PropertyFees.PropertyId == result.Inquiry.PropertyId).ToList();

                        return Json(new { success = true, booking = result, expenses = expenses, propertyFees = propertyFees }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, message = "No booking match with the id" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetBookingList(string propertyFilter, string monthYearFilter, string checkInDateFilter, string statusFilter)
        {
            try
            {
                int propertyId = 0;
                int.TryParse(propertyFilter, out propertyId);

                DateTime bookingDate;
                DateTime checkInDate;
                DateTime.TryParse(monthYearFilter, out bookingDate);
                DateTime.TryParse(checkInDateFilter, out checkInDate);
                var sessionCompanyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    List<int> propertyIds = new List<int>();
                    if (propertyId != 0)
                    {
                        var property = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault();

                        if (property.IsParentProperty)
                        {
                            propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == property.Id).Select(x => x.Id).ToList());
                        }
                        else { propertyIds.Add(property.Id); }
                    }
                    var propertyList = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where hc.CompanyId == sessionCompanyId select p).ToList();
                    propertyList.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 0).ToList());
                    List<BookingList> bl = new List<BookingList>();

                    var localbooking = (from p in db.Properties join i in db.Inquiries on p.ListingId equals i.PropertyId where p.CompanyId == sessionCompanyId && p.SiteType == 0 select new { Property = p, Inquiry = i }).ToList();
                    localbooking.ForEach(x => {
                        BookingList item = new BookingList();
                        item.HostingAccountId = x.Inquiry.HostId;
                        item.ThreadId = x.Inquiry.ThreadId;
                        item.Id = x.Inquiry.Id;
                        item.PropertyId = x.Property.Id;
                        var parentproperty = db.ParentChildProperties.Where(y => y.ChildPropertyId == x.Property.Id && y.CompanyId == SessionHandler.CompanyId && y.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (parentproperty != null)
                        {
                            item.ParentPropertyId = parentproperty.ParentPropertyId;
                        }
                        var prop = propertyList.Where(p => p.Id == item.PropertyId).FirstOrDefault();
                        if (prop != null)
                        {
                            if (parentproperty == null)
                            {
                                item.PropertyName = x.Property.Name;
                            }
                            else
                            {
                                var parentProperty = propertyList.Where(p => p.Id == parentproperty.ParentPropertyId).FirstOrDefault();
                                if (parentProperty != null) { item.PropertyName = parentProperty.Name; }
                                else { item.PropertyName = x.Property.Name; }
                            }
                        }
                        var timezone = x.Property.TimeZoneName;

                        item.Site = ((Core.Enumerations.SiteType)x.Inquiry.SiteType).ToString();
                        var renters = db.Renters.Where(y => y.BookingId == x.Inquiry.Id && y.IsActive).ToList();
                        //item.GuestId = x.Guest.GuestId;
                        foreach (var renter in renters)
                        {
                            item.GuestName += renter.Firstname+" "+renter.Lastname;
                        }
                        item.InquiryDate = x.Inquiry.InquiryDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.InquiryDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.InquiryDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.BookingDate = x.Inquiry.BookingDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.BookingDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.BookingDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.CheckInDate = x.Inquiry.CheckInDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.CheckInDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.CheckOutDate = x.Inquiry.CheckOutDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.CheckOutDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.ReservationCost = x.Inquiry.ReservationCost == 0 ? " - " : string.Format("{0:#,0.00}", x.Inquiry.ReservationCost);
                        item.TotalPayout = x.Inquiry.TotalPayout == 0 ? " - " : string.Format("{0:#,0.00}", x.Inquiry.TotalPayout);
                        item.StatusCode = x.Inquiry.BookingStatusCode;
                        item.Status = db.BookingStatus.Where(y => y.Code == x.Inquiry.BookingStatusCode).FirstOrDefault().Description;
                        item.ConfirmationCode = x.Inquiry.ConfirmationCode;
                        item.IsAltered = x.Inquiry.IsAltered;
                        if (x.Inquiry.BookingStatusCode == "A")
                        {
                            item.Actions = "<a class='ui mini button edit-airbnb-booking' title='Edit'><i class='icon edit'></i></a>";
                        }
                        else if (x.Inquiry.SiteType == 1)
                        {
                            item.Actions = "<a class='ui mini button js-btn_create-new-booking edit-booking' title='Edit'><i class='icon edit'></i></a>" +
                                           "<a class='ui mini button delete-booking' title='Delete'><i class='icon delete'></i></a>";
                        }
                        bl.Add(item);
                    });

                    var inquiry = db.Inquiries
                        .Join(db.Hosts, x => x.HostId, y => y.Id, (x, y) => new { Inquiry = x, Host = y })
                        .Join(db.HostCompanies, x => x.Host.Id, y => y.HostId, (x, y) => new { Inquiry = x.Inquiry, Host = x.Host, HostCompany = y, })
                        .Join(db.BookingStatus, x => x.Inquiry.BookingStatusCode, y => y.Code, (x, y) => new { HostCompany = x.HostCompany, Inquiry = x.Inquiry, BookingStatus = y, Host = x.Host })
                        .Join(db.Properties, x => x.Inquiry.PropertyId, y => y.ListingId, (x, y) => new { HostCompany = x.HostCompany, Inquiry = x, Property = y, BookingStatus = x.BookingStatus })
                        .Join(db.Guests, x => x.Inquiry.Inquiry.GuestId, y => y.Id, (x, y) => new { HostCompany = x.HostCompany, Inquiry = x.Inquiry.Inquiry, Property = x.Property, Guest = y, BookingStatus = x.BookingStatus })
                        .Where(x => x.HostCompany.CompanyId == sessionCompanyId)
                        .Where(x => (propertyId == 0 ? true : propertyIds.Contains(x.Property.Id)))
                        .Where(x => (statusFilter == "all" ? true : (x.Inquiry.BookingStatusCode.ToUpper() == statusFilter)))
                        .Where(x => (string.IsNullOrEmpty(monthYearFilter) ? true : (x.Inquiry.BookingDate.HasValue && x.Inquiry.BookingDate.Value.Month == bookingDate.Month && x.Inquiry.BookingDate.Value.Year == bookingDate.Year)))
                        .Where(x => (string.IsNullOrEmpty(checkInDateFilter) ? true : (x.Inquiry.CheckInDate.HasValue && x.Inquiry.CheckInDate.Value.Month == checkInDate.Month && x.Inquiry.CheckInDate.Value.Year == checkInDate.Year)))
                        .OrderByDescending(x => x.Inquiry.CheckOutDate)
                        .ToList();

                    var inbox = db.Inbox
                    .Join(db.Hosts, x => x.HostId, y => y.Id, (x, y) => new { Inbox = x, Host = y })
                    .Join(db.HostCompanies, x => x.Host.Id, y => y.HostId, (x, y) => new { Inbox = x.Inbox, Host = x.Host, HostCompany = y, })
                    .Join(db.BookingStatus, x => x.Inbox.Status, y => y.Code, (x, y) => new { Inbox = x.Inbox, BookingStatus = y, Host = x.Host, HostCompany = x.HostCompany })
                    .Join(db.Properties, x => x.Inbox.PropertyId, y => y.ListingId, (x, y) => new { Inbox = x.Inbox, Property = y, BookingStatus = x.BookingStatus, Host = x.Host, HostCompany = x.HostCompany })
                    .Join(db.Guests, x => x.Inbox.GuestId, y => y.Id, (x, y) => new { Inbox = x.Inbox, Property = x.Property, Guest = y, BookingStatus = x.BookingStatus, Host = x.Host, HostCompany = x.HostCompany })
                    .Where(x => x.HostCompany.CompanyId == sessionCompanyId)
                    .Where(x => (propertyId == 0 ? true : propertyIds.Contains(x.Property.Id)))
                    .Where(x => (statusFilter == "all" ? true : (x.Inbox.Status.ToUpper() == statusFilter)))
                    .Where(x => (string.IsNullOrEmpty(checkInDateFilter) ? true : (x.Inbox.CheckInDate.HasValue && x.Inbox.CheckInDate.Value.Month == checkInDate.Month && x.Inbox.CheckInDate.Value.Year == checkInDate.Year)))
                    .Where(x => x.Inbox.Status == "I" || x.Inbox.Status == "NP")
                    .OrderByDescending(x => x.Inbox.CheckOutDate)
                    .ToList();
                    if (statusFilter != "all" && statusFilter != "I")
                    {
                        inbox.Clear();
                    }

                    inbox.ForEach(x =>
                    {
                        BookingList item = new BookingList();
                        item.HostingAccountId = x.Inbox.HostId;
                        item.ThreadId = x.Inbox.ThreadId;
                        item.Id = x.Inbox.Id.ToInt();
                        item.PropertyId = x.Property.Id;
                        var parentproperty = db.ParentChildProperties.Where(y => y.ChildPropertyId == x.Property.Id && y.CompanyId == SessionHandler.CompanyId && y.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (parentproperty != null)
                        {

                            item.ParentPropertyId = parentproperty.ParentPropertyId;
                        }
                        var prop = propertyList.Where(p => p.Id == item.PropertyId).FirstOrDefault();
                        if (prop != null)
                        {
                            if (parentproperty == null)
                            {
                                item.PropertyName = x.Property.Name;
                            }
                            else
                            {
                                var parentProperty = propertyList.Where(p => p.Id == parentproperty.ParentPropertyId).FirstOrDefault();
                                if (parentProperty != null) { item.PropertyName = parentProperty.Name; }
                                else { item.PropertyName = x.Property.Name; }
                            }
                        }
                        var timezone = x.Property.TimeZoneName;

                        item.Site = ((Core.Enumerations.SiteType)x.Inbox.SiteType).ToString();
                        item.GuestId = x.Guest.GuestId;
                        item.GuestName = "<img class=\"ui mini avatar image\" src=\"" + (x.Guest.ProfilePictureUrl != null ? x.Guest.ProfilePictureUrl : Core.Helper.Utilities.GetDefaultAvatar(x.Guest.Name)) + "\">" + (x.Guest.FullName != "" && x.Guest.FullName != null ? x.Guest.FullName : x.Guest.Name);
                        item.CheckInDate = x.Inbox.CheckInDate == null ? (DateTime?)null : (timezone == null ? x.Inbox.CheckInDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inbox.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.CheckOutDate = x.Inbox.CheckOutDate == null ? (DateTime?)null : (timezone == null ? x.Inbox.CheckOutDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inbox.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.StatusCode = x.Inbox.Status;
                        item.Status = x.BookingStatus.Description;
                        item.InquiryDate = x.Inbox.InquiryDate.HasValue ? x.Inbox.InquiryDate.ToDateTime() : (DateTime?)null;
                        item.ReservationCost = x.Inbox.RentalFee == 0 ? " - " : "<div class=\"ui accordion\">"+
                                                                                        "<div class=\"title\">"+
                                                                                            "<i class=\"dropdown icon\"></i>"+ string.Format("{0:#,0.00}", x.Inbox.RentalFee) +
                                                                                        "</div>"+
                                                                                        "<div class=\"content\">"+
                                                                                            "<p class=\"transition hidden\">"+x.Inbox.SummaryFee+"</p>"+
                                                                                        "</div>"+
                                                                                    "</div>";
                        item.TotalPayout = x.Inbox.TotalPayout == 0 ? " - " : string.Format("{0:#,0.00}", x.Inbox.TotalPayout);
                        //if (x.Inbox.Status == "A")
                        //{
                        //    item.Actions = "<a class='ui mini button edit-airbnb-booking' title='Edit'><i class='icon edit'></i></a>";
                        //}
                        //else if (x.Inbox.SiteType == 1)
                        //{
                        //    item.Actions = "<a class='ui mini button js-btn_create-new-booking edit-booking' title='Edit'><i class='icon edit'></i></a>" +
                        //                   "<a class='ui mini button delete-booking' title='Delete'><i class='icon delete'></i></a>";
                        //}
                        bl.Add(item);
                    });

                    inquiry.ForEach(x =>
                    {
                        BookingList item = new BookingList();
                        item.HostingAccountId = x.Inquiry.HostId;
                        item.ThreadId = x.Inquiry.ThreadId;
                        item.Id = x.Inquiry.Id;
                        item.PropertyId = x.Property.Id;
                        var parentproperty = db.ParentChildProperties.Where(y => y.ChildPropertyId == x.Property.Id && y.CompanyId == SessionHandler.CompanyId && y.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (parentproperty != null)
                        {
                            item.ParentPropertyId = parentproperty.ParentPropertyId;
                        }
                        var prop = propertyList.Where(p => p.Id == item.PropertyId).FirstOrDefault();
                        if (prop != null)
                        {
                            if (parentproperty == null)
                            {
                                item.PropertyName = x.Property.Name;
                            }
                            else
                            {
                                var parentProperty = propertyList.Where(p => p.Id == parentproperty.ParentPropertyId).FirstOrDefault();
                                if (parentProperty != null) { item.PropertyName = parentProperty.Name; }
                                else { item.PropertyName = x.Property.Name; }
                            }
                        }
                        var timezone = x.Property.TimeZoneName;
                        var income = db.Income.Where(y => y.BookingId == x.Inquiry.Id).FirstOrDefault();
                        if (income != null)
                        {
                            var payments = db.IncomePayment.Where(y => y.IncomeId == income.Id).ToList();
                            foreach (var payment in payments)
                            {
                                item.Payments += payment.Amount + "-" + payment.CreatedAt.ToString("MMM dd,yyyy")+"<br/>";
                            }
                            var balance = (x.Inquiry.TotalPayout - payments.Sum(y => y.Amount));
                           item.Balance= "<p style=\"color:"+(balance<0.0M? "#008000" : balance == 0.0M?"": "#FF0000") +"\">"+Math.Abs(balance)+ "</p>";
                        }

                        item.Site = ((Core.Enumerations.SiteType)x.Inquiry.SiteType).ToString();
                        item.GuestId = x.Guest.GuestId;
                        item.GuestName = "<img class=\"ui mini avatar image\" src=\""+(x.Guest.ProfilePictureUrl!=null? x.Guest.ProfilePictureUrl : Core.Helper.Utilities.GetDefaultAvatar(x.Guest.Name)) +"\">"+(x.Guest.FullName!="" && x.Guest.FullName!=null?x.Guest.FullName:x.Guest.Name);
                        item.InquiryDate = x.Inquiry.InquiryDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.InquiryDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.InquiryDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.BookingDate = x.Inquiry.BookingDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.BookingDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.BookingDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.CheckInDate = x.Inquiry.CheckInDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.CheckInDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.CheckOutDate = x.Inquiry.CheckOutDate == null ? (DateTime?)null : (timezone == null ? x.Inquiry.CheckOutDate.ToDateTime() : TimeZoneInfo.ConvertTimeFromUtc(x.Inquiry.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(timezone)));
                        item.ReservationCost = x.Inquiry.ReservationCost == 0 ? " - " :  "<div class=\"ui accordion\">"+
                                                                                        "<div class=\"title\">"+
                                                                                            "<i class=\"dropdown icon\"></i>"+ string.Format("{0:#,0.00}", x.Inquiry.ReservationCost) +
                                                                                        "</div>"+
                                                                                        "<div class=\"content\">"+
                                                                                            "<p class=\"transition hidden\">"+x.Inquiry.SummaryFee+"</p>"+
                                                                                        "</div>"+
                                                                                    "</div>";
                        item.TotalPayout = x.Inquiry.TotalPayout == 0 ? " - " : string.Format("{0:#,0.00}", x.Inquiry.TotalPayout);
                        item.StatusCode = x.Inquiry.BookingStatusCode;
                        item.Status = x.BookingStatus.Description;
                        item.ConfirmationCode = x.Inquiry.ConfirmationCode;
                        item.IsAltered = x.Inquiry.IsAltered;


                        if (x.Inquiry.BookingStatusCode == "A")
                        {
                            item.Actions = "<a class='ui mini button edit-airbnb-booking' title='Edit'><i class='icon edit'></i></a>";
                        }
                        else if (x.Inquiry.SiteType == 1)
                        {
                            item.Actions = "<a class='ui mini button js-btn_create-new-booking edit-booking' title='Edit'><i class='icon edit'></i></a>" +
                                           "<a class='ui mini button delete-booking' title='Delete'><i class='icon delete'></i></a>";
                        }
                        bl.Add(item);
                    });


                    var jsonData= Json(new { data = bl }, JsonRequestBehavior.AllowGet);
                    jsonData.MaxJsonLength = int.MaxValue;
                    return jsonData;
                }
            }
            catch (Exception)
            {
                return Json(new { data = new List<BookingList>() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpcommingBooking()
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = SessionHandler.CompanyId;
                var reservations = (from reservation in db.Inquiries join host in db.Hosts on reservation.HostId equals host.Id join hostCompany in db.HostCompanies on host.Id equals hostCompany.HostId join property in db.Properties on reservation.PropertyId equals property.ListingId join guest in db.Guests on reservation.GuestId equals guest.Id where hostCompany.CompanyId ==companyId && reservation.IsActive select new {Booking = reservation, Property = property,Guest =guest, Host = host }).ToList();
                List<UpcommingBooking> data = new List<UpcommingBooking>();
                foreach(var reservation in reservations)
                {
                    var signedContract = db.ContractDocuments.Where(x => x.Type == (int)Core.Enumerations.ContractType.Signed && x.BookingId == reservation.Booking.Id).FirstOrDefault();
                    
                        data.Add(new Models.UpcommingBooking()
                        {
                            Id = reservation.Booking.Id,
                            Site = ((Core.Enumerations.SiteType)reservation.Booking.SiteType).ToString(),
                            Property = reservation.Property.Name,
                            GuestName = string.IsNullOrEmpty(reservation.Guest.FullName) ? reservation.Guest.Name : reservation.Guest.FullName,
                            CheckInDate = TimeZoneInfo.ConvertTimeFromUtc(reservation.Booking.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(reservation.Property.TimeZoneName)),
                            CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc(reservation.Booking.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(reservation.Property.TimeZoneName)),
                            Actions = "<button class=\"ui mini button view-contract\" data-tooltip=\"View Contract\">" +
                                            "<i class=\"icon pen square\"></i></button>" +
                                            (signedContract != null ? "<a href=\"" + signedContract.AttachmentPath + "\" target=\"_blank\"><button class=\"ui mini button\" data-tooltip=\"Signed Contract\">" +
                                            "<i class=\"icon folder open outline\"></i></button></a>" : "")
                        });
                }
                
                return Json(new {data}, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetContractTemplateWithData(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    int generatedDocumentId = 0;
                    var userId = User.Identity.Name.ToInt();
                    var reservation = db.Inquiries.Where(x => x.Id == id).FirstOrDefault();
                    var property = db.Properties.Where(x => x.ListingId == reservation.PropertyId).FirstOrDefault();
                    var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                    var guest = db.Guests.Where(x => x.Id == reservation.GuestId).FirstOrDefault();
                    var emailData = db.GuestEmails.Where(x => x.GuestId == guest.GuestId).FirstOrDefault();
                    string email = "";
                    if (emailData != null)
                    {
                        email = emailData.Email;
                    }
                    var host = db.Hosts.Where(x => x.Id == reservation.HostId).FirstOrDefault();
                    ContractTemplate template = null;
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == user.CompanyId).FirstOrDefault();
                    //The priority of templates are from Parent,child,default
                    if (parentChild != null)
                    {//Get template of parent property
                        var parent = db.Properties.Where(x => x.Id == parentChild.ParentPropertyId).FirstOrDefault();
                        template = db.ContractTemplates.Where(x => x.ListingId == parent.ListingId && x.CompanyId == user.CompanyId).FirstOrDefault();
                    }
                    if (template == null)
                    {//Get template of child property
                        template = db.ContractTemplates.Where(x => x.ListingId == property.ListingId && x.CompanyId == user.CompanyId).FirstOrDefault();
                    }
                    if (template == null)
                    {//Get Default template property
                        template = db.ContractTemplates.Where(x => x.TemplateType == 0).FirstOrDefault();
                    }

                    if (template != null)
                    {
                        List<string> variables = new List<string>();
                        var filename = template.Path.Split('/').Last();
                        var file = "/Documents/ReservationContract/" + reservation.ConfirmationCode + " " + filename;
                        var newFile = Server.MapPath("~" + file);
                        var fileLocation = Server.MapPath("~" + template.Path);
                        Document word = new Document(fileLocation);
                        variables.Add((string.IsNullOrEmpty(guest.FullName) ? guest.Name : guest.FullName));
                        variables.Add(string.IsNullOrWhiteSpace(property.Address)?"[Property Address]":property.Address);
                        variables.Add(TimeZoneInfo.ConvertTimeFromUtc(reservation.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName)).ToString("MMM dd,yyyy"));
                        variables.Add(TimeZoneInfo.ConvertTimeFromUtc(reservation.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName)).ToString("MMM dd,yyyy"));
                        variables.Add(reservation.ConfirmationCode);
                        variables.Add(reservation.GuestCount.ToString()+ " Guest(s)");
                        word.Replace("[Guest]", (string.IsNullOrEmpty(guest.FullName)? guest.Name: guest.FullName) , true, true);
                        word.Replace("[Property address]", string.IsNullOrEmpty(property.Address) ? "[Property Address]" : property.Address, true, true);
                        word.Replace("[Checkin Date]", TimeZoneInfo.ConvertTimeFromUtc(reservation.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName)).ToString("MMM dd,yyyy"), true, true);
                        word.Replace("[Checkout Date]", TimeZoneInfo.ConvertTimeFromUtc(reservation.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName)).ToString("MMM dd,yyyy"), true, true);
                        word.Replace("[Confirmation Code]",reservation.ConfirmationCode,true,true);
                        word.Replace("[Guest Count]", reservation.GuestCount.ToString() + " Guest(s)", true, true);
                        word.SaveToFile(newFile);
                        var temp = db.ContractDocuments.Where(x => x.BookingId == id && x.Type==(int)Core.Enumerations.ContractType.Template).FirstOrDefault();
                        var contract = new ContractDocument();
                        if (temp == null)
                        {
                            contract = new ContractDocument() { BookingId = reservation.Id, AttachmentPath = file, Type = (int)Core.Enumerations.ContractType.Template };
                            db.ContractDocuments.Add(contract);
                            db.SaveChanges();
                        }
                        else
                        {
                            temp.AttachmentPath = file;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                            contract = temp;

                        }
                        Document doc = new Document(Server.MapPath("~" + contract.AttachmentPath));
                        var htmlname = "/Documents/ContractHtml/" + reservation.ConfirmationCode + " " + filename + ".html";
                        var htmlpath = Server.MapPath("~" + htmlname);
                        doc.SaveToFile(htmlpath);
                        var contextUrl = System.Web.HttpContext.Current.Request.Url;
                        var url = contextUrl.Scheme + "://" + contextUrl.Authority + htmlname;
                        contract.Html = AirbnbScrapper.Test(url);

                        return Json(new { success = true, document=contract,email ,variables}, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = true, generatedId = generatedDocumentId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveGeneratedDocument(int id, string content,string email)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ContractDocuments.Where(x => x.Id == id).FirstOrDefault();
                if (temp != null)
                {
                    Document doc = new Document();
                    var file = temp.AttachmentPath;
                    var path = Server.MapPath("~" + file);
                    Section s = doc.AddSection();
                    Paragraph para = s.AddParagraph();
                    para.AppendBookmarkStart("content");
                    para.AppendHTML(content.Trim());
                    para.AppendBookmarkEnd("content");
                    doc.SaveToFile(path);
                    temp.AttachmentPath = file;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    Contract contract = new Contract();
                    contract.Send(temp.BookingId, id, email, User.Identity.Name.ToInt());
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Test()
        {
            Contract contract = new Contract();
            contract.Send(69852, 14, "demesa12@gmail.com",1);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [CheckSessionTimeout]
        //get the conversation for booking action.
        public ActionResult GetGuestConversation(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                IList<MessageEntry> messages = new List<MessageEntry>();
                if (inbox != null)
                {
                    ICommonRepository objIList = new CommonRepository();
                    var sms = new List<SMS>();
                    var calls = new List<Call>();
                    var messageThread = objIList.GetMessageThread(threadId);
                    var threadDetails = objIList.GetThreadDetail(threadId);
                    if (inbox.GuestId != 0)
                    {
                        sms = db.SMS.Where(x => x.GuestId == inbox.GuestId).ToList();
                        calls = db.Calls.Where(x => x.GuestId == inbox.GuestId).ToList();
                    }
                    var contactInfos = db.GuestContactInfos.Where(x => x.GuestId == inbox.GuestId).ToList();

                    messageThread.ToList().ForEach(x =>
                    {

                        var message = new MessageEntry();
                        message.MessageContent = x.MessageContent;
                        message.IsMyMessage = x.IsMyMessage;
                        message.CreatedDate = x.CreatedDate;
                        message.Id = x.Id;
                        message.Type = 0;
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        messages.Add(message);
                    });
                    sms.ForEach(x =>
                    {
                        var message = new MessageEntry();
                        message.Id = x.Id;
                        message.MessageContent = x.Body;
                        message.IsMyMessage = x.IsMyMessage;
                        message.Source = x.IsMyMessage ? x.To : x.From;
                        if (message.Source.Substring(0, 8) == "whatsapp")
                        {
                            message.Type = 3;
                        }
                        else if (message.Source.Substring(0, 9) == "messenger")
                        {
                            message.Type = 4;
                        }
                        else
                        {
                            message.Type = 1;
                        }
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        message.CreatedDate = x.CreatedDate;
                        message.ResourceSid = x.MessageSid;
                        messages.Add(message);
                    }
                    );
                    calls.ForEach(x =>
                    {
                        var message = new MessageEntry();
                        message.Id = x.Id;
                        message.IsMyMessage = x.IsMyMessage;
                        message.Type = 2;
                        message.Source = x.IsMyMessage ? x.To : x.From;
                        message.CreatedDate = x.CreatedDate;
                        message.ResourceSid = x.CallSid;
                        message.RecordingUrl = x.RecordingUrl;
                        message.Duration = x.CallDuration == null ? x.RecordingDuration : x.CallDuration;
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        messages.Add(message);
                    });

                    return Json(new { success = true, inbox = inbox, messages = messages }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<Property> GetPropertyList()
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return (from h in db.Hosts
                        join hc in db.HostCompanies on h.Id equals hc.HostId
                        join p in db.Properties on h.Id equals p.HostId
                        where hc.CompanyId == SessionHandler.CompanyId && p.ListingId != 0
                        select p).ToList();
                //db.Properties.Where(x => x.CompanyId == companyId && x.ListingId != 0).ToList();
            }
        }

        public List<Property> GetChildPropertyList()
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return (from h in db.Hosts
                        join hc in db.HostCompanies on h.Id equals hc.HostId
                        join p in db.Properties on h.Id equals p.HostId
                        where !p.IsParentProperty && hc.CompanyId == SessionHandler.CompanyId
                        select p).ToList();
                //db.Properties.Where(x => x.CompanyId == companyId).Where(x => !x.IsParentProperty).ToList();
            }
        }

        public IEnumerable<BookingStatus> GetStatusList()
        {
            using (var db = new ApplicationDbContext())
            {
                List<BookingStatus> ListItems = (from list in db.BookingStatus where list.Description != null select list).AsEnumerable().Select(p => new BookingStatus
                {
                    Code = p.Code,
                    Description = p.Description
                }).ToList();
                return ListItems;
            }
        }

        [HttpGet]
        public ActionResult GetSiteBookingDetails(string confirmationCode)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.Inquiries.Where(x => x.ConfirmationCode == confirmationCode)
                        .Join(db.Properties, x => x.PropertyId, y => y.ListingId, (x, y) => new { Inquiry = x, Property = y })
                        .FirstOrDefault();
                    return Json(new { success = true, reservation = temp }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e) { return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet); }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> AcceptBooking(int siteType, string hostId, string reservationCode, string message, string threadId = "")
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Changing booking status into accepted...", Core.Enumerations.NotificationType.INFORMATION);
            //Call the API AcceptBooking in API Project
            using (HttpClient client = new HttpClient())
            {// set the parameter for API request
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("companyId",SessionHandler.CompanyId.ToString());
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId);
                reqParams.Add("reservationCode", reservationCode);
                reqParams.Add("message", message);
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("threadId", threadId);
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/AcceptBooking", reqContent);
                if (result != null && result.Content != null)
                {
                    //Get the Result and parse it into JObject
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Booking was successfully accepted!", Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while changing the booking status.", 
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean(), message =messages["message"].ToString()},JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = true, message = "" });
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> DeclineBooking(int siteType, string hostId, string reservationCode, string message, string threadId = "", string listingId = "")
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Changing booking status into declined...", Core.Enumerations.NotificationType.INFORMATION);
            //Call the API DeclineBooking in API Project
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                //set the parameter for API request
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId);
                reqParams.Add("reservationCode", reservationCode);
                reqParams.Add("message", message);
                reqParams.Add("threadId", threadId);
                reqParams.Add("listingId", listingId);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/DeclineBooking", reqContent);
                if (result != null && result.Content != null)
                {
                    //get and parse the result into Object to get the content
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Booking was successfully cancelled!", Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while changing the booking status.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString()}, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> GetDetails(int siteType, int hostId, string reservationId, string listingId, string checkinDate, string checkoutDate, string threadId)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Calculating total amount...", Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("reservationId", reservationId);
                reqParams.Add("listingId", listingId);
                reqParams.Add("checkinDate", checkinDate);
                reqParams.Add("checkoutDate", checkoutDate);
                reqParams.Add("threadId", threadId);

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/GetDetails", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (messages["message"] != null)
                    {
                        if (messages["success"].ToBoolean())
                        {
                            return Json(new { success = messages["success"].ToBoolean(), totalAmount = messages["totalAmount"].ToString() }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(new { success = false, message = "Error occur on alter booking" }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> AlterBooking(int siteType, int hostId, string reservationCode, string listingId, string checkinDate, string checkoutDate, string price, string reservationId = "", bool submitAlteration = false, int id = 0, string threadId = "", int guests = 0, int adults = 0, int children = 0, int alterType = 1)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Altering booking...", Core.Enumerations.NotificationType.INFORMATION);
            //Call the api AlterBooking in Api Project
            using (HttpClient client = new HttpClient())
            {//Set the parameter for Api request
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("reservationCode", reservationCode);
                reqParams.Add("listingId", listingId);
                reqParams.Add("checkinDate", checkinDate);
                reqParams.Add("checkoutDate", checkoutDate);
                reqParams.Add("price", price);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                reqParams.Add("submitAlteration", submitAlteration.ToString());
                reqParams.Add("id", id.ToString());

                reqParams.Add("threadId", threadId);
                reqParams.Add("guests", guests.ToString());
                reqParams.Add("adults", adults.ToString());
                reqParams.Add("children", children.ToString());
                reqParams.Add("alterType", alterType.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/AlterBooking", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["message"] != null)
                    //{
                    //    if (messages["success"].ToBoolean())
                    //    {
                    //        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //            "Booking was successfully altered!", Core.Enumerations.NotificationType.SUCCESS);
                    //    }
                    //    else
                    //    {
                    //        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //            "An error occured while altering your booking.",
                    //            Core.Enumerations.NotificationType.ERROR);
                    //    }
                    //    return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString(), amount = messages["amount"].ToString(), currency = messages["currency"].ToString() }, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while altering your booking.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //    return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                    //}

                }
            }
            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> AddPaymentRequest(int hostId, int siteType, string threadId, string description, DateTime dueDate, string message, float amount)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Adding payment request...", Core.Enumerations.NotificationType.INFORMATION);
            //Call Api AddPaymentRequest in Api Project
            using (HttpClient client = new HttpClient())
            {//Set the parameter for api request
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();

                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("threadId", threadId);
                reqParams.Add("description", description);
                reqParams.Add("dueDate", dueDate.ToString());
                reqParams.Add("message", message);
                reqParams.Add("amount", amount.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/AddPaymentRequest", reqContent);
                if (result != null && result.Content != null)
                {
                    //get and parse request to JObject to get content
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (messages["success"].ToBoolean())
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "Payment request was successfully added on booking!", Core.Enumerations.NotificationType.SUCCESS);
                    }
                    else
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "An error occured while adding your payment request.",
                            Core.Enumerations.NotificationType.ERROR);
                    }

                    return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString() }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = false, message = "" });
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> SendRefund(int hostId, int siteType, string threadId, short amount, string description)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Sending refund...",
                Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();

                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("threadId", threadId);
                reqParams.Add("description", description);
                reqParams.Add("amount", amount.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/SendRefund", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (messages["success"].ToBoolean())
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "Refund was successfully sent on booking!",
                            Core.Enumerations.NotificationType.SUCCESS);
                    }
                    else
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "An error occured while sending your refund.",
                            Core.Enumerations.NotificationType.ERROR);
                    }

                    return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString() }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = false, message = "" });

        }
        [HttpGet]
        public ActionResult GetInquiry(string code, int siteType)
        {
            Inquiry inquiry = null;
            using (var db = new ApplicationDbContext())
            {
                if (siteType == 2)
                {
                    inquiry = (from i in db.Inquiries where i.ReservationId == code select i).FirstOrDefault();
                }
                else if (siteType == 1)
                {
                    inquiry = (from i in db.Inquiries where i.ConfirmationCode == code select i).FirstOrDefault();
                }

                var p = db.Properties.Where(x => x.ListingId == inquiry.PropertyId).FirstOrDefault();
                if (p.TimeZoneName != null)
                {
                    inquiry.CheckInDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)inquiry.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(p.TimeZoneName));
                    inquiry.CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)inquiry.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(p.TimeZoneName));
                }

                return Json(new { success = true, inquiry = inquiry }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult IsConfirmationCodeExist(string code)
        {
            bool result = false;
            using (var db = new ApplicationDbContext())
            {
                var inquiry = (from i in db.Inquiries where i.ConfirmationCode == code select i).FirstOrDefault();
                if (inquiry != null)
                {
                    result = true;
                }
            }
            return Json(new { exist = result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetPropertyByHost(int hostId)
        {
            List<Property> properties = null;
            using (var db = new ApplicationDbContext())
            {
                properties = (from p in db.Properties where p.HostId == hostId select p).ToList();
            }
            return Json(new { success = true, properties = properties }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPropertyBySiteType(int sitetype)
        {
            List<Property> properties = null;
            using (var db = new ApplicationDbContext())
            {
                properties = (from p in db.Properties where p.SiteType == sitetype select p).ToList();
            }
            return Json(new { success = true, properties = properties }, JsonRequestBehavior.AllowGet);
        }
        //[HttpGet]
        //[CheckSessionTimeout]
        //public ActionResult RefreshSingleBooking(string hostId, string reservationCode)
        //{
        //    try
        //    {
        //        if (User.Identity.IsAuthenticated && SiteConstants.accounts.ContainsKey(hostId))
        //        {
        //            using (ScrapeManager sm = new ScrapeManager())
        //            {
        //                var res = sm.ScrapeInquiries(SiteConstants.accounts.Where(x => x.Key == hostId).FirstOrDefault().Value.SessionToken, false, false, reservationCode);
        //                if (res.Success)
        //                {
        //                    var inquiryList = objIList.GetInquiryList();
        //                    return Json(new { success = res.Success, count = res.TotalRecords, message = res.Message, inquiryList = inquiryList }, JsonRequestBehavior.AllowGet);
        //                }
        //                else
        //                {
        //                    return Json(new { success = false, message = res.Message }, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //        }
        //        return Json(new { success = false, message = "Error occur in refresh single booking" }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> WithdrawSpecialOffer(string hostId, string threadId)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Altering booking...", Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Airbnb));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId);
                reqParams.Add("threadId", threadId);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/WithdrawSpecialOffer", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Booking was successfully altered!", Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while altering your booking.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> PreApproveInquiry(int siteType, string threadId, string hostId)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Pre-approving inquiry...", Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("threadId", threadId);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/PreApproveInquiry", reqContent);
                if (result != null && result.Content != null)
                {
                    //messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Inquiry was successfully pre-approved!", Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while pre-approving an inquiry.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean()},JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> WithdrawPreApproveInquiry(int siteType, string threadId, string hostId, string message = "")
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Pre-approved inquiry was successfully cancelled!",
                Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("threadId", threadId);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId);
                reqParams.Add("message", message);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/WithdrawPreApproveInquiry", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Pre-approved inquiry was successfully cancelled!", 
                    //        Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while cancelling a pre-approved inquiry.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> DeclineInquiry(string threadId, string hostId, string message)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Declining inquiry...", Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Airbnb));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("threadId", threadId);
                reqParams.Add("hostId", hostId);
                reqParams.Add("message", message);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/DeclineInquiry", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Inquiry was successfully declined!", Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while declining an inquiry.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public async Task<ActionResult> CheckSpecialOfferValidity(string hostId, string listingId, string guestId, string startDate, string endDate, int numOfGuest, string currentStartDate, string currentEndDate)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Checking special offer validity...", Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Airbnb));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId);
                reqParams.Add("listingId", listingId);
                reqParams.Add("guestId", guestId);
                reqParams.Add("startDate", startDate);
                reqParams.Add("endDate", endDate);
                reqParams.Add("numOfGuest", numOfGuest.ToString());
                reqParams.Add("currentStartDate", currentStartDate);
                reqParams.Add("currentEndDate", currentEndDate);
                //reqParams.Add("price", price);
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/CheckSpecialOfferValidity", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    if (messages["result"] != null)
                    {
                        var res = messages["result"] as JObject;

                        Airbnb.Models.SpecialOfferSummary results = res.ToObject<Airbnb.Models.SpecialOfferSummary>();
                        return Json(new { success = messages["success"].ToBoolean(), result = results }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        //VRBO check reservation details
        [HttpPost]
        public async Task<ActionResult> CheckReservationValidity(int hostId, int siteType, string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Checking reservation validity...", Core.Enumerations.NotificationType.INFORMATION);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("conversationId", conversationId);
                reqParams.Add("checkInDate", checkInDate.ToString());
                reqParams.Add("checkoutDate", checkoutDate.ToString());
                reqParams.Add("price", price.ToString());
                reqParams.Add("adults", adults.ToString());
                reqParams.Add("children", children.ToString());
                //reqParams.Add("price", price);
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/CheckReservationValidity", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    //if (messages["result"] != null)
                    //{
                    var res = messages["result"] as JObject;

                    //Airbnb.Models.SpecialOfferSummary results = res.ToObject<Airbnb.Models.SpecialOfferSummary>();
                    return Json(new { success = messages["success"].ToBoolean(), subtotal = messages["difference"].ToString() }, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);
                    //}
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[CheckSessionTimeout]
        //public ActionResult WithdrawSpecialOffer(string hostId,string threadId)
        //{
        //    try
        //    {
        //        if (SiteConstants.accounts.ContainsKey(hostId))
        //        {
        //            var token = SiteConstants.accounts.Where(x => x.Key == hostId).FirstOrDefault().Value.SessionToken;
        //            using (ScrapeManager sm = new Airbnb.ScrapeManager())
        //            {
        //                if(sm.ValidateTokenAndLoadCookies(token))
        //                {
        //                    string removeUrl = sm.GetRemoveOfferUrl(threadId);
        //                    if(!string.IsNullOrEmpty(removeUrl))
        //                    {
        //                        var res = sm.WithdrawSpecialOffer(removeUrl);
        //                        sm.ScrapeInbox(token, true);
        //                        return Json(new { success = res}, JsonRequestBehavior.AllowGet);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch { }
        //    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> SendSpecialOffer(string hostId, string listingId, string threadId, int numOfGuest, string startDate, string endDate, string price)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Altering booking...", Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Airbnb));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId);
                reqParams.Add("listingId", listingId);
                reqParams.Add("threadId", threadId);
                reqParams.Add("numOfGuest", numOfGuest.ToString());
                reqParams.Add("startDate", startDate);
                reqParams.Add("endDate", endDate);
                reqParams.Add("price", price);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/SendSpecialOffer", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    //if (messages["success"].ToBoolean())
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "Booking was successfully altered!", Core.Enumerations.NotificationType.SUCCESS);
                    //}
                    //else
                    //{
                    //    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    //        "An error occured while altering your booking.",
                    //        Core.Enumerations.NotificationType.ERROR);
                    //}

                    //return Json(new { success = messages["success"].ToBoolean()}, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> CancelAlteration(int siteType, string hostId, string reservationCode)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Cancelling booking alteration...",
                Core.Enumerations.NotificationType.INFORMATION);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("hostId", hostId);
                reqParams.Add("reservationCode", reservationCode);
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/CancelAlteration", reqContent);
                if (result != null && result.Content != null)
                {
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    if (messages["message"] != null)
                    {
                        if (messages["success"].ToBoolean())
                        {
                            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                                "Booking alteration was successfully cancelled!",
                                Core.Enumerations.NotificationType.SUCCESS);
                        }
                        else
                        {
                            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                                "An error occured while cancelling your alteration on booking.",
                                Core.Enumerations.NotificationType.ERROR);
                        }
                        return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString() }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "An error occured while cancelling your alteration on booking.",
                            Core.Enumerations.NotificationType.ERROR);
                        return Json(new { success = messages["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { success = false, message = "Error occur in cancel alteration" }, JsonRequestBehavior.AllowGet);

            //try
            //{
            //    Core.Enumerations.SiteType type = (Core.Enumerations.SiteType)siteType;

            //    if (type == Core.Enumerations.SiteType.Airbnb)
            //    {
            //        using (Airbnb.ScrapeManager sm = new Airbnb.ScrapeManager())
            //        {
            //            var result = sm.CancelAlteration(Core.API.Airbnb.HostSessionCookies.Token, reservationCode);
            //            //sm.ScrapeInquiries(Core.API.Airbnb.HostSessionCookies.Token, false, false, reservationCode);
            //            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
            //        }
            //    }
            //    else if(type == Core.Enumerations.SiteType.VRBO)
            //    {
            //        using (VRBO.ScrapeManager sm = new VRBO.ScrapeManager())
            //        {
            //            var result = sm.CancelAlteration(reservationCode);
            //            //sm.ScrapeInquiries(Core.API.Airbnb.HostSessionCookies.Token, false, false, reservationCode);
            //            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
            //        }
            //    }
            //    return Json(new { success = false, message = "Error occur in cancel alteration" }, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception e) { return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet); }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddBooking(int SiteType, string ConfirmationCode, string GuestName, int GuestCount,
            int PropertyId, string CheckinDate, string CheckinTime, string CheckoutDate, string CheckoutTime,
            string InquiryPrice, string PayoutAmt, string Payoutdate, string NetRevenue, string Bookeddate,
            string Bookedtime, string Inquirydate, string inquirytime, string Status, int Id, string feeValue,
            string taxValue, string feeFeeId, string taxFeeId)
        {
            DateTime checkInDate;
            DateTime.TryParse(CheckinDate, out checkInDate);
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
            }
            if (Id == 0)
            {
                Guest guest = new Guest();
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.Guests.Where(x => x.Name.ToLower() == GuestName.ToLower()).FirstOrDefault();
                    if (temp == null)
                    {
                        Guest g = new Guest
                        {
                            Name = GuestName,
                            GuestId = Guid.NewGuid().ToString(),
                            ProfilePictureUrl = ""
                        };
                        guest = objIList.AddGuest(g);
                    }
                    else
                    {
                        guest = temp;
                    }
                }

                var guestLastname = guest.Name.Split(' ').Last();
                var guestFirstname = guest.Name.Replace(guestLastname, "");

                var threadId = Guid.NewGuid().ToString();
                var reservationId = Guid.NewGuid().ToString();
                var confirmationCode = Guid.NewGuid().ToString();

                var hostId = Core.API.AllSite.Properties.GetByLocalPropertyId(PropertyId).HostId;

                Inquiry iq = new Inquiry
                {
                    SiteType = SiteType,
                    HostId = hostId,
                    ConfirmationCode = SiteType == 0 ? confirmationCode : ConfirmationCode,
                    GuestCount = GuestCount,
                    PropertyId = Core.API.AllSite.Properties.GetByLocalPropertyId(PropertyId).ListingId.ToInt(),
                    CheckInDate = DateTime.ParseExact((CheckinDate + " " + CheckinTime), "MMMM d, yyyy h:mm tt", new CultureInfo("en-US")),
                    CheckOutDate = DateTime.ParseExact((CheckoutDate + " " + CheckoutTime), "MMMM d, yyyy h:mm tt", new CultureInfo("en-US")),
                    ReservationCost = decimal.Parse(InquiryPrice, CultureInfo.InvariantCulture),
                    TotalPayout = decimal.Parse(PayoutAmt, CultureInfo.InvariantCulture),
                    PayoutDate = DateTime.ParseExact(Payoutdate, "MMMM d, yyyy", new CultureInfo("en-US")),
                    NetRevenueAmount = decimal.Parse(NetRevenue, CultureInfo.InvariantCulture),
                    BookingDate = DateTime.ParseExact((Bookeddate + " " + Bookedtime), "MMMM d, yyyy h:mm tt", new CultureInfo("en-US")),
                    InquiryDate = DateTime.ParseExact(Inquirydate + " " + inquirytime, "MMMM d, yyyy h:mm tt", new CultureInfo("en-US")),
                    BookingStatusCode = Status,
                    GuestId = guest.Id,
                    ThreadId = threadId,
                    ReservationId = reservationId,
                    BookingCancelDate = (DateTime?)null,
                    BookingConfirmDate = (DateTime?)null,
                    //CompanyId = SessionHandler.CompanyId,
                    IsActive = true
                };

                Core.API.Local.Inboxes.Add(hostId, DateTime.UtcNow, false, "", guest.GuestId,
                    guestFirstname, guestLastname, threadId, false, "",
                    (DateTime)iq.CheckInDate, (DateTime)iq.CheckOutDate, iq.PropertyId);

                int bookingId = objIList.AddBooking(iq);

                using (var db = new ApplicationDbContext())
                {
                    if (Status == "A" && (from prop in db.Properties where prop.ListingId == iq.PropertyId select prop.AutoCreateIncomeExpense).FirstOrDefault())
                    {
                        int incomeTypeId = objIList.GetAirbnbIncomeTypeId();
                        Income income = new Income
                        {
                            PropertyId = iq.PropertyId,
                            IncomeTypeId = incomeTypeId,
                            BookingId = bookingId,
                            CompanyId = SessionHandler.CompanyId,
                            Amount = decimal.Parse(InquiryPrice, CultureInfo.InvariantCulture),
                            Status = 1,
                            DateRecorded = DateTime.Now,
                            Description = GuestName,
                            DueDate = DateTime.ParseExact(Payoutdate, "MMMM d, yyyy", new CultureInfo("en-US")),
                        };
                        var add = objIList.AddIncome(income);

                        //db.Expenses.RemoveRange(db.Expenses.Where(x => x.BookingId == bookingId));
                        //db.Income.RemoveRange(db.Income.Where(x => x.BookingId == bookingId));

                        string[] Value1 = feeValue.Split(',');
                        string[] Value11 = feeFeeId.Split(',');
                        for (int i = 0; i < Value1.Count(); i++)
                        {
                            if (Value1[i] != "")
                            {
                                objIList.AddExpense(bookingId, iq.PropertyId, GuestName, decimal.Parse(Value1[i], CultureInfo.InvariantCulture), int.Parse(Value11[i]), true, true, Payoutdate, DateTime.Now, checkInDate);
                            }
                        }
                        string[] Value2 = taxValue.Split(',');
                        string[] Value22 = taxFeeId.Split(',');
                        for (int i = 0; i < Value2.Count(); i++)
                        {
                            if (Value2[i] != "")
                            {
                                objIList.AddExpense(bookingId, iq.PropertyId, GuestName, decimal.Parse(Value2[i], CultureInfo.InvariantCulture), int.Parse(Value22[i]), false, false, "", DateTime.Now, checkInDate);
                            }
                        }
                    }
                }

                return Json(bookingId, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(UpdateBookingPage(SiteType, ConfirmationCode, GuestName, GuestCount, PropertyId, CheckinDate, CheckinTime, CheckoutDate, CheckoutTime, InquiryPrice, PayoutAmt, Payoutdate, NetRevenue, Bookeddate, Bookedtime, Inquirydate, Status, Id, feeValue, taxValue, feeFeeId, taxFeeId), JsonRequestBehavior.AllowGet);
            }
        }

        private int UpdateBookingPage(int SiteType, string ConfirmationCode, string GuestName, int GuestCount, int Property, string CheckinDate, string CheckinTime, string CheckoutDate, string CheckoutTime, string InquiryPrice, string PayoutAmt, string Payoutdate, string NetRevenue, string Bookeddate, string Bookedtime, string Inquirydate, string Status, int Id, string feeValue, string taxValue, string feeFeeId, string taxFeeId)
        {
            DateTime checkInDate;
            DateTime.TryParse(CheckinDate, out checkInDate);
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                int airbnbFeeId = objIList.GetAirbnbFeeId();
                int propertId = (from h in db.Hosts
                                 join hc in db.HostCompanies on h.Id equals hc.HostId
                                 join i in db.Inquiries on h.Id equals i.HostId
                                 where hc.CompanyId == SessionHandler.CompanyId && i.Id == Id
                                 select i.PropertyId).FirstOrDefault();
                var expenses = db.Expenses.Where(x => x.BookingId == Id && x.CompanyId == SessionHandler.CompanyId).ToList();
                db.Expenses.RemoveRange(expenses);
                db.SaveChanges();

                if (Status == "A")
                {
                    int incomeTypeId = objIList.GetAirbnbIncomeTypeId();
                    var tempBookingIncome = db.Income.FirstOrDefault(x => x.BookingId == Id);
                    if (tempBookingIncome != null)
                    {
                        tempBookingIncome.PropertyId = Property;
                        tempBookingIncome.Amount = decimal.Parse(InquiryPrice, CultureInfo.InvariantCulture);
                        tempBookingIncome.DueDate = Convert.ToDateTime(Payoutdate);
                        tempBookingIncome.Description = GuestName;
                        db.Entry(tempBookingIncome).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        Income inc = new Income
                        {
                            PropertyId = Property,
                            IncomeTypeId = incomeTypeId,
                            BookingId = Id,
                            CompanyId = SessionHandler.CompanyId,
                            Amount = decimal.Parse(InquiryPrice, CultureInfo.InvariantCulture),
                            Status = 1,
                            DateRecorded = DateTime.Now,
                            DueDate = Convert.ToDateTime(Payoutdate),
                        };
                        objIList.AddIncome(inc);
                    }
                }

                string[] Value1 = feeValue.Split(',');
                string[] Value11 = feeFeeId.Split(',');
                for (int i = 0; i < Value1.Count(); i++)
                {
                    if (Value1[i] != "")
                    {
                        objIList.AddExpense(Id, Property, GuestName, decimal.Parse(Value1[i], CultureInfo.InvariantCulture), int.Parse(Value11[i]), true, true, Payoutdate, DateTime.Now, checkInDate);
                    }
                }
                string[] Value2 = taxValue.Split(',');
                string[] Value22 = taxFeeId.Split(',');
                for (int i = 0; i < Value2.Count(); i++)
                {
                    if (Value2[i] != "")
                    {
                        objIList.AddExpense(Id, Property, GuestName, decimal.Parse(Value2[i], CultureInfo.InvariantCulture), int.Parse(Value22[i]), false, false, "", DateTime.Now, checkInDate);
                    }
                }

                var dataRow = (from i in db.Inquiries where i.Id == Id select i).FirstOrDefault();
                var dataRow2 = (from i in db.Guests where i.Id == dataRow.GuestId select i).FirstOrDefault();
                dataRow2.Name = GuestName;
                db.Entry(dataRow2).State = EntityState.Modified;
                int res = db.SaveChanges();
                dataRow.SiteType = 1;
                dataRow.ConfirmationCode = ConfirmationCode;
                dataRow.GuestCount = GuestCount;
                dataRow.PropertyId = Property;
                //dataRow.CompanyId = companyId;
                string test = CheckinDate + " " + CheckinTime;
                dataRow.CheckInDate = DateTime.ParseExact((CheckinDate + " " + CheckinTime), "MMMM d, yyyy h:mm tt", new CultureInfo("en-US"));
                dataRow.CheckOutDate = DateTime.ParseExact((CheckoutDate + " " + CheckoutTime), "MMMM d, yyyy h:mm tt", new CultureInfo("en-US"));
                dataRow.PayoutDate = DateTime.ParseExact(Payoutdate, "MMMM d, yyyy", new CultureInfo("en-US"));
                dataRow.NetRevenueAmount = decimal.Parse(NetRevenue, CultureInfo.InvariantCulture);
                dataRow.BookingDate = DateTime.ParseExact((Bookeddate + " " + Bookedtime), "MMMM d, yyyy h:mm tt", new CultureInfo("en-US"));
                dataRow.InquiryDate = DateTime.ParseExact(Inquirydate, "MMMM d, yyyy", new CultureInfo("en-US"));
                dataRow.ReservationCost = decimal.Parse(InquiryPrice, CultureInfo.InvariantCulture);
                dataRow.TotalPayout = decimal.Parse(PayoutAmt, CultureInfo.InvariantCulture);
                dataRow.BookingStatusCode = Status;
                db.Entry(dataRow).State = EntityState.Modified;
                db.SaveChanges();
                return res;
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult RemoveBooking(int Id)
        {
            ICommonRepository repo = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (repo.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
            }
            return Json(repo.DeleteBooking(Id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ChangeBookingTime(int inquiryId, string checkInTime, string checkOutTime)
        {
            try
            {
                string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                TwilioClient.Init(accountSid, authToken);
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.Inquiries.Find(inquiryId);
                    if (temp != null)
                    {
                        var assignments = db.WorkerAssignments.Where(x => x.ReservationId == temp.Id && DbFunctions.TruncateTime(x.StartDate) == DateTime.Today).ToList();
                        var phoneNumber = "";
                        if (assignments != null)
                        {
                            foreach (var assignment in assignments)
                            {
                                var property = db.Properties.Where(x => x.Id == assignment.PropertyId).FirstOrDefault();
                                var parentproperty = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ChildPropertyId == property.Id && pc.CompanyId == SessionHandler.CompanyId && p.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p).FirstOrDefault();
                                var rule = db.WorkerMessageRules.Where(x => x.PropertyId == property.Id && x.Type == 2).FirstOrDefault();
                                if (rule != null)
                                {
                                    var template = db.WorkerTemplateMessages.Where(x => x.Id == rule.WorkerTemplateMessageId).FirstOrDefault();
                                    var reservation = db.Inquiries.Where(x => x.Id == assignment.ReservationId).FirstOrDefault();
                                    var worker = db.Workers.Where(x => x.Id == assignment.WorkerId).FirstOrDefault();
                                    var workerContact = db.WorkerContactInfos.Where(x => x.WorkerId == worker.Id).FirstOrDefault();
                                    var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == property.TwilioNumberId).FirstOrDefault();
                                    var guest = db.Guests.Where(x => x.Id == reservation.GuestId).FirstOrDefault();
                                    if (twilioNumber != null)
                                    {
                                        phoneNumber = twilioNumber.PhoneNumber;
                                    }
                                    else
                                    {
                                        var host = db.Hosts.Where(x => x.Id == property.HostId).FirstOrDefault();
                                        twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == host.TwilioNumberId).FirstOrDefault();
                                        if (twilioNumber != null)
                                        {
                                            phoneNumber = twilioNumber.PhoneNumber;
                                        }
                                    }
                                    var workerThread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && x.WorkerId == assignment.WorkerId).FirstOrDefault();
                                    if (workerThread == null)
                                    {
                                        workerThread = new CommunicationThread();
                                        db.CommunicationThreads.Add(workerThread);
                                        workerThread.WorkerId = worker.Id;
                                        workerThread.ThreadId = PasswordGenerator.Generate(20);
                                        workerThread.CompanyId = SessionHandler.CompanyId;
                                        db.CommunicationThreads.Add(workerThread);
                                        db.SaveChanges();
                                    }
                                    var message = template.Message;
                                    var replacements = new Dictionary<string, string>
                            {
                                { "[Guest Name]", guest.Name },
                                { "[Worker Name]", worker.Firstname },
                                { "[Parent Property]", parentproperty!=null?parentproperty.Name:property.Name },
                                { "[Listing Name]", property.Name },
                                { "[Guest Count]", reservation.GuestCount.ToString()},
                                { "[Check In Date]", reservation.CheckInDate.ToDateTime().ToString("MMM dd,yyyy") },
                                { "[Check Out Date]", reservation.CheckOutDate.ToDateTime().ToString("MMM dd,yyyy") },
                                { "[Assignment Start]", assignment.StartDate.ToDateTime().ToString("MMM dd,yyyy hhtt")},
                                { "[Assignment End]", assignment.EndDate.ToDateTime().ToString("MMM dd,yyyy hhtt") }
                            };
                                    foreach (var replacement in replacements)
                                    {
                                        message = message.Replace(replacement.Key, replacement.Value);
                                    }

                                    var smsMessage = MessageResource.Create(
                                     from: new Twilio.Types.PhoneNumber(phoneNumber),
                                     to: new Twilio.Types.PhoneNumber(workerContact.Value),
                                     body: message);

                                    CommunicationSMS sms = new CommunicationSMS
                                    {
                                        ThreadId = workerThread.ThreadId,
                                        Message = message,
                                        From = phoneNumber,
                                        To = workerContact.Value,
                                        CreatedDate = DateTime.Now,
                                        UserId = 1

                                    };
                                    db.CommunicationSMS.Add(sms);
                                    db.SaveChanges();

                                }
                            }
                        }
                        temp.CheckInDate = Core.Helper.DateTimeZone.ConvertToUTC(temp.CheckInDate.ToDateTime().Date + DateTime.ParseExact(checkInTime, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay, temp.PropertyId);
                        temp.CheckOutDate = Core.Helper.DateTimeZone.ConvertToUTC(temp.CheckOutDate.ToDateTime().Date + DateTime.ParseExact(checkOutTime, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay, temp.PropertyId);
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();

                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetBookingDetails(int inquiryId)
        {
            bool isSuccess = false;
            string message = "";
            object booking = null;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    booking = db.Inquiries
                        .Join(db.Properties, x => x.PropertyId, y => y.Id, (x, y) => new { Inquiry = x, Property = y })
                        .Join(db.Guests, x => x.Inquiry.GuestId, y => y.Id, (x, y) => new { Inquiry = x.Inquiry, Property = x.Property, Guest = y })
                        .Where(x => x.Inquiry.Id == inquiryId)
                        .Select(r => new
                        {
                            InquiryId = r.Inquiry.Id,
                            AirbnbInquiryId = r.Inquiry.ReservationId,
                            InquiryCode = r.Inquiry.ConfirmationCode,
                            PropertyId = r.Property.Id,
                            PropertyName = r.Property.Name,
                            InquiryStatus = r.Inquiry.BookingStatusCode == "A" ? "Accepted" : "",
                            CheckInDate = r.Inquiry.CheckInDate.Value,
                            CheckOutDate = r.Inquiry.CheckOutDate.Value,
                            GuestName = r.Guest.Name,
                            GuestId = r.Guest.GuestId,
                            GuestPicture = r.Guest.ProfilePictureUrl,
                            HostId = r.Inquiry.HostId
                        })
                        .FirstOrDefault();
                    if (booking != null)
                    {
                        isSuccess = true;
                        message = "Operation Successful";
                    }
                    else
                    {
                        message = "Operation failed";
                    }
                }
            }
            catch (Exception e) { message = e.ToString(); }
            return Json(new { isSuccess = isSuccess, message = message, booking = booking }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetBookingDetailsByThreadId(string threadId, long inquiryId = 0)
        {
            List<Core.Database.Entity.Review> hostReviews = new List<Core.Database.Entity.Review>();
            List<Core.Database.Entity.Review> guestReviews = new List<Core.Database.Entity.Review>();
            bool isSuccess = false;
            string message = "Operation failed";
            object booking = null;
            List<CctvFootageNote> cctvFootageNotes = new List<CctvFootageNote>();
            LockPincode pincode= null;
            int inquiryLocalId = 0;
            List<Device> Devices = null;
            var hasVideo = false;

            try
            {
                booking = objIList.GetThreadDetail(threadId, inquiryId);

                try
                {
                    var inquiry = Core.API.AllSite.Inquiries.GetByThreadId(threadId);
                    var inbox = Core.API.AllSite.Inbox.GetByThreadId(threadId);
                    var guest = Core.API.AllSite.Guests.GetByLocalId(inbox.GuestId);
                    hostReviews = Core.API.AllSite.Reviews.GetReviewToGuest(guest.GuestId);
                    guestReviews = Core.API.AllSite.Reviews.GetReviewFromGuest(guest.GuestId);
                    

                    using (var db = new ApplicationDbContext())
                    {
                        var localProperty = Core.API.AllSite.Properties.GetByListingId(inquiry.PropertyId);
                        pincode = db.LockPincodes.Where(x => x.ReservationId == inquiry.Id).FirstOrDefault();
                        //var localParentPropertyId = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ChildPropertyId == localProperty.Id && pc.CompanyId == SessionHandler.CompanyId && p.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p.Id).FirstOrDefault();
                        if (localProperty != null)
                        {
                            DateTime start = (localProperty.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc(inquiry.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(localProperty.TimeZoneName)) : inquiry.CheckInDate.ToDateTime());
                            DateTime end = (localProperty.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc(inquiry.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(localProperty.TimeZoneName)) : inquiry.CheckOutDate.ToDateTime());
                            var dtStart = start.Date;
                            var dtEnd = end.Date;
                            List<int> propertyIds = new List<int>();
                            propertyIds.Add(localProperty.Id);
                            var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == localProperty.Id).FirstOrDefault();
                            if (parentChild != null)
                            {
                                propertyIds.Add(parentChild.ParentPropertyId);
                                propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId).Select(x => x.ChildPropertyId).ToList());
                            }
                            var listingIds = db.Properties.Where(x => propertyIds.Contains(x.Id)).Select(x => x.ListingId).ToList();
                            hasVideo = (from d in db.Devices
                                          join v in db.Videos on d.Id equals v.LocalDeviceId
                                          join a in db.DeviceAccounts on d.AccountId equals a.Id
                                          where listingIds.Contains((long)d.ListingId) && (EntityFunctions.TruncateTime(v.DateCreated) >= dtStart && EntityFunctions.TruncateTime(v.DateCreated) <= dtEnd)
                                          && v.IsDeleted == false
                                        select new { device = d, video = v, account = a }).OrderBy(x => x.video.DateCreated).Any();
                            Devices = db.Devices.Where(x => x.ListingId == localProperty.ListingId).ToList();
                            inquiryLocalId = inquiry.Id;
                            cctvFootageNotes = Core.API.AllSite.Inquiries.CctvFootageNotes(inquiryLocalId);

                        }

                    }

                }
                catch (Exception e)
                {

                }
                if (booking != null)
                {
                    isSuccess = true;
                    message = "Operation Successful";
                }
            }
            catch (Exception e)
            {
                message = e.ToString();
            }
            var jsonData = Json(new
            {
                hasVideo= hasVideo,
                isSuccess = isSuccess,
                message = message,
                booking = booking,
                devices= Devices,
                pincode= pincode,
                cctvFootageNotes = cctvFootageNotes,
                hostReviews = hostReviews,
                guestReviews = guestReviews
            }, JsonRequestBehavior.AllowGet);
            jsonData.MaxJsonLength = int.MaxValue;
            return jsonData;
        }


        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetDetailBooks(int Id)
        {
            ICommonRepository repo = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (repo.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
            }
            using (var db = new ApplicationDbContext())
            {
                List<Property> plist = repo.GetAllProperty();
                int CompanyID = SessionHandler.CompanyId;
                var ilist = (from h in db.Hosts
                             join hc in db.HostCompanies on h.Id equals hc.HostId
                             join i in db.Inquiries on h.Id equals i.HostId
                             where hc.CompanyId == SessionHandler.CompanyId && i.Id == Id
                             select i).ToList();//(from i in db.Inquiries where i.Id == Id && i.CompanyId == CompanyID select i).ToList();
                List<Guest> glist = db.Guests.ToList();
                var inqlist = (from i in ilist
                               join g in glist on i.GuestId equals g.Id
                               join p in plist on i.PropertyId equals p.Id
                               select new
                               {
                                   ID = i.Id,
                                   SiteType = 1,
                                   ConfirmationCode = i.ConfirmationCode,
                                   GuestName = g.Name,
                                   GuestCount = i.GuestCount,
                                   Property = p.Name,
                                   PropertyId = i.PropertyId,
                                   InquiryDate = i.InquiryDate == null ? null : i.InquiryDate.ToDateTime().ToString("MMMM d, yyyy"),
                                   PayoutDate = i.PayoutDate == null ? null : i.PayoutDate.ToDateTime().ToString("MMMM d, yyyy"),
                                   NetRevAmt = i.NetRevenueAmount,
                                   BookDate = i.BookingDate == null ? null : i.BookingDate.ToDateTime().ToString("MMMM d, yyyy"),
                                   BookTime = i.BookingDate == null ? null : string.Format("{0:h:mm tt}", i.BookingDate),
                                   StayStartDate = i.CheckInDate == null ? null : i.CheckInDate.ToDateTime().ToString("MMMM d, yyyy"),
                                   StayStartTime = i.CheckInDate == null ? null : string.Format("{0:h:mm tt}", i.CheckInDate),
                                   StayEndDate = i.CheckOutDate == null ? null : i.CheckOutDate.ToDateTime().ToString("MMMM d, yyyy"),
                                   StayEndTime = i.CheckOutDate == null ? null : string.Format("{0:h:mm tt}", i.CheckOutDate),
                                   Price = i.ReservationCost,
                                   ExpensePayout = i.TotalPayout,
                                   Status = i.BookingStatusCode,
                                   Expenses = db.Expenses.Where(x => x.BookingId == i.Id).ToList(),
                                   PropertyFees = db.PropertyFees.Join(db.FeeTypes, x => x.FeeTypeId, y => y.Id, (x, y) => new { PropertyFees = x, FeeTypes = y }).Where(x => x.PropertyFees.PropertyId == i.PropertyId).ToList()
                               }).ToList();
                return Json(inqlist, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetPropertyDetailsById(string PropertyId)
        {
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json(new { success = false, message = "Error is coming due to unauthorized permission" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, message = "Error is coming due to unauthorized permission" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                if (!string.IsNullOrEmpty(PropertyId))
                {
                    using (var db = new ApplicationDbContext())
                    {
                        int Id = Convert.ToInt32(PropertyId);
                        var Fees = (from p in db.PropertyFees
                                    join f in db.FeeTypes on p.FeeTypeId equals f.Id
                                    where p.PropertyId == Id
                                    select new
                                    {
                                        Id = p.Id,
                                        ComputeBasis = p.ComputeBasis,
                                        AmountPercent = p.AmountPercent,
                                        FeeTypeId = p.FeeTypeId,
                                        FeeName = f.Fee_Type,
                                        Method = p.Method,
                                        InclusiveOrExclusive = p.InclusiveOrExclusive,
                                        Show = p.Show ? true : false,
                                        PreDeducted = p.IsPreDeducted ? true : false
                                    }).ToList();
                        return Json(new { success = true, feeList = Fees }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult EditAirbnbBooking(int bookingId, int propertyId, string guestName, int[] feeId, decimal[] amount, string checkInTime, string checkOutTime)
        {
            try
            {
                DateTime? checkInDate = null;
                int airbnbFeeId = objIList.GetAirbnbFeeId();
                using (var db = new ApplicationDbContext())
                {
                    var inquiry = db.Inquiries.Find(bookingId);
                    if (inquiry != null)
                    {
                        inquiry.CheckInDate = inquiry.CheckInDate.Value.Date + DateTime.ParseExact(checkInTime, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                        inquiry.CheckOutDate = inquiry.CheckOutDate.Value.Date + DateTime.ParseExact(checkOutTime, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                        db.Entry(inquiry).State = EntityState.Modified;
                        db.SaveChanges();
                        if (inquiry.CheckInDate.HasValue) { checkInDate = inquiry.CheckInDate.Value; }
                    }
                    db.Expenses.RemoveRange(db.Expenses.Where(x => x.BookingId == bookingId && x.IsPreDeducted == false));
                    db.SaveChanges();
                    if (feeId != null && amount != null)
                    {
                        for (int i = 0; i < feeId.Length; i++)
                        {
                            objIList.AddExpense(bookingId, propertyId, guestName, amount[i], feeId[i], false, false, "", DateTime.Now, checkInDate);
                        }
                    }
                }
                return Json(new { success = true, message = "Operation Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> MarkCreditCardAsInvalid(int hostId, string listingId, string reservationId, string reason, string reasonId, string card4Digit)
        {
            try
            {
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    "Changing credit card status to invalid...",
                    Core.Enumerations.NotificationType.INFORMATION);

                //Call the API MarkCreditCardAsInvalid in API Project
                using (HttpClient client = new HttpClient())
                {// set the parameter for API request
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.BookingCom));
                    JObject messages = null;
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("hostId", hostId.ToString());
                    reqParams.Add("listingId", listingId);
                    reqParams.Add("reservationId", reservationId);
                    reqParams.Add("reason", reason);
                    reqParams.Add("reasonId", reasonId);
                    reqParams.Add("card4Digit", card4Digit);
                    var reqContent = new FormUrlEncodedContent(reqParams);

                    var result = await client.PostAsync("Booking/MarkCreditCardAsInvalid", reqContent);
                    if (result != null && result.Content != null)
                    {
                        //Get the Result and parse it into JObject
                        messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                        if (messages["success"].ToBoolean())
                        {
                            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                                "Credit card was successfully marked as invalid!",
                                Core.Enumerations.NotificationType.SUCCESS);
                        }
                        else
                        {
                            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                                "An error occured while changing credit card status to invalid.",
                                Core.Enumerations.NotificationType.ERROR);
                        }

                        return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString() }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { success = false, message = "" });
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "An error occured." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> CheckAvailabilityAndPrice(int hostId, string reservationId, string confirmationCode,
            string listingId, DateTime checkInDate, DateTime checkOutDate)
        {
            try
            {
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    "Checking availability and price...",
                    Core.Enumerations.NotificationType.INFORMATION);

                //Call the API CheckAvailabilityAndPrice in API Project
                using (HttpClient client = new HttpClient())
                {// set the parameter for API request
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.BookingCom));
                    JObject messages = null;
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("hostId", hostId.ToString());
                    reqParams.Add("reservationId", reservationId);
                    reqParams.Add("confirmationCode", confirmationCode);
                    reqParams.Add("listingId", listingId);
                    reqParams.Add("checkInDate", checkInDate.ToString());
                    reqParams.Add("checkOutDate", checkOutDate.ToString());
                    var reqContent = new FormUrlEncodedContent(reqParams);

                    var result = await client.PostAsync("Booking/CheckAvailabilityAndPrice", reqContent);
                    if (result != null && result.Content != null)
                    {
                        //Get the Result and parse it into JObject
                        messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        if (messages["result"] != null)
                        {
                            return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString(), result = messages["result"].ToString() }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString() }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new { success = false, message = "" });
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = "An error occured." }, JsonRequestBehavior.AllowGet);
            }
        }

   
        [HttpGet]
        public ActionResult UpdateCctvFootageNote(int inquiryLocalId, string note, DateTime date, string camera)
        {
            var cctvFootageNotes = Core.API.AllSite.Properties.AddOrUpdateCctvFootageNote(inquiryLocalId, note, date, camera);

            if (cctvFootageNotes == null)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, notes = cctvFootageNotes }, JsonRequestBehavior.AllowGet);
            }
        }
        #region MarkAsNoShow
        [HttpPost]
        public async Task<ActionResult> MarkAsNoShow(int hostId, string listingId, string bookingReferenceNumber, string reservationId)
        {
            using (HttpClient client = new HttpClient())
            {
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                    "Changing booking status to no show...",
                    Core.Enumerations.NotificationType.INFORMATION);
                // set the parameter for API request
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.BookingCom));
                JObject messages = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("listingId", listingId);
                reqParams.Add("bookingReferenceNumber", bookingReferenceNumber);
                reqParams.Add("reservationId", reservationId);

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Booking/MarkAsNoShow", reqContent);
                if (result != null && result.Content != null)
                {
                    //Get the Result and parse it into JObject
                    messages = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (messages["success"].ToBoolean())
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "Booking was successfully marked as no show!",
                            Core.Enumerations.NotificationType.SUCCESS);
                    }
                    else
                    {
                        Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                            "An error occured while changing booking status to no show.",
                            Core.Enumerations.NotificationType.ERROR);
                    }

                    return Json(new { success = messages["success"].ToBoolean(), message = messages["message"].ToString() }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = false, message = "" });
            }
        }
        #endregion

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult SendPaymentToCustomer(Booking.com.Models.SendPaymentDetails sendPaymentModel)
        {
            var path = Server.MapPath("~/EmailTemplates/PaymentRequestEmailTemplate.html");
            SendConfirmationEmail(sendPaymentModel, path);

            return RedirectToAction("PaymentRequestSent");
        }

        public void SendConfirmationEmail(Booking.com.Models.SendPaymentDetails sendPaymentModel, string path)
        {
            if (!System.IO.File.Exists(path)) return;
            var template = System.IO.File.ReadAllText(path);

            var callbackUrl = "";

            if (sendPaymentModel.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Paypal)
            {
                callbackUrl = Url.Action("PaymentPaypal", "Booking", new { confirmationCode = sendPaymentModel.ConfirmationCode, paymentHistoryId = sendPaymentModel.PaymentHistoryId }, protocol: Request.Url.Scheme);
            }
            else if (sendPaymentModel.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Square)
            {
                callbackUrl = Url.Action("PaymentSquare", "Booking", new { confirmationCode = sendPaymentModel.ConfirmationCode, paymentHistoryId = sendPaymentModel.PaymentHistoryId }, protocol: Request.Url.Scheme);
            }
            else if (sendPaymentModel.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Stripe)
            {
                callbackUrl = Url.Action("PaymentStripe", "Booking", new { confirmationCode = sendPaymentModel.ConfirmationCode, paymentHistoryId = sendPaymentModel.PaymentHistoryId }, protocol: Request.Url.Scheme);
            }

            template = template.Replace("@Name", sendPaymentModel.Customer)
                    .Replace("@Code", sendPaymentModel.ConfirmationCode)
                    .Replace("@CheckIn", sendPaymentModel.CheckIn.ToString("MMMM dd,yyyy"))
                    .Replace("@CheckOut", sendPaymentModel.CheckOut.ToString("MMMM dd,yyyy"))
                    .Replace("@TotalAmount", sendPaymentModel.AmountDue)
                    .Replace("@Link", callbackUrl)
                ;

            var success = _emailService.SendEmail(sendPaymentModel.MailSubject, template, sendPaymentModel.Email);
        }

        //#region Payment

        //public ActionResult PaymentPaypal(string confirmationCode, long paymentHistoryId = 0)
        //{
        //    try
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            var reservation = Core.API.Booking.com.Inquiries.GetByConfirmationCode(confirmationCode);
        //            var paymentRequest = Core.API.Booking.com.Inquiries.GetPaymentHistory(confirmationCode, paymentHistoryId);
        //            var apiContext = _payPalService.GetApiContext();
        //            var payerId = Request.Params["PayerID"];

        //            if (!string.IsNullOrEmpty(payerId))
        //            {
        //                var _confirmationCode = Request.Params["confirmationCode"];
        //                var _paymentHistoryId = Request.Params["paymentHistoryId"];
        //                var paymentId = Request.Params["paymentId"];

        //                var executedPayment = _payPalService.ExecutePayment(apiContext, payerId, paymentId);
        //                executedPayment.state = "approved";

        //                if (executedPayment.state.ToLower() != "approved")
        //                {
        //                    return View("PaymentFail");
        //                }
        //                else
        //                {
        //                    Core.API.Booking.com.Inquiries.SettlePaymentRequest(_confirmationCode, _paymentHistoryId.ToLong());
        //                    Core.API.Booking.com.Inquiries.UpdateIfPaid(_confirmationCode);
        //                }
        //            }
        //            else if (!string.IsNullOrEmpty(paymentRequest.PaypalRedirectUrl))
        //            {
        //                return Redirect(paymentRequest.PaypalRedirectUrl);
        //            }
        //            else
        //            {
        //                Session["ReservationId"] = confirmationCode;

        //                var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                            "/Booking/PaymentPaypal?";

        //                var guid = Convert.ToString((new System.Random()).Next(100000));

        //                var sendPaymentModel = new Booking.com.Models.SendPaymentDetails
        //                {
        //                    ConfirmationCode = paymentRequest.ConfirmationCode,
        //                    Customer = Core.API.Booking.com.Guests.GetNameById(reservation.GuestId),
        //                    TotalAmount = paymentRequest.PaymentPrice.ToString(),
        //                    PaymentHistoryId = paymentRequest.Id,
        //                    Currency = Core.API.Booking.com.Properties.GetByListingId(reservation.PropertyId).Currency
        //                };

        //                var createdPayment = _payPalService.CreateFullPayment(apiContext, baseUri + "guid=" + guid + "&confirmationCode=" + confirmationCode + "&paymentHistoryId=" + paymentHistoryId, sendPaymentModel);

        //                var links = createdPayment.links.GetEnumerator();

        //                string paypalRedirectUrl = null;

        //                while (links.MoveNext())
        //                {
        //                    var lnk = links.Current;

        //                    if (lnk.rel.ToLower().Trim().Equals("approval_url"))
        //                    {
        //                        paypalRedirectUrl = lnk.href;
        //                    }
        //                }

        //                //Core.API.Booking.com.Inquiries.PaymentRequestSent(confirmationCode, paypalRedirectUrl);
        //                Core.API.Booking.com.Inquiries.AddPaypalRedirectUrl(confirmationCode, paymentHistoryId, paypalRedirectUrl);
        //                Session.Add(guid, createdPayment.id);
        //                return Redirect(paypalRedirectUrl);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return View("PaymentFail");
        //    }

        //    return View("PaymentSuccess");
        //}

        //public ActionResult PaymentStripe(string confirmationCode, long paymentHistoryId)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        if (confirmationCode == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }

        //        var paymentRequest = Core.API.Booking.com.Inquiries.GetPaymentHistory(confirmationCode, paymentHistoryId);

        //        if (paymentRequest == null)
        //        {
        //            return HttpNotFound();
        //        }

        //        var paymentModel = new Booking.com.Models.SendPaymentDetails();
        //        paymentModel.AmountDue = paymentRequest.PaymentPrice.ToString();
        //        paymentModel.ConfirmationCode = confirmationCode;
        //        paymentModel.PaymentHistoryId = paymentHistoryId;

        //        return View(paymentModel);
        //    }
        //}

        ////This method handles the payment integration to stripe from the page
        //[HttpPost]
        //public ActionResult PaymentStripe(string stripeEmail, string stripeToken,
        //    string confirmationCode, long paymentHistoryId)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var reservation = Core.API.Booking.com.Inquiries.GetByConfirmationCode(confirmationCode);
        //        var paymentRequest = Core.API.Booking.com.Inquiries.GetPaymentHistory(confirmationCode, paymentHistoryId);
        //        var amountDue = Math.Round(Convert.ToDecimal(paymentRequest.PaymentPrice), 2);

        //        var customers = new StripeCustomerService();
        //        var charges = new StripeChargeService();

        //        StripeConfiguration.SetApiKey(Core.API.Booking.com.Hosts.ConfigurationKeys.StripeSecretKey);

        //        var customer = customers.Create(new StripeCustomerCreateOptions
        //        {
        //            Email = stripeEmail,
        //            SourceToken = stripeToken
        //        });

        //        var charge = charges.Create(new StripeChargeCreateOptions
        //        {
        //            Amount = Convert.ToInt32(amountDue.ToString().Replace(".", "")),
        //            Description = "Payment for Reservation No. " + confirmationCode,
        //            Currency = Core.API.Booking.com.Properties.GetByListingId(reservation.PropertyId).Currency.ToLower(),
        //            CustomerId = customer.Id
        //        });

        //        if (charge.Status.Equals("succeeded"))
        //        {
        //            Core.API.Booking.com.Inquiries.SettlePaymentRequest(confirmationCode, paymentHistoryId);
        //            Core.API.Booking.com.Inquiries.UpdateIfPaid(confirmationCode);
        //            return View("PaymentSuccess");
        //        }
        //        else
        //        {
        //            return View("PaymentFail");
        //        }
        //    }
        //}

        //public ActionResult PaymentSquare(string confirmationCode, long paymentHistoryId)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        if (confirmationCode == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }

        //        var paymentRequest = Core.API.Booking.com.Inquiries.GetPaymentHistory(confirmationCode, paymentHistoryId);

        //        if (paymentRequest == null)
        //        {
        //            return HttpNotFound();
        //        }

        //        var squarePayment = new Booking.com.Models.SendPaymentDetails();
        //        squarePayment.AmountDue = paymentRequest.PaymentPrice.ToString();
        //        squarePayment.ConfirmationCode = confirmationCode;
        //        squarePayment.PaymentHistoryId = paymentHistoryId;

        //        return View(squarePayment);
        //    }
        //}

        ////This method handles the payment integration to square from the page
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult PaymentSquare(Booking.com.Models.SendPaymentDetails squarePayment)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var reservation = Core.API.Booking.com.Inquiries.GetByConfirmationCode(squarePayment.ConfirmationCode);
        //        var amountDue = Math.Round(Convert.ToDecimal(squarePayment.AmountDue), 2);
        //        var response = _squarePaymentService.ChargeCard(squarePayment.CreditCardNonce,
        //            Convert.ToInt64(amountDue.ToString().Replace(".", "")),
        //            Core.API.Booking.com.Properties.GetByListingId(reservation.PropertyId).Currency.ToUpper());

        //        if (response != null)
        //        {
        //            Core.API.Booking.com.Inquiries.SettlePaymentRequest(squarePayment.ConfirmationCode, squarePayment.PaymentHistoryId);
        //            Core.API.Booking.com.Inquiries.UpdateIfPaid(squarePayment.ConfirmationCode);

        //            return View("PaymentSuccess");
        //        }
        //        else
        //        {
        //            return View("PaymentFail");
        //        }
        //    }
        //}

        //#endregion
        [HttpPost]
        public ActionResult AddAndSendPaymentRequest(
            Core.Enumerations.PaymentRequestType paymentType,
            Core.Enumerations.PaymentRequestMethod paymentMethod,
            string paymentName, decimal paymentPrice,/* decimal balanceAfter,*/
            string confirmationCode, string guestEmail, string paymentString)
        {
            try
            {
                if (string.IsNullOrEmpty(guestEmail))
                {
                    return Json(new
                    {
                        success = false,
                        message = "E-mail is required. You can't send this payment request without guest's e-mail address."
                    },
                        JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var reservation = Core.API.Booking.com.Inquiries.GetByConfirmationCode(confirmationCode);
                    var emailSavingSuccess = Core.API.Booking.com.Guests.SaveEmail(reservation.GuestId, guestEmail);

                    var result = Core.API.Booking.com.Inquiries.AddPaymentRequest(
                        paymentType, paymentMethod, paymentName, paymentPrice,
                        0, confirmationCode);

                    var success = result.Item1;
                    var paymentHistoryId = result.Item2;

                    if (success)
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            var batchPaymentId = 0;
                            var batchPayment = new IncomeBatchPayment()
                            {
                                Amount = paymentPrice,
                                Description = paymentName,
                                BookingId = reservation.Id,
                                PaymentDate = DateTime.Now,
                                CompanyId = SessionHandler.CompanyId
                            };
                            db.IncomeBatchPayments.Add(batchPayment);
                            db.SaveChanges();
                            batchPaymentId = batchPayment.Id;
                            var data = JArray.Parse(paymentString);
                            foreach (var income in data)
                            {
                                if (income["Amount"].ToString() != "" && income["Amount"].ToString() != null && income["Amount"].ToString() != "0")
                                {
                                    if (income["PaymentId"].ToString() == "0")
                                    {
                                        IncomePayment objIncomePayment = new IncomePayment();
                                        objIncomePayment.Amount = income["Amount"].ToDecimal();
                                        objIncomePayment.CreatedAt = DateTime.UtcNow;
                                        objIncomePayment.Description = paymentName;
                                        objIncomePayment.Type = (int)IncomePaymentType.Payment;
                                        objIncomePayment.IncomeId = income["IncomeId"].ToInt();
                                        objIncomePayment.IncomeBatchPaymentId = batchPaymentId;
                                        db.IncomePayment.Add(objIncomePayment);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        var tempid = income["PaymentId"].ToInt();
                                        var tempPayment = db.IncomePayment.Where(x => x.Id == tempid).FirstOrDefault();
                                        tempPayment.Amount = income["Amount"].ToDecimal();
                                        tempPayment.Description = paymentName;
                                        db.Entry(tempPayment).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                                else if ((income["Amount"].ToString() == "" || income["Amount"].ToString() == null || income["Amount"].ToString() == "0") && income["PaymentId"].ToString() != "0")
                                {
                                    var id = income["PaymentId"].ToInt();
                                    db.IncomePayment.Remove(db.IncomePayment.Where(x => x.Id == id).FirstOrDefault());
                                    db.SaveChanges();
                                }
                            }
                        }
                        var path = Server.MapPath("~/EmailTemplates/PaymentRequestEmailTemplate.html");
                        var guestName = Core.API.Booking.com.Guests.GetNameById(reservation.GuestId);
                        if (guestName == null)
                        {
                            using (var db = new ApplicationDbContext())
                            {
                                var renters = db.Renters.Where(x => x.BookingId == reservation.Id).Select(x => new { Name = x.Firstname + " " + x.Lastname }).ToList();
                                foreach (var renter in renters)
                                {
                                    guestName += renter + ",";
                                }
                            }
                        }
                        var paymentModel = new Booking.com.Models.SendPaymentDetails
                        {
                            ConfirmationCode = confirmationCode,
                            Customer = guestName,
                            CheckIn = (DateTime)reservation.CheckInDate,
                            CheckOut = (DateTime)reservation.CheckOutDate,
                            AmountDue = paymentPrice.ToString(),
                            MailSubject = paymentName,
                            PaymentMethod = paymentMethod,
                            PaymentHistoryId = paymentHistoryId,
                            Email = guestEmail
                        };

                        SendConfirmationEmail(paymentModel, path);

                        return Json(new { success = true, message = "Successfully added !" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false,
                            message = "An error occured. Something went wrong while adding your partial payment request."
                        },
                            JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    message = "An error occured. Something went wrong while adding your partial payment request."
                },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeletePaymentRequest(long paymentHistoryId)
        {
            var success = Core.API.Booking.com.Inquiries.DeletePaymentHistory(paymentHistoryId);

            if (success)
            {
                return Json(new { success = true, message = "Successfully deleted !" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "An error occured. Something went wrong while trying to delete the payment request."
                },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ResendPaymentRequest(string confirmationCode, long paymentHistoryId)
        {
            try
            {
                var path = Server.MapPath("~/EmailTemplates/PaymentRequestEmailTemplate.html");
                var reservation = Core.API.Booking.com.Inquiries.GetByConfirmationCode(confirmationCode);
                var paymentRequest = Core.API.Booking.com.Inquiries.GetPaymentHistory(confirmationCode, paymentHistoryId);
                var guestName = Core.API.Booking.com.Guests.GetNameById(reservation.GuestId);

                var paymentModel = new Booking.com.Models.SendPaymentDetails
                {
                    ConfirmationCode = confirmationCode,
                    Customer = guestName,
                    CheckIn = (DateTime)reservation.CheckInDate,
                    CheckOut = (DateTime)reservation.CheckOutDate,
                    AmountDue = paymentRequest.PaymentPrice.ToString(),
                    MailSubject = paymentRequest.PaymentName,
                    PaymentMethod = (Core.Enumerations.PaymentRequestMethod)paymentRequest.PaymentMethod,
                    PaymentHistoryId = paymentHistoryId
                };

                SendConfirmationEmail(paymentModel, path);

                return Json(new { success = true, message = "Successfully sent !" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    message = "An error occured. Something went wrong while trying to re-send the payment request."
                },
                    JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public async Task<ActionResult> UpdateReservationStatus(string userId, int type, string message, string outbox)
        {
            var notifType = (Core.Enumerations.NotificationType)type;
            var js = new JavaScriptSerializer();
            var outboxData = js.Deserialize<Outbox>(outbox);
            Core.SignalR.Hubs.NotificationHub.NotifyUser(userId,
                           message, notifType, outboxData);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

//#endregion
    }
}