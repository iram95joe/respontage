﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Models.Devices;
using MessagerSolution.Models.Videos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class VideoController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var filterProperties = new List<Property>();
                using (var db = new ApplicationDbContext())
                {
                    var properties = (from h in db.Hosts
                                      join hc in db.HostCompanies on h.Id equals hc.HostId
                                      join p in db.Properties on h.Id equals p.HostId
                                      where (hc.CompanyId == SessionHandler.CompanyId && p.SiteType != 3) && p.IsActive
                                      select p).ToList();
                    properties.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());
                    foreach (var local in properties)
                    {
                        var t = db.ParentChildProperties.Where(x => x.ChildPropertyId == local.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (t == null && local.IsParentProperty == false)
                            filterProperties.Add(local);
                        else if (local.IsParentProperty && local.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts)
                        {
                            filterProperties.Add(local);
                        }
                    }
                }
                ViewBag.Properties = filterProperties;
                return View();
            }
            return RedirectToAction("Index", "Home");
            
        }
        public async Task<ActionResult> Login(string username, string password,string site)
        {
            try
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority;
                switch (site)
                {
                    case "Arlo":
                        using (var sm = new MultiMedia.Arlo.ScrapeManager())
                        {
                            var loginresult = sm.LogIn(username, password, url, SessionHandler.CompanyId);
                                return Json(new { success = loginresult.Success }, JsonRequestBehavior.AllowGet);
                        }
                    case "Blink":
                        using (var sm = new Blink.ScrapeManager())
                        {
                            var loginresult = sm.LogIn(username, password, url, SessionHandler.CompanyId,true);
                            return Json(new { success = loginresult.Success,localId = loginresult.localId,hasSecurity = loginresult.HasSecurity }, JsonRequestBehavior.AllowGet);
                        }
                    case "Wyze":
                        using (var sm = new Wyze.ScrapeManager())
                        {
                            var loginresult = sm.LogIn(username, password, url, SessionHandler.CompanyId);
                                return Json(new { success = loginresult.Success }, JsonRequestBehavior.AllowGet);
                        }
                }
                //JM 3/15/2020
                //Remove because we merge Code of multi media scrapper to respontage project
                //We will use this on future
                //using (HttpClient client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(GlobalVariables.ApiVideoUrl);
                //    var reqParams = new Dictionary<string, string>();
                //    reqParams.Add("username", username);
                //    reqParams.Add("password", password);
                //    reqParams.Add("site", site);
                //    reqParams.Add("companyId", SessionHandler.CompanyId.ToString());

                //    var reqContent = new FormUrlEncodedContent(reqParams);

                //    var result = await client.PostAsync("Video/Login", reqContent);
                //    if (result != null && result.Content != null)
                //    {
                //        //It parse the response of API call
                //        //message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                //    }
                //}
            }
            catch (Exception e) 
            { 
            }
            return Json(new { success =false }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult BlinkSecurity(int id,string code)
        {
            using (var sm = new Blink.ScrapeManager())
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority;
                var loginresult = sm.Security(id,code);
                return Json(new {success=loginresult }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetPropertyVideos(string propertyIds,DateTime videoDate)
        {
            using(var db= new ApplicationDbContext())
            {
                List<PropertyVideosModel> propertyVideos = new List<PropertyVideosModel>();
                var pIds = propertyIds.Split(',').Select(Int32.Parse).ToList();
                var propIds = new List<int>();
                int companyId = SessionHandler.CompanyId;

                if (pIds.Contains(0))
                {
                    var properties = (from h in db.Hosts
                                      join hc in db.HostCompanies on h.Id equals hc.HostId
                                      join p in db.Properties on h.Id equals p.HostId
                                      where (hc.CompanyId == companyId && p.SiteType != 3) && p.IsActive
                                      select p).ToList();
                    properties.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());
                    foreach (var local in properties)
                    {
                        var t = db.ParentChildProperties.Where(x => x.ChildPropertyId == local.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (t == null && local.IsParentProperty == false)
                            propIds.Add(local.Id);
                        else if (local.IsParentProperty && local.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts)
                        {
                            propIds.Add(local.Id);
                        }
                    }
                }
                else
                {
                    propIds = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where (hc.CompanyId == companyId) && (p.SiteType != 3 && p.IsActive) && pIds.ToList().Contains(p.Id)
                            select p.Id).ToList();
                    propIds.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == companyId && pIds.Contains(x.Id)).Select(x=>x.Id).ToList());
                }

                foreach (var propertyId in propIds)
                {
                    var pName = db.Properties.Where(x => x.Id == propertyId).Select(x => x.Name).FirstOrDefault();
                    var ids = new List<int>();
                    ids.Add(propertyId);
                    ids.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId==SessionHandler.CompanyId).Select(x => x.ChildPropertyId));

                    var datas = (from property in db.Properties join device in db.Devices on property.ListingId equals device.ListingId join video in db.Videos on device.Id equals video.LocalDeviceId join account in db.DeviceAccounts on device.AccountId equals account.Id where ids.Contains(property.Id) && video.IsDeleted==false && EntityFunctions.TruncateTime(video.DateCreated) == EntityFunctions.TruncateTime(videoDate) select new {Device= device,Video=video,Account=account,Property = property}).OrderByDescending(x=>x.Video.DateCreated).ToList();
                    if (datas.Count == 0)
                        continue;
                    List<VideoModel> Videos  = new List<VideoModel>();
                    foreach(var data in datas)
                    {
                        Videos.Add(new VideoModel() {Id=data.Video.Id,VideoUrl=data.Video.VideoUrl,Description= ((Core.Enumerations.DevicesSiteType)data.Account.SiteType).ToSafeString() +":"+data.Device.Name+"("+data.Device.DeviceId +") ",DateCreated=data.Video.DateCreated,IsSaved=data.Video.IsSaved,IsWatched=data.Video.IsWatched,TimeZoneId=data.Property.TimeZoneId});
                    }
                    propertyVideos.Add(new PropertyVideosModel()
                    {
                        PropertyName = pName,
                        Videos = Videos
                    });
                   
                }
                return Json(new { propertyVideos }, JsonRequestBehavior.AllowGet);
            }

        }
        public async Task<ActionResult>DeleteVideos(string videoIds)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                   "Deleting video...", NotificationType.INFORMATION);
            var Ids =JsonConvert.DeserializeObject<List<int>>( videoIds);

            using(var db = new ApplicationDbContext())
            {
                int failedCount = 0;
                var videos =db.Videos.Where(x => Ids.Contains(x.Id)).ToList();
                var accountIds = videos.GroupBy(x => x.AccountId).Select(x => x.Key).ToList();
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority;
                foreach (var accountId in accountIds)
                {
                    try
                    {
                        var account = db.DeviceAccounts.Where(x => x.Id == accountId).FirstOrDefault();
                        var vids = videos.Where(x => x.AccountId == accountId).ToList();
                        var siteType = (Core.Enumerations.DevicesSiteType)account.SiteType;
                        switch (siteType)
                        {
                            case Core.Enumerations.DevicesSiteType.Arlo:
                                using (var sm = new MultiMedia.Arlo.ScrapeManager())
                                {
                                    var loginresult = sm.LogIn(account.Username, account.Password, url, SessionHandler.CompanyId, false);
                                    if (loginresult.Success)
                                    {
                                        foreach (var vid in vids)
                                        {
                                            var device = db.Devices.Where(x => x.Id == vid.LocalDeviceId).FirstOrDefault();
                                            char[] separator = { ',' };
                                            var details = vid.Details.Split(separator);
                                            var success = sm.DeleteVideo(loginresult.AccessToken, details[1], device.DeviceId, details[2]);
                                            if (success)
                                            {
                                                System.IO.File.Delete(vid.ServerPath);
                                                //System.IO.File.Delete(vid.ServerPath.Replace(".mp4", ".jpeg"));
                                                vid.IsDeleted = true;
                                                db.Entry(vid).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                            else
                                            {
                                                failedCount = failedCount + 1;
                                            }
                                            await System.Threading.Tasks.Task.Delay(2000);
                                        }
                                    }
                                }
                                break;

                            case Core.Enumerations.DevicesSiteType.Blink:
                                using (var sm = new Blink.ScrapeManager())
                                {
                                    var loginresult = sm.LogIn(account.Username, account.Password, url, SessionHandler.CompanyId, false);
                                    if (loginresult.Success)
                                    {//The List of VideoId will delete all video in one request
                                        var isdeleted = sm.DeleteVideo(loginresult, string.Join(",", vids.Select(x => x.SiteVideoId).ToList()));
                                        if (isdeleted)
                                        {
                                            foreach (var vid in vids)
                                            {
                                                System.IO.File.Delete(vid.ServerPath);
                                                vid.IsDeleted = true;
                                                db.Entry(vid).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                                break;
                            case Core.Enumerations.DevicesSiteType.Wyze:
                                using (var sm = new Wyze.ScrapeManager())
                                {
                                    var loginresult = sm.LogIn(account.Username, account.Password, url, SessionHandler.CompanyId, false);
                                    if (loginresult.Success)
                                    {
                                        foreach (var vid in vids)
                                        {
                                            var device = db.Devices.Where(x => x.Id == vid.LocalDeviceId).FirstOrDefault();
                                            var isdeleted = sm.DeleteVideo(loginresult.AccessToken, device.DeviceId, vid.SiteVideoId, (long)(vid.DateCreated - new DateTime(1970, 1, 1)).TotalMilliseconds);
                                            if (isdeleted)
                                            {
                                                System.IO.File.Delete(vid.ServerPath);
                                                vid.IsDeleted = true;
                                                db.Entry(vid).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                            else
                                            {
                                                failedCount = failedCount + 1;
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    catch (Exception e){
                        HtmlLogs.Add("Video Delete AccountId:" + accountId+ " " + e.InnerException + e.Message);
                        HtmlLogs.Add("Video Delete AccountId:" + accountId);
                    }
                }
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                  "Successfully delete video", NotificationType.SUCCESS);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> DeleteVideo(int videoId)
        {
            using (var db = new ApplicationDbContext())
            {
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                   "Deleting video...", NotificationType.INFORMATION);
                var video = db.Videos.Where(x => x.Id == videoId).FirstOrDefault();
                var account = db.DeviceAccounts.Where(x => x.Id == video.AccountId).FirstOrDefault();
                var device = db.Devices.Where(x => x.Id == video.LocalDeviceId).FirstOrDefault();
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority;
                var siteType = (Core.Enumerations.DevicesSiteType)account.SiteType;
                switch (siteType)
                {
                    case Core.Enumerations.DevicesSiteType.Arlo:
                        using (var sm = new MultiMedia.Arlo.ScrapeManager())
                        {
                            var loginresult = sm.LogIn(account.Username, account.Password, url, SessionHandler.CompanyId, false);
                            char[] separator = { ',' };
                            var details = video.Details.Split(separator);
                            var success = sm.DeleteVideo(loginresult.AccessToken, details[1], device.DeviceId, details[2]);
                            if (success)
                            {
                                System.IO.File.Delete(video.ServerPath);
                                video.IsDeleted = true;
                                db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        break;

                    case Core.Enumerations.DevicesSiteType.Blink:
                        using (var sm = new Blink.ScrapeManager())
                        {
                            var loginresult = sm.LogIn(account.Username, account.Password, url, SessionHandler.CompanyId, false);
                            var isdeleted = sm.DeleteVideo(loginresult, video.SiteVideoId);
                            if (isdeleted)
                            {
                                System.IO.File.Delete(video.ServerPath);
                                //System.IO.File.Delete(video.ServerPath.Replace(".mp4", ".jpeg"));
                                video.IsDeleted = true;
                                db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }

                        }
                        break;
                    case Core.Enumerations.DevicesSiteType.Wyze:
                        using (var sm = new Wyze.ScrapeManager())
                        {
                            var loginresult = sm.LogIn(account.Username, account.Password, url, SessionHandler.CompanyId, false);
                            if (loginresult.Success)
                            {
                                var isdeleted = sm.DeleteVideo(loginresult.AccessToken, device.DeviceId, video.SiteVideoId, (long)(video.DateCreated - new DateTime(1970, 1, 1)).TotalMilliseconds);
                                if (isdeleted)
                                {
                                    System.IO.File.Delete(video.ServerPath);
                                    video.IsDeleted = true;
                                    db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                        break;
                }
                    Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                   "Successfully delete video", NotificationType.SUCCESS);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            //using (HttpClient client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(GlobalVariables.ApiVideoUrl);
            //    var reqParams = new Dictionary<string, string>();
            //    reqParams.Add("videoId",videoId);
            //    var reqContent = new FormUrlEncodedContent(reqParams);
            //    var result = await client.PostAsync("Video/DeleteVideo", reqContent);

            //    if (result != null && result.Content != null)
            //    {
            //        return Json(new { success = true}, JsonRequestBehavior.AllowGet);
            //    }
            //    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            //}
        }
        [HttpPost]
        public ActionResult WatchVideo(int videoId)
        {
            using (var db = new ApplicationDbContext())
            {
                var video = db.Videos.Where(x => x.Id == videoId).FirstOrDefault();
                if (video != null)
                {
                    video.IsWatched= true;
                    db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetVideoNote(int videoId) {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.VideoNotes.Where(x => x.VideoId == videoId).FirstOrDefault();
                return Json(new { VideoNote = temp }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveVideoNote(int videoId, string note)
        {
            using (var db = new ApplicationDbContext())
            {
                var video = db.Videos.Where(x => x.Id == videoId).FirstOrDefault();
                if (video != null)
                {
                    video.IsSaved = true;
                    db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    var temp = db.VideoNotes.Where(x => x.VideoId == videoId).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Note = note;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        db.VideoNotes.Add(new Core.Database.Entity.VideoNote() { VideoId = videoId, DateCreated = DateTime.Now, Note = note });
                        db.SaveChanges();

                    }
                }
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                  "Successfully saved video", NotificationType.SUCCESS);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveVideo(int videoId,bool isSave)
        {
            using (var db = new ApplicationDbContext())
            {
                var video = db.Videos.Where(x => x.Id == videoId).FirstOrDefault();
                if (video != null)
                {
                    video.IsSaved = isSave;
                    db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                  "Successfully saved video", NotificationType.SUCCESS);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetVideos(string date,long listingId)
        {
            using (var db= new ApplicationDbContext())
            {
                var dt = date.ToDateTime();
                var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                List<VideoViewModel> data = new List<VideoViewModel>();
                var videos = (from d in db.Devices
                              join v in db.Videos on d.Id equals v.LocalDeviceId
                              join a in db.DeviceAccounts on d.AccountId equals a.Id
                              where d.ListingId == property.ListingId && EntityFunctions.TruncateTime(v.DateCreated) == EntityFunctions.TruncateTime(dt) && v.IsDeleted == false 
                              select new { device =d,video = v,account =a}).OrderByDescending(x=>x.video.DateCreated).ToList();
                foreach( var video in videos)
                {
                    data.Add(new VideoViewModel() {Video = video.video,Device = video.device,Account = video.account});
                }
                var jsonData = Json(new { success = true, videos = data }, JsonRequestBehavior.AllowGet);
                jsonData.MaxJsonLength = int.MaxValue;
                return jsonData;
            }
        }

        public ActionResult GetVideosByDateAndCamera(string date, int deviceId,string startDate, string endDate,long listingId)
        {
            using(var db = new ApplicationDbContext())
            {
                var start = startDate.ToDateTime();
                var end = endDate.ToDateTime();
                var dt = date.ToDateTime(); List<int> propertyIds = new List<int>();
                var localProperty = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                propertyIds.Add(localProperty.Id);
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == localProperty.Id).FirstOrDefault();
                if (parentChild != null)
                {
                    propertyIds.Add(parentChild.ParentPropertyId);
                    propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId).Select(x => x.ChildPropertyId).ToList());
                }
                var listingIds = db.Properties.Where(x => propertyIds.Contains(x.Id)).Select(x => x.ListingId).ToList();
                List<VideoViewModel> data = new List<VideoViewModel>();
                var videos = (from d in db.Devices
                              join v in db.Videos on d.Id equals v.LocalDeviceId
                              join a in db.DeviceAccounts on d.AccountId equals a.Id
                              where v.IsDeleted == false && listingIds.Contains((long)d.ListingId) && (deviceId==0?true:d.Id==deviceId) &&(date==""? (EntityFunctions.TruncateTime(v.DateCreated)>=start && EntityFunctions.TruncateTime(v.DateCreated) <=end): EntityFunctions.TruncateTime(v.DateCreated) == EntityFunctions.TruncateTime(dt))
                              select new { device = d, video = v, account = a }).OrderBy(x => x.video.DateCreated).OrderByDescending(x=>x.video.DateCreated).ToList();
                foreach (var video in videos)
                {
                    data.Add(new VideoViewModel() { Video = video.video, Device = video.device, Account = video.account });
                }
                var jsonData =Json(new { success = true, videos = data }, JsonRequestBehavior.AllowGet);
                jsonData.MaxJsonLength = int.MaxValue;
                return jsonData;
            }
        }
        [HttpPost]
        public ActionResult PostVideoResult(VideoResult video)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = db.Videos.Where(x => x.VideoUrl.Contains(video.Filename)).FirstOrDefault();
                    if (temp != null) {
                    temp.InCount = video.InCount;
                    temp.OutCount = video.OutCount;
                    temp.IsProcessed = true;
                    db.SaveChanges();
                }
            }
            return Json(new { success=true}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUnprocessVideos()
        {
            using (var db = new ApplicationDbContext())
            {
               var filenames= db.Videos.Where(x => x.IsProcessed == false).Select(x=>x.VideoUrl).ToList();
                return Json(new { filenames }, JsonRequestBehavior.AllowGet);
            }
                
        }
    }
}