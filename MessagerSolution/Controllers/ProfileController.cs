﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MessagerSolution.Controllers
{
    public class ProfileController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        private readonly Service.SquareService _squarePaymentService = new Service.SquareService();
        //
        // GET: /Profile/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int companyId)
        {
            if (objIList.CheckValidCompany(Convert.ToInt32(User.Identity.Name), companyId) == false) { return Redirect("/"); }

            FormsIdentity fid = User.Identity as FormsIdentity;
            string plainTextUserData = fid.Ticket.UserData;
            ViewBag.UserRole = plainTextUserData.ToLower();
            ViewBag.UserId = User.Identity.Name;
            return View();
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetProfileDetails()
        {
            try
            {
                int companyId = SessionHandler.CompanyId;
                var result = objIList.GetUser(Convert.ToInt32(User.Identity.Name));
                return Json(new { success = true, result = result }, JsonRequestBehavior.AllowGet);
            }
            catch { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
        }
        [NonAction]
        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UpdateProfile(User objUser,HttpPostedFileWrapper Image)
        {
            if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false) { return Redirect("/"); }

            var result = objIList.GetUser(Convert.ToInt32(User.Identity.Name));
            result.FirstName = objUser.FirstName;
            result.LastName = objUser.LastName;
            result.UserName = objUser.UserName;
            result.TimeZone = objUser.TimeZone;
            if (Image != null)
            {
                result.ImageUrl = UploadImage(Image, "~/Images/User/");
            }
            if (result.WorkerId !=0 && result.WorkerId != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    var worker = db.Workers.Where(x => x.Id == result.WorkerId).FirstOrDefault();
                    worker.Firstname = objUser.FirstName;
                    worker.Lastname = objUser.LastName;
                    if (Image != null)
                    {
                        worker.ImageUrl = UploadImage(Image, "~/Images/Worker/");
                    }
                    db.Entry(worker).State =System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return Json(new { success = objIList.UpdateUser(result) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetSubUserDetails(int id)
        {
            using(var db = new ApplicationDbContext())
            {
                var subUser = db.Users.Find(id);
                List<int> roles = new List<int>();
                if(subUser != null)
                {
                    db.tbRoleLink.Where(x => x.UserId == id).ToList()
                        .ForEach(a => { roles.Add(a.RoleId); });
                }
                return Json(new { success = subUser!= null, sub_user = subUser, roles = roles }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetSubUserList()
        {
            try
            {
                List<SubUserTableData> sul = new List<SubUserTableData>();
                var subUsers = objIList.GetSubUser(Convert.ToInt32(User.Identity.Name));
                subUsers.ForEach(item =>
                {
                    SubUserTableData u = new SubUserTableData();
                    u.Id = item.Id;
                    u.FirstName = item.FirstName;
                    u.LastName = item.LastName;
                    u.Status = item.Status ? "Active" : "Blocked" ;
                    u.Email = item.UserName;
                    u.Actions = "<a class=\"ui mini button edit-sub-user\" title=\"Edit\"><i class=\"icon edit\"></i></a> " +
                                "<a class=\"ui mini button delete-sub-user\" title=\"Delete\"><i class=\"icon trash alternate\"></i></a>" +
                                (item.Status? "<a class=\"ui mini button block-sub-user\" title=\"Block\"> <i class=\"icon ban\"></i></a>" :
                                "<a class=\"ui mini button unblock-sub-user\" title=\"Unblock\"> <i class=\"icon ban\"></i></a>");
                    sul.Add(u);
                });
                return Json(new { data = sul }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception) { return Json(new { data = new List<SubUserTableData>() }, JsonRequestBehavior.AllowGet); }
        }

        //getting DELETE USER
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteUser(int id)
        {
            try
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false) { return Redirect("/"); }
                objIList.DeleteUserRole(id);
                return Json(new { success = objIList.DeleteUser(id) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, error = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //getting BLOCK USER
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Block(int id)
        {
            try { return Json(new { success = objIList.BlockUser(id) }, JsonRequestBehavior.AllowGet); }
            catch { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
        }

        //getting UNBLOCK USER
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Unblock(int id)
        {
            try { return Json(new { success = objIList.UnBlockUser(id) }, JsonRequestBehavior.AllowGet); }
            catch { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
        }


        //getting Change Password
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ChangePassword(ChangePassword model)
        {
            try
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }

                model.OldPassword = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], model.OldPassword, true);
                model.NewPassword = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], model.NewPassword, true);

                var result = objIList.ChangePassword(Convert.ToInt32(User.Identity.Name), model.OldPassword, model.NewPassword);
                return Json(new { success = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetRegister(UserRoleModel objUser,HttpPostedFileWrapper Image)
        {
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                //encrypting the password for security
                string userPassword = objUser.Password;
                objUser.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], objUser.Password.Trim(), true);
                var isNewCompany = false;
                // setting the ParentId of subaccount.
                if (User.Identity.IsAuthenticated)
                {
                    if (User.Identity.Name.Trim() != "")
                    {
                        objUser.CompanyId = SessionHandler.CompanyId;
                    }
                }
                else
                {
                    using(var db = new ApplicationDbContext())
                    {
                        var company = db.Companies.Add(new Company() { CompanyName = objUser.CompanyName });
                        db.SaveChanges();
                        objUser.CompanyId = company.Id;
                        db.CompanyTypeCompanies.Add(new CompanyTypeCompany() {CompanyId =company.Id,CompanyTypeId=objUser.CompanyType });
                        db.SaveChanges();
                        isNewCompany = true;
                    }
                }
                //checking the the already existing or not.
                var resultValid = objCommon.ValidUser(objUser.UserName);
                if (resultValid != null)
                {
                    return Json("A1", JsonRequestBehavior.AllowGet);
                }



                User objtbUser = new User();
                objtbUser.FirstName = objUser.FirstName;
                objtbUser.LastName = objUser.LastName;
                objtbUser.Password = objUser.Password;
                objtbUser.TimeZone = objUser.TimeZone;
                objtbUser.UserName = objUser.UserName;
                objtbUser.ValidStatus = objUser.ValidStatus;
                objtbUser.Status = objUser.Status;
                objtbUser.ConfirmEmail = objUser.ConfirmEmail;
                objtbUser.CompanyId = objUser.CompanyId;
                objtbUser.Status = true;
                objUser.ValidStatus = false;
                objtbUser.CreatedDate = DateTime.Now;
                if (Image != null)
                {
                    objtbUser.ImageUrl = UploadImage(Image, "~/Images/User/");
                }
                //saving the detail of register user into db
                var result = objCommon.SaveUser(objtbUser);
                    for (int i = 0; i < objUser.RoleId.Count(); i++)
                    {
                        tbRoleLink objRoleLink = new tbRoleLink();
                        objRoleLink.RoleId = objUser.RoleId[i];
                        objRoleLink.UserId = result.Id;
                        objCommon.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);
                    }
                if (result != null)
                {
                    objUser.Id = result.Id;
                    _squarePaymentService.CreateSubscription(objUser);
                    //sending email to username for check the valid email.
                    if (isNewCompany)
                    {
                        Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p>Hi " + objtbUser.FirstName + ' ' + objtbUser.LastName + ",</p><p> Thank you for creating new account on messager website. <br style='clear:both' /> <p>Please click on following link to validate the account</p> <br style='clear:both' /> <a href='" + string.Format("{0}://{1}{2}",
                                                         HttpContext.Request.Url.Scheme,
                                                         HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                         (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                         ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>Click to validate the account</a> </div>", objUser.UserName.Trim(), "Respontage.com");
                    }
                    else
                    {
                        Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p>Hi " + objtbUser.FirstName + ' ' + objtbUser.LastName + ",</p><p> Thank you for creating new account on messager website. <br style='clear:both' /> <p>Please click on following link to validate the account, Your password is " + userPassword + "</p> <br style='clear:both' /> <a href='" + string.Format("{0}://{1}{2}",
                                                       HttpContext.Request.Url.Scheme,
                                                       HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                       (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                       ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>Click to validate the account</a> </div>", objUser.UserName.Trim(), "Respontage.com");
                    }
                    return Json("S1", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("E1", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com reset password, click this <a href='" + string.Format("{0}://{1}{2}",
                                 HttpContext.Request.Url.Scheme,
                                 HttpContext.Request.ServerVariables["HTTP_HOST"],
                                 (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                 ) + "/Profile/ResetPassword?Email=" + Email + "'>link</a></p></div>", Email.Trim(), "Respontage.com");
            return Json("S1", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult ResetPassword(string Email)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(x => x.UserName == Email).FirstOrDefault();
                if(user == null)
                {
                    return HttpNotFound();
                }

                return View(user);
            }

        }

        [HttpPost]
        public ActionResult ResetPassword(string Email,string Password)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(x => x.UserName == Email).FirstOrDefault();
                if (user == null)
                {
                    return Json("V1", JsonRequestBehavior.AllowGet); 
                }
                else
                {
                    user.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"],Password.Trim(), true);
                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return Json("S1", JsonRequestBehavior.AllowGet);
                }


            }
        }
        
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UpdateSubUserDetail(UserRoleModel objUser,HttpPostedFileWrapper Image)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objCommon.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            var result = objCommon.GetUser(Convert.ToInt32(objUser.Id));
            result.FirstName = objUser.FirstName;
            result.LastName = objUser.LastName;
            result.UserName = objUser.UserName;
            if (Image != null)
            {
                result.ImageUrl = UploadImage(Image, "~/Images/User/");
            }
            if (result.WorkerId != 0 && result.WorkerId != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    var worker = db.Workers.Where(x => x.Id == result.WorkerId).FirstOrDefault();
                    worker.Firstname = objUser.FirstName;
                    worker.Lastname = objUser.LastName;
                    db.Entry(worker).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            if (objUser.Password != "")
            {
                Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), string.Format("The admin reset your password, the new password is {0}", objUser.Password), objUser.UserName.Trim(), "Respontage.com");
            }
            objCommon.UpdateUser(result);

            //resetting the role of current select sub-user
            objCommon.DeleteUserRole(objUser.Id);
            for (int i = 0; i < objUser.RoleId.Count(); i++)
            {
                tbRoleLink objRoleLink = new tbRoleLink();
                objRoleLink.RoleId = objUser.RoleId[i];
                objRoleLink.UserId = result.Id;
                objCommon.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);
            }
            return Json("S1", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult SavePaymentAccountInformation(PaymentAccount paymentAccount)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.PaymentAccount.Where(x => x.CompanyId == SessionHandler.CompanyId && x.PaymentMethod == paymentAccount.PaymentMethod).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.PaypalClientId = paymentAccount.PaypalClientId;
                        temp.PaypalClientSecret = paymentAccount.PaypalClientSecret;
                        temp.SquareAccessToken = paymentAccount.SquareAccessToken;
                        temp.SquareApplicationId = paymentAccount.SquareApplicationId;
                        temp.SquareLocationId = paymentAccount.SquareLocationId;
                        temp.StripePublishableKey = paymentAccount.StripePublishableKey;
                        temp.StripeSecretKey = paymentAccount.StripeSecretKey;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        paymentAccount.CompanyId = SessionHandler.CompanyId;
                        db.PaymentAccount.Add(paymentAccount);
                        db.SaveChanges();
                    }
                }
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }
    }
}