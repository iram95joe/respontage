﻿using Core.Database.Context;
using Core.Database.Entity;
using MessagerSolution.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Api.V2010.Account.AvailablePhoneNumberCountry;
using Twilio.TwiML;

namespace MessagerSolution.Controllers
{   
    public class TwilioAccountController : TwilioController
    {
        [HttpGet]
        public ActionResult Index(
            string country = "US",
            bool? smsEnabled = true,
            bool? mmsEnabled = true,
            bool? voiceEnabled = true,
            bool? faxEnabled = null,
            string contains = null,
            int? areaCode = null,
            int? pageSize = null
            )
        {

            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

            TwilioClient.Init(accountSid, authToken);

            try
            {
                var availableNumbers = LocalResource.Read(
                    country,
                    smsEnabled: smsEnabled,
                    mmsEnabled: mmsEnabled,
                    voiceEnabled: voiceEnabled,
                    faxEnabled: faxEnabled,
                    contains: contains,
                    areaCode: areaCode,
                    pageSize: pageSize
                );

                return Json(new { success = true, availableNumbers }, JsonRequestBehavior.AllowGet);
            } catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}