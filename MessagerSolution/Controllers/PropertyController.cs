﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Extensions;
using Core.Helper;
using Core.Repository;
using Core.Sites.Smartthings.Models;
using MessagerSolution.Helper;
using MessagerSolution.Helper.Location;
using MessagerSolution.Models;
using MessagerSolution.Models.Properties;
using MessagerSolution.Models.Worker;
using MlkPwgen;
//using MessagerSolution.Models.Tasks;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MessagerSolution.Controllers
{
    public class PropertyController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        //
        // GET: /Property/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int companyId)
        {
            if (User.Identity.IsAuthenticated)
            {

                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Properties") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.PropType = new SelectList(objIList.GetAllPropertyType(), "Id", "Property_type");
                ViewBag.Countries =new SelectList(objIList.GetCountries(), "Id", "Name");
                ViewBag.PropertyType = objIList.GetAllPropertyType();
                ViewBag.Properties = objIList.GetProperties();
                ViewBag.TemplateList = objIList.GetWorkerTemplateMessages();
                string drp = "";
                var allfeetype = objIList.GetAllFeeType();
                foreach (var f in allfeetype)
                {
                    drp += @"<option value=" + f.Id.ToString() + ">" + f.Fee_Type + "</option>";
                }
                ViewBag.feetype = drp;
                ViewBag.Hosts = Core.API.AllSite.Hosts.GetList;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpGet]
        public ActionResult GetRequestContracts()
        {
            using (var db = new ApplicationDbContext())
            {
                List<RequestContractViewModel> data = new List<RequestContractViewModel>();
                var results = (from property in db.Properties join contract in db.ContractRequests on property.Id equals contract.PropertyId where property.CompanyId == SessionHandler.CompanyId select new { PropertyId = property.Id, PropertyName = property.Name, Contract = contract }).OrderByDescending(x => x.Contract.RequestDate).ToList();
                foreach (var result in results)
                {
                    data.Add(new RequestContractViewModel()
                    {
                        PropertyId = result.PropertyId,
                        PropertyName = result.PropertyName,
                        EndDate = result.Contract.EndDate,
                        StartDate = result.Contract.StartDate,
                        RequestDate = result.Contract.RequestDate,
                        Status = result.Contract.Status,
                        Id = result.Contract.Id,
                        RenterName = db.RenterRequests.Where(x => x.ContractRequestId == result.Contract.Id).Select(x => x.Name).FirstOrDefault()

                    });
                }

                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetStates(int id)
        {
            using(var db = new ApplicationDbContext())
            {
                var states = db.States.Where(x => x.CountryId == id).ToList();
                return Json(new { states }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetCities(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var cities = db.Cities.Where(x => x.StateId == id).ToList();
                return Json(new { cities }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetPropertyForMaintenanceAndExpense(int propertyId)
        {
            using (var db = new ApplicationDbContext()) {
                Property property;
                var temp = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault();
                if (temp.IsParentProperty && temp.ParentType==(int)Core.Enumerations.ParentPropertyType.Sync)
                {
                    //property = db.Properties.Where(x => x.ParentPropertyId == temp.Id && x.SiteType==0).FirstOrDefault();
                    property = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where p.CompanyId ==SessionHandler.CompanyId &&  p.SiteType == 0 && pc.ParentPropertyId == temp.Id select p).FirstOrDefault();

                }
                else
                {
                    property = temp;
                }

            return Json(new { property}, JsonRequestBehavior.AllowGet); }
        }

        //JM 03/02/19 Select all proeprty for message rule template
        [HttpGet]
        public ActionResult GetProperty()
        {
            using (var db = new ApplicationDbContext())
            {
                var Parentproperties = (from h in db.Hosts
                                        join hc in db.HostCompanies on h.Id equals hc.HostId
                                        join p in db.Properties on h.Id equals p.HostId
                                        where hc.CompanyId == SessionHandler.CompanyId && p.IsParentProperty
                                        select p).ToList();

                var Childroperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId && (p.IsParentProperty == false) select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();

                return Json(new { Parentproperties = Parentproperties, Childroperties = Childroperties }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateIsActive(int propertyId, bool isActive)
        {
            using (var db = new ApplicationDbContext())
            {
                var property = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault();
                property.IsActive = isActive;
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Template
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetMessageRulesList(string propertyFilter)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    string actions = "<a class=\"ui mini button edit-message-rule-btn\" title=\"Edit\">" +
                                         "<i class=\"icon edit\"></i></a>" +
                                         "<a class=\"ui mini button delete-message-rule-btn\" title=\"Delete\">" +
                                         "<i class=\"icon delete\"></i></a>";
                    List<WorkerMessageRuleTableData> mr = new List<WorkerMessageRuleTableData>();
                    var messageRules = db.WorkerMessageRules.Join(db.WorkerTemplateMessages, x => x.WorkerTemplateMessageId, y => y.Id, (x, y) => new { WorkerMessageRules = x, WorkerTemplateMessage = y })
                        .Join(db.Properties, x => x.WorkerMessageRules.PropertyId, y => y.Id, (x, y) => new { WorkerMessageRules = x.WorkerMessageRules, WorkerTemplateMessage = x.WorkerTemplateMessage, Property = y })
                        .ToList();

                    if (propertyFilter != "all")
                    {
                        int propertyId = int.Parse(propertyFilter);
                        messageRules = messageRules.Where(x => x.Property.Id == propertyId).ToList();
                    }

                    messageRules.ForEach(x =>
                    {
                        WorkerMessageRuleTableData temp = new WorkerMessageRuleTableData();
                        temp.Id = x.WorkerMessageRules.Id;
                        temp.PropertyName = x.Property.Name;
                        temp.Title = x.WorkerTemplateMessage.Title;
                        temp.Time = x.WorkerMessageRules.Time.ToString();
                        temp.Message = x.WorkerTemplateMessage.Message;
                        temp.Actions = actions;
                        mr.Add(temp);
                    });
                    return Json(new { data = mr }, JsonRequestBehavior.AllowGet);

                }
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetTemplateList()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        string actions = "<a class=\"ui mini button edit-template-btn\" title=\"Edit\">" +
                                         "<i class=\"icon edit\"></i></a>" +
                                         "<a class=\"ui mini button delete-template-btn\" title=\"Delete\">" +
                                         "<i class=\"icon delete\"></i></a>";

                        List<TemplateMessageTableData> td = new List<TemplateMessageTableData>();
                        var accounts = db.WorkerTemplateMessages.Where(x => x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.Title).ToList();
                        accounts.ForEach(x =>
                        {
                            TemplateMessageTableData d = new TemplateMessageTableData();
                            d.Id = x.Id;
                            d.Title = x.Title;
                            d.Message = x.Message;
                            d.Actions = actions;
                            td.Add(d);
                        });
                        return Json(new { data = td }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetTemplates()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    var templates = db.WorkerTemplateMessages.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    return Json(new { success = true, templates }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetTemplate(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    var template = db.WorkerTemplateMessages.Where(x => x.Id == id && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                    return Json(new { success = true, template }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteTemplate(int id)
        {
            bool isSuccess = false;
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.WorkerTemplateMessages.Where(x => x.Id == id).FirstOrDefault();
                    db.WorkerTemplateMessages.Remove(temp);
                    db.SaveChanges();
                    isSuccess = true;
                }
            }
            return Json(new { success = isSuccess }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddTemplate(string title, string message)
        {
            bool isSuccess = false;
            bool isDuplicate = false;
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    if (db.WorkerTemplateMessages.FirstOrDefault(x => x.Title == title) == null)
                    {
                        WorkerTemplateMessage tm = new WorkerTemplateMessage();
                        tm.Title = title;
                        tm.Message = message;
                        tm.CompanyId = SessionHandler.CompanyId;
                        tm.CreatedDate = DateTime.Now;
                        db.WorkerTemplateMessages.Add(tm);
                        isSuccess = db.SaveChanges() > 0 ? true : false;
                    }
                    else
                    {
                        isDuplicate = true;
                    }
                }
            }
            return Json(new { success = isSuccess, is_duplicate = isDuplicate }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult EditTemplate(WorkerTemplateMessage model)
        {
            bool isDuplicate = false;
            bool isSuccess = false;
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.WorkerTemplateMessages.FirstOrDefault(x => x.Title == model.Title && x.Id != model.Id);
                    if (temp != null)
                    {
                        isDuplicate = true;
                    }
                    else
                    {
                        var tempm = db.WorkerTemplateMessages.Find(model.Id);
                        if (tempm != null)
                        {
                            tempm.Title = model.Title;
                            tempm.Message = model.Message;
                            db.Entry(tempm).State = EntityState.Modified;
                            isSuccess = db.SaveChanges() > 0 ? true : false;
                        }
                    }
                }
            }
            return Json(new { success = isSuccess, is_duplicate = isDuplicate }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult MessageRuleDetails(int id)
        {
            bool isSuccess = false;
            MessageRule rules = new MessageRule();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    rules = db.MessageRules.FirstOrDefault(i => i.Id == id);
                    if (rules != null) isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, messageRules = rules }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddMessageRules(int[] PropertyIds, int templateMessageId, string time, string title, string message, int ruleType)
        {
            bool result = false;
            try
            {
                foreach (var propertyId in PropertyIds)
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var property = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault();
                        if (property.IsParentProperty)
                        {
                            //var child = db.Properties.Where(x => x.ParentPropertyId == property.Id).ToList();
                            var child = db.ParentChildProperties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.ParentPropertyId == property.Id).Select(x=>x.ChildPropertyId).ToList();
                            foreach (var p in child)
                            {
                                if (templateMessageId == 0)
                                {
                                    WorkerTemplateMessage temp = new WorkerTemplateMessage();
                                    temp.Title = title;
                                    temp.Message = message;
                                    temp.CompanyId = SessionHandler.CompanyId;
                                    temp.CreatedDate = DateTime.Now;
                                    db.WorkerTemplateMessages.Add(temp);
                                    db.SaveChanges();

                                    WorkerMessageRule rule = new WorkerMessageRule();
                                    rule.PropertyId = p;
                                    rule.WorkerTemplateMessageId = temp.Id;
                                    rule.Type = ruleType;
                                    if (time != "" && ruleType == 1)
                                    {
                                        rule.Time = DateTime.ParseExact(time, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                                        result = true;
                                        db.WorkerMessageRules.Add(rule);
                                        db.SaveChanges();
                                    }
                                    else if (time == "" && ruleType != 1)
                                    {
                                        rule.Time = null;
                                        result = true;
                                        db.WorkerMessageRules.Add(rule);
                                        db.SaveChanges();
                                    }

                                    return Json(new { success = result, message = "Successfully Added Message Rule" }, JsonRequestBehavior.AllowGet);
                                }

                                else
                                {
                                    WorkerMessageRule rul = new WorkerMessageRule();
                                    rul.PropertyId = p;
                                    rul.WorkerTemplateMessageId = templateMessageId;
                                    rul.Type = ruleType;
                                    if (time != "" && ruleType == 1)
                                    {
                                        rul.Time = DateTime.ParseExact(time, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                                        result = true;
                                        db.WorkerMessageRules.Add(rul);
                                        db.SaveChanges();
                                    }
                                    else if (time == "" && ruleType != 1)
                                    {
                                        rul.Time = null;
                                        result = true;
                                        db.WorkerMessageRules.Add(rul);
                                        db.SaveChanges();
                                    }


                                }
                            }
                        }
                        else
                        {
                            if (templateMessageId == 0)
                            {
                                WorkerTemplateMessage temp = new WorkerTemplateMessage();
                                temp.Title = title;
                                temp.Message = message;
                                temp.CompanyId = SessionHandler.CompanyId;
                                temp.CreatedDate = DateTime.Now;
                                db.WorkerTemplateMessages.Add(temp);
                                db.SaveChanges();

                                WorkerMessageRule rule = new WorkerMessageRule();
                                rule.PropertyId = propertyId;
                                rule.WorkerTemplateMessageId = temp.Id;
                                rule.Type = ruleType;
                                if (time != "" && ruleType == 1)
                                {
                                    rule.Time = DateTime.ParseExact(time, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                                    result = true;
                                    db.WorkerMessageRules.Add(rule);
                                    db.SaveChanges();
                                }
                                else if (time == "" && ruleType != 1)
                                {
                                    rule.Time = null;
                                    result = true;
                                    db.WorkerMessageRules.Add(rule);
                                    db.SaveChanges();
                                }

                                return Json(new { success = result, message = "Successfully Added Message Rule" }, JsonRequestBehavior.AllowGet);
                            }

                            else
                            {
                                WorkerMessageRule rul = new WorkerMessageRule();
                                rul.PropertyId = propertyId;
                                rul.WorkerTemplateMessageId = templateMessageId;
                                rul.Type = ruleType;
                                if (time != "" && ruleType == 1)
                                {
                                    rul.Time = DateTime.ParseExact(time, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                                    result = true;
                                    db.WorkerMessageRules.Add(rul);
                                    db.SaveChanges();
                                }
                                else if (time == "" && ruleType != 1)
                                {
                                    rul.Time = null;
                                    result = true;
                                    db.WorkerMessageRules.Add(rul);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
                return Json(new { success = result, message = "Successfully Added Message Rule" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMessageRule(int id, string title, string message, int[] PropertyIds, int templateMessageId, string time, int ruleType)
        {
            using (var db = new ApplicationDbContext())
            {
                bool result = false;
                try
                {
                    foreach (var propertyId in PropertyIds)
                    {

                        var rules = db.WorkerMessageRules.Find(id);
                        if (templateMessageId == 0)
                        {
                            WorkerTemplateMessage temp = new WorkerTemplateMessage();
                            temp.Message = message;
                            temp.Title = title;
                            temp.CompanyId = SessionHandler.CompanyId;
                            temp.CreatedDate = DateTime.Now;
                            db.WorkerTemplateMessages.Add(temp);
                            db.SaveChanges();

                            rules.PropertyId = propertyId;
                            rules.WorkerTemplateMessageId = temp.Id;
                            rules.Time = TimeSpan.Parse(time);
                            rules.Type = ruleType;
                            db.Entry(rules).State = EntityState.Modified;
                            db.SaveChanges();
                            return Json(new { success = true, message = "Message Rule details has been updated" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            rules.PropertyId = propertyId;
                            rules.WorkerTemplateMessageId = templateMessageId;
                            rules.Type = ruleType;
                            if (time != "" && ruleType == 1)
                            {
                                rules.Time = DateTime.ParseExact(time, "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                                result = true;
                                db.WorkerMessageRules.Add(rules);
                                db.SaveChanges();
                            }
                            else if (time == "" && ruleType != 1)
                            {
                                rules.Time = null;
                                result = true;
                                db.WorkerMessageRules.Add(rules);
                                db.SaveChanges();
                            }
                            db.Entry(rules).State = EntityState.Modified;
                            db.SaveChanges();

                        }
                    }
                    return Json(new { success = result, message = "Message Rule details has been updated" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteMessageRule(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var b = db.WorkerMessageRules.Where(x => x.Id == id).FirstOrDefault();
                db.WorkerMessageRules.Remove(b);
                db.SaveChanges();
                return Json(new { success = true, message = "Message Rule has been deleted" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetPropertyList()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<PropertyListing> pl = new List<PropertyListing>();
                    //db.Properties
                    //    .Join(db.PropertyType, x => x.Type, y => y.Id, (x, y) => new { Property = x, Type = y })
                    //    .Join(db.Hosts,q=>q.Property, y => y.Id, (q, y) => new { Property = q, Host = y })
                    //    .Where(x => x.Property.CompanyId == SessionHandler.CompanyId)
                    //    .ToList()
                    (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select new { Property = p, Host = h }).ToList()
                    .ForEach(x =>
                    {
                        PropertyListing p = new PropertyListing();
                        var count = db.Inquiries.Where(z => z.IsActive & z.PropertyId == x.Property.ListingId).ToList().Count;
                        var parents = db.ParentChildProperties.Where(i => i.CompanyId == SessionHandler.CompanyId && i.ChildPropertyId == x.Property.Id).ToList();
                        foreach(var parent in parents)
                        {
                            var parentType = (Core.Enumerations.ParentPropertyType)parent.ParentType;
                            var tempp = db.Properties.Where(y => y.Id == parent.ParentPropertyId).FirstOrDefault();
                            switch (parentType)
                            {
                                case Core.Enumerations.ParentPropertyType.Sync:
                                    if (tempp != null)
                                        p.SyncParent = tempp.Name;
                                    break;
                                case Core.Enumerations.ParentPropertyType.MultiUnit:
                                    if (tempp != null)
                                        p.MultiUnitParent= tempp.Name;
                                    break;
                                case Core.Enumerations.ParentPropertyType.Accounts:
                                    if (tempp != null)
                                        p.AccountParent = tempp.Name;
                                    break;
                            }
                        }
                     
                        p.Id = x.Property.Id;
                        p.Name = x.Property.Name;
                        p.Site = ((Core.Enumerations.SiteType)x.Property.SiteType).ToString();
                        var Type = db.PropertyType.Where(y => y.Id == x.Property.Type).FirstOrDefault();
                        p.Type = Type !=null?Type.Property_Type:"-";
                        p.Actions = "<a class=\"ui mini button edit-property-btn\" title=\"Edit\">" +
                                        "<i class=\"icon edit\"></i>" +
                                    "</a>" +
                                    ((x.Property.ListingId == 0) ? "<a class=\"ui mini button delete-property-btn\" title=\"Delete\" >" +
                                        "<i class=\"icon ban\"></i>" +
                                        "</a>" : "");
                        p.ActiveBooking = count;
                        p.IsActive = ("<td class=\"collapsing\">" +
                                                        "<div class=\"ui toggle checkbox\">" +
                                                            "<input type=\"checkbox\" class=\"chk-active\" onchange='UpdateIsActive(this)'" + (x.Property.IsActive ? "checked" : "") + "><label></label>" +
                                                        "</div>" +
                                                     "</td>");
                        p.Host = x.Host.Firstname + "" + x.Host.Lastname + " (" + x.Host.Username + ")";
                        pl.Add(p);
                    });

                    (from p in db.Properties where p.SiteType == 0 && p.CompanyId == SessionHandler.CompanyId select new { Property = p}).ToList()
                  .ForEach(x =>
                  {
                      var count = db.Inquiries.Where(z => z.IsActive & z.PropertyId == x.Property.ListingId).ToList().Count;
                      
                      //var parentChild = db.ParentChildProperties.Where(i => i.CompanyId == SessionHandler.CompanyId && i.ChildPropertyId == x.Property.Id).FirstOrDefault();
                      //if (parentChild != null)
                      //{
                      //    parent = db.Properties.Where(z => z.Id == parentChild.ParentPropertyId).FirstOrDefault();
                      //}
                      PropertyListing p = new PropertyListing();
                      p.Id = x.Property.Id;
                      var parents = db.ParentChildProperties.Where(i => i.CompanyId == SessionHandler.CompanyId && i.ChildPropertyId == x.Property.Id).ToList();
                      foreach (var parent in parents)
                      {
                          var parentType = (Core.Enumerations.ParentPropertyType)parent.ParentType;
                          var tempp = db.Properties.Where(y => y.Id == parent.ParentPropertyId).FirstOrDefault();
                          switch (parentType)
                          {
                              case Core.Enumerations.ParentPropertyType.Sync:
                                  if (tempp != null)
                                      p.SyncParent = tempp.Name;
                                  break;
                              case Core.Enumerations.ParentPropertyType.MultiUnit:
                                  if (tempp != null)
                                      p.MultiUnitParent = tempp.Name;
                                  break;
                              case Core.Enumerations.ParentPropertyType.Accounts:
                                  if (tempp != null)
                                      p.AccountParent = tempp.Name;
                                  break;
                          }
                      }
                      //if (parent != null)
                      //{
                      //    p.Parent = parent.Name;
                      //}
                      p.Name = x.Property.Name;
                      p.Site = x.Property.IsParentProperty ? ((ParentPropertyType)x.Property.ParentType == ParentPropertyType.Sync?"Sync": (ParentPropertyType)x.Property.ParentType == ParentPropertyType.MultiUnit?"Multi-Unit":"Account") 
                      +" Parent" : ((Core.Enumerations.SiteType)x.Property.SiteType).ToString();
                      var Type = db.PropertyType.Where(y => y.Id == x.Property.Type).FirstOrDefault();
                      p.Type = Type != null ? Type.Property_Type : "-";
                      p.Actions = "<a class=\"ui mini button edit-property-btn\" title=\"Edit\">" +
                                      "<i class=\"icon edit\"></i>" +
                                  "</a>" +
                                  ((x.Property.IsParentProperty) ? "<a class=\"ui mini button delete-property-btn\" title=\"Delete\" >" +
                                      "<i class=\"icon ban\"></i>" +
                                      "</a>" : "");
                      p.ActiveBooking = count;
                      p.IsActive = ("<td class=\"collapsing\">" +
                                                      "<div class=\"ui toggle checkbox\">" +
                                                          "<input type=\"checkbox\" class=\"chk-active\" onchange='UpdateIsActive(this)'" + (x.Property.IsActive ? "checked" : "") + "><label></label>" +
                                                      "</div>" +
                                                   "</td>");
                      pl.Add(p);
                  });

                    return Json(new { data = pl }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e) { return Json(new { data = new List<PropertyListing>() }, JsonRequestBehavior.AllowGet); }
        }

        [HttpPost]
        [CheckSessionTimeout]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FormCollection f)
        {
            int companyId = SessionHandler.CompanyId;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    if (companyId != 0)
                    {
                        if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    var utc = DateTime.UtcNow;
                    System.Random r = new System.Random();
                    Property p = new Property();
                    p.HostId = f["HostId"].ToInt();
                    p.ListingId = r.Next(100000000).ToLong();//(utc.Year.ToString()[2] + "" + utc.Year.ToString()[3] + "" + utc.Month + "" + utc.Day + "" + utc.Hour + "" + utc.Second).ToLong();
                    p.Name = f["PropertyName"].ToString();
                    p.Type = f["PropertyType"].ToInt();
                    p.UnitCount = 0;
                    p.IcalUrl = f["ical-url"].ToSafeString();
                    p.Description = f["Description"].ToSafeString();
                    //p.Address = f["Address1"].ToSafeString() + " " + f["Address2"].ToSafeString() + " " + f["Address3"].ToSafeString();
                    decimal monthlyExpense = 0;
                    decimal.TryParse(f["MonthlyExpense"].ToSafeString(), out monthlyExpense);
                    p.MonthlyExpense = monthlyExpense;
                    p.ExpenseNotes = f["MonthlyExpenseNotes"].ToSafeString();
                    //p.ActiveDateStart = DateTime.ParseExact(f["StartDate"], "MMMM d, yyyy", new CultureInfo("en-US"));
                    //p.ActiveDateEnd = DateTime.ParseExact(f["EndDate"], "MMMM d, yyyy", new CultureInfo("en-US"));
                    if (f["start-date"].ToSafeString() != "")
                    {
                        p.ActiveDateStart = f["start-date"].ToDateTime();
                    }
                    p.CommissionId = f["commission-property"].ToSafeString().ToInt();
                    p.CommissionAmount = f["commission-amount"].ToSafeString().ToDecimal();
                    p.AutoCreateIncomeExpense = f["autoCreate"] == null ? false : true;
                    p.AllowedSelfCheckinCheckout = f["selfCheckinCheckout"] == null ? false : true;
                    //p.ActiveDateStart = f["start-date"].ToDateTime();
                    //p.ActiveDateEnd = f["endDate"].ToDateTime();
                    p.IsActive = f["status"] == null ? false : true;
                    p.CheckInTime = new TimeSpan();
                    p.CheckOutTime = new TimeSpan();
                    p.IsLocallyCreated = true;
                    p.IsParentProperty = f["IsParentProperty"].ToSafeString() == "" ? false : f["IsParentProperty"].ToSafeString() == "on" ? true : false;
                    p.CompanyId = companyId;
                    p.Longitude = f["longitude"].ToSafeString();
                    p.Latitude = f["latitude"].ToSafeString();
                    p.ParentType = f["parent-type"].ToSafeString().ToInt();
                    p.StreetNumber = f["street-number"].ToSafeString();
                    p.UnitNumber = f["unit-number"].ToSafeString();
                    p.Street = f["street"].ToSafeString();
                    p.Building = f["building"].ToSafeString();
                    p.CountryId = f["countries"].ToSafeString().ToInt();
                    p.StateId = f["states"].ToSafeString().ToInt();
                    p.CityId = f["cities"].ToSafeString().ToInt();
                    p.Address = p.UnitNumber + " " + p.Building+ " " + p.StreetNumber +" "+ p.Street + " " + Address.City(p.CityId)+ " " + Address.State(p.StateId) + " " + Address.Country(p.CountryId);
                    using (var sm = new Multimedia.ScrapeManager())
                    {
                        var geoData = sm.GeLongitudeLatitude(p.Address);
                        p.Longitude = geoData.Item1;
                        p.Latitude = geoData.Item2;

                        if (p.Longitude != "" && p.Latitude != "")
                        {
                            var timezone = sm.GetTimeZone(p.Longitude, p.Latitude);
                            p.TimeZoneName = timezone.Item1;
                            p.TimeZoneId = timezone.Item2;
                        }
                    }
                    db.Properties.Add(p);
                    db.SaveChanges();

                

                    List<string> Fee_Types = f["vatdrp"] == null ? new List<string>() : f["vatdrp"].Split(',').ToList<string>();
                    List<string> ComputeBasis = f["feetype"] == null ? new List<string>() : f["feetype"].Split(',').ToList<string>();
                    List<string> inclusiveexcluves = f["inclusiveexcluve"] == null ? new List<string>() : f["inclusiveexcluve"].Split(',').ToList<string>();
                    List<string> Shows = f["checkboxName"] == null ? new List<string>() : f["checkboxName"].Split(',').ToList<string>();
                    List<string> shows2 = f["checkboxName2"] == null ? new List<string>() : f["checkboxName2"].Split(',').ToList<string>();
                    List<string> AmountPercents = f["AmountPercents"] == null ? new List<string>() : f["AmountPercents"].Split(',').ToList<string>();
                    List<string> vatamounts = f["vatamount"] == null ? new List<string>() : f["vatamount"].Split(',').ToList<string>();

                    if (p.IsParentProperty && p.IsLocallyCreated)
                    {
                        List<string> ChildProperties = f["ChildProperties"] == null ? new List<string>() : f["ChildProperties"].Split(',').ToList<string>();

                        foreach (var child in ChildProperties)
                        {
                            if (!string.IsNullOrEmpty(child))
                            {
                                int id = int.Parse(child);
                                Core.API.Local.Properties.AddChildToParent(
                                new ParentChildProperty()
                                {
                                    ChildPropertyId = id,
                                    ParentPropertyId = p.Id,
                                    ParentType = p.ParentType,
                                    CompanyId = SessionHandler.CompanyId
                                });
                            }
                        }
                        db.SaveChanges();
                        if (p.IsParentProperty && p.ParentType == (int)ParentPropertyType.Sync)
                        {
                            var tempproperty = (from prop in db.Properties join pc in db.ParentChildProperties on prop.Id equals pc.ChildPropertyId where pc.ParentPropertyId == p.Id && prop.CompanyId == SessionHandler.CompanyId && prop.SiteType == 0 select prop).FirstOrDefault();
                            // db.Properties.Where(x => x.ParentPropertyId == p.Id && x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                            if (tempproperty == null)
                            {
                                var localChild = new Property()
                                {

                                    ListingId = r.Next(100000000).ToLong(),
                                    ListingUrl = "",
                                    Name = p.Name,
                                    Longitude = p.Longitude,
                                    Latitude = p.Latitude,
                                    Address = p.Address,
                                    UnitCount = p.UnitCount,
                                    MinStay = p.MinStay,
                                    Internet = p.Internet,
                                    Pets = p.Pets,
                                    WheelChair = p.WheelChair,
                                    Description = p.Description,
                                    Reviews = p.Reviews,
                                    Sleeps = p.Sleeps,
                                    Bedrooms = p.Bedrooms,
                                    Bathrooms = p.Bathrooms,
                                    PropertyType = p.PropertyType,
                                    Type = p.Type,
                                    AccomodationType = p.AccomodationType,
                                    Smoking = p.Smoking,
                                    AirCondition = p.AirCondition,
                                    SwimmingPool = p.SwimmingPool,
                                    PriceNightlyMax = p.PriceNightlyMax,
                                    PriceWeeklyMin = p.PriceWeeklyMin,
                                    PriceWeeklyMax = p.PriceWeeklyMax,
                                    Images = p.Images,
                                    Status = p.Status,
                                    MonthlyExpense = p.MonthlyExpense,
                                    ExpenseNotes = p.ExpenseNotes,
                                    ActiveDateStart = p.ActiveDateStart,
                                    ActiveDateEnd = p.ActiveDateEnd,
                                    CommissionId = p.CommissionId,
                                    CommissionAmount = p.CommissionAmount,
                                    AutoCreateIncomeExpense = p.AutoCreateIncomeExpense,
                                    CheckInTime = p.CheckInTime,
                                    CheckOutTime = p.CheckOutTime,
                                    IsLocallyCreated = p.IsLocallyCreated,
                                    IsParentProperty = false,
                                    IsSyncParent = false,
                                    SiteType = 0,
                                    CompanyId = SessionHandler.CompanyId,
                                    CalendarRemark = p.CalendarRemark,
                                    Currency = p.Currency,
                                    TimeZoneName = p.TimeZoneName,
                                    TimeZoneId = p.TimeZoneId,
                                    IsActive = true,
                                    TwilioNumberId = null,
                                    AllowedSelfCheckinCheckout = false,
                                    PriceRuleSync = false
                                };

                                db.Properties.Add(localChild);
                                db.SaveChanges();
                                Core.API.Local.Properties.AddChildToParent(
                                        new ParentChildProperty()
                                        {
                                            ChildPropertyId = localChild.Id,
                                            ParentPropertyId = p.Id,
                                            ParentType = p.ParentType,
                                            CompanyId = SessionHandler.CompanyId
                                        });
                            }
                        }
                        //JM 11/7/19 Questionable function
                        //var lastChildId = ChildProperties.Last().ToInt();
                        //var lastChildProperty = db.Properties.Where(x => x.Id == lastChildId).FirstOrDefault();

                        //p.TimeZoneId = lastChildProperty.TimeZoneId;
                        //db.Entry(p).State = EntityState.Modified;
                        //db.SaveChanges();
                    }

                    int counter = 0;
                    foreach (string c in Fee_Types)
                    {
                        try
                        {
                            PropertyFee ft = new PropertyFee
                            {
                                PropertyId = p.Id,
                                ComputeBasis = Convert.ToInt32(ComputeBasis[counter]),
                                FeeTypeId = Convert.ToInt32(c),
                                Show = Shows[counter].Equals("1") ? true : false,
                                IsPreDeducted = shows2[counter].Equals("1") ? true : false,
                                Method = AmountPercents[counter].ToInt(),
                                AmountPercent = vatamounts[counter].ToDecimal(),
                                InclusiveOrExclusive = Convert.ToInt32(inclusiveexcluves[counter])
                            };
                            db.PropertyFees.Add(ft);
                            db.SaveChanges();
                            counter++;
                        }
                        catch { }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection f)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var entity = db.Properties.Where(x => x.Id == id).FirstOrDefault(); //(from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId
                    //                    where hc.CompanyId == SessionHandler.CompanyId select p).FirstOrDefault();
                    if (entity == null)
                    {
                        return Redirect("/");
                    }

                    entity.Name = f["PropertyName"].ToString();

                    entity.Type = f["PropertyType"].ToInt();
                    entity.UnitCount = 0;
                    entity.Description = f["Description"].ToString();
                    //entity.Address = f["Address1"].ToString() + " " + f["Address2"].ToString() + " " + f["Address3"].ToString();
                    entity.IcalUrl = f["ical-url"].ToSafeString().ToString();
                    //entity.MonthlyExpense = decimal.Parse(f["MonthlyExpense"].ToString());
                    decimal monthlyExpense = 0;
                    decimal.TryParse(f["MonthlyExpense"].ToSafeString(), out monthlyExpense);
                    entity.MonthlyExpense = monthlyExpense;
                    entity.ExpenseNotes = f["MonthlyExpenseNotes"].ToSafeString();
                    //entity.ActiveDateStart = DateTime.ParseExact(f["StartDate"], "MMMM d, yyyy", new CultureInfo("en-US"));
                    //entity.ActiveDateEnd = DateTime.ParseExact(f["EndDate"], "MMMM d, yyyy", new CultureInfo("en-US"));
                   
                    if (f["start-date"].ToSafeString() != "")
                    {
                        entity.ActiveDateStart = f["start-date"].ToDateTime();
                    }
                    //entity.ActiveDateEnd = f["end-date"].ToDateTime();
                    entity.IsActive = f["status"] == null ? false : true;
                    entity.CommissionId = f["commission-property"].ToInt();
                    entity.CommissionAmount = f["commission-amount"].ToInt();
                    entity.AutoCreateIncomeExpense = f["autoCreate"] == null ? false : true;
                    entity.AllowedSelfCheckinCheckout = f["selfCheckinCheckout"] == null ? false : true;
                    entity.CheckInTime = new TimeSpan();
                    entity.CheckOutTime = new TimeSpan();
                    entity.IsParentProperty = f["IsParentProperty"].ToSafeString() == "" ? false : f["IsParentProperty"].ToSafeString() == "on" ? true : false;
                    entity.Longitude = f["longitude"].ToSafeString();
                    entity.Latitude = f["latitude"].ToSafeString();
                    entity.ParentType = f["parent-type"].ToSafeString().ToInt();
                    entity.StreetNumber = f["street-number"].ToSafeString();
                    entity.UnitNumber = f["unit-number"].ToSafeString();
                    entity.Street = f["street"].ToSafeString();
                    entity.Building = f["building"].ToSafeString();
                    entity.CountryId = f["countries"].ToSafeString().ToInt();
                    entity.StateId = f["states"].ToSafeString().ToInt();
                    entity.CityId = f["cities"].ToSafeString().ToInt();
                    entity.Address = entity.UnitNumber + " " + entity.Building + " " + entity.StreetNumber + " " + entity.Street + " " + Address.City(entity.CityId) + " " + Address.State(entity.StateId) + " " + Address.Country(entity.CountryId);

                    using (var sm = new Multimedia.ScrapeManager())
                    {
                        var geoData = sm.GeLongitudeLatitude(entity.Address);
                        entity.Longitude = geoData.Item1;
                        entity.Latitude = geoData.Item2;
                        var timezone = sm.GetTimeZone(entity.Longitude, entity.Latitude);
                        entity.TimeZoneName = timezone.Item1;
                        entity.TimeZoneId = timezone.Item2;
                    }
                    
                    // Add Template Ids into PropertyTemplate table
                    List<string> propertyTemplates = f["task-template-property"] == null ? new List<string>() : f["task-template-property"].Split(',').ToList<string>();
                    List<int> propertyTemplateList = propertyTemplates.Select(item => Convert.ToInt32(item)).ToList();

                    var currentPropertyTemplates = db.PropertyTemplates.Where(x => x.PropertyId == id);
                    foreach (var item in currentPropertyTemplates)
                    {
                        db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    }

                    entity.PriceRuleSync = f["priceRuleSync"].ToSafeString() == "" ? false : f["priceRuleSync"].ToSafeString() == "on" ? true : false;

                    db.SaveChanges();

                    try
                    {
                        var priceRuleSettings = db.PriceRuleSettings.Where(x => x.ListingId == entity.ListingId).ToList();
                        db.PriceRuleSettings.RemoveRange(priceRuleSettings);

                        try
                        {
                            var priceRuleBasis = f["priceRuleBasis"].ToString().Split(',').Select(x => x.ToInt()).ToArray();
                            var priceRuleCondition = f["priceRuleCondition"].ToString().Split(',').Select(x => x.ToInt()).ToArray();
                            var priceRuleConditionPercentage = f["priceRuleConditionPercentage"].ToString().Split(',').Select(x => x.ToDouble()).ToArray();
                            var priceRuleAmountPlusMinus = f["priceRuleAmountPlusMinus"].ToString().Split(',').Select(x => x.ToInt()).ToArray();
                            var priceRuleAmount = f["priceRuleAmount"].ToString().Split(',').Select(x => x.ToDouble()).ToArray();
                            var priceRuleSign = f["priceRuleSign"].ToString().Split(',').Select(x => x.ToInt()).ToArray();
                            var priceRuleAveragePriceBasis = f["priceRuleAveragePriceBasis"].ToString().Split(',').Select(x => x.ToInt()).ToArray();

                            for (int i = 0; i < priceRuleBasis.Length; i++)
                            {
                                db.PriceRuleSettings.Add(new PriceRuleSettings
                                {
                                    ListingId = entity.ListingId,
                                    PriceRuleBasis = priceRuleBasis[i],
                                    PriceRuleCondition = priceRuleCondition[i],
                                    PriceRuleConditionPercentage = priceRuleConditionPercentage[i],
                                    PriceRuleAmount = priceRuleAmountPlusMinus[i] == 0 ? priceRuleAmount[i] : (priceRuleAmount[i] * -1),
                                    PriceRuleSign = priceRuleSign[i],
                                    PriceRuleAveragePriceBasis = priceRuleAveragePriceBasis[i],
                                    CompanyId = GlobalVariables.CompanyId
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                    }

                    //var childProperties = db.Properties.Where(x => x.ParentPropertyId == id).ToList();
                    var childPropertyIds = db.ParentChildProperties.Where(x => x.ParentPropertyId == id).Select(x=>x.ChildPropertyId).ToList();
                    foreach (var childPropertyId in childPropertyIds)
                    {
                        var childProperty = db.Properties.Where(x => x.Id == childPropertyId).FirstOrDefault();
                        childProperty.IsSyncParent = false;
                    }

                    try
                    {
                        var syncParentPropertyId = f["syncParentPropertyId"].ToSafeString().ToInt();
                        var syncParent = db.Properties.Where(x => x.Id == syncParentPropertyId).FirstOrDefault();

                        syncParent.IsSyncParent = true;
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                    }

                    try
                    {
                        //var lastChildId = childPropertyIds.Last();
                        //var lastChildProperty = db.Properties.Where(x => x.Id == lastChildId).FirstOrDefault();

                        //entity.TimeZoneId = lastChildProperty.TimeZoneId;
                        //db.Entry(entity).State = EntityState.Modified;
                        //db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                    }

                    foreach (var item in propertyTemplateList)
                    {
                        PropertyTemplate template = new PropertyTemplate
                        {
                            PropertyId = id,
                            TemplateId = item
                        };

                        db.PropertyTemplates.Add(template);
                    }
                    db.SaveChanges();

                    var syncSettings = db.PropertySyncSettings.Where(x => x.PropertyLocalId == id).FirstOrDefault();

                    if (syncSettings == null)
                    {
                        db.PropertySyncSettings.Add(new PropertySyncSettings
                        {
                            PropertyLocalId = id,
                            AirbnbAdjustmentValue = f["airbnbAdjustmentValue"].ToDouble(),
                            AirbnbAdjustmentType = f["airbnbAdjustmentType"].ToInt(),
                            AirbnbSymbolType = f["airbnbSymbolType"].ToInt(),
                            VrboAdjustmentValue = f["vrboAdjustmentValue"].ToDouble(),
                            VrboAdjustmentType = f["vrboAdjustmentType"].ToInt(),
                            VrboSymbolType = f["vrboSymbolType"].ToInt(),
                            BookingComAdjustmentValue = f["bookingComAdjustmentValue"].ToDouble(),
                            BookingComAdjustmentType = f["bookingComAdjustmentType"].ToInt(),
                            BookingComSymbolType = f["bookingComSymbolType"].ToInt(),
                            SyncPrice = f["syncPrice"].ToBoolean(),
                            SyncAvailability = f["syncAvailability"].ToBoolean()
                        });
                    }
                    else
                    {
                        syncSettings.AirbnbAdjustmentValue = f["airbnbAdjustmentValue"].ToDouble();
                        syncSettings.AirbnbAdjustmentType = f["airbnbAdjustmentType"].ToInt();
                        syncSettings.AirbnbSymbolType = f["airbnbSymbolType"].ToInt();
                        syncSettings.VrboAdjustmentValue = f["vrboAdjustmentValue"].ToDouble();
                        syncSettings.VrboAdjustmentType = f["vrboAdjustmentType"].ToInt();
                        syncSettings.VrboSymbolType = f["vrboSymbolType"].ToInt();
                        syncSettings.BookingComAdjustmentValue = f["bookingComAdjustmentValue"].ToDouble();
                        syncSettings.BookingComAdjustmentType = f["bookingComAdjustmentType"].ToInt();
                        syncSettings.BookingComSymbolType = f["bookingComSymbolType"].ToInt();
                        syncSettings.SyncPrice = f["syncPrice"].ToBoolean();
                        syncSettings.SyncAvailability = f["syncAvailability"].ToBoolean();
                    }

                    db.SaveChanges();

                    List<string> Fee_Types = f["vatdrp"] == null ? new List<string>() : f["vatdrp"].Split(',').ToList<string>();
                    List<string> ComputeBasis = f["feetype"] == null ? new List<string>() : f["feetype"].Split(',').ToList<string>();
                    List<string> inclusiveexcluves = f["inclusiveexcluve"] == null ? new List<string>() : f["inclusiveexcluve"].Split(',').ToList<string>();
                    List<string> Shows = f["checkboxName"] == null ? new List<string>() : f["checkboxName"].Split(',').ToList<string>();
                    List<string> shows2 = f["checkboxName2"] == null ? new List<string>() : f["checkboxName2"].Split(',').ToList<string>();
                    List<string> AmountPercents = f["AmountPercents"] == null ? new List<string>() : f["AmountPercents"].Split(',').ToList<string>();
                    List<string> vatamounts = f["vatamount"] == null ? new List<string>() : f["vatamount"].Split(',').ToList<string>();

                    if (entity.IsParentProperty)
                    {
                        var childProperties = db.ParentChildProperties.Where(x => x.ParentPropertyId == entity.Id && x.CompanyId == SessionHandler.CompanyId).ToList(); //db.Properties.Where(x => x.ParentPropertyId == entity.Id && x.SiteType != 0).ToList();
                        db.ParentChildProperties.RemoveRange(childProperties);
                        db.SaveChanges();
                        List<string> ChildProperties = f["ChildProperties"] == null ? new List<string>() : f["ChildProperties"].Split(',').ToList<string>();

                        foreach (var child in ChildProperties)
                        {
                            if (!string.IsNullOrEmpty(child))
                            {
                                int childId = int.Parse(child);
                                Core.API.Local.Properties.AddChildToParent(
                                    new ParentChildProperty()
                                    {
                                        ChildPropertyId =childId,
                                        ParentPropertyId = entity.Id,
                                        ParentType = entity.ParentType,
                                        CompanyId = SessionHandler.CompanyId
                                    });
                            }
                        }
                        db.SaveChanges();
                        if (entity.IsParentProperty && entity.ParentType == (int)ParentPropertyType.Sync)
                        {
                            var tempproperty = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == entity.Id && p.CompanyId == SessionHandler.CompanyId &&p.SiteType==0  select p).FirstOrDefault();
                            // db.Properties.Where(x => x.ParentPropertyId == entity.Id && x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                            if (tempproperty == null)
                            {
                                System.Random r = new System.Random();
                                var localChild = new Property()
                                {

                                    ListingId = r.Next(100000000).ToLong(),
                                    ListingUrl = "",
                                    Name = entity.Name,
                                    Longitude = entity.Longitude,
                                    Latitude = entity.Latitude,
                                    Address = entity.Address,
                                    UnitCount = entity.UnitCount,
                                    MinStay = entity.MinStay,
                                    Internet = entity.Internet,
                                    Pets = entity.Pets,
                                    WheelChair = entity.WheelChair,
                                    Description = entity.Description,
                                    Reviews = entity.Reviews,
                                    Sleeps = entity.Sleeps,
                                    Bedrooms = entity.Bedrooms,
                                    Bathrooms = entity.Bathrooms,
                                    PropertyType = entity.PropertyType,
                                    Type = entity.Type,
                                    AccomodationType = entity.AccomodationType,
                                    Smoking = entity.Smoking,
                                    AirCondition = entity.AirCondition,
                                    SwimmingPool = entity.SwimmingPool,
                                    PriceNightlyMax = entity.PriceNightlyMax,
                                    PriceWeeklyMin = entity.PriceWeeklyMin,
                                    PriceWeeklyMax = entity.PriceWeeklyMax,
                                    Images = entity.Images,
                                    Status = entity.Status,
                                    MonthlyExpense = entity.MonthlyExpense,
                                    ExpenseNotes = entity.ExpenseNotes,
                                    ActiveDateStart = entity.ActiveDateStart,
                                    ActiveDateEnd = entity.ActiveDateEnd,
                                    CommissionId = entity.CommissionId,
                                    CommissionAmount = entity.CommissionAmount,
                                    AutoCreateIncomeExpense = entity.AutoCreateIncomeExpense,
                                    CheckInTime = entity.CheckInTime,
                                    CheckOutTime = entity.CheckOutTime,
                                    IsLocallyCreated = entity.IsLocallyCreated,
                                    IsParentProperty = false,
                                    IsSyncParent = false,
                                    SiteType = 0,
                                    CompanyId = SessionHandler.CompanyId,
                                    CalendarRemark = entity.CalendarRemark,
                                    Currency = entity.Currency,
                                    TimeZoneName = entity.TimeZoneName,
                                    TimeZoneId = entity.TimeZoneId,
                                    IsActive = true,
                                    TwilioNumberId = null,
                                    AllowedSelfCheckinCheckout = false,
                                    PriceRuleSync = false
                                };

                                db.Properties.Add(localChild);
                                db.SaveChanges();
                                Core.API.Local.Properties.AddChildToParent(
                                        new ParentChildProperty()
                                        {
                                            ChildPropertyId = localChild.Id,
                                            ParentPropertyId = entity.Id,
                                            ParentType = entity.ParentType,
                                            CompanyId = SessionHandler.CompanyId
                                        });
                            }
                        }
                    }

                    db.PropertyFees.RemoveRange(db.PropertyFees.Where(x => x.PropertyId == id).ToList());

                    int couner = 0;
                    foreach (string c in ComputeBasis)
                    {
                        PropertyFee ft = new PropertyFee
                        {
                            PropertyId = entity.Id,
                            ComputeBasis = Convert.ToInt32(ComputeBasis[couner]),
                            FeeTypeId = Convert.ToInt32(Fee_Types[couner]),
                            Show = Shows[couner].Equals("1") ? true : false,
                            IsPreDeducted = shows2[couner].Equals("1") ? true : false,
                            Method = AmountPercents[couner].ToInt(),
                            AmountPercent = vatamounts[couner].ToDecimal(),
                            InclusiveOrExclusive = Convert.ToInt32(inclusiveexcluves[couner])
                        };
                        db.PropertyFees.Add(ft);
                        db.SaveChanges();
                        couner++;
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.PropertyFees.RemoveRange(db.PropertyFees.Where(x => x.PropertyId == id).ToList());
                    db.Properties.RemoveRange(db.Properties.Where(x => x.Id == id).ToList());
                    var childProperties = db.ParentChildProperties.Where(x => x.ParentPropertyId == id && x.CompanyId== SessionHandler.CompanyId).ToList();
                    db.ParentChildProperties.RemoveRange(childProperties);
                    db.SaveChanges();
                    return Json(new { success =true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult GetDetails(int Id)
        {
            ICommonRepository objIList = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
            }
            using (var db = new ApplicationDbContext())
            {
                var property = db.Properties.Where(x => x.Id == Id).FirstOrDefault();
                var ilist = property.SiteType == 0 ? (from p in db.Properties where p.CompanyId == SessionHandler.CompanyId && p.Id == Id select p).ToList() : (from h in db.Hosts.DefaultIfEmpty()
                                                                                                                                                                join hc in db.HostCompanies.DefaultIfEmpty() on h.Id equals hc.HostId
                                                                                                                                                                join p in db.Properties on h.Id equals p.HostId
                                                                                                                                                                where hc.CompanyId == SessionHandler.CompanyId && p.Id == Id
                                                                                                                                                                select p).ToList();
                List<ChildPropertyModel> availableLocal = new List<ChildPropertyModel>();
                List<ChildPropertyModel> childLocal = new List<ChildPropertyModel>();
                List<ChildPropertyModel> AvailableChildProperties = new List<ChildPropertyModel>();
                var ChildIds = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where !p.IsParentProperty && hc.CompanyId == SessionHandler.CompanyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();
                foreach (var child in ChildIds)
                {
                    var tempchild = db.ParentChildProperties.Where(x =>x.ChildPropertyId ==child.Id && x.ParentType ==property.ParentType ).FirstOrDefault();
                    if (tempchild == null)
                    {
                        AvailableChildProperties.Add(new ChildPropertyModel()
                        {
                            Host = child.Host,
                            IsSyncProperty = child.IsSyncParent,
                            SiteType = child.SiteType,
                            Id = child.Id,
                            Name =  child.Name
                        });
                    }
                }

                var parentType = (Core.Enumerations.ParentPropertyType)property.ParentType;
                var localchildProperty = db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId && x.IsParentProperty == false).ToList();
                foreach (var p in localchildProperty)
                {
                    var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == property.ParentType).FirstOrDefault();
                    if (temp == null)
                    {
                        availableLocal.Add(new ChildPropertyModel()
                        {
                            Host = "",
                            IsSyncProperty = p.IsSyncParent,
                            SiteType = p.SiteType,
                            Id = p.Id,
                            Name = p.Name
                        });
                    }
                }
                if (parentType != Core.Enumerations.ParentPropertyType.Sync)
                {
                    var localParentProperty= db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId && x.IsParentProperty == true && (parentType == Core.Enumerations.ParentPropertyType.MultiUnit ? x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync : (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts))).ToList();
                    foreach (var p in localParentProperty)
                    {
                        var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == property.ParentType).FirstOrDefault();
                        if (temp == null)
                        {
                            availableLocal.Add(new ChildPropertyModel()
                            {
                                Host = "",
                                IsSyncProperty = p.IsSyncParent,
                                SiteType = p.SiteType,
                                Id = p.Id,
                                Name = p.Name
                            });
                        }
                    }
                }

                var localChildIds = db.ParentChildProperties.Where(x => x.ParentPropertyId == property.Id && x.CompanyId==SessionHandler.CompanyId).Select(x=>x.ChildPropertyId).ToList();
                foreach(var id in localChildIds)
                {
                    var tempproperty = db.Properties.Where(x => x.Id == id && x.SiteType==0).FirstOrDefault();
                    if (tempproperty != null)
                    {
                        childLocal.Add(new ChildPropertyModel()
                        {
                            Host = "",
                            IsSyncProperty = tempproperty.IsSyncParent,
                            SiteType = tempproperty.SiteType,
                            Id = tempproperty.Id,
                            Name = tempproperty.Name
                        });
                    }
                }
                var inqlist = (from i in ilist
                               select new
                               {
                                   ID = i.Id,
                                   Building = i.Building,
                                   Street=i.Street,
                                   UnitNumber=i.UnitNumber,
                                   StreetNumber =i.StreetNumber,
                                   HostId = i.HostId,
                                   Longitude = i.Longitude,
                                   Latitude = i.Latitude,
                                   PropertyName = i.Name,
                                   PropertyType = i.Type,
                                   UnitCount = i.UnitCount,
                                   Description = i.Description,
                                   Address = i.Address,
                                   ActiveDateStart = i.ActiveDateStart,
                                   ActiveDateEnd = i.ActiveDateEnd,
                                   MonthlyExpence = i.MonthlyExpense,
                                   MonthlyExpenceNote = i.ExpenseNotes,
                                   commission = i.CommissionId,
                                   CommissionAmount = i.CommissionAmount,
                                   AutoCreate = i.AutoCreateIncomeExpense,
                                   IsLocallyCreated = i.IsLocallyCreated,
                                   IsParentProperty = i.IsParentProperty,
                                   StartDate = i.ActiveDateStart,
                                   EndDate = i.ActiveDateEnd,
                                   ParentType =i.ParentType,
                                   AllowedSelfCheckinCheckout = i.AllowedSelfCheckinCheckout,
                                   IsActive = i.IsActive,
                                   PriceRuleSync = i.PriceRuleSync,
                                   IcalUrl=i.IcalUrl,
                                   PriceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(i.ListingId),
                                   //Accounts = Core.API.AllSite.Properties.Accounts(i.ListingId),
                                   ChildProperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId  where pc.ParentPropertyId == Id && hc.CompanyId == SessionHandler.CompanyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList(),
                                   AvailableChildProperties = AvailableChildProperties,// (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where !p.IsParentProperty && p.ParentPropertyId == 0 && hc.CompanyId == SessionHandler.CompanyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList(),
                                   AvailableLocalChild =availableLocal,
                                   ChildLocal =childLocal,
                                   PropertySyncSettings = Core.API.AllSite.PropertySyncSettings.GetByLocalPropertyId(Id),
                                   Templates = db.PropertyTemplates.Where(x => x.PropertyId == Id).Select(x => new { TemplateId = x.TemplateId }).ToList(),
                                   FeeAndTaxlist = db.PropertyFees.Where(p => p.PropertyId == i.Id).Select(fs => new
                                   {
                                       Id = fs.Id,
                                       Fee_Type = fs.FeeTypeId,
                                       ComputeBasis = fs.ComputeBasis,
                                       Show = fs.Show ? "1" : "0",
                                       Show2 = fs.IsPreDeducted == true ? "1" : "0",
                                       showbool = fs.Show,
                                       showbool2 = fs.IsPreDeducted,
                                       Method = fs.Method,
                                       AmountPercent = fs.AmountPercent,
                                       InclusiveOrExclesive = fs.InclusiveOrExclusive,

                                   }).ToList()

                               }).ToList();
                return Json(inqlist, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetAvailableForChildProperty(int type,int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                List<ChildPropertyModel> childproperties = new List<ChildPropertyModel>();
                var parentChildProperties = db.ParentChildProperties.Where(x => x.ParentType == type && x.ParentPropertyId == propertyId).ToList();
               foreach(var p in parentChildProperties)
                {
                    var tempProperty = db.Properties.Where(x => x.Id == p.ChildPropertyId).FirstOrDefault();
                    if (tempProperty != null)
                    {
                        var tempHost = db.Hosts.Where(x => x.Id == tempProperty.HostId).FirstOrDefault();
                        childproperties.Add(new ChildPropertyModel()
                        {
                            Host = tempHost != null ? tempHost.Username + " (" + tempHost.Firstname + " " + tempHost.Lastname + ")" : "",
                            IsSyncProperty = tempProperty.IsSyncParent,
                            SiteType = tempProperty.SiteType,
                            Id = tempProperty.Id,
                            Name = tempProperty.Name,
                            IsChild = true
                        }) ;
                    }
                }
                int companyId = SessionHandler.CompanyId;
                var availableProperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where p.IsParentProperty==false && hc.CompanyId == companyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();
                foreach(var p in availableProperties)
                {
                    var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                    if (temp ==null)
                    {
                        childproperties.Add(new ChildPropertyModel()
                        {
                            Host = p.Host,
                            IsSyncProperty = p.IsSyncParent,
                            SiteType = p.SiteType,
                            Id = p.Id,
                            Name = p.Name,
                            IsChild = false
                        }) ;
                    }
                }
                var parentType = (Core.Enumerations.ParentPropertyType)type;
                var localchildProperty = db.Properties.Where(x =>x.SiteType==0 && x.CompanyId == SessionHandler.CompanyId && x.IsParentProperty==false).ToList();
                foreach (var p in localchildProperty)
                {
                    var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId==SessionHandler.CompanyId && x.ParentType ==type).FirstOrDefault();
                    if (temp == null)
                    {
                            childproperties.Add(new ChildPropertyModel()
                            {
                                Host = "",
                                IsSyncProperty = p.IsSyncParent,
                                SiteType = p.SiteType,
                                Id = p.Id,
                                Name = p.Name,
                                IsChild=false
                            });
                    }
                }
                if (parentType != Core.Enumerations.ParentPropertyType.Sync)
                {
                    var localParentProperty = db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId && x.IsParentProperty == true && (parentType == Core.Enumerations.ParentPropertyType.MultiUnit?x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync:(x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts))).ToList();
                    foreach (var p in localParentProperty)
                    {
                        var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == type).FirstOrDefault();
                        if (temp == null)
                        {
                            childproperties.Add(new ChildPropertyModel()
                            {
                                Host = "",
                                IsSyncProperty = p.IsSyncParent,
                                SiteType = p.SiteType,
                                Id = p.Id,
                                Name = p.Name,
                                IsChild=false
                            });
                        }
                    }
                }
                return Json(new { success = true,childproperties }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetTaskTemplateList()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<TemplateListing> tpl = new List<TemplateListing>();
                    db.TaskTemplates.ToList()
                    .ForEach(x =>
                    {
                        TemplateListing p = new TemplateListing();
                        p.Id = x.Id;
                        p.TemplateName = x.TemplateName;
                        p.Actions = "<a class=\"ui mini button edit-task-template-btn\" title=\"Edit\">" +
                                        "<i class=\"icon edit\"></i>" +
                                    "</a>" +
                                    (x.Id <= 0 ? "<a class=\"ui mini button delete-property-btn\" title=\"Delete\" >" +
                                        "<i class=\"icon ban\"></i>" +
                                        "</a>" : "");
                        tpl.Add(p);
                    });
                    return Json(new { data = tpl }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception) { return Json(new { data = new List<PropertyListing>() }, JsonRequestBehavior.AllowGet); }
        }


        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }
        [HttpPost]
        [CheckSessionTimeout]
        //[ValidateAntiForgeryToken]
        public ActionResult AddTaskTemplate(MessagerSolution.Models.Tasks.TaskTemplateModel Model)
        {
            try
            {
                // Add new task template to Task Template table in Database
                using (var db = new ApplicationDbContext())
                {
                    TaskTemplate taskTemplate = new TaskTemplate();
                    taskTemplate.TemplateName = Model.Name;
                    taskTemplate.DateCreated = DateTime.Now;
                    taskTemplate.CompanyId = SessionHandler.CompanyId;
                    taskTemplate.TemplateTypeId = Model.Type.ToInt();
                    db.TaskTemplates.Add(taskTemplate);
                    db.SaveChanges();
                    int taskTemplateId = taskTemplate.Id;
                    if (Model.Tasks != null)
                    {
                        foreach (var Task in Model.Tasks)
                        {
                            Task newTask = new Task();

                            newTask.Title = Task.Title;
                            newTask.Description = Task.Description;
                            newTask.DateCreated = DateTime.Now;
                            newTask.TemplateId = taskTemplateId;
                            if (Task.Image != null)
                            {
                                newTask.ImageUrl = UploadImage(Task.Image, "~/Images/Task/");
                            }
                            db.Tasks.Add(newTask);
                            db.SaveChanges();
                            if (Task.SubTasks != null)
                            {
                                foreach (var sub in Task.SubTasks)
                                {
                                    SubTask subTask = new SubTask
                                    {
                                        Title = sub.Title,
                                        Description = sub.Description,
                                        TaskId = newTask.Id
                                    };
                                    if (sub.Image != null)
                                    {
                                        subTask.ImageUrl = UploadImage(sub.Image, "~/Images/SubTask/");
                                    }
                                    db.SubTasks.Add(subTask);
                                    db.SaveChanges();

                                }
                            }

                        }
                    }

                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //[CheckSessionTimeout]
        ////[ValidateAntiForgeryToken]
        //public ActionResult AddTaskTemplate(string TaskTemplateName, int taskTemplateType,string Tasks, IEnumerable<HttpPostedFileBase> TaskImage/*,FormCollection form*/)
        //{
        //    int companyId = SessionHandler.CompanyId;
        //    try
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            if (companyId != 0)
        //            {
        //                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name), companyId) == false)
        //                {
        //                    return RedirectToAction("Index", "Home");
        //                }
        //            }
        //            else
        //            {
        //                return RedirectToAction("Index", "Home");
        //            }
        //            var js = new JavaScriptSerializer();
        //            var TaskList = js.Deserialize<List<MessagerSolution.Models.Tasks.Task>>(Tasks);
        //           // Add new task template to Task Template table in Database
        //            TaskTemplate taskTemplate = new TaskTemplate();
        //            taskTemplate.TemplateName = TaskTemplateName; //form["TaskTemplateName"]
        //            taskTemplate.DateCreated = DateTime.Now;
        //            taskTemplate.CompanyId = companyId;
        //            taskTemplate.TemplateTypeId = taskTemplateType;
        //            db.TaskTemplates.Add(taskTemplate);
        //            db.SaveChanges();
        //            int taskTemplateId = taskTemplate.Id;

        //            for(int x = 0; x < TaskList.Count; x++)
        //            {
        //                Task newTask = new Task();

        //                newTask.Title = TaskList[x].Title;
        //                newTask.Description = TaskList[x].Description;
        //                newTask.DateCreated = DateTime.Now;
        //                newTask.TemplateId = taskTemplateId;
        //                //newTask.Picture = TaskImage.ElementAt(x) == null ? null : ImageHelper.ConvertToBytes(TaskImage.ElementAt(x));
        //                db.Tasks.Add(newTask);
        //                db.SaveChanges();
        //                foreach(var item in TaskList[x].SubTasks)
        //                {
        //                    SubTask subTask = new SubTask
        //                    {
        //                        Title = item.Title,
        //                        Description = item.Description,
        //                        TaskId =newTask.Id

        //                    };
        //                    db.SubTasks.Add(subTask);
        //                    db.SaveChanges();

        //                }
        //            }
        //            //List<string> Task_Title = TaskTitle; //form["TaskTitle"] == null ? new List<string>() : new List<string>(form["TaskTitle"].Split(','));
        //            //List<string> Task_Description = TaskDescription; //form["TaskDescription"] == null ? new List<string>() : new List<string>(form["TaskDescription"].Split(','));
        //            ////List<Task> Tasks = new List<Task>();

        //            ////References: Save image to db http://www.c-sharpcorner.com/UploadFile/b696c4/how-to-upload-and-display-image-in-mvc/

        //            ////Add task lists to Task table in Database
        //            //if (taskTemplateId != -1)
        //            //{
        //            //    var totalTaskCount = Task_Title.Count;
        //            //    for (var e = 0; e < totalTaskCount; e++)
        //            //    {
        //            //        Task newTask = new Task
        //            //        {
        //            //            Title = Task_Title[e],
        //            //            Description = Task_Description[e],
        //            //            DateCreated = DateTime.Now,
        //            //            TemplateId = taskTemplateId,
        //            //            Picture = TaskImage == null ? null : ImageHelper.ConvertToBytes(TaskImage.ElementAt(e))
        //            //        };
        //            //        Tasks.Add(newTask);
        //            //    }

        //            //    foreach (var item in Tasks)
        //            //    {
        //            //        try
        //            //        {
        //            //            db.Tasks.Add(item);
        //            //            db.SaveChanges();
        //            //        }
        //            //        catch (Exception ex)
        //            //        {

        //            //        }
        //            //    }
        //            //}
        //            //else
        //            //{
        //            //    throw new Exception();
        //            //}

        //            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        //    }
        //}


        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult GetTaskTemplateWithTask(int taskTemplateId)
        {
            ICommonRepository objIList = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
            }

            using (var db = new ApplicationDbContext())
            {
                MessagerSolution.Models.Tasks.TaskTemplateModel Model = new Models.Tasks.TaskTemplateModel();
                var template = db.TaskTemplates.Where(x => x.Id == taskTemplateId).FirstOrDefault();
                var tasks = db.Tasks.Where(x => x.TemplateId == template.Id).ToList();
                Model.Id = template.Id.ToSafeString();
                Model.Name = template.TemplateName;
                Model.Type = template.TemplateTypeId.ToString();
                foreach (var task in tasks)
                {
                    var taskModel = new MessagerSolution.Models.Tasks.Task();
                    taskModel.Id = task.Id.ToString();
                    taskModel.Title = task.Title;
                    taskModel.Description = task.Description;
                    taskModel.ImageUrl = task.ImageUrl;
                    var subtasks = db.SubTasks.Where(x => x.TaskId == task.Id).ToList();
                    foreach (var subtask in subtasks)
                    {
                        var subTaskModel = new MessagerSolution.Models.Tasks.SubTask();
                        subTaskModel.Id = subtask.Id.ToString();
                        subTaskModel.Title = subtask.Title;
                        subTaskModel.Description = subtask.Description;
                        subTaskModel.ImageUrl = subtask.ImageUrl;
                        taskModel.SubTaskList.Add(subTaskModel);
                    }
                    Model.TaskList.Add(taskModel);
                }
                return Json(new { Template = Model }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        //[ValidateAntiForgeryToken]
        public ActionResult EditTaskTemplate(MessagerSolution.Models.Tasks.TaskTemplateModel Model)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var templateId = Model.Id.ToInt();
                    var Template = db.TaskTemplates.Where(x => x.Id == templateId).FirstOrDefault();
                    Template.TemplateName = Model.Name;
                    Template.TemplateTypeId = Model.Type.ToInt();
                    db.Entry(Template).State = EntityState.Modified;
                    db.SaveChanges();

                    foreach (var task in Model.Tasks)
                    {
                        var taskId = task.Id.ToInt();
                        if (taskId != 0)
                        {
                            var tempTask = db.Tasks.Where(x => x.Id == taskId).FirstOrDefault();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            if (task.Image != null)
                            {
                                tempTask.ImageUrl = UploadImage(task.Image, "~/Images/Task/");
                            }
                            db.Entry(tempTask).State = EntityState.Modified;
                            db.SaveChanges();
                            if (task.SubTasks != null)
                            {
                                foreach (var subtask in task.SubTasks)
                                {
                                    var subtaskId = subtask.Id.ToInt();
                                    if (subtaskId != 0)
                                    {
                                        var tempSub = db.SubTasks.Where(x => x.Id == subtaskId).FirstOrDefault();
                                        tempSub.Title = subtask.Title;
                                        tempSub.Description = subtask.Description;
                                        if (subtask.Image != null)
                                        {
                                            tempSub.ImageUrl = UploadImage(subtask.Image, "~/Images/SubTask/");
                                        }
                                        db.Entry(tempSub).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {

                                        SubTask subTask = new SubTask
                                        {
                                            Title = subtask.Title,
                                            Description = subtask.Description,
                                            TaskId = tempTask.Id
                                        };
                                        if (subtask.Image != null)
                                        {
                                            subTask.ImageUrl = UploadImage(subtask.Image, "~/Images/SubTask/");
                                        }
                                        db.SubTasks.Add(subTask);
                                        db.SaveChanges();

                                    }
                                }
                            }
                        }
                        else
                        {
                            Task newTask = new Task();

                            newTask.Title = task.Title;
                            newTask.Description = task.Description;
                            newTask.DateCreated = DateTime.Now;
                            newTask.TemplateId = Template.Id;
                            if (task.Image != null)
                            {
                                newTask.ImageUrl = UploadImage(task.Image, "~/Images/Task/");
                            }
                            db.Tasks.Add(newTask);
                            db.SaveChanges();
                            if (task.SubTasks != null)
                            {
                                foreach (var sub in task.SubTasks)
                                {
                                    SubTask subTask = new SubTask
                                    {
                                        Title = sub.Title,
                                        Description = sub.Description,
                                        TaskId = newTask.Id
                                    };
                                    if (sub.Image != null)
                                    {
                                        subTask.ImageUrl = UploadImage(sub.Image, "~/Images/SubTask/");
                                    }
                                    db.SubTasks.Add(subTask);
                                    db.SaveChanges();

                                }
                            }
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        //[HttpPost]
        //[CheckSessionTimeout]
        ////[ValidateAntiForgeryToken]
        //public ActionResult EditTaskTemplate(int TaskTemplateId, string TaskTemplateName, int taskTemplateType, List<int> TaskId,string Tasks, IEnumerable<HttpPostedFileBase> TaskImage/*FormCollection f*/)
        //{
        //    try
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            var js = new JavaScriptSerializer();
        //            var TaskList = js.Deserialize<List<MessagerSolution.Models.Tasks.Task>>(Tasks);
        //            var taskTemplateId = TaskTemplateId; //Convert.ToInt32(f["taskTemplateId"]);
        //            var entity = db.TaskTemplates.FirstOrDefault(x => x.Id == taskTemplateId && x.CompanyId == SessionHandler.CompanyId);
        //            if (entity == null)
        //            {
        //                return Redirect("/");
        //            }
        //            entity.TemplateName = TaskTemplateName; //f["TaskTemplateName"];    
        //            entity.TemplateTypeId = taskTemplateType;
        //            db.SaveChanges();

        //            List<int> Task_Ids = TaskId; //new List<int>();



        //            //List<string> Task_Title = TaskTitle; 
        //            //List<string> Task_Description = TaskDescription; 
        //            //List<Task> Task_List = new List<Task>();

        //            //if (Task_Ids.Count == Task_Title.Count && Task_Ids.Count == Task_Description.Count)
        //            //{
        //            //    int totalCount = Task_Ids.Count;
        //            //    for (var e = 0; e < totalCount; e++)
        //            //    {
        //            //        Task model = new Task();
        //            //        model.Id = Task_Ids[e];
        //            //        model.Title = Task_Title[e];
        //            //        model.Description = Task_Description[e];
        //            //        model.PictureName = TaskImage.ElementAt(e).FileName;
        //            //        model.Picture = ImageHelper.ConvertToBytes(TaskImage.ElementAt(e));
        //            //        Task_List.Add(model);
        //            //    }

        //            //}

        //            //if (Task_List.Any())
        //            //{
        //            //    foreach (var item in Task_List)
        //            //    {
        //            //        //Edit mode
        //            //        if (item.Id != 0)
        //            //        {
        //            //            var taskEntity = db.Tasks.FirstOrDefault(x => x.Id == item.Id);

        //            //            if (taskEntity != null)
        //            //            {
        //            //                taskEntity.Title = item.Title;
        //            //                taskEntity.Description = item.Description;
        //            //                if (!item.PictureName.Equals("NoNewFile"))
        //            //                {
        //            //                    taskEntity.Picture = item.Picture;
        //            //                }
        //            //                taskEntity.DateUpdated = DateTime.Now;
        //            //                db.Entry(taskEntity).State = System.Data.Entity.EntityState.Modified;
        //            //            }
        //            //        }
        //            //        //Add mode
        //            //        else
        //            //        {
        //            //            Task newTask = new Task
        //            //            {
        //            //                Title = item.Title,
        //            //                Description = item.Description,
        //            //                DateCreated = DateTime.Now,
        //            //                Picture = item.Picture,
        //            //                TemplateId = taskTemplateId
        //            //            };
        //            //            db.Tasks.Add(newTask);
        //            //            db.SaveChanges();
        //            //            var taskId = newTask.Id;


        //            //        }
        //            //    }
        //            //    db.SaveChanges();
        //            //}



        //            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteTask(int taskTemplateId, int taskId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var entity = db.Tasks.FirstOrDefault(x => x.Id == taskId && x.TemplateId == taskTemplateId);
                    db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;


                    //Updated 19/9/17: When delete task from task template, need to delete the inquiry task records associated to the deleted task by task id.
                    var inquiryTaskList = db.InquiryTasks.Where(x => x.TemplateId == taskTemplateId && x.TaskId == taskId);

                    foreach (var item in inquiryTaskList)
                    {
                        db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                    }

                    db.SaveChanges();
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [CheckSessionTimeout]
        public ActionResult RetrieveImage(int taskId)
        {
            using (var db = new ApplicationDbContext())
            {
                //byte[] cover = db.Tasks.Where(x => x.Id == taskId).FirstOrDefault().Picture;
                //if (cover != null)
                //{
                //    return File(cover, "image/jpg");
                //}
                return null;
            }
        }
        [HttpGet]
        public ActionResult GetAvailableProperty(string bedroom, string bathroom, string size, string sleeps)
        {
            using (var db = new ApplicationDbContext())
            {
                int bedrooms = bedroom.ToInt();
                int bathrooms = bathroom.ToInt();
                int? sizes = string.IsNullOrWhiteSpace(size) ? (int?)null : (int?)size.ToInt();
                int sleep = sleeps.ToInt();
                List<DisplayPropertyModel> properties = new List<DisplayPropertyModel>();
                var data = db.Properties.Where(x => x.IsActive && x.SiteType == 0 && x.IsParentProperty == false && (x.Bedrooms >= bedrooms && x.Bathrooms >= bathrooms && (x.Size == null ? true : x.Size >= sizes) && x.Sleeps >= sleep)).ToList();
                foreach (var p in data)
                {
                    properties.Add(new DisplayPropertyModel()
                    {
                        Id = p.Id,
                        Amenities = (from amenity in db.Amenities join pAmenity in db.PropertyAmenities on amenity.Id equals pAmenity.AmenityId where pAmenity.PropertyId == p.Id select amenity).ToList(),
                        Images = db.PropertyImage.Where(x => x.PropertyId == p.Id).Select(x => x.FileUrl).ToList(),
                        Name = p.Name,
                        Bathrooms = p.Bathrooms.ToString(),
                        Bedrooms = p.Bedrooms.ToString(),
                        Price = p.Price,
                        Size = p.Size,
                        Sleeps = p.Sleeps
                    });
                }
                return Json(new { properties }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
