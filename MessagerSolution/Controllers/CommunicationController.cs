﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using MessagerSolution.Models.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class CommunicationController : Controller
    {
        private readonly Service.SquareService _squarePaymentService = new Service.SquareService();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetBalance()
        {
            using(var db = new ApplicationDbContext())
            {
                var balance =db.CommunicationLoads.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList().Sum(x => x.Amount);
                var usage = db.CommunicationCharges.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList().Sum(x => x.Amount);
                balance = balance - usage;
                return Json(new { balance,usage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetUsage(string date)
        {
            using(var db = new ApplicationDbContext())
            {
                var data = new List<CommunicationUsageViewModel>();
                var filter = date.ToDateTime();
                var threads = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
              foreach(var thread in threads)
                {
                    var sms = (from user in db.Users
                               join m in db.CommunicationSMS on user.Id equals m.UserId
                               join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                               where t.ThreadId == thread.ThreadId
                               select new { Id = m.Id, ResourceSid = m.MessageSid, To = m.To, From = m.From, Message = m.Message, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();

                    foreach (var txt in sms) { 
                        var mms = "<div class=\"ui small image\">";
                    db.MMSImages.Where(x => x.CommunicationSMSId == txt.Id).Select(x=>x.ImageUrl).ToList().ForEach(x =>
                    {
                        mms += "<img src=\""+x+"\">";
                    });
                        mms += "</div>";
                        data.Add(new CommunicationUsageViewModel()
                        {
                            SendBy = txt.Name,
                            Content = txt.Message+"<br/>"+mms,
                            To = txt.To,
                            Type = "SMS"
                        });
                    }
                    var calls = (from user in db.Users
                                join m in db.CommunicationCalls on user.Id equals m.UserId
                                join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                                where t.ThreadId == thread.ThreadId
                                select new { CallSid = m.CallSid, ResourceSid = m.ResourceSid, To = m.To, From = m.From, RecordingUrl = m.RecordingUrl, Duration = m.CallDuration == null ? m.RecordingDuration : m.CallDuration, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();
                    
                    foreach (var call in calls)
                    {
                        data.Add(new CommunicationUsageViewModel()
                        {
                            SendBy =call.Name,
                            Content = call.Duration.Value.ToString(),
                            To = call.To,
                            Type = "Call"
                        });
                    }
                }
                return Json(new { data}, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddCommunicationLoad(string CreditCardNonce, string amount)
        {
            var response = _squarePaymentService.ChargeLoad(CreditCardNonce,
                       Convert.ToInt64(amount.ToString().Replace(".", "")),
                       "USD");
            if (response != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    db.CommunicationLoads.Add(new CommunicationLoad() { Amount = amount.ToDecimal(), DateRecorded = DateTime.Now, CompanyId = SessionHandler.CompanyId, SitePaymentId = response.Payment.Id });
                    db.SaveChanges();
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetChargeRate()
        {
            using(var db = new ApplicationDbContext())
            {
                return Json(new {data= db.CommunicationChargeRates.ToList() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetCompanyUsage(string companyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var id = companyId.ToInt();
                var data = new List<CommunicationUsageViewModel>();
                var threads = db.CommunicationThreads.Where(x => x.CompanyId == id).ToList();
                foreach (var thread in threads)
                {
                    var sms = (from user in db.Users
                               join m in db.CommunicationSMS on user.Id equals m.UserId
                               join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                               where t.ThreadId == thread.ThreadId
                               select new { Id = m.Id, ResourceSid = m.MessageSid, To = m.To, From = m.From, Message = m.Message, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();

                    foreach (var txt in sms)
                    {
                        var mms = "<div class=\"ui small image\">";
                        db.MMSImages.Where(x => x.CommunicationSMSId == txt.Id).Select(x => x.ImageUrl).ToList().ForEach(x =>
                        {
                            mms += "<img src=\"" + x + "\">";
                        });
                        mms += "</div>";
                        data.Add(new CommunicationUsageViewModel()
                        {
                            SendBy = txt.Name,
                            Content = txt.Message + "<br/>" + mms,
                            To = txt.To,
                            Type = "SMS"
                        });
                    }
                    var calls = (from user in db.Users
                                 join m in db.CommunicationCalls on user.Id equals m.UserId
                                 join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                                 where t.ThreadId == thread.ThreadId
                                 select new { CallSid = m.CallSid, ResourceSid = m.ResourceSid, To = m.To, From = m.From, RecordingUrl = m.RecordingUrl, Duration = m.CallDuration == null ? m.RecordingDuration : m.CallDuration, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();

                    foreach (var call in calls)
                    {
                        data.Add(new CommunicationUsageViewModel()
                        {
                            SendBy = call.Name,
                            Content = call.Duration.Value.ToString(),
                            To = call.To,
                            Type = "Call"
                        });
                    }
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveCommunicationRate(CommunicationChargeRate chargeRate)
        {
            using(var db = new ApplicationDbContext())
            {
                if (chargeRate.Id == 0)
                {
                    db.CommunicationChargeRates.Add(chargeRate);
                }
                else
                {
                    var temp = db.CommunicationChargeRates.Where(x => x.Id == chargeRate.Id).FirstOrDefault();
                    temp.Amount = chargeRate.Amount;
                    temp.EffectiveDate = chargeRate.EffectiveDate;
                    temp.Type = chargeRate.Type;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();
                return Json(new {success=true }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpGet]
        public ActionResult GetCommunicationRate(int id)
        {
            using(var db = new ApplicationDbContext())
            {
                return Json(new {rate = db.CommunicationChargeRates.Where(x=>x.Id==id).FirstOrDefault() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ThreadInfo(string threadId)
        {
            using(var db= new ApplicationDbContext())
            {
                var thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                var Name = thread.RenterId != null ? db.Renters.Where(x => x.Id== thread.RenterId).Select(x => new {Name = x.Firstname +" "+x.Lastname}).ToList() : thread.VendorId != null ? db.Vendors.Where(x => x.Id == thread.VendorId).Select(x => new { Name = x.Name }).ToList() : db.Workers.Where(x => x.Id == thread.WorkerId).Select(x => new  { Name = x.Firstname + " " + x.Lastname }).ToList();
                return Json(new { Name }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        public ActionResult AddContact(string threadId, string phoneNumber)
        {
            using (var db = new ApplicationDbContext())
            {
                
                var thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (thread.RenterId != null)
                {
                    var contacts = db.RenterContacts.Where(x => x.RenterId == thread.RenterId.Value).ToList();
                    contacts.ForEach(x => x.IsDefault = false);
                    db.SaveChanges();
                    db.RenterContacts.Add(new RenterContact() { Contact = phoneNumber, RenterId =thread.RenterId.Value});
                    db.SaveChanges();
                }
                else if (thread.WorkerId != null)
                {
                    var contacts = db.WorkerContactInfos.Where(x => x.WorkerId== thread.WorkerId.Value).ToList();
                    contacts.ForEach(x => x.IsDefault = false);
                    db.SaveChanges();
                    db.WorkerContactInfos.Add(new WorkerContactInfo() { Value = phoneNumber, WorkerId = thread.WorkerId.Value });
                    db.SaveChanges();
                }
                else if (thread.VendorId != null)
                {
                    var contacts = db.VendorContactNumbers.Where(x => x.VendorId == thread.VendorId.Value).ToList();
                    contacts.ForEach(x => x.IsDefault = false);
                    db.SaveChanges();
                    db.VendorContactNumbers.Add(new VendorContactNumber() {Value= phoneNumber, VendorId = thread.VendorId.Value });
                    db.SaveChanges();

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddEmail(string threadId, string email)
        {
            using (var db = new ApplicationDbContext())
            {
                var thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (thread.RenterId != null)
                {
                    var contacts = db.RenterEmails.Where(x => x.RenterId == thread.RenterId.Value).ToList();
                    contacts.ForEach(x => x.IsDefault = false);
                    db.SaveChanges();
                    db.RenterEmails.Add(new RenterEmail() { Email =email, RenterId = thread.RenterId.Value });
                    db.SaveChanges();
                }
                else if (thread.WorkerId != null)
                {
                    var contacts = db.WorkerEmails.Where(x => x.WorkerId == thread.WorkerId.Value).ToList();
                    contacts.ForEach(x => x.IsDefault = false);
                    db.SaveChanges();
                    db.WorkerEmails.Add(new WorkerEmail() { Email = email, WorkerId = thread.WorkerId.Value });
                    db.SaveChanges();
                }
                else if (thread.VendorId != null)
                {
                    var contacts = db.VendorEmails.Where(x => x.VendorId == thread.VendorId.Value).ToList();
                    contacts.ForEach(x => x.IsDefault = false);
                    db.SaveChanges();
                    db.VendorEmails.Add(new VendorEmail() { Email = email, VendorId = thread.VendorId.Value });
                    db.SaveChanges();

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}