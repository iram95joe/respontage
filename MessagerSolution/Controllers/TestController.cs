﻿using BaseScrapper;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class TestController : Controller
    {
        ICommonRepository objIList = new CommonRepository();

        public string Index()
        {
            return "TEST MODE";
        }

        //public async Task<string> ModifyCalendar(long listingId)
        //{
        //    try
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();


        //            if ((bool)property.PriceRuleSync)
        //            {
        //                Core.Models.Analytics analytics = new Core.Models.Analytics();
        //                var calendar = db.PropertyBookingDate.Where(x => x.PropertyId == listingId).ToList();

        //                foreach (var pbd in calendar)
        //                {
        //                    await System.Threading.Tasks.Task.Run(() =>
        //                    {
        //                        if (pbd.Date >= DateTime.Now)
        //                        {
        //                            using (var sm = new Airbnb.ScrapeManager())
        //                            {
        //                                analytics = sm.GetAnalytics(property.Latitude, property.Longitude, pbd.Date, property.Bedrooms);
        //                            }

        //                            pbd.Price = property.PriceRuleSign == 0
        //                                ? (double)(pbd.Price + (analytics.AveragePriceByDate * ("." + property.PriceRuleAmount).ToDouble()))
        //                                : (double)(pbd.Price + analytics.AveragePriceByDate + property.PriceRuleAmount);

        //                            db.SaveChanges();
        //                        }
        //                    });
        //                }

        //                return "Calendar successfully modified !";
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return "Failed on modifying calendar...";
        //    }

        //    return "Failed on modifying calendar...";
        //}

        [Route("test/propertylist")]
        [CheckSessionTimeout]
        public async Task<ActionResult> PropertyList()  
        {
                using (var db = new ApplicationDbContext())
                {
                    var properties =(from property in db.Properties join host in db.Hosts on property.HostId equals host.Id join hostCompany in db.HostCompanies on host.Id equals hostCompany.HostId where !property.IsParentProperty && property.SiteType != 0 && hostCompany.CompanyId == SessionHandler.CompanyId select property).ToList();
                    properties.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && !x.IsParentProperty).ToList());
                    ViewBag.Property = properties;
                    ViewBag.PriceRuleSettings = JsonConvert.SerializeObject(db.PriceRuleSettings.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList());
                    ViewBag.Hosts = JsonConvert.SerializeObject((from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select h).ToList());
                }
            return View();
        }

        [Route("test/calendar/samplecalendar")]
        public async Task<ActionResult> SampleCalendar(long listingId, int month = 0, int year = 0)
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                using (var db = new ApplicationDbContext())
                {
                    var calendar = new List<Core.Database.Entity.PropertyBookingDate>();
                    //string redirectUrl = TempData["redirect"] != null ? TempData["redirect"].ToString() : "analytics";
                    //if (month != 0 && year != 0)
                    //{
                    //    calendar = db.PropertyBookingDate.Where(x => x.PropertyId == listingId
                    //        && x.Date.Month == month
                    //        && x.Date.Year == year)
                    //        .ToList();

                    //    redirectUrl = redirectUrl + "?month=" + month + "&year=" + year;
                    //}
                    //else
                    //{
                    //    calendar = db.PropertyBookingDate.Where(x => x.PropertyId == listingId).ToList();
                    //}
                    //ViewBag.Calendar = calendar;
                    ViewBag.Property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                    ViewBag.PriceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(listingId);
                    ViewBag.RuleUsedAndComputations = TempData["ruleUsedAndComputations"];
                    ViewBag.ComputationResults = TempData["computationResults"];
                    //ViewBag.Redirect = redirectUrl;
                    ViewBag.ButtonText = TempData["buttonText"] != null ? TempData["buttonText"] : "Get Analytics and Calculate";
                }
            });

            return View();
        }

        //[Route("test/calendar/samplecalendar/analytics")]
        //public async Task<ActionResult> AnalyticsSampleCalendar(long listingId, List<string> bookingDateCheckbox, int month = 0, int year = 0)
        //{
        //    List<Core.Models.PriceModelerResult> result = new List<Core.Models.PriceModelerResult>();
        //    Core.Models.ComparablesAndBedrooms comparables = new Core.Models.ComparablesAndBedrooms();
        //    List<ExcludedComparable> excludedComparables = new List<ExcludedComparable>();

        //    await System.Threading.Tasks.Task.Run(() =>
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
        //            excludedComparables = db.ExcludedComparables.Where(x => x.HostId == property.HostId).ToList();
        //            //Selected booking cdateCheckbox is required
        //            foreach (var bd in bookingDateCheckbox)
        //            {

        //                string[] bdValue = bd.Split('|');
        //                int calendarId = bdValue[0].ToInt();
        //                DateTime calendarDate = bdValue[1].ToDateTime();
        //                double calendarPrice = bdValue[2].ToDouble();
        //                Core.Models.Analytics analytics = new Core.Models.Analytics();
        //                string ruleUsedAndComputations = "";
        //                //Get comparable
        //                using (var sm = new Airbnb.ScrapeManager())
        //                {
        //                    comparables = sm.GetComparables(property.Latitude, property.Longitude, property.Bedrooms, calendarDate);
        //                }

        //                string excludedListingsQuery = "";
        //                foreach (var listing in db.ExcludedComparables)
        //                {
        //                    excludedListingsQuery += "&excludedListings=" + listing.ListingId;
        //                }

        //                //Get analytics
        //                switch ((Core.Enumerations.SiteType)property.SiteType)
        //                {
        //                    case Core.Enumerations.SiteType.Airbnb:
        //                        using (var sm = new Airbnb.ScrapeManager())
        //                        {
        //                            analytics = sm.GetAnalytics(property.Latitude, property.Longitude, calendarDate, property.Bedrooms, excludedListingsQuery);
        //                        }
        //                        break;

        //                    case Core.Enumerations.SiteType.VRBO:
        //                        using (var sm = new VRBO.ScrapeManager())
        //                        {
        //                            analytics = sm.GetAnalytics(property.Latitude, property.Longitude, calendarDate, property.Bedrooms, excludedListingsQuery);
        //                        }
        //                        break;

        //                    case Core.Enumerations.SiteType.Homeaway:
        //                        using (var sm = new VRBO.ScrapeManager())
        //                        {
        //                            analytics = sm.GetAnalytics(property.Latitude, property.Longitude, calendarDate, property.Bedrooms, excludedListingsQuery);
        //                        }
        //                        break;
        //                }

        //                var priceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(listingId);

        //                double rate = 0;
        //                double computedPriceRuleAmount = 0;
        //                double price = 0;
        //                double finalPrice = 0;

        //                foreach (var settings in priceRuleSettings)
        //                {
        //                    ruleUsedAndComputations = "If ";

        //                    #region PriceRuleBasis

        //                    switch (settings.PriceRuleBasis)
        //                    {
        //                        case 0:
        //                            rate = analytics.OccupancyRateByDate;
        //                            ruleUsedAndComputations += "Occupancy Rate By Date is ";
        //                            break;

        //                        case 1:
        //                            rate = analytics.OccupancyRateByDateForXRooms;
        //                            ruleUsedAndComputations += "Occupancy Rate By Date for 'x' Rooms is ";
        //                            break;

        //                        case 2:
        //                            rate = analytics.OccupancyRateByMonth;
        //                            ruleUsedAndComputations += "Occupancy Rate By Month is ";
        //                            break;

        //                        case 3:
        //                            rate = analytics.OccupancyRateByMonthForXRooms;
        //                            ruleUsedAndComputations += "Occupancy Rate By Month for 'x' Rooms is ";
        //                            break;

        //                        case 4:
        //                            rate = analytics.AvailabilityRateByDate;
        //                            ruleUsedAndComputations += "Availability Rate By Date is ";
        //                            break;

        //                        case 5:
        //                            rate = analytics.AvailabilityRateByDateForXRooms;
        //                            ruleUsedAndComputations += "Availability Rate By Date for 'x' Rooms is ";
        //                            break;

        //                        case 6:
        //                            rate = analytics.AvailabilityRateByMonth;
        //                            ruleUsedAndComputations += "Availability Rate By Month";
        //                            break;

        //                        case 7:
        //                            rate = analytics.AvailabilityRateByMonthForXRooms;
        //                            ruleUsedAndComputations += "Availability Rate By Month for 'x' Rooms is ";
        //                            break;
        //                    }

        //                    #endregion

        //                    #region PriceRuleAveragePriceBasis

        //                    switch (settings.PriceRuleAveragePriceBasis)
        //                    {
        //                        case 0:
        //                            price = analytics.AveragePriceByDate;
        //                            break;

        //                        case 1:
        //                            price = analytics.AveragePriceByDateForXRooms;
        //                            break;

        //                        case 2:
        //                            price = analytics.AveragePriceByMonth;
        //                            break;

        //                        case 3:
        //                            price = analytics.AveragePriceByMonthForXRooms;
        //                            break;
        //                    }

        //                    #endregion

        //                    if (settings.PriceRuleCondition == 0)
        //                    {
        //                        if (rate >= settings.PriceRuleConditionPercentage)
        //                        {
        //                            if (settings.PriceRuleSign == 0)
        //                            {
        //                                computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
        //                                finalPrice = price + computedPriceRuleAmount;
        //                                ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + " %; and " +
        //                                    "average price = " + price + "; computedPriceRuleAmount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "; " +
        //                                    "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                computedPriceRuleAmount = (double)settings.PriceRuleAmount;
        //                                finalPrice = price + computedPriceRuleAmount;
        //                                ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + " $; and " +
        //                                    "average price = " + price + "; computedPriceRuleAmount = " + (double)settings.PriceRuleAmount + "; " +
        //                                    "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                break;
        //                            }
        //                        }
        //                        else {
        //                            //computedPriceRuleAmount = parseFloat(price).toFixed(2);
        //                            //price = parseFloat(parseFloat(price).toFixed(2));
        //                            continue;
        //                        }
        //                    }
        //                    else if (settings.PriceRuleCondition == 1)
        //                    {
        //                        if (rate <= settings.PriceRuleConditionPercentage)
        //                        {
        //                            if (settings.PriceRuleSign == 0)
        //                            {
        //                                computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
        //                                finalPrice = price + computedPriceRuleAmount;
        //                                ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " %; and " +
        //                                    "average price = " + price + "; computedPriceRuleAmount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "; " +
        //                                    "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                computedPriceRuleAmount = (double)settings.PriceRuleAmount;
        //                                finalPrice = price + computedPriceRuleAmount;
        //                                ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " $; and " +
        //                                    "average price = " + price + "; computedPriceRuleAmount = " + (double)settings.PriceRuleAmount + "; " +
        //                                    "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                break;
        //                            }
        //                        }
        //                        else {
        //                            //computedPriceRuleAmount = parseFloat(parseFloat(price).toFixed(2));
        //                            //price = parseFloat(parseFloat(price).toFixed(2));
        //                            continue;
        //                        }
        //                    }
        //                }

        //                result.Add(new Core.Models.PriceModelerResult
        //                {
        //                    Analytics = JsonConvert.SerializeObject(analytics),
        //                    Bedrooms = property.Bedrooms,
        //                    HostId = property.HostId,
        //                    CalendarId = calendarId,
        //                    Comparables = JsonConvert.SerializeObject(comparables),
        //                    RuleUsedAndComputations = ruleUsedAndComputations
        //                });
        //            }
        //        }
        //    });

        //    TempData["computationResults"] = result;
        //    TempData["redirect"] = "update";
        //    TempData["buttonText"] = "Modify Price Using Price Rule";

        //    return RedirectToAction("SampleCalendar", new { listingId = listingId, month = month, year = year });
        //}

        //[Route("test/calendar/samplecalendar/analyticsresult")]
        //public async Task<ActionResult> AnalyticsResult(long listingId, DateTime date, string analyticsResult)
        //{
        //    string ruleUsedAndComputations = "";
        //    var analytics = JsonConvert.DeserializeObject<Core.Models.Analytics>(analyticsResult);

        //    await System.Threading.Tasks.Task.Run(() =>
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
        //            var priceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(listingId);

        //            double rate = 0;
        //            double computedPriceRuleAmount = 0;
        //            double price = 0;
        //            double finalPrice = 0;

        //            foreach (var settings in priceRuleSettings)
        //            {
        //                ruleUsedAndComputations = "If ";

        //                #region PriceRuleBasis

        //                switch (settings.PriceRuleBasis)
        //                {
        //                    case 0:
        //                        rate = analytics.OccupancyRateByDate;
        //                        ruleUsedAndComputations += "Occupancy Rate By Date is ";
        //                        break;

        //                    case 1:
        //                        rate = analytics.OccupancyRateByDateForXRooms;
        //                        ruleUsedAndComputations += "Occupancy Rate By Date for 'x' Rooms is ";
        //                        break;

        //                    case 2:
        //                        rate = analytics.OccupancyRateByMonth;
        //                        ruleUsedAndComputations += "Occupancy Rate By Month is ";
        //                        break;

        //                    case 3:
        //                        rate = analytics.OccupancyRateByMonthForXRooms;
        //                        ruleUsedAndComputations += "Occupancy Rate By Month for 'x' Rooms is ";
        //                        break;

        //                    case 4:
        //                        rate = analytics.AvailabilityRateByDate;
        //                        ruleUsedAndComputations += "Availability Rate By Date is ";
        //                        break;

        //                    case 5:
        //                        rate = analytics.AvailabilityRateByDateForXRooms;
        //                        ruleUsedAndComputations += "Availability Rate By Date for 'x' Rooms is ";
        //                        break;

        //                    case 6:
        //                        rate = analytics.AvailabilityRateByMonth;
        //                        ruleUsedAndComputations += "Availability Rate By Month";
        //                        break;

        //                    case 7:
        //                        rate = analytics.AvailabilityRateByMonthForXRooms;
        //                        ruleUsedAndComputations += "Availability Rate By Month for 'x' Rooms is ";
        //                        break;
        //                }

        //                #endregion

        //                #region PriceRuleAveragePriceBasis

        //                switch (settings.PriceRuleAveragePriceBasis)
        //                {
        //                    case 0:
        //                        price = analytics.AveragePriceByDate;
        //                        break;

        //                    case 1:
        //                        price = analytics.AveragePriceByDateForXRooms;
        //                        break;

        //                    case 2:
        //                        price = analytics.AveragePriceByMonth;
        //                        break;

        //                    case 3:
        //                        price = analytics.AveragePriceByMonthForXRooms;
        //                        break;
        //                }

        //                #endregion
        //                //Price rules condition use for greater or lower base on result
        //                if (settings.PriceRuleCondition == 0)
        //                {
        //                    if (rate >= settings.PriceRuleConditionPercentage)
        //                    {
        //                        //price rule sign is to know 
        //                        if (settings.PriceRuleSign == 0)
        //                        {
        //                            computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
        //                            finalPrice = price + computedPriceRuleAmount;
        //                            ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + " %, and " +
        //                                "average price = " + price + "; computedPriceRuleAmount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "; " +
        //                                "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            computedPriceRuleAmount = (double)settings.PriceRuleAmount;
        //                            finalPrice = price + computedPriceRuleAmount;
        //                            ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + " $, and " +
        //                                "average price = " + price + "; computedPriceRuleAmount = " + (double)settings.PriceRuleAmount + "; " +
        //                                "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                            break;
        //                        }
        //                    }
        //                    else {
        //                        //computedPriceRuleAmount = parseFloat(price).toFixed(2);
        //                        //price = parseFloat(parseFloat(price).toFixed(2));
        //                        continue;
        //                    }
        //                }
        //                else if (settings.PriceRuleCondition == 1)
        //                {
        //                    if (rate <= settings.PriceRuleConditionPercentage)
        //                    {
        //                        if (settings.PriceRuleSign == 0)
        //                        {
        //                            computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
        //                            finalPrice = price + computedPriceRuleAmount;
        //                            ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " %, and " +
        //                                "average price = " + price + "; computedPriceRuleAmount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "; " +
        //                                "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            computedPriceRuleAmount = (double)settings.PriceRuleAmount;
        //                            finalPrice = price + computedPriceRuleAmount;
        //                            ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " $, and " +
        //                                "average price = " + price + "; computedPriceRuleAmount = " + (double)settings.PriceRuleAmount + "; " +
        //                                "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                            break;
        //                        }
        //                    }
        //                    else {
        //                        //computedPriceRuleAmount = parseFloat(parseFloat(price).toFixed(2));
        //                        //price = parseFloat(parseFloat(price).toFixed(2));
        //                        continue;
        //                    }
        //                }
        //            }

        //            ViewBag.Analytics = analytics;
        //            ViewBag.Property = property.Name;
        //            ViewBag.Date = date.ToString("MMMM dd, yyyy");
        //            ViewBag.Computations = ruleUsedAndComputations;
        //        }
        //    });

        //    return View();
        //}

        //[Route("test/calendar/samplecalendar/update")]
        //public async Task<ActionResult> UpdateSampleCalendar(long listingId, List<string> bookingDateCheckbox, int month = 0, int year = 0)
        //{
        //    List<Tuple<long, string>> ruleUsedAndComputations = new List<Tuple<long, string>>();
        //    try
        //    {
        //        await System.Threading.Tasks.Task.Run(async () =>
        //        {
        //            using (var db = new ApplicationDbContext())
        //            {
        //                var calendar = db.PropertyBookingDate.Where(x => x.PropertyId == listingId).ToList();
        //                var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
        //                var priceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(listingId);

        //                foreach (var bd in bookingDateCheckbox)
        //                {
        //                    Core.Models.Analytics analytics = new Core.Models.Analytics();
        //                    string[] bdValue = bd.Split('|');
        //                    int calendarId = bdValue[0].ToInt();
        //                    DateTime calendarDate = bdValue[1].ToDateTime();
        //                    double calendarPrice = bdValue[2].ToDouble();

        //                    string excludedListingsQuery = "";
        //                    foreach (var listing in db.ExcludedComparables)
        //                    {
        //                        excludedListingsQuery += "&excludedListings=" + listing.ListingId;
        //                    }

        //                    switch ((Core.Enumerations.SiteType)property.SiteType)
        //                    {
        //                        case Core.Enumerations.SiteType.Airbnb:
        //                            using (var sm = new Airbnb.ScrapeManager())
        //                            {
        //                                analytics = sm.GetAnalytics(property.Latitude, property.Longitude, calendarDate, property.Bedrooms, excludedListingsQuery, siteType, includeAllSite);
        //                            }
        //                            break;

        //                        case Core.Enumerations.SiteType.VRBO:
        //                            using (var sm = new VRBO.ScrapeManager())
        //                            {
        //                                analytics = sm.GetAnalytics(property.Latitude, property.Longitude, calendarDate, property.Bedrooms, excludedListingsQuery);
        //                            }
        //                            break;

        //                        case Core.Enumerations.SiteType.Homeaway:
        //                            using (var sm = new VRBO.ScrapeManager())
        //                            {
        //                                analytics = sm.GetAnalytics(property.Latitude, property.Longitude, calendarDate, property.Bedrooms, excludedListingsQuery);
        //                            }
        //                            break;
        //                    }

        //                    //PriceRuleBasis
        //                    //0 - "Occupancy Rate by Date"
        //                    //1 - "Occupancy Rate by Date for 'x' Rooms"
        //                    //2 - "Occupancy Rate by Month"
        //                    //3 - "Occupancy Rate by Month for 'x' Rooms"
        //                    //4 - "Availability Rate by Date"
        //                    //5 - "Availability Rate by Date for 'x' Rooms"
        //                    //6 - "Availability Rate by Month"
        //                    //7 - "Availability Rate by Month for 'x' Rooms"

        //                    //PriceRuleCondition
        //                    //0 - "greater than"
        //                    //1 - "less than"

        //                    //PriceRuleSign
        //                    //0 - "%"
        //                    //1 - "$"

        //                    //PriceRuleAveragePriceBasis
        //                    //0 - "Average Price by Date"
        //                    //1 - "Average Price by Date for 'x' Rooms"
        //                    //2 - "Average Price by Month"
        //                    //3 - "Average Price by Month for 'x' Rooms"

        //                    double rate = 0;
        //                    double computedPriceRuleAmount = 0;
        //                    double price = 0;
        //                    double finalPrice = 0;

        //                    foreach (var settings in priceRuleSettings)
        //                    {
        //                        string ruleUsedTxt = "If ";

        //                        #region PriceRuleBasis

        //                        switch (settings.PriceRuleBasis)
        //                        {
        //                            case 0:
        //                                rate = analytics.OccupancyRateByDate;
        //                                ruleUsedTxt += "Occupancy Rate By Date is ";
        //                                break;

        //                            case 1:
        //                                rate = analytics.OccupancyRateByDateForXRooms;
        //                                ruleUsedTxt += "Occupancy Rate By Date for 'x' Rooms is ";
        //                                break;

        //                            case 2:
        //                                rate = analytics.OccupancyRateByMonth;
        //                                ruleUsedTxt += "Occupancy Rate By Month is ";
        //                                break;

        //                            case 3:
        //                                rate = analytics.OccupancyRateByMonthForXRooms;
        //                                ruleUsedTxt += "Occupancy Rate By Month for 'x' Rooms is ";
        //                                break;

        //                            case 4:
        //                                rate = analytics.AvailabilityRateByDate;
        //                                ruleUsedTxt += "Availability Rate By Date is ";
        //                                break;

        //                            case 5:
        //                                rate = analytics.AvailabilityRateByDateForXRooms;
        //                                ruleUsedTxt += "Availability Rate By Date for 'x' Rooms is ";
        //                                break;

        //                            case 6:
        //                                rate = analytics.AvailabilityRateByMonth;
        //                                ruleUsedTxt += "Availability Rate By Month";
        //                                break;

        //                            case 7:
        //                                rate = analytics.AvailabilityRateByMonthForXRooms;
        //                                ruleUsedTxt += "Availability Rate By Month for 'x' Rooms is ";
        //                                break;
        //                        }

        //                        #endregion

        //                        #region PriceRuleAveragePriceBasis

        //                        switch (settings.PriceRuleAveragePriceBasis)
        //                        {
        //                            case 0:
        //                                price = analytics.AveragePriceByDate;
        //                                break;

        //                            case 1:
        //                                price = analytics.AveragePriceByDateForXRooms;
        //                                break;

        //                            case 2:
        //                                price = analytics.AveragePriceByMonth;
        //                                break;

        //                            case 3:
        //                                price = analytics.AveragePriceByMonthForXRooms;
        //                                break;
        //                        }

        //                        #endregion

        //                        if (settings.PriceRuleCondition == 0)
        //                        {
        //                            if (rate >= settings.PriceRuleConditionPercentage)
        //                            {
        //                                if (settings.PriceRuleSign == 0)
        //                                {
        //                                    computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
        //                                    finalPrice = price + computedPriceRuleAmount;
        //                                    ruleUsedTxt += "greater than " + settings.PriceRuleConditionPercentage + " %, and " +
        //                                        "average price = " + price + "; computedPriceRuleAmount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "; " +
        //                                        "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                    ruleUsedAndComputations.Add(new Tuple<long, string>(calendarId, ruleUsedTxt));
        //                                    break;
        //                                }
        //                                else
        //                                {
        //                                    computedPriceRuleAmount = (double)settings.PriceRuleAmount;
        //                                    finalPrice = price + computedPriceRuleAmount;
        //                                    ruleUsedTxt += "greater than " + settings.PriceRuleConditionPercentage + " $, and " +
        //                                        "average price = " + price + "; computedPriceRuleAmount = " + (double)settings.PriceRuleAmount + "; " +
        //                                        "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                    ruleUsedAndComputations.Add(new Tuple<long, string>(calendarId, ruleUsedTxt));
        //                                    break;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                //computedPriceRuleAmount = parseFloat(price).toFixed(2);
        //                                //price = parseFloat(parseFloat(price).toFixed(2));
        //                                continue;
        //                            }
        //                        }
        //                        else if (settings.PriceRuleCondition == 1)
        //                        {
        //                            if (rate <= settings.PriceRuleConditionPercentage)
        //                            {
        //                                if (settings.PriceRuleSign == 0)
        //                                {
        //                                    computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
        //                                    finalPrice = price + computedPriceRuleAmount;
        //                                    ruleUsedTxt += "less than " + settings.PriceRuleConditionPercentage + " %, and " +
        //                                        "average price = " + price + "; computedPriceRuleAmount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "; " +
        //                                        "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                    ruleUsedAndComputations.Add(new Tuple<long, string>(calendarId, ruleUsedTxt));
        //                                    break;
        //                                }
        //                                else
        //                                {
        //                                    computedPriceRuleAmount = (double)settings.PriceRuleAmount;
        //                                    finalPrice = price + computedPriceRuleAmount;
        //                                    ruleUsedTxt += "less than " + settings.PriceRuleConditionPercentage + " $, and " +
        //                                        "average price = " + price + "; computedPriceRuleAmount = " + (double)settings.PriceRuleAmount + "; " +
        //                                        "final price = average price (" + price + ") + computedPriceRuleAmount (" + computedPriceRuleAmount + "); final price = " + finalPrice;
        //                                    ruleUsedAndComputations.Add(new Tuple<long, string>(calendarId, ruleUsedTxt));
        //                                    break;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                //computedPriceRuleAmount = parseFloat(parseFloat(price).toFixed(2));
        //                                //price = parseFloat(parseFloat(price).toFixed(2));
        //                                continue;
        //                            }
        //                        }
        //                    }

        //                    Tuple<bool, Core.Database.Entity.PropertyBookingDate> updateResult =
        //                    Core.API.AllSite.PropertyBookingDate.Update(new PropertyBookingDate { Id = calendarId, Price = finalPrice });

        //                    bool success = updateResult.Item1;
        //                    var bookingDate = updateResult.Item2;

        //                    if (success)
        //                    {
        //                        using (HttpClient client = new HttpClient())
        //                        {
        //                            client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(bookingDate.SiteType));
        //                            //Set the parameter of API post request
        //                            JObject message = null;
        //                            var reqParams = new Dictionary<string, string>();
        //                            reqParams.Add("hostId", property.HostId.ToString());
        //                            reqParams.Add("propertyId", property.ListingId.ToString());
        //                            reqParams.Add("from", bookingDate.Date.ToString("yyyy-MM-dd"));
        //                            reqParams.Add("to", bookingDate.Date.ToString("yyyy-MM-dd"));
        //                            reqParams.Add("isAvailable", bookingDate.IsAvailable.ToString());
        //                            reqParams.Add("price", finalPrice.ToString());
        //                            reqParams.Add("siteType", bookingDate.SiteType.ToString());
        //                            reqParams.Add("syncChangesToOtherChild", false.ToString());
        //                            var reqContent = new FormUrlEncodedContent(reqParams);

        //                            var result = await client.PostAsync("Calendar/SetPrice", reqContent);
        //                            if (result != null && result.Content != null)
        //                            {
        //                                //get result and parse it into JObject to get the content
        //                                message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        });

        //        TempData["ruleUsedAndComputations"] = ruleUsedAndComputations;
        //        TempData["redirect"] = "analytics";

        //    }
        //    catch
        //    {
        //    }
        //    return RedirectToAction("SampleCalendar", new { listingId = listingId, month = month, year = year });
        //}

        [HttpPost]
        [Route("test/pricesettings/save")]
        public async Task<ActionResult> SavePriceRuleSettings(long listingId, int[] priceRuleBasis,
            int[] priceRuleCondition, int[] priceRuleConditionPercentage, double[] priceRuleAmountPlusMinus,
            int[] priceRuleAmount, int[] priceRuleSign, int[] priceRuleAveragePriceBasis)
        {
            bool success = false;

            await System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var priceRuleSettings = db.PriceRuleSettings.Where(x => x.ListingId == listingId).ToList();
                        db.PriceRuleSettings.RemoveRange(priceRuleSettings);
                            
                        try
                        {
                            for (int i = 0; i < priceRuleBasis.Length; i++)
                            {
                                db.PriceRuleSettings.Add(new PriceRuleSettings
                                {
                                    ListingId = listingId,
                                    PriceRuleBasis = priceRuleBasis[i],
                                    PriceRuleCondition = priceRuleCondition[i],
                                    PriceRuleConditionPercentage = priceRuleConditionPercentage[i],
                                    PriceRuleAmount = priceRuleAmountPlusMinus[i] == 0 ? priceRuleAmount[i] : (priceRuleAmount[i] * -1),
                                    PriceRuleSign = priceRuleSign[i],
                                    PriceRuleAveragePriceBasis = priceRuleAveragePriceBasis[i],
                                    CompanyId = GlobalVariables.CompanyId
                                });
                            }

                            success = true;
                        }
                        catch (Exception e)
                        {
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {

                }
            });

            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        [Route("test/property/comparable")]
        public async Task<ActionResult> FilterComparables(bool isPrivateRoomOnly, bool includeSiteByOnly,int siteType,long listingId, int bedrooms, DateTime date, int miles = 20)
        {
            Core.Models.ComparablesAndBedrooms comparables = new Core.Models.ComparablesAndBedrooms();

            await System.Threading.Tasks.Task.Run(() =>
            {
                using (var db = new ApplicationDbContext())
                {
                    var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();

                    using (var sm = new Airbnb.ScrapeManager())
                    {
                        comparables = sm.GetComparables(isPrivateRoomOnly,siteType, includeSiteByOnly,property.Latitude, property.Longitude, bedrooms, date, miles);
                    }
                }
            });

            JsonResult json = Json(new { comparables }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        [Route("test/property/comparable/exclude")]
        public async Task<ActionResult> ExcludeComparables(int hostId, string listingId)
        {
            bool success = false;

            await System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        db.ExcludedComparables.Add(new ExcludedComparable
                        {
                            HostId = hostId,
                            ListingId = listingId
                        });

                        db.SaveChanges();

                        success = true;
                    }
                }
                catch (Exception e)
                {
                    success = false;
                }
            });

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }

        [Route("test/property/comparable/include")]
        public async Task<ActionResult> IncludeComparables(int hostId, string listingId)
        {
            bool success = false;

            await System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var comparable = db.ExcludedComparables.Where(x => x.HostId == hostId && x.ListingId == listingId).FirstOrDefault();
                        db.ExcludedComparables.Remove(comparable);
                        db.SaveChanges();

                        success = true;
                    }
                }
                catch (Exception e)
                {
                    success = false;
                }
            });

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }
    }
}
