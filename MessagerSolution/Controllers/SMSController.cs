﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using Core.SignalR.Hubs;
using MessagerSolution.Helper.ImagesCompress;
using MessagerSolution.Models;
using Microsoft.Win32;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;

namespace MessagerSolution.Controllers
{   
    public class SMSController : TwilioController
    {
        private const string SavePath = "~/Images/MMS/";
        [HttpPost]
        public TwiMLResult Index(SmsRequest request, int numMedia)
        {
            var messagingResponse = new MessagingResponse();
            try
            {
                List<string> MMSImages = new List<string>();
                for (var i = 0; i < numMedia; i++)
                {
                    var mediaUrl = Request.Form[$"MediaUrl{i}"];
                    var contentType = Request.Form[$"MediaContentType{i}"];
                    var file = GetMediaFileName(mediaUrl, contentType);
                    SaveMMS(mediaUrl, Server.MapPath(SavePath + file));
                    MMSImages.Add(SavePath.Replace("~","") +file);
                }
                using (var db = new ApplicationDbContext())
                {
                    var twillioNumber = db.TwilioNumbers.Where(x => x.PhoneNumber == request.To).FirstOrDefault();

                    if (twillioNumber == null)
                    {
                        return new TwiMLResult(messagingResponse);
                    }

                    var checkIfBlocked = db.BlockedPhoneNumbers.Where(x => x.PhoneNumber == request.From && x.CompanyId == twillioNumber.CompanyId).FirstOrDefault();

                    if (checkIfBlocked != null)
                    {
                        return new TwiMLResult(messagingResponse);
                    }

                    List<string> formats = new List<string>();
                    formats.Add(request.From);
                    var phoneNumber = request.From;
                    if (phoneNumber[0] == '1')
                    {
                        formats.Add("+" + phoneNumber.Substring(1));
                    }
                    if (phoneNumber[0] == '0')
                    {
                        formats.Add(phoneNumber.Substring(1));
                    }
                    if (phoneNumber.Contains('(') || phoneNumber.Contains(')') || phoneNumber.Contains(' ') || phoneNumber.Contains('-'))
                    {
                        formats.Add(phoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                    }


                    var worker = db.Workers.Join(db.WorkerContactInfos, x => x.Id, y => y.WorkerId, (x, y) => new { Worker = x, WorkerContactInfo = y })
                   .Where(x => formats.Contains(x.WorkerContactInfo.Value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();
                    var guest = db.Guests.Join(db.GuestContactInfos, x => x.Id, y => y.GuestId, (x, y) => new { Guest = x, GuestContactInfo = y })
                          .Where(x => formats.Contains(x.GuestContactInfo.Value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();
                    var vendor = db.Vendors.Join(db.VendorContactNumbers, x => x.Id, y => y.VendorId, (x, y) => new { Vendor = x, VendorContactNumber = y })
                          .Where(x => formats.Contains(x.VendorContactNumber.Value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();

                    var renter = db.Renters.Join(db.RenterContacts, x => x.Id, y => y.RenterId, (x, y) => new { Renter = x, RenterContact = y })
                          .Where(x => formats.Contains(x.RenterContact.Contact.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""))).FirstOrDefault();
                    //var renter = db.Renters.Where(x => formats.Contains(x.PhoneNumber)).FirstOrDefault();
                    
                    if (renter != null)
                    {
                        //var u = db.Users.Where(x => x.WorkerId == worker.Worker.Id).FirstOrDefault();
                        var thread = db.CommunicationThreads.Where(x => x.RenterId == renter.Renter.Id && x.CompanyId == twillioNumber.CompanyId).FirstOrDefault();
                        CommunicationThread workerThread;
                        if (thread == null)
                        {
                            workerThread = new CommunicationThread();
                            workerThread.WorkerId = renter.Renter.Id;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = SessionHandler.CompanyId;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                        else
                        {
                            workerThread = db.CommunicationThreads.Where(x => x.ThreadId == thread.ThreadId).FirstOrDefault();
                            workerThread.IsEnd = false;
                            db.Entry(workerThread).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        CommunicationSMS CommunicaationSMS = new CommunicationSMS
                        {
                            ThreadId = workerThread.ThreadId,
                            Message = request.Body,
                            From = request.From,
                            To = request.To,
                            MessageSid = request.SmsSid,
                            CreatedDate = DateTime.Now,

                        };
                        db.CommunicationSMS.Add(CommunicaationSMS);
                        db.SaveChanges();
                        Core.Helper.Company.Communication.ChargeUsage(CommunicaationSMS.Id, true);
                        foreach (var filePath in MMSImages)
                        {
                            Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = CommunicaationSMS.Id });
                        }
                        //Core.SignalR.Hubs.InboxHub.WorkerMessageReceived(workerThread.ThreadId);
                    }
                    else if (worker != null && worker.Worker != null)
                    {
                        var u = db.Users.Where(x => x.WorkerId == worker.Worker.Id).FirstOrDefault();
                        var thread = db.CommunicationThreads.Where(x => x.WorkerId == worker.Worker.Id && x.CompanyId == twillioNumber.CompanyId).FirstOrDefault();
                        CommunicationThread workerThread;
                        if (thread == null)
                        {
                            workerThread = new CommunicationThread();
                            workerThread.WorkerId = worker.Worker.Id;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = SessionHandler.CompanyId;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                        else
                        {
                            workerThread = db.CommunicationThreads.Where(x => x.ThreadId == thread.ThreadId).FirstOrDefault();
                            workerThread.IsEnd = false;
                            db.Entry(workerThread).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        CommunicationSMS CommunicaationSMS = new CommunicationSMS
                        {
                            ThreadId = workerThread.ThreadId,
                            Message = request.Body,
                            From = request.From,
                            To = request.To,
                            UserId = u.Id,
                            MessageSid = request.SmsSid,
                            CreatedDate = DateTime.Now,

                        };
                        db.CommunicationSMS.Add(CommunicaationSMS);
                        db.SaveChanges();
                        Core.Helper.Company.Communication.ChargeUsage(CommunicaationSMS.Id, true);
                        foreach (var filePath in MMSImages)
                        {
                            Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = CommunicaationSMS.Id });
                        }
                        Core.SignalR.Hubs.InboxHub.WorkerMessageReceived(workerThread.ThreadId);

                    }
                    else if (vendor!=null && vendor.Vendor!=null)
                    {
                        //var u = db.Users.Where(x => x.WorkerId == worker.Worker.Id).FirstOrDefault();
                        var thread = db.CommunicationThreads.Where(x => x.VendorId == vendor.Vendor.Id && x.CompanyId == twillioNumber.CompanyId).FirstOrDefault();
                        CommunicationThread vendorThread;
                        if (thread == null)
                        {
                            vendorThread = new CommunicationThread();
                            vendorThread.VendorId = vendor.Vendor.Id;
                            vendorThread.ThreadId = PasswordGenerator.Generate(20);
                            vendorThread.CompanyId = SessionHandler.CompanyId;
                            db.CommunicationThreads.Add(vendorThread);
                            db.SaveChanges();
                        }
                        else
                        {
                            vendorThread = db.CommunicationThreads.Where(x => x.ThreadId == thread.ThreadId).FirstOrDefault();
                            vendorThread.IsEnd = false;
                            db.Entry(vendorThread).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        CommunicationSMS CommunicaationSMS = new CommunicationSMS
                        {
                            ThreadId =vendorThread.ThreadId,
                            Message = request.Body,
                            From = request.From,
                            To = request.To,
                            //UserId = u.Id,
                            MessageSid = request.SmsSid,
                            CreatedDate = DateTime.Now,

                        };
                        db.CommunicationSMS.Add(CommunicaationSMS);
                        db.SaveChanges();
                        Core.Helper.Company.Communication.ChargeUsage(CommunicaationSMS.Id, true);
                        foreach (var filePath in MMSImages)
                        {
                            Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = CommunicaationSMS.Id });
                        }
                        Core.SignalR.Hubs.InboxHub.WorkerMessageReceived(vendorThread.ThreadId);
                    }
                    else
                    {
                        SMS sms = new SMS
                        {
                            Body = request.Body,
                            From = request.From,
                            To = request.To,
                            FromCountry = request.FromCountry,
                            MessageSid = request.SmsSid,
                            CreatedDate = DateTime.Now,
                            IsMyMessage = false
                        };
                        if (guest != null && guest.Guest != null)
                        {
                            sms.GuestId = guest.Guest.Id;
                            var inbox = db.Inbox.Where(x => x.GuestId == guest.Guest.Id).FirstOrDefault();
                            inbox.HasUnread = false;
                            IMessageRepository messageRepo = new MessageRepository();
                            InboxHub.MessageReceived(User.Identity.Name, messageRepo.GetNewInbox());
                            
                        }

                        db.SMS.Add(sms);
                        db.SaveChanges();
                        foreach (var filePath in MMSImages)
                        {
                            Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, GuestSMSId= sms.Id });
                        }
                    }
                }
            }
            catch { }

            return TwiML(messagingResponse);
        }
        private string GetMediaFileName(string mediaUrl,
         string contentType)
        {
            return Path.GetFileName(mediaUrl) +GetDefaultExtension(contentType);
        }
        public void SaveMMS(string url,string saveFile)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(url, saveFile);
            }
        }

        public static string GetDefaultExtension(string mimeType)
        {
            // NOTE: This implementation is Windows specific (uses Registry)
            // Platform independent way might be to download a known list of
            // mime type mappings like: http://bit.ly/2gJYKO0
            var key = Registry.ClassesRoot.OpenSubKey(
                @"MIME\Database\Content Type\" + mimeType, false);
            var ext = key?.GetValue("Extension", null)?.ToString();
            return ext ?? "application/octet-stream";
        }
        [HttpPost]
        public ActionResult Send(string to, string message, long? guestId, int? type, HttpPostedFileWrapper[] Images)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var contextUrl = System.Web.HttpContext.Current.Request.Url;
                    string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                    var mediaUrl = new List<Uri>();
                    List<string> filesPath = new List<string>();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            var path = Files.Upload(image, "~/Images/MMS/"); //UploadImage(image, "~/Images/MMS/");
                            filesPath.Add(path);
                            mediaUrl.Add(new Uri(Url + path));
                        }
                    }
                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                    string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                    TwilioClient.Init(accountSid, authToken);

                    int userId = User.Identity.Name.ToInt();
                    var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                    string phoneNumber = "";

                    if (type == 2)
                    {
                        phoneNumber = "whatsapp:+14155238886"; // Twilio WhatsApp Phone Number
                    } else if (type == 3)
                    {
                        phoneNumber = "messenger:539592003176920"; // Messenger
                    }
                    else
                    {
                        if (user != null)
                        {
                            var tempInq = db.Inquiries.Where(x => x.GuestId == guestId).FirstOrDefault();
                            var tempProperty = db.Properties.Where(x => x.ListingId == tempInq.PropertyId).FirstOrDefault();

                            var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId && x.Id==tempProperty.TwilioNumberId).FirstOrDefault();
                            if (twilioNumber != null)
                            {
                                phoneNumber = twilioNumber.PhoneNumber;
                            }
                            else
                            {
                                var host = db.Hosts.Where(x => x.Id == tempProperty.HostId).FirstOrDefault();
                                twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId && x.Id == host.TwilioNumberId).FirstOrDefault();

                                if (twilioNumber != null)
                                {
                                    phoneNumber = twilioNumber.PhoneNumber;
                                }
                            }

                        }

                        if (string.IsNullOrEmpty(phoneNumber))
                        {
                            return Json(new { success = false, message = "You have no registered Number" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    var smsMessage = MessageResource.Create(
                        from: new Twilio.Types.PhoneNumber(phoneNumber),
                        to: new Twilio.Types.PhoneNumber(to),
                        body: message,
                        mediaUrl:mediaUrl
                        
                    );

                    SMS sms = new SMS
                    {
                        Body = message,
                        From = phoneNumber,
                        To = to,
                        MessageSid = smsMessage.Sid,
                        CreatedDate = DateTime.Now,
                        UserId=User.Identity.Name.ToInt(),
                        IsMyMessage = true
                    };

                    if (guestId == null)
                    {
                        var guest = db.Guests.Join(db.GuestContactInfos, x => x.Id, y => y.GuestId, (x, y) => new { Guest = x, GuestContactInfo = y })
                            .Where(x => x.GuestContactInfo.Value == sms.From || x.Guest.ContactNumber == sms.From).FirstOrDefault();
                        if (guest != null && guest.Guest != null)
                        {
                            sms.GuestId = guest.Guest.Id;
                        }
                    } else
                    {
                        sms.GuestId = guestId.Value;
                    }

                    db.SMS.Add(sms);
                    foreach (var filePath in filesPath)
                    {
                        Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, GuestSMSId = sms.Id });
                    }
                    db.SaveChanges();
                    Core.Helper.Company.Communication.ChargeUsage(sms.Id, true);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            var messagingResponse = new MessagingResponse();

            return Json(new { success = true, message = "Message Sent" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CommunicationSend(string message, string threadId,string to, int? type, HttpPostedFileWrapper[] Images)
        {
            CommunicationThread thread;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var contextUrl = System.Web.HttpContext.Current.Request.Url;
                    string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                    var mediaUrl = new List<Uri>();
                    List<string> filesPath = new List<string>();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            var path = Files.Upload(image, "~/Images/MMS/");//UploadImage(image, "~/Images/MMS/");
                            filesPath.Add(path);
                            mediaUrl.Add(new Uri(Url + path));
                        }
                    }
                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                    string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                    TwilioClient.Init(accountSid, authToken);

                    int userId = User.Identity.Name.ToInt();
                    var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                    string phoneNumber = null;

                    if (type == 2)
                    {
                        phoneNumber = "whatsapp:+14155238886"; // Twilio WhatsApp Phone Number
                    }
                    else if (type == 3)
                    {
                        phoneNumber = "messenger:278080232837381"; // Messenger
                    }
                    else
                    {
                        if (user != null)
                        {
                            var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId).FirstOrDefault();
                            if (twilioNumber != null)
                            {
                                phoneNumber = twilioNumber.PhoneNumber;
                            }
                        }

                        if (string.IsNullOrEmpty(phoneNumber))
                        {
                            return Json(new { success = false, message = "You have no registered Number" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();

                   var smsMessage = MessageResource.Create(
                        from: new Twilio.Types.PhoneNumber(phoneNumber),
                        to: new Twilio.Types.PhoneNumber(to),
                        body: message,
                        mediaUrl: mediaUrl
                    );


                    CommunicationSMS sms = new CommunicationSMS
                    {
                        ThreadId = thread.ThreadId,
                        Message = message,
                        From = phoneNumber,
                        To = to,
                        CreatedDate = DateTime.Now,
                        UserId = User.Identity.Name.ToInt()

                    };
                    db.CommunicationSMS.Add(sms);
                    db.SaveChanges();
                    Core.Helper.Company.Communication.ChargeUsage(sms.Id, true);
                    foreach (var filePath in filesPath)
                    {
                        Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = sms.Id });
                    }
                    //InboxHub.WorkerMessageReceived(workerThread.ThreadId, isworker);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            var messagingResponse = new MessagingResponse();

            return Json(new { success = true, message = "Message Sent", threadId = thread.ThreadId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RenterSend(string to, string message, string threadId,int renterId,int ?type, HttpPostedFileWrapper[] Images)
        {
            CommunicationThread thread;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var contextUrl = System.Web.HttpContext.Current.Request.Url;
                    string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                    var mediaUrl = new List<Uri>();
                    List<string> filesPath = new List<string>();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            var path = Files.Upload(image, "~/Images/MMS/");//UploadImage(image, "~/Images/MMS/");
                            filesPath.Add(path);
                            mediaUrl.Add(new Uri(Url + path));
                        }
                    }
                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                    string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                    TwilioClient.Init(accountSid, authToken);

                    int userId=User.Identity.Name.ToInt();
                    var user = db.Users.Where(x=>x.Id==userId).FirstOrDefault();
                    string phoneNumber = null;

                    if (type == 2)
                    {
                        phoneNumber = "whatsapp:+14155238886"; // Twilio WhatsApp Phone Number
                    }
                    else if (type == 3)
                    {
                        phoneNumber = "messenger:278080232837381"; // Messenger
                    }
                    else
                    {
                        if (user != null)
                        {
                            var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId).FirstOrDefault();
                            if (twilioNumber != null)
                            {
                                phoneNumber = twilioNumber.PhoneNumber;
                            }
                        }

                        if (string.IsNullOrEmpty(phoneNumber))
                        {
                            return Json(new { success = false, message = "You have no registered Number" }, JsonRequestBehavior.AllowGet);
                        }
                    }


                    if (threadId == "0")
                    {
                        thread = new CommunicationThread();
                        thread.RenterId = renterId;
                        thread.ThreadId = PasswordGenerator.Generate(20);
                        thread.CompanyId = SessionHandler.CompanyId;
                        thread.IsRenter = true;
                        db.CommunicationThreads.Add(thread);
                        db.SaveChanges();
                    }
                    else
                    {
                        thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                    }
                    var smsMessage = MessageResource.Create(
                        from: new Twilio.Types.PhoneNumber(phoneNumber),
                        to: new Twilio.Types.PhoneNumber(to),
                        body: message,
                        mediaUrl:mediaUrl
                    );


                    CommunicationSMS sms = new CommunicationSMS
                    {
                        ThreadId = thread.ThreadId,
                        Message = message,
                        From = phoneNumber,
                        To = to,
                        CreatedDate = DateTime.Now,
                        UserId = User.Identity.Name.ToInt()

                    };
                    db.CommunicationSMS.Add(sms);
                    db.SaveChanges();
                    Core.Helper.Company.Communication.ChargeUsage(sms.Id, true);
                    foreach (var filePath in filesPath)
                    {
                        Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = sms.Id });
                    }
                    //InboxHub.WorkerMessageReceived(workerThread.ThreadId, isworker);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            var messagingResponse = new MessagingResponse();

            return Json(new { success = true, message = "Message Sent", threadId = thread.ThreadId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult WorkerSend(string to, string message,string threadId, int workerId, int? type,bool isworker, HttpPostedFileBase[] Images)
        {
            CommunicationThread workerThread;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var contextUrl = System.Web.HttpContext.Current.Request.Url;
                    string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                    var mediaUrl = new List<Uri>();
                    List<string> filesPath = new List<string>();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            var path = Files.Upload(image, "~/Images/MMS/"); //UploadImage(image, "~/Images/MMS/");
                            filesPath.Add(path);
                            mediaUrl.Add(new Uri(Url + path));
                        }
                    }
                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                    string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                    TwilioClient.Init(accountSid, authToken);

                    int userId = User.Identity.Name.ToInt();
                    var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                    string phoneNumber = null;

                    if (type == 2)
                    {
                        phoneNumber = "whatsapp:+14155238886"; // Twilio WhatsApp Phone Number
                    }
                    else if (type == 3)
                    {
                        phoneNumber = "messenger:278080232837381"; // Messenger
                    }
                    else
                    {
                        if (user != null)
                        {
                            var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId).FirstOrDefault();
                            if (twilioNumber != null)
                            {
                                phoneNumber = twilioNumber.PhoneNumber;
                            }
                        }

                        if (string.IsNullOrEmpty(phoneNumber))
                        {
                            return Json(new { success = false, message = "You have no registered Number" }, JsonRequestBehavior.AllowGet);
                        }
                    }


                    if (threadId == "0")
                    {
                        workerThread = new CommunicationThread();
                        db.CommunicationThreads.Add(workerThread);
                        if (isworker == true)
                        {
                            workerThread.WorkerId = workerId;
                        }
                        else
                        {
                            workerThread.VendorId = workerId;
                        }
                        workerThread.ThreadId = PasswordGenerator.Generate(20);
                        workerThread.CompanyId = SessionHandler.CompanyId;
                        db.CommunicationThreads.Add(workerThread);
                        db.SaveChanges();
                    }
                    else
                    {
                        workerThread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                    }
                    var smsMessage = MessageResource.Create(
                        from: new Twilio.Types.PhoneNumber(phoneNumber),
                        to: new Twilio.Types.PhoneNumber(to),
                        body: message,
                        mediaUrl:mediaUrl
                    );


                    CommunicationSMS sms = new CommunicationSMS
                    {
                        ThreadId = workerThread.ThreadId,
                        Message = message,
                        From = phoneNumber,
                        To = to,
                        CreatedDate = DateTime.Now,
                        UserId = User.Identity.Name.ToInt()
                       
                    };
                    db.CommunicationSMS.Add(sms);
                    db.SaveChanges();
                    Core.Helper.Company.Communication.ChargeUsage(sms.Id, true);
                    foreach (var filePath in filesPath)
                    {
                        Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = sms.Id });
                    }
                    InboxHub.WorkerMessageReceived(workerThread.ThreadId,isworker);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            var messagingResponse = new MessagingResponse();

            return Json(new { success = true, message = "Message Sent", threadId = workerThread.ThreadId }, JsonRequestBehavior.AllowGet);
        }
    }
}