﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Models.Invoice;
using MessagerSolution.Models.Notices;
using MlkPwgen;
using Spire.Doc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spire.Doc.Documents;
using MessagerSolution.Models.Invoice;
using Spire.Doc.Fields;
using Core.Enumerations;

namespace MessagerSolution.Controllers
{
    public class PaymentController : Controller
    {
        private static Airbnb.ScrapeManager _AirbnbScrapeManager = null;
        public static Airbnb.ScrapeManager AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb.ScrapeManager();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }
        private readonly Service.EmailService _emailService = new Service.EmailService();
        private readonly Service.PayPalService _payPalService = new Service.PayPalService();
        private readonly Service.SquareService _squarePaymentService = new Service.SquareService();
        private readonly Service.Stripe _stripePaymentService = new Service.Stripe();

        public ActionResult CreateInvoice(InvoiceViewModel model)
        {
            MessagerSolution.Helper.InvoiceSchedule invoiceSchedule = new MessagerSolution.Helper.InvoiceSchedule(System.Web.HttpContext.Current);
            invoiceSchedule.CreateInvoice(model);
            return Json(new { success = true, message = "Successfully added !" }, JsonRequestBehavior.AllowGet);
            //using (var db = new ApplicationDbContext())
            //{
            //    var reservation = db.Inquiries.Where(x => x.Id == model.BookingId).FirstOrDefault();
            //    var data = (from renter in db.Renters
            //                join e in db.RenterEmails on renter.Id equals e.RenterId
            //                where renter.BookingId == reservation.Id
            //                select new { Email = e, Renter = renter }).FirstOrDefault();

            //    if (string.IsNullOrEmpty(data.Email.Email))
            //    {
            //        return Json(new
            //        {
            //            success = false,
            //            message = "E-mail is required. You can't send this payment request without guest's e-mail address."
            //        },
            //            JsonRequestBehavior.AllowGet);
            //    }
            //    else
            //    {
            //        var invoice = new Invoice()
            //        {
            //            BookingId = model.BookingId,
            //            DateCreated = DateTime.Now,
            //            TotalAmount = model.RequestPayment.Sum(x => x.Amount),
            //            PaymentMethod = (int)model.PaymentMethod,
            //            RequestPaymentJsonString = model.RequestPaymentJsonString,
            //            FileUrl = CreateInvoiceFile(model.RequestPayment, model.BookingId, model.InvoiceTemplateId)
            //        };
            //        db.Invoices.Add(invoice);
            //        db.SaveChanges();

            //        var paymentModel = new Models.Invoice.InvoicePaymentRequest
            //        {
            //            MailSubject = DateTime.Now.ToString("MMMM yyyy") + "Bills",
            //            TotalAmount = model.RequestPayment.Sum(x => x.Amount),
            //            Email = data.Renter.Email,
            //            DateCreated = DateTime.Now,
            //            Renter = data.Renter.Firstname + " " + data.Renter.Lastname,
            //            InvoiceId = invoice.Id,
            //            PaymentMethod = model.PaymentMethod

            //        };
            //        SendInvoiceEmail(paymentModel, EmailTemplateReplaceValue(model.RequestPayment, model.BookingId, model.EmailTemplateId), invoice.FileUrl);

            //        return Json(new { success = true, message = "Successfully added !" }, JsonRequestBehavior.AllowGet);

            //    }

            //}
        }
        //Get the description of bill in summary
        public string GetRequestBillDescription(List<RequestAmountViewModel> RequestPayment)
        {
            var today = DateTime.Now;
            var bill = "";
            var previous = 0M;
            var current = 0M;
            bill += "Previous Bill <br>";
            RequestPayment.Where(x => x.DueDate < today).Select(x => x.DueDate.ToString("MMM dd,yyyy") + " " + x.Description + " " + x.Amount + "<br>").ToList().ForEach(x => bill += x);
            previous = RequestPayment.Where(x => x.DueDate < today).Sum(x => x.Amount);
            bill += "<div style=\"border: thin solid gray; width: 300px;\"></div>" +
                 "<b>Total: " + previous + "</b>";
            bill += "Current Bill <br>";
            RequestPayment.Where(x => x.DueDate > today).Select(x => x.DueDate.ToString("MMM dd,yyyy") + " - " + x.Description + " - " + x.Amount + "<br>").ToList().ForEach(x => bill += x);
            current = RequestPayment.Where(x => x.DueDate > today).Sum(x => x.Amount);
            bill += "<div style=\"border: thin solid gray; width: 300px;\"></div>" +
                  "<b>Total: " + current + "</b></b>";

            bill += "<div style=\"border: thin solid black; width: 300px;\"></div>" +
                  "<b>TOTAL: " + RequestPayment.Sum(x => x.Amount) + "</b>";
            return bill;
        }

        public string EmailTemplateReplaceValue(List<RequestAmountViewModel> RequestPayment, int bookingId, int templateId)
        {
            using (var db = new ApplicationDbContext())
            {
                var reservation = db.Inquiries.Where(x => x.Id == bookingId).FirstOrDefault();
                var renter = db.Renters.Where(x => x.BookingId == bookingId).FirstOrDefault();
                var property = db.Properties.Where(x => reservation.PropertyId == x.ListingId).FirstOrDefault();
                var company = db.Companies.Where(x => x.Id == SessionHandler.CompanyId).FirstOrDefault();
                var template = db.EmailTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == templateId).FirstOrDefault().Description;

                template = template.Replace("[Renter Lastname]", renter.Lastname)
                 .Replace("[Transaction Details]", GetRequestBillDescription(RequestPayment))
                 .Replace("[Renter Firstname]", renter.Firstname)
                 .Replace("[Renter Phone]", renter.PhoneNumber)
                 .Replace("[Renter Address]", property.Address)
                 .Replace("[Property Address]", property.Address)
                 .Replace("[Company]", company.CompanyName)
                 .Replace("[Total Amount]", RequestPayment.Sum(x => x.Amount).ToString())
                 .Replace("[Date]", DateTime.Now.ToString("MMMM dd,yyyy"));
                return template;
            }

        }
        public void SendInvoiceEmail(Models.Invoice.InvoicePaymentRequest data, string template, string attachment)
        {
            //if (!System.IO.File.Exists(path)) return;
            //var template = System.IO.File.ReadAllText(path);

            var callbackUrl = "";

            if (data.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Paypal)
            {
                callbackUrl = Url.Action("Paypal", "Payment", new { invoiceId = data.InvoiceId }, protocol: Request.Url.Scheme);
            }
            else if (data.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Square)
            {
                callbackUrl = Url.Action("Square", "Payment", new { invoiceId = data.InvoiceId }, protocol: Request.Url.Scheme);
            }
            else if (data.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Stripe)
            {
                callbackUrl = Url.Action("Stripe", "Payment", new { invoiceId = data.InvoiceId }, protocol: Request.Url.Scheme);
            }
            //template = template.Replace("@Name", data.Renter)
            //        .Replace("@Description", data.Description)
            //        .Replace("@DateCreated", data.DateCreated.ToString("MMMM dd,yyyy"))
            //        .Replace("@TotalAmount", data.TotalAmount.ToString())
            //        .Replace("@Link", callbackUrl);

            template += "<br/>" +
             "LinK: <a href=\"" + callbackUrl + "\">Click Here</a><br />" +
             "<br/>";


            var success = _emailService.SendEmail(data.MailSubject, template, data.Email, Server.MapPath("~" + attachment));
        }
        public ActionResult Paypal(int invoiceId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    //if there is income batch payment the payment is complete
                    var temp = db.IncomeBatchPayments.Where(x => x.InvoiceId == invoiceId).FirstOrDefault();
                    if (temp != null)
                    {
                        return View("PaymentSuccess");
                    }
                    var apiContext = _payPalService.GetApiContext(invoiceId);
                    var payerId = Request.Params["PayerID"];
                    var invoice = db.Invoices.Where(x => x.Id == invoiceId).FirstOrDefault();
                    var reservation = db.Inquiries.Where(x => x.Id == invoice.BookingId).FirstOrDefault();
                    var data = (from renter in db.Renters
                                join e in db.RenterEmails on renter.Id equals e.RenterId
                                where renter.BookingId == reservation.Id
                                select new { Email = e, Renter = renter }).FirstOrDefault();
                    //Mark as initiated meaning the user click our link
                    invoice.Status = 1;
                    db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    if (!string.IsNullOrEmpty(payerId))
                    {
                        var paymentId = Request.Params["paymentId"];

                        var executedPayment = _payPalService.ExecutePayment(apiContext, payerId, paymentId);
                        executedPayment.state = "approved";

                        if (executedPayment.state.ToLower() != "approved")
                        {
                            return View("PaymentFail");
                        }
                        else if (!string.IsNullOrEmpty(invoice.PaymentUrl))
                        {
                            return Redirect(invoice.PaymentUrl);
                        }
                        else
                        {
                            //When payment is complete
                            //Core.API.Booking.com.Inquiries.SettlePaymentRequest(_confirmationCode, _paymentHistoryId.ToLong());
                            //Core.API.Booking.com.Inquiries.UpdateIfPaid(_confirmationCode);
                        }
                    }
                    //else if (!string.IsNullOrEmpty(paymentRequest.PaypalRedirectUrl))
                    //{
                    //    return Redirect(paymentRequest.PaypalRedirectUrl);
                    //}
                    else
                    {
                        Session["InvoiceId"] = invoiceId;

                        var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority +
                                    "/Paypal/PaymentPaypal?";

                        var guid = Convert.ToString((new System.Random()).Next(100000));

                       

                        var paymentModel = new Models.Invoice.InvoicePaymentRequest
                        {
                            MailSubject = "Bills",
                            TotalAmount = invoice.TotalAmount,
                            Email = data.Renter.Email,
                            DateCreated = DateTime.Now,
                            Renter = data.Renter.Firstname + " " + data.Renter.Lastname,
                        };

                        var createdPayment = _payPalService.CreateFullPayment(apiContext, baseUri + "guid=" + guid + "&invoiceId=" + invoiceId, paymentModel);

                        var links = createdPayment.links.GetEnumerator();

                        string paypalRedirectUrl = null;

                        while (links.MoveNext())
                        {
                            var lnk = links.Current;

                            if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                            {
                                invoice.Status = 2;
                                db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                CreateBatchIncomeFromInvoice(invoice);
                                paypalRedirectUrl = lnk.href;
                            }
                        }
                        invoice.PaymentUrl = paypalRedirectUrl;
                        db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        Session.Add(guid, createdPayment.id);
                        return Redirect(paypalRedirectUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                return View("PaymentFail");
            }

            return View("PaymentSuccess");
        }

        public ActionResult Stripe(int invoiceId)
        {
            using (var db = new ApplicationDbContext())
            {//if there is income batch payment the payment is complete
                //var temp = db.IncomeBatchPayments.Where(x => x.InvoiceId == invoiceId).FirstOrDefault();
                //if (temp != null)
                //{
                //    return View("PaymentSuccess");
                //}
                var invoice = db.Invoices.Where(x => x.Id == invoiceId).FirstOrDefault();
                var reservation = db.Inquiries.Where(x => x.Id == invoice.BookingId).FirstOrDefault();
                var data = (from renter in db.Renters
                            join e in db.RenterEmails on renter.Id equals e.RenterId
                            where renter.BookingId == reservation.Id
                            select new { Email = e, Renter = renter }).FirstOrDefault();
                //Mark as initiated meaning the user click our link
                invoice.Status = 1;
                db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
               
                var paymentModel = new Models.Invoice.InvoicePaymentRequest
                {
                    InvoiceId = invoiceId,
                    MailSubject = "Bills",
                    TotalAmount = invoice.TotalAmount,
                    Email = data.Renter.Email,
                    DateCreated = DateTime.Now,
                    Renter = data.Renter.Firstname + " " + data.Renter.Lastname,
                };

                return View(paymentModel);
            }
        }

        //This method handles the payment integration to stripe from the page
        [HttpPost]
        public ActionResult Stripe(string stripeEmail, string stripeToken,
            int invoiceId)
        {
            using (var db = new ApplicationDbContext())
            {
                var invoice = db.Invoices.Where(x => x.Id == invoiceId).FirstOrDefault();
                var reservation = db.Inquiries.Where(x => x.Id == invoice.BookingId).FirstOrDefault();
                var data = (from renter in db.Renters
                            join e in db.RenterEmails on renter.Id equals e.RenterId
                            where renter.BookingId == reservation.Id
                            select new { Email = e, Renter = renter }).FirstOrDefault();
                var description = "";
                var total = 0.0M;
            
                var paymentModel = new Models.Invoice.InvoicePaymentRequest
                {
                    MailSubject = "Bills",
                    TotalAmount = invoice.TotalAmount,
                    Email = data.Renter.Email,
                    DateCreated = DateTime.Now,
                    Renter = data.Renter.Firstname + " " + data.Renter.Lastname,
                };
                var charge = _stripePaymentService.CreatePayment(stripeToken, stripeEmail, invoice);
                if (charge.Status.Equals("succeeded"))
                {
                    invoice.Status = 2;
                    invoice.SitePaymentId = charge.Id;
                    db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    CreateBatchIncomeFromInvoice(invoice);
                    return View("PaymentSuccess");
                }
                else
                {
                    return View("PaymentFail");
                }
            }
        }


        public ActionResult Square(int invoiceId)
        {
            using (var db = new ApplicationDbContext())
            {//if there is income batch payment the payment is complete
                var temp = db.IncomeBatchPayments.Where(x => x.InvoiceId == invoiceId).FirstOrDefault();
                if (temp != null)
                {
                    return View("PaymentSuccess");
                }
                var invoice = db.Invoices.Where(x => x.Id == invoiceId).FirstOrDefault();
                var reservation = db.Inquiries.Where(x => x.Id == invoice.BookingId).FirstOrDefault();
                var data = (from renter in db.Renters
                            join e in db.RenterEmails on renter.Id equals e.RenterId
                            where renter.BookingId == reservation.Id
                            select new { Email = e, Renter = renter }).FirstOrDefault();
                //Mark as initiated meaning the user click our link
                invoice.Status = 1;
                db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            
                var paymentModel = new Models.Invoice.InvoicePaymentRequest
                {
                    InvoiceId = invoiceId,
                    MailSubject = "Bills",
                    TotalAmount = invoice.TotalAmount,
                    Email = data.Renter.Email,
                    DateCreated = DateTime.Now,
                    Renter = data.Renter.Firstname + " " + data.Renter.Lastname,
                };


                return View(paymentModel);
            }
        }

    
        //This method handles the payment integration to square from the page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Square(int invoiceId, string CreditCardNonce)
        {

            using (var db = new ApplicationDbContext())
            {
                var invoice = db.Invoices.Where(x => x.Id == invoiceId).FirstOrDefault();
                var reservation = db.Inquiries.Where(x => x.Id == invoice.BookingId).FirstOrDefault();
                var data = (from renter in db.Renters
                            join e in db.RenterEmails on renter.Id equals e.RenterId
                            where renter.BookingId == reservation.Id
                            select new { Email = e, Renter = renter }).FirstOrDefault();
                var description = "";
                var total = 0.0M;
                foreach (var req in invoice.RequestPayment)
                {
                    description += req.DueDate.ToString("MMM dd,yyyy") + " - " + req.Description + " - " + req.Amount + "<br>";
                    total += req.Amount;
                }
                var amountDue = Math.Round(Convert.ToDecimal(invoice.TotalAmount), 2);
                var response = _squarePaymentService.ChargeCard(CreditCardNonce,
                    Convert.ToInt64(amountDue.ToString().Replace(".", "")),
                    "USD", invoice.Id);

                if (response != null)
                {
                    invoice.Status = 2;
                    invoice.SitePaymentId = response.Payment.Id;
                    db.Entry(invoice).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    CreateBatchIncomeFromInvoice(invoice);

                    return View("PaymentSuccess");
                }
                else
                {
                    return View("PaymentFail");
                }
            }
        }

        private string CreateTransactionDetails(List<RequestAmountViewModel> RequestPayment)
        {
            var today = DateTime.Now;
            var details = "";
            details += "<tr><td colspan=\"3\">Previous bill</td></tr>";
            foreach (var req in RequestPayment.Where(x => x.DueDate < today))
            {
                details += "<tr>" +
                                "<td style=\"text-align:center\">" + req.Description + "</td>" +
                                "<td style=\"text-align:center\">" + req.DueDate.ToString("MMM dd,yyyy") + "</td>" +
                                "<td style=\"text-align:center\">" + req.Amount + "</td>" +
                            "</tr>";
            }
            details += "<tr><td colspan=\"3\">Current Bill</td></tr>";
            foreach (var req in RequestPayment.Where(x => x.DueDate > today))
            {
                details += "<tr>" +
                                "<td style=\"text-align:center\">" + req.Description + "</td>" +
                                "<td style=\"text-align:center\">" + req.DueDate.ToString("MMM dd,yyyy") + "</td>" +
                                "<td style=\"text-align:center\">" + req.Amount + "</td>" +
                            "</tr>";
            }

            return details;
        }
        private static void ReplaceObj(TextRangeLocation location, List<DocumentObject> replacement)
        {
            //will be replaced
            TextRange textRange = location.Text;

            //textRange index
            int index = location.Index;

            //owener paragraph
            Paragraph paragraph = location.Owner;

            //owner text body
            Body sectionBody = paragraph.OwnerTextBody;

            //get the index of paragraph in section
            int paragraphIndex = sectionBody.ChildObjects.IndexOf(paragraph);

            int replacementIndex = -1;
            if (index == 0)
            {
                //remove
                paragraph.ChildObjects.RemoveAt(0);

                replacementIndex = sectionBody.ChildObjects.IndexOf(paragraph);
            }
            else if (index == paragraph.ChildObjects.Count - 1)
            {
                paragraph.ChildObjects.RemoveAt(index);
                replacementIndex = paragraphIndex + 1;

            }
            else
            {
                //split owner paragraph
                Paragraph paragraph1 = (Paragraph)paragraph.Clone();
                while (paragraph.ChildObjects.Count > index)
                {
                    paragraph.ChildObjects.RemoveAt(index);
                }
                int i = 0;
                int count = index + 1;
                while (i < count)
                {
                    paragraph1.ChildObjects.RemoveAt(0);
                    i += 1;
                }
                sectionBody.ChildObjects.Insert(paragraphIndex + 1, paragraph1);

                replacementIndex = paragraphIndex + 1;
            }

            //insert replacement
            for (int i = 0; i <= replacement.Count - 1; i++)
            {
                sectionBody.ChildObjects.Insert(replacementIndex + i, replacement[i].Clone());
            }
        }
        public string CreateInvoiceFile(List<RequestAmountViewModel> RequestPayment, int bookingId, int templateId)
        {
            using (var db = new ApplicationDbContext())
            {
                var reservation = db.Inquiries.Where(x => x.Id == bookingId).FirstOrDefault();
                var renter = db.Renters.Where(x => x.BookingId == bookingId).FirstOrDefault();
                var property = db.Properties.Where(x => reservation.PropertyId == x.ListingId).FirstOrDefault();
                var company = db.Companies.Where(x => x.Id == SessionHandler.CompanyId).FirstOrDefault();
                var template = db.InvoiceTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == templateId).FirstOrDefault();
                var file = "/Documents/Invoice/" + PasswordGenerator.Generate(10) + ".pdf";
                var table = "<table style=\"width:100%;border-collapse: collapse;\">" +
                            "<thead>" +
                                "<tr>" +
                                    "<th>Description</th>" +
                                    "<th>Date</th>" +
                                    "<th>Amount</th>" +
                                "</tr>" +
                            "</thead>" +
                            "<tbody>" +
                              CreateTransactionDetails(RequestPayment) +
                            "</tbody>" +
                        "</table>";
                var total = 0.0M;
                foreach (var req in RequestPayment)
                {
                    total += req.Amount;
                }
                var newFile = Server.MapPath("~" + file);
                var fileLocation = Server.MapPath("~" + template.FileUrl);
                Document word = new Document(fileLocation);
                //var selection = word.FindString("[Transaction Details]",true,true);
                Section tempSection = word.AddSection();
                Paragraph p4 = tempSection.AddParagraph();
                p4.AppendHTML(table);
                List<DocumentObject> replacement = new List<DocumentObject>();
                foreach (var obj in tempSection.Body.ChildObjects)
                {
                    DocumentObject O = obj as DocumentObject;
                    replacement.Add(O);
                }
                TextSelection[] selections = word.FindAllString("[Transaction Details]", false, true);
                List<TextRangeLocation> locations = new List<TextRangeLocation>();
                foreach (TextSelection selection in selections)
                {
                    locations.Add(new TextRangeLocation(selection.GetAsOneRange()));
                }
                locations.Sort();
                foreach (TextRangeLocation location in locations)
                {
                    ReplaceObj(location, replacement);
                }
                //remove the temp section
                word.Sections.Remove(tempSection);
                word.Replace("[Renter Lastname]", renter.Lastname, true, true);
                word.Replace("[Renter Firstname]", renter.Firstname, true, true);
                word.Replace("[Property Address]", string.IsNullOrWhiteSpace(property.Address) ? "[Property address]" : property.Address, true, true);
                word.Replace("[Date]", DateTime.Now.ToString("MMMM dd,yyyy"), true, true);
                //word.Replace("[Transaction Details]", table, true, true);
                word.Replace("[Total Amount]", total.ToString("F"), true, true);
                word.Replace("[Company]", company.CompanyName, true, true);
                word.Replace("[Renter Address]", property.Address, true, true);
                word.Replace("[Renter Phone]", renter.PhoneNumber, true, true);
                word.HTMLTrackChanges = true;
                word.SaveToFile(Server.MapPath("~" + file));
                return file;
            }

        }

        public void CreateBatchIncomeFromInvoice(Invoice invoice)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = (from reservation in db.Inquiries join property in db.Properties on reservation.PropertyId equals property.ListingId where reservation.Id == invoice.BookingId select property.CompanyId).FirstOrDefault();
                var batchPayment = new IncomeBatchPayment()
                {
                    Amount = invoice.TotalAmount,
                    Description = "Payment through " + invoice.PaymentMethodName,
                    BookingId = invoice.BookingId,
                    PaymentDate = DateTime.UtcNow,
                    CompanyId = companyId,
                    InvoiceId = invoice.Id
                };
                db.IncomeBatchPayments.Add(batchPayment);
                db.SaveChanges();

                foreach (var item in invoice.RequestPayment)
                {

                    IncomePayment objIncomePayment = new IncomePayment();
                    objIncomePayment.Amount = item.Amount;
                    objIncomePayment.CreatedAt = DateTime.UtcNow;
                    objIncomePayment.Description = batchPayment.Description;
                    objIncomePayment.Type = (int)IncomePaymentType.Payment;
                    objIncomePayment.IncomeId = item.IncomeId;
                    objIncomePayment.IncomeBatchPaymentId = batchPayment.Id;
                    db.IncomePayment.Add(objIncomePayment);
                    db.SaveChanges();
                }
            }
        }

        [HttpGet]
        public JsonResult GetTemplates()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<InvoiceTemplateViewModel>();
                var documents = db.InvoiceTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new { success = true, documents }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetInvoiceTemplates()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<InvoiceTemplateViewModel>();
                var documents = db.InvoiceTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach (var document in documents)
                {
                    list.Add(new InvoiceTemplateViewModel()
                    {
                        Id = document.Id,
                        Name = document.Name,
                        Actions =
                                        "<button class=\"ui mini button edit-invoice-template\" data-tooltip=\"Edit\">" +
                                        "<i class=\"icon edit outline\"></i></button>"
                                        + "<a href=\"" + document.FileUrl + "\"><button class=\"ui mini button\" data-tooltip=\"Download\">" +
                                        "<i class=\"icon download\"></i></button></a>" +
                                        "<button class=\"ui mini button delete-invoice-template\" data-tooltip=\"Delete\">" +
                                        "<i class=\"icon trash\"></i></button>"
                    });
                }
                return Json(new { success = true, data = list }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CreateInvoiceTemplate(CreateTemplateModel Template, HttpPostedFileWrapper[] Files)
        {

            if (Files != null)
            {
                foreach (var File in Files)
                {
                    if (File != null)
                    {
                        var fileName = Path.GetFileName(File.FileName);
                        string url = "~/Documents/InvoiceTemplates/" + fileName;
                        File.SaveAs(Server.MapPath(url));
                        url = url.Replace("~", "");
                        using (var db = new ApplicationDbContext())
                        {
                            db.InvoiceTemplates.Add(new InvoiceTemplate() { Name = Path.GetFileNameWithoutExtension(File.FileName), FileUrl = url, CompanyId = SessionHandler.CompanyId });
                            db.SaveChanges();

                        }
                    }
                }
            }
            using (var db = new ApplicationDbContext())
            {
                var temp = db.InvoiceTemplates.Where(x => x.Id == Template.Id).FirstOrDefault();
                if (Template.DocumentName != "" && Template.DocumentName != null)
                {

                    var file = "";
                    var path = "";
                    Document doc = new Document();
                    file = "/Documents/InvoiceTemplates/" + Template.DocumentName + ".docx";
                    path = Server.MapPath("~" + file);
                    Section s = doc.AddSection();
                    Paragraph para = s.AddParagraph();
                    para.AppendBookmarkStart("content");
                    para.AppendHTML(Template.Content.Trim());
                    para.AppendBookmarkEnd("content");
                    doc.SaveToFile(path, FileFormat.Docx);



                    if (Template.Id == 0)
                    {
                        db.InvoiceTemplates.Add(new InvoiceTemplate() { Name = Template.DocumentName, FileUrl = file, CompanyId = SessionHandler.CompanyId });
                        db.SaveChanges();
                    }
                    else
                    {

                        temp.Name = Template.DocumentName;
                        temp.FileUrl = file;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTemplateDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var template = db.InvoiceTemplates.Where(x => x.Id == id).FirstOrDefault();
                Document doc = new Document(Server.MapPath("~" + template.FileUrl));
                var filename = "/Documents/DocumentHtml/" + template.Name + ".html";
                var htmlpath = Server.MapPath("~" + filename);
                doc.SaveToFile(htmlpath);
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority + filename;
                template.Html = AirbnbScrapper.Test(url);
                return Json(new { success = true, template }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEmailTemplates()
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.EmailTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEmailTemplate(EmailTemplate template)
        {
            using (var db = new ApplicationDbContext())
            {
                if (template.Id == 0)
                {
                    template.CompanyId = SessionHandler.CompanyId;
                    db.EmailTemplates.Add(template);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.EmailTemplates.Where(x => x.Id == template.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Description = template.Description;
                        temp.Name = template.Name;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEmailTemplate(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var template = db.EmailTemplates.Where(x => x.Id == id).FirstOrDefault();
                return Json(new { template }, JsonRequestBehavior.AllowGet);
            }
        }
    }
    public class TextRangeLocation : IComparable<TextRangeLocation>
    {
        public TextRangeLocation(TextRange text)
        {
            this.Text = text;
        }
        public TextRange Text
        {
            get { return m_Text; }
            set { m_Text = value; }
        }
        private TextRange m_Text;
        public Paragraph Owner
        {
            get { return this.Text.OwnerParagraph; }
        }
        public int Index
        {
            get { return this.Owner.ChildObjects.IndexOf(this.Text); }
        }
        public int CompareTo(TextRangeLocation other)
        {
            return -(this.Index - other.Index);
        }
    }
}