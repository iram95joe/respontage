﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Models;
using Core.Repository;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using MessagerSolution.Helper.ImagesCompress;
using MessagerSolution.Helper.PropertyOwner;
using MessagerSolution.MobileSources.Models;
using MessagerSolution.MobileSources.Service;
using MessagerSolution.Models;
using MessagerSolution.Models.Expense;
using MessagerSolution.Models.Income;
using MessagerSolution.Models.Properties;
using MessagerSolution.Models.Rentals;
using MlkPwgen;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;
using Message = MessagerSolution.MobileSources.Models.Message;

namespace MessagerSolution.Controllers
{
    public class MobileController : Controller
    {
        IMessageRepository messageRepo = new MessageRepository();
        ICommonRepository objIList = new CommonRepository();
        [HttpPost]
        public ActionResult Login(string username,string password)
        {
            try
            {using (var db = new ApplicationDbContext())
                {
                    //encrypting the password for security
                    password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], password.Trim(), true);
                    var user = db.Users.Where(x => x.UserName == username && x.Password == password).FirstOrDefault();
                    if (user != null)
                    {
                        return Json(new {success=true,user }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false,user= new User() }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, user = new User() }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Worker
        [HttpGet]
        public JsonResult GetWorkerList(string search,int userId)
        {
            using(var db = new ApplicationDbContext())
            {
                int companyId = db.Users.Where(x => x.Id == userId).FirstOrDefault().CompanyId;
                var workers = db.Workers.Where(x => x.CompanyId == companyId && (search=="" || search==null)?true:(x.Firstname +" "+x.Lastname).Contains(search)).ToList();
                return Json(new {workers }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult GetWorkerDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var worker = db.Workers.Where(x => x.Id==id).Select(x=>new {x.WorkerRetainers, x.WorkerContactInfos,x.WorkerJobs }).FirstOrDefault();
                //ICollection<WorkerRetainer> WorkerRetainers = worker.WorkerRetainers;
                //ICollection<WorkerContactInfo> WorkerContactInfos = worker.WorkerContactInfos;
                //ICollection<WorkerJob> WorkerJobs = worker.WorkerJobs;
                return Json(new { worker.WorkerRetainers, worker.WorkerContactInfos, worker.WorkerJobs }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveWorker(Worker worker)
        {
            using (var db = new ApplicationDbContext())
            {
                if (worker.Id == 0)
                {
                    db.Workers.Add(worker);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.Workers.Where(x => x.Id == worker.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Firstname = worker.Firstname;
                        temp.Lastname = worker.Lastname;
                        temp.Bonus = worker.Bonus;
                        temp.Mobile = worker.Mobile;
                        temp.Address = worker.Address;
                        temp.FixedRate = worker.FixedRate;
                        temp.Frequency = worker.Frequency;
                        temp.RetainerRate = worker.RetainerRate;
                        temp.RetainerFrequency = worker.RetainerFrequency;
                        temp.ImageUrl = worker.ImageUrl;
                    }
                }
                var thread = db.CommunicationThreads.Where(x => x.CompanyId == worker.CompanyId && x.WorkerId == worker.Id).FirstOrDefault();
                if (thread == null)
                {

                    CommunicationThread workerThread = new CommunicationThread();
                    db.CommunicationThreads.Add(workerThread);
                    workerThread.WorkerId = worker.Id;
                    workerThread.ThreadId = PasswordGenerator.Generate(20);
                    workerThread.CompanyId = worker.CompanyId;
                    db.CommunicationThreads.Add(workerThread);
                    db.SaveChanges();

                }
                if (worker.WorkerJobs != null)
                {
                    foreach (var job in worker.WorkerJobs)
                    {
                        job.WorkerId = worker.Id;
                        if (job.Id == 0)
                        {
                            db.WorkerJobs.Add(job);
                            db.SaveChanges();
                        }
                        else
                        {
                            var temp = db.WorkerJobs.Where(x => x.Id == job.Id).FirstOrDefault();
                            if (temp != null)
                            {
                                temp.JobTypeId = job.JobTypeId;
                                temp.PaymentRate = job.PaymentRate;
                                temp.PaymentType = job.PaymentType;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                }
                if (worker.WorkerRetainers != null)
                {
                    foreach (var retainer in worker.WorkerRetainers)
                    {
                        retainer.WorkerId = worker.Id;
                        if (retainer.Id == 0)
                        {
                            db.WorkerRetainers.Add(retainer);
                            db.SaveChanges();
                        }
                        else
                        {
                            var temp = db.WorkerRetainers.Where(x => x.Id == retainer.Id).FirstOrDefault();
                            if (temp != null)
                            {
                                temp.EffectiveDate = retainer.EffectiveDate;
                                temp.Frequency = retainer.Frequency;
                                temp.Rate = retainer.Rate;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                }

                if (worker.WorkerContactInfos != null)
                {
                    foreach (var contactInfo in worker.WorkerContactInfos)
                    {
                        contactInfo.WorkerId = worker.Id;
                        if (contactInfo.Id == 0)
                        {
                            db.WorkerContactInfos.Add(contactInfo);
                            db.SaveChanges();
                        }
                        else
                        {
                            var temp = db.WorkerContactInfos.Where(x => x.Id == contactInfo.Id).FirstOrDefault();
                            if (temp != null)
                            {
                                temp.Value = contactInfo.Value;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }

            return Json(new { workerId = worker.Id }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpPost]
        public ActionResult SaveProperty(PropertyViewModel viewModel)
        {
            int propertyId = 0;
            var companyId = viewModel.Property.CompanyId;
            using (var db = new ApplicationDbContext())
            {
                if (viewModel.Property.Id == 0)
                {
                    System.Random r = new System.Random();
                    viewModel.Property.ListingId = r.Next(100000000).ToLong();
                    db.Properties.Add(viewModel.Property);
                    db.SaveChanges();
                    propertyId = viewModel.Property.Id;
                }
                else
                {
                    var temp = db.Properties.Where(x => x.Id == viewModel.Property.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Name = viewModel.Property.Name;
                        temp.Description = viewModel.Property.Description;
                        temp.MonthlyExpense = viewModel.Property.MonthlyExpense;
                        temp.ExpenseNotes = viewModel.Property.ExpenseNotes;
                        temp.UnitNumber = viewModel.Property.UnitNumber;
                        temp.Building = viewModel.Property.Building;
                        temp.StreetNumber = viewModel.Property.StreetNumber;
                        temp.CountryId = viewModel.Property.CountryId;
                        temp.StateId = viewModel.Property.StateId;
                        temp.CityId = viewModel.Property.CityId;
                        temp.IsParentProperty = viewModel.Property.IsParentProperty;
                        temp.ParentType = viewModel.Property.ParentType;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                        propertyId = temp.Id;
                    }
                }
                //Get all child property
                var childProperties = db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId && x.CompanyId == companyId).ToList();
                if (viewModel.ChildPropertyIds != null && viewModel.ChildPropertyIds.Count > 0)
                {
                    foreach (var childId in viewModel.ChildPropertyIds)
                    {
                        childProperties.Remove(childProperties.Where(x => x.ChildPropertyId == childId).FirstOrDefault());
                        //Add parent child link
                        Core.API.Local.Properties.AddChildToParent(
                                        new ParentChildProperty()
                                        {
                                            ChildPropertyId = childId,
                                            ParentPropertyId = propertyId,
                                            ParentType = viewModel.Property.ParentType,
                                            CompanyId = companyId
                                        });
                    }
                    //Remove all properties that is not included in new child list
                    db.ParentChildProperties.RemoveRange(childProperties);
                }

            }
            return Json(new { propertyId }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetAvailableForChildProperty(int type, int propertyId,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = db.Users.Where(x=>x.Id==userId).FirstOrDefault().CompanyId;

                List<ChildPropertyModel> childproperties = new List<ChildPropertyModel>();
                var parentChildProperties = db.ParentChildProperties.Where(x => x.ParentType == type && x.ParentPropertyId == propertyId).ToList();
                foreach (var p in parentChildProperties)
                {
                    var tempProperty = db.Properties.Where(x => x.Id == p.ChildPropertyId).FirstOrDefault();
                    if (tempProperty != null)
                    {
                        var tempHost = db.Hosts.Where(x => x.Id == tempProperty.HostId).FirstOrDefault();
                        childproperties.Add(new ChildPropertyModel()
                        {
                            Host = tempHost != null ? tempHost.Username + " (" + tempHost.Firstname + " " + tempHost.Lastname + ")" : "",
                            IsSyncProperty = tempProperty.IsSyncParent,
                            SiteType = tempProperty.SiteType,
                            Id = tempProperty.Id,
                            Name = tempProperty.Name,
                            IsChild = true
                        });
                    }
                }
                var availableProperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where p.IsParentProperty == false && hc.CompanyId == companyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();
                foreach (var p in availableProperties)
                {
                    var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == companyId).FirstOrDefault();
                    if (temp == null)
                    {
                        childproperties.Add(new ChildPropertyModel()
                        {
                            Host = p.Host,
                            IsSyncProperty = p.IsSyncParent,
                            SiteType = p.SiteType,
                            Id = p.Id,
                            Name = p.Name,
                            IsChild = false
                        });
                    }
                }
                var parentType = (Core.Enumerations.ParentPropertyType)type;
                var localchildProperty = db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == companyId && x.IsParentProperty == false).ToList();
                foreach (var p in localchildProperty)
                {
                    var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == companyId && x.ParentType == type).FirstOrDefault();
                    if (temp == null)
                    {
                        childproperties.Add(new ChildPropertyModel()
                        {
                            Host = "",
                            IsSyncProperty = p.IsSyncParent,
                            SiteType = p.SiteType,
                            Id = p.Id,
                            Name = p.Name,
                            IsChild = false
                        });
                    }
                }
                if (parentType != Core.Enumerations.ParentPropertyType.Sync)
                {
                    var localParentProperty = db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == companyId && x.IsParentProperty == true && (parentType == Core.Enumerations.ParentPropertyType.MultiUnit ? x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync : (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts))).ToList();
                    foreach (var p in localParentProperty)
                    {
                        var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == companyId && x.ParentType == type).FirstOrDefault();
                        if (temp == null)
                        {
                            childproperties.Add(new ChildPropertyModel()
                            {
                                Host = "",
                                IsSyncProperty = p.IsSyncParent,
                                SiteType = p.SiteType,
                                Id = p.Id,
                                Name = p.Name,
                                IsChild = false
                            });
                        }
                    }
                }
                return Json(new { childproperties }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetInbox(int companyId, int userId)
        {
            return Json(new { inbox = InboxService.GetInboxes(companyId, userId) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMessages(int skip,string threadId,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                return Json(new { messages = InboxService.GetMessages(threadId,userId,skip) }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetContactInfo(int renterId)
        {
            using (var db = new ApplicationDbContext())
            {
                var contactInfo = db.RenterContacts.Where(x => x.RenterId == renterId).Select(x => x.Contact).ToList();
                return Json(new { contactInfo = contactInfo }, JsonRequestBehavior.AllowGet);
            }
        } 

        public JsonResult GetProperties(string search,int skip,int userId)
        {
            List<Property> Properties;
            if (Core.Helper.User.Owners.IsOwner(userId))
            {
                Properties = Core.Helper.User.Owners.GetProperties(userId).Where(x=>string.IsNullOrWhiteSpace(search)?true:x.Name.Contains(search)).Skip(skip).Take(10).ToList();
            }
            else
            {
                using (var db = new ApplicationDbContext())
                {
                    var companyId = db.Users.Where(x => x.Id == userId).FirstOrDefault().CompanyId;
                    Properties = db.Properties.Where(x => x.CompanyId == companyId).ToList();
                    Properties = Properties.Where(x => string.IsNullOrWhiteSpace(search) ? true : x.Name.Contains(search)).Skip(skip).Take(10).ToList();
                }
            }
            return Json(new { Properties }, JsonRequestBehavior.AllowGet);

        }
        string UploadImage(HttpPostedFileBase file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            Files.CompressImage(Server.MapPath(url), 30, Server.MapPath(Destination));
            return url.Replace("~", "");
        }

        [HttpPost] 
        public ActionResult SendMessage(string to, string message, string threadId, int renterId,int userId, int? type)
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            CommunicationThread thread;
                Message msg = new Message();
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                       
                        var mediaUrl = new List<Uri>();
                        List<string> filesPath = new List<string>();
                    var httpRequest = HttpContext.Request;
                    if (httpRequest.Files.Count > 0)
                    {
                        foreach (string file in httpRequest.Files)
                        {
                            var filePosted = httpRequest.Files[file];
                            var path = Files.Upload(filePosted, "~/Images/MMS/");
                            filesPath.Add(path);
                            mediaUrl.Add(new Uri(Url + path));
                        }
                    }
                    string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
                        string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

                        TwilioClient.Init(accountSid, authToken);


                        var user = db.Users.Find(userId);
                        string phoneNumber = null;

                        if (type == 2)
                        {
                            phoneNumber = "whatsapp:+14155238886"; // Twilio WhatsApp Phone Number
                        }
                        else if (type == 3)
                        {
                            phoneNumber = "messenger:278080232837381"; // Messenger
                        }
                        else
                        {
                            if (user != null)
                            {
                                var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == user.CompanyId).FirstOrDefault();
                                if (twilioNumber != null)
                                {
                                    phoneNumber = twilioNumber.PhoneNumber;
                                }
                            }

                            if (string.IsNullOrEmpty(phoneNumber))
                            {
                                return Json(new { success = false, message = "You have no registered Number" }, JsonRequestBehavior.AllowGet);
                            }
                        }


                        if (threadId == "0")
                        {
                            thread = new CommunicationThread();
                            thread.RenterId = renterId;
                            thread.ThreadId = PasswordGenerator.Generate(20);
                            thread.CompanyId = user.CompanyId;
                            thread.IsRenter = true;
                            db.CommunicationThreads.Add(thread);
                            db.SaveChanges();
                        }
                        else
                        {
                            thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                        }
                        var smsMessage = MessageResource.Create(
                            from: new Twilio.Types.PhoneNumber(phoneNumber),
                            to: new Twilio.Types.PhoneNumber(to),
                            body: message,
                            mediaUrl: mediaUrl
                        );


                        CommunicationSMS sms = new CommunicationSMS
                        {
                            ThreadId = thread.ThreadId,
                            Message = message,
                            From = phoneNumber,
                            To = to,
                            CreatedDate = DateTime.Now,
                            UserId = userId

                        };
                        db.CommunicationSMS.Add(sms);
                        db.SaveChanges();
                        foreach (var filePath in filesPath)
                        {
                            Helper.MMSImages.SaveMMS(new MMSImage() { ImageUrl = filePath, CommunicationSMSId = sms.Id });
                        }

                    //This data will return for display of images and created data
                        msg = new Message();
                        msg.MessageContent = sms.Message;
                        msg.IsMyMessage = true;
                        msg.CreatedDate = sms.CreatedDate;
                        msg.Source =sms.From;
                    //Get files with Id
                    (from file in db.MMSImages where file.CommunicationSMSId == sms.Id select new { Id = file.Id, FileUrl = Url + file.ImageUrl }).ToList().ForEach(x =>
                      msg.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));

                    if (msg.Source.Substring(0, 8) == "whatsapp")
                    {
                        msg.Type = 3;
                    }
                    else if (msg.Source.Substring(0, 9) == "messenger")
                    {
                        msg.Type = 4;
                    }
                    else
                    {
                        msg.Type = 1;
                    }
                }
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = new Message() }, JsonRequestBehavior.AllowGet);
                }
                var messagingResponse = new MessagingResponse();

                return Json(new { success = true, message = msg, threadId = thread.ThreadId }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRentals(int companyId)
        {
            return Json(new {rentals = RentalService.GetRental(companyId) }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetPropertyPayment(int BookingId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var runningBalance = 0.0M;

                    var result0 = db.Income.Where(x => x.BookingId == BookingId).OrderBy(x => x.DueDate).ToList();
                    List<IncomeDetails> objIncomes = new List<IncomeDetails>();

                    foreach (var item in result0)
                    {
                        IncomeDetails objIncome = new IncomeDetails();

                        objIncome.BookingId = item.BookingId;
                        objIncome.Id = item.Id;

                        var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                        decimal dueAmount = 0;
                        dueAmount = payments.Sum(x => x.Amount);
                        if (payments.Count > 0)
                        {
                            foreach (var payment in payments)
                            {
                                objIncome.PaymentList.Add(payment);
                            }
                        }
                        objIncome.Payments = dueAmount;
                        objIncome.Amount = (item.Amount.ToDecimal());
                        objIncome.DueDate = item.DueDate;
                        objIncome.Description = item.Description;
                        objIncome.IncomeTypeId = item.IncomeTypeId;
                        objIncome.IsActive = item.IsActive;
                        if (item.IsActive && item.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund)
                        {
                            runningBalance += payments.Where(x => x.Type != (int)IncomePaymentType.Credit).Sum(x => x.Amount);
                        }
                        else if (item.IsActive == true && item.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund)
                        {
                            runningBalance -= item.Amount.Value;
                        }
                        objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments < objIncome.Amount) ? "Partial" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : objIncome.Payments > objIncome.Amount ? "Over Paid" : "Unpaid");

                        objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                        objIncome.RunningBalance = dueAmount != 0 ? runningBalance : 0;
                        objIncomes.Add(objIncome);
                    }
                    var batchPayments = db.IncomeBatchPayments.Where(x => x.BookingId == BookingId).ToList();
                    foreach (var payment in batchPayments)
                    {
                        IncomeDetails objIncome = new IncomeDetails();

                        objIncome.BookingId = payment.BookingId;
                        objIncome.Id = payment.Id;

                        //objIncome.IncomePayments = new List<IncomePayment>();
                        var incomePayments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == payment.Id && x.IsRejected == false).ToList();
                        decimal dueAmount = 0;
                        dueAmount = incomePayments.Sum(x => x.Amount);

                        var totalpay = incomePayments.Where(x => x.Type == (int)IncomePaymentType.Payment).Sum(x => x.Amount);
                        var unallocated = payment.Amount - totalpay;
                        objIncome.Status = unallocated == 0 ? "Full-allocated" : payment.Amount == unallocated ? "Unallocated" : "Partial-allocated";
                        objIncome.Amount = payment.Amount;
                        runningBalance += unallocated;
                        objIncome.RunningBalance = runningBalance;
                        objIncome.DueDate = payment.PaymentDate;
                        objIncome.Balance = unallocated;
                        objIncome.Description = payment.Description;
                        objIncome.IncomeTypeId = 6;
                        objIncome.IsActive = true;
                        var payments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == payment.Id && x.IsRejected == false).ToList();
                        if (payments.Count > 0)
                        {
                            foreach (var pay in payments)
                            {
                                var income = db.Income.Where(x => x.Id == pay.IncomeId).FirstOrDefault();
                                var tempayments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                                tempayments.Sum(x => x.Amount);
                                pay.Description = income.Description;
                                objIncome.PaymentList.Add(pay);

                            }
                        }
                        objIncomes.Add(objIncome);
                    }
                    objIncomes = objIncomes.OrderByDescending(x => x.DueDate).ToList();
                    return Json(new { success = true, Income = objIncomes, IsOwner = Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt()) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SavePayment()
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            var httpRequest = HttpContext.Request;
            var payment= JsonConvert.DeserializeObject<IncomePayment>(httpRequest.Form.Get("payment"));
            using (var db = new ApplicationDbContext())
            {
                var temp = db.IncomePayment.Where(x => x.Id == payment.Id).FirstOrDefault();
                if (temp != null)
                {
                    temp.Amount = payment.Amount;
                    temp.Description = payment.Description;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    var fileIds = JsonConvert.DeserializeObject<List<int>>(httpRequest.Form.Get("fileIds"));
                    if (fileIds.Count > 0)
                    {
                        db.IncomePaymentFiles.RemoveRange(db.IncomePaymentFiles.Where(x => x.PaymentId == payment.Id && !fileIds.Contains(x.Id)));
                        db.SaveChanges();
                    }
                }
                else
                {
                    db.IncomePayment.Add(payment);
                    db.SaveChanges();
                }
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var filePosted = httpRequest.Files[file];
                        db.IncomePaymentFiles.Add(new IncomePaymentFile() { PaymentId = payment.Id, FileUrl = Files.Upload(filePosted, "~/Images/IncomePayments/")});
                        db.SaveChanges();
                    }
                }
                var files = new List<FileModel>();
                (from file in db.IncomePaymentFiles where file.PaymentId== payment.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                return Json(new { result = true,paymentId=payment.Id,files }, JsonRequestBehavior.AllowGet);
            }
        }
        //Scheduler for rentals
        [HttpGet]
        public JsonResult GetPropertyMaintenance(int userId,string propertyIds="0")
        {
            List<int> pIds = propertyIds.Split(',').Select(int.Parse).ToList();
            var Properties = new List<Property>();
            var LocalChildIds = new List<int>();
            var isOwner = Core.Helper.User.Owners.IsOwner(userId);
           
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                  var properties = isOwner ? Core.Helper.User.Owners.GetProperties(User.Identity.Name.ToInt()).Where(x => (pIds.Contains(0) ? true : pIds.Contains(x.Id))).ToList() : db.Properties.Where(x => x.CompanyId == companyId && (pIds.Contains(0) ? true : pIds.Contains(x.Id))).ToList();


                foreach (var property in properties)
                {
                    List<IncomeDetail> objIncomes = new List<IncomeDetail>();
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    if (parentChild != null && property.IsParentProperty == false)
                    {
                        continue;
                    }

                    Properties.Add(property);
                    List<int> childPropertyIds = db.ParentChildProperties.Where(x => x.ParentPropertyId == property.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).Select(x => x.ChildPropertyId).ToList();
                    if (childPropertyIds.Count == 0)
                    {
                        childPropertyIds.Add(property.Id);
                    }
                    LocalChildIds.Add((from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where p.SiteType == 0 && pc.ParentPropertyId == property.Id && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p.Id).FirstOrDefault());
                }
            }

            var json = Json(new { success = true, Properties, LocalChildIds }, JsonRequestBehavior.AllowGet);

            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        public JsonResult GetScheduledMaintenanceExpensesEvent(int type,int skip,string search,int propertyId, int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                List<PropertyScheduledExpense> data;
                if(string.IsNullOrWhiteSpace(search))
                data = db.PropertyScheduledExpenses.Where(x => propertyId==0?true:x.PropertyId==propertyId&& x.Type == type && x.CompanyId == companyId).OrderBy(x => x.RecurringDate).Skip(skip).Take(5).ToList();
                else
                    data = db.PropertyScheduledExpenses.Where(x => propertyId == 0 ? true : x.PropertyId == propertyId && x.Description.Contains(search)&& x.Type == type && x.CompanyId == companyId).OrderBy(x => x.RecurringDate).Skip(skip).Take(5).ToList();
            
                return Json(new { data}, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetScheduledMaintenanceExpensesEventByProperty(int propertyId, int userId)
        {
            var toAppendPropertyId = propertyId;
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                List<PropertyScheduledExpense> data = new List<PropertyScheduledExpense>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).FirstOrDefault();
                if (parentChild == null)
                {
                    data = db.PropertyScheduledExpenses.Where(x => x.PropertyId == propertyId && x.CompanyId == companyId).OrderBy(x => x.RecurringDate).ToList();
                }
                else
                {
                    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).ToList();
                    toAppendPropertyId = parentChild.ParentPropertyId;
                    foreach (var p in propertyChild)
                    {
                        data.AddRange(db.PropertyScheduledExpenses.Where(x => x.PropertyId == p.ChildPropertyId && x.CompanyId == companyId).OrderBy(x => x.RecurringDate).ToList());
                    }
                }
                return Json(new { data, propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult SaveIncome()
        {
            //try
            //{
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            var httpRequest = HttpContext.Request;
                var income = JsonConvert.DeserializeObject<Income>(httpRequest.Form.Get("income"));
                using (var db = new ApplicationDbContext())
                {
                    if (income.Id == 0)
                    {
                        db.Income.Add(income);
                        db.SaveChanges();
                    }
                    else
                    {
                        var temp = db.Income.Where(x => x.Id == income.Id).FirstOrDefault();
                        if (temp != null)
                        {
                            temp.Description = income.Description;
                            temp.Amount = income.Amount;
                            temp.DueDate = income.DueDate;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    var fileIds = JsonConvert.DeserializeObject<List<int>>(httpRequest.Form.Get("fileIds"));
                    if (fileIds.Count > 0)
                    {
                        db.IncomeFiles.RemoveRange(db.IncomeFiles.Where(x => x.IncomeId==income.Id && !fileIds.Contains(x.Id)));
                        db.SaveChanges();
                    }

                }
                    
                    if (httpRequest.Files.Count > 0)
                    {
                        foreach (string file in httpRequest.Files)
                        {
                            var filePosted = httpRequest.Files[file];
                            db.IncomeFiles.Add(new IncomeFile() {IncomeId=income.Id,FileUrl= Files.Upload(filePosted, "~/Images/Income/") });
                            db.SaveChanges();
                        }
                    }
                var files = new List<FileModel>();
                (from file in db.IncomeFiles where file.IncomeId == income.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));

                return Json(new { success = true, incomeId = income.Id,files }, JsonRequestBehavior.AllowGet);
                }
            //}
            //catch (Exception ex)
            //{
            //    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            //}
        }
        public JsonResult GetIncome(int skip,string search,int propertyId, int incomeTypeId,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                #region Income
                List<IncomeDetails> objIncomes = new List<IncomeDetails>();
                List<Income> incomes;
                if (string.IsNullOrWhiteSpace(search))
                {
                    incomes = db.Income.Where(x => x.IsActive && x.CompanyId == companyId && (propertyId == 0 ? true : x.PropertyId == propertyId && incomeTypeId == 0 ? true : x.IncomeTypeId == incomeTypeId)).OrderBy(x=>(from payment in db.IncomePayment where payment.IncomeId == x.Id select payment.Amount).Sum()== 0).ThenBy(x=> (from payment in db.IncomePayment where payment.IncomeId == x.Id select payment.Amount).Sum() < x.Amount).ThenBy(x => (from payment in db.IncomePayment where payment.IncomeId == x.Id select payment.Amount).Sum() == x.Amount).OrderByDescending(x => x.DueDate).Skip(skip).Take(5).ToList();
                }
                else
                {
                    incomes = db.Income.Where(x => x.Description.Contains(search) && x.IsActive && x.CompanyId == companyId && (propertyId == 0 ? true : x.PropertyId == propertyId && incomeTypeId == 0 ? true : x.IncomeTypeId == incomeTypeId)).OrderBy(x => (from payment in db.IncomePayment where payment.IncomeId == x.Id select payment.Amount).Sum() == 0).ThenBy(x => (from payment in db.IncomePayment where payment.IncomeId == x.Id select payment.Amount).Sum() < x.Amount).ThenBy(x => (from payment in db.IncomePayment where payment.IncomeId == x.Id select payment.Amount).Sum() == x.Amount).OrderByDescending(x => x.DueDate).Skip(skip).Take(5).ToList();
                }
                foreach (var item in incomes)
                {
                    IncomeDetails objIncome = new IncomeDetails();
                    objIncome.BookingId = item.BookingId;
                    objIncome.Id = item.Id;

                    var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                    decimal dueAmount = 0;
                    dueAmount = payments.Sum(x => x.Amount);
                    
                    if (payments.Count > 0)
                    {
                        foreach (var payment in payments)
                        {
                            //Get files for income payment
                            (from file in db.IncomePaymentFiles where file.PaymentId == payment.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                            payment.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                           
                        }
                        objIncome.PaymentList =payments;
                    }
                    //Get Files for income
                     (from file in db.IncomeFiles where file.IncomeId == item.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList()
                        .ForEach(x =>objIncome.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                    objIncome.Payments = dueAmount;
                    objIncome.Amount = (item.Amount.ToDecimal());
                    objIncome.IncomeTypeId = item.IncomeTypeId;
                    objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments < objIncome.Amount) ? "Partial" : objIncome.Payments > objIncome.Amount ? "Over Paid" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                    objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                    objIncome.DueDate = item.DueDate;
                    objIncome.Description = item.Description == null ? "" : item.Description;
                    objIncome.IsActive = item.IsActive;
                    objIncome.DateRecorded = item.DateRecorded;
                    var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                    //var syncParent = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    objIncome.PropertyName = tempProperty.Name;
                    objIncome.PropertyId = tempProperty.Id;
                    objIncome.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                    var tempInquiry = db.Inquiries.SingleOrDefault(t => t.Id == item.BookingId);
                    if (tempInquiry != null)
                    {
                        var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                        var renter = db.Renters.Where(x => x.BookingId == tempInquiry.Id).ToList();
                        if (tempGuest != null) { objIncome.GuestName = tempGuest.Name; }
                        else if (renter.Count > 0)
                        {
                            renter.ForEach(x => objIncome.GuestName += x.Firstname+" "+x.Lastname + ",");
                        }
                        objIncome.BookingCode = tempInquiry.ConfirmationCode;
                        //var siteType = siteTypeList.SingleOrDefault(t => t.Code == tempInquiry.SiteTypeCode);
                        //if (siteType != null) { temp.SiteType = siteType.Description; }
                    }
                    objIncomes.Add(objIncome);
                }
                #endregion
                return Json(new { Incomes = objIncomes.OrderByDescending(x => x.DueDate)}, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetExpense(int skip, string search, int propertyId,int expenseCategoryId, int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                List<ExpenseList> el = new List<ExpenseList>();
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                List<Expense> expenses;
                expenses = db.Expenses.Where(x => (search=="" || search==null ? true : x.Description.Contains(search)) && x.PropertyScheduledExpenseId != null && x.IsActive && x.CompanyId == companyId && propertyId == 0 ? true : x.PropertyId == propertyId || expenseCategoryId == 0 ? true : x.ExpenseCategoryId == expenseCategoryId).OrderBy(x => (from payment in db.ExpensePayment where payment.ExpenseId == x.Id select payment.Amount).Sum() == 0).ThenBy(x => (from payment in db.ExpensePayment where payment.ExpenseId == x.Id select payment.Amount).Sum() < x.Amount).ThenBy(x => (from payment in db.ExpensePayment where payment.ExpenseId == x.Id select payment.Amount).Sum() == x.Amount).OrderByDescending(x => x.DueDate).Skip(skip).Take(5).ToList();

                expenses.ForEach(item =>
                {
                    var tempBooking = item.BookingId == 0 ? null : db.Inquiries.Where(x => x.Id == item.BookingId).FirstOrDefault();
                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Id).ToList();
                    ExpenseList temp = new ExpenseList();
                    //Get Files for expense files
                    (from file in db.ExpenseFiles where file.ExpenseId == item.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                    temp.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                    //Get Files for Expense Payment
                    foreach(var payment in payments)
                    {
                        (from file in db.ExpensePaymentFiles where file.PaymentId == item.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                         payment.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                    }
                    temp.Payments = payments;
                    temp.Amount = string.Format("{0:#,0.00}", item.Amount);
                    temp.BookingCode = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.ConfirmationCode;
                    var expenseCategory = db.ExpenseCategories.Where(x => x.Id == item.ExpenseCategoryId).FirstOrDefault();
                    temp.Category = expenseCategory != null ? expenseCategory.ExpenseCategoryType : "-";
                    temp.ExpenseCategoryId = expenseCategory != null ? expenseCategory.Id : 0;
                    var paymentMethod = db.PaymentMethods.Where(x => x.Id == item.PaymentMethodId).FirstOrDefault();
                    temp.PaymentMethodId = paymentMethod!=null ? paymentMethod.Id:0;
                    temp.PaymentMethod = paymentMethod != null ? paymentMethod.Name :"";
                    temp.CreatedDate = item.DateCreated.ToString("MMM d, yyyy");
                    temp.Description = item.Description;
                    temp.DueDate = item.DueDate.ToString("MMM d, yyyy");
                    temp.IsPaid = item.IsPaid;
                    temp.FeeName = db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault() == null ? "-" : db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault().Fee_Type;
                    if (tempBooking != null)
                    {
                        var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempBooking.GuestId);
                        var renter = db.Renters.Where(x => x.BookingId == tempBooking.Id).ToList();

                        if (tempGuest != null) { temp.Guest = tempGuest.Name; }
                        else if (renter.Count > 0)
                        {
                            renter.ForEach(x => temp.Guest += x.Firstname + " " + x.Lastname + ",");
                        }
                    }
                    temp.PaymentDate = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToString("MMM d,yyyy") : "-") : "-";// item.Expenses.PaymentDate.HasValue ? item.Expenses.PaymentDate.Value.ToString("MMM d, yyyy") : "-";
                    var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                    if (tempProperty != null)
                    {
                        temp.PropertyId = tempProperty.Id;
                        temp.PropertyName = tempProperty.Name;
                    }  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                    temp.Status = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? "Paid" : "Unpaid") : "Unpaid";
                    temp.Balance = Math.Abs(item.Amount.ToDecimal() - payments.Sum(x => x.Amount.ToDecimal()));
                    temp.StayDate = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.CheckInDate.Value.ToString("MMM d-") + (tempBooking.CheckOutDate.Value.Month == tempBooking.CheckInDate.Value.Month ? tempBooking.CheckOutDate.Value.ToString("d yyyy") : tempBooking.CheckOutDate.Value.ToString("MMM d yyyy"));
                    temp.Id = item.Id;

                    el.Add(temp);
                });
                return Json(new { Expenses = el.OrderByDescending(x => x.DueDate) }, JsonRequestBehavior.AllowGet);
         
            }
        }
        public JsonResult GetMaintenanceEntries(int skip, string search,int propertyId, int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                List<MaintenanceEntryViewModel> MaintenanceEntries = new List<MaintenanceEntryViewModel>();
                List<MaintenanceEntry> maintenanceEnties;
                if (string.IsNullOrWhiteSpace(search))
                {
                    maintenanceEnties = db.MaintenanceEntries.Where(x =>propertyId==0?true:x.PropertyId==propertyId&& x.IsActive && x.CompanyId == companyId).OrderByDescending(x => x.Status==(int)MaintenanceStatus.Started).ThenBy(x=>x.Status== (int)MaintenanceStatus.NotYetStarted).ThenBy(x=>x.Status== (int)MaintenanceStatus.Complete).Skip(skip).Take(5).ToList();
                }
                else {
                    maintenanceEnties = db.MaintenanceEntries.Where(x => propertyId == 0 ? true : x.PropertyId == propertyId && x.Description.Contains(search) && x.IsActive && x.CompanyId == companyId).OrderByDescending(x => x.DueDate).Skip(skip).Take(5).ToList();
                }
                foreach (var maintenanceEntry in maintenanceEnties)
                {
                    var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                    var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                    MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                    {
                        Id = maintenanceEntry.Id,
                        Description = maintenanceEntry.Description,
                        DueDate = maintenanceEntry.DueDate,
                        Amount = maintenanceEntry.Amount.ToDecimal(),
                        Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                        Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                        DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "",
                        DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "",
                        PropertyId = maintenanceEntry.PropertyId,
                        WorkerId = maintenanceEntry.WorkerId.GetValueOrDefault(),
                        JobTypeId= maintenanceEntry.JobTypeId.GetValueOrDefault(),
                        VendorId = maintenanceEntry.VendorId.GetValueOrDefault()
                    }) ;
                }
                return Json(new { MaintenanceEntries = MaintenanceEntries }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIncomeByProperty(int propertyId, int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var toAppendPropertyId = propertyId;
                #region Income
                List<int> propertyChildIds = new List<int>();
                List<IncomeDetail> objIncomes = new List<IncomeDetail>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).FirstOrDefault();
                if (parentChild == null)
                {
                    propertyChildIds.Add(propertyId);
                }
                else
                {
                    propertyChildIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).Select(x=>x.ChildPropertyId));
                }
                var result0 = db.Income.Where(x => (propertyChildIds.Contains(x.PropertyId) && x.IsActive) && x.CompanyId == companyId).OrderBy(x => x.DueDate).OrderByDescending(x=>x.DueDate).ToList();
                foreach (var item in result0)
                {
                    IncomeDetail objIncome = new IncomeDetail();

                    objIncome.BookingId = item.BookingId;
                    objIncome.Id = item.Id;

                    var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                    decimal dueAmount = 0;
                    dueAmount = payments.Sum(x => x.Amount);
                    if (payments.Count > 0)
                    {
                        foreach (var payment in payments)
                        {
                            objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                        }
                    }
                    else { objIncome.PaymentDetails = "-"; }
                    objIncome.Payments = dueAmount;
                    objIncome.Amount = (item.Amount.ToDecimal());
                    objIncome.IncomeTypeId = item.IncomeTypeId;
                    objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments < objIncome.Amount) ? "Partial" : objIncome.Payments > objIncome.Amount ? "Over Paid" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                    objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                    objIncome.DueDate = item.DueDate;
                    objIncome.Description = item.Description == null ? "" : item.Description;
                    objIncome.IsActive = item.IsActive;
                    objIncome.DateRecorded = item.DateRecorded;
                    var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                    //var syncParent = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    objIncome.PropertyName = tempProperty.Name;

                    objIncome.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                    var tempInquiry = db.Inquiries.SingleOrDefault(t => t.Id == item.BookingId);
                    if (tempInquiry != null)
                    {
                        var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                        var renter = db.Renters.Where(x => x.BookingId == tempInquiry.Id).ToList();
                        if (tempGuest != null) { objIncome.GuestName = tempGuest.Name; }
                        else if (renter.Count > 0)
                        {
                            renter.ForEach(x => objIncome.GuestName += x.Firstname + " " + x.Lastname + ",");
                        }
                        objIncome.BookingCode = tempInquiry.ConfirmationCode;
                        //var siteType = siteTypeList.SingleOrDefault(t => t.Code == tempInquiry.SiteTypeCode);
                        //if (siteType != null) { temp.SiteType = siteType.Description; }
                    }
                    objIncomes.Add(objIncome);
                }

                //if (parentChild == null)
                //{
                //    var result0 = db.Income.Where(x => (x.PropertyId == propertyId && x.IsActive) && x.CompanyId ==companyId).OrderBy(x => x.DueDate).ToList();
                //    foreach (var item in result0)
                //    {
                //        IncomeDetail objIncome = new IncomeDetail();

                //        objIncome.BookingId = item.BookingId;
                //        objIncome.Id = item.Id;

                //        var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                //        decimal dueAmount = 0;
                //        dueAmount = payments.Sum(x => x.Amount);
                //        if (payments.Count > 0)
                //        {
                //            foreach (var payment in payments)
                //            {
                //                objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                //            }
                //        }
                //        else { objIncome.PaymentDetails = "-"; }
                //        objIncome.Payments = dueAmount;
                //        objIncome.Amount = (item.Amount.ToDecimal());
                //        objIncome.IncomeTypeId = item.IncomeTypeId;
                //        objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments < objIncome.Amount) ? "Partial" : objIncome.Payments > objIncome.Amount ? "Over Paid" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                //        objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                //        objIncome.DueDate = item.DueDate;
                //        objIncome.Description = item.Description == null ? "" : item.Description;
                //        objIncome.IsActive = item.IsActive;
                //        objIncome.DateRecorded = item.DateRecorded;
                //        var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                //        //var syncParent = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                //        objIncome.PropertyName = tempProperty.Name;

                //        objIncome.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                //        var tempInquiry = db.Inquiries.SingleOrDefault(t => t.Id == item.BookingId);
                //        if (tempInquiry != null)
                //        {
                //            var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                //            var renter = db.Renters.Where(x => x.BookingId == tempInquiry.Id).ToList();
                //            if (tempGuest != null) { objIncome.GuestName = tempGuest.Name; }
                //            else if (renter.Count > 0)
                //            {
                //                renter.ForEach(x => objIncome.GuestName += x.Name + ",");
                //            }
                //            objIncome.BookingCode = tempInquiry.ConfirmationCode;
                //            //var siteType = siteTypeList.SingleOrDefault(t => t.Code == tempInquiry.SiteTypeCode);
                //            //if (siteType != null) { temp.SiteType = siteType.Description; }
                //        }
                //        objIncomes.Add(objIncome);
                //    }
                //}
                //else
                //{
                //    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).ToList();
                //    toAppendPropertyId = parentChild.ParentPropertyId;
                //    foreach (var p in propertyChild)
                //    {
                //        var result0 = db.Income.Where(x => (x.PropertyId == p.ChildPropertyId && x.IsActive) && x.CompanyId ==companyId).OrderBy(x => x.DueDate).ToList();
                //        foreach (var item in result0)
                //        {
                //            IncomeDetail objIncome = new IncomeDetail();

                //            objIncome.BookingId = item.BookingId;
                //            objIncome.Id = item.Id;

                //            var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                //            decimal dueAmount = 0;
                //            dueAmount = payments.Sum(x => x.Amount);
                //            if (payments.Count > 0)
                //            {
                //                foreach (var payment in payments)
                //                {
                //                    objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                //                }
                //            }
                //            else { objIncome.PaymentDetails = "-"; }
                //            objIncome.Payments = dueAmount;
                //            objIncome.Amount = (item.Amount.ToDecimal());
                //            objIncome.IncomeTypeId = item.IncomeTypeId;
                //            objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Amount != objIncome.Payments) ? "Partial" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                //            objIncome.Balance = item.Amount.ToDecimal() - dueAmount;
                //            objIncome.DueDate = item.DueDate;
                //            objIncome.Description = item.Description == null ? "" : item.Description;
                //            objIncome.IsActive = item.IsActive;
                //            objIncome.DateRecorded = item.DateRecorded;
                //            var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                //            objIncome.PropertyName = tempProperty.Name;
                //            objIncome.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                //            var tempInquiry = db.Inquiries.SingleOrDefault(t => t.Id == item.BookingId);
                //            if (tempInquiry != null)
                //            {
                //                var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                //                var renter = db.Renters.Where(x => x.BookingId == tempInquiry.Id).ToList();
                //                if (tempGuest != null) { objIncome.GuestName = tempGuest.Name; }
                //                else if (renter.Count > 0)
                //                {
                //                    renter.ForEach(x => objIncome.GuestName += x.Name + ",");
                //                }
                //                objIncome.BookingCode = tempInquiry.ConfirmationCode;
                //                //var siteType = siteTypeList.SingleOrDefault(t => t.Code == tempInquiry.SiteTypeCode);
                //                //if (siteType != null) { temp.SiteType = siteType.Description; }
                //            }
                //            objIncomes.Add(objIncome);
                //        }
                //    }
                //}
                #endregion
                return Json(new { Incomes = objIncomes.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetExpenseByProperty(int propertyId, int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                List<ExpenseList> el = new List<ExpenseList>();
                var toAppendPropertyId = propertyId;
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                List<int> propertyChildIds = new List<int>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId ==companyId).FirstOrDefault();
                if (parentChild == null)
                {
                    propertyChildIds.Add(propertyId);
                }
                else
                {
                    propertyChildIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).Select(x => x.ChildPropertyId));
                }
                var expenses = db.Expenses.Where(x => x.PropertyScheduledExpenseId != null && propertyChildIds.Contains(x.PropertyId) && x.IsActive && x.CompanyId == companyId).OrderByDescending(x => x.DueDate).ToList();
                expenses.ForEach(item =>
                {
                    var tempBooking = item.BookingId == 0 ? null : db.Inquiries.Where(x => x.Id == item.BookingId).FirstOrDefault();
                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Id).ToList();
                    ExpenseList temp = new ExpenseList();
                    temp.Payments = payments;
                    temp.Amount = string.Format("{0:#,0.00}", item.Amount);
                    temp.BookingCode = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.ConfirmationCode;
                    var expenseCategory = db.ExpenseCategories.Where(x => x.Id == item.ExpenseCategoryId).FirstOrDefault();
                    temp.Category = expenseCategory != null ? expenseCategory.ExpenseCategoryType : "-";
                    temp.ExpenseCategoryId = expenseCategory != null ? expenseCategory.Id : 0;
                    var paymentMethod = db.PaymentMethods.Where(x => x.Id == item.PaymentMethodId).FirstOrDefault();
                    temp.PaymentMethodId = paymentMethod != null ? paymentMethod.Id : 0;
                    temp.PaymentMethod = paymentMethod != null ? paymentMethod.Name : "";
                    temp.CreatedDate = item.DateCreated.ToString("MMM d, yyyy");
                    temp.Description = item.Description;
                    temp.DueDate = item.DueDate.ToString("MMM d, yyyy");
                    temp.IsPaid = item.IsPaid;
                    temp.FeeName = db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault() == null ? "-" : db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault().Fee_Type;
                    if (tempBooking != null)
                    {
                        var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempBooking.GuestId);
                        var renter = db.Renters.Where(x => x.BookingId == tempBooking.Id).ToList();

                        if (tempGuest != null) { temp.Guest = tempGuest.Name; }
                        else if (renter.Count > 0)
                        {
                            renter.ForEach(x => temp.Guest += x.Firstname + " " + x.Lastname + ",");
                        }
                    }
                    temp.PaymentDate = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToString("MMM d,yyyy") : "-") : "-";// item.Expenses.PaymentDate.HasValue ? item.Expenses.PaymentDate.Value.ToString("MMM d, yyyy") : "-";
                    var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                    temp.PropertyName = tempProperty.Name;  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                    temp.Status = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? "Paid" : "Unpaid") : "Unpaid";
                    temp.Balance = Math.Abs(item.Amount.ToDecimal() - payments.Sum(x => x.Amount.ToDecimal()));
                    temp.StayDate = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.CheckInDate.Value.ToString("MMM d-") + (tempBooking.CheckOutDate.Value.Month == tempBooking.CheckInDate.Value.Month ? tempBooking.CheckOutDate.Value.ToString("d yyyy") : tempBooking.CheckOutDate.Value.ToString("MMM d yyyy"));
                    temp.Id = item.Id;

                    el.Add(temp);
                });
                return Json(new { Expenses = el.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
                //if (parentChild == null)
                //{

                //    var expenses = db.Expenses.Where(x =>x.PropertyScheduledExpenseId !=null && x.PropertyId == propertyId && x.IsActive && x.CompanyId == companyId).OrderBy(x => x.DueDate).ToList();
                //    expenses.ForEach(item =>
                //    {
                //        var tempBooking = item.BookingId == 0 ? null : db.Inquiries.Where(x => x.Id == item.BookingId).FirstOrDefault();
                //        var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Id).ToList();
                //        ExpenseList temp = new ExpenseList();
                //        temp.Payments = payments;
                //        temp.Amount = string.Format("{0:#,0.00}", item.Amount);
                //        temp.BookingCode = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.ConfirmationCode;
                //        var expenseCategory = db.ExpenseCategories.Where(x => x.Id == item.ExpenseCategoryId).FirstOrDefault();
                //        temp.Category = expenseCategory != null ? expenseCategory.ExpenseCategoryType : "-";
                //        temp.CreatedDate = item.DateCreated.ToString("MMM d, yyyy");
                //        temp.Description = item.Description;
                //        temp.DueDate = item.DueDate.ToString("MMM d, yyyy");
                //        temp.IsPaid = item.IsPaid;
                //        temp.FeeName = db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault() == null ? "-" : db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault().Fee_Type;
                //        if (tempBooking != null)
                //        {
                //            var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempBooking.GuestId);
                //            var renter = db.Renters.Where(x => x.BookingId == tempBooking.Id).ToList();

                //            if (tempGuest != null) { temp.Guest = tempGuest.Name; }
                //            else if (renter.Count > 0)
                //            {
                //                renter.ForEach(x => temp.Guest += x.Name + ",");
                //            }
                //        }
                //        temp.PaymentDate = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToString("MMM d,yyyy") : "-") : "-";// item.Expenses.PaymentDate.HasValue ? item.Expenses.PaymentDate.Value.ToString("MMM d, yyyy") : "-";
                //        var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                //        temp.PropertyName = tempProperty.Name;  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                //        temp.Status = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? "Paid" : "Unpaid") : "Unpaid";
                //        temp.Balance = Math.Abs(item.Amount.ToDecimal() - payments.Sum(x => x.Amount.ToDecimal()));
                //        temp.StayDate = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.CheckInDate.Value.ToString("MMM d-") + (tempBooking.CheckOutDate.Value.Month == tempBooking.CheckInDate.Value.Month ? tempBooking.CheckOutDate.Value.ToString("d yyyy") : tempBooking.CheckOutDate.Value.ToString("MMM d yyyy"));
                //        temp.Id = item.Id;

                //        el.Add(temp);
                //    });
                //    return Json(new { Expenses = el.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);

                //}
                //else
                //{
                //    toAppendPropertyId = parentChild.ParentPropertyId;
                //    var propertyChildId = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).Select(x => x.ChildPropertyId).ToList();
                //    var expenses = db.Expenses.Where(x => propertyChildId.Contains(x.PropertyId) && x.IsActive && x.CompanyId == companyId).OrderBy(x => x.DueDate).ToList();
                //    expenses.ForEach(item =>
                //    {
                //        var tempBooking = item.BookingId == 0 ? null : db.Inquiries.Where(x => x.Id == item.BookingId).FirstOrDefault();
                //        var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Id).ToList();
                //        ExpenseList temp = new ExpenseList();
                //        temp.Amount = string.Format("{0:#,0.00}", item.Amount);
                //        temp.BookingCode = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.ConfirmationCode;
                //        var expenseCategory = db.ExpenseCategories.Where(x => x.Id == item.ExpenseCategoryId).FirstOrDefault();
                //        temp.Category = expenseCategory != null ? expenseCategory.ExpenseCategoryType : "-";
                //        temp.CreatedDate = item.DateCreated.ToString("MMM d, yyyy");
                //        temp.Description = item.Description;
                //        temp.DueDate = item.DueDate.ToString("MMM d, yyyy");


                //        temp.IsPaid = item.IsPaid;
                //        temp.FeeName = db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault() == null ? "-" : db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault().Fee_Type;
                //        if (tempBooking != null)
                //        {
                //            var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempBooking.GuestId);
                //            var renter = db.Renters.Where(x => x.BookingId == tempBooking.Id).ToList();

                //            if (tempGuest != null) { temp.Guest = tempGuest.Name; }
                //            else if (renter.Count > 0)
                //            {
                //                renter.ForEach(x => temp.Guest += x.Name + ",");
                //            }
                //        }
                //        temp.PaymentDate = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToString("MMM d,yyyy") : "-") : "-";// item.Expenses.PaymentDate.HasValue ? item.Expenses.PaymentDate.Value.ToString("MMM d, yyyy") : "-";
                //        var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                //        temp.PropertyName = tempProperty.Name;  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                //        temp.Status = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? "Paid" : "Unpaid") : "Unpaid";
                //        temp.Balance = Math.Abs(item.Amount.ToDecimal() - payments.Sum(x => x.Amount.ToDecimal()));
                //        temp.StayDate = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.CheckInDate.Value.ToString("MMM d-") + (tempBooking.CheckOutDate.Value.Month == tempBooking.CheckInDate.Value.Month ? tempBooking.CheckOutDate.Value.ToString("d yyyy") : tempBooking.CheckOutDate.Value.ToString("MMM d yyyy"));
                //        temp.Id = item.Id;
                //        el.Add(temp);
                //    });
                //    return Json(new { Expenses = el.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);

                //}
            }
        }
        public JsonResult GetMaintenanceEntriesByProperty(int propertyId,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var toAppendPropertyId = propertyId;
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                List<MaintenanceEntryViewModel> MaintenanceEntries = new List<MaintenanceEntryViewModel>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId ==companyId).FirstOrDefault();
                List<int> propertyChildIds = new List<int>();

                if (parentChild == null)
                {
                    propertyChildIds.Add(propertyId);
                }
                else
                {
                    propertyChildIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).Select(x => x.ChildPropertyId));
                }
                var maintenanceEnties = db.MaintenanceEntries.Where(x => propertyChildIds.Contains(x.PropertyId) && x.IsActive && x.CompanyId == companyId).OrderByDescending(x=>x.DueDate).ToList();
                foreach (var maintenanceEntry in maintenanceEnties)
                {
                    var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                    var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                    MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                    {
                        Id = maintenanceEntry.Id,
                        Description = maintenanceEntry.Description,
                        DueDate = maintenanceEntry.DueDate,
                        Amount = maintenanceEntry.Amount.ToDecimal(),
                        Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                        Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                        DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "",
                        DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "",
                        WorkerId = maintenanceEntry.WorkerId.GetValueOrDefault(),
                        JobTypeId = maintenanceEntry.JobTypeId.GetValueOrDefault(),
                        VendorId = maintenanceEntry.VendorId.GetValueOrDefault()
                    });
                }
                //if (parentChild == null)
                //{
                //    var maintenanceEnties = db.MaintenanceEntries.Where(x => x.PropertyId == propertyId && x.IsActive && x.CompanyId == companyId).ToList();
                //    foreach (var maintenanceEntry in maintenanceEnties)
                //    {
                //        var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                //        var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                //        MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                //        {
                //            Id = maintenanceEntry.Id,
                //            Description = maintenanceEntry.Description,
                //            DueDate = maintenanceEntry.DueDate,
                //            Amount = maintenanceEntry.Amount.ToDecimal(),
                //            Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                //            Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                //            DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "-",
                //            DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "-",
                //            Actions = "<a class=\"ui mini button edit-maintenance-entry-btn\" title=\"Edit\">" +
                //                      "<i class=\"icon edit\"></i>" +
                //                      "</a>" +
                //                      "<a class=\"ui mini button delete-maintenance-entry\" title=\"Delete\">" +
                //                      "<i class=\"icon trash\"></i>" +
                //                      "</a>"
                //        });
                //    }
                //}
                //else
                //{
                //    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == companyId).ToList();
                //    toAppendPropertyId = parentChild.ParentPropertyId;
                //    foreach (var p in propertyChild)
                //    {
                //        var maintenanceEnties = db.MaintenanceEntries.Where(x => x.PropertyId == p.ChildPropertyId && x.IsActive && x.CompanyId == companyId).ToList();
                //        foreach (var maintenanceEntry in maintenanceEnties)
                //        {
                //            var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                //            var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                //            MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                //            {
                //                Id = maintenanceEntry.Id,
                //                Description = maintenanceEntry.Description,
                //                DueDate = maintenanceEntry.DueDate,
                //                Amount = maintenanceEntry.Amount.ToDecimal(),
                //                Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                //                Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                //                DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "-",
                //                DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "-",
                //                Actions = "<a class=\"ui mini button edit-maintenance-entry-btn\" title=\"Edit\">" +
                //                          "<i class=\"icon edit\"></i>" +
                //                          "</a>" +
                //                          "<a class=\"ui mini button delete-maintenance-entry\" title=\"Delete\">" +
                //                          "<i class=\"icon trash\"></i>" +
                //                          "</a>"
                //            });
                //        }
                //    }
                //}
                return Json(new { MaintenanceEntries = MaintenanceEntries, propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIncomeTypes(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var IncomeTypes = db.IncomeTypes.Where(x => (x.CompanyTerm != 2) && (x.CompanyId == companyId || !x.IsByCompany)).ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in IncomeTypes)
                {
                    data.Add(item.Id.ToString(), item.TypeName);
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetPaymentMethods(int userId)
        {
            using(var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var PaymentMethods = db.PaymentMethods.Where(x=>x.CompanyId ==companyId || x.Type == 0).ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach(var item in PaymentMethods)
                {
                    data.Add(item.Id.ToString(), item.Name);
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetExpenseCategories(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                //Company term 2 is short term
                var ExpenseCategories = db.ExpenseCategories.Where(x => x.CompanyId == companyId || x.Type == 2 && x.CompanyTerm!=2).ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in ExpenseCategories)
                {
                    data.Add(item.Id.ToString(), item.ExpenseCategoryType);
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetWorkers(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var Workers = db.Workers.Where(x => x.CompanyId == companyId).ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in Workers)
                {
                    data.Add(item.Id.ToString(), item.Firstname+" "+item.Lastname);
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetJobTypes(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var JobTypes = db.WorkerJobTypes.Where(x => x.CompanyId == companyId || x.CompanyId == 0).ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in JobTypes)
                {
                    data.Add(item.Id.ToString(), item.Name);
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetJobTypeList(string search,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var JobTypes = db.WorkerJobTypes.Where(x =>(search=="" || search==null)?true:x.Name.Contains(search) && x.CompanyId == companyId || x.CompanyId == 0).ToList();
                return Json(new { data = JobTypes }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetJobTypeClassification()
        {
            using (var db = new ApplicationDbContext())
            {
                var classifications = db.WorkerClassifications.ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in classifications)
                {
                    data.Add(item.Id.ToString(), item.Name);
                }
                return Json(new { data}, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult SaveIncomeType(Core.Database.Entity.IncomeType incomeType)
        {
            using(var db = new ApplicationDbContext())
            {
                if(incomeType.Id == 0)
                {
                    db.IncomeTypes.Add(incomeType);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.IncomeTypes.Where(x => x.Id == incomeType.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.TypeName = incomeType.TypeName;
                        temp.CompanyTerm = incomeType.CompanyTerm;
                        db.SaveChanges();
                    }
                }

                return Json(new { id= incomeType.Id }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIncomeTypeList(string search,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var IncomeTypes = db.IncomeTypes.Where(x => (x.CompanyTerm !=2) && (x.CompanyId == companyId || !x.IsByCompany)).ToList();
                
                return Json(new { data =IncomeTypes }, JsonRequestBehavior.AllowGet);

            }
        }
        public JsonResult GetExpenseCategoryList(string search,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var ExpenseCategories = db.ExpenseCategories.Where(x => (x.CompanyTerm != 2)&& (x.CompanyId == companyId || x.Type == 2) ).ToList();
                return Json(new { data= ExpenseCategories }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        public JsonResult SaveExpenseCategory(Core.Database.Entity.ExpenseCategory expenseCategory)
        {
            using (var db = new ApplicationDbContext())
            {
                if (expenseCategory.Id == 0)
                {
                    db.ExpenseCategories.Add(expenseCategory);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.ExpenseCategories.Where(x => x.Id == expenseCategory.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.ExpenseCategoryType = expenseCategory.ExpenseCategoryType;
                        temp.CompanyTerm = expenseCategory.CompanyTerm;
                        db.SaveChanges();
                    }
                }

            }
            return Json(new { id = expenseCategory.Id }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveJobType(WorkerJobType jobType)
        {
            using(var db = new ApplicationDbContext())
            {
                if (jobType.Id == 0)
                {
                    db.WorkerJobTypes.Add(jobType);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.WorkerJobTypes.Where(x => x.Id == jobType.Id).FirstOrDefault();
                    temp.Name = jobType.Name;
                    temp.ClassificationId = jobType.ClassificationId;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { Id = jobType.Id}, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetVendors(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var companyId = db.Users.Where(x => x.Id == userId).First().CompanyId;
                var Vendors = db.Vendors.Where(x => x.CompanyId == companyId).ToList();
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in Vendors)
                {
                    data.Add(item.Id.ToString(), item.Name);
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveExpensePayment()
        {
            using (var db = new ApplicationDbContext())
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                var httpRequest = HttpContext.Request;
                 var payment = JsonConvert.DeserializeObject<ExpensePayment>(httpRequest.Form.Get("payment"));
                if (payment.Id != 0)
                {
                    var temp = db.ExpensePayment.Where(x => x.Id == payment.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Amount = payment.Amount;
                        temp.Description = payment.Description;
                        temp.CreatedAt = payment.CreatedAt;
                        temp.Type = payment.Type;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    var fileIds = JsonConvert.DeserializeObject<List<int>>(httpRequest.Form.Get("fileIds"));
                    if (fileIds.Count > 0)
                    {
                        db.ExpensePaymentFiles.RemoveRange(db.ExpensePaymentFiles.Where(x => x.PaymentId == payment.Id && !fileIds.Contains(x.Id)));
                        db.SaveChanges();
                    }
                }
                else
                {
                    db.ExpensePayment.Add(payment);
                    db.SaveChanges();
                }
                
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var filePosted = httpRequest.Files[file];
                        db.ExpensePaymentFiles.Add(new ExpensePaymentFile() { PaymentId = payment.Id, FileUrl = Files.Upload(filePosted, "~/Images/ExpensePayments/") });
                        db.SaveChanges();
                    }
                }
                var files = new List<FileModel>();
                (from file in db.ExpensePaymentFiles where file.PaymentId == payment.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                 files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                return Json(new { success = true,paymentId = payment.Id,files}, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SaveExpense()
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            var httpRequest = HttpContext.Request;
            using (var db = new ApplicationDbContext())
            {
                var expense = JsonConvert.DeserializeObject<Expense>(httpRequest.Form.Get("expense"));
            var expenseId = 0;
            if (expense.Id != 0)
            {
                 expenseId=objIList.EditExpense(expense);
                var fileIds = JsonConvert.DeserializeObject<List<int>>(httpRequest.Form.Get("fileIds"));
                    if (fileIds.Count > 0)
                    {
                        db.ExpenseFiles.RemoveRange(db.ExpenseFiles.Where(x => x.ExpenseId == expense.Id && !fileIds.Contains(x.Id)));
                        db.SaveChanges();
                    }
            }
            else
            {
               expenseId= objIList.InsertExpense(expense);
            }
            
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var filePosted = httpRequest.Files[file];
                        db.ExpenseFiles.Add(new ExpenseFile() {ExpenseId =expenseId, FileUrl = Files.Upload(filePosted, "~/Images/Expense/") });
                        db.SaveChanges();
                    }
                }
                var files = new List<FileModel>();
                (from file in db.ExpenseFiles where file.ExpenseId == expenseId select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                 files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                return Json(new { expenseId,files }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult SaveScheduleMaintenanceExpense(PropertyScheduledExpense data)
        {
            var companyId = data.CompanyId;
            using (var db = new ApplicationDbContext())
            {
                if (data.Id != 0)
                {
                    var temp = db.PropertyScheduledExpenses.Where(x => x.Id == data.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.JobTypeId = data.JobTypeId;
                        temp.Amount = data.Amount;
                        temp.RecurringDate = data.RecurringDate;
                        temp.RecurringEndDate = data.RecurringEndDate;
                        temp.Description = data.Description;
                        temp.Frequency = data.Frequency;
                        temp.ExpenseCategoryId = data.ExpenseCategoryId;
                        temp.PaymentMethodId = data.PaymentMethodId;
                        temp.WorkerId = data.WorkerId;
                        temp.JobTypeId = data.JobTypeId;
                        temp.VendorId = data.VendorId;
                        //temp.DayOfWeek = (int)data.RecurringDate.DayOfWeek;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        var dataTemp = db.Expenses.Where(x =>
                                      x.PropertyScheduledExpenseId == temp.Id).FirstOrDefault();
                        if (temp.Type == 1 && dataTemp!=null)
                        {
                            var maintenanceEntry = new MaintenanceEntry()
                            {
                                Amount = temp.Amount,
                                Description = temp.Description,
                                DueDate = temp.RecurringDate,
                                PropertyId = temp.PropertyId,
                                JobTypeId = temp.JobTypeId,
                                VendorId = temp.VendorId,
                                WorkerId = temp.WorkerId,
                                PropertyScheduledExpenseId = temp.Id,
                                CompanyId = temp.CompanyId

                            };
                            Core.API.AllSite.MaintenanceEntries.Update(dataTemp.MaintenanceEntryId.Value, maintenanceEntry);

                            dataTemp.DueDate = temp.RecurringDate;
                            dataTemp.Description = temp.Description;
                            dataTemp.Amount = temp.Amount;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    data.CompanyId = companyId;
                    db.PropertyScheduledExpenses.Add(data);
                    db.SaveChanges();

                    #region Ontime
                    if (data.Frequency == (int)MaintenanceFrequency.Onetime)
                    {
                        #region Create MAintence and Worker Assignment
                        var assignmentId = 0;
                        var maintenanceEntryId = 0;
                        if (data.Type == 1)
                        {
                            if (data.WorkerId != null && data.JobTypeId != null)
                            {
                                WorkerAssignment assignment = new WorkerAssignment()
                                {
                                    CompanyId = companyId,
                                    WorkerId = data.WorkerId.ToInt(),
                                    JobTypeId = data.JobTypeId.ToInt(),
                                    StartDate = data.RecurringDate,
                                    EndDate = data.RecurringDate,
                                    PropertyId = data.PropertyId,

                                };
                                db.WorkerAssignments.Add(assignment);
                                db.SaveChanges();
                                assignmentId = assignment.Id;
                            }
                            var maintenanceEntry = new MaintenanceEntry()
                            {
                                Amount = data.Amount,
                                Description = data.Description,
                                DueDate = data.RecurringDate,
                                PropertyId = data.PropertyId,
                                JobTypeId = data.JobTypeId,
                                VendorId = data.VendorId,
                                WorkerId = data.WorkerId,
                                PropertyScheduledExpenseId = data.Id,
                                CompanyId = companyId
                            };
                            maintenanceEntryId = Core.API.AllSite.MaintenanceEntries.Add(maintenanceEntry);
                        }
                        #endregion
                        #region Create Expense
                        Expense model = new Expense
                        {
                            Amount = data.Amount,
                            Description = data.Description,
                            ExpenseCategoryId = data.ExpenseCategoryId,
                            PropertyId = data.PropertyId,
                            DueDate = data.RecurringDate,
                            PaymentDate = null,
                            IsCapitalExpense = false,
                            IsStartupCost = false,
                            Frequency = data.Frequency,
                            DateLastJobRun = null,
                            DateCreated = DateTime.Now,
                            DateNextJobRun = null,
                            PaymentMethodId = data.PaymentMethodId,
                            CompanyId = data.CompanyId,
                            IsPaid = false,
                            WorkerAssignmentId = assignmentId,
                            PropertyScheduledExpenseId = data.Id,
                            MaintenanceEntryId = maintenanceEntryId

                        };
                        db.Expenses.Add(model);
                        db.SaveChanges();
                        #endregion
                    }
                    #endregion
                }

            }

            return Json(new { maintenanceExpenseId =data.Id }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveIncomeBatchPayment()
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            var httpRequest = HttpContext.Request;
            var data =JsonConvert.DeserializeObject<IncomeBatchViewModel>(httpRequest.Form.Get("data"));
            int batchId = 0;
            var batch = data.IncomeBatchPayment;
            var payments = data.IncomePaymentList;
            using(var db = new ApplicationDbContext())
            {
                if (batch.Id == 0)
                {
                    db.IncomeBatchPayments.Add(batch);
                    db.SaveChanges();
                    batchId = batch.Id;
                }
                else
                {
                    var temp = db.IncomeBatchPayments.Where(x => x.Id == batch.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        batchId = temp.Id;
                        temp.PaymentDate = batch.PaymentDate;
                        temp.Description = batch.Description;
                        temp.Amount = batch.Amount;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    var fileIds = JsonConvert.DeserializeObject<List<int>>(httpRequest.Form.Get("fileIds"));
                    if (fileIds.Count > 0)
                    {
                        db.IncomePaymentFiles.RemoveRange(db.IncomePaymentFiles.Where(x =>x.BatchPaymentId == batch.Id && !fileIds.Contains(x.Id)));
                        db.SaveChanges();
                    }
                }
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var filePosted = httpRequest.Files[file];
                        db.IncomePaymentFiles.Add(new IncomePaymentFile() {BatchPaymentId =batchId, FileUrl = Files.Upload(filePosted, "~/Images/IncomePayments/") });
                        db.SaveChanges();
                    }
                }

                if (payments.Count>0 && payments != null)
                {
                    foreach(var payment in payments)
                    {
                        if (payment.Amount != 0)
                        {
                            if (payment.IncomePaymentId == 0)
                            {
                                db.IncomePayment.Add(new IncomePayment()
                                {
                                    Amount = payment.Amount,
                                    Description = batch.Description,
                                    CreatedAt = batch.PaymentDate,
                                    IncomeId = payment.IncomeId,
                                    IncomeBatchPaymentId = batchId,
                                    Type = (int)IncomePaymentType.Payment
                                });
                                db.SaveChanges();
                            }
                            else
                            {
                                var temp = db.IncomePayment.Where(x => x.Id == payment.IncomePaymentId).FirstOrDefault();
                                if (temp != null)
                                {
                                    temp.Amount = payment.Amount;
                                    temp.Description = batch.Description;
                                    temp.CreatedAt = batch.PaymentDate;
                                    db.Entry(temp).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                        else if (payment.Amount == 0 & payment.IncomePaymentId != 0)
                        {
                            db.IncomePayment.Remove(db.IncomePayment.Where(x => x.Id == payment.IncomePaymentId).FirstOrDefault());
                            db.SaveChanges();
                        }
                    }
                }

                var incomePayments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == batch.Id).ToList();
                payments.Clear();
                foreach (var payment in incomePayments)
                {
                    var income = db.Income.Where(x => x.Id == payment.IncomeId).FirstOrDefault();
                    if (income != null)
                    {
                        var totalpayment = db.IncomePayment.Where(x => x.IncomeId == income.Id).Sum(x => x.Amount);
                        payments.Add(new BatchIncomePaymentViewModel()
                        {
                            Balance = income.Amount.Value - totalpayment,
                            Amount = payment.Amount,
                            IncomeId = income.Id,
                            IncomePaymentId = payment.Id,
                            Description = income.Description,
                            DueDate = income.DueDate
                        });
                    }
                }
                batch.Files.Clear();
                (from file in db.IncomePaymentFiles where file.BatchPaymentId == batch.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                 batch.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                var batchPayment = (new IncomeBatchViewModel() { IncomeBatchPayment = batch, IncomePaymentList = payments});
                return Json(new { batchPayment}, JsonRequestBehavior.AllowGet);
            }
       
        }
        [HttpPost]
        public JsonResult SaveExpenseBatchPayment()
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
            var httpRequest = HttpContext.Request;
            var data = JsonConvert.DeserializeObject<ExpenseBatchViewModel>(httpRequest.Form.Get("data"));
            int batchId = 0;
            var batch = data.ExpenseBatchPayment;
            var payments = data.ExpensePaymentList;
            using (var db = new ApplicationDbContext())
            {
                if (batch.Id == 0)
                {
                    db.ExpenseBatchPayments.Add(batch);
                    db.SaveChanges();
                    batchId = batch.Id;
                }
                else
                {
                    var temp = db.ExpenseBatchPayments.Where(x => x.Id == batch.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        batchId = temp.Id;
                        temp.PaymentDate = batch.PaymentDate;
                        temp.Description = batch.Description;
                        temp.Amount = batch.Amount;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    var fileIds = JsonConvert.DeserializeObject<List<int>>(httpRequest.Form.Get("fileIds"));
                    if (fileIds.Count > 0)
                    {
                        db.ExpensePaymentFiles.RemoveRange(db.ExpensePaymentFiles.Where(x => x.BatchPaymentId == batch.Id && !fileIds.Contains(x.Id)));
                        db.SaveChanges();
                    }
                }
               
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var filePosted = httpRequest.Files[file];
                        db.ExpensePaymentFiles.Add(new ExpensePaymentFile() { BatchPaymentId = batchId, FileUrl = Files.Upload(filePosted, "~/Images/ExpensePayments/") });
                        db.SaveChanges();
                    }
                }

                if (payments.Count > 0 && payments != null)
                {
                    foreach (var payment in payments)
                    {
                        if (payment.Amount != 0)
                        {
                            if (payment.ExpensePaymentId == 0)
                            {
                                db.ExpensePayment.Add(new ExpensePayment()
                                {
                                    Amount = payment.Amount,
                                    Description = batch.Description,
                                    CreatedAt = batch.PaymentDate,
                                    ExpenseId = payment.ExpenseId,
                                    ExpenseBatchPaymentId = batchId,
                                });
                                db.SaveChanges();
                            }
                            else
                            {
                                var temp = db.ExpensePayment.Where(x => x.Id == payment.ExpensePaymentId).FirstOrDefault();
                                if (temp != null)
                                {
                                    temp.Amount = payment.Amount;
                                    temp.Description = batch.Description;
                                    temp.CreatedAt = batch.PaymentDate;
                                    db.Entry(temp).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                        else if(payment.Amount==0 & payment.ExpensePaymentId != 0)
                        {
                            db.ExpensePayment.Remove(db.ExpensePayment.Where(x => x.Id == payment.ExpensePaymentId).FirstOrDefault());
                            db.SaveChanges();
                        }
                    }
                }


                var ExpensePayments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batchId).ToList();
                payments.Clear();
                foreach (var payment in ExpensePayments)
                {
                    var expense = db.Expenses.Where(x => x.Id == payment.ExpenseId).FirstOrDefault();
                    if (expense != null)
                    {
                        var totalpayment = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).Sum(x => x.Amount);
                        payments.Add(new BatchExpensePaymentViewModel()
                        {
                            Balance = expense.Amount.Value - totalpayment,
                            Amount = payment.Amount,
                            ExpenseId = expense.Id,
                            ExpensePaymentId = payment.Id,
                            Description = expense.Description,
                            DueDate = expense.DueDate
                        });
                    }
                }
                batch.Files.Clear();
                (from file in db.ExpensePaymentFiles where file.BatchPaymentId == batch.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                 batch.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));

                var batchPayment = (new ExpenseBatchViewModel() { ExpenseBatchPayment = batch, ExpensePaymentList = payments });
                return Json(new { batchPayment }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetIncomeBatchPayment(int skip,string search,int userId,int reservationId)
        {
            using(var db=new ApplicationDbContext())
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                List<IncomeBatchViewModel> data = new List<IncomeBatchViewModel>(); 
                var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                List<IncomeBatchPayment> batchList;
                if (string.IsNullOrWhiteSpace(search))
                    batchList = db.IncomeBatchPayments.Where(x =>(reservationId ==0?true:x.BookingId==reservationId) && x.CompanyId == user.CompanyId).OrderByDescending(x => x.PaymentDate).Skip(skip).Take(5).ToList();
                else
                    batchList = db.IncomeBatchPayments.Where(x => (reservationId == 0 ? true : x.BookingId == reservationId) && x.Description.Contains(search) && x.CompanyId == user.CompanyId).OrderByDescending(x => x.PaymentDate).Skip(skip).Take(5).ToList();
                foreach (var batch in batchList)
                {
                    //Get files of income batch payment
                    (from file in db.IncomePaymentFiles where file.BatchPaymentId == batch.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                          batch.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                    var payments = new List<BatchIncomePaymentViewModel>();
                    var incomePayments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == batch.Id).ToList();

                    foreach (var payment in incomePayments)
                    {
                        var income = db.Income.Where(x => x.Id == payment.IncomeId).FirstOrDefault();
                        if (income != null)
                        {
                            var totalpayment = db.IncomePayment.Where(x => x.IncomeId == income.Id).Sum(x => x.Amount);
                            payments.Add(new BatchIncomePaymentViewModel()
                            {
                                Balance = income.Amount.Value -totalpayment,
                                Amount = payment.Amount,
                                IncomeId = income.Id,
                                IncomePaymentId = payment.Id,
                                Description = income.Description,
                                DueDate = income.DueDate
                            });
                        }
                    }
                    data.Add(new IncomeBatchViewModel() { IncomeBatchPayment = batch,IncomePaymentList=payments });
                }

                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetExpenseBatchPayment(int skip,string search,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                string Url = contextUrl.Scheme + "://" + contextUrl.Authority;
                List<ExpenseBatchViewModel> data = new List<ExpenseBatchViewModel>();
                var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                List<ExpenseBatchPayment> batchList;

                if (string.IsNullOrWhiteSpace(search))
                    batchList = db.ExpenseBatchPayments.Where(x => x.CompanyId == user.CompanyId).OrderByDescending(x=>x.PaymentDate).Skip(skip).Take(5).ToList();
                else
                    batchList = db.ExpenseBatchPayments.Where(x =>x.Description.Contains(search) && x.CompanyId == user.CompanyId).OrderByDescending(x => x.PaymentDate).Skip(skip).Take(5).ToList();
                foreach (var batch in batchList)
                {
                    (from file in db.ExpensePaymentFiles where file.BatchPaymentId == batch.Id select new { Id = file.Id, FileUrl = Url + file.FileUrl }).ToList().ForEach(x =>
                       batch.Files.Add(new FileModel() { Id = x.Id, FileUrl = x.FileUrl }));
                    var payments = new List<BatchExpensePaymentViewModel>();
                    var ExpensePayments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batch.Id).ToList();

                    foreach (var payment in ExpensePayments)
                    {
                        var expense = db.Expenses.Where(x => x.Id == payment.ExpenseId).FirstOrDefault();
                        if (expense != null)
                        {
                            var totalpayment = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).Sum(x=>x.Amount);
                            payments.Add(new BatchExpensePaymentViewModel()
                            {
                                Balance = expense.Amount.Value -totalpayment,
                                Amount = payment.Amount,
                                ExpenseId = expense.Id,
                                ExpensePaymentId = payment.Id,
                                Description = expense.Description,
                                DueDate = expense.DueDate
                            });
                        }
                    }
                    data.Add(new ExpenseBatchViewModel() { ExpenseBatchPayment = batch, ExpensePaymentList = payments });
                }

                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult EditMaintenanceEntry(MaintenanceEntry entry/*, string NoteString, HttpPostedFileWrapper[] Images*/)
        {
            using (var db = new ApplicationDbContext())
            {
                var maintenance = db.MaintenanceEntries.Where(x => x.Id == entry.Id).FirstOrDefault();
                maintenance.DueDate = entry.DueDate;
                maintenance.Amount = entry.Amount;
                maintenance.Description = entry.Description;
                maintenance.WorkerId = entry.WorkerId;
                maintenance.JobTypeId = entry.JobTypeId;
                maintenance.VendorId = entry.VendorId;
                maintenance.DateStart = entry.DateStart;
                maintenance.DateEnd = entry.DateEnd;
                maintenance.Status = entry.Status;
                db.Entry(maintenance).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var expense = db.Expenses.Where(x => x.MaintenanceEntryId == maintenance.Id).FirstOrDefault();

                if (expense != null)
                {
                    expense.Description = maintenance.Description;
                    expense.Amount = maintenance.Amount;
                    db.Entry(expense).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                var httpRequest = HttpContext.Request;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var filePosted = httpRequest.Files[file];
                        db.MaintenanceEntryFiles.Add(new MaintenanceEntryFile() { MaintenanceEntryId =maintenance.Id, FileUrl = UploadImage(filePosted, "~/Images/MaintenanceEntry/") });
                        db.SaveChanges();
                    }
                }
                //if (Images != null)
                //{
                //    foreach (var image in Images)
                //    {
                //        var Image = new MaintenanceEntryFile() { MaintenanceEntryId = maintenance.Id, FileUrl = UploadFile(image, "~/Images/MaintenanceEntry/") };
                //        db.MaintenanceEntryFiles.Add(Image);
                //        db.SaveChanges();
                //    }
                //}

                //var js = new JavaScriptSerializer();
                //var notes = js.Deserialize<List<MaintenanceEntryNote>>(NoteString);
                //var removeNoteIds = db.MaintenanceEntryNotes.Where(x => x.MaintenanceEntryId == maintenance.Id).Select(x => x.Id).ToList();
                //foreach (var note in notes)
                //{
                //    removeNoteIds.Remove(note.Id);
                //    var temp = db.MaintenanceEntryNotes.Where(x => x.Id == note.Id).FirstOrDefault();
                //    if (temp == null)
                //    {
                //        note.MaintenanceEntryId = maintenance.Id;
                //        db.MaintenanceEntryNotes.Add(note);
                //    }
                //    else
                //    {
                //        temp.Note = note.Note;
                //        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                //    }
                //    db.SaveChanges();
                //}
                ////JM 10/22/19 Remove deleted notes
                //foreach (var removeId in removeNoteIds)
                //{
                //    db.MaintenanceEntryNotes.Remove(db.MaintenanceEntryNotes.Where(x => x.Id == removeId).FirstOrDefault());
                //    db.SaveChanges();
                //}
                return Json(new { success = true, maintenance }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult GetContractByProperty(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var listingId = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault().ListingId;
                var rentals = (from reservation in db.Inquiries join renter in db.Renters on reservation.Id equals renter.BookingId where reservation.PropertyId == listingId select new { BookingId = reservation.Id, RenterName = renter.Firstname + " " + renter.Lastname, Checkin = reservation.CheckInDate }).ToList();

                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (var item in rentals)
                {
                    data.Add(item.BookingId.ToString(), item.RenterName + " - " + item.Checkin.Value.ToString("MMM dd,yyyy"));
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}