﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using MessagerSolution.Helper;
using MessagerSolution.Helper.ImagesCompress;
using MessagerSolution.Helper.Renters;
using MessagerSolution.Service.Gmail;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class EmailController : Controller
    {
        // GET: Email
       
        [HttpGet]
        public ActionResult GetEmailAccount()
        {
            using(var db = new ApplicationDbContext())
            {
                var data = db.EmailAccounts.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new {data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SyncEmail(int id)
        {
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                   "Syncing Emails...", NotificationType.INFORMATION);
            //EmailHandler.SyncEmail(id);
            GmailApi.Login(SessionHandler.CompanyId);
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                "Successfully Sync Emails", NotificationType.SUCCESS);
            return Json(new { success=true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddEmail(int renterId,string email)
        {
            Emails.AddOrUpdate(renterId, email);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveEmail(EmailAccount account)
        {
            using(var db = new ApplicationDbContext())
            {
                if (account.Id == 0)
                {
                    account.CompanyId = SessionHandler.CompanyId;
                    account.Password = Security.Encrypt(account.Password);
                    db.EmailAccounts.Add(account);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.EmailAccounts.Where(x => x.Id == account.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Email = account.Email;
                        if (!string.IsNullOrWhiteSpace(account.Password)){
                            temp.Password= Security.Encrypt(account.Password);
                        }
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            return Json(new {success=true}, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteEmail(int id)
        {
            using(var db = new ApplicationDbContext())
            {
                db.EmailAccounts.Remove(db.EmailAccounts.Where(x => x.Id == id).FirstOrDefault());
                db.SaveChanges();
            }
            return Json(new { success=true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Send(string threadId, string message, string renterEmail, HttpPostedFileBase[] Attachments)
        {
            using (var db = new ApplicationDbContext())
            {
                var account = db.EmailAccounts.Where(x => x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (account != null)
                {
                    if (renterEmail != null)
                    {
                        var attachments = new List<string>();
                        if (Attachments != null)
                        {
                            foreach (var file in Attachments)
                            {
                                var fAttachment = Files.Upload(file, "~/Images/EmailAttachment/");
                                attachments.Add(fAttachment);
                            }
                        }
                        var service = GmailApi.OAuth();
                        GmailApi.Send(service, message, renterEmail, "Rentals", attachments);

                        CommunicationThread workerThread;
                        if (threadId == "0")
                        {
                            workerThread = new CommunicationThread();
                            db.CommunicationThreads.Add(workerThread);
                            workerThread.IsRenter = true;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = SessionHandler.CompanyId;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                        else
                        {
                            workerThread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                        }
                        CommunicationEmail email = new CommunicationEmail
                        {
                            ThreadId = workerThread.ThreadId,
                            Message = message,
                            From = account.Email,
                            To = renterEmail,
                            CreatedDate = DateTime.Now,
                            IsMyMessage = true

                        };
                        db.CommunicationEmails.Add(email);
                        db.SaveChanges();
                        foreach (var f in attachments)
                        {
                            db.EmailAttachments.Add(new CommunicationEmailAttachment() { CommunicationEmailId = email.Id, FileUrl = f });
                            db.SaveChanges();
                        }

                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }


        }
    }
}