﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;
using Syroot.Windows.IO;
using Core.Extensions;
using System.Data.Entity.Core.Objects;
using Twilio.TwiML.Voice;
using System.Globalization;

namespace MessagerSolution.Controllers
{
    public class CalendarController : Controller
    {
        ICommonRepository objIList = new CommonRepository();

        Airbnb.ScrapeManager _airbnbSm;
        VRBO.ScrapeManager _vrboSm;

        Airbnb.ScrapeManager AirbnbScrapper
        {
            get
            {
                if (_airbnbSm == null)
                    _airbnbSm = new Airbnb.ScrapeManager();

                return _airbnbSm;
            }
        }

        VRBO.ScrapeManager VrboScrapper
        {
            get
            {
                if (_vrboSm == null)
                    _vrboSm = new VRBO.ScrapeManager();

                return _vrboSm;
            }
        }

        #region  
        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ScrapeCalendarFromIcal(string propertyId)
        {
            var hostId = Core.API.AllSite.Properties.GetByListingId(propertyId.ToLong()).HostId;
            using (var db = new ApplicationDbContext())
            {
                var _propertyId = propertyId.ToLong();
                var site = db.Properties.Where(x => x.ListingId == _propertyId).FirstOrDefault();

                var siteType = site.SiteType;

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                    //Set the parameter of API post request
                    JObject message = null;
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("hostId", hostId.ToString());
                    reqParams.Add("propertyId", propertyId);
                    reqParams.Add("siteType", siteType.ToString());


                    var reqContent = new FormUrlEncodedContent(reqParams);

                    var result = await client.PostAsync("Calendar/ScrapeCalendarFromIcal", reqContent);
                    if (result != null && result.Content != null)
                    {
                        //get result and parse it into JObject to get the content
                        message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        return Json(new { success = message["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                    }
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);

                }
            }


        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ExportCalendar(int propertyId, int roomId, string site)
        {

            List<string> calendar = new List<string>();
            string downloadsPath = new KnownFolder(KnownFolderType.Downloads).Path;
            if (System.IO.File.Exists(downloadsPath + propertyId + ".ics"))
            {
                System.IO.File.Delete(downloadsPath + propertyId + ".ics");

            }
            System.IO.Directory.CreateDirectory(Server.MapPath("~/App_Data/Ical/"));
            if (System.IO.File.Exists(Server.MapPath("~/App_Data/Ical/") + propertyId + ".ics"))
            {
                System.IO.File.Delete(Server.MapPath("~/App_Data/Ical/") + propertyId + ".ics");

            }


            using (var db = new ApplicationDbContext())
            {
                List<PropertyBookingDate> temp = new List<PropertyBookingDate>();

                if (roomId != 0)
                {
                    temp = db.PropertyBookingDate

                           .Where(x => x.PropertyId == propertyId && x.FromIcal == true && x.RoomId == roomId && x.IsAvailable == false)
                           .ToList();
                }
                else
                {
                    temp = db.PropertyBookingDate

                       .Where(x => x.PropertyId == propertyId && x.FromIcal == true && x.IsAvailable == false)
                       .ToList();
                }

                StringBuilder sb = new StringBuilder();

                sb.AppendLine("BEGIN:VCALENDAR");
                sb.AppendLine("VERSION:2.0");
                sb.AppendLine("PRODID:-//PropertyDB//PropertyDB Calendar//EN");
                sb.AppendLine("CALSCALE:GREGORIAN");

                foreach (var item in temp)
                {
                    sb.AppendLine("BEGIN:VEVENT");
                    sb.AppendLine("DTSTART;VALUE=DATE:" + item.Date.ToString("yyyyMMdd"));
                    sb.AppendLine("DTEND;VALUE=DATE:" + item.Date.ToString("yyyyMMdd"));
                    sb.AppendLine("UID;" + item.ReservationId + "@prod.respontage.com");
                    sb.AppendLine("SUMMARY:" + item.IcalTags == null ? "Blocked" : item.IcalTags);
                    sb.AppendLine("END:VEVENT");
                }

                sb.AppendLine("END:VCALENDAR");
                calendar.Add(sb.ToString());
            }


            return Json(new { success = true, data = calendar, property = propertyId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ChooseProperty(int siteType)
        {
            using (var db = new ApplicationDbContext())
            {
                List<Property> temp = new List<Property>();
                temp = db.Properties

                       .Where(x => x.SiteType == siteType)
                       .ToList();
                return Json(new { success = true, data = temp }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ChooseRoom(int property)
        {
            using (var db = new ApplicationDbContext())
            {
                List<Core.Database.Entity.Room> temp = new List<Core.Database.Entity.Room>();
                temp = db.Rooms
                       .Where(x => x.ListingId == property)
                       .ToList();
                return Json(new { success = true, data = temp }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ExportCalendarToSite(int propertyId)
        {
            var localIcalURL = "";
            try
            {
                string downloadsPath = new KnownFolder(KnownFolderType.Downloads).Path;
                var directory = new DirectoryInfo(downloadsPath);
                var myFile = (from f in directory.GetFiles()
                              orderby f.LastWriteTime descending
                              select f).First();

                var local = Path.Combine(downloadsPath, myFile.ToString());
                var remote = Path.Combine(Server.MapPath("~/App_Data/Ical/"), myFile.ToString());
                System.IO.File.Copy(local, remote);
                localIcalURL = Server.MapPath("~/App_Data/Ical/") + myFile.ToString();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }



            return Json(new { success = true, data = localIcalURL }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //
        // GET: /Calendar/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int companyId)
        {
            FormsIdentity fid = User.Identity as FormsIdentity;
            string plainTextUserData = fid.Ticket.UserData;
            ViewBag.UserRole = plainTextUserData.ToLower();
            ViewBag.UserId = User.Identity.Name;
            var filterProperties = new List<Property>();
            using (var db = new ApplicationDbContext())
            {
                var properties = (from h in db.Hosts
                                  join hc in db.HostCompanies on h.Id equals hc.HostId
                                  join p in db.Properties on h.Id equals p.HostId
                                  where (hc.CompanyId == SessionHandler.CompanyId && p.SiteType != 3) && p.IsActive
                                  select p).ToList();
                properties.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());
                foreach (var local in properties)
                {
                    var t = db.ParentChildProperties.Where(x => x.ChildPropertyId == local.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    if (t == null && local.IsParentProperty == false)
                        filterProperties.Add(local);
                    else if (local.IsParentProperty && local.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts)
                    {
                        filterProperties.Add(local);
                    }
                }
            }
            ViewBag.Properties = filterProperties;// objIList.GetProperties();
            ViewBag.Workers = objIList.GetWorkers();
            ViewBag.JobTypes = objIList.GetWorkerJobTypes();
            ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();
            ViewBag.StatusList = new SelectList(GetStatusList(), "Code", "Description");
            ViewBag.SiteTypeList = new SelectList(objIList.GetAllSiteType(), "Code", "Description");
            ViewBag.IsUserWorker = GlobalVariables.UserWorkerId != 0 ? true : false;
            if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }


        #region GetSiteTypeColor

        private string GetSiteTypeColor(int siteType)
        {
            switch (siteType)
            {
                case 1: return "<a class=\"ui pink empty tiny label\"></a>";
                case 2: return "<a class=\"ui green empty tiny label\"></a>";
                case 3: return "<a class=\"ui blue empty tiny label\"></a>";
                default: return "";
            }
        }
        #endregion


        #region GetCalendarResources

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetCalendarResources(string propertyId, bool showPricer, bool showWorker, bool showTravelEvents)
        {

            if (GlobalVariables.UserWorkerId != 0)
            {
                showPricer = false;
            }
            var companyId = SessionHandler.CompanyId;

            using (var db = new ApplicationDbContext())
            {
                string[] propertyIds = null;

                try { propertyIds = propertyId.Split(','); }
                catch (Exception e) { propertyIds = null; }

                List<Property> temp = new List<Property>();

                if (propertyIds == null || string.IsNullOrEmpty(propertyId) || propertyId == "0" || propertyIds.Contains("0"))
                {
                    temp = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where (hc.CompanyId == companyId && p.SiteType != 3) && p.IsActive
                            select p).ToList();
                    temp.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == companyId && x.IsActive).ToList());

                }
                else
                {
                    temp = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where (hc.CompanyId == companyId) && (p.SiteType != 3 && p.IsActive) && propertyIds.ToList().Contains(p.Id.ToString())
                            select p).ToList();
                    temp.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == companyId && propertyIds.ToList().Contains(x.Id.ToString())).ToList());
                }

                if (GlobalVariables.UserWorkerId != 0)
                {
                    var propertyIdWithReservation = db.WorkerAssignments.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).Select(x => x.PropertyId).Distinct().ToList();
                    //propertyIdWithReservation.Distinct();
                    foreach (var p in propertyIdWithReservation)
                    {
                        temp.RemoveAll(x => x.Id != p && !propertyIdWithReservation.Contains(x.Id));
                    }
                }

                List<ResourceData> resourceList = new List<ResourceData>();
                ResourceData obj = null;
                foreach (var item in temp)
                {
                    var pc = db.ParentChildProperties.Where(x => x.ChildPropertyId == item.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    // do not display child property linked to a parent property
                    //if (item.ParentPropertyId != 0) continue;
                    // do not display child property linked to a parent property
                    if (pc != null) continue; // do not display child property linked to a parent property

                    //List<Property> childProperties = db.Properties.Where(x => x.ParentPropertyId == item.Id && !x.IsParentProperty).ToList();
                    List<Property> childProperties = (from p in db.Properties join pcp in db.ParentChildProperties on p.Id equals pcp.ChildPropertyId where pcp.ParentPropertyId == item.Id select p).ToList();

                    obj = new ResourceData();
                    obj.id = item.Id + "";
                    obj.title = item.Name;

                    ResourceChildData resourceChild = new ResourceChildData();
                    obj.children = new List<ResourceChildData>();

                    if (showTravelEvents && ((item.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && item.IsParentProperty) || (item.IsParentProperty == false && item.SiteType != 0)))
                    {
                        resourceChild.id = obj.id + "a";
                        resourceChild.title = "Travel Events";
                        resourceChild.eventColor = "blue";
                        obj.children.Add(resourceChild);
                        resourceList.Add(obj);
                    }

                    if (showWorker)
                    {
                        resourceChild = new ResourceChildData();
                        resourceChild.id = obj.id + "b";
                        resourceChild.title = "Cleaner";
                        resourceChild.eventColor = "orange";
                        obj.children.Add(resourceChild);
                        resourceList.Add(obj);
                    }

                    if (showPricer && ((item.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && item.IsParentProperty) || (item.IsParentProperty == false && item.SiteType != 0)))
                    {
                        if (!item.IsParentProperty)
                        {
                            resourceChild = new ResourceChildData();
                            resourceChild.id = obj.id + "c";
                            resourceChild.title = "Set Price";
                            obj.children.Add(resourceChild);
                            resourceList.Add(obj);
                        }
                        else
                        {
                            foreach (var child in childProperties)
                            {
                                if (child.SiteType == 3)
                                {
                                    var rooms = Core.API.Booking.com.Properties.GetRoom(child.ListingId);

                                    foreach (var r in rooms)
                                    {
                                        resourceChild = new ResourceChildData();
                                        resourceChild.id = child.Id + "" + r.Id + "c";
                                        resourceChild.title =
                                            "Set Price\n( " + child.Name + " )\n( " + r.RoomName + " - " + r.RoomId + " )";
                                        obj.children.Add(resourceChild);
                                        resourceList.Add(obj);
                                    }
                                }
                                else
                                {
                                    var host = db.Hosts.Where(x => x.Id == child.HostId).FirstOrDefault();
                                    resourceChild = new ResourceChildData();
                                    resourceChild.id = child.Id + "c";
                                    resourceChild.title = host != null ? host.SiteType.ToSiteTypeText() + "-" + host.Username + /*"("+host.Firstname + " " + host.Lastname +")"+*/ "\n( " + child.Name + " )" : child.SiteType.ToSiteTypeText() + "\n( " + child.Name + " )";
                                    obj.children.Add(resourceChild);
                                    resourceList.Add(obj);
                                }
                            }
                        }
                    }

                    if (true) // showCCTV ?
                    {
                        resourceChild = new ResourceChildData();
                        resourceChild.id = obj.id + "d";
                        resourceChild.title = "CCTV";
                        resourceChild.eventColor = "green";
                        obj.children.Add(resourceChild);
                        resourceList.Add(obj);
                    }

                }

                List<Core.Database.Entity.Property> bookingComProperties = new List<Property>();

                if (propertyIds == null || propertyIds.Contains("0"))
                {
                    bookingComProperties = Core.API.Booking.com.Properties.List;
                }
                else
                {
                    var bdcProperties = Core.API.Booking.com.Properties.List;

                    foreach (var p in propertyIds)
                    {
                        var pResult = bdcProperties.Where(x => x.Id == p.ToInt()).FirstOrDefault();

                        if (pResult != null)
                        {
                            bookingComProperties.Add(pResult);
                        }
                    }
                }

                foreach (var p in bookingComProperties)
                {
                    var room = Core.API.Booking.com.Properties.GetRoom(p.ListingId);

                    //if (p.ParentPropertyId != 0) continue; // do not display child property linked to a parent property

                    //List<Property> childProperties = db.Properties.Where(x => x.ParentPropertyId == p.Id && !x.IsParentProperty).ToList();
                    var pc = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    // do not display child property linked to a parent property
                    //if (item.ParentPropertyId != 0) continue;
                    // do not display child property linked to a parent property
                    if (pc != null) continue; // do not display child property linked to a parent property

                    //List<Property> childProperties = db.Properties.Where(x => x.ParentPropertyId == item.Id && !x.IsParentProperty).ToList();
                    List<Property> childProperties = (from prop in db.Properties join pcp in db.ParentChildProperties on prop.Id equals pcp.ChildPropertyId where pcp.ParentPropertyId == p.Id select prop).ToList();

                    foreach (var r in room)
                    {

                        obj = new ResourceData();
                        obj.id = p.Id + "" + r.Id + "";
                        obj.title = p.Name + "\n( " + r.RoomName + " )";

                        ResourceChildData resourceChild = new ResourceChildData();
                        obj.children = new List<ResourceChildData>();

                        if (showTravelEvents && ((p.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && p.IsParentProperty) || (p.IsParentProperty == false && p.SiteType != 0)))
                        {
                            resourceChild.id = obj.id + "a";
                            resourceChild.title = "Travel Events";
                            resourceChild.eventColor = "blue";
                            obj.children.Add(resourceChild);
                            resourceList.Add(obj);
                        }

                        if (showWorker)
                        {
                            resourceChild = new ResourceChildData();
                            resourceChild.id = obj.id + "b";
                            resourceChild.title = "Cleaner";
                            resourceChild.eventColor = "orange";
                            obj.children.Add(resourceChild);
                            resourceList.Add(obj);
                        }

                        if (showPricer && ((p.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && p.IsParentProperty) || (p.IsParentProperty == false && p.SiteType != 0)))
                        {
                            if (!p.IsParentProperty)
                            {
                                resourceChild = new ResourceChildData();
                                resourceChild.id = obj.id + "c";
                                resourceChild.title = "Set Price";
                                obj.children.Add(resourceChild);
                                resourceList.Add(obj);
                            }
                            else
                            {
                                foreach (var child in childProperties)
                                {
                                    var host = db.Hosts.Where(x => x.Id == child.HostId).FirstOrDefault();
                                    resourceChild = new ResourceChildData();
                                    resourceChild.id = child.Id + "c";
                                    resourceChild.title = host.SiteType.ToSiteTypeText() + "-" + host.Username + /*"(" + host.Firstname + " " + host.Lastname + ")" +*/ "\n( " + child.Name + " )"; //"<img style=\"float: left\" class=\"ui mini circular image\" src=\"/Content/img\"" + getSiteName(host.SiteType).ToLower() + "400.png/>" + " " + host.Firstname + " " + host.Lastname + "\n( " + child.Name + " )";
                                    obj.children.Add(resourceChild);
                                    resourceList.Add(obj);
                                }
                            }
                        }

                        if (true) // showCCTV ?
                        {
                            resourceChild = new ResourceChildData();
                            resourceChild.id = obj.id + "d";
                            resourceChild.title = "CCTV";
                            resourceChild.eventColor = "green";
                            obj.children.Add(resourceChild);
                            resourceList.Add(obj);
                        }
                    }
                }

                var res1 = Json(resourceList, JsonRequestBehavior.AllowGet);
                return res1;
            }
        }

        #endregion

        #region GetCalendarEvents

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetCalendarEvents(string propertyId, string workerId, bool showBooking, bool showWorker,
            DateTime dateToDisplay)
        {
            var propertyIds = propertyId.Split(',').Select(Int32.Parse).ToList();
            string[] workerIds = workerId.Split(',');
            DateTime NextMonth = dateToDisplay.AddMonths(1);
            DateTime startDate = new DateTime(dateToDisplay.Year, dateToDisplay.Month, 1).AddDays(-7);
            DateTime endDate = new DateTime(NextMonth.Year, NextMonth.Month,
                 DateTime.DaysInMonth(NextMonth.Year, NextMonth.Month));


            using (var db = new ApplicationDbContext())
            {
                try
                {
                    int companyId = SessionHandler.CompanyId;
                    var ParentChildProperties = db.ParentChildProperties.Where(x => x.CompanyId == companyId).ToList();
                    var Inquiries = (from i in db.Inquiries join h in db.Hosts on i.HostId equals h.Id
                                     join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == companyId
                                     && i.CheckInDate >= startDate && i.CheckOutDate <= endDate
                                     select i
                                     ).ToList();
                    var assignmentReservationId = db.WorkerAssignments.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).Select(x => x.ReservationId).ToList();



                    List<EventData> eventList = new List<EventData>();
                    EventData obj = null;
                    //var guests = db.Guests.ToList();
                    //JM 3.23.20
                    var Properties = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where hc.CompanyId == companyId select p).ToList();
                    Properties.AddRange(db.Properties.Where(x => x.CompanyId == companyId && x.SiteType == 0).ToList());
                    if (!propertyId.Contains("-1") && showBooking)
                    {
                        //List<int> queryIds = new List<int>();

                        if (propertyIds != null && !string.IsNullOrEmpty(propertyId) && !propertyIds.Contains(0))
                        {
                            //searching for code
                            propertyIds.AddRange(db.ParentChildProperties.ToList().Where(x => propertyIds.Contains(x.ParentPropertyId) && x.CompanyId == companyId).Select(x => x.ChildPropertyId).ToList());
                            #region Deleted code
                            //queryIds.AddRange(db.ParentChildProperties.ToList().Where(x => propertyIds.Contains(x.ParentPropertyId) && x.CompanyId == companyId).Select(x => x.Id).ToList());

                            //foreach (int id in propertyIds)
                            //{
                            //    bool isParent = false;
                            //    try
                            //    {
                            //        isParent = (from p in db.Properties.ToList()
                            //                    where p.Id == id
                            //                         && p.IsParentProperty
                            //                    select true).FirstOrDefault();
                            //    }
                            //    catch (Exception err) { isParent = false; }

                            //   if (isParent)
                            //    {//Get Id of Child Property
                            //        queryIds.AddRange(db.ParentChildProperties.ToList().Where(x =>
                            //            x.ParentPropertyId== id && x.CompanyId == companyId).Select(x=>x.Id).ToList());
                            //    }
                            //    else queryIds.Add(id);
                            //}
                            #endregion
                        }
                        //var result = propertyIds == null || propertyIds.Contains(0)
                        //        ?
                        //        db.Inquiries.ToList()
                        //       .Join(db.Hosts, n => n.HostId, i => i.Id, (n, i) => new { Host = i, Inquiry = n })
                        //        .Join(db.HostCompanies, n => n.Host.Id, i => i.HostId, (n, i) => new { HostCompany = i, Host = n.Host, Inquiry = n.Inquiry })
                        //        .Join(db.Properties, x => x.Inquiry.PropertyId, y => y.ListingId, (x, y) => new { HostCompany = x.HostCompany, Host = x.Host, Inquiry = x.Inquiry, Property = y })
                        //        .Where(x => (x.Inquiry.BookingStatusCode.ToUpper() == "A" || x.Inquiry.BookingStatusCode.ToUpper() == "B") && x.Inquiry.CheckInDate != null && x.Inquiry.CheckOutDate != null && x.Inquiry.GuestId != 0 &&
                        //        x.HostCompany.CompanyId == companyId &&
                        //            (x.Inquiry.CheckInDate.Value >= startDate &&
                        //            x.Inquiry.CheckInDate.Value <= endDate)
                        //        ).Where(x => GlobalVariables.UserWorkerId != 0 ? assignmentReservationId.Contains(x.Inquiry.Id) : true)
                        //        .ToList()
                        //        :
                        //        db.Inquiries.ToList()
                        //         .Join(db.Hosts, n => n.HostId, i => i.Id, (n, i) => new { Host = i, Inquiry = n })
                        //        .Join(db.HostCompanies, n => n.Host.Id, i => i.HostId, (n, i) => new { HostCompany = i, Host = n.Host, Inquiry = n.Inquiry })
                        //        .Join(db.Properties, x => x.Inquiry.PropertyId, y => y.ListingId, (x, y) => new { HostCompany = x.HostCompany, Host = x.Host, Inquiry = x.Inquiry, Property = y })
                        //        .Where(x => x.Inquiry.BookingStatusCode.ToUpper() == "A" || x.Inquiry.BookingStatusCode.ToUpper() == "B"
                        //         && x.Inquiry.CheckInDate != null && x.Inquiry.CheckOutDate != null && x.Inquiry.GuestId != 0
                        //         && (x.HostCompany.CompanyId == companyId &&
                        //        (x.Inquiry.CheckInDate.Value >= startDate &&
                        //            x.Inquiry.CheckInDate.Value <= endDate)))
                        //        .Where(x => propertyIds.Contains(x.Property.Id))
                        //        .Where(x => GlobalVariables.UserWorkerId != 0 ? assignmentReservationId.Contains(x.Inquiry.Id) : true)
                        //        .ToList();

                        var result = (from i in Inquiries
                                      join h in db.Hosts on i.HostId equals h.Id
                                      join p in Properties on i.PropertyId equals p.ListingId
                                      join hc in db.HostCompanies on h.Id equals hc.HostId
                                      join g in db.Guests on i.GuestId equals g.Id
                                      where
                                      (propertyIds.Contains(0) ? true : propertyIds.Contains(p.Id))
                                      //** (i.CheckInDate >= startDate && i.CheckOutDate <= endDate)
                                      && (GlobalVariables.UserWorkerId != 0 ? assignmentReservationId.Contains(i.Id) : true)
                                      && (i.BookingStatusCode == "A" || i.BookingStatusCode == "B")
                                      && hc.CompanyId == companyId
                                      select new { Host = h, HostCompany = hc, Inquiry = i, Property = p, Guest =g }).ToList();
                        foreach (var item in result)
                        {
                            obj = new EventData();
                            obj.eventId = item.Inquiry.Id.ToString();
                            obj.threadId = item.Inquiry.ThreadId.ToString();
                            obj.hostId = item.Inquiry.HostId.ToString();
                            var ewqe = item.Inquiry.PropertyId;
                            var prop = Properties.Where(x => x.Id == item.Property.Id).FirstOrDefault();
                            if (prop != null)
                            {
                                var pc = ParentChildProperties.Where(x => x.ChildPropertyId == prop.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                                if (pc == null)
                                {
                                    obj.resourceId = item.Property.Id.ToString();
                                }
                                else
                                {
                                    obj.resourceId = pc.ParentPropertyId.ToString();
                                }
                            }
                            obj.type = "2";
                            obj.start = (item.Property.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)item.Inquiry.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(item.Property.TimeZoneName)) : (DateTime)item.Inquiry.CheckInDate).ToString("yyyy-MM-ddTHH:mm:ss");

                            // obj.start = Convert.ToDateTime(item.Inquiry.CheckInDate).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = (item.Property.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)item.Inquiry.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(item.Property.TimeZoneName)) : (DateTime)item.Inquiry.CheckOutDate).ToString("yyyy-MM-ddTHH:mm:ss");
                            //Convert.ToDateTime(item.Inquiry.CheckOutDate).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = item.Guest.Name;//guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault() == null ? "" : guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault().Name;
                            obj.image = item.Guest.ProfilePictureUrl;//guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault() == null ? "" : guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault().ProfilePictureUrl;
                            obj.propertyName = item.Property.Name;
                            obj.siteType = item.Property.SiteType;
                            obj.BookingStatus = item.Inquiry.BookingStatusCode;
                            if (!string.IsNullOrEmpty(obj.title))
                                eventList.Add(obj);
                        }

                        var Rentals = new List<Inquiry>();
                        if (propertyIds.Count > 0 && !propertyIds.Contains(0))
                        {
                            var localListing =Properties.Where(x => propertyIds.Contains(x.Id)).Select(x => x.ListingId).ToList();

                            Rentals = db.Inquiries.Where(x => x.SiteType == 0 && localListing.Contains(x.PropertyId)).ToList();
                        }
                        else
                        {
                            Rentals = (from property in db.Properties join r in db.Inquiries on property.ListingId equals r.PropertyId where property.SiteType ==0 && property.CompanyId == companyId select r).ToList();//db.Inquiries.Where(x => x.SiteType == 0).ToList();
                        }
                        foreach (var reservation in Rentals)
                        {

                            obj = new EventData();
                            obj.eventId = reservation.Id.ToString();
                            obj.threadId = reservation.ThreadId.ToString();
                            obj.hostId = reservation.HostId.ToString();
                            var ewqe = reservation.PropertyId;
                            var prop = Properties.Where(x => x.ListingId == reservation.PropertyId).FirstOrDefault();
                            if (prop != null)
                            {
                                var pc = ParentChildProperties.Where(x => x.ChildPropertyId == prop.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                                if (pc == null)
                                {
                                    obj.resourceId = prop.Id.ToString();
                                }
                                else
                                {
                                    obj.resourceId = pc.ParentPropertyId.ToString();
                                }
                            }
                            obj.type = "2";
                            obj.start = (prop.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)reservation.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(prop.TimeZoneName)) : (DateTime)reservation.CheckInDate).ToString("yyyy-MM-ddTHH:mm:ss");

                            // obj.start = Convert.ToDateTime(item.Inquiry.CheckInDate).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = (reservation.IsMonthToMonth || reservation.IsFixedMonthToMonth) ? reservation.CheckInDate.Value.AddYears(1).ToString("yyyy-MM-ddTHH:mm:ss") : (prop.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)reservation.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(prop.TimeZoneName)) : (DateTime)reservation.CheckOutDate).ToString("yyyy-MM-ddTHH:mm:ss");
                            //Convert.ToDateTime(item.Inquiry.CheckOutDate).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = db.Renters.Where(x => x.BookingId == reservation.Id).Select(x => new { Name = x.Firstname + " " + x.Lastname }).FirstOrDefault().Name;//guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault() == null ? "" : guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault().Name;
                            obj.image = null;// guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault() == null ? "" : guests.Where(x => x.Id == item.Inquiry.GuestId).FirstOrDefault().ProfilePictureUrl;
                            obj.propertyName = prop.Name;
                            obj.siteType = prop.SiteType;
                            obj.BookingStatus = reservation.BookingStatusCode;
                            if (!string.IsNullOrEmpty(obj.title))
                                eventList.Add(obj);
                        }
                    }


                    if (!workerId.Contains("-1"))
                    {
                        if (showWorker)
                        {
                            //var result2 = db.WorkerAssignments
                            //    .Join(db.Properties, wa => wa.PropertyId, p => p.Id, (wa, p) => new { wa, p })
                            //    .Join(db.Workers, wa => wa.wa.WorkerId, w => w.Id, (wa, w) => new { WorkerAssignment = wa.wa, Worker = w, Property = wa.p })
                            //    .Join(db.Inquiries, wa => wa.WorkerAssignment.ReservationId, i => i.Id, (wa, i) => new { WorkerAssignment = wa.WorkerAssignment, Worker = wa.Worker, Property = wa.Property, Inquiry = i })
                            //    .Where(w => w.Worker.CompanyId == companyId && (w.Inquiry.CheckInDate.Value >= startDate &&
                            //        w.Inquiry.CheckInDate.Value <= endDate))
                            //        .Where(x => GlobalVariables.UserWorkerId != 0 ? assignmentReservationId.Contains(x.Inquiry.Id) && x.WorkerAssignment.WorkerId == GlobalVariables.UserWorkerId : true)
                            //    .ToList();
                            //JM 3/23/20 Get all worker assignment between start and end date
                            var result2 = (from i in Inquiries join wa in db.WorkerAssignments on  i.Id equals wa.ReservationId
                                                  join w in db.Workers on wa.WorkerId equals w.Id 
                                                  join p in db.Properties on wa.PropertyId equals p.Id
                                                  where w.CompanyId == companyId &&
                                                  (wa.StartDate >= startDate &&
                                                   wa.EndDate <= endDate) &&
                                                   (GlobalVariables.UserWorkerId != 0 ? assignmentReservationId.Contains(i.Id) && wa.WorkerId == GlobalVariables.UserWorkerId : true)
                                                  select new { Assignment = wa, Worker = w, Property = p,Inquiry= i == null ? null: i }).ToList();
                            foreach (var item2 in result2)
                            {
                                if (item2.Inquiry == null)
                                {
                                }
                                if ((workerIds.Contains(item2.Worker.Id.ToString()) || workerIds.Contains("0") ||
                                    string.IsNullOrEmpty(workerId)) && showWorker)
                                {
                                    obj = new EventData();
                                    obj.eventId = item2.Inquiry.Id.ToString();
                                    obj.threadId = item2.Inquiry.ThreadId;
                                    obj.hostId = item2.Inquiry.HostId.ToString();

                                    var prop = Properties.Where(x => x.Id == item2.Property.Id).FirstOrDefault();
                                    if (prop != null)
                                    {
                                        if (prop != null)
                                        {
                                            var pc = ParentChildProperties.Where(x => x.ChildPropertyId == prop.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                                            if (pc == null)
                                            {
                                                obj.resourceId = item2.Property.Id + "b";
                                            }
                                            else
                                            {
                                                obj.resourceId = pc.ParentPropertyId + "b";
                                            }
                                        }

                                    }
                                    obj.scheduleId = item2.Assignment.Id.ToString();
                                    obj.type = "3";
                                    obj.start = item2.Assignment.StartDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                    obj.end = item2.Assignment.EndDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                    obj.title = item2.Worker.Firstname + " " + item2.Worker.Lastname;
                                    obj.image = item2.Worker.ImageUrl != null ? item2.Worker.ImageUrl : Url.Content("~/Content/img/eve.png");
                                    obj.propertyName = item2.Property.Name;
                                    obj.BookingStatus = item2.Inquiry.BookingStatusCode;
                                    obj.AssignmentStatus = item2.Assignment.Status;
                                    eventList.Add(obj);
                                }
                            }

                            //Display all Non booking assignment
                            var assignmentTemp = (from wa in db.WorkerAssignments
                                                  join w in db.Workers on wa.WorkerId equals w.Id
                                                  join p in db.Properties on wa.PropertyId equals p.Id
                                                  where w.CompanyId == companyId &&
                                                  (wa.StartDate >= startDate &&
                                                   wa.EndDate <= endDate) where wa.ReservationId == 0 && wa.CompanyId==companyId
                                                  select new { Assignment = wa, Worker = w, Property = p }).ToList();
                            foreach (var a in assignmentTemp)
                            {
                                obj = new EventData();
                                obj.scheduleId = a.Assignment.Id.ToString();
                                obj.type = "3";
                                obj.start = a.Assignment.StartDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                obj.end = a.Assignment.EndDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                obj.title = a.Worker.Firstname + " " + a.Worker.Lastname;
                                obj.image = a.Worker.ImageUrl != null ? a.Worker.ImageUrl : Url.Content("~/Content/img/eve.png");
                                obj.propertyName = a.Property.Name;
                                obj.BookingStatus = "A";
                                obj.AssignmentStatus = a.Assignment.Status;
                                var pc = ParentChildProperties.Where(x => x.ChildPropertyId == a.Property.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                                if (pc == null)
                                {
                                    obj.resourceId = a.Property.Id + "b";
                                }
                                else
                                {
                                    obj.resourceId = pc.ParentPropertyId + "b";
                                }

                                eventList.Add(obj);
                            }
                        }
                    }

                    //var result3 = db.tbNote
                    //    .Join(db.Inquiries, n => n.ReservationId, i => i.Id, (n, i) => new { Note = n, Inquiry = i })
                    //    .Join(db.Hosts, n => n.Inquiry.HostId, i => i.Id, (n, i) => new { Host = i, Note = n.Note, Inquiry = n.Inquiry })
                    //    .Join(db.HostCompanies, n => n.Host.Id, i => i.HostId, (n, i) => new { HostCompany = i, Host = n.Host, Note = n.Note, Inquiry = n.Inquiry })
                    //    .Join(db.Properties, x => x.Inquiry.PropertyId, y => y.ListingId, (x, y) => new { HostCompany = x.HostCompany, Host = x.Host, Note = x.Note, Inquiry = x.Inquiry, Property = y })
                    //    .Where(x => x.HostCompany.CompanyId == companyId && (x.Inquiry.CheckInDate.Value >= startDate &&
                    //            x.Inquiry.CheckInDate.Value <= endDate))
                    //    .ToList();
                    var result3 = (from i in Inquiries
                                    join h in db.Hosts on i.HostId equals h.Id
                                    join hc in db.HostCompanies on h.Id equals hc.HostId
                                    join n in db.tbNote on i.Id equals n.ReservationId
                                    join p in db.Properties on i.PropertyId equals p.ListingId
                                    where hc.CompanyId ==companyId && i.CheckInDate.Value>=startDate && i.CheckOutDate.Value <=endDate
                                    select new { Inquiry = i, Note = n,Host = h,Property = p }).ToList();

                    foreach (var item3 in result3)
                    {
                        obj = new EventData();
                        var prop = Properties.Where(x => x.ListingId == item3.Inquiry.PropertyId).FirstOrDefault();
                        if (prop != null)
                        {
                            var pc = ParentChildProperties.Where(x => x.ChildPropertyId == prop.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                            if (pc == null)
                            {
                                obj.resourceId = item3.Property.Id + "a";
                            }
                            else
                            {
                                obj.resourceId = pc.ParentPropertyId + "a";
                            }
                        }

                        obj.type = "1";
                        obj.propertyName = item3.Property.Name;
                        obj.eventId = item3.Inquiry.Id.ToString();
                        obj.threadId = item3.Inquiry.ThreadId;
                        obj.hostId = item3.Inquiry.HostId.ToString();

                        if (item3.Note.NoteType == 2)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.ArrivalDateTime).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.ArrivalDateTime).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Flight Arrival";
                            obj.noteType = "2";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.ArrivalDateTime).ToString("MMMM dd, yyyy hh:mm tt") + "\n" + "Name: " + item3.Note.People + "\nAirline: " + item3.Note.Airline + "\nFlight No.:" + item3.Note.FlightNumber;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 3)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.DepartureDateTime).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.DepartureDateTime).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Flight Departure";
                            obj.noteType = "2";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.DepartureDateTime).ToString("MMMM dd, yyyy hh:mm tt") + "\n" + "Name: " + item3.Note.People + "\nAirline: " + item3.Note.Airline + "\nFlight No.:" + item3.Note.FlightNumber;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 4)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.MeetingDateTime).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.MeetingDateTime).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Meeting";
                            obj.noteType = "1";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.MeetingDateTime).ToString("MMMM dd, yyyy hh:mm tt") + "\n Name:" + item3.Note.People + "\n Place:" + item3.Note.MeetingPlace;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 5)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.ArrivalDateTime).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.ArrivalDateTime).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Drive in";
                            obj.noteType = "4";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.ArrivalDateTime).ToString("MMMM dd, yyyy hh:mm tt") + "\nDeparture City:" + item3.Note.DepartureCity;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 6)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.ArrivalDateTime).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.ArrivalDateTime).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Cruise";
                            obj.noteType = "3";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.ArrivalDateTime).ToString("MMMM dd, yyyy hh:mm tt") + "\nName:" + item3.Note.People + "\nCruise name:" + item3.Note.CruiseName;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 13)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.Checkin).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.Checkin).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Self Checkin";
                            obj.noteType = "5";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.Checkin).ToString("MMMM dd, yyyy hh:mm tt") + "\nName:" + item3.Note.ContactPerson;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 14)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.Checkout).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.Checkout).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Self Checkout";
                            obj.noteType = "5";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.Checkout).ToString("MMMM dd, yyyy hh:mm tt") + "\nName:" + item3.Note.ContactPerson;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 13)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.Checkin).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.Checkin).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Self Checkin";
                            obj.noteType = "5";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.Checkin).ToString("MMMM dd, yyyy hh:mm tt") + "\nName:" + item3.Note.ContactPerson;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                        else if (item3.Note.NoteType == 14)
                        {
                            obj.start = Convert.ToDateTime(item3.Note.Checkout).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.end = Convert.ToDateTime(item3.Note.Checkout).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                            obj.title = "Self Checkout";
                            obj.noteType = "5";
                            obj.note = "Date:" + Convert.ToDateTime(item3.Note.Checkout).ToString("MMMM dd, yyyy hh:mm tt") + "\nName:" + item3.Note.ContactPerson;
                            obj.BookingStatus = item3.Inquiry.BookingStatusCode;
                            eventList.Add(obj);
                        }
                    }


                    var result4 = (from h in db.Hosts
                                   join hc in db.HostCompanies on h.Id equals hc.HostId
                                   join p in db.Properties on h.Id equals p.HostId
                                   join pbd in db.PropertyBookingDate on p.ListingId equals pbd.PropertyId
                                   where
                                     hc.CompanyId == companyId && ((pbd.Date >= startDate &&
                                 pbd.Date <= endDate))
                                   select new
                                   {
                                       PROPERTYBOOKINGDATE = pbd,
                                       PROPERTY = p
                                   }).ToList();

                    var roomDictionary = Core.API.Booking.com.Properties.GetRoom();
                    string currentProperty = "";
                    int count = 0;
                    foreach (var item in result4)
                    {

                        if (item.PROPERTY.SiteType == 3)
                        {

                        }
                        if (item.PROPERTY.IsParentProperty)
                            continue;
                        else
                        {
                            RegionInfo region = new RegionInfo(!string.IsNullOrEmpty(item.PROPERTY.CountryCode)?item.PROPERTY.CountryCode:"CA");
                            
                            obj = new EventData();
                            obj.eventId = item.PROPERTYBOOKINGDATE.Id.ToString();
                            obj.date = item.PROPERTYBOOKINGDATE.Date.ToString("yyyy-MM-dd 00:00:00");
                            obj.price = item.PROPERTYBOOKINGDATE.Price;
                            obj.isAvailable = item.PROPERTYBOOKINGDATE.IsAvailable;
                            obj.title = (item.PROPERTYBOOKINGDATE.IsAvailable ? region.CurrencySymbol+""+item.PROPERTYBOOKINGDATE.Price.ToString("N2") : "");
                            obj.propertyId = item.PROPERTY.ListingId.ToString();
                            obj.latitude = item.PROPERTY.Latitude;
                            obj.longitude = item.PROPERTY.Longitude;
                            obj.bedrooms = item.PROPERTY.Bedrooms;

                            obj.resourceId = item.PROPERTY.SiteType == 3 ? item.PROPERTY.Id + "" + roomDictionary[item.PROPERTY.ListingId + "" + (long)item.PROPERTYBOOKINGDATE.RoomId].Id + "c" : item.PROPERTY.Id + "c";
                            obj.type = "4";
                            obj.propertyName = item.PROPERTY.Name;
                            obj.siteType = item.PROPERTY.SiteType;
                            obj.vrboResId = item.PROPERTYBOOKINGDATE.ReservationId;
                            obj.roomId = item.PROPERTYBOOKINGDATE.RoomId;
                            obj.hostId = item.PROPERTY.HostId.ToString();

                            eventList.Add(obj);

                            if (item.PROPERTY.Name != currentProperty)
                            {
                                count = 1;
                                currentProperty = item.PROPERTY.Name;
                            }
                            else
                            {
                                count++;
                            }
                        }
                    }
                    //Remove because we use properties is also get
                    //var result5 = (from h in db.Hosts
                    //               join hc in db.HostCompanies on h.Id equals hc.HostId
                    //               join p in db.Properties on h.Id equals p.HostId
                    //               where
                    //                 hc.CompanyId == companyId
                    //               select p).ToList();
                    //result5.AddRange(db.Properties.Where(x => x.CompanyId == companyId && ((x.IsParentProperty && x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts) || x.IsParentProperty == false)).ToList());
                    //var today = DateTime.Now;
                    //var firsday = new DateTime(today.Year, today.Month, 1);
                    var listings = Properties.Select(x => x.ListingId).ToList();
                    var videos = (from d in db.Devices
                                  join v in db.Videos on d.Id equals v.LocalDeviceId
                                  where listings.Contains(d.ListingId.Value) && EntityFunctions.TruncateTime(v.DateCreated) >= EntityFunctions.TruncateTime(startDate)
                                  && EntityFunctions.TruncateTime(v.DateCreated) <= EntityFunctions.TruncateTime(endDate) && v.IsDeleted ==false
                                  select new { Video =v, Device = d}).ToList();

                    for (DateTime date = startDate; date <= DateTime.Now; date = date.AddDays(1))
                    {
                        var listingIdHasVideo = videos.Where(v => v.Video.DateCreated.Date ==date.Date).Select(x=>x.Device.ListingId).Distinct().ToList();
                        //Check if there`s any video
                        foreach(var listingId  in listingIdHasVideo)
                        {
                            var p = Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                            if (p != null)
                            {
                                obj = new EventData();
                                obj.date = date.ToString("MMMM dd,yyyy");
                                obj.hasFootage = true;
                                obj.propertyId = p.ListingId.ToString();
                                obj.propertyName = p.Name;
                                var pc = ParentChildProperties.Where(x => x.ChildPropertyId == p.Id).FirstOrDefault();
                                if (pc == null)
                                {
                                    if (p.TimeZoneId != null && !string.IsNullOrWhiteSpace(p.TimeZoneId))
                                    {
                                        obj.timezoneId =p.TimeZoneId;
                                    }
                                    else
                                    {
                                        var child = db.ParentChildProperties.Where(x => x.ParentPropertyId == p.Id).Select(x => x.ChildPropertyId).ToList();
                                        var pro = db.Properties.Where(x => child.Contains(x.Id) && x.TimeZoneId != null).FirstOrDefault();
                                        if (pro != null)
                                        {
                                            obj.timezoneId = pro.TimeZoneId;
                                        }
                                    }
                                    obj.resourceId =p.Id + "d";
                                }
                                else
                                {
                                    obj.resourceId = pc.ParentPropertyId + "d";
                                    var parent = db.Properties.Where(x => x.Id == pc.ParentPropertyId).FirstOrDefault();
                                    if (parent.TimeZoneId != null && !string.IsNullOrWhiteSpace(parent.TimeZoneId))
                                    {
                                        obj.timezoneId = parent.TimeZoneId;
                                    }
                                    else
                                    {
                                        var child = db.ParentChildProperties.Where(x => x.ParentPropertyId == pc.ParentPropertyId).Select(x => x.ChildPropertyId).ToList();
                                        var pro = db.Properties.Where(x => child.Contains(x.Id) && x.TimeZoneId != null).FirstOrDefault();
                                        if(pro != null)
                                        {
                                            obj.timezoneId = pro.TimeZoneId;
                                        }
                                    }
                                }
                                obj.type = "5";
                                obj.hostId =p.HostId.ToString();
                                eventList.Add(obj);
                            }
                        }
                       
                    }
                    //JM 3/26/2020 I can`t see its purpose
                    //count = 0;
                    //foreach (var item in Properties)
                    //{
                    //    if (item.SiteType == 3)
                    //    {
                    //        var rooms = Core.API.Booking.com.Properties.GetRoom(item.ListingId);

                    //        #region Empty Cells

                    //        foreach (var r in rooms)
                    //        {
                    //            for (DateTime date = DateTime.Now; date <= DateTime.Now.AddMonths(3); date = date.AddDays(1))
                    //            {
                    //                bool cellExist = (from e in eventList
                    //                                  where
                    //                                    e.propertyId == item.ListingId.ToString() &&
                    //                                    e.date == date.ToString("yyyy-MM-dd 00:00:00")
                    //                                  select e).Any();

                    //                if (!cellExist)
                    //                {
                    //                    obj = new EventData();
                    //                    obj.type = "-1";
                    //                    //obj.start = date.ToString("yyyy-MM-ddT00:00:00");
                    //                    //obj.end = date.AddDays(1).ToString("yyyy-MM-ddT00:00:00");
                    //                    obj.date = date.ToString("yyyy-MM-dd 00:00:00");
                    //                    obj.price = 10000.00;
                    //                    obj.isAvailable = true;
                    //                    obj.title = "";
                    //                    obj.propertyId = item.ListingId.ToString();
                    //                    obj.latitude = item.Latitude;
                    //                    obj.longitude = item.Longitude;
                    //                    obj.bedrooms = item.Bedrooms;
                    //                    obj.resourceId = item.SiteType == 3 ? item.Id + r.Id + "c" : item.Id + "c";
                    //                    obj.propertyName = item.Name;
                    //                    obj.siteType = item.SiteType;
                    //                    obj.hostId = item.HostId.ToString();
                    //                    eventList.Add(obj);
                    //                    count++;
                    //                }
                    //            }
                    //        }

                    //        #endregion
                    //    }
                    //    else
                    //    {
                    //        #region Empty Cells
                    //        //JM 3/26/20 Check if date is block or have reservation
                    //        for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                    //        {
                    //            bool cellExist = (from e in eventList
                    //                              where
                    //                                e.propertyId == item.ListingId.ToString() &&
                    //                                e.date == date.ToString("yyyy-MM-dd 00:00:00")
                    //                              select e).Any();

                    //            if (!cellExist)
                    //            {
                    //                obj = new EventData();
                    //                obj.type = "-1";
                    //                //obj.start = date.ToString("yyyy-MM-ddT00:00:00");
                    //                //obj.end = date.AddDays(1).ToString("yyyy-MM-ddT00:00:00");
                    //                obj.date = date.ToString("yyyy-MM-dd 00:00:00");
                    //                obj.price = 10000.00;
                    //                obj.isAvailable = true;
                    //                obj.title = "";
                    //                obj.propertyId = item.ListingId.ToString();
                    //                obj.latitude = item.Latitude;
                    //                obj.longitude = item.Longitude;
                    //                obj.bedrooms = item.Bedrooms;
                    //                obj.resourceId = item.Id + "c";
                    //                obj.propertyName = item.Name;
                    //                obj.siteType = item.SiteType;
                    //                obj.hostId = item.HostId.ToString();
                    //                eventList.Add(obj);
                    //                count++;
                    //            }
                    //        }

                    //        #endregion
                    //    }
                    //}
                    EventData fr = eventList.First();
                    JsonResult json = Json(eventList, JsonRequestBehavior.AllowGet);
                    json.MaxJsonLength = int.MaxValue;
                    return json;
                }
                catch (Exception e)
                {
                    return Json(new List<EventData>(), JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region SetPrice
        //Call the API Set Price in Api project
        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> SetPrice(int hostId, long propertyId, DateTime from, DateTime? to, bool isAvailable,
            double price, int siteType, bool syncChangesToOtherChild, long? roomId = null)
        {

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                //Set the parameter of API post request
                JObject message = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("propertyId", propertyId.ToString());
                reqParams.Add("from", from.ToString());
                reqParams.Add("to", to.ToString());
                reqParams.Add("isAvailable", isAvailable.ToString());
                reqParams.Add("price", price.ToString());
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("syncChangesToOtherChild", syncChangesToOtherChild.ToString());
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Calendar/SetPrice", reqContent);
                if (result != null && result.Content != null)
                {
                    //get result and parse it into JObject to get the content
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return Json(new { success = message["success"].ToBoolean(), message = message["message"].ToString() }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);

            }


        }

        #endregion

        #region BlockDate

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> BlockDate(int hostId, long propertyId, DateTime from, DateTime? to, bool notBlocked,
            long price, int siteType, bool syncChangesToOtherChild, string resId, long? roomId = null)
        {
            //Call the API BlockDate in Api project
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject message = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("propertyId", propertyId.ToString());
                reqParams.Add("from", from.ToString());
                reqParams.Add("to", to.ToString());
                reqParams.Add("notBlocked", notBlocked.ToString());
                reqParams.Add("price", price.ToString());
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("syncChangesToOtherChild", syncChangesToOtherChild.ToString());
                reqParams.Add("resId", resId);
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Calendar/BlockDate", reqContent);
                if (result != null && result.Content != null)
                {
                    //get result and parse it into JObject to get the content
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return Json(new { success = message["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                }

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }




        }

        #endregion

        #region UpdateStandardRate

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UpdateStandardRate(double rate)
        {
            try
            {
                Core.API.AnySite.DefaultRate = rate;
                return Json(true);
            }
            catch (Exception err)
            {
                return Json(false);
            }
        }


        #endregion

        #region UpdateSpecificDate

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> UpdateSpecificDate(int id, double price)
        {
            //public ActionResult SetPrice(int hostId, long propertyId, DateTime from, DateTime? to, bool isAvailable,
            //double price, int siteType, bool syncChangesToOtherChild)
            Tuple<bool, Core.Database.Entity.PropertyBookingDate> updateResult =
                Core.API.AllSite.PropertyBookingDate.Update(new PropertyBookingDate { Id = id, Price = price });

            bool success = updateResult.Item1;
            var bookingDate = updateResult.Item2;
            Core.Database.Entity.Property property = Core.API.AllSite.Properties.GetByListingId(bookingDate.PropertyId);

            if (success)
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(bookingDate.SiteType));
                    //Set the parameter of API post request
                    JObject message = null;
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("hostId", property.HostId.ToString());
                    reqParams.Add("propertyId", property.ListingId.ToString());
                    reqParams.Add("from", bookingDate.Date.ToString("yyyy-MM-dd"));
                    reqParams.Add("to", bookingDate.Date.ToString("yyyy-MM-dd"));
                    reqParams.Add("isAvailable", bookingDate.IsAvailable.ToString());
                    reqParams.Add("price", price.ToString());
                    reqParams.Add("siteType", bookingDate.SiteType.ToString());
                    reqParams.Add("syncChangesToOtherChild", false.ToString());
                    var reqContent = new FormUrlEncodedContent(reqParams);

                    var result = await client.PostAsync("Calendar/SetPrice", reqContent);
                    if (result != null && result.Content != null)
                    {
                        //get result and parse it into JObject to get the content
                        message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    }
                }
            }

            return Json(new { success = success, price = price, id = id }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetGuestConversation(int guestId)
        {
            using (var db = new ApplicationDbContext())
            {
                var inbox = db.Inbox.Where(x => x.GuestId == guestId).FirstOrDefault();
                if (inbox != null)
                {
                    var threadId = inbox.ThreadId;
                    var messages = db.Messages.Where(x => x.ThreadId == threadId).OrderBy(x => x.CreatedDate).ToList();
                    return Json(new { success = true, inbox = inbox, messages = messages }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult LoadAvailableWorkers(int propertyId, int inquiryId)
        {
            int sessionCompanyId = SessionHandler.CompanyId;

            using (var db = new ApplicationDbContext())
            {
                var res = db.Workers
                    .Where(x => x.CompanyId == sessionCompanyId)
                    .ToList();

                var temp = db.WorkerAssignments
                    .Where(x => x.PropertyId == propertyId)
                    .Where(x => x.ReservationId == inquiryId)
                    .Where(x => x.CompanyId == sessionCompanyId)
                    .FirstOrDefault();

                return Json(new { workers = res, assigned = temp }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AssignWorker(WorkerAssignment model)
        {
            using (var db = new ApplicationDbContext())
            {
                //var temp = db.WorkerAssignments
                //    .Where(x => x.InquiryId == model.InquiryId)
                //    .Where(x => x.PropertyId == model.PropertyId)
                //    .FirstOrDefault();

                //if(temp != null)
                //{
                //    temp.StartDate = model.StartDate;
                //    temp.EndDate = model.EndDate;
                //    temp.WorkerId = model.WorkerId;
                //    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                //}
                //else
                {
                    model.CompanyId = SessionHandler.CompanyId;
                    db.WorkerAssignments.Add(model);
                }
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public IEnumerable<BookingStatus> GetStatusList()
        {
            using (var db = new ApplicationDbContext())
            {
                List<BookingStatus> ListItems = (from list in db.BookingStatus where list.Description != null select list).AsEnumerable().Select(p => new BookingStatus
                {
                    Code = p.Code,
                    Description = p.Description
                }).ToList();
                return ListItems;
            }
        }

        public ActionResult MultiCalendar()
        {
            ViewBag.Properties = objIList.GetProperties();

            return View();
        }
    }
}