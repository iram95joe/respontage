﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
//using MessagerSolution.Models.Reports.Cashflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MessagerSolution.Enumerations.Cashflow;
using MessagerSolution.Models.Reports.IncomeExpense;
using Core.Models.Reports;
using Core.Models.Reports.Profit;
using MessagerSolution.Models.Reports.Commission;
using MessagerSolution.Enumerations.Commission;
using MessagerSolution.Utilities;
using System.Web.Script.Serialization;
using Core.Enumerations;
using Newtonsoft.Json;

namespace MessagerSolution.Controllers
{
    public class ReportController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        //
        // GET: /Report/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index()
        {
            var propertyList = new List<Property>();
            var propertyFilter = new List<Property>();
            var isOwner = Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt());
            using (var db = new ApplicationDbContext())
            {
                var userId = User.Identity.Name.ToInt();
                ViewBag.CashflowSaveCriteria = db.SearchCriterias.Where(x => x.UserId == userId && x.Type == (int)SearchCriteriaType.Cashflow).ToList();
                ViewBag.IncomeExpenseSaveCriteria = db.SearchCriterias.Where(x => x.UserId == userId && x.Type == (int)SearchCriteriaType.IncomeAndExpense).ToList();
                if (isOwner == false)
                {
                    propertyList = (from h in db.Hosts
                                    join hc in db.HostCompanies on h.Id equals hc.HostId
                                    join p in db.Properties on h.Id equals p.HostId
                                    where hc.CompanyId == SessionHandler.CompanyId
                                    select p).ToList();
                    propertyList.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 0).ToList());

                    foreach (var p in propertyList)
                    {
                        p.Name = p.IsParentProperty ? p.Name + "(Parent)" : p.Name;
                        propertyFilter.Add(p);
                    }
                }
                else
                {
                    propertyFilter.AddRange(Core.Helper.User.Owners.GetProperties(User.Identity.Name.ToInt()));
                }
            }
            ViewBag.Properties = propertyFilter;
            ViewBag.ExpenseCategory = GetExpenseCategories();
            return View();
        }

        [NonAction]
        private List<Core.Database.Entity.ExpenseCategory> GetExpenseCategories()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.ExpenseCategories.Where(x => x.CompanyId == SessionHandler.CompanyId || x.CompanyId == 0).ToList();
            }
        }
        [HttpGet]
        public ActionResult SearchCriteriaGetData(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.SearchCriterias.Where(x => x.Id == id).FirstOrDefault();
                return Json(new { success = true, searchCriteria = data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveSearch(SearchCriteria model)
        {

            using (var db = new ApplicationDbContext())
            {
                model.UserId = User.Identity.Name.ToInt();
                db.SearchCriterias.Add(model);
                db.SaveChanges();
            }
            return Json(new { searchCriteria = model }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetIncomeExpenseInfoByMonth(DateTime date, string propertyIds, bool isPaidOnly, bool includeChild)
        {
            List<DateTime> monthYearRange = new List<DateTime>();
            int status = (int)StatusEnum.FAIL;

            List<IncomeExpenseViewModel> incomeExpenseInfoList = new List<IncomeExpenseViewModel>();
            var js = new JavaScriptSerializer();
            var Ids = js.Deserialize<List<int>>(propertyIds);
            try
            {

                foreach (var propertyId in Ids)
                {

                    var incomeData = objIList.GetIncomeInfo(propertyId, isPaidOnly, includeChild);
                    var expenseData = objIList.GetExpenseInfo(propertyId, isPaidOnly, includeChild);
                    var incomeInfo = (from income in incomeData where income.DueDate.Month == date.Month && income.DueDate.Year == date.Year select income).ToList();
                    var expenseInfo = (from expense in expenseData where expense.DueDate.Month == date.Month && expense.DueDate.Year == date.Year select expense).ToList();
                    var totalExpenseAmount = 0.0M;
                    var totalIncomeAmount = 0.0M;
                    var startupCostRefund = 0.0M;
                    var expenseRefund = 0.0M;
                    var incomeRefund = 0.0M;
                    var totalStartupCost = 0.0M;
                    var IncomeSummary = "<h2>Income</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    var IncomeRefundSummary = "<h2>Income Refund</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    var ExpenseSummary = "<h2>Expense</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    var ExpenseRefundSummary = "<h2>Expense Refund</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    var StartUpCostSummary = "<h2>Startup Cost</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    var StartupCostRefundSummary = "<h2>Startup Cost Refund</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";

                    if (isPaidOnly)
                    {
                        using (var db = new ApplicationDbContext())
                        {  //check if income is paid
                            foreach (var temp in incomeInfo.Where(x => x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund))
                            {
                                var payments = db.IncomePayment.Where(x => x.IncomeId == temp.Id && x.IsRejected == false).ToList();
                                if (payments.Sum(x => x.Amount) > 0.0M)
                                {
                                    IncomeSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == temp.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                    totalIncomeAmount += payments.Sum(x => x.Amount);
                                }
                            }
                            //check if income refund is paid
                            foreach (var temp in incomeInfo.Where(x => x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund))
                            {
                                var payments = db.IncomePayment.Where(x => x.IncomeId == temp.Id && x.IsRejected == false).ToList();
                                if (payments.Sum(x => x.Amount) > 0.0M)
                                {
                                    IncomeRefundSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == temp.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                    incomeRefund += payments.Sum(x => x.Amount);
                                }
                            }
                            //check if expense is paid
                            foreach (var temp in expenseInfo.Where(x => !x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel))
                            {
                                var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                if (payments.Sum(x => x.Amount) > 0.0M)
                                {
                                    ExpenseSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                    ;
                                    totalExpenseAmount += payments.Sum(x => x.Amount);
                                }
                            }
                            //check if startup cost is paid
                            foreach (var temp in expenseInfo.Where(x => x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel))
                            {
                                var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                if (payments.Sum(x => x.Amount) > 0.0M)
                                {
                                    StartUpCostSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                    totalStartupCost += payments.Sum(x => x.Amount);
                                }
                            }
                            //check if expense refund is paid
                            foreach (var temp in expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost == false))
                            {
                                var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                if (payments.Sum(x => x.Amount) > 0.0M)
                                {
                                    ExpenseRefundSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                    expenseRefund += payments.Sum(x => x.Amount);
                                }
                            }
                            //check if startup cost refund is paid
                            foreach (var temp in expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost))
                            {
                                var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                if (payments.Sum(x => x.Amount) > 0.0M)
                                {
                                    StartupCostRefundSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                    startupCostRefund = payments.Sum(x => x.Amount);
                                }
                            }
                        }
                    }
                    else
                    {//
                        totalIncomeAmount = incomeInfo.Where(x => x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).Sum(x => x.Amount.Value);
                        totalExpenseAmount = expenseInfo.Where(x => !x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel).Sum(x => x.Amount.Value);
                        totalStartupCost = expenseInfo.Where(x => x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel).Sum(x => x.Amount.Value);
                        incomeRefund = incomeInfo.Where(x => x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund).ToList().Sum(x => x.Amount.Value);
                        expenseRefund = expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost == false).Sum(x => x.Amount.Value);
                        startupCostRefund = expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost).Sum(x => x.Amount.Value);
                        //Summary infomation of all column
                        using (var db = new ApplicationDbContext())
                        {
                            (from income in incomeInfo where income.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund select new { summary = "<td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + income.Amount + "</td>" }).ToList().ForEach(x => IncomeSummary += "<tr>" + x.summary + "</tr>");
                            (from income in incomeInfo where income.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund select new { summary = "<td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + income.Amount + "</td>" }).ToList().ForEach(x => IncomeRefundSummary += "<tr>" + x.summary + "</tr>");
                            (from expense in expenseInfo where !expense.IsStartupCost && expense.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => ExpenseSummary += "<tr>" + x.summary + "</tr>");
                            (from expense in expenseInfo where expense.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && expense.IsStartupCost == false select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => ExpenseRefundSummary += "<tr>" + x.summary + "</tr>");
                            (from expense in expenseInfo where expense.IsStartupCost && expense.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => StartUpCostSummary += "<tr>" + x.summary + "</tr>");
                            (from expense in expenseInfo where expense.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && expense.IsStartupCost select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => StartupCostRefundSummary += "<tr>" + x.summary + "</tr>");
                        }
                    }

                    incomeExpenseInfoList.Add(new IncomeExpenseViewModel
                    {
                        Property = Core.API.AllSite.Properties.GetName(propertyId),
                        IncomeSummary = js.Serialize(IncomeSummary + "</tbody></table>"),
                        IncomeRefundSummary = js.Serialize(IncomeRefundSummary + "</tbody></table>"),
                        ExpenseSummary = js.Serialize(ExpenseSummary + "</tbody></table>"),
                        ExpenseRefundSummary = js.Serialize(ExpenseRefundSummary + "</tbody></table>"),
                        StartUpCostSummary = js.Serialize(StartUpCostSummary + "</tbody></table>"),
                        StartupCostRefundSummary = js.Serialize(StartupCostRefundSummary + "</tbody></table>"),
                        IncomeRefund = incomeRefund,
                        ExpenseRefund = expenseRefund,
                        StartupCostRefund = startupCostRefund,
                        IncomeAmount = totalIncomeAmount,
                        ExpenseAmount = totalExpenseAmount,
                        StartupCost = totalStartupCost,
                        OperatingNetProfit = (totalIncomeAmount - incomeRefund) - (totalExpenseAmount - expenseRefund),
                        NetProfit = (totalIncomeAmount - incomeRefund) - (totalExpenseAmount - expenseRefund) - (totalStartupCost - startupCostRefund)
                    });

                }

                status = (int)StatusEnum.SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            var response = new { incomeExpenseInfoList, status };
            return Json(response);
        }
        [HttpPost]
        public ActionResult GetIncomeExpenseInfo(int propertyId, string dateFrom, string dateTo, bool isPaidOnly, bool includeChild)
        {
            if (dateTo == "")
            {
                dateTo = dateFrom;
            }
            List<DateTime> monthYearRange = new List<DateTime>();
            int status = (int)StatusEnum.FAIL;
            int totalPageCount = 0;
            var js = new JavaScriptSerializer();
            List<IncomeExpenseViewModel> incomeExpenseInfoList = new List<IncomeExpenseViewModel>();
            SearchDateModel searchDateModel = new SearchDateModel()
            {
                DateFrom = dateFrom.ToDateTime(),
                DateTo = new DateTime(dateTo.ToDateTime().Year, dateTo.ToDateTime().Month, DateTime.DaysInMonth(dateTo.ToDateTime().Year, dateTo.ToDateTime().Month)),


            };
            var dt = searchDateModel.DateTo;
            var df = searchDateModel.DateFrom;

            while (df <= dt)
            {
                monthYearRange.Add(df);
                df = df.AddMonths(1);
            }
            try
            {
                var incomeData = objIList.GetIncomeInfo(propertyId, isPaidOnly, includeChild);
                var expenseData = objIList.GetExpenseInfo(propertyId, isPaidOnly, includeChild);

                foreach (var item in monthYearRange)
                {
                    var incomeInfo = (from income in incomeData where income.DueDate.Month == item.Month && income.DueDate.Year == item.Year select income).ToList();
                    var expenseInfo = (from expense in expenseData where expense.DueDate.Month == item.Month && expense.DueDate.Year == item.Year select expense).ToList();
                    if (expenseInfo.Count >= 1 || incomeInfo.Count >= 1)
                    {
                        var totalExpenseAmount = 0.0M;
                        var totalIncomeAmount = 0.0M;
                        var startupCostRefund = 0.0M;
                        var expenseRefund = 0.0M;
                        var incomeRefund = 0.0M;
                        var totalStartupCost = 0.0M;
                        var IncomeSummary = "<h2>Income</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        var IncomeRefundSummary = "<h2>Income Refund</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        var ExpenseSummary = "<h2>Expense</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        var ExpenseRefundSummary = "<h2>Expense Refund</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        var StartUpCostSummary = "<h2>Startup Cost</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        var StartupCostRefundSummary = "<h2>Startup Cost Refund</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>"; if (isPaidOnly)
                        {
                            using (var db = new ApplicationDbContext())
                            {  //check if income is paid
                                foreach (var temp in incomeInfo.Where(x => x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund))
                                {
                                    var payments = db.IncomePayment.Where(x => x.IncomeId == temp.Id && x.IsRejected == false).ToList();
                                    if (payments.Sum(x => x.Amount) > 0.0M)
                                    {
                                        IncomeSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == temp.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                        totalIncomeAmount += payments.Sum(x => x.Amount);
                                    }
                                }
                                //check if income refund is paid
                                foreach (var temp in incomeInfo.Where(x => x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund))
                                {
                                    var payments = db.IncomePayment.Where(x => x.IncomeId == temp.Id && x.IsRejected == false).ToList();
                                    if (payments.Sum(x => x.Amount) > 0.0M)
                                    {
                                        IncomeRefundSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == temp.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                        incomeRefund += payments.Sum(x => x.Amount);
                                    }
                                }
                                //check if expense is paid
                                foreach (var temp in expenseInfo.Where(x => !x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel))
                                {
                                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                    if (payments.Sum(x => x.Amount) > 0.0M)
                                    {
                                        ExpenseSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                        ;
                                        totalExpenseAmount += payments.Sum(x => x.Amount);
                                    }
                                }
                                //check if startup cost is paid
                                foreach (var temp in expenseInfo.Where(x => x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel))
                                {
                                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                    if (payments.Sum(x => x.Amount) > 0.0M)
                                    {
                                        StartUpCostSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                        totalStartupCost += payments.Sum(x => x.Amount);
                                    }
                                }
                                //check if expense refund is paid
                                foreach (var temp in expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost == false))
                                {
                                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                    if (payments.Sum(x => x.Amount) > 0.0M)
                                    {
                                        ExpenseRefundSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                        expenseRefund += payments.Sum(x => x.Amount);
                                    }
                                }
                                //check if startup cost refund is paid
                                foreach (var temp in expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost))
                                {
                                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                                    if (payments.Sum(x => x.Amount) > 0.0M)
                                    {
                                        StartupCostRefundSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == temp.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + temp.Description + "</td><td>" + temp.Amount + "</td></tr>";
                                        startupCostRefund = payments.Sum(x => x.Amount);
                                    }
                                }
                            }
                        }
                        else
                        {//
                            totalIncomeAmount = incomeInfo.Where(x => x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).Sum(x => x.Amount.Value);
                            totalExpenseAmount = expenseInfo.Where(x => !x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel).Sum(x => x.Amount.Value);
                            totalStartupCost = expenseInfo.Where(x => x.IsStartupCost && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel).Sum(x => x.Amount.Value);
                            incomeRefund = incomeInfo.Where(x => x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund).ToList().Sum(x => x.Amount.Value);
                            expenseRefund = expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost == false).Sum(x => x.Amount.Value);
                            startupCostRefund = expenseInfo.Where(x => x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && x.IsStartupCost).Sum(x => x.Amount.Value);
                            //Summary infomation of all column
                            using (var db = new ApplicationDbContext())
                            {
                                (from income in incomeInfo where income.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund select new { summary = "<td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + income.Amount + "</td>" }).ToList().ForEach(x => IncomeSummary += "<tr>" + x.summary + "</tr>");
                                (from income in incomeInfo where income.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund select new { summary = "<td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + income.Amount + "</td>" }).ToList().ForEach(x => IncomeRefundSummary += "<tr>" + x.summary + "</tr>");
                                (from expense in expenseInfo where !expense.IsStartupCost && expense.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => ExpenseSummary += "<tr>" + x.summary + "</tr>");
                                (from expense in expenseInfo where expense.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && expense.IsStartupCost == false select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => ExpenseRefundSummary += "<tr>" + x.summary + "</tr>");
                                (from expense in expenseInfo where expense.IsStartupCost && expense.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => StartUpCostSummary += "<tr>" + x.summary + "</tr>");
                                (from expense in expenseInfo where expense.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel && expense.IsStartupCost select new { summary = "<td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + expense.Amount + "</td>" }).ToList().ForEach(x => StartupCostRefundSummary += "<tr>" + x.summary + "</tr>");
                            }

                        }
                        incomeExpenseInfoList.Add(new IncomeExpenseViewModel
                        {
                            Date = item,
                            IncomeSummary = js.Serialize(IncomeSummary + "</tbody></table>"),
                            IncomeRefundSummary = js.Serialize(IncomeRefundSummary + "</tbody></table>"),
                            ExpenseSummary = js.Serialize(ExpenseSummary + "</tbody></table>"),
                            ExpenseRefundSummary = js.Serialize(ExpenseRefundSummary + "</tbody></table>"),
                            StartUpCostSummary = js.Serialize(StartUpCostSummary + "</tbody></table>"),
                            StartupCostRefundSummary = js.Serialize(StartupCostRefundSummary + "</tbody></table>"),
                            IncomeRefund = incomeRefund,
                            ExpenseRefund = expenseRefund,
                            StartupCostRefund = startupCostRefund,
                            Month = item.Month,
                            Year = item.Year,
                            IncomeAmount = totalIncomeAmount,
                            ExpenseAmount = totalExpenseAmount,
                            StartupCost = totalStartupCost,
                            OperatingNetProfit = (totalIncomeAmount - incomeRefund) - (totalExpenseAmount - expenseRefund),
                            NetProfit = (totalIncomeAmount - incomeRefund) - (totalExpenseAmount - expenseRefund) - (totalStartupCost - startupCostRefund)
                        });
                    }
                }
                //totalPageCount = (int)Math.Ceiling((double)incomeExpenseInfoList.Count / Configuration.TotalRecordPerPage);

                //if (pageNumber != 1)
                //{
                //    int totalRecordsToSkip = (pageNumber - 1) * Configuration.TotalRecordPerPage;
                //    incomeExpenseInfoList =
                //        new List<IncomeExpenseViewModel>(
                //            incomeExpenseInfoList.OrderBy(x => x.Month)
                //                .Skip(totalRecordsToSkip)
                //                .Take(Configuration.TotalRecordPerPage));
                //}
                //else
                //{
                //    incomeExpenseInfoList = new List<IncomeExpenseViewModel>(incomeExpenseInfoList.Take(Configuration.TotalRecordPerPage));
                //}

                status = (int)StatusEnum.SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            var response = new { incomeExpenseInfoList = incomeExpenseInfoList.OrderByDescending(x => x.Date), status, totalPageCount };
            return Json(response);
        }

        [HttpPost]
        public ActionResult GetCommissionsReport(string selectedProperties, string selectedDate)
        {
            //Get the month value from selectedDate param
            int selectedMonth = !string.IsNullOrEmpty(selectedDate) ? Convert.ToDateTime(selectedDate).Month : 0;
            int selectedYear = !string.IsNullOrEmpty(selectedDate) ? Convert.ToDateTime(selectedDate).Year : 0;
            List<CommissionReportViewModel> commissionReportList = new List<CommissionReportViewModel>();

            var propertyItems = selectedProperties.Split(',');

            List<int> properties = propertyItems.Select(selectedProperty => Convert.ToInt32(selectedProperty)).ToList();

            foreach (var property in properties)
            {
                CommissionReportViewModel report = new CommissionReportViewModel();
                var propertyInfo = objIList.GetPropertyRecord(property);
                report.PropertyName = propertyInfo.Name;

                var totalIncome = objIList.GetIncomeInfo(property, false, false);

                if (propertyInfo.CommissionId == (int)CommissionTypeEnum.FixedPricePerBooking)
                {
                    var allBookingByProperty = objIList.GetBookingsByProperty(property);

                    //After get all related booking by Property ID, filter based on CheckedInDate & BookingStatusCode = A
                    var totalBookings =
                        new List<Inquiry>(
                            allBookingByProperty.Where(
                                x =>
                                    x.CheckInDate.Value.Month == selectedMonth &&
                                    x.CheckInDate.Value.Year == selectedYear && x.BookingStatusCode.Equals("A")));


                    report.CommissionType = Definition.Description(CommissionTypeEnum.FixedPricePerBooking);
                    report.Unit = propertyInfo.CommissionAmount;
                    report.NetProfit = totalBookings.Count;
                    report.Total = propertyInfo.CommissionAmount * totalBookings.Count;

                }
                else if (propertyInfo.CommissionId == (int)CommissionTypeEnum.FixedPricePerNight)
                {
                    var allBookingByProperty = objIList.GetBookingsByProperty(property);

                    //After get all related booking by Property ID, filter based on CheckedInDate & BookingStatusCode = A
                    var totalBookings =
                        new List<Inquiry>(
                            allBookingByProperty.Where(
                                x =>
                                    x.CheckInDate.Value.Month == selectedMonth &&
                                    x.CheckInDate.Value.Year == selectedYear && x.BookingStatusCode.Equals("A")));


                    report.CommissionType = Definition.Description(CommissionTypeEnum.FixedPricePerNight);
                    report.Unit = propertyInfo.CommissionAmount;
                    var totalNightsWithBooking = (decimal)totalBookings.Sum(x => (x.CheckOutDate.Value.Date - x.CheckInDate.Value.Date).TotalDays);
                    report.NetProfit = totalNightsWithBooking;
                    report.Total = propertyInfo.CommissionAmount * totalNightsWithBooking;
                }
                else if (propertyInfo.CommissionId == (int)CommissionTypeEnum.PercentofTotalRevenuefortheMonth)
                {
                    var incomeInfo =
                        new List<Income>(
                            totalIncome.Where(
                                x => x.DateRecorded.Month == selectedMonth && x.DateRecorded.Year == selectedYear));

                    var totalIncomeAmount = incomeInfo.Sum(x => x.Amount.Value);

                    report.CommissionType = Definition.Description(CommissionTypeEnum.PercentofTotalRevenuefortheMonth);
                    report.Unit = propertyInfo.CommissionAmount;
                    report.NetProfit = totalIncomeAmount;
                    report.Total = totalIncomeAmount * (propertyInfo.CommissionAmount / 100);
                }
                else if (propertyInfo.CommissionId == (int)CommissionTypeEnum.PercentofTotalProfitfortheMonth)
                {
                    //var totalIncome = repo.GetIncomeInfo(property, false);
                    var totalExpense = objIList.GetExpenseInfoByPropertyId(property);

                    var incomeInfo =
                        new List<Income>(
                            totalIncome.Where(
                                x => x.DateRecorded.Month == selectedMonth && x.DateRecorded.Year == selectedYear));
                    var expenseRecordList = totalExpense.Where(expenseRecord => expenseRecord.DueDate != null).ToList();
                    var expenseInfo =
                        expenseRecordList.Where(
                            record =>
                                record.DueDate.Month == selectedMonth && record.DueDate.Year == selectedYear)
                            .ToList();


                    var totalIncomeAmount = incomeInfo.Sum(x => x.Amount);
                    var totalExpenseAmount = expenseInfo.Sum(x => x.Amount);
                    var totalNetProfit = totalIncomeAmount - totalExpenseAmount;

                    report.CommissionType = Definition.Description(CommissionTypeEnum.PercentofTotalProfitfortheMonth);
                    report.Unit = propertyInfo.CommissionAmount;
                    report.NetProfit = totalNetProfit.Value;
                    report.Total = totalNetProfit.Value * (propertyInfo.CommissionAmount / 100);
                }
                else if (propertyInfo.CommissionId == (int)CommissionTypeEnum.AllAmountAfterXAmountofTotalRevenue)
                {
                    //var totalIncome = repo.GetIncomeInfo(property, false);
                    var incomeInfo =
                        new List<Income>(
                            totalIncome.Where(
                                x => x.DateRecorded.Month == selectedMonth && x.DateRecorded.Year == selectedYear));

                    var totalIncomeAmount = incomeInfo.Sum(x => x.Amount);

                    report.CommissionType = Definition.Description(CommissionTypeEnum.AllAmountAfterXAmountofTotalRevenue);
                    report.Unit = propertyInfo.CommissionAmount;
                    report.NetProfit = totalIncomeAmount.Value;
                    report.Total = totalIncomeAmount.Value - propertyInfo.CommissionAmount;
                }
                else if (propertyInfo.CommissionId == (int)CommissionTypeEnum.AllAmountAfterXAmountofTotalNetProfit)
                {
                    var totalExpense = objIList.GetExpenseInfoByPropertyId(property);

                    var incomeInfo =
                        new List<Income>(
                            totalIncome.Where(
                                x => x.DateRecorded.Month == selectedMonth && x.DateRecorded.Year == selectedYear));
                    var expenseRecordList = totalExpense.Where(expenseRecord => expenseRecord.DueDate != null).ToList();
                    var expenseInfo =
                        expenseRecordList.Where(
                            record =>
                                record.DueDate.Month == selectedMonth && record.DueDate.Year == selectedYear)
                            .ToList();


                    var totalIncomeAmount = incomeInfo.Sum(x => x.Amount);
                    var totalExpenseAmount = expenseInfo.Sum(x => x.Amount);
                    var totalNetProfit = totalIncomeAmount - totalExpenseAmount;

                    report.CommissionType = Definition.Description(CommissionTypeEnum.AllAmountAfterXAmountofTotalNetProfit);
                    report.Unit = propertyInfo.CommissionAmount;
                    report.NetProfit = totalNetProfit.Value;
                    report.Total = totalNetProfit.Value - propertyInfo.CommissionAmount;
                }

                commissionReportList.Add(report);

            }

            var response = new
            {
                result = commissionReportList
            };

            return Json(response);
        }

    }
}
