﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Models.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class InvoiceController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        // GET: Invoice
        public ActionResult Index()
        {
            ViewBag.Properties = objIList.GetProperties();
            return View();
        }
        public ActionResult GetInvoices()
        {
            using (var db = new ApplicationDbContext())
            {
                var data = new List<InvoiceEntryViewModel>();
                var infos = (from invoice in db.Invoices join reservation in db.Inquiries on invoice.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where property.CompanyId == SessionHandler.CompanyId select new { Invoice = invoice, Reservation = reservation, Property = property }).ToList();

                foreach (var info in infos)
                {
                    data.Add(new InvoiceEntryViewModel()
                    {
                        DateCreated = info.Invoice.DateCreated,
                        File = info.Invoice.FileUrl,
                        PaymentMethod = ((Core.Enumerations.PaymentRequestMethod)info.Invoice.PaymentMethod).ToString(),
                        Status = info.Invoice.Status == 0 ? "Sent" : info.Invoice.Status == 1 ? "Initiated" : info.Invoice.Status == 2 ? "Pending" : "Complete",
                        Property = info.Property.Name,
                        Renter = db.Renters.Where(x => x.BookingId == info.Reservation.Id).Select(x=>new {Name= x.Firstname+" "+ x.Lastname}).FirstOrDefault().Name

                    }) ;
            }
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetInvoiceSchedules()
        {
            using(var db = new ApplicationDbContext())
            {
                var data = new List<InvoiceScheduleViewModel>();
                var schedules= db.InvoiceSchedules.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach(var schedule in schedules)
                {
                    var propertyIds = schedule.PropertyIds.Split(',').Select(Int32.Parse).ToList();
                    data.Add(new InvoiceScheduleViewModel()
                    {
                        Id=schedule.Id,
                        Description = schedule.Description,
                        EmailTemplate = db.EmailTemplates.Where(x=>x.Id==schedule.EmailTemplateId).Select(x=>x.Name).FirstOrDefault(),
                        InvoiceTemplate = db.InvoiceTemplates.Where(x => x.Id == schedule.InvoiceTemplateId).Select(x => x.Name).FirstOrDefault(),
                        PaymentMethod = ((Core.Enumerations.PaymentRequestMethod)schedule.PaymentMethod).ToString(),
                        Properties = string.Join("<br>", db.Properties.Where(x => propertyIds.Contains(x.Id)).Select(x => x.Name).ToList()),
                        
                }) ;
                }


                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetInvoiceSchedule(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var schedule = db.InvoiceSchedules.Where(x => x.Id == id).FirstOrDefault();
                return Json(new {schedule }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveInvoiceSchedule(InvoiceSchedule schedule)
        {
            using (var db = new ApplicationDbContext())
            {
                if (schedule.Id == 0)
                {
                    schedule.CompanyId = SessionHandler.CompanyId;
                    db.InvoiceSchedules.Add(schedule);
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.InvoiceSchedules.Where(x => x.Id == schedule.Id).FirstOrDefault();
                    temp.Description = schedule.Description;
                    temp.DayOfMonth = schedule.DayOfMonth;
                    temp.EmailTemplateId = schedule.EmailTemplateId;
                    temp.InvoiceTemplateId = schedule.InvoiceTemplateId;
                    temp.PropertyIds = schedule.PropertyIds;
                    temp.PaymentMethod = schedule.PaymentMethod;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}