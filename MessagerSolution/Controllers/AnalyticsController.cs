﻿using Core.Database.Context;
using Core.Helper;
using Core.Models;
using Core.Repository;
using Core.SignalR.Hubs;
using MessagerSolution.Models.Analytics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MessagerSolution.Controllers
{
    public class AnalyticsController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        public ActionResult Calendar(long listingId, int month = 0, int year = 0)
        {
            using (var db = new ApplicationDbContext())
            {
                var calendar = new List<Core.Database.Entity.PropertyBookingDate>();

                ViewBag.Property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                ViewBag.PriceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(listingId);
            }
            return View();
        }

        public ActionResult GetComparables(string address,string date,string bedrooms,string miles,bool isPrivateRoomOnly,bool isAirbnbOnly)
        {
          
            using (var media = new Multimedia.ScrapeManager())
            {
                var geoData = media.GeLongitudeLatitude(address);
                var longitude = geoData.Item1;
                var latitude = geoData.Item2;
                using (var sm = new Airbnb.ScrapeManager())
                {
                    List<Comparables> comparables = new List<Comparables>();
                    if(!string.IsNullOrWhiteSpace(address) && !string.IsNullOrWhiteSpace(longitude)&&!string.IsNullOrWhiteSpace(latitude)){
                        comparables = sm.SearchComparables(latitude, longitude, date.ToDateTime().Date, bedrooms, miles, isPrivateRoomOnly,isAirbnbOnly); }
                    return Json(new { data = comparables}, JsonRequestBehavior.AllowGet) ;
                }

            }

        }
        public ActionResult GetCityProperties()
        {
            using (var sm = new Airbnb.ScrapeManager())
            {
                List<CityPropertyViewModel> cityProperties = new List<CityPropertyViewModel>();
             
                cityProperties = sm.GetCityProperties(SessionHandler.CompanyId);
                return Json(new { data = cityProperties }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SearchProperty(string location)
        {
            
            using (var media = new Multimedia.ScrapeManager())
            {
                var geoData = media.GetLocation(location);
                using (var sm = new Airbnb.ScrapeManager())
                {
                    var success = sm.SearchLocation(geoData,SessionHandler.CompanyId,User.Identity.Name.ToInt());
                    return Json(new { success=success }, JsonRequestBehavior.AllowGet);
                }

            }


        }

        public ActionResult UpdateProgress(string userId, int count,int cityId,string location)
        {
            ImportHub.UpdateAnalyticsProgress(userId, count,cityId,location);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProperyDate(string date,long listingId)
        {
            using(var db = new ApplicationDbContext())
            {
                var dateFilter = date=="0"?DateTime.Now: date.ToDateTime();
               var dates = db.PropertyBookingDate.Where(x => x.PropertyId == listingId && (date=="0"?x.Date.Year==dateFilter.Year: (dateFilter.Month== x.Date.Month && dateFilter.Year == x.Date.Year))).OrderBy(x=>x.Date).ToList();
                var data = new List<PropertyDateModel>();
                foreach(var dt in dates)
                {
                    data.Add(new PropertyDateModel()
                    {  Checkbox="<div class=\"ui checkbox\"><input class=\"request-price-check\" type=\"checkbox\"><label></label></div>",
                        Id = dt.Id,
                        Date = dt.Date.ToString("MMMM d,yyyy"),
                        IsAvailable = dt.IsAvailable,
                        Availability=dt.IsAvailable?"Available":"Not Available",
                        Price = dt.Price,
                        Actions = "<button class=\"ui button blue show-analytics\">Show Analytics</button>" +
                        "<button class=\"ui button blue show-comparable\">Show Comparables</button>"

                    }); ;
                }
                return Json(new { data,success =true  }, JsonRequestBehavior.AllowGet);
            }  
        }

        public ActionResult GetAnalyticsPrice(int dateId, bool includeSiteByOnly,int bedroom, int siteType)
        {
            using (var db = new ApplicationDbContext())
            {
                Core.Models.Analytics analytics = new Core.Models.Analytics();
                var date = db.PropertyBookingDate.Where(x => x.Id == dateId).FirstOrDefault();
                var property = db.Properties.Where(x => x.ListingId == date.PropertyId).FirstOrDefault();
                var excludedComparables = db.ExcludedComparables.Where(x => x.HostId == property.HostId).ToList();
                string excludedListingsQuery = "";
                foreach (var listing in db.ExcludedComparables)
                {
                    excludedListingsQuery += "&excludedListings=" + listing.ListingId;
                }
                using (var sm = new Airbnb.ScrapeManager())
                {
                   analytics = sm.GetAnalytics(property.Latitude, property.Longitude, date.Date, bedroom, excludedListingsQuery, siteType, includeSiteByOnly);
                }
                string ruleUsedAndComputations = "";
                var priceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(property.ListingId);

                double rate = 0;
                double computedPriceRuleAmount = 0;
                double price = 0;
                double finalPrice = 0;
                //Pricer setting rules
                foreach (var settings in priceRuleSettings)
                {
                    ruleUsedAndComputations = "The ";

                    #region PriceRuleBasis

                    switch (settings.PriceRuleBasis)
                    {
                        case 0:
                            rate = analytics.OccupancyRateByDate;
                            ruleUsedAndComputations += "Occupancy rate by date is ";
                            break;

                        case 1:
                            rate = analytics.OccupancyRateByDateForXRooms;
                            ruleUsedAndComputations += "Occupancy rate by date for "+bedroom+" rooms is ";
                            break;

                        case 2:
                            rate = analytics.OccupancyRateByMonth;
                            ruleUsedAndComputations += "Occupancy rate by month is ";
                            break;

                        case 3:
                            rate = analytics.OccupancyRateByMonthForXRooms;
                            ruleUsedAndComputations += "Occupancy rate by month for " + bedroom + " rooms is ";
                            break;

                        case 4:
                            rate = analytics.AvailabilityRateByDate;
                            ruleUsedAndComputations += "Availability rate by date is ";
                            break;

                        case 5:
                            rate = analytics.AvailabilityRateByDateForXRooms;
                            ruleUsedAndComputations += "Availability rate by date for " + bedroom + " rooms is ";
                            break;

                        case 6:
                            rate = analytics.AvailabilityRateByMonth;
                            ruleUsedAndComputations += "Availability rate by month";
                            break;

                        case 7:
                            rate = analytics.AvailabilityRateByMonthForXRooms;
                            ruleUsedAndComputations += "Availability rate by month for " + bedroom + " rooms is ";
                            break;
                    }

                    #endregion

                    #region PriceRuleAveragePriceBasis

                    switch (settings.PriceRuleAveragePriceBasis)
                    {
                        case 0:
                            price = analytics.AveragePriceByDate;
                            break;

                        case 1:
                            price = analytics.AveragePriceByDateForXRooms;
                            break;

                        case 2:
                            price = analytics.AveragePriceByMonth;
                            break;

                        case 3:
                            price = analytics.AveragePriceByMonthForXRooms;
                            break;
                    }

                    #endregion
                    //Price rules condition is applied when rate is greater than percent assign
                    if (settings.PriceRuleCondition == 0)
                    {
                        if (rate >= settings.PriceRuleConditionPercentage)
                        {
                            if (settings.PriceRuleSign == 0)
                            {
                                computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + "% <br/> " +
                                    "average price = " + price + "<br/> computed price rule amount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                            else
                            {
                                computedPriceRuleAmount = (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + " $ <br/>" +
                                    "average price = " + price + "<br/> computed price rule amount = " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else if (settings.PriceRuleCondition == 1)
                    {
                        if (rate <= settings.PriceRuleConditionPercentage)
                        {
                            if (settings.PriceRuleSign == 0)
                            {
                                computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " % <br/>" +
                                    "average price = " + price + "<br/> computed pricerule amount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                            else
                            {
                                computedPriceRuleAmount = (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " $ <br/>" +
                                    "average price = " + price + "<br/> computed price rule amount = " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                return Json(new {success = true, ruleUsedAndComputations, finalPrice }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowAnalytics(int dateId,bool includeSiteByOnly,int bedroom, int siteType)
        {
            using (var db = new ApplicationDbContext())
            {
                Core.Models.Analytics analytics = new Core.Models.Analytics();
                var date = db.PropertyBookingDate.Where(x => x.Id == dateId).FirstOrDefault();
                var property = db.Properties.Where(x => x.ListingId == date.PropertyId).FirstOrDefault();
                var excludedComparables = db.ExcludedComparables.Where(x => x.HostId == property.HostId).ToList();
                string excludedListingsQuery = "";
                foreach (var listing in db.ExcludedComparables)
                {
                    excludedListingsQuery += "&excludedListings=" + listing.ListingId;
                }
                using (var sm = new Airbnb.ScrapeManager())
                {
                    analytics = sm.GetAnalytics(property.Latitude, property.Longitude, date.Date, bedroom, excludedListingsQuery, siteType, includeSiteByOnly);
                }
                string ruleUsedAndComputations = "";
                var priceRuleSettings = Core.API.AllSite.PriceRuleSettings.GetByListingId(property.ListingId);

                double rate = 0;
                double computedPriceRuleAmount = 0;
                double price = 0;
                double finalPrice = 0;
                //Pricer setting rules
                //Pricer setting rules
                foreach (var settings in priceRuleSettings)
                {
                    ruleUsedAndComputations = "The ";

                    #region PriceRuleBasis

                    switch (settings.PriceRuleBasis)
                    {
                        case 0:
                            rate = analytics.OccupancyRateByDate;
                            ruleUsedAndComputations += "Occupancy rate by date is ";
                            break;

                        case 1:
                            rate = analytics.OccupancyRateByDateForXRooms;
                            ruleUsedAndComputations += "Occupancy rate by date for " + bedroom + " rooms is ";
                            break;

                        case 2:
                            rate = analytics.OccupancyRateByMonth;
                            ruleUsedAndComputations += "Occupancy rate by month is ";
                            break;

                        case 3:
                            rate = analytics.OccupancyRateByMonthForXRooms;
                            ruleUsedAndComputations += "Occupancy rate by month for " + bedroom + " rooms is ";
                            break;

                        case 4:
                            rate = analytics.AvailabilityRateByDate;
                            ruleUsedAndComputations += "Availability rate by date is ";
                            break;

                        case 5:
                            rate = analytics.AvailabilityRateByDateForXRooms;
                            ruleUsedAndComputations += "Availability rate by date for " + bedroom + " rooms is ";
                            break;

                        case 6:
                            rate = analytics.AvailabilityRateByMonth;
                            ruleUsedAndComputations += "Availability rate by month";
                            break;

                        case 7:
                            rate = analytics.AvailabilityRateByMonthForXRooms;
                            ruleUsedAndComputations += "Availability rate by month for " + bedroom + " rooms is ";
                            break;
                    }

                    #endregion

                    #region PriceRuleAveragePriceBasis

                    switch (settings.PriceRuleAveragePriceBasis)
                    {
                        case 0:
                            price = analytics.AveragePriceByDate;
                            break;

                        case 1:
                            price = analytics.AveragePriceByDateForXRooms;
                            break;

                        case 2:
                            price = analytics.AveragePriceByMonth;
                            break;

                        case 3:
                            price = analytics.AveragePriceByMonthForXRooms;
                            break;
                    }

                    #endregion
                    //Price rules condition is applied when rate is greater than percent assign
                    if (settings.PriceRuleCondition == 0)
                    {
                        if (rate >= settings.PriceRuleConditionPercentage)
                        {
                            if (settings.PriceRuleSign == 0)
                            {
                                computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + "% <br/> " +
                                    "average price = " + price + "<br/> computed price rule amount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                            else
                            {
                                computedPriceRuleAmount = (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "greater than " + settings.PriceRuleConditionPercentage + " $ <br/>" +
                                    "average price = " + price + "<br/> computed price rule amount = " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else if (settings.PriceRuleCondition == 1)
                    {
                        if (rate <= settings.PriceRuleConditionPercentage)
                        {
                            if (settings.PriceRuleSign == 0)
                            {
                                computedPriceRuleAmount = (price / 100) * (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " % <br/>" +
                                    "average price = " + price + "<br/> computed pricerule amount = (" + price + " / 100) * " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                            else
                            {
                                computedPriceRuleAmount = (double)settings.PriceRuleAmount;
                                finalPrice = price + computedPriceRuleAmount;
                                ruleUsedAndComputations += "less than " + settings.PriceRuleConditionPercentage + " $ <br/>" +
                                    "average price = " + price + "<br/> computed price rule amount = " + (double)settings.PriceRuleAmount + "<br/> " +
                                    "final price = average price (" + price + ") + computed price rule amount (" + computedPriceRuleAmount + ")<br/> final price = " + finalPrice;
                                break;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                return Json(new { property = property, success = true , analytics ,date.Date, ruleUsedAndComputations = ruleUsedAndComputations }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowComparables(int dateId, bool includeSiteByOnly, int siteType,bool isPrivateRoomOnly =false)
        {
            using (var db = new ApplicationDbContext())
            {
                Core.Models.Analytics analytics = new Core.Models.Analytics();
                var date = db.PropertyBookingDate.Where(x => x.Id == dateId).FirstOrDefault();
                var property = db.Properties.Where(x => x.ListingId == date.PropertyId).FirstOrDefault();
                Core.Models.ComparablesAndBedrooms comparables = new Core.Models.ComparablesAndBedrooms();
                using (var sm = new Airbnb.ScrapeManager())
                {
                    comparables = sm.GetComparables(isPrivateRoomOnly,siteType, includeSiteByOnly, property.Latitude, property.Longitude, property.Bedrooms, date.Date);
                }
                JsonResult json = Json(new { property = property, success = true, date.Date, comparables }, JsonRequestBehavior.AllowGet);
                json.MaxJsonLength = int.MaxValue;
                return json;
            }
        }
        [HttpGet]
        public ActionResult GetPriceRules(long listingId)
        {
            using(var db =new ApplicationDbContext())
            {
                var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                var pricerules = db.PriceRuleSettings.Where(x => x.ListingId == property.ListingId).ToList();
                var host = db.Hosts.Where(x => x.Id == property.HostId).FirstOrDefault();
                return Json(new {property,pricerules,host }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public async Task<ActionResult> UpdatePrice(int dateId,decimal amount)
        {
            using (var db = new ApplicationDbContext())
            {
                var calendarDate = db.PropertyBookingDate.Where(x => x.Id == dateId).FirstOrDefault();
                var property = db.Properties.Where(x => x.ListingId == calendarDate.PropertyId).FirstOrDefault();
                var host = db.Hosts.Where(x => x.Id == property.HostId).FirstOrDefault();
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(1));
                    //Set the parameter of API post request
                    JObject message = null;
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("hostId", host.Id.ToString());
                    reqParams.Add("propertyId", property.ListingId.ToString());
                    reqParams.Add("from", calendarDate.Date.ToString());
                    reqParams.Add("to", calendarDate.Date.ToString());
                    reqParams.Add("isAvailable", calendarDate.IsAvailable.ToString());
                    reqParams.Add("price", amount.ToString());
                    reqParams.Add("siteType", property.SiteType.ToString());
                    reqParams.Add("syncChangesToOtherChild",true.ToString());
                    reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                    var reqContent = new FormUrlEncodedContent(reqParams);

                    var result = await client.PostAsync("Calendar/SetPrice", reqContent);
                    if (result != null && result.Content != null)
                    {
                        //get result and parse it into JObject to get the content
                        message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        return Json(new { success = message["success"].ToBoolean(), message = message["message"].ToString() }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

    }
}