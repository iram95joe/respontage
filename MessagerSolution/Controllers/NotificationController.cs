﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using MessagerSolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class NotificationController : Controller
    {
        // GET: Notification
        public ActionResult GetNotifications()
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Notifications.Where(x=>x.CompanyId==SessionHandler.CompanyId).ToList();
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetNotificationEntries()
        {
            using (var db = new ApplicationDbContext())
            {
                List<NotificationEntryModel> entries = new List<NotificationEntryModel>();
                var datas = (from entry in db.NotificationEntries join notification in db.Notifications on entry.NotificationId equals notification.Id where notification.CompanyId ==SessionHandler.CompanyId select new { NotificationEntry = entry,ScheduleType= notification.ScheduleType }).ToList();
                foreach(var data in datas)
                {
                    entries.Add(new NotificationEntryModel() {
                        Id=data.NotificationEntry.Id,
                        Description =data.NotificationEntry.Description,
                        Date = data.NotificationEntry.DateCreated,
                        Property = db.Properties.Where(x => x.Id == data.NotificationEntry.PropertyId).Select(x => x.Name).FirstOrDefault(),
                        ScheduleType = ((NotificationScheduleType)data.ScheduleType).ToString().ToSentence(),
                        Status = data.NotificationEntry.Status});
                }
                return Json(new { data=entries.OrderByDescending(x=>x.Date) }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateNotificationStatus(int id,int status)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = db.NotificationEntries.Where(x => x.Id == id).FirstOrDefault();
                if (temp != null)
                {
                    temp.Status = status;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            Core.SignalR.Hubs.NotificationHub.NotifyUser(User.Identity.Name,
                 "Notification status updated!", NotificationType.SUCCESS);
            return Json(new { success=true}, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetNotification(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.Notifications.Where(x => x.Id==id).FirstOrDefault();
                var renterIds = db.NotificationRenters.Where(x => x.NotificationId == id).Select(x => x.RenterId).ToList();
                var propertyIds = db.NotificationProperties.Where(x => x.NotificationId == id).Select(x => x.PropertyId).ToList();

                return Json(new { notification =data,renterIds,propertyIds }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveNotification(Notification notification,string PropertyIds/*,string RenterIds*/)
        {
            using (var db = new ApplicationDbContext())
            {
                //var renterIds = string.IsNullOrEmpty(RenterIds) ? new List<int>() : RenterIds.Split(',').Select(Int32.Parse).ToList();
                var propertyIds = string.IsNullOrEmpty(PropertyIds)?new List<int>():PropertyIds.Split(',').Select(Int32.Parse).ToList();
                if (notification.Id == 0)
                {
                    notification.CompanyId= SessionHandler.CompanyId;
                    db.Notifications.Add(notification);
                    db.SaveChanges();
                    //foreach(var id in renterIds)
                    //{
                    //    db.NotificationRenters.Add(new NotificationRenter() { NotificationId = notification.Id, RenterId = id });
                    //    db.SaveChanges();
                    //}
                    foreach (var id in propertyIds)
                    {
                        db.NotificationProperties.Add(new NotificationProperty() { NotificationId = notification.Id, PropertyId = id });
                        db.SaveChanges();
                    }
                }
                else
                {
                    var temp = db.Notifications.Where(x => x.Id == notification.Id).FirstOrDefault();
                    temp.Frequency = notification.Frequency;
                    temp.Interval = notification.Interval;
                    temp.Description = notification.Description;
                    temp.ScheduleType = notification.ScheduleType;
                    //foreach (var id in renterIds)
                    //{
                    //    var data = db.NotificationRenters.Where(x => x.RenterId == id && x.NotificationId == id).FirstOrDefault();
                    //    if (data == null)
                    //    {
                    //        db.NotificationRenters.Add(new NotificationRenter() { NotificationId = notification.Id, RenterId = id });
                    //        db.SaveChanges();
                    //    }
                    //}
                    foreach (var id in propertyIds)
                    {
                        var data = db.NotificationProperties.Where(x => x.PropertyId == id && x.NotificationId == id).FirstOrDefault();
                        if (data == null)
                        {
                            db.NotificationProperties.Add(new NotificationProperty() { NotificationId = notification.Id, PropertyId = id });
                            db.SaveChanges();
                        }
                    }
                }
            }
            return Json(new { success=true }, JsonRequestBehavior.AllowGet);
        }
    }
}