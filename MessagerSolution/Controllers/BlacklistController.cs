﻿using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class BlacklistController : Controller
    {
        ICommonRepository objIList = new CommonRepository();

        [Route("blacklist")]
        public ActionResult Index(string siteHostId = "")
        {
            var hosts = Core.API.Airbnb.Hosts.GetList;
            ViewBag.Hosts = hosts;

            using (var sm = new Airbnb.ScrapeManager())
            {
                if (siteHostId == "")
                {
                    var host = hosts.FirstOrDefault();
                    var token = Core.API.AllSite.HostSessionCookie.GetToken(host.Id);
                    ViewBag.Reviews = sm.GuestReviewsToHost(token, host.SiteHostId);
                }
                else
                {
                    var host = hosts.Where(x => x.SiteHostId == siteHostId).FirstOrDefault();
                    var token = Core.API.AllSite.HostSessionCookie.GetToken(host.Id);
                    ViewBag.Reviews = sm.GuestReviewsToHost(token, siteHostId);
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult Post(GoogleSheetsData item)
        {
            var googleSheetsService = GoogleSheets.AuthorizeGoogleApp();
            var dummyData = GoogleSheets.GenerateData(item.Name, item.Feedback, item.Photo);
            GoogleSheets.Update(dummyData, GoogleSheets.SheetId, "A:A", googleSheetsService);
            return Json(new { message = item.Name });
        }
    }
}