﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DocumentFormat.OpenXml.ExtendedProperties;
using MessagerSolution.Models;
using MessagerSolution.Models.Expense;
using MessagerSolution.Models.Worker;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MessagerSolution.Controllers
{
    public class WorkerController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        // GET: Worker
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetBatchPayment(int id)
        {
            using (var db = new ApplicationDbContext())
            {

                var batchPayment = db.ExpenseBatchPayments.Where(x => x.Id == id).FirstOrDefault();
                var payments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batchPayment.Id).ToList();
                List<BatchExpensePaymentViewModel> assignmentReimbursement = new List<BatchExpensePaymentViewModel>();
                List<BatchExpensePaymentViewModel> retainerFixed = new List<BatchExpensePaymentViewModel>();
                List<BatchExpensePaymentViewModel> bonus = new List<BatchExpensePaymentViewModel>();
                var laborId=objIList.GetExpenseCategoryId("Labor");
                var reimburseId =objIList.GetExpenseCategoryId("Reimburse");
                var fixedId =objIList.GetExpenseCategoryId("Fixed");
                var retainerId =objIList.GetExpenseCategoryId("Retainer");
                var bonusId = objIList.GetExpenseCategoryId("Bonus");
                var workerExpenseCategoryIds =
                    db.ExpenseCategories.Where(x =>
                    (x.ExpenseCategoryType == "Labor" ||
                    x.ExpenseCategoryType == "Reimburse" ||
                    x.ExpenseCategoryType == "Fixed" ||
                    x.ExpenseCategoryType == "Retainer" ||
                    x.ExpenseCategoryType == "Bonus") && x.Type == 2).Select(x => x.Id).ToList();
                foreach (var payment in payments)
                {
                    BatchExpensePaymentViewModel temp = new BatchExpensePaymentViewModel();
                    var Expense = db.Expenses.Where(x => x.Id == payment.ExpenseId).FirstOrDefault();
                    temp.Balance = Expense.Amount.Value;
                    temp.Amount = payment.Amount;
                    temp.ExpenseId = Expense.Id;
                    temp.ExpensePaymentId = payment.Id;
                    temp.Description = Expense.Description;
                    temp.DueDate = Expense.DueDate;
                    if (Expense.ExpenseCategoryId == laborId || Expense.ExpenseCategoryId ==reimburseId)
                    {
                        temp.Category = Expense.ExpenseCategoryId == laborId ? "Labor" : "Reimburse";
                        assignmentReimbursement.Add(temp);

                    }
                    else if (Expense.ExpenseCategoryId == retainerId || Expense.ExpenseCategoryId == fixedId)
                    {
                        temp.Category = Expense.ExpenseCategoryId ==retainerId ? "Retainer" : "Fixed";
                        retainerFixed.Add(temp);
                    }
                    else if (Expense.ExpenseCategoryId == bonusId)
                    {
                        temp.Category = "Bonus";
;                        bonus.Add(temp);
                    } 
                }
                List<string> ImageUrls = db.IncomePaymentFiles.Where(x => x.BatchPaymentId == batchPayment.Id).Select(x => x.FileUrl).ToList();
                return Json(new { batchpayment = batchPayment, assignmentReimbursement, retainerFixed,bonus, images = ImageUrls }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult WorkerPaymentReport()
        {
            using(var db =new ApplicationDbContext())
            {
                var companyId = SessionHandler.CompanyId;
                List<ExpensePaymentViewModel> list = new List<ExpensePaymentViewModel>();
                var workerExpenseCategoryIds =
                    db.ExpenseCategories.Where(x => 
                    (x.ExpenseCategoryType == "Labor" ||
                    x.ExpenseCategoryType == "Reimburse" ||
                    x.ExpenseCategoryType == "Fixed" ||
                    x.ExpenseCategoryType== "Retainer" ||
                    x.ExpenseCategoryType == "Bonus") && x.Type==2).Select(x => x.Id).ToList();
                var BatchPayments = (from e in db.Expenses join p in db.ExpensePayment on e.Id equals p.ExpenseId join be in db.ExpenseBatchPayments on p.ExpenseBatchPaymentId equals be.Id where workerExpenseCategoryIds.Contains(e.ExpenseCategoryId) && p.ExpenseBatchPaymentId != null && be.CompanyId == companyId select be).Distinct().ToList();
                var ExpensePayments = (from p in db.ExpensePayment join e in db.Expenses on p.ExpenseId equals e.Id where p.ExpenseBatchPaymentId ==null && e.CompanyId == companyId where workerExpenseCategoryIds.Contains(e.ExpenseCategoryId) select new { Payment = p, Expense = e}).ToList();
                foreach (var batch in BatchPayments)
                {
                    ExpensePaymentViewModel temp = new ExpensePaymentViewModel();
                    temp.Id = batch.Id;
                    temp.Description = batch.Description;
                    temp.Date = batch.PaymentDate;// batch.PaymentDate.ToString("MMMM d,yyyy");
                    temp.Action = "";
                    var expensePayments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batch.Id).ToList();
                    decimal dueAmount = 0;
                    dueAmount = expensePayments.Sum(x => x.Amount);

                    var totalpay = expensePayments.Sum(x => x.Amount);
                    var unallocated = batch.Amount - totalpay;
                    temp.Status = unallocated == 0 ? "Full-allocated" : batch.Amount == unallocated ? "Unallocated" : "Partial-allocated";
                    temp.Amount = batch.Amount;
                    temp.Balance = unallocated;
                    var expensepayments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batch.Id).ToList();
                    if (expensepayments.Count > 0)
                    {
                        foreach (var pay in expensepayments)
                        {
                            var expense = db.Expenses.Where(x => x.Id == pay.ExpenseId).FirstOrDefault();
                            if (expense != null)
                            {
                                var property = db.Properties.Where(x => x.Id == expense.PropertyId).FirstOrDefault();
                                var tempayments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                                tempayments.Sum(x => x.Amount);
                                temp.ExpensesPaid += (property != null ? "(" + property.Name + ")" : "") + expense.DueDate.ToString("MMM d,yyyy") + " - " + expense.Description + " - " + pay.Amount + "</br>";
                            }

                        }
                    }
                    else
                    {
                        temp.ExpensesPaid = "-";
                    }
                    temp.Action = "<button title=\"Allocate Payment\" class=\"ui mini button allocate-btn\"><i class=\"align edit icon\"></i></button>";
                    list.Add(temp);
                }
                foreach (var expensePayment in ExpensePayments)
                {
                    try
                    {
                        var property = db.Properties.Where(x => x.Id == expensePayment.Expense.PropertyId).FirstOrDefault();
                        ExpensePaymentViewModel temp = new ExpensePaymentViewModel();
                        temp.Amount = expensePayment.Payment.Amount;
                        temp.Description = expensePayment.Payment.Description;
                        temp.Date = expensePayment.Payment.CreatedAt;
                        temp.ExpensesPaid += "(" + property.Name + ")" + expensePayment.Expense.DueDate.ToString("MMM d,yyyy") + " - " + expensePayment.Expense.Description + " - " + expensePayment.Payment.Amount + "</br>";
                        temp.Status = "Complete";
                        temp.Action = "";
                        list.Add(temp);
                    }
                    catch { }
                }
                return Json(new { data =list }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetWorkerReimbursementReport(string workerIds, string propertyIds, string month)
        {
            try
            {
                var companyId = SessionHandler.CompanyId;
                List<int> pIds = propertyIds.Split(',').Select(int.Parse).ToList();
                List<int> wIds = workerIds.Split(',').Select(int.Parse).ToList();
                List<ReimbursementReportTableData> reportList = new List<ReimbursementReportTableData>();
                var date = month == "" ? DateTime.Now : month.ToDateTime();
                using (var db = new ApplicationDbContext())
                {

                    var result = (from a in db.WorkerAssignments
                                  join r in db.ReimbursementNotes on a.Id equals r.AssignmentId
                                  join w in db.Workers on a.WorkerId equals w.Id
                                  where ((wIds.Contains(0) ? true : wIds.Contains(a.WorkerId)) && (pIds.Contains(0) ? true : pIds.Contains(a.PropertyId))
                                 && (a.StartDate.Year == date.Year && a.StartDate.Month == date.Month) && a.CompanyId == companyId)
                                  select new { Id = r.Id, Name = w.Firstname + w.Lastname, r.Note, r.Amount, Date = a.StartDate, r.AssignmentId }).ToList();

                    var properties = db.Properties.Where(x => pIds.Contains(x.Id)).ToList();
                    foreach (var property in properties)
                    {
                        if (property != null)
                        {
                            if (property.IsParentProperty)
                            {
                                var childIds = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == property.Id && pc.CompanyId == SessionHandler.CompanyId && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p.Id).ToList();
                                //db.Properties.Where(x => x.ParentPropertyId == property.Id).ToList();
                                foreach (var childId in childIds)
                                {
                                    result.AddRange((from a in db.WorkerAssignments
                                                     join r in db.ReimbursementNotes on a.Id equals r.AssignmentId
                                                     join w in db.Workers on a.WorkerId equals w.Id
                                                     where ((wIds.Contains(0) ? true : wIds.Contains(a.WorkerId)) && a.PropertyId == childId
                                                        && (a.StartDate.Year == date.Year && a.StartDate.Month == date.Month) && a.CompanyId == companyId)
                                                     select new { Id = r.Id, Name = w.Firstname + w.Lastname, r.Note, r.Amount, Date = a.StartDate, r.AssignmentId }).ToList());
                                }
                            }
                        }
                    }
                    foreach (var r in result)
                    {
                        var reservationId = db.WorkerAssignments.Where(x => x.Id == r.AssignmentId).FirstOrDefault().ReservationId;
                        var reservation = db.Inquiries.Where(x => x.Id == reservationId).FirstOrDefault();
                        ReimbursementReportTableData rr = new ReimbursementReportTableData();
                        if (reservation != null)
                        {
                            rr.HostId = reservation.HostId.ToString();
                            rr.ThreadId = reservation.ThreadId.ToString();
                            rr.InquiryId = reservation.Id.ToString();
                        }
                        rr.ReimbursementId = r.Id;
                        rr.Name = r.Name;
                        rr.Amount = r.Amount;
                        rr.Note = r.Note;
                        rr.Date = r.Date.ToString("MMM d, yyyy h:mmtt (dddd)");
                        var expense = db.Expenses.Where(x => x.ReimbursementId == r.Id).FirstOrDefault();
                        if (expense != null)
                        {
                            var temp = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                            if (temp.Sum(x => x.Amount) == expense.Amount)
                            {
                                rr.ExpenseStatus = "Paid";
                                rr.Checkbox = "";
                            }
                            else if (temp.Sum(x => x.Amount) != 0 && temp.Sum(x => x.Amount) < expense.Amount)
                            {
                                rr.ExpenseStatus = "Partial";
                            }
                            else
                            {
                                rr.ExpenseStatus = "Unpaid";
                            }
                        }
                        else
                        {
                            rr.ExpenseStatus = "Unpaid";
                        }


                        reportList.Add(rr);
                    }
                    return Json(new { data = reportList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { data = new List<ReimbursementReportTableData>() }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult GetWorkerBonus(string month)
        {
            try
            {
                var now = DateTime.Now;
                var m = month == "" ? new DateTime(now.Year, now.Month, 1) : month.ToDateTime();
                var dt = new DateTime(m.Year, m.Month, 1);
                var duedate = dt.ToString("MMM dd,yyyy");
                int expenseCategoryId = objIList.GetExpenseCategoryId("Bonus");
                using (var db = new ApplicationDbContext())
                {
                    var workers = (from w in db.Workers where w.CompanyId == SessionHandler.CompanyId select new { ExpenseBonus = db.Expenses.Where(x => x.ExpenseCategoryId == expenseCategoryId && x.Description == w.Firstname + " " + w.Lastname && (x.DueDate.Month == dt.Month && x.DueDate.Year == dt.Year)).ToList().Count(),AssignmentCount = db.WorkerAssignments.Where(x => x.WorkerId == w.Id && x.StartDate.Month == dt.Month && x.StartDate.Year == dt.Year).ToList().Count, DueDate = duedate, Description = w.Firstname + " " + w.Lastname, Amount = w.Bonus }).ToList();
                    workers = workers.Where(x => x.AssignmentCount > 0).ToList();
                    workers = workers.Where(x => x.ExpenseBonus == 0).ToList();
                    return Json(new { workers = workers }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { workers = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetWorkerWithoutAssignment(string month)
        {
            try
            {
                var now = DateTime.Now;
                var m = month == "" ? new DateTime(now.Year, now.Month, 1) : month.ToDateTime();
                var dt = new DateTime(m.Year, m.Month, 1);
                var duedate = dt.ToString("MMM dd,yyyy");
                //JM 5/11/2020 it will check if Bonus is already paid
                int expenseCategoryId = objIList.GetExpenseCategoryId("Bonus");
                using (var db = new ApplicationDbContext())
                {
                    var workers = (from w in db.Workers where w.CompanyId == SessionHandler.CompanyId select new { ExpenseBonus = db.Expenses.Where(x=>x.ExpenseCategoryId == expenseCategoryId && x.Description== w.Firstname + " " + w.Lastname &&(x.DueDate.Month == dt.Month && x.DueDate.Year == dt.Year)).ToList().Count(),AssignmentCount = db.WorkerAssignments.Where(x => x.WorkerId == w.Id && x.StartDate.Month == dt.Month && x.StartDate.Year == dt.Year).ToList().Count, Description = w.Firstname + " " + w.Lastname,Id = w.Id }).ToList();
                    //JM 5/11/2020 worker must no assignment and no bonus
                    workers = workers.Where(x => x.AssignmentCount == 0).ToList();
                    workers = workers.Where(x => x.ExpenseBonus == 0).ToList();

                    return Json(new { workers = workers }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { workers = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetWorkerWithoutAssignmentById(int id,string month)
        {
            try
            {
                var now = DateTime.Now;
                var m = month == "" ? new DateTime(now.Year, now.Month, 1) : month.ToDateTime();
                var dt = new DateTime(m.Year, m.Month, 1);
                var duedate = dt.ToString("MMM dd,yyyy");
                using (var db = new ApplicationDbContext())
                {
                    var worker= (from w in db.Workers where w.Id== id select new { AssignmentCount = db.WorkerAssignments.Where(x => x.WorkerId == w.Id && x.StartDate.Month == dt.Month && x.StartDate.Year == dt.Year).ToList().Count, DueDate = duedate, Description = w.Firstname + " " + w.Lastname, Amount = w.Bonus }).FirstOrDefault();

                    return Json(new { worker = worker }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { workers = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetWorkerRetainerFixedReport(string workerIds, string month)
        {
            try
            {
                var now = DateTime.Now;
                List<int> wIds = workerIds.Split(',').Select(int.Parse).ToList();
                var m = month == "" ? new DateTime(now.Year, now.Month, 1) : month.ToDateTime();
                var dt = new DateTime(m.Year, m.Month, 1);
                List<WorkerRetainerFixedReportTableData> reportList = new List<WorkerRetainerFixedReportTableData>();
                using (var db = new ApplicationDbContext())
                {

                    var workerFixed = (from w in db.Workers where w.FixedRate != null && w.FixedRate != 0.0M where (wIds.Contains(0) ? true : wIds.Contains(w.Id)) select w).ToList();
                    var fixedExpenseCategory = objIList.GetExpenseCategoryId("Fixed");


                    foreach (var wf in workerFixed)
                    {
                        WorkerRetainerFixedReportTableData wr = new WorkerRetainerFixedReportTableData
                        {
                            Name = wf.Firstname + " " + wf.Lastname,
                            Amount = wf.FixedRate.ToString(),
                            Date = dt.ToString("MMMM yyyy"),
                            Type = "Fixed"

                        };
                        var fixedExpense = db.Expenses.Where(x => x.Description == wr.Name && x.ExpenseCategoryId == fixedExpenseCategory && x.DueDate.Year == dt.Year && x.DueDate.Month == dt.Month).FirstOrDefault();
                        if (fixedExpense != null)
                        {
                            var temp = db.ExpensePayment.Where(x => x.ExpenseId == fixedExpense.Id).ToList();
                            if (temp.Sum(x => x.Amount) == fixedExpense.Amount)
                            {
                                wr.ExpenseStatus = "Paid";
                                wr.Checkbox = "";
                            }
                            else if (temp.Sum(x => x.Amount) != 0 && temp.Sum(x => x.Amount) < fixedExpense.Amount)
                            {
                                wr.ExpenseStatus = "Partial";
                            }
                            else
                            {
                                wr.ExpenseStatus = "Unpaid";
                            }
                        }
                        else
                        {
                            wr.ExpenseStatus = "Unpaid";
                        }
                        wr.HasExpense = fixedExpense != null ? true : false;
                        reportList.Add(wr);
                    }

                    var retainerExpenseCategory = objIList.GetExpenseCategoryId("Retainer");

                    var workers = db.Workers.Where(x => x.CompanyId == SessionHandler.CompanyId && (wIds.Contains(0) ? true : wIds.Contains(x.Id))).ToList();
                    foreach (var worker in workers)
                    {
                        WorkerRetainer retainer = db.WorkerRetainers.Where(x => x.EffectiveDate <= dt && x.WorkerId == worker.Id).OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

                        if (retainer != null)
                        {
                            WorkerRetainerFixedReportTableData wr = new WorkerRetainerFixedReportTableData
                            {
                                Name = worker.Firstname + " " + worker.Lastname,
                                Amount = retainer.Rate.ToString(),
                                Date = dt.ToString("MMMM yyyy"),
                                Type = "Retainer"

                            };
                            var retainerExpense = db.Expenses.Where(x => x.Description == wr.Name && x.ExpenseCategoryId == retainerExpenseCategory && x.DueDate.Year == dt.Year && x.DueDate.Month == dt.Month).FirstOrDefault();
                            if (retainerExpense != null)
                            {
                                var temp = db.ExpensePayment.Where(x => x.ExpenseId == retainerExpense.Id).ToList();
                                //decimal dueAmount = 0;
                                if (temp.Sum(x => x.Amount) == retainerExpense.Amount)
                                {
                                    wr.ExpenseStatus ="Paid";
                                    wr.Checkbox = "";
                                }
                                else if(temp.Sum(x => x.Amount)!=0 && temp.Sum(x => x.Amount) < retainerExpense.Amount)
                                {
                                    wr.ExpenseStatus = "Partial";
                                }
                                else {
                                    wr.ExpenseStatus = "Unpaid";
                                }
                            }
                            else
                            {
                                wr.ExpenseStatus = "Unpaid";
                            }
                            wr.HasExpense = retainerExpense != null ? true : false;
                            reportList.Add(wr);
                        }
                    }
                    return Json(new { data = reportList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { data = new List<WorkerRetainerFixedReportTableData>() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetWorkerAssignmentReport(string workerIds, string propertyIds, string month)
        {

            try
            {
                List<int> wIds = workerIds.Split(',').Select(int.Parse).ToList();
                List<int> pIds = propertyIds.Split(',').Select(int.Parse).ToList();
                var companyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {

                    var dt = month == "" ? DateTime.Now : month.ToDateTime();
                    if (dt.Year == DateTime.MinValue.Year)
                    {
                        dt = DateTime.Now;
                    }
                    List<AssignmentReportTableData> reportList = new List<AssignmentReportTableData>();
                    var results = (from a in db.WorkerAssignments
                                   join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                                   join p in db.Properties on a.PropertyId equals p.Id
                                   join j in db.WorkerJobs
                                         on new { w.Id, a.JobTypeId }
                                     equals new { Id = j.WorkerId, j.JobTypeId }
                                   join t in db.WorkerJobTypes on new { JobTypeId = j.JobTypeId } equals new { JobTypeId = t.Id }
                                   where a.CompanyId == companyId
                                   where ((wIds.Contains(0) ? true : wIds.Contains(a.WorkerId)) && (pIds.Contains(0) ? true : pIds.Contains(a.PropertyId)) && (a.StartDate.Month == dt.Month && a.StartDate.Year == dt.Year))
                                   orderby a.StartDate ascending
                                   select new
                                   {
                                       a.Id,
                                       a.Description,
                                       w.FixedRate,
                                       w.Firstname,
                                       w.Lastname,
                                       a.StartDate,
                                       a.EndDate,
                                       Property = p.Name,
                                       j.PaymentType,
                                       JobType = t.Name,
                                       j.PaymentRate,
                                       a.ReservationId,
                                   }).ToList();

                    var properties = db.Properties.Where(x => pIds.Contains(x.Id)).ToList();
                    foreach (var property in properties)
                    {
                        if (property != null)
                        {
                            if (property.IsParentProperty && property.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync)
                            {
                                var childs = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == property.Id && pc.CompanyId == SessionHandler.CompanyId && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p).ToList();

                                //var childs = db.Properties.Where(x => x.ParentPropertyId == propertyId).ToList();
                                foreach (var child in childs)
                                {
                                    results.AddRange((from a in db.WorkerAssignments
                                                      join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                                                      join p in db.Properties on a.PropertyId equals p.Id
                                                      join j in db.WorkerJobs
                                                            on new { w.Id, a.JobTypeId }
                                                        equals new { Id = j.WorkerId, j.JobTypeId }
                                                      join t in db.WorkerJobTypes on new { JobTypeId = j.JobTypeId } equals new { JobTypeId = t.Id }
                                                      where a.CompanyId == companyId
                                                      where ((wIds.Contains(0) ? true : wIds.Contains(a.WorkerId)) && a.PropertyId == child.Id && (a.StartDate.Month == dt.Month && a.StartDate.Year == dt.Year))
                                                      orderby a.StartDate ascending
                                                      select new
                                                      {
                                                          a.Id,
                                                          a.Description,
                                                          w.FixedRate,
                                                          w.Firstname,
                                                          w.Lastname,
                                                          a.StartDate,
                                                          a.EndDate,
                                                          Property = p.Name,
                                                          j.PaymentType,
                                                          JobType = t.Name,
                                                          j.PaymentRate,
                                                          a.ReservationId,
                                                      }).ToList());
                                }
                            }
                        }
                    }

                    var assignmentExpenseCategory = objIList.GetExpenseCategoryId("Labor");
                    results = results.Distinct().ToList();
                    foreach (var item in results)
                    {
                        AssignmentReportTableData td = new AssignmentReportTableData();
                        var reservation = db.Inquiries.Where(x => x.Id == item.ReservationId).FirstOrDefault();
                        td.Action = "<a assignmentId=\"" + item.Id + "\" class=\"ui mini button delete-assignment\" title=\"Delete\">" +
                                              "<i class=\"icon trash\"></i>" +
                                          "</a>";
                        td.AssignmentId = item.Id;
                        td.Description = item.Description;
                        td.WorkerName = item.Firstname + " " + item.Lastname;
                        td.AssignmentStartDate = item.StartDate.ToString("MMM d, yyyy h:mmtt (dddd)");
                        td.AssignmentEndDate = item.EndDate.ToString("MMM d, yyyy h:mmtt (dddd)");
                        td.Property = item.Property;
                        td.Job = item.JobType;
                        if (item.FixedRate == 0.0M || item.FixedRate == null)
                        {
                            td.Rate = string.Format("{0:#,0.00}", item.PaymentRate);
                            if (item.PaymentType == 1)
                            {
                                td.Unit = item.EndDate.Subtract(item.StartDate).TotalHours + " hrs";
                                td.Amount = string.Format("{0:#,0.00}", Convert.ToDecimal(item.EndDate.Subtract(item.StartDate).TotalMinutes.ToInt() * (item.PaymentRate / 60)));
                            }
                            else if (item.PaymentType == 2)
                            {
                                TimeSpan difference = item.EndDate.Date - item.StartDate.Date;
                                int numOfDays = difference.TotalDays.ToInt() == 0 ? 1 : difference.TotalDays.ToInt() > 1 ? difference.TotalDays.ToInt() : 0;
                                td.Unit = numOfDays + " days";
                                td.Amount = string.Format("{0:#,0.00}", Convert.ToDecimal(numOfDays * item.PaymentRate));
                            }
                            else if (item.PaymentType == 3)
                            {
                                td.Unit = "1 Appointment";
                                td.Amount = string.Format("{0:#,0.00}", Convert.ToDecimal(item.PaymentRate));
                            }
                        }
                        else { td.Amount = "0"; }
                        var expense= db.Expenses.Where(x => x.ExpenseCategoryId == assignmentExpenseCategory &&x.WorkerAssignmentId == item.Id).FirstOrDefault();
                        if (expense != null)
                        {
                            var temp = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                            //decimal dueAmount = 0;
                            if (temp.Sum(x => x.Amount) == expense.Amount)
                            {
                                td.ExpenseStatus = "Paid";
                                td.Checkbox = "";
                            }
                            else if (temp.Sum(x => x.Amount) != 0 && temp.Sum(x => x.Amount) < expense.Amount)
                            {
                                td.ExpenseStatus = "Partial";
                            }
                            else
                            {
                                td.ExpenseStatus = "Unpaid";
                            }
                        }
                        reportList.Add(td);
                    }
                    return Json(new { data = reportList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { data = new List<PropertyListing>() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult RetainerFixedGenerateExpense(string expenses)
        {
            var js = new JavaScriptSerializer();
            var ExpensesJson = js.Deserialize<List<RetainerExpenseModel>>(expenses);
            ExpensesJson.RemoveAt(0);
            using (var db = new ApplicationDbContext())
            {
                foreach (var item in ExpensesJson)
                {
                    if (item.Amount == "No data available in table")
                    {
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);

                    }
                    int expenseCategoryId = objIList.GetExpenseCategoryId(item.Type);

                    var date = item.Date.ToDateTime();
                    int paymentMethodId = objIList.GetWorkerDefaultPaymentMethodId();

                    var dt = item.Date.ToDateTime();
                    DateTime dueDate = new DateTime(dt.Year, dt.Month,
                                     DateTime.DaysInMonth(dt.Year, dt.Month));

                    var temp = db.Expenses.Where(x => x.Description == item.Worker && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId == expenseCategoryId).FirstOrDefault();
                    if (temp == null)
                    {
                        Expense e = new Expense();
                        e.Description = item.Worker;
                        e.ExpenseCategoryId = expenseCategoryId;
                        e.PropertyId = 0;
                        e.DueDate = dueDate;
                        e.PaymentMethodId = paymentMethodId;
                        e.DateLastJobRun = null;
                        e.DateCreated = DateTime.Now;
                        e.DateNextJobRun = null;
                        e.CompanyId = SessionHandler.CompanyId;
                        //e.IsPaid = false;
                        e.Amount = item.Amount.ToDecimal();// assignment.PaymentRate;
                        e.WorkerAssignmentId = 0;//workerAssignmentId;
                        db.Expenses.Add(e);
                        db.SaveChanges();
                    }
                    else
                    {
                        temp.DueDate = dueDate;
                        temp.Amount = item.Amount.ToDecimal();
                        db.SaveChanges();
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteNonBookingAssignment(int assignmentId)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == assignmentId).FirstOrDefault();
                if (assignment != null)
                {
                    var reimbusement = db.ReimbursementNotes.Where(x => x.AssignmentId == assignmentId).ToList();
                    var expenses = db.Expenses.Where(x => x.WorkerAssignmentId == assignment.Id).ToList();
                    db.WorkerAssignments.Remove(assignment);
                    db.ReimbursementNotes.RemoveRange(reimbusement);
                    db.Expenses.RemoveRange(expenses);
                    db.SaveChanges();
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult OnTheWayAssignment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == id).FirstOrDefault();
                assignment.InDayStatus = 1;
                db.Entry(assignment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult StartAssignment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == id).FirstOrDefault();
                assignment.StartDate = DateTime.Now;
                assignment.InDayStatus = 2;
                db.Entry(assignment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult FinishAssignment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == id).FirstOrDefault();
                assignment.InDayStatus = 3;
                assignment.EndDate = DateTime.Now;
                db.Entry(assignment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AcceptAssignment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == id).FirstOrDefault();
                assignment.Status = 1;
                db.Entry(assignment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeclineAssignment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == id).FirstOrDefault();
                assignment.Status = 2;
                db.Entry(assignment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CancelAssignment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var assignment = db.WorkerAssignments.Where(x => x.Id == id).FirstOrDefault();
                assignment.Status = 3;
                db.Entry(assignment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetUnhiredWorkers(/*int WorkerTypeId, string search*/)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {

                List<WorkerDetails> WorkerList = new List<WorkerDetails>();
                using (var db = new ApplicationDbContext())
                {
                    //var Workers = (from w in db.Workers join t in db.CommunicationThreads on w.CompanyId equals t.CompanyId into cthread from ct in cthread.DefaultIfEmpty() select w ).ToList();
                    var Workers= (from w in db.Workers
                     from t in db.CommunicationThreads.DefaultIfEmpty()
                     where w.CompanyId !=SessionHandler.CompanyId
                     select w).ToList();
                    foreach (var w in Workers)
                    {
                        var Mobiles = db.WorkerContactInfos.Where(x => x.WorkerId == w.Id).ToList();
                        string contacts = "";
                        foreach (var item in Mobiles)
                        {
                            contacts += item.Value + ",";
                        }
                        WorkerDetails workerDetails = new WorkerDetails
                        {

                            Firstname = w.Firstname,
                            Lastname = w.Lastname,
                            Mobile = contacts,
                            Action = "<a class=\"ui mini button js-hired-worker\" name=\"" + w.Id + "\" data-tooltip=\"Hire\"> <i class=\"icon edit\"></i> </a>"

                        };
                        WorkerList.Add(workerDetails);
                    }
                }
                return Json(new { data = WorkerList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult HireWorker(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var thread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && x.WorkerId == id).FirstOrDefault();
                if(thread == null)
                {

                        CommunicationThread workerThread = new CommunicationThread();
                        db.CommunicationThreads.Add(workerThread);
                        workerThread.WorkerId = id;
                        workerThread.ThreadId = PasswordGenerator.Generate(20);
                        workerThread.CompanyId = SessionHandler.CompanyId;
                        db.CommunicationThreads.Add(workerThread);
                        db.SaveChanges();
                
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDefaultWorker(string list)
        {
            var js = new JavaScriptSerializer();
            var DefaultList = js.Deserialize<List<PropertyDefaultWorker>>(list);
            using(var db = new ApplicationDbContext())
            {
                foreach (var defaultWorker in DefaultList)
                {
                    if (defaultWorker.Id == 0)
                    {
                        db.PropertyDefaultWorkers.Add(defaultWorker);
                        db.SaveChanges();
                    }
                    else
                    {
                        var tempdefault = db.PropertyDefaultWorkers.Where(x => x.Id == defaultWorker.Id).FirstOrDefault();
                        tempdefault.PropertyId = defaultWorker.PropertyId;
                        tempdefault.JobTypeId = defaultWorker.JobTypeId;
                        tempdefault.Type = defaultWorker.Type;
                        tempdefault.WorkerId = defaultWorker.WorkerId;
                        tempdefault.IsAutoDelete = defaultWorker.IsAutoDelete;
                        db.Entry(tempdefault).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                    
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSearch(WorkerReportSearchCriteria model)
        {

            using (var db = new ApplicationDbContext())
            {
                model.UserId = User.Identity.Name.ToInt();
                db.WorkerReportSearchCriterias.Add(model);
                db.SaveChanges();
            }
            return Json(new { searchCriteria = model }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetSearchCriteria()
        {
            using (var db = new ApplicationDbContext())
            {
                var userId = User.Identity.Name.ToInt();
                var data = db.WorkerReportSearchCriterias.Where(x=>x.UserId ==userId).ToList();
                return Json(new { success = true, searchCriterias = data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult SearchCriteriaGetData(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = db.WorkerReportSearchCriterias.Where(x => x.Id == id).FirstOrDefault();
                return Json(new { success = true, searchCriteria = data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult JobTypeReport(string workerIds, string propertyIds, string month)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {

                    var dt = month == "" ? DateTime.Now : month.ToDateTime();
                    List<int> wIds = workerIds.Split(',').Select(int.Parse).ToList();
                    List<int> pIds = propertyIds.Split(',').Select(int.Parse).ToList();
                    List<int> propIds = new List<int>();
                    if (!pIds.Contains(0))
                    {
                        foreach (var id in pIds)
                        {
                            var prop = db.Properties.Where(x => x.Id == id).FirstOrDefault();
                            if (prop.IsParentProperty)
                            {
                                var childs = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == prop.Id && pc.CompanyId == SessionHandler.CompanyId && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p.Id).ToList();
                                propIds.Add(prop.Id);
                                propIds.AddRange(childs);
                            }
                            else
                            {
                                propIds.Add(id);
                            }
                        }
                    }
                    else
                    {
                        propIds.Add(0);
                    }
                    List<WorkerReportByJobType> reports = new List<WorkerReportByJobType>();
                    var workers = db.Workers.Where(x => wIds.Contains(0) ? true : wIds.Contains(x.Id)).ToList();
                    foreach (var worker in workers)
                    {
                        List<JobTypeReport> jobTypeReports = new List<JobTypeReport>();
                        var jobTypes = (from j in db.WorkerJobs join wj in db.WorkerJobTypes on j.JobTypeId equals wj.Id join w in db.Workers on j.WorkerId equals w.Id where w.Id == worker.Id select new { Job = j, JobType = wj }).ToList();
                        foreach (var job in jobTypes)
                        {
                            var assignments = db.WorkerAssignments.Where(x => x.WorkerId == worker.Id && x.JobTypeId == job.JobType.Id && (propIds.Contains(0) ? true : propIds.Contains(x.PropertyId)) && (x.StartDate.Month == dt.Month && x.StartDate.Year == dt.Year)).ToList();
                            if (assignments.Count > 0)
                            {
                                jobTypeReports.Add(new Models.Worker.JobTypeReport() { Amount = job.Job.PaymentRate, Count = assignments.Count, JobType = job.JobType.Name, Total = (assignments.Count * job.Job.PaymentRate) });
                            }
                        }
                        if (jobTypeReports.Count > 0)
                        {
                            reports.Add(new WorkerReportByJobType() { Worker = worker, JobTypReports = jobTypeReports });
                        }
                    }
                    return Json(new { success = true, reports }, JsonRequestBehavior.AllowGet);
                }catch(Exception e)
                {
                    return Json(new { success = true, reports = new List<WorkerReportByJobType>() }, JsonRequestBehavior.AllowGet);
                }
            }

        }
    }
}