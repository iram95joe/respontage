﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Repository;
using MessagerSolution.Enumerations;
using MessagerSolution.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class FormController : Controller
    {
        [HttpGet]
        [AllowAnonymous]
        public ActionResult TravelEvent(/*string enc,*/ string id)
        {
            int result = 0;
            if (/*string.IsNullOrEmpty(enc) || */string.IsNullOrEmpty(id) || !int.TryParse(id, out result))
            {
                return View("Error_404");
            }

            TravelEventFormViewModel model = new TravelEventFormViewModel();
            ICommonRepository repo = new CommonRepository();

            var inquiryId = int.Parse(id);

            var travelEventsList = repo.GetAllTravelEvents(inquiryId);

            var travelGroupInfoList = travelEventsList.Select(item => new TravelGroupInfo()
            {
                Id = item.Id,
                InquiryId = (int)item.ReservationId,
                HashValue = item.HashValue,
                TotalPersonInGroup = item.NumberOfPeople,
                ContactPerson = item.ContactPerson,
                ContactNumber = item.ContactNumber,
                People = item.People,
                TravelType = item.NoteType == (int)NoteTypeEnum.CHECK_OUT ? (int)NoteTypeEnum.CHECK_OUT : item.NoteType == (int)NoteTypeEnum.CHECK_IN? (int)NoteTypeEnum.CHECK_IN: item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (int)TravelTypeEnum.ARRIVAL : (int)TravelTypeEnum.DEPARTURE,
                TravelDateTime = item.NoteType == (int)NoteTypeEnum.CHECK_OUT ? item.Checkout.Value.ToShortDateString() : item.NoteType == (int)NoteTypeEnum.CHECK_IN ? item.Checkin.Value.ToShortDateString() : item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (item.ArrivalDateTime.Value == null ? null : item.ArrivalDateTime.Value.ToShortDateString()) : (item.DepartureDateTime.Value == null ? null : item.DepartureDateTime.Value.ToShortDateString()),
                NoteType = item.NoteType,
            }).ToList();

            model.TravelGroupInfoList = travelGroupInfoList;
            model.ArrivalTravelTypes = repo.GetTravelTypeList(TravelTypeEnum.ARRIVAL);
            model.DepartureTravelTypes = repo.GetTravelTypeList(TravelTypeEnum.DEPARTURE);

            using (var db = new ApplicationDbContext()) {
                var inquiry = db.Inquiries.Where(x => x.Id == inquiryId).FirstOrDefault();

                ViewBag.AllowedSelfCheckinCheckout = db.Properties.Where(x => x.ListingId == inquiry.PropertyId).FirstOrDefault().AllowedSelfCheckinCheckout;
            }
            //bool isHashExpired = true;



            //var hashObject = repo.GetHashInfo(enc);

            //if (hashObject != null)
            //{
            //    isHashExpired = repo.IsHashValueExpired(hashObject);
            //}

            //if (!isHashExpired)
            //{
            //    return View(model);
            //}

            //return View("Error_404");
            return View(model);
        }


        [HttpPost]
        public ActionResult RegisterTravelGroup(TravelGroupInfo data)
        {
            var response = new Response();
            ICommonRepository repo = new CommonRepository();

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    Note model = new Note();

                    model.ReservationId = data.InquiryId;
                   
                    model.NumberOfPeople = data.TotalPersonInGroup;
                    model.ContactPerson = data.ContactPerson;
                    model.ContactNumber = data.ContactNumber;
                    model.HashValue = data.HashValue;
                    model.NoteType = data.NoteType;
                    model.People = data.People;
                    model.Checkin = data.Checkin ==null? (DateTime?)null :DateTime.Parse(data.Checkin);
                    model.Checkout = data.Checkout == null ? (DateTime?)null : DateTime.Parse(data.Checkout);
                    if (model.Checkin != null)
                    {
                        model.NoteType = 13;
                        db.tbNote.Add(model);
                        db.SaveChanges();
                    }
                    if (model.Checkout != null)
                    {
                        model.NoteType = 14;
                        db.tbNote.Add(model);
                        db.SaveChanges();

                    }


                    model.NoteType = data.NoteType;
                    if (data.TravelType == 1)
                    {
                        model.ArrivalDateTime = Convert.ToDateTime(data.TravelDateTime);
                        model.Airline = data.Airline;
                        model.AirportCodeArrival = data.AirportCodeArrival;
                        model.ArrivalAirportCode = data.AirportCodeArrival;
                        model.AirportCodeDeparture = data.AirportCodeDeparture;
                        model.FlightNumber = data.FlightNumber;

                        model.CarColor = data.CarColor;
                        model.CarType = data.CarType;
                        model.PlateNumber = data.CarPlateNumber;

                        model.CruiseName = data.CruiseName;
                        model.ArrivalPort = data.CruiseArrivalPort;
                        model.DeparturePort = data.CruiseDeparturePort;

                        model.TerminalStation = data.TerminalStation;
                        model.Name = data.Name;
                    }
                    else if (data.TravelType == 2)
                    {
                        model.DepartureDateTime = Convert.ToDateTime(data.TravelDateTime);
                        model.Airline = data.Airline;
                        model.AirportCodeArrival = data.AirportCodeArrival;
                        model.ArrivalAirportCode = data.AirportCodeArrival;
                        model.AirportCodeDeparture = data.AirportCodeDeparture;
                        model.FlightNumber = data.FlightNumber;

                        model.CarColor = data.CarColor;
                        model.CarType = data.CarType;
                        model.PlateNumber = data.CarPlateNumber;
                        model.DepartureCity = data.DepartureCity;

                        model.CruiseName = data.CruiseName;
                        model.ArrivalPort = data.CruiseArrivalPort;
                        model.DeparturePort = data.CruiseDeparturePort;

                        model.TerminalStation = data.TerminalStation;
                        model.Name = data.Name;
                    }

                    db.tbNote.Add(model);
                    db.SaveChanges();

                    var travelEventsList = repo.GetAllTravelEvents(data.InquiryId);

                    var travelGroupInfoList = travelEventsList.Select(item => new TravelGroupInfo
                    {
                        Id = item.Id,
                        InquiryId = (int)item.ReservationId,
                        HashValue = item.HashValue,
                        TotalPersonInGroup = item.NumberOfPeople,
                        ContactPerson = item.ContactPerson,
                        ContactNumber = item.ContactNumber,
                        People = item.People,
                        TravelType = item.NoteType == (int)NoteTypeEnum.CHECK_OUT ? (int)NoteTypeEnum.CHECK_OUT : item.NoteType == (int)NoteTypeEnum.CHECK_IN ? (int)NoteTypeEnum.CHECK_IN : item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (int)TravelTypeEnum.ARRIVAL : (int)TravelTypeEnum.DEPARTURE,
                        TravelDateTime = item.NoteType == (int)NoteTypeEnum.CHECK_OUT ? item.Checkout.Value.ToShortDateString() : item.NoteType == (int)NoteTypeEnum.CHECK_IN ? item.Checkin.Value.ToShortDateString() : item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (item.ArrivalDateTime.Value == null ? null : item.ArrivalDateTime.Value.ToShortDateString()) : (item.DepartureDateTime.Value == null ? null : item.DepartureDateTime.Value.ToShortDateString()),
                        NoteType = item.NoteType,
                    }).ToList();

                    response.TravelGroupInfoList = travelGroupInfoList;
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
            }
            return Json(response);
        }

        [HttpPost]
        public ActionResult DeleteTravelEventRecord(int travelEventRecordId, int inquiryId)
        {
            ICommonRepository repo = new CommonRepository();
            var response = new Response();

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var entity = db.tbNote.FirstOrDefault(x => x.Id == travelEventRecordId);
                    db.Entry(entity).State = EntityState.Deleted;
                    db.SaveChanges();

                    var travelEventsList = repo.GetAllTravelEvents(inquiryId);

                    var travelGroupInfoList = travelEventsList.Select(item => new TravelGroupInfo
                    {
                        Id = item.Id,
                        InquiryId = (int)item.ReservationId,
                        HashValue = item.HashValue,
                        TotalPersonInGroup = item.NumberOfPeople,
                        ContactPerson = item.ContactPerson,
                        ContactNumber = item.ContactNumber,
                        People = item.People,
                        TravelType = item.NoteType == (int)NoteTypeEnum.CHECK_OUT ? (int)NoteTypeEnum.CHECK_OUT : item.NoteType == (int)NoteTypeEnum.CHECK_IN ? (int)NoteTypeEnum.CHECK_IN : item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (int)TravelTypeEnum.ARRIVAL : (int)TravelTypeEnum.DEPARTURE,
                        TravelDateTime = item.NoteType == (int)NoteTypeEnum.CHECK_OUT ? item.Checkout.Value.ToShortDateString() : item.NoteType == (int)NoteTypeEnum.CHECK_IN ? item.Checkin.Value.ToShortDateString() : item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (item.ArrivalDateTime.Value == null ? null : item.ArrivalDateTime.Value.ToShortDateString()) : (item.DepartureDateTime.Value == null ? null : item.DepartureDateTime.Value.ToShortDateString()),
                        NoteType = item.NoteType,
                        }).ToList();

                    response.TravelGroupInfoList = travelGroupInfoList;
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
            }

            return Json(response);
        }

        [HttpGet]
        public ActionResult GetTravelEventRecord(int travelEventRecordId)
        {
            ICommonRepository repo = new CommonRepository();
            var response = new GetFormTravelEventResponse();

            try
            {
                var travelEventRecord = repo.GetTravelEventRecord(travelEventRecordId);

                TravelGroupInfo responseModel = new TravelGroupInfo
                {
                    Id = travelEventRecord.Id,
                    HashValue = travelEventRecord.HashValue,
                    TotalPersonInGroup = travelEventRecord.NumberOfPeople,
                    ContactPerson = travelEventRecord.ContactPerson,
                    ContactNumber = travelEventRecord.ContactNumber,
                    People = travelEventRecord.People,
                    TravelType = travelEventRecord.NoteType == (int)NoteTypeEnum.CHECK_OUT ? (int)NoteTypeEnum.CHECK_OUT : travelEventRecord.NoteType == (int)NoteTypeEnum.CHECK_IN ? (int)NoteTypeEnum.CHECK_IN : travelEventRecord.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (int)TravelTypeEnum.ARRIVAL : (int)TravelTypeEnum.DEPARTURE,
                    TravelDateTime = travelEventRecord.NoteType == (int)NoteTypeEnum.CHECK_OUT ? travelEventRecord.Checkout.Value.ToShortDateString() : travelEventRecord.NoteType == (int)NoteTypeEnum.CHECK_IN ? travelEventRecord.Checkin.Value.ToShortDateString() : travelEventRecord.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || travelEventRecord.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (travelEventRecord.ArrivalDateTime.Value == null ? null : travelEventRecord.ArrivalDateTime.Value.ToShortDateString()) : (travelEventRecord.DepartureDateTime.Value == null ? null : travelEventRecord.DepartureDateTime.Value.ToShortDateString()),
                    NoteType = travelEventRecord.NoteType,
                    AirportCodeDeparture = travelEventRecord.AirportCodeDeparture,
                    AirportCodeArrival = travelEventRecord.AirportCodeArrival,
                    FlightNumber = travelEventRecord.FlightNumber,
                    Airline = travelEventRecord.Airline,
                    CarType = travelEventRecord.CarType,
                    CarColor = travelEventRecord.CarColor,
                    CarPlateNumber = travelEventRecord.PlateNumber,
                    DepartureCity = travelEventRecord.DepartureCity,
                    CruiseName = travelEventRecord.CruiseName,
                    CruiseArrivalPort = travelEventRecord.ArrivalPort,
                    CruiseDeparturePort = travelEventRecord.DeparturePort,
                    TerminalStation = travelEventRecord.TerminalStation,
                    Name = travelEventRecord.Name,
                    Checkin = travelEventRecord.Checkin.ToString(),
                    Checkout = travelEventRecord.Checkout.ToString()

                    
                };

                response.TravelGroupInfo = responseModel;
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateTravelEventRecord(TravelGroupInfo data)
        {
            ICommonRepository repo = new CommonRepository();
            var response = new Response();

            try
            {
                using (var db = new ApplicationDbContext())
                {

                    var record = db.tbNote.FirstOrDefault(x => x.Id == data.Id);
                    if (record != null)
                    {   record.Checkin = data.Checkin == null ? (DateTime?)null : DateTime.Parse(data.Checkin);
                        record.Checkout= data.Checkout == null ? (DateTime?)null : DateTime.Parse(data.Checkout);
                        record.NumberOfPeople = data.TotalPersonInGroup;
                        record.ContactPerson = data.ContactPerson;
                        record.ContactNumber = data.ContactNumber;
                        record.People = data.People;
                        record.NoteType = data.NoteType;
                        record.Checkin = data.Checkin == null ? (DateTime?)null : DateTime.Parse(data.Checkin);
                        record.Checkout = data.Checkout == null ? (DateTime?)null : DateTime.Parse(data.Checkout);



                        if (data.TravelType == (int)TravelTypeEnum.ARRIVAL)
                        {
                            record.ArrivalDateTime = Convert.ToDateTime(data.TravelDateTime);
                        }
                        else if (data.TravelType == (int)TravelTypeEnum.DEPARTURE)
                        {
                            record.DepartureDateTime = Convert.ToDateTime(data.TravelDateTime);
                        }

                        record.AirportCodeDeparture = data.AirportCodeDeparture;
                        record.AirportCodeArrival = data.AirportCodeArrival;
                        record.FlightNumber = data.FlightNumber;
                        record.Airline = data.Airline;
                        record.CarType = data.CarType;
                        record.CarColor = data.CarColor;
                        record.PlateNumber = data.CarPlateNumber;
                        record.DepartureCity = data.DepartureCity;
                        record.CruiseName = data.CruiseName;
                        record.ArrivalPort = data.CruiseArrivalPort;
                        record.DeparturePort = data.CruiseDeparturePort;
                        record.TerminalStation = data.TerminalStation;
                        record.Name = data.Name;
                        db.Entry(record).State = EntityState.Modified;
                    }
                    db.SaveChanges();


                    var travelEventsList = repo.GetAllTravelEvents(data.InquiryId);

                    var travelGroupInfoList = travelEventsList.Select(item => new TravelGroupInfo()
                    {
                        Id = item.Id,
                        InquiryId = (int)item.ReservationId,
                        HashValue = item.HashValue,
                        TotalPersonInGroup = item.NumberOfPeople,
                        ContactPerson = item.ContactPerson,
                        ContactNumber = item.ContactNumber,
                        People = item.People,
                        TravelType = item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (int)TravelTypeEnum.ARRIVAL : (int)TravelTypeEnum.DEPARTURE,
                        NoteType = item.NoteType,
                        TravelDateTime = item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL || item.NoteType == (int)NoteTypeEnum.FLIGHT_ARRIVAL || item.NoteType == (int)NoteTypeEnum.CRUISE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.TRAIN_ARRIVAL || item.NoteType == (int)NoteTypeEnum.DRIVE_ARRIVAL || item.NoteType == (int)NoteTypeEnum.BUS_ARRIVAL ? (item.ArrivalDateTime.Value == null ? null : item.ArrivalDateTime.Value.ToShortDateString()) : (item.DepartureDateTime.Value == null ? null : item.DepartureDateTime.Value.ToShortDateString()),
                    }).ToList();

                    response.TravelGroupInfoList = travelGroupInfoList;
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}