﻿using Airbnb;
using Airbnb.Models;
using Core;
using Core.Database.Context;
using Core.Enumerations;
using Core.Helper;
using Core.Repository;
using Core.SignalR.Hubs;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MessagerSolution.Controllers
{
    public class ImportController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        Logger logger = LogManager.GetCurrentClassLogger();

        private static Airbnb.ScrapeManager _AirbnbScrapeManager = null;

        private static VRBO.ScrapeManager _VrboScrapeManager = null;

        public static Airbnb.ScrapeManager AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb.ScrapeManager();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }

        public static VRBO.ScrapeManager VrboScrapper
        {
            get
            {
                if (_VrboScrapeManager == null)
                    _VrboScrapeManager = new VRBO.ScrapeManager();

                return _VrboScrapeManager;
            }
            set
            {
                _VrboScrapeManager = value;
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                FormsIdentity fid = User.Identity as FormsIdentity;
                string plainTextUserData = fid.Ticket.UserData;
                ViewBag.UserRole = plainTextUserData.ToLower();
                ViewBag.UserId = User.Identity.Name;
                SiteConstants.CompanyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    ViewBag.OwnerList = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select h).ToList();// db.Hosts.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                }
                return View();
            }
            return Redirect("/");
        }

        [HttpGet]
        public ActionResult CaptchaLock(string lockId, string siteKey)
        {
            CaptchaViewModel model = new CaptchaViewModel();
            model.LockId = lockId;
            model.SiteKey = siteKey;
            return View(model);
        }

        [HttpPost]
        public ActionResult CaptchaLock(CaptchaViewModel model)
        {
            model.CaptchaResponse = Request.Params["g-recaptcha-response"];

            var result = AirbnbScrapper.ProcessCaptchaLock(model);

            return RedirectToAction("Login");

        }

        [HttpGet]
        public ActionResult GetAccountDetails(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                var host = db.Hosts.Where(x => x.Id == Id).FirstOrDefault();
                return Json(new { host = host }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveGmailAccount(string email, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                var hosts = db.Hosts.Where(x => x.Username == email).ToList();
                foreach (var host in hosts)
                {
                    host.GmailPassword = Core.Helper.Security.Encrypt(password.Trim());
                    db.Entry(host).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetAccountList()
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {


                        List<ImportTableData> td = new List<ImportTableData>();
                        var accounts = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select h).OrderBy(x => x.Firstname).ToList();
                        accounts.ForEach(x => {
                            string actions = "<button class=\"ui mini button js-btn_redownload\" data-tooltip=\"Re-download\">" +
                                        "<i class=\"icon download\"></i></button>"+
                                        "<button class=\"ui mini button js-btn_reconnect\" data-tooltip=\"Re-connect\">" +
                                        "<i class=\"icon undo\"></i></button>" +
                                        (x.Username.Contains("@gmail.com") ?
                                        "<button class=\"ui mini button js-btn_add_gmail\" data-tooltip=\"Gmail Account\">" +
                                        "<i class=\"icon google\"></i>" + (x.GmailPassword != null && x.GmailPassword != "" ? "<i class=\"green small check circle outline icon\"></i>" : "") + "</button>" : "") +
                                        "<button class=\"ui mini button js-btn_delete-account\" data-tooltip=\"Delete\">" +
                                        "<i class=\"icon delete\"></i></button>";

                            ImportTableData d = new ImportTableData();
                            d.Id = x.Id;
                            d.Username = x.Username;
                            d.Site = ((SiteType)x.SiteType).ToString();
                            d.SiteType = x.SiteType;
                            d.Name = "<h4 class=\"ui image header\">" +
                                     "<img src=\"" + (string.IsNullOrEmpty(x.ProfilePictureUrl) ? Core.Helper.Utilities.GetDefaultAvatar(x.Firstname) : x.ProfilePictureUrl) + "\" class=\"ui mini avatar image\">" +
                                     "<div class=\"content\">" + (x.Firstname + " " + x.Lastname) +
                                     "</div>" +
                                     "</h4>";
                            d.AutoSync = "<div class=\"ui toggle checkbox autosync\">" +
                                         "<input type=\"checkbox\" name=\"autosync\"" + (x.AutoSync ? "checked" : "") + ">" +
                                         "<label></label>" +
                                         "</div>";
                            d.Actions = actions;
                            td.Add(d);
                        });
                        return Json(new { data = td.OrderBy(x => x.Name).ToList() }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> PostImport(string username, string password, int siteType)
        {


            JObject message = null;
            //It trgigger the Login in API Project
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("username", username);
                reqParams.Add("password", password);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("companyId", GlobalVariables.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Import/PostImport", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    //loginResult = message["result"].ToBoolean();
                    return Json(new { success = message["success"].ToBoolean() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);

        }
        #region ImportLogIn

        // I.G. > When the user enters username and password in import, or the user clicked the re-download button, 
        // this is the method to be called after that.
        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ImportLogIn(string username, string password, int siteType)
        {
            GlobalVariables.ClearImportCounters();
            SiteType type = (SiteType)siteType;
            JObject message = null;

            bool isRelogin = false;
            ReLogin:
            //It trgigger the Login in API Project
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("username", username);
                reqParams.Add("password", password);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Import/ImportLogin", reqContent);
                if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    if (isRelogin == false)
                    {
                        isRelogin = true;
                        goto ReLogin;
                    }
                }

                if (result != null && result.Content != null)
                {

                    //It parse the response of API call
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    if (type == SiteType.Airbnb)
                    {

                        if (message["airlock"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            Airbnb.Models.AirlockViewModel airlock = viewModel.ToObject<Airbnb.Models.AirlockViewModel>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock });
                        }
                        else
                        {
                            return Json(new { success = message["success"], isAirlock = message["isAirlock"] });
                        }

                    }
                    else if (type == SiteType.VRBO)
                    {
                        if (message["airlock"] != null && message["phoneNumbers"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = message["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, countryCodes = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean() });
                        }
                    }
                    else if (type == SiteType.BookingCom)
                    {
                        if (message["airlock"] != null && message["phoneNumber"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            Booking.com.Models.AirlockViewModel airlock = viewModel.ToObject<Booking.com.Models.AirlockViewModel>();
                            var phones = message["phoneNumber"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();

                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, phoneNumberKeys = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean() });
                        }
                    }
                    else if (type == SiteType.Homeaway)
                    {
                        if (message["airlock"] != null && message["phoneNumbers"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = message["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, countryCodes = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean() });
                        }
                    }
                }
            }
            return Json(new { success = false, isAirlock = false }, JsonRequestBehavior.AllowGet);


        }


        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> RedownloadData(string id, int siteType)
        {
            bool success = false;
            string token = "";
            GlobalVariables.ClearImportCounters();
            SiteType type = (SiteType)siteType;

            bool isRelogin = false;
        ReLogin:
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject message = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("id", id);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);
                HttpResponseMessage result = new HttpResponseMessage();

                try
                {
                    result = await client.PostAsync("Import/RedownloadData", reqContent);
                }
                catch (Exception e)
                {

                }
                if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    if (isRelogin == false)
                    {
                        isRelogin = true;
                        goto ReLogin;
                    }
                }
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (type == SiteType.Airbnb)
                    {
                        if (message["airlock"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            Airbnb.Models.AirlockViewModel airlock = viewModel.ToObject<Airbnb.Models.AirlockViewModel>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, hostProfilePic = message["hostProfilePic"].ToString(), hostName = message["hostName"].ToString() }, JsonRequestBehavior.AllowGet);
                        }
                        else if (message["captcha"] != null)
                        {
                            var viewModel = message["captcha"] as JObject;
                            Airbnb.Models.CaptchaViewModel captcha = viewModel.ToObject<Airbnb.Models.CaptchaViewModel>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), isCaptcha = message["isCaptcha"].ToBoolean(), captcha = captcha }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            success = message["success"].ToBoolean();
                        }
                    }
                    else if (type == SiteType.VRBO)
                    {
                        if (message["airlock"] != null && message["phoneNumbers"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = message["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, countryCodes = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            success = message["success"].ToBoolean();
                        }
                    }
                    else if (type == SiteType.BookingCom)
                    {
                        if (message["airlock"] != null && message["phoneNumber"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            Booking.com.Models.AirlockViewModel airlock = viewModel.ToObject<Booking.com.Models.AirlockViewModel>();
                            var phones = message["phoneNumber"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();

                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, phoneNumberKeys = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean() });
                        }
                    }
                    else if (type == SiteType.Homeaway)
                    {
                        if (message["airlock"] != null && message["phoneNumbers"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = message["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, countryCodes = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            success = message["success"].ToBoolean();
                        }
                    }

                    return Json(new { success = success, isAirlock = false }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = success, isAirlock = false }, JsonRequestBehavior.AllowGet);

            #region Commented Code

            //if (User.Identity.IsAuthenticated)
            //{
            //    try
            //    {
            //        if (type == SiteType.Airbnb)
            //        {
            //            #region Airbnb

            //            var user = Core.API.Airbnb.Hosts.GetHostUsernameAndPassword(id.ToInt());
            //            token = Core.API.Airbnb.Hosts.GetToken(id.ToInt());
            //            var scrapeHostSuccess = AirbnbScrapper.ScrapeGuestAccount(token, user.Item1, user.Item2, true);

            //            if (scrapeHostSuccess)
            //            {
            //                Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                try
            //                {
            //                    JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                }
            //                catch (NullReferenceException err) { }

            //                GlobalVariables.StillImporting = true;
            //                JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                success = true;
            //            }
            //            else
            //            {
            //                var result = AirbnbScrapper.Login(user.Item1, user.Item2, true);

            //                if (result.Success)
            //                {
            //                    Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                    try
            //                    {
            //                        JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                    }
            //                    catch (NullReferenceException err) { }

            //                    GlobalVariables.StillImporting = true;
            //                    JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                    success = true;
            //                }
            //                else if (!result.Success && result.AirLock != null)
            //                {
            //                    result.AirLock.Username = user.Item1;
            //                    result.AirLock.Password = user.Item2;

            //                    return Json(new
            //                    {
            //                        success = success,
            //                        isAirlock = true,
            //                        airlock = result.AirLock
            //                    }, JsonRequestBehavior.AllowGet);
            //                }
            //            }

            //            #endregion
            //        }
            //        else if (type == SiteType.VRBO)
            //        {
            //            #region Vrbo

            //            var user = Core.API.Vrbo.Hosts.GetHostUsernameAndPassword(id.ToInt());
            //            token = Core.API.Vrbo.Hosts.GetToken(id.ToInt());
            //            var scrapeHostSuccess = VrboScrapper.ScrapeHostProfile(user.Item2, true, token);

            //            if (scrapeHostSuccess)
            //            {
            //                Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                try
            //                {
            //                    JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                }
            //                catch (NullReferenceException err) { }

            //                GlobalVariables.StillImporting = true;
            //                JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                success = true;
            //            }
            //            else
            //            {
            //                var result = VrboScrapper.Login(user.Item1, user.Item2, true);

            //                if (result.Success)
            //                {
            //                    Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                    try
            //                    {
            //                        JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                    }
            //                    catch (NullReferenceException err) { }

            //                    GlobalVariables.StillImporting = true;
            //                    JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                    success = true;
            //                }
            //                else if (!result.Success && result.AirLock != null)
            //                {
            //                    result.AirLock.Username = user.Item1;
            //                    result.AirLock.Password = user.Item2;

            //                    return Json(new
            //                    {
            //                        success = success,
            //                        isAirlock = true,
            //                        airlock = result.AirLock,
            //                        phoneNumbers = result.AirLock.PhoneNumbers.Values
            //                    }, JsonRequestBehavior.AllowGet);
            //                }
            //            }

            //            #endregion
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        logger.ErrorException("Error occured in scrape manager ImportLogin", e);
            //    }
            //}

            #endregion
        }


        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> Reconnect(string id, int siteType)
        {
            bool success = false;
            string token = "";
            GlobalVariables.ClearImportCounters();
            SiteType type = (SiteType)siteType;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType));
                JObject message = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", User.Identity.Name);
                reqParams.Add("id", id);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);
                HttpResponseMessage result = new HttpResponseMessage();

                try
                {
                    result = await client.PostAsync("Import/Reconnect", reqContent);
                }
                catch (Exception e)
                {

                }

                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (type == SiteType.Airbnb)
                    {
                        if (message["airlock"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            Airbnb.Models.AirlockViewModel airlock = viewModel.ToObject<Airbnb.Models.AirlockViewModel>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, hostProfilePic = message["hostProfilePic"].ToString(), hostName = message["hostName"].ToString() }, JsonRequestBehavior.AllowGet);
                        }
                        else if (message["captcha"] != null)
                        {
                            var viewModel = message["captcha"] as JObject;
                            Airbnb.Models.CaptchaViewModel captcha = viewModel.ToObject<Airbnb.Models.CaptchaViewModel>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), isCaptcha = message["isCaptcha"].ToBoolean(), captcha = captcha }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            success = message["success"].ToBoolean();
                        }
                    }
                    else if (type == SiteType.VRBO)
                    {
                        if (message["airlock"] != null && message["phoneNumbers"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = message["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            success = message["success"].ToBoolean();
                        }
                    }
                    else if (type == SiteType.BookingCom)
                    {
                        if (message["airlock"] != null && message["phoneNumber"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            Booking.com.Models.AirlockViewModel airlock = viewModel.ToObject<Booking.com.Models.AirlockViewModel>();
                            var phones = message["phoneNumber"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();

                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values, phoneNumberKeys = phoneNumbers.Keys }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean() });
                        }
                    }
                    else if (type == SiteType.Homeaway)
                    {
                        if (message["airlock"] != null && message["phoneNumbers"] != null)
                        {
                            var viewModel = message["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = message["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), airlock = airlock, phoneNumbers = phoneNumbers.Values }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            success = message["success"].ToBoolean();
                        }
                    }

                    return Json(new { success = success, isAirlock = false }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { success = success, isAirlock = false }, JsonRequestBehavior.AllowGet);

            #region Commented Code

            //if (User.Identity.IsAuthenticated)
            //{
            //    try
            //    {
            //        if (type == SiteType.Airbnb)
            //        {
            //            #region Airbnb

            //            var user = Core.API.Airbnb.Hosts.GetHostUsernameAndPassword(id.ToInt());
            //            token = Core.API.Airbnb.Hosts.GetToken(id.ToInt());
            //            var scrapeHostSuccess = AirbnbScrapper.ScrapeGuestAccount(token, user.Item1, user.Item2, true);

            //            if (scrapeHostSuccess)
            //            {
            //                Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                try
            //                {
            //                    JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                }
            //                catch (NullReferenceException err) { }

            //                GlobalVariables.StillImporting = true;
            //                JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                success = true;
            //            }
            //            else
            //            {
            //                var result = AirbnbScrapper.Login(user.Item1, user.Item2, true);

            //                if (result.Success)
            //                {
            //                    Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                    try
            //                    {
            //                        JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                    }
            //                    catch (NullReferenceException err) { }

            //                    GlobalVariables.StillImporting = true;
            //                    JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                    success = true;
            //                }
            //                else if (!result.Success && result.AirLock != null)
            //                {
            //                    result.AirLock.Username = user.Item1;
            //                    result.AirLock.Password = user.Item2;

            //                    return Json(new
            //                    {
            //                        success = success,
            //                        isAirlock = true,
            //                        airlock = result.AirLock
            //                    }, JsonRequestBehavior.AllowGet);
            //                }
            //            }

            //            #endregion
            //        }
            //        else if (type == SiteType.VRBO)
            //        {
            //            #region Vrbo

            //            var user = Core.API.Vrbo.Hosts.GetHostUsernameAndPassword(id.ToInt());
            //            token = Core.API.Vrbo.Hosts.GetToken(id.ToInt());
            //            var scrapeHostSuccess = VrboScrapper.ScrapeHostProfile(user.Item2, true, token);

            //            if (scrapeHostSuccess)
            //            {
            //                Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                try
            //                {
            //                    JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                }
            //                catch (NullReferenceException err) { }

            //                GlobalVariables.StillImporting = true;
            //                JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                success = true;
            //            }
            //            else
            //            {
            //                var result = VrboScrapper.Login(user.Item1, user.Item2, true);

            //                if (result.Success)
            //                {
            //                    Job.ScrapeJob.TryStop(User.Identity.Name, id.ToInt());

            //                    try
            //                    {
            //                        JobScheduler.ImportJobScheduler.Stop(User.Identity.Name + "import");
            //                    }
            //                    catch (NullReferenceException err) { }

            //                    GlobalVariables.StillImporting = true;
            //                    JobScheduler.ImportJobScheduler.Start(User.Identity.Name + "import", user.Item1, user.Item2, siteType);
            //                    success = true;
            //                }
            //                else if (!result.Success && result.AirLock != null)
            //                {
            //                    result.AirLock.Username = user.Item1;
            //                    result.AirLock.Password = user.Item2;

            //                    return Json(new
            //                    {
            //                        success = success,
            //                        isAirlock = true,
            //                        airlock = result.AirLock,
            //                        phoneNumbers = result.AirLock.PhoneNumbers.Values
            //                    }, JsonRequestBehavior.AllowGet);
            //                }
            //            }

            //            #endregion
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        logger.ErrorException("Error occured in scrape manager ImportLogin", e);
            //    }
            //}

            #endregion
        }

        #endregion

        #region ProcessAirlockSelection

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessAirbnbAirlockSelection(string lockId, string userId, string phoneId, int choice, bool isNew,string username)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Airbnb));
                JObject message = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("lockId", lockId);
                reqParams.Add("userId", userId);
                reqParams.Add("phoneId", phoneId);
                reqParams.Add("choice", choice.ToString());
                reqParams.Add("isNew", isNew.ToString());
                reqParams.Add("username", username);

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Import/ProcessAirbnbAirlockSelection", reqContent);

                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return Json(new { sendCodeAllowed = message["sendCodeAllowed"].ToBoolean() }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { sendCodeAllowed = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessVrboAirlockSelection(VRBO.Models.AirlockViewModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.VRBO));
                var content = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var reqContent = new ByteArrayContent(buffer);
                reqContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                JObject message = null;


                var result = await client.PostAsync("Import/ProcessVrboAirlockSelection", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    var viewModel = message["airlock"] as JObject;
                    VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                    return Json(new { airlock = airlock }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { airlock = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessBookingAirlockSelection(Booking.com.Models.AirlockViewModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.BookingCom));
                var content = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var reqContent = new ByteArrayContent(buffer);
                reqContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                JObject message = null;


                var result = await client.PostAsync("Import/ProcessBookingAirlockSelection", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    var viewModel = message["airlock"] as JObject;
                    Booking.com.Models.AirlockViewModel airlock = viewModel.ToObject<Booking.com.Models.AirlockViewModel>();
                    return Json(new { airlock = airlock }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { airlock = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessHomeawayAirlockSelection(VRBO.Models.AirlockViewModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Homeaway));
                var content = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var reqContent = new ByteArrayContent(buffer);
                reqContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                JObject message = null;


                var result = await client.PostAsync("Import/ProcessHomeawayAirlockSelection", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    var viewModel = message["airlock"] as JObject;
                    VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                    return Json(new { airlock = airlock }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { airlock = model }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ProcessAirlock

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessAirbnbAirlock(string code, string lockId, string userId, string phoneId, int choice, bool isNew, string username, string password)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Airbnb));
                JObject message = null;
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("code", code);
                reqParams.Add("lockId", lockId);
                reqParams.Add("userId", userId);
                reqParams.Add("phoneId", phoneId);
                reqParams.Add("choice", choice.ToString());
                reqParams.Add("isNew", isNew.ToString());
                reqParams.Add("username", username);
                reqParams.Add("password", password);
                reqParams.Add("appUserId", User.Identity.Name);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Import/ProcessAirbnbAirlock", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    GlobalVariables.AirlockIsTriggered = false;
                    return Json(new { success = message["success"].ToBoolean(), username = username, password = password }, JsonRequestBehavior.AllowGet);
                }
            }

            //var result = AirbnbScrapper.ProcessAirlock(code, lockId, userId, phoneId, choice, isNew, username, password);
            return Json(new { success = false, token = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessVrboAirlock(VRBO.Models.AirlockViewModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.VRBO));
                var content = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var reqContent = new ByteArrayContent(buffer);
                reqContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                JObject message = null;


                var result = await client.PostAsync("Import/ProcessVrboAirlock", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    //var viewModel = message["airlock"] as JObject;
                    //VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                    GlobalVariables.AirlockIsTriggered = false;
                    return Json(new { success = message["success"].ToBoolean(), username = model.Username, password = model.Password }, JsonRequestBehavior.AllowGet);

                }
            }
            //var result = VrboScrapper.ProcessAirlock(model);
            return Json(new { success = false, token = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessBookingAirlock(Booking.com.Models.AirlockViewModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.BookingCom));
                var content = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var reqContent = new ByteArrayContent(buffer);
                reqContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                JObject message = null;


                var result = await client.PostAsync("Import/ProcessBookingAirlock", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    //var viewModel = message["airlock"] as JObject;
                    //VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                    GlobalVariables.AirlockIsTriggered = false;
                    return Json(new { success = message["success"].ToBoolean(), username = model.Email, password = model.Password }, JsonRequestBehavior.AllowGet);

                }
            }
            //var result = VrboScrapper.ProcessAirlock(model);
            return Json(new { success = false, token = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public async Task<ActionResult> ProcessHomeawayAirlock(VRBO.Models.AirlockViewModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl((int)Core.Enumerations.SiteType.Homeaway));
                var content = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var reqContent = new ByteArrayContent(buffer);
                reqContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                JObject message = null;


                var result = await client.PostAsync("Import/ProcessHomeawayAirlock", reqContent);
                if (result != null && result.Content != null)
                {
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    //var viewModel = message["airlock"] as JObject;
                    //VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                    GlobalVariables.AirlockIsTriggered = false;
                    return Json(new { success = message["success"].ToBoolean(), username = model.Username, password = model.Password }, JsonRequestBehavior.AllowGet);

                }
            }
            //var result = VrboScrapper.ProcessAirlock(model);
            return Json(new { success = false, token = "" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult ChangeAutoSyncStatus(string id, bool toggle)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.Hosts.Find(id.ToInt());
                    temp.AutoSync = toggle;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Commented Code

        //public void GetAirbnbHostAccountDetails()
        //{
        //    try
        //    {
        //        Trace.WriteLine("Get airbnb host account started..");
        //        SiteConstants.accounts.Clear();
        //        using (var db = new ApplicationDbContext())
        //        {
        //            db.Hosts.Where(x => x.CompanyId == SiteConstants.CompanyId).ToList().ForEach(item =>
        //            {
        //                AirbnbAccount a = new AirbnbAccount();
        //                a.Id = item.Id;
        //                a.Username = item.Username;
        //                a.Password = Security.Decrypt(item.Password);

        //                var entity = db.UserSessions.Where(x => x.OwnerId == item.Id).Where(x => x.IsActive == true).FirstOrDefault();
        //                if (entity != null)
        //                {
        //                    using (ScrapeManager sm = new ScrapeManager())
        //                    {
        //                        var isloaded = sm.ValidateTokenAndLoadCookies(entity.SessionId);
        //                        if (isloaded)
        //                        {
        //                            a.SessionToken = entity.SessionId;
        //                            SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                        }
        //                        else
        //                        {
        //                            db.UserSessions.RemoveRange(db.UserSessions.Where(x => x.OwnerId == entity.OwnerId).ToList());
        //                            db.SaveChanges();
        //                            var result = sm.Login(a.Username, a.Password, true);
        //                            a.SessionToken = result.SessionToken;
        //                            SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    using (ScrapeManager sm = new ScrapeManager())
        //                    {
        //                        var result = sm.Login(a.Username, a.Password, true);
        //                        a.SessionToken = result.SessionToken;
        //                        SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                    }
        //                }
        //            });
        //        }
        //        Trace.WriteLine("Get airbnb host account done..");
        //    }
        //    catch (Exception e)
        //    {
        //        Trace.WriteLine(e.ToString());
        //    }
        //}

        //Update signalR Toastr every details that download from Host

        #endregion

        #region ImportProgress
        public ActionResult UpdateProgress(string userId, string importedHost, int propertyCount,
            int messageCount, int bookingCount, string hostId)
        {
            GlobalVariables.ImportedHostProfile = importedHost;
            GlobalVariables.ImportedPropertyCount = propertyCount;
            GlobalVariables.ImportedMessagesCount = messageCount;
            GlobalVariables.ImportedBookingCount = bookingCount;
            ImportHub.UpdateProgress(userId, hostId);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowHostName(string userId, string importedHost, string hostId)
        {
            ImportHub.ShowHostName(userId, importedHost, hostId);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DoneImporting(string userId)
        {
            ImportHub.DoneImporting(userId);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ShowBlockUser
        public async Task<bool> ShowBlockUser(string userId, string BlockDetails)
        {
            GlobalAirlockHub.ShowBlockUser    (userId, BlockDetails);
            return true;
        }
        #endregion
        #region SuccessLoginRemoveToastr
        public async Task<bool> SuccessLoginRemoveToastr(string userId, string hostId)
        {
            GlobalAirlockHub.SuccessLoginRemoveToastr(userId, hostId.ToInt());
            return true;
        }
        #endregion

        #region AirlockEnableToastr
        public async Task<bool> AirlockEnableToastr(string hostId)
        {
            GlobalAirlockHub.AirlockEnableToastr(User.Identity.Name, hostId);
            return true;
        }
        #endregion

        #region ShowLogin
        public async Task<bool> ShowLogin(string userId, string loginModel, string hostId, bool IsUserShow = false)
        {
            if (IsUserShow)
            {
                GlobalAirlockHub.ShowUserProcessAirlock(User.Identity.Name, hostId.ToInt());
                GlobalAirlockHub.ShowLogin(User.Identity.Name, loginModel, IsUserShow);
            }
            else
            {
                GlobalAirlockHub.ShowLogin(User.Identity.Name, loginModel, IsUserShow);
            }
            return true;
        }
        #endregion

        #region NotDoneModalClose

        #endregion
        #region Showirlock
        public async Task<bool> ShowAirlock(string userId, string airlockModel, string hostId, bool IsUserShow = false)
        {
            if (IsUserShow)
            {
                GlobalAirlockHub.ShowUserProcessAirlock(User.Identity.Name, hostId.ToInt());
                GlobalAirlockHub.ShowAirlock(User.Identity.Name, airlockModel, IsUserShow);
            }
            else
            {
                GlobalAirlockHub.ShowAirlock(User.Identity.Name, airlockModel, IsUserShow);
            }

            return true;
        }
        #endregion

        #region AutoAirlock
        public async Task<bool> AutoAirlock(string userId, string hostId, string siteType)
        {
            using (HttpClient client = new HttpClient())
            {
                bool isRelogin = false;
                ReLogin:
                SiteType type = (SiteType)(siteType.ToInt());
                HttpResponseMessage result = new HttpResponseMessage();
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(siteType.ToInt()));
                JObject data = null;
                var reqParams = new Dictionary<string, string>();
                var companyId = Core.API.AllSite.Users.GetCompanyId(userId.ToInt());
                reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("hostId", hostId);
                reqParams.Add("siteType", siteType.ToString());
                reqParams.Add("companyId", companyId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                try
                {
                    result = await client.PostAsync("Import/AutoAirlock", reqContent);
                }
                catch (Exception e)
                {

                }
                if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    if (isRelogin == false)
                    {
                        isRelogin = true;
                        goto ReLogin;
                    }
                }
                if (result != null && result.Content != null)
                {
                  
                    // AFTER GETTING LOG IN RESULTS SUCH AS AIRLOCK, DO ACTION HERE...
                    data = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                    if (type == SiteType.Airbnb)
                    {
                        if (data["airlock"] != null)
                        {
                            var viewModel = data["airlock"] as JObject;
                            Airbnb.Models.AirlockViewModel airlock = viewModel.ToObject<Airbnb.Models.AirlockViewModel>();
                            dynamic json = new JObject();
                            json.success = data["success"].ToBoolean();
                            json.isAirlock = data["isAirlock"].ToBoolean();
                            json.airlock = JsonConvert.SerializeObject(airlock);
                            json.hostProfilePic = data["hostProfilePic"].ToString();
                            json.hostName = data["hostName"].ToString();
                            json.siteType = "Airbnb";
                            json.hostId = hostId;
                            json.userId = userId;
                            GlobalAirlockHub.ShowAirlock(userId, JsonConvert.SerializeObject(json));
                        }
                        else if (data["captcha"] != null)
                        {
                            var viewModel = data["captcha"] as JObject;
                            Airbnb.Models.CaptchaViewModel captcha = viewModel.ToObject<Airbnb.Models.CaptchaViewModel>();
                            //return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean(), isCaptcha = message["isCaptcha"].ToBoolean(), captcha = captcha }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            GlobalVariables.AirlockIsTriggered = false;
                            //success = message["success"].ToBoolean();
                        }
                    }
                    else if (type == SiteType.VRBO)
                    {
                        if (data["airlock"] != null && data["phoneNumbers"] != null)
                        {
                            var viewModel = data["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = data["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            dynamic json = new JObject();

                            json.success = data["success"].ToBoolean();
                            json.isAirlock = data["isAirlock"].ToBoolean();
                            json.airlock = JsonConvert.SerializeObject(airlock);
                            json.phoneNumbers = JsonConvert.SerializeObject(phoneNumbers);
                            json.hostProfilePic = data["hostProfilePic"].ToString();
                            json.hostName = data["hostName"].ToString();
                            json.siteType = "Vrbo";
                            json.hostId = hostId;
                            json.userId = userId;

                            GlobalAirlockHub.ShowAirlock(userId, JsonConvert.SerializeObject(json));
                        }
                        else
                        {
                            GlobalVariables.AirlockIsTriggered = false;
                            //success = message["success"].ToBoolean();
                        }
                    }
                    else if (type == SiteType.BookingCom)
                    {
                        if (data["airlock"] != null && data["phoneNumber"] != null)
                        {
                            var viewModel = data["airlock"] as JObject;
                            Booking.com.Models.AirlockViewModel airlock = viewModel.ToObject<Booking.com.Models.AirlockViewModel>();
                            var phones = data["phoneNumber"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();

                            dynamic json = new JObject();
                            json.success = data["success"].ToBoolean();
                            json.isAirlock = data["isAirlock"].ToBoolean();
                            json.airlock = JsonConvert.SerializeObject(airlock);
                            json.phoneNumbers = JsonConvert.SerializeObject(phoneNumbers);
                            //json.phoneNumberKeys = JsonConvert.SerializeObject(phoneNumbers.Keys);
                            json.hostProfilePic = data["hostProfilePic"].ToString();
                            json.hostName = data["hostName"].ToString();
                            json.siteType = "Booking";
                            json.hostId = hostId;
                            json.userId = userId;

                            GlobalAirlockHub.ShowAirlock(userId, JsonConvert.SerializeObject(json));
                        }
                        else
                        {
                            GlobalVariables.AirlockIsTriggered = false;
                            //return Json(new { success = message["success"].ToBoolean(), isAirlock = message["isAirlock"].ToBoolean() });
                        }
                    }
                    else if (type == SiteType.Homeaway)
                    {
                        if (data["airlock"] != null && data["phoneNumbers"] != null)
                        {
                            var viewModel = data["airlock"] as JObject;
                            VRBO.Models.AirlockViewModel airlock = viewModel.ToObject<VRBO.Models.AirlockViewModel>();
                            var phones = data["phoneNumbers"] as JObject;
                            Dictionary<string, string> phoneNumbers = phones.ToObject<Dictionary<string, string>>();
                            dynamic json = new JObject();
                            json.success = data["success"].ToBoolean();
                            json.isAirlock = data["isAirlock"].ToBoolean();
                            json.airlock = JsonConvert.SerializeObject(airlock);
                            json.phoneNumbers = JsonConvert.SerializeObject(phoneNumbers);
                            json.hostProfilePic = data["hostProfilePic"].ToString();
                            json.hostName = data["hostName"].ToString();
                            json.siteType = "Homeaway";
                            json.hostId = hostId;
                            json.userId = userId;
                            GlobalAirlockHub.ShowAirlock(userId, JsonConvert.SerializeObject(json));
                        }
                        else
                        {
                            GlobalVariables.AirlockIsTriggered = false;
                        }
                    }

                    //return Json(new { success = success, isAirlock = false }, JsonRequestBehavior.AllowGet);
                }
            }

            GlobalVariables.AirlockIsTriggered = false;
            return true;
        }
        #endregion
    }
}
