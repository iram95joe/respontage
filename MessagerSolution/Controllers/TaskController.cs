﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using Core.Models.Task;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class TaskController : Controller
    {
        ICommonRepository objIList = new CommonRepository();

        private readonly List<int> travelEventsDepartureTypes = new List<int>
        {
            (int) NoteType.FLIGHT_DEPARTURE,
            (int) NoteType.BUS_DEPARTURE,
            (int) NoteType.CRUISE_DEPARTURE,
            (int) NoteType.TRAIN_DEPARTURE,
            (int) NoteType.DRIVE_DEPARTURE
        };

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult TaskSummary(int companyId)
        {
            if (User.Identity.IsAuthenticated)
            {

                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Tasks") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.PropertyList = objIList.GetProperties();
                ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();

                List<TasksViewModel> model = new List<TasksViewModel>();

                //Get Full Set of Inquiry Task
                using (var db = new ApplicationDbContext())
                {
                    //JM 10/8/18 Start new Code
                    List<int> propertyParentIds = new List<int>();
                    var Properties = db.Properties.Where(x => x.IsParentProperty == false).ToList();

                    foreach (var property in Properties)
                    {
                        List<Inquiry> Inquiries = new List<Inquiry>();
                        List<long> propertyChildId = new List<long>();
                        //JM 10/30/19 Check if property has sync parent
                        var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (parentChild != null)
                        {//Get all child propertyId
                            if (!propertyParentIds.Contains(parentChild.ParentPropertyId))
                            {
                                propertyChildId = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parentChild.ParentPropertyId select p.ListingId).ToList();
                                //db.Properties.Where(x => x.ParentPropertyId == property.ParentPropertyId).Select(x => x.ListingId).ToList();
                                propertyParentIds.Add(parentChild.ParentPropertyId);
                            }

                            foreach (var p in propertyChildId)
                            {
                                Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == p && x.BookingStatusCode == "A" && x.CheckInDate > DateTime.Now));
                            }

                        }
                        else
                        {
                            Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.BookingStatusCode == "A" && x.CheckInDate > DateTime.Now));
                        }

                  
                        Inquiries = Inquiries.OrderBy(x => x.CheckInDate).ToList();
                        int count = Inquiries.Count;
                        for (int x = 0; x < count; x++)
                        {
                            Inquiry previousReservation = new Inquiry();
                            if (x > 0)
                            {
                                previousReservation = Inquiries[x - 1];
                            }

                            Inquiry futureReservation = new Inquiry();
                            if (x < count-1)
                            {
                                futureReservation = Inquiries[x + 1];
                            }

                            var currentReservation = Inquiries[x];

                            var propertyInfo = db.Properties.Where(y => y.ListingId == currentReservation.PropertyId).FirstOrDefault();
                            var guestInfo = db.Guests.FirstOrDefault(y => y.Id == currentReservation.GuestId);
                            if (guestInfo == null)
                            {

                            }
                            if (propertyInfo.TimeZoneName != null)
                            {
                                currentReservation.CheckInDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                                currentReservation.CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                            }

                            var localId = Core.API.AllSite.Properties.GetLocalId(currentReservation.PropertyId);
                            var futurelocalId = Core.API.AllSite.Properties.GetLocalId(futureReservation.PropertyId);
                            var prevlocalId = Core.API.AllSite.Properties.GetLocalId(previousReservation.PropertyId);

                            List<Core.Models.Task.Task> inquiryTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> turnoverTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> bookingTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> checkInTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> checkOutTasks = new List<Core.Models.Task.Task>();

                            var inquiryTasksData = (from task in db.Tasks
                                                    join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                    where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.INQUIRY
                                                    select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
        
                            foreach (var task in inquiryTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl,UserId =ist.UserId,LastUpdateDate =ist.LastUpdateDate}).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl,LastUpdateDate =item.LastUpdateDate,UpdateBy = UpdateBy};
                                    tempTask.SubTasks.Add(sub);
                                }
                                inquiryTasks.Add(tempTask);
                            }
                            var turnoverTasksData = (from task in db.Tasks
                                                     join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                     where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.TURNOVER
                                                     select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.TURNOVER).ToList();

                            foreach (var task in turnoverTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                turnoverTasks.Add(tempTask);
                            }
                            var bookingTasksData = (from task in db.Tasks
                                                    join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                    where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.BOOKING
                                                    select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.BOOKING).ToList();

                            foreach (var task in bookingTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                bookingTasks.Add(tempTask);
                            }
                            var checkInTasksData = (from task in db.Tasks
                                                    join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                    where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.CHECK_IN
                                                    select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.CHECK_IN).ToList();
                            foreach (var task in checkInTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName: null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                checkInTasks.Add(tempTask);
                            }
                            var checkOutTasksData = (from task in db.Tasks
                                                     join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                     where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.CHECK_OUT
                                                     select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.CHECK_OUT).ToList();
                            foreach (var task in checkOutTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                checkOutTasks.Add(tempTask);
                            }

                            var workersAssigned = db.WorkerAssignments.Where(y => y.ReservationId == currentReservation.Id && y.PropertyId == localId).OrderBy(y => y.StartDate).FirstOrDefault();

                            AssignedWorkerInfo assignedWorker = null;

                            if (workersAssigned != null)
                            {
                                var workerInfo = db.Workers.FirstOrDefault(y => y.Id == workersAssigned.WorkerId);
                                if (workerInfo != null)
                                {
                                    assignedWorker = new AssignedWorkerInfo();
                                    assignedWorker.FirstName = workerInfo.Firstname;
                                    assignedWorker.LastName = workerInfo.Lastname;
                                    assignedWorker.StartDate = workersAssigned.StartDate;
                                    assignedWorker.EndDate = workersAssigned.EndDate;
                                    assignedWorker.Mobile = workerInfo.Mobile;
                                }
                            }

                        var preCheckInTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                            join workerJobType in db.WorkerJobTypes
                                                            on workerAssignments.JobTypeId equals workerJobType.Id
                                                            where 
                                                                 (workerAssignments.ReservationId ==previousReservation.Id&&
                                                                  workerAssignments.PropertyId == prevlocalId &&
                                                                  workerAssignments.StartDate >= previousReservation.CheckOutDate &&
                                                                  workerJobType.ClassificationId == 1) 
                                                                  ||
                                                                  (workerAssignments.ReservationId == currentReservation.Id &&
                                                                  workerAssignments.PropertyId == localId &&
                                                                  workerAssignments.StartDate <= currentReservation.CheckInDate &&
                                                                  workerJobType.ClassificationId == 1) //Turnover Classification
                                                            select workerAssignments).ToList();

                        var checkInWorkerList = (from workerAssignments in db.WorkerAssignments
                                                 join workerJobType in db.WorkerJobTypes
                                                 on workerAssignments.JobTypeId equals workerJobType.Id
                                                 where 
                                                       workerAssignments.PropertyId == localId &&
                                                       DbFunctions.TruncateTime(workerAssignments.StartDate) >= DbFunctions.TruncateTime(currentReservation.CheckInDate) &&
                                                       workerAssignments.EndDate <= currentReservation.CheckOutDate &&
                                                       workerJobType.ClassificationId == 3 //Turnover Classification
                                                 select workerAssignments).ToList();

                        var checkOutWorkerList = (from workerAssignments in db.WorkerAssignments
                                                  join workerJobType in db.WorkerJobTypes
                                                  on workerAssignments.JobTypeId equals workerJobType.Id
                                                  where 
                                                        workerAssignments.PropertyId == localId &&
                                                        workerAssignments.StartDate > currentReservation.CheckOutDate &&
                                                        DbFunctions.TruncateTime(workerAssignments.EndDate) <= DbFunctions.TruncateTime(currentReservation.CheckOutDate) &&
                                                        workerJobType.ClassificationId == 3 //Turnover Classification
                                                  select workerAssignments).ToList();

                        var postCheckOutTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                              join workerJobType in db.WorkerJobTypes
                                                              on workerAssignments.JobTypeId equals workerJobType.Id
                                                              where 
                                                                  (workerAssignments.ReservationId ==currentReservation.Id &&
                                                                    workerAssignments.PropertyId == localId &&
                                                                    workerAssignments.StartDate >= currentReservation.CheckOutDate &&
                                                                    workerJobType.ClassificationId == 1) 
                                                                    ||
                                                                    (workerAssignments.ReservationId == futureReservation.Id &&
                                                                    workerAssignments.PropertyId == futurelocalId &&
                                                                    workerAssignments.StartDate <= futureReservation.CheckInDate &&
                                                                    workerJobType.ClassificationId == 1)//Turnover Classification
                                                              select workerAssignments).ToList();

                            //var preCheckInTurnoverWorker = preCheckInTurnoverWorkerList.Any(y => y.AssignmentStatus == 1) ? preCheckInTurnoverWorkerList.OrderByDescending(y => y.StartDate).FirstOrDefault(y => y.AssignmentStatus == 1) : preCheckInTurnoverWorkerList.OrderByDescending(y => y.StartDate).tol();
                            //var checkInWorker = checkInWorkerList.Any(y => y.AssignmentStatus == 3) ? checkInWorkerList.FirstOrDefault(y => y.AssignmentStatus == 3) : checkInWorkerList.FirstOrDefault();
                            //var checkOutWorker = checkOutWorkerList.Any(y => y.AssignmentStatus == 3) ? checkOutWorkerList.FirstOrDefault(y => y.AssignmentStatus == 3) : checkOutWorkerList.FirstOrDefault();
                            //var postCheckOutTurnoverWorker = postCheckOutTurnoverWorkerList.Any(y => y.AssignmentStatus == 1) ? postCheckOutTurnoverWorkerList.FirstOrDefault(y => y.AssignmentStatus == 1) : postCheckOutTurnoverWorkerList.FirstOrDefault();

                            //var preCheckInTurnoverWorker = db.WorkerAssignments.FirstOrDefault(y => y.InquiryId == item.InquiryId && y.PropertyId == item.PropertyId && /*y.WorkerCategory == (int)WorkerCategory.TURNOVER && y.AssignmentStatus &&*/ y.StartDate < item.CheckInDate);
                            //var postCheckOutTurnoverWorker = db.WorkerAssignments.FirstOrDefault(y => y.InquiryId == item.InquiryId && y.PropertyId == item.PropertyId && /*y.WorkerCategory == (int)WorkerCategory.TURNOVER && y.AssignmentStatus &&*/ y.StartDate >= item.CheckOutDate);

                            List<Core.Models.Task.AssignedWorkerInfo> preCheckInTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (preCheckInTurnoverWorkerList.Count > 0)
                            {
                                foreach (var preCheckInTurnoverWorker in preCheckInTurnoverWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == preCheckInTurnoverWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = preCheckInTurnoverWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = preCheckInTurnoverWorker.StartDate;
                                        WorkerInfo.EndDate = preCheckInTurnoverWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = preCheckInTurnoverWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = preCheckInTurnoverWorker.InDayStatus.ToInt();
                                        preCheckInTurnoverWorkersInfo.Add(WorkerInfo);
                                    }
                                }
                            }

                            List<Core.Models.Task.AssignedWorkerInfo> checkInWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (checkInWorkerList.Count > 0)
                            {
                                foreach (var checkInWorker in checkInWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == checkInWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = checkInWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = checkInWorker.StartDate;
                                        WorkerInfo.EndDate = checkInWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = checkInWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = checkInWorker.InDayStatus.ToInt();
                                        checkInWorkersInfo.Add(WorkerInfo);
                                    }
                                }
                            }

                            List<Core.Models.Task.AssignedWorkerInfo> checkOutWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (checkOutWorkerList.Count > 0)
                            {
                                foreach (var checkOutWorker in checkOutWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == checkOutWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = checkOutWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = checkOutWorker.StartDate;
                                        WorkerInfo.EndDate = checkOutWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = checkOutWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = checkOutWorker.InDayStatus.ToInt();
                                        checkOutWorkersInfo.Add(WorkerInfo);
                                    }
                                }
                            }

                            List<Core.Models.Task.AssignedWorkerInfo> postCheckOutTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (postCheckOutTurnoverWorkerList.Count > 0)
                            {
                                foreach (var postCheckOutTurnoverWorker in postCheckOutTurnoverWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == postCheckOutTurnoverWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = postCheckOutTurnoverWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = postCheckOutTurnoverWorker.StartDate;
                                        WorkerInfo.EndDate = postCheckOutTurnoverWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = postCheckOutTurnoverWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = postCheckOutTurnoverWorker.InDayStatus.ToInt();
                                        postCheckOutTurnoverWorkersInfo.Add(WorkerInfo);

                                    }
                                }
                            }
                            var sortedNoteList = new List<Note>(db.tbNote.Where(y => y.ReservationId == currentReservation.Id));
                            List<TravelEventsViewModel> travelEventList = new List<TravelEventsViewModel>();

                            foreach (var note in sortedNoteList)
                            {
                                TravelEventsViewModel travelEvent = new TravelEventsViewModel
                                {
                                    Id = note.Id,
                                    ReservationId = note.ReservationId,
                                    NoteType = note.NoteType,
                                    InquiryNote = note.InquiryNote,
                                    FlightNumber = note.FlightNumber,
                                    ArrivalDateTime = note.ArrivalDateTime,
                                    DepartureDateTime = note.DepartureDateTime,
                                    DepartureCityCode = note.DepartureCityCode,
                                    LeavingDate = note.LeavingDate,
                                    ArrivalAirportCode = note.ArrivalAirportCode,
                                    MeetingPlace = note.MeetingPlace,
                                    MeetingDateTime = note.MeetingDateTime,
                                    People = note.People,
                                    Airline = note.Airline,
                                    NumberOfPeople = note.NumberOfPeople,
                                    CarType = note.CarType,
                                    CarColor = note.CarColor,
                                    PlateNumber = note.PlateNumber,
                                    SourceCity = note.SourceCity,
                                    CruiseName = note.CruiseName,
                                    AirportCodeDeparture = note.AirportCodeDeparture,
                                    AirportCodeArrival = note.AirportCodeArrival,
                                    IdentifiableFeature = note.IdentifiableFeature,
                                    DepartureCity = note.DepartureCity,
                                    DeparturePort = note.DeparturePort,
                                    ArrivalPort = note.ArrivalPort,
                                    Name = note.Name,
                                    TerminalStation = note.TerminalStation,
                                    ComparisonDate = travelEventsDepartureTypes.Contains(note.NoteType) ? note.DepartureDateTime : (note.NoteType == (int)NoteType.MEETING ? note.MeetingDateTime : note.ArrivalDateTime)
                                    ,Checkin = note.Checkin,
                                    Checkout=note.Checkout,
                                    ContactPerson = note.ContactPerson
                                };

                                travelEventList.Add(travelEvent);
                            }

                            var sortedTravelEvents = new List<TravelEventsViewModel>(travelEventList.OrderBy(y => y.ComparisonDate));


                            TasksViewModel record = new TasksViewModel
                            {
                                InquiryTask = inquiryTasks,
                                TurnoverTask = turnoverTasks,
                                BookingTask = bookingTasks,
                                CheckInTask = checkInTasks,
                                CheckOutTask = checkOutTasks,
                                PropertyInfo = propertyInfo,
                                GuestInfo = guestInfo,
                                Inquiry = currentReservation,
                                TravelEvents = sortedTravelEvents,
                                //AssignedWorkers = assignedWorker,
                                PreCheckInTurnoverWorkers = preCheckInTurnoverWorkersInfo,
                                CheckInWorkers = checkInWorkersInfo,
                                CheckOutWorkers = checkOutWorkersInfo,
                                PostCheckOutTurnoverWorkers = postCheckOutTurnoverWorkersInfo
                            };
                            model.Add(record);

                        }


                    }
                    if (GlobalVariables.UserWorkerId!=0) {
                        var assignmentReservationIds = db.WorkerAssignments.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).Select(x => x.ReservationId).ToList();
                      
                       
                        foreach(var id in assignmentReservationIds)
                        {
                            model.RemoveAll(x=>x.Inquiry.Id !=id && !assignmentReservationIds.Contains(x.Inquiry.Id));
                        }
                    }
                    
                        
                    return View(model.OrderBy(x => x.Inquiry.CheckInDate));
                    //JM 10/8//18 End New Code

                    //    var inquiries = db.Inquiries.Where(x => x.CheckInDate >= DateTime.Now && x.BookingStatusCode =="A").OrderBy(x=>x.PropertyId).OrderByDescending( x=>x.CheckInDate).ToList();
                    //    foreach (var item in inquiries)
                    //    {

                    //        var property = db.Properties.Where(x => x.ListingId == item.PropertyId).FirstOrDefault();
                    //        var localId = Core.API.AllSite.Properties.GetLocalId(item.PropertyId);
                    //        if (property.TimeZoneName != null)
                    //        {
                    //            item.CheckInDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)item.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName));
                    //            item.CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)item.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName));
                    //        }

                    //        var propertyInfo = db.Properties.FirstOrDefault(x => x.ListingId == item.PropertyId);
                    //        var guestInfo = db.Guests.FirstOrDefault(x => x.Id == item.GuestId);
                    //        var workersAssigned = db.WorkerAssignments.Where(x => x.ReservationId == item.ReservationId && x.PropertyId == localId).OrderBy(x => x.StartDate).FirstOrDefault();

                    //        AssignedWorkerInfo assignedWorker = null;

                    //        if (workersAssigned != null)
                    //        {
                    //            var workerInfo = db.Workers.FirstOrDefault(x => x.Id == workersAssigned.WorkerId);
                    //            if (workerInfo != null)
                    //            {
                    //                assignedWorker = new AssignedWorkerInfo();
                    //                assignedWorker.FirstName = workerInfo.Firstname;
                    //                assignedWorker.LastName = workerInfo.Lastname;
                    //                assignedWorker.StartDate = workersAssigned.StartDate;
                    //                assignedWorker.EndDate = workersAssigned.EndDate;
                    //                assignedWorker.Mobile = workerInfo.Mobile;
                    //            }
                    //        }

                    //        //var checkInDateWithHour =  item.CheckInDate.Value.AddHours(1);
                    //        //var checkOutDateWithHour = item.CheckOutDate.Value.AddHours(-1);

                    //        var preCheckInTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                    //                                           join workerJobType in db.WorkerJobTypes
                    //                                           on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                           where //workerAssignments.InquiryId == item.InquiryId
                    //                                                 //&& 
                    //                                                 workerAssignments.PropertyId == localId &&
                    //                                                 workerAssignments.StartDate < item.CheckInDate && 
                    //                                                 workerJobType.ClassificationId == 1 //Turnover Classification
                    //                                           select workerAssignments).ToList();

                    //        var checkInWorkerList = (from workerAssignments in db.WorkerAssignments
                    //                                join workerJobType in db.WorkerJobTypes
                    //                                on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                where //workerAssignments.InquiryId == item.InquiryId
                    //                                      //&& 
                    //                                      workerAssignments.PropertyId == localId &&
                    //                                      DbFunctions.TruncateTime(workerAssignments.StartDate) >= DbFunctions.TruncateTime(item.CheckInDate) &&
                    //                                      workerAssignments.EndDate <= item.CheckOutDate &&
                    //                                      workerJobType.ClassificationId == 3 //Turnover Classification
                    //                                select workerAssignments).ToList();

                    //        var checkOutWorkerList = (from workerAssignments in db.WorkerAssignments
                    //                                 join workerJobType in db.WorkerJobTypes
                    //                                 on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                 where //workerAssignments.InquiryId == item.InquiryId
                    //                                       //&& 
                    //                                       workerAssignments.PropertyId == localId &&
                    //                                       workerAssignments.StartDate > item.CheckOutDate && 
                    //                                       DbFunctions.TruncateTime(workerAssignments.EndDate) == DbFunctions.TruncateTime(item.CheckOutDate) &&
                    //                                       workerJobType.ClassificationId == 3 //Turnover Classification
                    //                                 select workerAssignments).ToList();

                    //        var postCheckOutTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                    //                                             join workerJobType in db.WorkerJobTypes
                    //                                             on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                             where //workerAssignments.InquiryId == item.InquiryId
                    //                                                   //&& 
                    //                                                   workerAssignments.PropertyId == localId &&
                    //                                                   workerAssignments.StartDate >= item.CheckOutDate && 
                    //                                                   workerJobType.ClassificationId == 1 //Turnover Classification
                    //                                             select workerAssignments).ToList();

                    //        /*TODO 14/3/2018 Danial to test  */
                    //        var preCheckInTurnoverWorker = preCheckInTurnoverWorkerList.Any(x => x.AssignmentStatus == 1) ? preCheckInTurnoverWorkerList.OrderByDescending(x => x.StartDate).FirstOrDefault(x => x.AssignmentStatus == 1) : preCheckInTurnoverWorkerList.OrderByDescending(x => x.StartDate).FirstOrDefault();
                    //        var checkInWorker = checkInWorkerList.Any(x => x.AssignmentStatus == 3) ? checkInWorkerList.FirstOrDefault(x => x.AssignmentStatus == 3) : checkInWorkerList.FirstOrDefault();
                    //        var checkOutWorker = checkOutWorkerList.Any(x => x.AssignmentStatus == 3) ? checkOutWorkerList.FirstOrDefault(x => x.AssignmentStatus == 3) : checkOutWorkerList.FirstOrDefault();
                    //        var postCheckOutTurnoverWorker = postCheckOutTurnoverWorkerList.Any(x => x.AssignmentStatus == 1) ? postCheckOutTurnoverWorkerList.FirstOrDefault(x => x.AssignmentStatus == 1) : postCheckOutTurnoverWorkerList.FirstOrDefault();

                    //        //var preCheckInTurnoverWorker = db.WorkerAssignments.FirstOrDefault(x => x.InquiryId == item.InquiryId && x.PropertyId == item.PropertyId && /*x.WorkerCategory == (int)WorkerCategory.TURNOVER && x.AssignmentStatus &&*/ x.StartDate < item.CheckInDate);
                    //        //var postCheckOutTurnoverWorker = db.WorkerAssignments.FirstOrDefault(x => x.InquiryId == item.InquiryId && x.PropertyId == item.PropertyId && /*x.WorkerCategory == (int)WorkerCategory.TURNOVER && x.AssignmentStatus &&*/ x.StartDate >= item.CheckOutDate);

                    //        AssignedWorkerInfo preCheckInTurnoverWorkerInfo = null;

                    //        if (preCheckInTurnoverWorker != null)
                    //        {
                    //            var workerInfo = db.Workers.FirstOrDefault(x => x.Id == preCheckInTurnoverWorker.WorkerId);
                    //            if (workerInfo != null)
                    //            {
                    //                preCheckInTurnoverWorkerInfo = new AssignedWorkerInfo();
                    //                preCheckInTurnoverWorkerInfo.FirstName = workerInfo.Firstname;
                    //                preCheckInTurnoverWorkerInfo.LastName = workerInfo.Lastname;
                    //                preCheckInTurnoverWorkerInfo.StartDate = preCheckInTurnoverWorker.StartDate;
                    //                preCheckInTurnoverWorkerInfo.EndDate = preCheckInTurnoverWorker.EndDate;
                    //                preCheckInTurnoverWorkerInfo.Mobile = workerInfo.Mobile;
                    //            }
                    //        }

                    //        AssignedWorkerInfo checkInWorkerInfo = null;

                    //        if (checkInWorker != null)
                    //        {
                    //            var workerInfo = db.Workers.FirstOrDefault(x => x.Id == checkInWorker.WorkerId);
                    //            if (workerInfo != null)
                    //            {
                    //                checkInWorkerInfo = new AssignedWorkerInfo();
                    //                checkInWorkerInfo.FirstName = workerInfo.Firstname;
                    //                checkInWorkerInfo.LastName = workerInfo.Lastname;
                    //                checkInWorkerInfo.StartDate = checkInWorker.StartDate;
                    //                checkInWorkerInfo.EndDate = checkInWorker.EndDate;
                    //                checkInWorkerInfo.Mobile = workerInfo.Mobile;
                    //            }
                    //        }

                    //        AssignedWorkerInfo checkOutWorkerInfo = null;

                    //        if (checkOutWorker != null)
                    //        {
                    //            var workerInfo = db.Workers.FirstOrDefault(x => x.Id == checkOutWorker.WorkerId);
                    //            if (workerInfo != null)
                    //            {
                    //                checkOutWorkerInfo = new AssignedWorkerInfo();
                    //                checkOutWorkerInfo.FirstName = workerInfo.Firstname;
                    //                checkOutWorkerInfo.LastName = workerInfo.Lastname;
                    //                checkOutWorkerInfo.StartDate = checkOutWorker.StartDate;
                    //                checkOutWorkerInfo.EndDate = checkOutWorker.EndDate;
                    //                checkOutWorkerInfo.Mobile = workerInfo.Mobile;
                    //            }
                    //        }

                    //        AssignedWorkerInfo postCheckOutTurnoverWorkerInfo = null;

                    //        if (postCheckOutTurnoverWorker != null)
                    //        {
                    //            var workerInfo = db.Workers.FirstOrDefault(x => x.Id == postCheckOutTurnoverWorker.WorkerId);
                    //            if (workerInfo != null)
                    //            {
                    //                postCheckOutTurnoverWorkerInfo = new AssignedWorkerInfo();
                    //                postCheckOutTurnoverWorkerInfo.FirstName = workerInfo.Firstname;
                    //                postCheckOutTurnoverWorkerInfo.LastName = workerInfo.Lastname;
                    //                postCheckOutTurnoverWorkerInfo.StartDate = postCheckOutTurnoverWorker.StartDate;
                    //                postCheckOutTurnoverWorkerInfo.EndDate = postCheckOutTurnoverWorker.EndDate;
                    //                postCheckOutTurnoverWorkerInfo.Mobile = workerInfo.Mobile;
                    //            }
                    //        }

                    //        var sortedNoteList = new List<Note>(db.tbNote.Where(x => x.ReservationId == item.Id));
                    //        List<TravelEventsViewModel> travelEventList = new List<TravelEventsViewModel>();

                    //        foreach (var note in sortedNoteList)
                    //        {
                    //            TravelEventsViewModel travelEvent = new TravelEventsViewModel
                    //            {
                    //                Id = note.Id,
                    //                ReservationId = note.ReservationId,
                    //                NoteType = note.NoteType,
                    //                InquiryNote = note.InquiryNote,
                    //                FlightNumber = note.FlightNumber,
                    //                ArrivalDateTime = note.ArrivalDateTime,
                    //                DepartureDateTime = note.DepartureDateTime,
                    //                DepartureCityCode = note.DepartureCityCode,
                    //                LeavingDate = note.LeavingDate,
                    //                ArrivalAirportCode = note.ArrivalAirportCode,
                    //                MeetingPlace = note.MeetingPlace,
                    //                MeetingDateTime = note.MeetingDateTime,
                    //                People = note.People,
                    //                Airline = note.Airline,
                    //                NumberOfPeople = note.NumberOfPeople,
                    //                CarType = note.CarType,
                    //                CarColor = note.CarColor,
                    //                PlateNumber = note.PlateNumber,
                    //                SourceCity = note.SourceCity,
                    //                CruiseName = note.CruiseName,
                    //                AirportCodeDeparture = note.AirportCodeDeparture,
                    //                AirportCodeArrival = note.AirportCodeArrival,
                    //                IdentifiableFeature = note.IdentifiableFeature,
                    //                DepartureCity = note.DepartureCity,
                    //                DeparturePort = note.DeparturePort,
                    //                ArrivalPort = note.ArrivalPort,
                    //                Name = note.Name,
                    //                TerminalStation = note.TerminalStation,
                    //                ComparisonDate = travelEventsDepartureTypes.Contains(note.NoteType) ? note.DepartureDateTime : (note.NoteType == (int)NoteType.MEETING ? note.MeetingDateTime : note.ArrivalDateTime)
                    //            };

                    //            travelEventList.Add(travelEvent);
                    //        }

                    //        var sortedTravelEvents = new List<TravelEventsViewModel>(travelEventList.OrderBy(x => x.ComparisonDate));


                    //        TasksViewModel record = new TasksViewModel
                    //        {
                    //            InquiryTask = inquiryTasks,
                    //            TurnoverTask = turnoverTasks,
                    //            BookingTask = bookingTasks,
                    //            CheckInTask = checkInTasks,
                    //            CheckOutTask = checkOutTasks,
                    //            PropertyInfo = propertyInfo,
                    //            GuestInfo = guestInfo,
                    //            Inquiry = item,
                    //            TravelEvents = sortedTravelEvents,
                    //            AssignedWorker = assignedWorker,
                    //            PreCheckInTurnoverWorker = preCheckInTurnoverWorkerInfo,
                    //            CheckInWorker = checkInWorkerInfo,
                    //            CheckOutWorker = checkOutWorkerInfo,
                    //            PostCheckOutTurnoverWorker = postCheckOutTurnoverWorkerInfo
                    //        };
                    //        model.Add(record);
                    //    }
                    //}

                    //return View(model.OrderBy(x => x.Inquiry.CheckInDate));
                }
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetInquiryTask(string confirmationCode)
        {
            InquiryTaskViewModel taskList = new InquiryTaskViewModel();
            ICommonRepository objIList = new CommonRepository();
            object responseData = null;

            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Error is coming due to unauthorized permission", JsonRequestBehavior.AllowGet);
            }
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var inquiryTasks = db.InquiryTasks.Where(x => x.ConfirmationCode == confirmationCode && x.CompanyId == SessionHandler.CompanyId);

                    
                    var TaskData = (from a in inquiryTasks
                                 group a by a.TemplateId
                                       into g
                                 orderby g.Key
                                 select new
                                 {
                                     TemplateId = g.Key,
                                     Tasks = g,


                                 }).ToList();

                    List<Core.Models.Task.TaskTemplate> Templates = new List<Core.Models.Task.TaskTemplate>();

                    foreach (var temp in TaskData) {
                        Core.Models.Task.TaskTemplate tempTemplate = new Core.Models.Task.TaskTemplate();
                        var template = db.TaskTemplates.Where(x => x.Id == temp.TemplateId).FirstOrDefault();
                        tempTemplate.Id = template.Id;
                        tempTemplate.Name = template.TemplateName;

                        var tasks = (from task in db.Tasks
                                     join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                     where task.TemplateId ==template.Id && inqTask.ConfirmationCode == confirmationCode
                                     select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                 
                        foreach (var task in tasks)
                        {
                            Core.Models.Task.Task tempTask= new Core.Models.Task.Task();
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            tempTask.Title = task.Title;
                            var subTask = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status ,ImageUrl = st.ImageUrl,UserId= ist.UserId,LastUpdateDate=ist.LastUpdateDate}).ToList();
                            foreach (var sub in subTask)
                            {
                                string UpdateBy = (sub.UserId != null ? db.Users.Where(y => y.Id == sub.UserId).FirstOrDefault().FirstName : null);

                                Core.Models.Task.SubTask tempSub = new Core.Models.Task.SubTask();
                                tempSub.Id = sub.Id.ToString();
                                tempSub.ImageUrl = sub.ImageUrl;
                                tempSub.Status = sub.Status;
                                tempSub.Title = sub.Title;
                                tempSub.UpdateBy = UpdateBy;
                                tempSub.LastUpdateDate = sub.LastUpdateDate;
                                tempSub.Description = sub.Description;

                                tempTask.SubTasks.Add(tempSub);
                            }
                            tempTemplate.Tasks.Add(tempTask);
                            
                        }
                        Templates.Add(tempTemplate);


                    }
                    responseData = new
                    {
                        Templates =Templates,
                        TotalTaskCount = inquiryTasks.Count(),
                        CompletedTaskCount = inquiryTasks.Count(x => x.Status == (int) TaskListStatus.COMPLETED),
                        PendingTaskCount = inquiryTasks.Count(x => x.Status == (int) TaskListStatus.PENDING)
                    };


                    /*
                    //Get Task Id list
                    var taskIds = inquiryTasks.Select(x => x.TaskId);

                    //Get Task Template Id
                    var taskTemplateIds = inquiryTasks.Select(x => x.TemplateId);

                    if (taskIds.Any())
                    {
                        var tasks = new List<Task>(
                            from task in db.Tasks
                            where taskIds.Contains(task.Id)
                            select task
                            );

                        foreach (var item in tasks)
                        {
                            item.Status = inquiryTasks.FirstOrDefault(x => x.TaskId == item.Id).Status;
                            item.InquiryTaskId = inquiryTasks.FirstOrDefault(x => x.TaskId == item.Id).Id;
                        }

                        if (tasks.Any())
                        {
                            TODO 3/10/2017: Maybe no need as is not used in front-end.
                            var taskTemplateId = taskTemplateIds.FirstOrDefault();
                            var taskTemplate = db.TaskTemplates.FirstOrDefault(x => x.Id == taskTemplateId);

                            taskList.TaskTemplate = taskTemplate;

                            foreach (var item in tasks)
                            {
                                var taskTemplate = db.TaskTemplates.FirstOrDefault(x => x.Id == item.TemplateId);
                                if (taskTemplate != null)
                                {
                                    item.TemplateName = taskTemplate.TemplateName;
                                }
                            }

                            //TODO 4/10/2017: Need to group by template name and in UI show in grid instead of column for Template Name.
                            //TODO 4/10/2017: Test the grouping below part with UI handling whether can support current flow.
                            var Tasks = from a in tasks
                                       group a by a.TemplateId
                                       into g
                                       orderby g.Key
                                       select new {
                                           TemplateId = g.Key,
                                           TemplateName = g.First().TemplateName,
                                           Tasks = g
                                       };

                            responseData = new
                            {
                                Tasks,
                                TotalTaskCount = tasks.Count,
                                CompletedTaskCount = tasks.Count(x => x.Status == (int) TaskListStatus.COMPLETED),
                                PendingTaskCount = tasks.Count(x => x.Status == (int) TaskListStatus.PENDING)
                            };

                            taskList.Tasks = tasks;

                            taskList.TotalTaskCount = tasks.Count;
                            taskList.CompletedTaskCount = tasks.Count(x => x.Status == (int)TaskListStatus.COMPLETED);
                            taskList.PendingTaskCount = tasks.Count(x => x.Status == (int)TaskListStatus.PENDING);
                        }   
                } 
                */
                }
                return Json(responseData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                object task = null;
                responseData = new
                {
                    Templates = task,
                    TotalTaskCount = 0,
                    CompletedTaskCount = 0,
                    PendingTaskCount = 0 
                };
                return Json(responseData, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetInquiryWithInquiryTask(int selectedMonth, int selectedYear, int selectedProperty)
        {
            List<TasksViewModel> model = new List<TasksViewModel>();

            try
            {

                //Add by Danial 22-2-2018
                //Default settings for selected month & year
                if (selectedMonth == 0 || selectedYear == 0)
                {
                    selectedMonth = DateTime.Now.Month;
                    selectedYear = DateTime.Now.Year;
                }


                using (var db = new ApplicationDbContext())
                {
                    List<Property> Properties = new List<Property>();

                    List<int> propertyParentIds = new List<int>();

                    if (selectedProperty != -1)
                    {
                        var property = db.Properties.Where(x => x.Id == selectedProperty).FirstOrDefault();
                        if (property.IsParentProperty)
                        {
                            Properties = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == property.Id select p).ToList();// db.Properties.Where(x => x.ParentPropertyId == property.Id).ToList();
                        }
                        else
                        {

                            Properties = db.Properties.Where(x => x.Id == selectedProperty).ToList();
                        }
                      

                    }
                    else    
                    {
                        Properties = db.Properties.Where(x => x.IsParentProperty == false).ToList();
                    }
                   

                    foreach (var property in Properties)
                    {
                        List<long> propertyChildId = new List<long>();
                        List<Inquiry> Inquiries = new List<Inquiry>();
                        var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();

                        if (parentChild!= null)
                        {
                            if (!propertyParentIds.Contains(parentChild.ParentPropertyId))
                            {
                                propertyChildId = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parentChild.ParentPropertyId select p.ListingId).ToList();//db.Properties.Where(x => x.ParentPropertyId == property.ParentPropertyId).Select(x => x.ListingId).ToList();
                                propertyParentIds.Add(parentChild.ParentPropertyId);
                            }

                            foreach (var p in propertyChildId)
                            {
                                Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == p && x.BookingStatusCode == "A" && 
                                ((x.CheckInDate.Value.Year == selectedYear && x.CheckInDate.Value.Month == selectedMonth) ||
                                (x.CheckInDate.Value.Year == (selectedMonth==12?selectedYear+1:selectedYear) && x.CheckInDate.Value.Month == (selectedMonth==12?1:selectedMonth+1)) ||
                                (x.CheckInDate.Value.Year == (selectedMonth == 1 ? selectedYear - 1 : selectedYear) && x.CheckInDate.Value.Month == (selectedMonth == 1 ? 12 : selectedMonth-1))) ));
                            }

                        }
                        else
                        {
                            Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.BookingStatusCode == "A" &&((x.CheckInDate.Value.Year == selectedYear && x.CheckInDate.Value.Month == selectedMonth) ||
                                (x.CheckInDate.Value.Year == (selectedMonth == 12 ? selectedYear + 1 : selectedYear) && x.CheckInDate.Value.Month == (selectedMonth == 12 ? 1 : selectedMonth+1)) ||
                                (x.CheckInDate.Value.Year == (selectedMonth == 1 ? selectedYear - 1 : selectedYear) && x.CheckInDate.Value.Month == (selectedMonth == 1 ? 12 : selectedMonth-1)))));
                        }

                        Inquiries = Inquiries.OrderBy(x => x.CheckInDate).ToList();
                        int count = Inquiries.Count;
                        for (int x = 0; x < count; x++)
                        {
                            Inquiry previousReservation = new Inquiry();
                            if (x > 0)
                            {
                                previousReservation = Inquiries[x - 1];
                            }
                            var currentReservation = Inquiries[x];

                            Inquiry futureReservation = new Inquiry();
                            if (x < count-1)
                            {
                                futureReservation = Inquiries[x + 1];
                            }
                            var propertyInfo = db.Properties.Where(y => y.ListingId == currentReservation.PropertyId).FirstOrDefault();
                            var guestInfo = db.Guests.FirstOrDefault(y => y.Id == currentReservation.GuestId);
                            if (guestInfo == null)
                            {

                            }
                            if (propertyInfo.TimeZoneName != null)
                            {
                                currentReservation.CheckInDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                                currentReservation.CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                            }

                            var localId = Core.API.AllSite.Properties.GetLocalId(currentReservation.PropertyId);
                            var prevlocalId = Core.API.AllSite.Properties.GetLocalId(previousReservation.PropertyId);
                            var futurelocalId = Core.API.AllSite.Properties.GetLocalId(futureReservation.PropertyId);
                            List<Core.Models.Task.Task> inquiryTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> turnoverTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> bookingTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> checkInTasks = new List<Core.Models.Task.Task>();
                            List<Core.Models.Task.Task> checkOutTasks = new List<Core.Models.Task.Task>();

                            var inquiryTasksData = (from task in db.Tasks
                                                    join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                    where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.INQUIRY
                                                    select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();

                            foreach (var task in inquiryTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                inquiryTasks.Add(tempTask);
                            }
                            var turnoverTasksData = (from task in db.Tasks
                                                     join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                     where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.TURNOVER
                                                     select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.TURNOVER).ToList();

                            foreach (var task in turnoverTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                turnoverTasks.Add(tempTask);
                            }
                            var bookingTasksData = (from task in db.Tasks
                                                    join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                    where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.BOOKING
                                                    select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.BOOKING).ToList();

                            foreach (var task in bookingTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                bookingTasks.Add(tempTask);
                            }
                            var checkInTasksData = (from task in db.Tasks
                                                    join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                    where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.CHECK_IN
                                                    select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.CHECK_IN).ToList();
                            foreach (var task in checkInTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                checkInTasks.Add(tempTask);
                            }
                            var checkOutTasksData = (from task in db.Tasks
                                                     join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                     where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)TaskTemplateType.CHECK_OUT
                                                     select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                            //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.CHECK_OUT).ToList();
                            foreach (var task in checkOutTasksData)
                            {
                                Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                                tempTask.Title = task.Title;
                                tempTask.Description = task.Description;
                                tempTask.Id = task.Id.ToString();
                                tempTask.ImageUrl = task.ImageUrl;
                                tempTask.Status = task.Status.ToBoolean();
                                var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                                foreach (var item in subs)
                                {
                                    string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                    var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                    tempTask.SubTasks.Add(sub);
                                }
                                checkOutTasks.Add(tempTask);
                            }
                            //var inquiryTasks = db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.INQUIRY).ToList();
                            //foreach (var task in inquiryTasks)
                            //{
                            //    var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status }).ToList();
                            //    foreach (var item in subs)
                            //    {
                            //        var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status };
                            //        task.SubTasks.Add(sub);
                            //    }
                            //}
                            //var turnoverTasks =db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.TURNOVER).ToList();

                            //foreach (var task in turnoverTasks)
                            //{
                            //    var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status }).ToList();
                            //    foreach (var item in subs)
                            //    {
                            //        var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status };
                            //        task.SubTasks.Add(sub);
                            //    }
                            //}
                            //var bookingTasks = db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.BOOKING).ToList();

                            //foreach (var task in bookingTasks)
                            //{
                            //    var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status }).ToList();
                            //    foreach (var item in subs)
                            //    {
                            //        var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status };
                            //        task.SubTasks.Add(sub);
                            //    }
                            //}
                            //var checkInTasks = db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.CHECK_IN).ToList();
                            //foreach (var task in checkInTasks)
                            //{
                            //    var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status }).ToList();
                            //    foreach (var item in subs)
                            //    {
                            //        var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status };
                            //        task.SubTasks.Add(sub);
                            //    }
                            //}
                            //var checkOutTasks =db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)TaskTemplateType.CHECK_OUT).ToList();
                            //foreach (var task in checkOutTasks)
                            //{
                            //    var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status }).ToList();
                            //    foreach (var item in subs)
                            //    {
                            //        var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description,Status=item.Status };
                            //        task.SubTasks.Add(sub);
                            //    }
                            //}

                            var workersAssigned = db.WorkerAssignments.Where(y => y.ReservationId == currentReservation.Id && y.PropertyId == localId).OrderBy(y => y.StartDate).FirstOrDefault();

                            AssignedWorkerInfo assignedWorker = null;

                            if (workersAssigned != null)
                            {
                                var workerInfo = db.Workers.FirstOrDefault(y => y.Id == workersAssigned.WorkerId);
                                if (workerInfo != null)
                                {
                                    assignedWorker = new AssignedWorkerInfo();
                                    assignedWorker.FirstName = workerInfo.Firstname;
                                    assignedWorker.LastName = workerInfo.Lastname;
                                    assignedWorker.StartDate = workersAssigned.StartDate;
                                    assignedWorker.EndDate = workersAssigned.EndDate;
                                    assignedWorker.Mobile = workerInfo.Mobile;
                                }
                            }

                            var preCheckInTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                                join workerJobType in db.WorkerJobTypes
                                                                on workerAssignments.JobTypeId equals workerJobType.Id
                                                                where
                                                                     (workerAssignments.ReservationId == previousReservation.Id &&
                                                                      workerAssignments.PropertyId == prevlocalId &&
                                                                      workerAssignments.StartDate >= previousReservation.CheckOutDate &&
                                                                      workerJobType.ClassificationId == 1)
                                                                      ||
                                                                      (workerAssignments.ReservationId == currentReservation.Id &&
                                                                      workerAssignments.PropertyId == localId &&
                                                                      workerAssignments.StartDate <= currentReservation.CheckInDate &&
                                                                      workerJobType.ClassificationId == 1) //Turnover Classification
                                                                select workerAssignments).ToList();

                            var checkInWorkerList = (from workerAssignments in db.WorkerAssignments
                                                     join workerJobType in db.WorkerJobTypes
                                                     on workerAssignments.JobTypeId equals workerJobType.Id
                                                     where
                                                           workerAssignments.PropertyId == localId &&
                                                           DbFunctions.TruncateTime(workerAssignments.StartDate) >= DbFunctions.TruncateTime(currentReservation.CheckInDate) &&
                                                           workerAssignments.EndDate <= currentReservation.CheckOutDate &&
                                                           workerJobType.ClassificationId == 3 //Turnover Classification
                                                     select workerAssignments).ToList();

                            var checkOutWorkerList = (from workerAssignments in db.WorkerAssignments
                                                      join workerJobType in db.WorkerJobTypes
                                                      on workerAssignments.JobTypeId equals workerJobType.Id
                                                      where
                                                            workerAssignments.PropertyId == localId &&
                                                            workerAssignments.StartDate > currentReservation.CheckOutDate &&
                                                            DbFunctions.TruncateTime(workerAssignments.EndDate) <= DbFunctions.TruncateTime(currentReservation.CheckOutDate) &&
                                                            workerJobType.ClassificationId == 3 //Turnover Classification
                                                      select workerAssignments).ToList();

                            var postCheckOutTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                                  join workerJobType in db.WorkerJobTypes
                                                                  on workerAssignments.JobTypeId equals workerJobType.Id
                                                                  where
                                                                      (workerAssignments.ReservationId == currentReservation.Id &&
                                                                        workerAssignments.PropertyId == localId &&
                                                                        workerAssignments.StartDate >= currentReservation.CheckOutDate &&
                                                                        workerJobType.ClassificationId == 1)
                                                                        ||
                                                                        (workerAssignments.ReservationId == futureReservation.Id &&
                                                                        workerAssignments.PropertyId == futurelocalId &&
                                                                        workerAssignments.StartDate <= futureReservation.CheckInDate &&
                                                                        workerJobType.ClassificationId == 1)//Turnover Classification
                                                                  select workerAssignments).ToList();

                            //var preCheckInTurnoverWorker = preCheckInTurnoverWorkerList.Any(y => y.AssignmentStatus == 1) ? preCheckInTurnoverWorkerList.OrderByDescending(y => y.StartDate).FirstOrDefault(y => y.AssignmentStatus == 1) : preCheckInTurnoverWorkerList.OrderByDescending(y => y.StartDate).tol();
                            //var checkInWorker = checkInWorkerList.Any(y => y.AssignmentStatus == 3) ? checkInWorkerList.FirstOrDefault(y => y.AssignmentStatus == 3) : checkInWorkerList.FirstOrDefault();
                            //var checkOutWorker = checkOutWorkerList.Any(y => y.AssignmentStatus == 3) ? checkOutWorkerList.FirstOrDefault(y => y.AssignmentStatus == 3) : checkOutWorkerList.FirstOrDefault();
                            //var postCheckOutTurnoverWorker = postCheckOutTurnoverWorkerList.Any(y => y.AssignmentStatus == 1) ? postCheckOutTurnoverWorkerList.FirstOrDefault(y => y.AssignmentStatus == 1) : postCheckOutTurnoverWorkerList.FirstOrDefault();

                            //var preCheckInTurnoverWorker = db.WorkerAssignments.FirstOrDefault(y => y.InquiryId == item.InquiryId && y.PropertyId == item.PropertyId && /*y.WorkerCategory == (int)WorkerCategory.TURNOVER && y.AssignmentStatus &&*/ y.StartDate < item.CheckInDate);
                            //var postCheckOutTurnoverWorker = db.WorkerAssignments.FirstOrDefault(y => y.InquiryId == item.InquiryId && y.PropertyId == item.PropertyId && /*y.WorkerCategory == (int)WorkerCategory.TURNOVER && y.AssignmentStatus &&*/ y.StartDate >= item.CheckOutDate);

                            List<Core.Models.Task.AssignedWorkerInfo> preCheckInTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (preCheckInTurnoverWorkerList.Count > 0)
                            {
                                foreach (var preCheckInTurnoverWorker in preCheckInTurnoverWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == preCheckInTurnoverWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = preCheckInTurnoverWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = preCheckInTurnoverWorker.StartDate;
                                        WorkerInfo.EndDate = preCheckInTurnoverWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = preCheckInTurnoverWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = preCheckInTurnoverWorker.InDayStatus.ToInt();
                                        preCheckInTurnoverWorkersInfo.Add(WorkerInfo);
                                    }
                                }
                            }

                            List<Core.Models.Task.AssignedWorkerInfo> checkInWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (checkInWorkerList.Count > 0)
                            {
                                foreach (var checkInWorker in checkInWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == checkInWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = checkInWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = checkInWorker.StartDate;
                                        WorkerInfo.EndDate = checkInWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = checkInWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = checkInWorker.InDayStatus.ToInt();
                                        checkInWorkersInfo.Add(WorkerInfo);
                                    }
                                }
                            }

                            List<Core.Models.Task.AssignedWorkerInfo> checkOutWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (checkOutWorkerList.Count > 0)
                            {
                                foreach (var checkOutWorker in checkOutWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == checkOutWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = checkOutWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = checkOutWorker.StartDate;
                                        WorkerInfo.EndDate = checkOutWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = checkOutWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = checkOutWorker.InDayStatus.ToInt();
                                        checkOutWorkersInfo.Add(WorkerInfo);
                                    }
                                }
                            }

                            List<Core.Models.Task.AssignedWorkerInfo> postCheckOutTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                            if (postCheckOutTurnoverWorkerList.Count > 0)
                            {
                                foreach (var postCheckOutTurnoverWorker in postCheckOutTurnoverWorkerList)
                                {
                                    var worker = db.Workers.FirstOrDefault(y => y.Id == postCheckOutTurnoverWorker.WorkerId);
                                    if (worker != null)
                                    {
                                        var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                        WorkerInfo.Id = postCheckOutTurnoverWorker.Id;
                                        WorkerInfo.FirstName = worker.Firstname;
                                        WorkerInfo.LastName = worker.Lastname;
                                        WorkerInfo.StartDate = postCheckOutTurnoverWorker.StartDate;
                                        WorkerInfo.EndDate = postCheckOutTurnoverWorker.EndDate;
                                        WorkerInfo.Mobile = worker.Mobile;
                                        WorkerInfo.Status = postCheckOutTurnoverWorker.Status.ToInt();
                                        WorkerInfo.InDayStatus = postCheckOutTurnoverWorker.InDayStatus.ToInt();
                                        postCheckOutTurnoverWorkersInfo.Add(WorkerInfo);

                                    }
                                }
                            }

                            var sortedNoteList = new List<Note>(db.tbNote.Where(y => y.ReservationId == currentReservation.Id));
                            List<TravelEventsViewModel> travelEventList = new List<TravelEventsViewModel>();

                            foreach (var note in sortedNoteList)
                            {
                                TravelEventsViewModel travelEvent = new TravelEventsViewModel
                                {
                                    Id = note.Id,
                                    ReservationId = note.ReservationId,
                                    NoteType = note.NoteType,
                                    InquiryNote = note.InquiryNote,
                                    FlightNumber = note.FlightNumber,
                                    ArrivalDateTime = note.ArrivalDateTime,
                                    DepartureDateTime = note.DepartureDateTime,
                                    DepartureCityCode = note.DepartureCityCode,
                                    LeavingDate = note.LeavingDate,
                                    ArrivalAirportCode = note.ArrivalAirportCode,
                                    MeetingPlace = note.MeetingPlace,
                                    MeetingDateTime = note.MeetingDateTime,
                                    People = note.People,
                                    Airline = note.Airline,
                                    NumberOfPeople = note.NumberOfPeople,
                                    CarType = note.CarType,
                                    CarColor = note.CarColor,
                                    PlateNumber = note.PlateNumber,
                                    SourceCity = note.SourceCity,
                                    CruiseName = note.CruiseName,
                                    AirportCodeDeparture = note.AirportCodeDeparture,
                                    AirportCodeArrival = note.AirportCodeArrival,
                                    IdentifiableFeature = note.IdentifiableFeature,
                                    DepartureCity = note.DepartureCity,
                                    DeparturePort = note.DeparturePort,
                                    ArrivalPort = note.ArrivalPort,
                                    Name = note.Name,
                                    TerminalStation = note.TerminalStation,
                                    ComparisonDate = travelEventsDepartureTypes.Contains(note.NoteType) ? note.DepartureDateTime : (note.NoteType == (int)NoteType.MEETING ? note.MeetingDateTime : note.ArrivalDateTime)
                                    ,
                                    Checkin = note.Checkin,
                                    Checkout = note.Checkout,
                                    ContactPerson = note.ContactPerson
                                };

                                travelEventList.Add(travelEvent);
                            }

                            var sortedTravelEvents = new List<TravelEventsViewModel>(travelEventList.OrderBy(y => y.ComparisonDate));


                            TasksViewModel record = new TasksViewModel
                            {
                                InquiryTask = inquiryTasks,
                                TurnoverTask = turnoverTasks,
                                BookingTask = bookingTasks,
                                CheckInTask = checkInTasks,
                                CheckOutTask = checkOutTasks,
                                PropertyInfo = propertyInfo,
                                GuestInfo = guestInfo,
                                Inquiry = currentReservation,
                                TravelEvents = sortedTravelEvents,
                                //AssignedWorkers = assignedWorker,
                                PreCheckInTurnoverWorkers = preCheckInTurnoverWorkersInfo,
                                CheckInWorkers = checkInWorkersInfo,
                                CheckOutWorkers = checkOutWorkersInfo,
                                PostCheckOutTurnoverWorkers = postCheckOutTurnoverWorkersInfo
                            };
                            model.Add(record);

                        }
                    }


                    //foreach (var item in inquiries)
                    //{

                    //    var property = db.Properties.Where(x => x.ListingId == item.PropertyId).FirstOrDefault();
                    //    item.CheckInDate = (DateTime)item.CheckInDate;//TimeZoneInfo.ConvertTimeFromUtc((DateTime)item.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName));
                    //    item.CheckOutDate = (DateTime)item.CheckOutDate;//TimeZoneInfo.ConvertTimeFromUtc((DateTime)item.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName));
                    //    var localId = Core.API.AllSite.Properties.GetLocalId(item.PropertyId);
                    //    var inquiryTasks = new List<InquiryTask>(db.InquiryTasks.Where(x => x.ReservationId == item.ReservationId && x.TemplateTypeId == (int)TaskTemplateType.INQUIRY));
                    //    var turnoverTasks = new List<InquiryTask>(db.InquiryTasks.Where(x => x.ReservationId == item.ReservationId && x.TemplateTypeId == (int)TaskTemplateType.TURNOVER));
                    //    var bookingTasks = new List<InquiryTask>(db.InquiryTasks.Where(x => x.ReservationId == item.ReservationId && x.TemplateTypeId == (int)TaskTemplateType.BOOKING));
                    //    var checkInTasks = new List<InquiryTask>(db.InquiryTasks.Where(x => x.ReservationId == item.ReservationId && x.TemplateTypeId == (int)TaskTemplateType.CHECK_IN));
                    //    var checkOutTasks = new List<InquiryTask>(db.InquiryTasks.Where(x => x.ReservationId == item.ReservationId && x.TemplateTypeId == (int)TaskTemplateType.CHECK_OUT));
                    //    var propertyInfo = db.Properties.FirstOrDefault(x => x.ListingId == item.PropertyId);
                    //    var guestInfo = db.Guests.FirstOrDefault(x => x.Id == item.GuestId);
                    //    var workersAssigned = db.WorkerAssignments.Where(x => x.ReservationId == item.ReservationId && x.PropertyId == localId).OrderBy(x => x.StartDate).FirstOrDefault();

                    //    AssignedWorkerInfo assignedWorker = null;

                    //    if (workersAssigned != null)
                    //    {
                    //        var workerInfo = db.Workers.FirstOrDefault(x => x.Id == workersAssigned.WorkerId);
                    //        if (workerInfo != null)
                    //        {
                    //            assignedWorker = new AssignedWorkerInfo();
                    //            assignedWorker.FirstName = workerInfo.Firstname;
                    //            assignedWorker.LastName = workerInfo.Lastname;
                    //            assignedWorker.StartDate = workersAssigned.StartDate;
                    //            assignedWorker.EndDate = workersAssigned.EndDate;
                    //            assignedWorker.Mobile = workerInfo.Mobile;
                    //        }
                    //    }

                    //    var checkInDateWithHour = item.CheckInDate.Value.AddHours(1);
                    //    var checkOutDateWithHour = item.CheckOutDate.Value.AddHours(-1);

                    //    var preCheckInTurnoverWorkerList = from workerAssignments in db.WorkerAssignments
                    //                                       join workerJobType in db.WorkerJobTypes
                    //                                       on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                       where //workerAssignments.InquiryId == item.InquiryId
                    //                                             //&& 
                    //                                             workerAssignments.PropertyId == localId &&
                    //                                             workerAssignments.StartDate < item.CheckInDate &&
                    //                                             workerJobType.ClassificationId == 1 //Turnover Classification
                    //                                       select workerAssignments;

                    //    var checkInWorkerList = from workerAssignments in db.WorkerAssignments
                    //                                       join workerJobType in db.WorkerJobTypes
                    //                                       on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                       where //workerAssignments.InquiryId == item.InquiryId
                    //                                             //&& 
                    //                                             workerAssignments.PropertyId == localId &&
                    //                                  DbFunctions.TruncateTime(workerAssignments.StartDate) >= DbFunctions.TruncateTime(item.CheckInDate) &&
                    //                                  workerAssignments.EndDate <= item.CheckOutDate &&
                    //                                  workerJobType.ClassificationId == 3 //Turnover Classification
                    //                                       select workerAssignments;

                    //    var checkOutWorkerList = from workerAssignments in db.WorkerAssignments
                    //                                    join workerJobType in db.WorkerJobTypes
                    //                                    on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                    where //workerAssignments.InquiryId == item.InquiryId
                    //                                          //&& 
                    //                                          workerAssignments.PropertyId == localId &&
                    //                                   workerAssignments.StartDate > item.CheckOutDate &&
                    //                                   DbFunctions.TruncateTime(workerAssignments.EndDate) == DbFunctions.TruncateTime(item.CheckOutDate) &&
                    //                                   workerJobType.ClassificationId == 3 //Turnover Classification
                    //                                    select workerAssignments;

                    //    var postCheckOutTurnoverWorkerList = from workerAssignments in db.WorkerAssignments
                    //                                         join workerJobType in db.WorkerJobTypes
                    //                                         on workerAssignments.JobTypeId equals workerJobType.Id
                    //                                         where //workerAssignments.InquiryId == item.InquiryId
                    //                                               //&& 
                    //                                               workerAssignments.PropertyId == localId &&
                    //                                               workerAssignments.StartDate >= item.CheckOutDate &&
                    //                                               workerJobType.ClassificationId == 1 //Turnover Classification
                    //                                         select workerAssignments;

                    //    /*TODO 14/3/2018 Danial to test  */
                    //    var preCheckInTurnoverWorker = preCheckInTurnoverWorkerList.Any(x => x.AssignmentStatus == 1) ? preCheckInTurnoverWorkerList.OrderByDescending(x => x.StartDate).FirstOrDefault(x => x.AssignmentStatus == 1) : preCheckInTurnoverWorkerList.OrderByDescending(x => x.StartDate).FirstOrDefault();
                    //    var checkInWorker = checkInWorkerList.Any(x => x.AssignmentStatus == 3) ? checkInWorkerList.FirstOrDefault(x => x.AssignmentStatus == 3) : checkInWorkerList.FirstOrDefault();
                    //    var checkOutWorker = checkOutWorkerList.Any(x => x.AssignmentStatus == 3) ? checkOutWorkerList.FirstOrDefault(x => x.AssignmentStatus == 3) : checkOutWorkerList.FirstOrDefault();
                    //    var postCheckOutTurnoverWorker = postCheckOutTurnoverWorkerList.Any(x => x.AssignmentStatus == 1) ? postCheckOutTurnoverWorkerList.FirstOrDefault(x => x.AssignmentStatus == 1) : postCheckOutTurnoverWorkerList.FirstOrDefault();

                    //    //var preCheckInTurnoverWorker = db.WorkerAssignments.FirstOrDefault(x => x.InquiryId == item.InquiryId && x.PropertyId == item.PropertyId && /*x.WorkerCategory == (int)WorkerCategory.TURNOVER && x.AssignmentStatus &&*/ x.StartDate < item.CheckInDate);
                    //    //var postCheckOutTurnoverWorker = db.WorkerAssignments.FirstOrDefault(x => x.InquiryId == item.InquiryId && x.PropertyId == item.PropertyId && /*x.WorkerCategory == (int)WorkerCategory.TURNOVER && x.AssignmentStatus &&*/ x.StartDate >= item.CheckOutDate);

                    //    AssignedWorkerInfo preCheckInTurnoverWorkerInfo = null;

                    //    if (preCheckInTurnoverWorker != null)
                    //    {
                    //        var workerInfo = db.Workers.FirstOrDefault(x => x.Id == preCheckInTurnoverWorker.WorkerId);
                    //        if (workerInfo != null)
                    //        {
                    //            preCheckInTurnoverWorkerInfo = new AssignedWorkerInfo();
                    //            preCheckInTurnoverWorkerInfo.FirstName = workerInfo.Firstname;
                    //            preCheckInTurnoverWorkerInfo.LastName = workerInfo.Lastname;
                    //            preCheckInTurnoverWorkerInfo.StartDate = preCheckInTurnoverWorker.StartDate;
                    //            preCheckInTurnoverWorkerInfo.EndDate = preCheckInTurnoverWorker.EndDate;
                    //            preCheckInTurnoverWorkerInfo.Mobile = workerInfo.Mobile;
                    //        }
                    //    }

                    //    AssignedWorkerInfo checkInWorkerInfo = null;

                    //    if (checkInWorker != null)
                    //    {
                    //        var workerInfo = db.Workers.FirstOrDefault(x => x.Id == checkInWorker.WorkerId);
                    //        if (workerInfo != null)
                    //        {
                    //            checkInWorkerInfo = new AssignedWorkerInfo();
                    //            checkInWorkerInfo.FirstName = workerInfo.Firstname;
                    //            checkInWorkerInfo.LastName = workerInfo.Lastname;
                    //            checkInWorkerInfo.StartDate = checkInWorker.StartDate;
                    //            checkInWorkerInfo.EndDate = checkInWorker.EndDate;
                    //            checkInWorkerInfo.Mobile = workerInfo.Mobile;
                    //        }
                    //    }

                    //    AssignedWorkerInfo checkOutWorkerInfo = null;

                    //    if (checkOutWorker != null)
                    //    {
                    //        var workerInfo = db.Workers.FirstOrDefault(x => x.Id == checkOutWorker.WorkerId);
                    //        if (workerInfo != null)
                    //        {
                    //            checkOutWorkerInfo = new AssignedWorkerInfo();
                    //            checkOutWorkerInfo.FirstName = workerInfo.Firstname;
                    //            checkOutWorkerInfo.LastName = workerInfo.Lastname;
                    //            checkOutWorkerInfo.StartDate = checkOutWorker.StartDate;
                    //            checkOutWorkerInfo.EndDate = checkOutWorker.EndDate;
                    //            checkOutWorkerInfo.Mobile = workerInfo.Mobile;
                    //        }
                    //    }

                    //    AssignedWorkerInfo postCheckOutTurnoverWorkerInfo = null;

                    //    if (postCheckOutTurnoverWorker != null)
                    //    {
                    //        var workerInfo = db.Workers.FirstOrDefault(x => x.Id == postCheckOutTurnoverWorker.WorkerId);
                    //        if (workerInfo != null)
                    //        {
                    //            postCheckOutTurnoverWorkerInfo = new AssignedWorkerInfo();
                    //            postCheckOutTurnoverWorkerInfo.FirstName = workerInfo.Firstname;
                    //            postCheckOutTurnoverWorkerInfo.LastName = workerInfo.Lastname;
                    //            postCheckOutTurnoverWorkerInfo.StartDate = postCheckOutTurnoverWorker.StartDate;
                    //            postCheckOutTurnoverWorkerInfo.EndDate = postCheckOutTurnoverWorker.EndDate;
                    //            postCheckOutTurnoverWorkerInfo.Mobile = workerInfo.Mobile;
                    //        }
                    //    }

                    //    var sortedNoteList = new List<Note>(db.tbNote.Where(x => x.ReservationId == item.Id));
                    //    List<TravelEventsViewModel> travelEventList = new List<TravelEventsViewModel>();

                    //    foreach (var note in sortedNoteList)
                    //    {
                    //        TravelEventsViewModel travelEvent = new TravelEventsViewModel
                    //        {
                    //            Id = note.Id,
                    //            ReservationId = note.ReservationId,
                    //            NoteType = note.NoteType,
                    //            InquiryNote = note.InquiryNote,
                    //            FlightNumber = note.FlightNumber,
                    //            ArrivalDateTime = note.ArrivalDateTime,
                    //            DepartureDateTime = note.DepartureDateTime,
                    //            DepartureCityCode = note.DepartureCityCode,
                    //            LeavingDate = note.LeavingDate,
                    //            ArrivalAirportCode = note.ArrivalAirportCode,
                    //            MeetingPlace = note.MeetingPlace,
                    //            MeetingDateTime = note.MeetingDateTime,
                    //            People = note.People,
                    //            Airline = note.Airline,
                    //            NumberOfPeople = note.NumberOfPeople,
                    //            CarType = note.CarType,
                    //            CarColor = note.CarColor,
                    //            PlateNumber = note.PlateNumber,
                    //            SourceCity = note.SourceCity,
                    //            CruiseName = note.CruiseName,
                    //            AirportCodeDeparture = note.AirportCodeDeparture,
                    //            AirportCodeArrival = note.AirportCodeArrival,
                    //            IdentifiableFeature = note.IdentifiableFeature,
                    //            DepartureCity = note.DepartureCity,
                    //            DeparturePort = note.DeparturePort,
                    //            ArrivalPort = note.ArrivalPort,
                    //            Name = note.Name,
                    //            TerminalStation = note.TerminalStation,
                    //            ComparisonDate = travelEventsDepartureTypes.Contains(note.NoteType) ? note.DepartureDateTime : (note.NoteType == (int)NoteType.MEETING ? note.MeetingDateTime : note.ArrivalDateTime)
                    //        };

                    //        travelEventList.Add(travelEvent);
                    //    }

                    //    var sortedTravelEvents = new List<TravelEventsViewModel>(travelEventList.OrderBy(x => x.ComparisonDate));


                    //    TasksViewModel record = new TasksViewModel
                    //    {
                    //        InquiryTask = inquiryTasks,
                    //        TurnoverTask = turnoverTasks,
                    //        BookingTask = bookingTasks,
                    //        CheckInTask = checkInTasks,
                    //        CheckOutTask = checkOutTasks,
                    //        PropertyInfo = propertyInfo,
                    //        GuestInfo = guestInfo,
                    //        Inquiry = item,
                    //        TravelEvents = sortedTravelEvents,
                    //        AssignedWorker = assignedWorker,
                    //        PreCheckInTurnoverWorker = preCheckInTurnoverWorkerInfo,
                    //        CheckInWorker = checkInWorkerInfo,
                    //        CheckOutWorker = checkOutWorkerInfo,
                    //        PostCheckOutTurnoverWorker = postCheckOutTurnoverWorkerInfo
                    //    };
                    //    model.Add(record);
                    //}

                    if (GlobalVariables.UserWorkerId != 0)
                    {
                        var assignmentReservationIds = db.WorkerAssignments.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).Select(x => x.ReservationId).ToList();


                        foreach (var id in assignmentReservationIds)
                        {
                            model.RemoveAll(x => x.Inquiry.Id != id && !assignmentReservationIds.Contains(x.Inquiry.Id));
                        }
                    }

                }

                return Json(model =model.Where(x=>x.Inquiry.CheckInDate.Value.Year== selectedYear && x.Inquiry.CheckInDate.Value.Month==selectedMonth).OrderBy(x => x.Inquiry.CheckInDate).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UpdateInquiryTaskStatus(string requestData,string customTaskList, string confirmationCode)
        {
            int totalTaskCount = 0, completedTaskCount = 0, pendingTaskCount = 0;

            try
            {
                var requestModel = JsonConvert.DeserializeObject<List<InquiryTaskRequestModel>>(requestData);
                var customTaskListRequestModel = JsonConvert.DeserializeObject<List<InquiryCustomTaskRequestModel>>(customTaskList);

                using (var db = new ApplicationDbContext())
                {
                    foreach (var item in requestModel)
                    {
                        var record = db.InquiryTasks.FirstOrDefault(x => x.Id == item.Id && x.TaskId == item.TaskId);
                        if (record != null)
                        {
                            record.Status = item.TaskStatus;
                            record.DateUpdated = DateTime.Now;
                            db.Entry(record).State = EntityState.Modified;
                        }
                    }
                    db.SaveChanges();

                    foreach (var customTask in customTaskListRequestModel)
                    {
                        InquiryTask newCustomTask = new InquiryTask
                        {
                            CompanyId = SessionHandler.CompanyId,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now,
                            ConfirmationCode = confirmationCode,
                            TaskId = -1,
                            TemplateId = -1,
                            TaskTitle = customTask.TaskTitle,
                            TaskDescription = customTask.TaskDescription,
                            TemplateName = customTask.TemplateName,
                            Status = customTask.TaskStatus

                        };
                        db.InquiryTasks.Add(newCustomTask);
                    }
                    db.SaveChanges();

                    var inquiryTaskList = db.InquiryTasks.Where(x => x.ConfirmationCode == confirmationCode);
                    totalTaskCount = inquiryTaskList.Count();
                    completedTaskCount = inquiryTaskList.Count(x => x.Status == (int) TaskListStatus.COMPLETED);
                    pendingTaskCount = inquiryTaskList.Count(x => x.Status == (int)TaskListStatus.PENDING);

                }

                var response = new
                {
                    success = true,
                    TotalTaskCount = totalTaskCount,
                    CompletedTaskCount = completedTaskCount,
                    PendingTaskCount = pendingTaskCount
                };

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var response = new
                {
                    success = false,
                    TotalTaskCount = totalTaskCount,
                    CompletedTaskCount = completedTaskCount,
                    PendingTaskCount = pendingTaskCount
                };

                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteInquiryTask(int inquiryTaskId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var entity = db.InquiryTasks.FirstOrDefault(x => x.Id == inquiryTaskId);
                    db.Entry(entity).State = EntityState.Deleted;
                    db.SaveChanges();
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetAvailableWorkers(string inquiryId, string dateSearch, int workerCategory, int workerAssignmentType)
        {
            ICommonRepository objCommon = new CommonRepository();
            var availableWorkerList = new List<MessagerSolution.Models.Worker.AvailableWorker2>();

            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                var availableWorkers = objCommon.GetAvailableWorkerList(inquiryId, dateSearch, workerCategory, workerAssignmentType);

                if (availableWorkers != null && availableWorkers.Any())
                {
                    foreach (var worker in availableWorkers)
                    {
                        var workerInfo = objCommon.GetWorker(worker.WorkerId);

                        MessagerSolution.Models.Worker.AvailableWorker2 workerObj = new MessagerSolution.Models.Worker.AvailableWorker2
                        {
                            Id = worker.Id, //table row ID (unique identifier to update status)
                            StartDate = worker.StartDate,
                            EndDate = worker.EndDate,
                            FirstName = workerInfo.Firstname,
                            LastName = workerInfo.Lastname,
                            Mobile = workerInfo.Mobile
                        };

                        availableWorkerList.Add(workerObj);
                    }
                }

                return Json(availableWorkerList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(availableWorkerList, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateTaskStatus(int Id,bool status)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.InquiryTasks.Where(x => x.Id == Id).FirstOrDefault();
                temp.Status = status?1:0;
                db.Entry(temp).State = EntityState.Modified;
                db.SaveChanges();

                var subTasks =db.InquirySubTask.Where(x => x.InquiryTaskId == temp.Id).ToList();
                foreach(var sub in subTasks)
                {
                    sub.UserId = User.Identity.Name.ToInt();
                    sub.LastUpdateDate = DateTime.Now;
                    sub.Status = status;
                    db.Entry(sub).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new {success = true}, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateSubTaskStatus(int Id, bool status)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.InquirySubTask.Where(x => x.Id == Id).FirstOrDefault();
                temp.Status = status;
                temp.UserId = User.Identity.Name.ToInt();
                temp.LastUpdateDate = DateTime.Now;
                db.Entry(temp).State = EntityState.Modified;
                db.SaveChanges();
                var tempTask = db.InquiryTasks.Where(x => x.Id == temp.InquiryTaskId).FirstOrDefault();
                var subtasks = db.InquirySubTask.Where(x => x.InquiryTaskId == tempTask.Id && x.Status==false).ToList();
                if (subtasks.Count == 0)
                {
                    tempTask.Status = 1;
                }
                else
                {
                    tempTask.Status = 0;
                }
                db.Entry(tempTask).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}