﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using MlkPwgen;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MessagerSolution.Controllers
{
    public class IncomeController : Controller
    {
        ICommonRepository objCommon = new CommonRepository();
        //
        // GET: /Income/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int companyId)
        {
            if (User.Identity.IsAuthenticated)
            {
                ICommonRepository objIList = new CommonRepository();
                if (!objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Income")
                    || !objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)))
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }

            List<Property> propertyList = new List<Property>();
            List<IncomeType> incomeTypeList = new List<IncomeType>();
            var propertyFilter = new List<Property>();
            var propertyForCreateIncome = new List<Property>();
            using (var db = new ApplicationDbContext())
            {
                incomeTypeList = db.IncomeTypes.Where(x => !x.IsByCompany || x.CompanyId == SessionHandler.CompanyId).ToList();

                propertyList = (from h in db.Hosts
                                join hc in db.HostCompanies on h.Id equals hc.HostId
                                join p in db.Properties on h.Id equals p.HostId
                                where hc.CompanyId == SessionHandler.CompanyId
                                select p).ToList();
                propertyList.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 0).ToList());
                
                foreach(var p in propertyList)
                {
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId== SessionHandler.CompanyId).FirstOrDefault();
                    if (parentChild == null)
                    {
                        propertyFilter.Add(p);
                    }
                }
                foreach (var p in propertyList)
                {
                    if((p.IsParentProperty && p.ParentType!= (int)Core.Enumerations.ParentPropertyType.Sync) || p.IsParentProperty==false)
                    {
                        propertyForCreateIncome.Add(p);
                    }
                }
            }
            ViewBag.PropertyListForFilter = propertyFilter;
            //ViewBag.PropertyList = propertyList;
            ViewBag.PropertyList = new SelectList(propertyForCreateIncome, "Id", "Name");
            ViewBag.IncomeTypeList = new SelectList(incomeTypeList, "Id", "TypeName");
            return View();
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetIncomeList(string propertyFilter, string monthYearFilter,bool includeChild,string paymentDate)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
              {
                    using (var db = new ApplicationDbContext())
                    {
                        var bookingList = db.Inquiries.ToList();
                        var guestList = db.Guests.ToList();
                        var siteTypeList = db.SiteType.ToList();
                        var propertyList = db.Properties.ToList();
                        List<IncomeList> il = new List<IncomeList>();

                        var income = db.Income.Join(db.IncomeTypes, x => x.IncomeTypeId, y => y.Id, (x, y) => new { Income = x, Income_type = y })
                        .Join(db.Properties, x => x.Income.PropertyId, y => y.Id, (x, y) => new { Income = x.Income, Income_type = x.Income_type, Property = y })
                        .Where(x => x.Income.CompanyId == SessionHandler.CompanyId)
                        .ToList();

                        var incomeWithoutBooking = db.Income.Join(db.IncomeTypes, x => x.IncomeTypeId, y => y.Id, (x, y) => new { Income = x, Income_type = y }).Where(x => x.Income.PropertyId == 0).Where(x => x.Income.CompanyId == SessionHandler.CompanyId).ToList() ;
                        string[] propertyFilters = null;
                        List<int> propertyIds = new List<int>();
                        try { propertyFilters = propertyFilter.Split(','); }
                        catch (Exception e) { propertyFilters = null; }
                        //JM 10/29/19 Get Child property of parent
                        foreach(var id in propertyFilters)
                        {
                            var pId = id.ToInt();
                            propertyIds.Add(pId);
                            var tempProperty = db.Properties.Where(x => x.IsParentProperty && x.Id==pId).FirstOrDefault();
                            if (tempProperty != null)
                            {
                                if (includeChild)
                                {
                                    propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == pId && x.CompanyId == SessionHandler.CompanyId).Select(x => x.ChildPropertyId).ToList());
                                }
                            }

                        }

                        if (propertyIds != null && !string.IsNullOrEmpty(propertyFilter) && propertyFilter != "0" && !propertyIds.Contains(0))
                        {
                            income = income.Where(x => propertyIds.Contains(x.Property.Id)).ToList();
                           incomeWithoutBooking.Clear();
                        }

                        income.ForEach(item =>
                        {
                            IncomeList temp = new IncomeList();
                            var creditsAmount = db.IncomePayment.Where(x => x.IncomeId == item.Income.Id && x.Type == (int)Core.Enumerations.IncomePaymentType.Credit && x.IsRejected == false).ToList().Sum(x => x.Amount);
                            var payments = db.IncomePayment.Where(x => x.IncomeId == item.Income.Id && x.IsRejected == false).ToList();
                            temp.Id = item.Income.Id;
                            temp.Amount = item.Income.Amount.ToDecimal() - creditsAmount;
                            temp.Description = item.Income.Description;
                            temp.IncomeType = item.Income_type.TypeName;
                            temp.DueDate = item.Income.DueDate.ToString("MMM d, yyyy");
                            temp.PaymentDate = payments.Count > 0 ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToDateTime().ToString("MMM d, yyyy") : "-"; //item.Income.DueDate.HasValue? item.Income.DueDate.ToDateTime().ToString("MMM d, yyyy") : "-";
                            var tempProperty = propertyList.Where(y => y.Id == item.Income.PropertyId).FirstOrDefault();
                            var syncParent = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.ParentType ==(int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                            temp.PropertyName = syncParent != null ? db.Properties.Where(x => x.Id == syncParent.ParentPropertyId).Select(x => x.Name).FirstOrDefault() : tempProperty.Name;
                            
                            temp.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                            var receivables = item.Income.Amount.ToDecimal();
                            temp.Receivables = receivables;
                            var tempInquiry = bookingList.SingleOrDefault(t => t.Id == item.Income.BookingId);
                            if (tempInquiry != null)
                            {
                                var tempGuest = guestList.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                                if (tempGuest != null) { temp.GuestName = tempGuest.Name; }
                                temp.BookingCode = tempInquiry.ConfirmationCode;
                                var expenseCategoryId = objCommon.GetBookingExpenseCategoryId("Booking Fee");
                                var expense = db.Expenses.Where(x => x.ExpenseCategoryId == expenseCategoryId && x.BookingId == tempInquiry.Id).FirstOrDefault();
                                if (expense != null)
                                {
                                    receivables = (item.Income.Amount - expense.Amount).ToDecimal();
                                    temp.Receivables = receivables;
                                   var summary= "<h2>Summary</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>"+ 
                                    "<tr>"+ "<td>"+item.Income_type.TypeName+"</td>" + "<td>"+item.Income.Description+"</td>" + "<td>"+item.Income.Amount+"</td>" + "</tr>"+
                                    "<tr>" + "<td>" + db.ExpenseCategories.Where(x=>x.Id==expenseCategoryId).FirstOrDefault().ExpenseCategoryType+ "</td>" + "<td>" + expense.Description + "</td>" + "<td>" + expense.Amount + "</td>" + "</tr>";
                                    //var js = new JavaScriptSerializer();
                                    temp.ReceivablesSummary = summary + "</tbody></table>";// js.Serialize();
                                }
                            }
                            temp.Receivables = receivables;
                            temp.Status = payments.Where(x => !x.IsRejected).Sum(x => x.Amount) == 0.0M ? "" : payments.Where(x => !x.IsRejected).Sum(x => x.Amount) == item.Income.Amount ? "Paid" : payments.Where(x => !x.IsRejected).Sum(x => x.Amount) > item.Income.Amount ? "Over Paid" : (payments.Where(x => !x.IsRejected).Sum(x => x.Amount) != item.Income.Amount && payments.Where(x => !x.IsRejected).Sum(x => x.Amount) != 0) ? "Partial" : "Unpaid";
                            temp.Balance = Math.Abs(receivables - payments.Where(x => !x.IsRejected).Sum(x => x.Amount));
                            temp.Actions = "<a class=\"ui mini button edit-income-btn\" title=\"Edit\">" +
                                            "<i class=\"icon edit\"></i>" +
                                            "</a>" +
                                            "<a class=\"ui mini button delete-income-btn\" title=\"Delete\">" +
                                                "<i class=\"icon ban\"></i>" +
                                            "</a>" + "<a class=\"ui mini button add-income-payment-btn\" title=\"Add Payment\">" +
                                                "<i class=\"dollar icon\"></i>" +
                                            "</a>";
                            il.Add(temp);
                        });
                        incomeWithoutBooking.ForEach(item =>
                        {
                            IncomeList temp = new IncomeList();
                            var creditsAmount = db.IncomePayment.Where(x => x.IncomeId == item.Income.Id && x.Type == (int)Core.Enumerations.IncomePaymentType.Credit && x.IsRejected == false).ToList().Sum(x => x.Amount);
                            var payments = db.IncomePayment.Where(x => x.IncomeId == item.Income.Id).ToList();
                            temp.Id = item.Income.Id;
                            temp.Amount = item.Income.Amount.ToDecimal() - creditsAmount;
                            temp.Description = item.Income.Description;
                            temp.IncomeType = item.Income_type.TypeName;
                            temp.DueDate = item.Income.DueDate.ToString("MMM d, yyyy");
                            temp.PaymentDate = payments.Count > 0 ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToDateTime().ToString("MMM d, yyyy") : "-"; //item.Income.DueDate.HasValue? item.Income.DueDate.ToDateTime().ToString("MMM d, yyyy") : "-";
                            temp.PropertyName= "-";
                            temp.Status = payments.Where(x => !x.IsRejected).Sum(x => x.Amount) == 0.0M ? "": payments.Where(x => !x.IsRejected).Sum(x => x.Amount) == item.Income.Amount ? "Paid" : payments.Where(x=>!x.IsRejected).Sum(x => x.Amount) > item.Income.Amount ? "Over Paid" : (payments.Where(x => !x.IsRejected).Sum(x => x.Amount) != item.Income.Amount && payments.Where(x => !x.IsRejected).Sum(x => x.Amount) != 0) ? "Partial" : "Unpaid";
                            temp.SiteType = "-";//((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                            var receivables = item.Income.Amount.ToDecimal();
                            temp.Receivables = receivables;
                            var tempInquiry = bookingList.SingleOrDefault(t => t.Id == item.Income.BookingId);
                            if (tempInquiry != null)
                            {
                                var tempGuest = guestList.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                                if (tempGuest != null) { temp.GuestName = tempGuest.Name; }
                                temp.BookingCode = tempInquiry.ConfirmationCode;
                                var expenseCategoryId = objCommon.GetBookingExpenseCategoryId("Booking Fee");
                                var expense = db.Expenses.Where(x => x.ExpenseCategoryId == expenseCategoryId && x.BookingId == tempInquiry.Id).FirstOrDefault();
                                if (expense != null)
                                {
                                    receivables = (item.Income.Amount - expense.Amount).ToDecimal();
                                    temp.Receivables = receivables;
                                    var summary = "<h2>Summary</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>" +
                                     "<tr>" + "<td>" + item.Income_type.TypeName + "</td>" + "<td>" + item.Income.Description + "</td>" + "<td>" + item.Income.Amount + "</td>" + "</tr>" +
                                     "<tr>" + "<td>" + db.ExpenseCategories.Where(x => x.Id == expenseCategoryId).FirstOrDefault().ExpenseCategoryType + "</td>" + "<td>" + expense.Description + "</td>" + "<td>" + expense.Amount + "</td>" + "</tr>";
                                    var js = new JavaScriptSerializer();
                                    temp.ReceivablesSummary = js.Serialize(summary + "</tbody></table>");
                                }
                            }
                            temp.Status = payments.Sum(x => x.Amount) == 0.0M ? "" : payments.Sum(x => x.Amount) == receivables ? "Paid" : payments.Sum(x => x.Amount) > receivables ? "Over Paid" : (payments.Sum(x => x.Amount) != receivables && payments.Sum(x => x.Amount) != 0) ? "Partial" : "Unpaid";
                            temp.Balance = Math.Abs(receivables- payments.Where(x => !x.IsRejected).Sum(x => x.Amount));
                            temp.Actions = "<a class=\"ui mini button edit-income-btn\" title=\"Edit\">" +
                                            "<i class=\"icon edit\"></i>" +
                                            "</a>" +
                                            "<a class=\"ui mini button delete-income-btn\" title=\"Delete\">" +
                                                "<i class=\"icon ban\"></i>" +
                                            "</a>" + "<a class=\"ui mini button add-income-payment-btn\" title=\"Add Payment\">" +
                                                "<i class=\"dollar icon\"></i>" +
                                            "</a>";
                            il.Add(temp);
                        });

                        if (!string.IsNullOrEmpty(monthYearFilter))
                        {
                            var dateFilter = Convert.ToDateTime(monthYearFilter);
                            il = il.Where(x => x.DueDate.ToDateTime().Year == dateFilter.Year && x.DueDate.ToDateTime().Month == dateFilter.Month).ToList();
                        }
                        if (!string.IsNullOrEmpty(paymentDate))
                        {
                            var dateFilter = Convert.ToDateTime(paymentDate);
                            il = il.Where(x => x.PaymentDate.ToDateTime().Year == dateFilter.Year && x.PaymentDate.ToDateTime().Month == dateFilter.Month).ToList();

                        }
                        var jsonData =Json(new { data = il.OrderByDescending(x => x.DueDate.ToDateTime()).ToList() }, JsonRequestBehavior.AllowGet);
                        jsonData.MaxJsonLength = int.MaxValue;
                        return jsonData;
                    }
                }
                catch(Exception e)
                { 
                    return Json(new { data = "" }, JsonRequestBehavior.AllowGet); 
                }
            }
            else
            {
                return HttpNotFound();
            }
        }
        [HttpPost]
        public ActionResult UpdatePaymentStatus(int Id,bool status)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.IncomePayment.Where(x => x.Id == Id).FirstOrDefault();
                if (temp != null)
                {
                    temp.IsRejected = status;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIncomeDetails(int Id)
        {

            using (var db = new ApplicationDbContext())
            {
                var income = db.Income.Find(Id);
                var incomePaid = db.IncomePayment.Where(x => x.IncomeId == Id).ToList();
                return Json(new { success = true, income = income, income_paid = incomePaid, message = "Income Details Load" }, JsonRequestBehavior.AllowGet);

            }


        }
        [HttpPost]
        public ActionResult AddPayment(IncomePayment payment, HttpPostedFileWrapper[] Images)
        {
            using (var db = new ApplicationDbContext())
            {
                payment.Type = (int)Core.Enumerations.IncomePaymentType.Payment;
                db.IncomePayment.Add(payment);
                db.SaveChanges();
                if (Images != null)
                {
                    foreach (var image in Images)
                    {
                        db.IncomePaymentFiles.Add(new IncomePaymentFile() {PaymentId =payment.Id , FileUrl = UploadImage(image, "~/Images/IncomePayments/") });
                        db.SaveChanges();
                    }
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");
        }
        [HttpPost]
        public ActionResult DeletePayment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var payment = db.IncomePayment.Where(x => x.Id == id).FirstOrDefault();
                db.IncomePayment.Remove(payment);
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        // GET: /Income/Details/
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Details(int id)
        {
            bool isSuccess = false;
            Income income = new Income();
            try
            {
                int companyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    income = db.Income.Where(i => i.CompanyId == companyId && i.Id == id).FirstOrDefault();
                    if (income != null) isSuccess = true;
                    var Files = db.IncomeFiles.Where(x => x.IncomeId == income.Id).ToList();
                    return Json(new { success = isSuccess, income = income, Files }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            { 
            }
            return Json(new { success = isSuccess}, JsonRequestBehavior.AllowGet);
        }

        // POST: /Income/Create
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Create(Income income, HttpPostedFileWrapper[] Images)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            if (SessionHandler.CompanyId != 0)
            {
                if (objCommon.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSaved = false;
            int companyId = SessionHandler.CompanyId;
            income.DateRecorded = DateTime.Now;
            income.CompanyId = companyId;
            if (companyId != 0)
            {
                isSaved = objCommon.AddIncome(income,Images);

            }

            return Json(new { success = isSaved }, JsonRequestBehavior.AllowGet);
        }

        // POST: /Income/Edit/
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Edit(Income income, HttpPostedFileWrapper[] Images,string fileIds)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objCommon.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            using (var db = new ApplicationDbContext())
            {
                var files = db.IncomeFiles.Where(x => x.IncomeId == income.Id).ToList();
                var Ids = JsonConvert.DeserializeObject<List<int>>(fileIds);
                files = files.Where(x => !Ids.Contains(x.Id)).ToList();
                db.IncomeFiles.RemoveRange(files);
                db.SaveChanges();
            }
                var isSaved = objCommon.EditIncome(income, Images);
            
            return Json(new { success = isSaved }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditTransaction(Income income)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = db.Income.Where(x => x.Id == income.Id).FirstOrDefault();
                if (temp != null)
                {
                    temp.Description = income.Description;
                    temp.Amount = income.Amount;
                    temp.DueDate = income.DueDate;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { success = true,incomeId=temp.Id }, JsonRequestBehavior.AllowGet);
            }
        }
        // POST: /Income/Delete/
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Delete(int id)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objCommon.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSaved = false;
            if (id != 0)
            {
                isSaved = objCommon.DeleteIncome(id);
            }
            return Json(new { success = isSaved }, JsonRequestBehavior.AllowGet);
        }
        // POST: /Income/Generate/
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Generate(string propertyFilter, string monthYearFilter)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var bookingList = (from h in db.Hosts
                                       join hc in db.HostCompanies on h.Id equals hc.HostId
                                       join i in db.Inquiries on h.Id equals i.HostId
                                       where hc.CompanyId == SessionHandler.CompanyId && i.BookingStatusCode.ToUpper() == "A"
                                       select i).ToList();
                    //var bookingList = db.Inquiries
                    //    .Where(x => x.BookingStatusCode.ToUpper() == "A")
                    //    .Where(x => x.CompanyId == SessionHandler.CompanyId)
                    //    .ToList();
                    if (propertyFilter != "all")
                    {
                        var p = int.Parse(propertyFilter);
                        var propertyId = Core.API.Airbnb.Properties.GetAirbnbId(propertyFilter.ToInt());
                        bookingList = bookingList.Where(x => x.PropertyId == propertyId.ToInt()).ToList();
                    }
                    if (!string.IsNullOrEmpty(monthYearFilter))
                    {
                        var dateRange = Convert.ToDateTime(monthYearFilter);
                        bookingList = bookingList.Where(x => x.BookingConfirmDate.ToDateTime().Year == dateRange.Year && x.BookingConfirmDate.ToDateTime().Month == dateRange.Month).ToList();
                    }

                    var incomeList = db.Income.ToList();
                    bookingList.ForEach(x =>
                    {
                        if (incomeList.SingleOrDefault(t => t.BookingId == x.Id) == null)
                        {
                            Income i = new Income();
                            i.Amount = x.ReservationCost;
                            i.BookingId = x.Id;
                            i.CompanyId = 1;
                            i.DateRecorded = DateTime.Now;
                            i.Description = "Generated from airbnb booking";
                            i.IncomeTypeId = 1;
                            i.IsBatchLoad = true;
                            i.DueDate = x.BookingConfirmDate.ToDateTime();
                            i.PropertyId = x.PropertyId;
                            i.Status = 2;
                            objCommon.AddIncome(i);
                        }
                    });
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetIncomeTypeList()
        {
            List<IncomeTypeViewModel> data = new List<IncomeTypeViewModel>();
            try
            { 
                using (var db = new ApplicationDbContext())
                {
                    var IncomeTypes = db.IncomeTypes.Where(x => !x.IsByCompany || x.CompanyId == SessionHandler.CompanyId).ToList();
                    foreach(var incomeType in IncomeTypes)
                    {
                        data.Add(new IncomeTypeViewModel() {
                        Id = incomeType.Id,
                        Name =incomeType.TypeName,
                        Type = (incomeType.IsByCompany? "Private" : "Public"),
                        Actions = (incomeType.IsByCompany ? "<button class=\"ui mini button income-type-edit\" data-tooltip=\"Edit\">" +
                                        "<i class=\"icon edit\"></i></button><button class=\"ui mini button income-type-delete\" data-tooltip=\"Delete\">" +
                                        "<i class=\"icon trash\"></i></button>" : ""),
                        });
                    }
                    return Json(new { data }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetIncomeTypeDetails(int id) {
            using (var db = new ApplicationDbContext())
            {
                var incomeType = db.IncomeTypes.Where(x => x.Id == id).FirstOrDefault();
                return Json(new { incomeType }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveIncomeType(IncomeType incomeType)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    if (incomeType.Id == 0)
                    {
                        incomeType.CompanyId = SessionHandler.CompanyId;
                        db.IncomeTypes.Add(incomeType);
                        db.SaveChanges();
                    }
                    else
                    {
                        var temp = db.IncomeTypes.Where(x => x.Id == incomeType.Id).FirstOrDefault();
                        if (temp != null)
                        {
                            temp.TypeName = incomeType.TypeName;
                            temp.CompanyTerm = incomeType.CompanyTerm;
                            db.SaveChanges();
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception e) {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetIncomeType()
        {
            using (var db = new ApplicationDbContext())
            {
                var incomeTypes = db.IncomeTypes.Where(x => !x.IsByCompany || x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new { incomeTypes }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteIncomeType(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var income = db.Income.Where(x => x.IncomeTypeId == id).FirstOrDefault();
                    if (income == null)
                    {
                        var temp = db.IncomeTypes.Where(x => x.Id == id).FirstOrDefault();
                        db.IncomeTypes.Remove(temp);
                        db.SaveChanges();
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet); ;
                }
            }catch(Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
