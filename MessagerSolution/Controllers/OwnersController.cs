﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Models;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class OwnersController : Controller
    {
        // GET: Owners
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                //ViewBag.RolePage = new CommonRepository().GetRolePageList(Convert.ToInt32(User.Identity.Name));
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult GetOwnerList()
        {
            using (var db = new ApplicationDbContext())
            {
                List<OwnerPropertiesModel> ownerProperties;
                var owners = (from user in db.Users join roleLink in db.tbRoleLink on user.Id equals roleLink.UserId join role in db.tbRole on roleLink.RoleId equals role.Id join ownerCompany in db.OwnerCompanies on user.Id equals ownerCompany.OwnerId where role.Id == 5 && ownerCompany.CompanyId == SessionHandler.CompanyId select user).ToList();
                if (owners.Count != 0)
                {
                    ownerProperties = new List<OwnerPropertiesModel>();
                    foreach (var owner in owners)
                    {
                        var p = string.Join("<br>", (from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId where ownerproperty.OwnerId == owner.Id && property.CompanyId == SessionHandler.CompanyId select property.Name).ToList()) +
                            "<br>" + string.Join("<br>", (from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId join host in db.Hosts on property.HostId equals host.Id join hostCompany in db.HostCompanies on host.Id equals hostCompany.HostId where ownerproperty.OwnerId == owner.Id && hostCompany.CompanyId == SessionHandler.CompanyId select property.Name).ToList());
                        ownerProperties.Add(new OwnerPropertiesModel() { OwnerId = owner.Id, Username = owner.UserName, Name = owner.FirstName + " " + owner.LastName, Properties = p });
                    }
                    return Json(new { data = ownerProperties }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { data = new List<OwnerPropertiesModel>() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetProperties()
        {
            using (var db = new ApplicationDbContext())
            {
                List<ChildPropertyModel> properties = new List<ChildPropertyModel>();
                int companyId = SessionHandler.CompanyId;
                var availableProperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where p.IsParentProperty == false && hc.CompanyId == companyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();
                foreach (var p in availableProperties)
                {
                    properties.Add(new ChildPropertyModel()
                    {
                        Host = p.Host,
                        IsSyncProperty = p.IsSyncParent,
                        SiteType = p.SiteType,
                        Id = p.Id,
                        Name = p.Name

                    });
                }
                var localchildProperty = db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach (var p in localchildProperty)
                {
                    properties.Add(new ChildPropertyModel()
                    {
                        Host = "",
                        IsSyncProperty = p.IsSyncParent,
                        SiteType = p.SiteType,
                        Id = p.Id,
                        Name = p.Name,
                        IsParent = p.IsParentProperty
                    });
                }
                return Json(new { properties }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult RegisterOwner(UserRoleModel owner, HttpPostedFileWrapper Image)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    ICommonRepository objCommon = new CommonRepository();
                    //encrypting the password for security
                    string userPassword = owner.Password;
                    owner.Password = owner.Password == null ? "" : Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], owner.Password.Trim(), true);
                    //checking the the already existing or not.

                    //New Owner
                    if (owner.Id == 0)
                    {
                        var resultValid = objCommon.ValidUser(owner.UserName);
                        if (resultValid != null)
                        {
                            var user = db.Users.Where(x => x.UserName == owner.UserName).FirstOrDefault();
                            db.OwnerCompanies.Add(new OwnerCompany() { OwnerId = user.Id, CompanyId = SessionHandler.CompanyId });
                            //Get all properties connected to host to remove all properties of company on current user
                            var companyProperties = (from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId where ownerproperty.OwnerId == user.Id && property.CompanyId == SessionHandler.CompanyId select property).ToList();
                            companyProperties.AddRange((from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId join host in db.Hosts on property.HostId equals host.Id join hostCompany in db.HostCompanies on host.Id equals hostCompany.HostId where ownerproperty.OwnerId == user.Id && hostCompany.CompanyId == SessionHandler.CompanyId select property).ToList());
                            var ownerPropertyIds = companyProperties.Select(x => x.Id).ToList();
                            db.SaveChanges();
                            var propertIds = owner.PropertyIds == null ? new List<int>() : owner.PropertyIds.Split(',').Select(Int32.Parse).ToList();
                            foreach (var propertyId in propertIds)
                            {
                                var temp = db.OwnerProperties.Where(x => x.OwnerId == user.Id && x.PropertyId == propertyId).FirstOrDefault();
                                if (temp == null)
                                {
                                    db.OwnerProperties.Add(new OwnerProperty() { OwnerId = user.Id, PropertyId = propertyId });
                                    db.SaveChanges();
                                }
                                else
                                {
                                    ownerPropertyIds.Remove(propertyId);
                                }
                            }
                            if (ownerPropertyIds.Count > 0)
                            {
                                var removeProperty = db.OwnerProperties.Where(x => ownerPropertyIds.Contains(x.PropertyId) && x.OwnerId == user.Id).ToList();
                                db.OwnerProperties.RemoveRange(removeProperty);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            User objtbUser = new User();
                            objtbUser.FirstName = owner.FirstName;
                            objtbUser.LastName = owner.LastName;
                            objtbUser.Password = owner.Password;
                            objtbUser.TimeZone = owner.TimeZone;
                            objtbUser.UserName = owner.UserName;
                            objtbUser.ValidStatus = owner.ValidStatus;
                            objtbUser.Status = owner.Status;
                            objtbUser.ConfirmEmail = owner.ConfirmEmail;
                            objtbUser.CompanyId = 0;//owner.CompanyId;
                            objtbUser.Status = true;
                            owner.ValidStatus = false;
                            objtbUser.CreatedDate = DateTime.Now;
                            if (Image != null)
                            {
                                objtbUser.ImageUrl = UploadImage(Image, "~/Images/User/");
                            }
                            //saving the detail of register user into db
                            var result = objCommon.SaveUser(objtbUser);
                            //inserting role of user into rolelink table
                            tbRoleLink objRoleLink = new tbRoleLink();
                            objRoleLink.RoleId = 5;
                            objRoleLink.UserId = result.Id;
                            objCommon.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);
                            var propertIds = owner.PropertyIds == null ? new List<int>() : owner.PropertyIds.Split(',').Select(Int32.Parse).ToList();
                            db.OwnerCompanies.Add(new OwnerCompany() { OwnerId = result.Id, CompanyId = SessionHandler.CompanyId });
                            db.SaveChanges();
                            foreach (var propertyId in propertIds)
                            {
                                db.OwnerProperties.Add(new OwnerProperty() { OwnerId = result.Id, PropertyId = propertyId });
                                db.SaveChanges();
                            }
                            if (result != null)
                            {
                                //Helper.EmailHandler objSendEmail = new Helper.EmailHandler();
                                //objSendEmail.mailTo = owner.UserName.Trim();
                                //objSendEmail.mailSubject = "Respontage.com";
                                //objSendEmail.mailBody = "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                //                        HttpContext.Request.Url.Scheme,
                                //                        HttpContext.Request.ServerVariables["HTTP_HOST"],
                                //                        (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                //                        ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>link</a> to verify your email. </p><p>Here is your temporary password - <span style=\"color: #f95c41\">" + userPassword + "</span>.</p></div>";
                                //objSendEmail.SendEmail();
                                Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                                            HttpContext.Request.Url.Scheme,
                                                            HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                            (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                            ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>link</a> to verify your email. </p><p>Here is your temporary password - <span style=\"color: #f95c41\">" + userPassword + "</span>.</p></div>", owner.UserName.Trim(), "Respontage.com");
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        var user = db.Users.Where(x => x.Id == owner.Id).FirstOrDefault();
                        user.FirstName = owner.FirstName;
                        user.LastName = owner.LastName;
                        if (owner.Password != null && !string.IsNullOrEmpty(owner.Password) && !string.IsNullOrWhiteSpace(owner.Password))
                        {
                            Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                                               HttpContext.Request.Url.Scheme,
                                                               HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                               (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                               ) + "/Home/ValidateAccount?username=" + user.Id + "'>link</a> to verify your email. </p><p>Here is your temporary password - <span style=\"color: #f95c41\">" + userPassword + "</span>.</p></div>", owner.UserName.Trim(), "Respontage.com");
                            user.Password = owner.Password;
                        }
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        //Get all properties connected to host to remove all properties of company on current user
                        var companyProperties = (from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId where ownerproperty.OwnerId == user.Id && property.CompanyId == SessionHandler.CompanyId select property).ToList();
                        companyProperties.AddRange((from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId join host in db.Hosts on property.HostId equals host.Id join hostCompany in db.HostCompanies on host.Id equals hostCompany.HostId where ownerproperty.OwnerId == user.Id && hostCompany.CompanyId == SessionHandler.CompanyId select property).ToList());
                        var ownerPropertyIds = companyProperties.Select(x => x.Id).ToList();
                        db.SaveChanges();
                        var propertIds = owner.PropertyIds == null ? new List<int>() : owner.PropertyIds.Split(',').Select(Int32.Parse).ToList();
                        foreach (var propertyId in propertIds)
                        {
                            var temp = db.OwnerProperties.Where(x => x.OwnerId == user.Id && x.PropertyId == propertyId).FirstOrDefault();
                            if (temp == null)
                            {
                                db.OwnerProperties.Add(new OwnerProperty() { OwnerId = user.Id, PropertyId = propertyId });
                                db.SaveChanges();
                            }
                            else
                            {
                                ownerPropertyIds.Remove(propertyId);
                            }
                        }
                        if (ownerPropertyIds.Count > 0)
                        {
                            var removeProperty = db.OwnerProperties.Where(x => ownerPropertyIds.Contains(x.PropertyId) && x.OwnerId == user.Id).ToList();
                            db.OwnerProperties.RemoveRange(removeProperty);
                            db.SaveChanges();
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpGet]
        public ActionResult GetOwnerProperties(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                List<ChildPropertyModel> properties = new List<ChildPropertyModel>();
                List<ChildPropertyModel> notConnectedProperties = new List<ChildPropertyModel>();
                var owner = db.Users.Where(x => x.Id == id).FirstOrDefault();
                var ownerProperties = (from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId where ownerproperty.OwnerId == id where property.CompanyId == SessionHandler.CompanyId select property).ToList();
                ownerProperties.AddRange((from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId join host in db.Hosts on property.HostId equals host.Id join hostCompany in db.HostCompanies on host.Id equals hostCompany.HostId where ownerproperty.OwnerId == owner.Id && hostCompany.CompanyId == SessionHandler.CompanyId select property).ToList());
                var ownerpropertyIds = ownerProperties.Select(x => x.Id).ToList();
                foreach (var p in ownerProperties)
                {
                    var host = db.Hosts.Where(x => x.Id == p.HostId).FirstOrDefault();
                    properties.Add(new ChildPropertyModel()
                    {
                        Host = p.HostId == 0 ? "" : host.Username + " (" + host.Firstname + " " + host.Lastname + ")",
                        IsSyncProperty = p.IsSyncParent,
                        SiteType = p.SiteType,
                        Id = p.Id,
                        Name = p.Name,
                        IsParent = p.IsParentProperty
                    });
                }

                var availableProperties = (from p in db.Properties join h in db.Hosts on p.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where p.IsParentProperty == false && hc.CompanyId == SessionHandler.CompanyId select new { Id = p.Id, Name = p.Name, SiteType = p.SiteType, IsSyncParent = p.IsSyncParent, Host = h.Username + " (" + h.Firstname + " " + h.Lastname + ")" }).ToList();
                foreach (var p in availableProperties)
                {
                    notConnectedProperties.Add(new ChildPropertyModel()
                    {
                        Host = p.Host,
                        IsSyncProperty = p.IsSyncParent,
                        SiteType = p.SiteType,
                        Id = p.Id,
                        Name = p.Name,
                    });
                }
                var localchildProperty = db.Properties.Where(x => x.SiteType == 0 && x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach (var p in localchildProperty)
                {
                    notConnectedProperties.Add(new ChildPropertyModel()
                    {
                        Host = "",
                        IsSyncProperty = p.IsSyncParent,
                        SiteType = p.SiteType,
                        Id = p.Id,
                        Name = p.Name,
                        IsParent = p.IsParentProperty
                    });
                }
                notConnectedProperties = notConnectedProperties.Where(x => !ownerpropertyIds.Contains(x.Id)).ToList();
                return Json(new { owner, properties, notConnectedProperties }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }
    }
}