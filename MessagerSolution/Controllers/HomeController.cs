﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MessagerSolution.Models;
using MessagerSolution.Helper;
using System.Web.Security;
using System.Diagnostics;
using System.Threading.Tasks;
using MessagerSolution.Models.Worker;
using NLog;
using Core;
using Core.Helper;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Models;
using Core.Repository;
using Newtonsoft.Json.Linq;
using System.Data.Entity;
using System.Net.Http;
using Core.SignalR.Hubs;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using MlkPwgen;
using MessagerSolution.JobScheduler;
using Twilio;
using System.Configuration;
using Twilio.Rest.Api.V2010.Account;
using System.Device.Location;
using Core.Enumerations;
using System.Security.Cryptography;
using Microsoft.Ajax.Utilities;
using Core.Models.MultiMedia;
using Wyze;
using MessagerSolution.Helper.PropertyOwner;
using MessagerSolution.Helper.SignRequestSettings;
using MessagerSolution.Tools;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;
using MessagerSolution.Models.Income;
using Spire.DataExport.PropEditors;
using MessagerSolution.Helper.Renters;
using MessagerSolution.WebServices;
using System.Security.Principal;

namespace MessagerSolution.Controllers
{
    public class HomeController : Controller
    {

        ICommonRepository objIList = new CommonRepository();
        Logger logger = LogManager.GetCurrentClassLogger();

        private readonly List<int> travelEventsDepartureTypes = new List<int>
        {
            (int) Core.Enumerations.NoteType.FLIGHT_DEPARTURE,
            (int) Core.Enumerations.NoteType.BUS_DEPARTURE,
            (int) Core.Enumerations.NoteType.CRUISE_DEPARTURE,
            (int) Core.Enumerations.NoteType.TRAIN_DEPARTURE,
            (int) Core.Enumerations.NoteType.DRIVE_DEPARTURE
        };

        [HttpGet]
        public ActionResult CheckSession()
        {
            bool isTimeout = false;
            if (!User.Identity.IsAuthenticated || User.Identity.Name == "")
            {
                FormsAuthentication.SignOut();
                SessionHandler.IsSessionExpired = true;
                isTimeout = true;
            }
            return Json(new { isTimeout, Url = Url.Action("Index", "Home") }, JsonRequestBehavior.AllowGet);
        }

        #region Guest User Code
        #region payment Square
        //[HttpPost]
        //public ActionResult SendPaymentRequest(string guestEmail, string guestName, string bookId, string guestId)
        //{
        //    try
        //    {
        //        var path = Server.MapPath("~/EmailTemplates/PaymentRequestEmailTemplate.html");
        //        SendPaymentModel sendPaymentModel = new SendPaymentModel();
        //        string callbackUrl = "";
        //        using (var db = new ApplicationDbContext())
        //        {
        //            int id = int.Parse(bookId);
        //            var dataResult = db.Inquiries.Where(x => x.Id == id).FirstOrDefault();

        //            sendPaymentModel.Email = guestEmail;
        //            sendPaymentModel.Customer = guestName;
        //            sendPaymentModel.CheckIn = DateTime.Parse(dataResult.CheckInDate.ToString());
        //            sendPaymentModel.CheckOut = DateTime.Parse(dataResult.CheckOutDate.ToString());
        //            sendPaymentModel.ReservationId = dataResult.ReservationId;
        //            sendPaymentModel.No = dataResult.Id.ToString();
        //            sendPaymentModel.PaymentType = PaymentType.Square;

        //            //updating email detail into database//
        //            var dataGuest = db.Guests.Where(x => x.Email == guestEmail).FirstOrDefault();
        //            if (dataGuest != null)
        //            {
        //                dataResult.GuestId = dataGuest.Id;
        //                db.SaveChanges();
        //            }
        //            else
        //            {
        //                var data = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
        //                data.Email = guestEmail;
        //                data.FullName = guestName;
        //                data.ProfilePictureUrl = "https://a0.muscache.com/defaults/user_pic-225x225.png?v=3";
        //                db.SaveChanges();
        //            }
        //            callbackUrl = SendConfirmationEmail(sendPaymentModel, path);
        //        }
        //        return Json(new { success = true, message = callbackUrl }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false, message = ex.InnerException.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public string SendConfirmationEmail(SendPaymentModel sendPaymentModel, string path)
        //{
        //    Service.EmailService _emailService = new Service.EmailService();
        //    if (!System.IO.File.Exists(path)) return "";
        //    var template = System.IO.File.ReadAllText(path);

        //    var callbackUrl = "";

        //    if (sendPaymentModel.PaymentType == PaymentType.PayPal)
        //    {
        //        callbackUrl = Url.Action("Payment", "Home", new { id = sendPaymentModel.No }, protocol: Request.Url.Scheme);
        //    }
        //    else if (sendPaymentModel.PaymentType == PaymentType.Square)
        //    {
        //        callbackUrl = Url.Action("PaymentSquare", "Home", new { id = sendPaymentModel.No }, protocol: Request.Url.Scheme);
        //    }
        //    else if (sendPaymentModel.PaymentType == PaymentType.Stripe)
        //    {
        //        callbackUrl = Url.Action("PaymentStripe", "Home", new { id = sendPaymentModel.No }, protocol: Request.Url.Scheme);
        //    }

        //    template = template.Replace("@Name", sendPaymentModel.Customer)
        //            .Replace("@Code", sendPaymentModel.ReservationId)
        //            .Replace("@CheckIn", sendPaymentModel.CheckIn.ToString())
        //            .Replace("@CheckOut", sendPaymentModel.CheckOut.ToString())
        //            .Replace("@TotalAmount", sendPaymentModel.AmountDue)
        //            .Replace("@Link", callbackUrl);

        //    Helper.EmailHandler objSendEmail = new Helper.EmailHandler();
        //    objSendEmail.mailTo = sendPaymentModel.Email;
        //    objSendEmail.mailSubject = "Kendric's PropertyDB (Request To Pay Payment)";
        //    objSendEmail.mailBody = template;
        //    objSendEmail.SendEmail();
        //    return callbackUrl;
        //}

        //public ActionResult PaymentSquare(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Inquiry dataResult = new Inquiry();
        //    using (var db = new ApplicationDbContext())
        //    {
        //        int iId = int.Parse(id);
        //        dataResult = db.Inquiries.Where(x => x.Id == iId).FirstOrDefault();
        //    }
        //    if (dataResult == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    var squarePayment = new SquarePaymentModel();
        //    squarePayment.AmountDue = dataResult.NetRevenueAmount.ToString();
        //    squarePayment.No = dataResult.Id.ToString();
        //    squarePayment.ReservationId = dataResult.ReservationId;
        //    return View(squarePayment);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult PaymentSquare(SquarePaymentModel squarePayment)
        //{
        //    try
        //    {
        //        Kendric.POC.Service.SquarePaymentService _squarePaymentService = new Kendric.POC.Service.SquarePaymentService();
        //        var amountDue = Math.Round(Convert.ToDecimal(squarePayment.AmountDue), 2);
        //        var response = _squarePaymentService.ChargeCard(squarePayment.CreditCardNonce,
        //          Convert.ToInt64(amountDue));

        //        if (response != null)
        //        {
        //            int id = int.Parse(squarePayment.No);
        //            using (var db = new ApplicationDbContext())
        //            {
        //                var dataResult = db.Inquiries.Where(x => x.Id == id).FirstOrDefault();
        //                dataResult.BookingStatusCode = "B";
        //                dataResult.Paid = true;
        //                db.SaveChanges();
        //            }
        //            return RedirectToAction("PaymentSuccess", "Home", new { pid = id.ToString() });
        //        }
        //        else
        //        {
        //            return View("PaymentFail");
        //        }
        //        return View(squarePayment);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}



        #endregion

      
        public ActionResult GuestUserBooking(int? pid)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            //GetBaseRateDetail(1028, 2, 7, DateTime.UtcNow.AddDays(3));
            return View();
        }

        [HttpGet]
        public ActionResult GetBookedDate(int id) 
        {
            bool isSuccess = false;
            List<DateTime> allDates = new List<DateTime>();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<JobTypeTableData> job = new List<JobTypeTableData>();
                    var data = db.Properties.Where(x => x.ListingId == id).FirstOrDefault();
                    var parent = db.ParentChildProperties.Where(x => x.ChildPropertyId == data.Id && SessionHandler.CompanyId == x.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    var dataLocal = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parent.ParentPropertyId select p).ToList();
                    //var dataLocal = db.Properties.Where(x => x.ParentPropertyId == data.ParentPropertyId).ToList();
                    List<Int64> obj = new List<Int64>();
                    foreach (var item in dataLocal)
                    {
                        obj.Add(item.ListingId);
                    }
                    var dataBooking = db.Inquiries.Where(x => obj.Contains(x.PropertyId) && x.CheckOutDate > DateTime.UtcNow).ToList();
                    foreach (var item in dataBooking)
                    {
                        for (DateTime date = DateTime.Parse(item.CheckInDate.ToString()); date <= item.CheckOutDate; date = date.AddDays(1))
                            allDates.Add(date);
                    }
                    isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, worker = allDates }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddBooking(string checkedInDate, string checkedOutDate, string guestName, int guestCount, string propertyId)
        {
            Guest guest = new Guest();
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Guests.Where(x => x.Name.ToLower() == guestName.ToLower()).FirstOrDefault();
                if (temp == null)
                {
                    Guest g = new Guest
                    {
                        Name = guestName,
                        GuestId = Guid.NewGuid().ToString(),
                        ProfilePictureUrl = ""
                    };
                    guest = objIList.AddGuest(g);
                }
                else
                {
                    guest = temp;
                }
            }

            var guestLastname = guest.Name.Split(' ').Last();
            var guestFirstname = guest.Name.Replace(guestLastname, "");


            var threadId = Guid.NewGuid().ToString();
            var reservationId = Guid.NewGuid().ToString();
            var confirmationCode = Guid.NewGuid().ToString();

            var result = Core.API.AllSite.Properties.GetByListingId(Int64.Parse(propertyId));
            var hostId = result.HostId;
            var guest_Id = 0;
            using (var db = new ApplicationDbContext())
            {
                guest_Id = Core.API.AllSite.Guests.GetLocalGuestId(db, guest.GuestId);
            }
            //get total count//
            int intDay = Convert.ToInt32((DateTime.Parse(checkedOutDate) - DateTime.Parse(checkedInDate)).TotalDays);
            var intArrayList = GetBaseRateDetail(result.Id, guestCount, intDay, DateTime.Parse(checkedInDate), DateTime.Parse(checkedOutDate));

            Inquiry iq = new Inquiry
            {
                SiteType = 0,
                HostId = hostId,
                ConfirmationCode = confirmationCode,
                GuestCount = guestCount,
                PropertyId = int.Parse(propertyId),
                CheckInDate = DateTime.Parse(checkedInDate),
                CheckOutDate = DateTime.Parse(checkedOutDate),
                BookingDate = DateTime.UtcNow,
                InquiryDate = DateTime.UtcNow,
                ServiceFee = Convert.ToDecimal(intArrayList[2]),
                CleaningFee = Convert.ToDecimal(intArrayList[1]),
                BookingStatusCode = "I",
                GuestId = guest_Id,
                ThreadId = threadId,
                ReservationId = reservationId,
                BookingCancelDate = (DateTime?)null,
                BookingConfirmDate = (DateTime?)null,
                //CompanyId = result.CompanyId,
                //Total Amount//
                NetRevenueAmount = Convert.ToDecimal(intArrayList[0]),
                IsActive = true
            };

            int bookingId = objIList.AddBooking(iq);
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    Inbox objInbox = new Inbox();
                    objInbox.PropertyId = iq.PropertyId;
                    objInbox.ThreadId = threadId;
                    objInbox.HostId = 0;
                    objInbox.IsArchived = false;
                    objInbox.HasUnread = false;
                    objInbox.GuestId = iq.GuestId;
                    objInbox.LastMessageAt = DateTime.UtcNow;
                    objInbox.CheckInDate = iq.CheckInDate;
                    objInbox.CheckOutDate = iq.CheckOutDate;
                    objInbox.Status = "I";
                    objInbox.SiteType = 0;
                    //Total Amount//
                    objInbox.Subtotal = Convert.ToDecimal(intArrayList[0]);
                    objInbox.StatusType = 0;
                    objInbox.ServiceFee = Convert.ToDecimal(intArrayList[2]);
                    objInbox.RentalFee = 0;
                    objInbox.IsInquiryOnly = false;
                    objInbox.TotalPayout = 0;
                    //Total Amount//
                    objInbox.GuestPay = Convert.ToDecimal(intArrayList[0]);
                    //Total Amount//
                    objInbox.GuestFee = Convert.ToDecimal(intArrayList[0]);
                    //objInbox.CompanyId = iq.CompanyId;
                    objInbox.CanWithdrawPreApprovalInquiry = false;
                    objInbox.CanDeclineInquiry = false;
                    objInbox.CanPreApproveInquiry = false;
                    objInbox.CleaningFee = 0;
                    objInbox.IsSpecialOfferSent = false;
                    objInbox.GuestCount = iq.GuestCount;
                    objInbox.Adult = 0;
                    objInbox.Children = 0;
                    db.Inbox.Add(objInbox);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            List<string> objBook = new List<string>();
            objBook.Add(bookingId.ToString());
            objBook.Add(guest.GuestId);
            objBook.Add(iq.Id.ToString());
            return Json(new { success = true, message = objBook }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SendMessage(string pid, string gid)
        //{
        //    return View();
        //}

        public ActionResult SendPaymentToCustomer(string gId, string iId)
        {
            return View();
        }

        private List<int> GetBaseRateDetail(int propertyId, int guestNumber, int dayNumber, DateTime checkedInDate, DateTime checkedOutDate)
        {
            try
            {
                List<int> intArrayList = new List<int>();

                int? totalAmount = 0;
                using (var db = new ApplicationDbContext())
                {
                    var result = db.EarlyBirdDiscounts.Where(x => x.PropertyId == propertyId).ToList();

                    var result1 = db.LastminuteDiscount.Where(x => x.PropertyId == propertyId).ToList();

                    var result2 = db.StandardFee.Where(x => x.PropertyId == propertyId && x.IsStandard == true).ToList();

                    var result3 = db.StandardFee.Where(x => x.PropertyId == propertyId && x.IsStandard == false).ToList();

                    var result4 = db.PropertyBaseRates.Where(x => x.PropertyId == propertyId).FirstOrDefault();

                    var result5 = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault();

                    int? totalSFeeAmount = 0;
                    foreach (var item in result2)
                    {
                        if (item.FeeType == "StandardPerGuestPerStay")
                        {
                            if (item.IsTaxable)
                            {
                                if (result5.TaxRate != null)
                                {
                                    int totalTxS = ((item.Amount + Convert.ToInt32(result4.NightRate)) * guestNumber);
                                    int amount1 = (int)Math.Round((double)(totalTxS * Convert.ToInt32(result5.TaxRate)) / 100);
                                    totalSFeeAmount += amount1;
                                }
                                totalSFeeAmount += ((item.Amount + Convert.ToInt32(result4.NightRate)) * guestNumber);
                            }
                            else
                            {
                                totalSFeeAmount += ((item.Amount + Convert.ToInt32(result4.NightRate)) * guestNumber);
                            }
                        }
                        else
                        {
                            if (item.IsTaxable)
                            {
                                if (result5.TaxRate != null)
                                {
                                    int totalTxS = (Convert.ToInt32(result4.NightRate) * guestNumber * dayNumber);
                                    int amount1 = (int)Math.Round((double)(totalTxS * Convert.ToInt32(result5.TaxRate)) / 100);
                                    totalSFeeAmount += amount1;
                                }
                                totalSFeeAmount += (item.Amount * guestNumber * dayNumber);
                            }
                            else
                            {
                                totalSFeeAmount += (item.Amount * guestNumber * dayNumber);
                            }
                        }
                    }
                    int? totalCFeeAmount = 0;
                    foreach (var item in result3)
                    {
                        if (item.FeeType == "StandardPerGuestPerStay")
                        {
                            if (item.IsTaxable)
                            {
                                if (result5.TaxRate != null)
                                {
                                    int totalTxC = (item.Amount * guestNumber);
                                    int amount1 = (int)Math.Round((double)(totalTxC * Convert.ToInt32(result5.TaxRate)) / 100);
                                    totalCFeeAmount += amount1;
                                }
                                totalCFeeAmount += (item.Amount * guestNumber);
                            }
                            else
                            {
                                totalCFeeAmount += (item.Amount * guestNumber);
                            }
                        }
                        else
                        {
                            if (item.IsTaxable)
                            {
                                if (result5.TaxRate != null)
                                {
                                    int totalTxC = (item.Amount * guestNumber * dayNumber);
                                    int amount1 = (int)Math.Round((double)(totalTxC * Convert.ToInt32(result5.TaxRate)) / 100);
                                    totalCFeeAmount += amount1;
                                }
                                totalCFeeAmount += (item.Amount * guestNumber * dayNumber);
                            }
                            else
                            {
                                totalCFeeAmount += (item.Amount * guestNumber * dayNumber);
                            }
                        }
                    }

                    if (result4.IsCustomize == true)
                    {
                        int intDay = Convert.ToInt32((checkedOutDate - checkedInDate).TotalDays);
                        for (var i = 0; i < intDay; i++)
                        {
                            if (i == 0)
                            {
                                var name = checkedInDate.DayOfWeek.ToString();
                                if (name == "Sunday")
                                {
                                    totalAmount += result4.Sunday;
                                }
                                if (name == "Monday")
                                {
                                    totalAmount += result4.Monday;
                                }
                                if (name == "Tuesday")
                                {
                                    totalAmount += result4.Tuesday;
                                }
                                if (name == "Wednesday")
                                {
                                    totalAmount += result4.Wednesday;
                                }
                                if (name == "Thursday")
                                {
                                    totalAmount += result4.Thursday;
                                }
                                if (name == "Friday")
                                {
                                    totalAmount += result4.Friday;
                                }
                                if (name == "Saturday")
                                {
                                    totalAmount += result4.Saturday;
                                }
                            }
                            else
                            {
                                var name = checkedInDate.AddDays(i).DayOfWeek.ToString();
                                if (name == "Sunday")
                                {
                                    totalAmount += result4.Sunday;
                                }
                                if (name == "Monday")
                                {
                                    totalAmount += result4.Monday;
                                }
                                if (name == "Tuesday")
                                {
                                    totalAmount += result4.Tuesday;
                                }
                                if (name == "Wednesday")
                                {
                                    totalAmount += result4.Wednesday;
                                }
                                if (name == "Thursday")
                                {
                                    totalAmount += result4.Thursday;
                                }
                                if (name == "Friday")
                                {
                                    totalAmount += result4.Friday;
                                }
                                if (name == "Saturday")
                                {
                                    totalAmount += result4.Saturday;
                                }
                            }
                        }
                    }
                    else
                    {
                        totalAmount = result4.NightRate;
                    }

                    totalAmount += (totalCFeeAmount + totalSFeeAmount);

                    //Discount from total//
                    if (dayNumber >= 7 && dayNumber <= 28)
                    {
                        if (result5.BaseDiscountsWeekly != null && result5.BaseDiscountsWeekly != 0)
                        {
                            int amount1 = (int)Math.Round((double)(Convert.ToInt32(totalAmount) * Convert.ToInt32(result5.BaseDiscountsWeekly)) / 100);
                            totalAmount = (totalAmount - amount1);
                        }
                    }
                    if (dayNumber > 28)
                    {
                        if (result5.BaseDiscountsMonthly != null && result5.BaseDiscountsMonthly != 0)
                        {
                            int amount1 = (int)Math.Round((double)(Convert.ToInt32(totalAmount) * Convert.ToInt32(result5.BaseDiscountsMonthly)) / 100);
                            totalAmount = (totalAmount - amount1);
                        }
                    }
                    foreach (var item in result)
                    {
                        int intMonth = Convert.ToInt32((checkedInDate - DateTime.UtcNow).TotalDays);
                        int diffDay = (Convert.ToInt32(intMonth) / 28);
                        if (diffDay >= item.MonthNumber)
                        {
                            int amount1 = (int)Math.Round((double)(Convert.ToInt32(totalAmount) * Convert.ToInt32(item.Discount)) / 100);
                            totalAmount = (totalAmount - amount1);
                        }
                    }
                    foreach (var item in result1)
                    {
                        int intDay = Convert.ToInt32((checkedInDate - DateTime.UtcNow).Days);
                        if (intDay >= item.DayNumber)
                        {
                            int amount1 = (int)Math.Round((double)(Convert.ToInt32(totalAmount) * Convert.ToInt32(item.Discount)) / 100);
                            totalAmount = (totalAmount - amount1);
                        }
                    }

                    if (result5.TaxRate != null)
                    {
                        int amount1 = (int)Math.Round((double)(totalAmount * Convert.ToInt32(result5.TaxRate)) / 100);
                        totalAmount += amount1;
                    }
                    //Total Amount is collecting//
                    intArrayList.Add(Convert.ToInt32(totalAmount));
                    intArrayList.Add(Convert.ToInt32(totalSFeeAmount));
                    intArrayList.Add(Convert.ToInt32(totalCFeeAmount));
                }
                return intArrayList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult PaymentSuccess(string pid)
        {
            try
            {
                int id = int.Parse(pid);
                Guest guests = new Guest();
                using (var db = new ApplicationDbContext())
                {
                    var dataResult = db.Inquiries.Where(x => x.Id == id).FirstOrDefault();
                    guests = db.Guests.Where(x => x.Id == dataResult.GuestId).FirstOrDefault();
                }
                return View(guests);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult SaveGuestInfo(string GuestId, string GuestEmail, string GuestPassword)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    int id = int.Parse(GuestId);
                    var guests = db.Guests.Where(x => x.Id == id).FirstOrDefault();
                    guests.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], GuestPassword.Trim(), true);
                    guests.GuestType = "Local";
                    guests.Email = GuestEmail;
                    db.SaveChanges();
                }
                return Json(new { success = true, message = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoginGuestInfo(string GuestEmail, string GuestPassword)
        {
            try
            {
                GlobalVariables.Username = GuestEmail;
                GlobalVariables.Password = GuestPassword;
                ICommonRepository objCommon = new CommonRepository();
                //encrypting the password for security
                GuestPassword = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], GuestPassword.Trim(), true);
                Guest guests = new Guest();
                using (var db = new ApplicationDbContext())
                {
                    guests = db.Guests.Where(x => x.Email == GuestEmail && x.Password == GuestPassword).FirstOrDefault();
                }
                if (guests != null)
                {
                    FormsAuthenticationTicket(guests.Id, "Guest");
                    GlobalVariables.UserId = guests.Id;
                    SessionHandler.IsSessionExpired = false;
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PaymentFail()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetBookedDetail(string guestId, int bookId)
        {
            bool isSuccess = false;
            Inquiry dataResult = new Inquiry();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var data = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
                    dataResult = db.Inquiries.Where(x => x.Id == bookId).FirstOrDefault();
                    dataResult.Type = data.Name;
                    isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, ResultBook = dataResult }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PropertyDetail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new ApplicationDbContext())
            {
                var result = db.PropertyCategory.Where(x => x.PropertyId == id).Select(x => x.CategoryName).Distinct().ToList();
                ViewBag.PropertyCategoryList = result;

                var result1 = db.PropertyCategory.Where(x => x.PropertyId == id).ToList();
                ViewBag.PropertyCategoryDetailList = result1;

                var result2 = db.PropertyImage.Where(x => x.PropertyId == id).ToList();
                ViewBag.PropertyImageList = result2;

                var result3 = db.Properties.Where(x => x.Id == id).FirstOrDefault();
                ViewBag.PropertyDetail = result3;
            }
            return View();
        }

        public ActionResult SearchProperty()
        {
            return View();
        }

        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult SearchProperty(string searchText)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<Models.City> ObjList = new List<Models.City>();
                    var resultList = (from p in db.Properties
                                      where p.Address.ToLower().Contains(searchText.ToLower()) && p.Longitude != null && p.Latitude != null
                                      select new Models.City
                                      {
                                          Address = p.Address,
                                          Longitude = p.Longitude.Trim(),
                                          Latitude = p.Latitude.Trim(),
                                      }).Distinct().ToList();
                    return Json(new { success = true, message = resultList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult SearchProperties(string geoLocation, string fromDate, string toDate, int? numberBathroom, int? beds, int? price, int? guestNumber, string otherSearch)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    string[] array = geoLocation.Split(',');
                    var secondcordinate = new GeoCoordinate(double.Parse(array[1]), double.Parse(array[0]));
                    IQueryable<Property> query = db.Properties.Where(x => x.Latitude != null && x.Longitude != null && x.SiteType == 0).Select(x => x).OrderByDescending(x => x.Id);
                    //filter Bathrooms
                    if (numberBathroom != null && numberBathroom > 0)
                    {
                        query = query.Where(x => x.Bathrooms >= numberBathroom);
                    }
                    //filter Bedrooms
                    if (beds != null && beds > 0)
                    {
                        query = query.Where(x => x.Bedrooms >= beds);
                    }
                    //filter guest number
                    if (guestNumber != null && guestNumber > 0)
                    {
                        query = query.Where(x => x.GuestLimit >= guestNumber);
                    }

                    //getting properties from property table//
                    var result = query.Select(x => x).ToList();

                    //filter with location diameter//
                    List<Property> property = new List<Property>();
                    foreach (var item in result)
                    {
                        var firstcordinate = new GeoCoordinate(double.Parse(item.Latitude), double.Parse(item.Longitude));
                        double distance = firstcordinate.GetDistanceTo(secondcordinate);
                        var miles = (distance / 1609.344);
                        if (miles <= 10)
                        {
                            int intDay = 1;
                            DateTime sDate = DateTime.UtcNow;
                            DateTime fDate = DateTime.UtcNow.AddDays(1);
                            List<int> priceTotal = GetBaseRateDetail(item.Id, 1, intDay, sDate, fDate);
                            if (priceTotal.Count > 0)
                            {
                                item.TaxRate = priceTotal[0];
                            }
                            property.Add(item);
                        }
                    }

                    //filter by price//
                    if (price > 0 && price != null)
                    {
                        List<Property> propertyResult = new List<Property>();
                        propertyResult = property;
                        property = new List<Property>();
                        foreach (var item in propertyResult)
                        {
                            if (item.TaxRate <= price)
                            {
                                property.Add(item);
                            }
                        }
                    }

                    //fitler Booked date//
                    if (fromDate.ToString() != string.Empty && toDate.ToString() != string.Empty)
                    {
                        List<Property> propertyResult = new List<Property>();
                        propertyResult = property;
                        property = new List<Property>();
                        foreach (var item in propertyResult)
                        {
                            List<DateTime> priceTotal = GetBookedDates(item.ListingId);
                            if (priceTotal.Count > 0)
                            {
                                var results = priceTotal.Where(x => x.Date >= Convert.ToDateTime(fromDate).Date && x.Date <= Convert.ToDateTime(fromDate).Date).ToList();
                                if (!(results.Count > 0))
                                {
                                    property.Add(item);
                                }
                            }
                            else
                            {
                                property.Add(item);
                            }
                        }
                    }


                    //fitler other search//
                    if (otherSearch.ToString() != string.Empty)
                    {
                        List<Property> propertyResult = new List<Property>();
                        propertyResult = property;
                        property = new List<Property>();
                        foreach (var item in propertyResult)
                        {
                            var resultss = db.PropertyCategory.Where(x => x.PropertyId == item.Id && x.SubCategoryName.ToLower().Contains(otherSearch.ToLower()) || x.CategoryName.ToLower().Contains(otherSearch.ToLower())).FirstOrDefault();
                            if (resultss != null)
                            {
                                property.Add(item);
                            }
                        }
                    }

                    var finalResult = property.OrderBy(x => x.TaxRate).ToList();
                    return Json(new { success = true, message = finalResult }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBookedDateList(int id)
        {
            bool isSuccess = false;
            List<DateTime> allDates = new List<DateTime>();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<JobTypeTableData> job = new List<JobTypeTableData>();
                    var data = db.Properties.Where(x => x.ListingId == id).FirstOrDefault();
                    var parent = db.ParentChildProperties.Where(x => x.ChildPropertyId == data.Id && SessionHandler.CompanyId == x.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    var dataLocal = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parent.ParentPropertyId select p).ToList();
                    //var dataLocal = db.Properties.Where(x => x.ParentPropertyId == data.ParentPropertyId).ToList();
                    List<Int64> obj = new List<Int64>();
                    foreach (var item in dataLocal)
                    {
                        obj.Add(item.ListingId);
                    }
                    var dataBooking = db.Inquiries.Where(x => obj.Contains(x.PropertyId) && x.CheckOutDate > DateTime.UtcNow).ToList();
                    foreach (var item in dataBooking)
                    {
                        for (DateTime date = DateTime.Parse(item.CheckInDate.ToString()); date <= item.CheckOutDate; date = date.AddDays(1))
                            allDates.Add(date);
                    }
                    isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, worker = allDates }, JsonRequestBehavior.AllowGet);
        }

        private List<DateTime> GetBookedDates(long id)
        {
            List<DateTime> allDates = new List<DateTime>();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<JobTypeTableData> job = new List<JobTypeTableData>();
                    var data = db.Properties.Where(x => x.ListingId == id).FirstOrDefault();
                    var parent = db.ParentChildProperties.Where(x => x.ChildPropertyId == data.Id && SessionHandler.CompanyId == x.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    var dataLocal = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parent.ParentPropertyId select p).ToList();
                    //var dataLocal = db.Properties.Where(x => x.ParentPropertyId == data.ParentPropertyId).ToList();
                    List<Int64> obj = new List<Int64>();
                    foreach (var item in dataLocal)
                    {
                        obj.Add(item.ListingId);
                    }
                    var dataBooking = db.Inquiries.Where(x => obj.Contains(x.PropertyId) && x.CheckOutDate > DateTime.UtcNow).ToList();
                    foreach (var item in dataBooking)
                    {
                        for (DateTime date = DateTime.Parse(item.CheckInDate.ToString()); date <= item.CheckOutDate; date = date.AddDays(1))
                            allDates.Add(date);
                    }
                }
            }
            catch { }
            return allDates;
        }

        public ActionResult ApplyApplication(int propertyId)
        {
            return View();
        }

        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult ApplyApplication(ApplyApplicationModel model)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    Guest objGuest = new Guest();
                    objGuest.Email = model.Email;
                    objGuest.FullName = model.FirstName + ' ' + model.LastName;
                    objGuest.Name = model.FirstName + ' ' + model.LastName;
                    string password = Guid.NewGuid().ToString();
                    objGuest.Password = password.Substring(0, 5);
                    objGuest.ProfilePictureUrl = "https://a0.muscache.com/defaults/user_pic-225x225.png?v=3";
                    objGuest.GuestType = "Local";
                    objGuest.GuestId = Guid.NewGuid().ToString();
                    db.Guests.Add(objGuest);
                    db.SaveChanges();

                    if (objGuest.Id != 0)
                    {

                        ApplicationDetail objApplicationDetail = new ApplicationDetail();
                        objApplicationDetail.GuestId = objGuest.Id;
                        objApplicationDetail.PropertyId = model.PropertyId;
                        objApplicationDetail.Income = model.Income.ToString();
                        db.ApplicationDetail.Add(objApplicationDetail);
                        db.SaveChanges();

                        if (objApplicationDetail.Id != 0)
                        {
                            List<ResidenceHistory> objResidenceHistory = new List<ResidenceHistory>();
                            int count = 0;
                            foreach (var item in model.ResidenceAddress)
                            {
                                ResidenceHistory residenceHistory = new ResidenceHistory();
                                residenceHistory.ResidenceAddress = item;
                                residenceHistory.ApplicationId = objApplicationDetail.Id;
                                residenceHistory.ContactPerson = model.ResidenceContactPerson[count];
                                residenceHistory.ContactNumber = model.ResidenceContactNumber[count];
                                residenceHistory.FromDate = DateTime.Parse(model.ResidenceFromDate[count]);
                                residenceHistory.ToDate = DateTime.Parse(model.ResidenceToDate[count]);
                                objResidenceHistory.Add(residenceHistory);
                                count++;
                            }
                            if (objResidenceHistory != null && objResidenceHistory.Count > 0)
                            {
                                db.ResidenceHistory.AddRange(objResidenceHistory);
                                db.SaveChanges();
                            }
                            List<JobHistory> objJobHistory = new List<JobHistory>();
                            int countJob = 0;
                            foreach (var item in model.JobCompanyAddress)
                            {
                                JobHistory jobHistory = new JobHistory();
                                jobHistory.CompanyAddress = item;
                                jobHistory.CompanyName = model.JobCompanyName[countJob];
                                jobHistory.Website = model.JobWebsite[countJob];
                                jobHistory.ApplicationId = objApplicationDetail.Id;
                                jobHistory.ContactPerson = model.JobContactPerson[countJob];
                                jobHistory.ContactNumber = model.JobContactNumber[countJob];
                                jobHistory.FromDate = DateTime.Parse(model.JobFromDate[countJob]);
                                jobHistory.ToDate = DateTime.Parse(model.JobToDate[countJob]);
                                objJobHistory.Add(jobHistory);
                                countJob++;
                            }
                            if (objJobHistory != null && objJobHistory.Count > 0)
                            {
                                db.JobHistory.AddRange(objJobHistory);
                                db.SaveChanges();
                            }
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion



        #region Guest property Payment
        [CheckSessionTimeout]
        public ActionResult PropertyPayment(int pid, int? rid)
        {

            if (User.Identity.IsAuthenticated && SessionHandler.CompanyId!=0)
            {
                using (var db = new ApplicationDbContext())
                {
                    ViewBag.PropertyName = db.Properties.Where(x => x.Id == pid).First().Name;
                }
                    return View();
            }
            return RedirectToAction("Index", "Home");

        }

        [HttpGet]
        [CheckSessionTimeout]
        public JsonResult GetBatchPayment(int id)
        {
            using (var db = new ApplicationDbContext()) {

                var batchPayment = db.IncomeBatchPayments.Where(x => x.Id == id).FirstOrDefault();
                var payments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == batchPayment.Id).ToList();
                List<BatchIncomePaymentViewModel> list = new List<BatchIncomePaymentViewModel>();
                var incomelist = new List<Income>();
                foreach (var payment in payments)
                {
                    BatchIncomePaymentViewModel temp = new BatchIncomePaymentViewModel();
                    var income = db.Income.Where(x => x.Id == payment.IncomeId).FirstOrDefault();
                    temp.Balance = income.Amount.Value;
                    temp.Amount = payment.Amount;
                    temp.IncomeId = income.Id;
                    temp.IncomePaymentId = payment.Id;
                    temp.Description = income.Description;
                    temp.DueDate = income.DueDate;
                    list.Add(temp);
                    
                }
                var incomes = db.Income.Where(x => x.BookingId == batchPayment.BookingId && x.IsActive && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).ToList();
                foreach (var income in incomes)
                {
                    var temp = db.IncomePayment.Where(x => x.IncomeId == income.Id &&x.IsRejected == false).ToList();
                    decimal dueAmount = 0;
                    if (temp.Sum(x => x.Amount) < income.Amount)
                    {
                        income.Amount = income.Amount - temp.Sum(x => x.Amount);
                        incomelist.Add(income);
                    }
                 
                }
                List<string> ImageUrls = db.IncomePaymentFiles.Where(x => x.BatchPaymentId == batchPayment.Id).Select(x => x.FileUrl).ToList();
                return Json(new {batchpayment=batchPayment, incomingpayment = list , monthlyrent = incomelist, images = ImageUrls }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [CheckSessionTimeout]
        public JsonResult GetMonthlyRenth(int bookingId)
        {
            using(var db = new ApplicationDbContext())
            {
                var list = new List<Income>();
                var incomes = db.Income.Where(x => x.BookingId == bookingId && x.IsActive && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).ToList();
                foreach(var income in incomes)
                {
                    var temp = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                    //JM 09/14/19  amount minus payment to get balance
                    if (temp.Sum(x=>x.Amount) <income.Amount) {
                        income.Amount = income.Amount - temp.Sum(x => x.Amount);
                        list.Add(income);
                    }
                }
                return Json(new { success = true, monthlyrent =list}, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetPropertyPayment(int PropertyId, int BookingId)
            {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var runningBalance = 0.0M;
                    var property = db.Properties.Where(x => x.Id == PropertyId).FirstOrDefault();

                    var result0 = db.Income.Where(x => x.PropertyId == property.Id && x.BookingId == BookingId).OrderBy(x=>x.DueDate).ToList();
                    List<IncomeDetail> objIncomes = new List<IncomeDetail>();

                    foreach (var item in result0)
                    {
                        IncomeDetail objIncome = new IncomeDetail();

                        objIncome.BookingId = item.BookingId;
                        objIncome.Id = item.Id;
                     
                        var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                        decimal dueAmount = 0;
                        dueAmount = payments.Sum(x => x.Amount);
                        if (payments.Count > 0)
                        {
                            foreach (var payment in payments)
                            {
                                objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                            }
                        }
                        else { objIncome.PaymentDetails ="-"; }
                        objIncome.Payments = dueAmount;
                        objIncome.Amount = (item.Amount.ToDecimal());
                        objIncome.DueDate = item.DueDate;
                        objIncome.Description = item.Description;
                        objIncome.IncomeTypeId = item.IncomeTypeId;
                        objIncome.IsActive = item.IsActive;
                        if (item.IsActive && item.IncomeTypeId !=(int)Core.Enumerations.IncomeType.Refund)
                        {
                            runningBalance += payments.Where(x => x.Type != (int)IncomePaymentType.Credit).Sum(x => x.Amount);
                        }
                        else if(item.IsActive==true && item.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund)
                        {
                            runningBalance -= item.Amount.Value;
                        }
                        objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments < objIncome.Amount) ? "Partial" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" :  objIncome.Payments> objIncome.Amount?"Over Paid": "Unpaid");
                        
                        objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                        objIncome.RunningBalance = dueAmount!=0? runningBalance:0;
                        objIncomes.Add(objIncome);
                    }
                    var batchPayments = db.IncomeBatchPayments.Where(x => x.BookingId == BookingId).ToList();
                    foreach (var payment in batchPayments)
                    {
                        IncomeDetail objIncome = new IncomeDetail();

                        objIncome.BookingId = payment.BookingId;
                        objIncome.Id = payment.Id;

                        //objIncome.IncomePayments = new List<IncomePayment>();
                        var incomePayments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == payment.Id && x.IsRejected == false).ToList();
                        decimal dueAmount = 0;
                        dueAmount = incomePayments.Sum(x => x.Amount);

                        var totalpay = incomePayments.Where(x => x.Type == (int)IncomePaymentType.Payment).Sum(x => x.Amount);
                        var unallocated = payment.Amount - totalpay;
                        objIncome.Status = unallocated ==0? "Full-allocated" :payment.Amount==unallocated?"Unallocated":"Partial-allocated";
                        objIncome.Amount =payment.Amount;
                        runningBalance += unallocated;
                        objIncome.RunningBalance = runningBalance;
                        objIncome.DueDate =payment.PaymentDate;
                        objIncome.Balance =unallocated;
                        objIncome.Description = payment.Description;
                        objIncome.IncomeTypeId = 6;
                        objIncome.IsActive = true;
                        var payments = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == payment.Id && x.IsRejected == false).ToList();
                        if (payments.Count > 0)
                        {
                            foreach (var pay in payments)
                            {
                                var income = db.Income.Where(x => x.Id == pay.IncomeId).FirstOrDefault();
                                var tempayments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                                tempayments.Sum(x => x.Amount);
                                objIncome.PaymentDetails += income.DueDate.ToString("MMM d,yyyy") + " - " + income.Description + " - " + pay.Amount + "</br>";

                            }
                        }
                        else
                        {
                            objIncome.PaymentDetails = "-";
                        }
                        objIncomes.Add(objIncome);
                    }
                    objIncomes= objIncomes.OrderByDescending(x=>x.DueDate).ToList();
                    return Json(new { success = true, Income = objIncomes, IsOwner= Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt()) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetIncomeDetail(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var result = db.Income.Where(x => x.Id == id).FirstOrDefault();

                    var results = db.IncomePayment.Where(x => x.IncomeId == id && x.IsRejected == false).ToList();
                    decimal dueAmount = 0;
                    dueAmount = results.Sum(x => x.Amount);
                    IncomeDetail objIncomeDetail = new IncomeDetail();
                    objIncomeDetail.Id = result.Id;
                    objIncomeDetail.Amount = (dueAmount>result.Amount.ToDecimal()?0:(result.Amount.ToDecimal() - dueAmount));
                    objIncomeDetail.DueDate = result.DueDate;
                    objIncomeDetail.Description = result.Description;
                    objIncomeDetail.BookingId = result.BookingId;
                    return Json(new { success = true, message = objIncomeDetail }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
     
        [HttpGet]
        public JsonResult GetIncomePayment(int id)
        {
            using(var db =new ApplicationDbContext())
            {
                var payment = db.IncomePayment.Where(x => x.Id == id).FirstOrDefault();
                var income = db.Income.Where(x => x.Id == payment.IncomeId).FirstOrDefault();

                var results = db.IncomePayment.Where(x => x.IncomeId == income.Id).ToList();
                decimal dueAmount = 0;
                //If overpaid the balance is zero
                dueAmount = results.Sum(x => x.Amount)> income.Amount.ToDecimal()?0: income.Amount.ToDecimal() - results.Sum(x => x.Amount);
                List<string> ImageUrls=db.IncomePaymentFiles.Where(x => x.PaymentId == payment.Id).Select(x => x.FileUrl).ToList();

                return Json(new { success = true ,payment = payment,images =ImageUrls,DueAmount =dueAmount}, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult EditPayment(IncomePayment payment, HttpPostedFileWrapper[] Images,int bookingId)
        {
            using (var db= new ApplicationDbContext())
            {
                var pay = db.IncomePayment.Where(x => x.Id == payment.Id).FirstOrDefault();
                pay.Amount = payment.Amount;
                pay.Description = payment.Description;
                pay.CreatedAt = payment.CreatedAt;
                db.Entry(pay).State = EntityState.Modified;
                db.SaveChanges();
                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Edit payment Description:{0},Amount:{1}", payment.Description, payment.Amount), bookingId);

                if (Images != null)
                {
                    foreach (var image in Images)
                    {
                        var IncomePaymentImage = new IncomePaymentFile() { PaymentId = pay.Id, FileUrl = UploadImage(image, "~/Images/IncomePayments/") };
                        db.IncomePaymentFiles.Add(IncomePaymentImage);
                        db.SaveChanges();
                    }
                }

            }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CreateBatchPayment(int batchId,int bookingId,string description,string paymentDate, HttpPostedFileWrapper[] Images, string amount,string payment)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {

                    var batchPaymentId = 0;
                    if (batchId == 0)
                    {
                        var batchPayment = new IncomeBatchPayment()
                        {
                            Amount = amount.ToDecimal(),
                            Description = description,
                            BookingId = bookingId,
                            PaymentDate = paymentDate.ToDateTime(),
                            CompanyId = SessionHandler.CompanyId
                        };
                        db.IncomeBatchPayments.Add(batchPayment);
                        db.SaveChanges();
                        batchPaymentId = batchPayment.Id;
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Created Batch payment Description:{0},Amount:{1}", description, amount), bookingId);

                    }
                    else
                    {
                        var temp = db.IncomeBatchPayments.Where(x => x.Id == batchId).FirstOrDefault();
                        temp.Amount = amount.ToDecimal();
                        temp.Description = description;
                        temp.PaymentDate = paymentDate.ToDateTime();
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                        batchPaymentId = temp.Id;
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Edit Batch payment Description:{0},Amount:{1}", description, amount), bookingId);
                    }
                    var data = JArray.Parse(payment);

                    foreach (var income in data)
                    {
                        if (income["Amount"].ToString() != "" && income["Amount"].ToString() != null && income["Amount"].ToString() != "0")
                        {
                            if (income["PaymentId"].ToString() == "0")
                            {
                                IncomePayment objIncomePayment = new IncomePayment();
                                objIncomePayment.Amount = income["Amount"].ToDecimal();
                                objIncomePayment.CreatedAt = DateTime.UtcNow;
                                objIncomePayment.Description = description;
                                objIncomePayment.Type = (int)IncomePaymentType.Payment;
                                objIncomePayment.IncomeId = income["IncomeId"].ToInt();
                                objIncomePayment.IncomeBatchPaymentId = batchPaymentId;
                                db.IncomePayment.Add(objIncomePayment);
                                db.SaveChanges();
                            }
                            else
                            {
                                var tempid = income["PaymentId"].ToInt();
                                var tempPayment = db.IncomePayment.Where(x => x.Id == tempid).FirstOrDefault();
                                tempPayment.Amount = income["Amount"].ToDecimal();
                                tempPayment.Description = description;
                                db.Entry(tempPayment).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if ((income["Amount"].ToString() == "" || income["Amount"].ToString() == null || income["Amount"].ToString() == "0") && income["PaymentId"].ToString() != "0")
                        {
                            var id = income["PaymentId"].ToInt();
                            db.IncomePayment.Remove(db.IncomePayment.Where(x => x.Id == id).FirstOrDefault());
                            db.SaveChanges();
                        }
                    }
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            var IncomePaymentImage = new IncomePaymentFile() { BatchPaymentId = batchPaymentId, FileUrl = UploadImage(image, "~/Images/IncomePayments/") };
                            db.IncomePaymentFiles.Add(IncomePaymentImage);
                            db.SaveChanges();
                        }
                    }
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreatePayment(IncomeDetail income, HttpPostedFileWrapper [] Images,int bookingId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    IncomePayment objIncomePayment = new IncomePayment();
                    objIncomePayment.Amount = income.Amount;
                    objIncomePayment.CreatedAt = DateTime.UtcNow;
                    objIncomePayment.Description =income.Description;
                    objIncomePayment.Type = income.IncomeTypeId;//(int)IncomePaymentType.Payment;
                    objIncomePayment.IncomeId = income.Id;
                    db.IncomePayment.Add(objIncomePayment);
                    db.SaveChanges();
                    Core.API.AllSite.ContractChangesLogs.Add(string.Format("Payment created Description:{0},Amount:{1}",income.Description,income.Amount),bookingId);
               
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            var IncomePaymentImage = new IncomePaymentFile() {PaymentId= objIncomePayment.Id, FileUrl = UploadImage(image, "~/Images/IncomePayments/") };
                            db.IncomePaymentFiles.Add(IncomePaymentImage);
                            db.SaveChanges();
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CancelIncome(int id,int bookingId)
        {
            using(var db = new ApplicationDbContext())
            {
                var income = db.Income.Where(x => x.Id == id).FirstOrDefault();
                income.IsActive = false;
                db.Entry(income).State = EntityState.Modified;
                Core.API.AllSite.Incomes.CreateBatchPaymentFromCancelledIncome(income.Id);
                db.SaveChanges();
                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Cancel Income Description:{0},Amount:{1}", income.Description, income.Amount), bookingId);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCredit(decimal amount,int id,string desc)
        {
            using(var db = new ApplicationDbContext())
            {
                IncomePayment objIncomePayment = new IncomePayment();
                objIncomePayment.Amount = decimal.Parse(amount.ToString()); ;
                objIncomePayment.CreatedAt = DateTime.UtcNow;
                objIncomePayment.Description = desc;
                objIncomePayment.Type = (int)IncomePaymentType.Credit;
                objIncomePayment.IncomeId = id;
                db.IncomePayment.Add(objIncomePayment);
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SaveIncome(Income income)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Income.Add(income);
                    db.SaveChanges();
                    return Json(new { success = true, incomeId=income.Id}, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
  
        #region EditPropertyPayment
        [HttpPost]
        public JsonResult EditPropertyPayment(LocalPropertyPayment propertyPayment)
        {
            using (var db = new ApplicationDbContext())
            {
                var property = db.Properties.Where(x => x.Id == propertyPayment.PropertyId).FirstOrDefault();
                var reservation = db.Inquiries.Where(x => x.Id == propertyPayment.BookingId).FirstOrDefault();
             
                if (reservation.CheckOutDate != propertyPayment.CheckedOutDate)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Checkout date from {0} to {1}",reservation.CheckOutDate.ToDateTime().ToString("MMM dd,yyyy"), propertyPayment.CheckedOutDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt()); }
                reservation.CheckOutDate = propertyPayment.CheckedOutDate;

                if (reservation.CheckInDate != propertyPayment.CheckedInDate)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Checkin date from {0} to {1}", reservation.CheckInDate.ToDateTime().ToString("MMM dd,yyyy"), propertyPayment.CheckedInDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt()); }
                reservation.CheckInDate = propertyPayment.CheckedInDate;

                if (reservation.LateFeeAmount != propertyPayment.LateFeeAmount)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Late amount fee from {0} to {1}", reservation.LateFeeAmount.ToDecimal().ToString("0.##"), propertyPayment.LateFeeAmount.ToDecimal().ToString("0.##")), propertyPayment.BookingId.ToInt()); }
                reservation.LateFeeAmount = propertyPayment.LateFeeAmount;

                if (reservation.LateFeeApplyDays != propertyPayment.LateFeeApplyDays)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Late fee apply days from {0} to {1}", reservation.LateFeeApplyDays, propertyPayment.LateFeeApplyDays), propertyPayment.BookingId.ToInt()); }
                reservation.LateFeeApplyDays = propertyPayment.LateFeeApplyDays;
                //reservation.IsMonthToMonth = propertyPayment.isMonthToMonth;
                //if (reservation.IsMonthToMonth != propertyPayment.isMonthToMonth)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsMonthToMonth, propertyPayment.isMonthToMonth), propertyPayment.BookingId.ToInt()); }
                
                if (reservation.RecurringRentStartDate != propertyPayment.RecurringRentStartDate)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Recurring date from {0} to {1}", reservation.RecurringRentStartDate.ToDateTime().ToString("MMM dd,yyyy"), propertyPayment.RecurringRentStartDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt()); }
                reservation.RecurringRentStartDate = propertyPayment.RecurringRentStartDate;
                //reservation.IsFixedMonthToMonth = propertyPayment.isFixedMonthToMonth;
                //if (reservation.IsFixedMonthToMonth != propertyPayment.isFixedMonthToMonth)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsFixedMonthToMonth, propertyPayment.isFixedMonthToMonth), propertyPayment.BookingId.ToInt()); }
               
                if (reservation.SecurityDepositAmount != propertyPayment.SecurityDepositAmount)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Security deposit amount from {0} to {1}", reservation.SecurityDepositAmount.ToDecimal().ToString("0.##"), propertyPayment.SecurityDepositAmount.ToDecimal().ToString("0.##")), propertyPayment.BookingId.ToInt()); }
                reservation.SecurityDepositAmount = propertyPayment.SecurityDepositAmount;
              
                if (reservation.AdvancePaymentMonth != propertyPayment.AdvancePaymentMonth)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Advance Payment Month from {0} to {1}", reservation.AdvancePaymentMonth.ToDecimal().ToString("0.##"), propertyPayment.AdvancePaymentMonth.ToDecimal().ToString("0.##")), propertyPayment.BookingId.ToInt()); }
                reservation.AdvancePaymentMonth = propertyPayment.AdvancePaymentMonth;
                reservation.IsAdvancePayment = propertyPayment.isAdvancePayment;
                //if (reservation.IsAdvancePayment != propertyPayment.isAdvancePayment)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsAdvancePayment, propertyPayment.isAdvancePayment), propertyPayment.BookingId.ToInt()); }
                reservation.IsLateFees = propertyPayment.isLateFees;
                //if (reservation.IsLateFees != propertyPayment.isLateFees)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsLateFees, propertyPayment.isLateFees), propertyPayment.BookingId.ToInt()); }
                reservation.IsPostDatedPayment = propertyPayment.isPostDatedPayment;
                //if (reservation.IsPostDatedPayment != propertyPayment.isPostDatedPayment)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsPostDatedPayment, propertyPayment.isPostDatedPayment), propertyPayment.BookingId.ToInt()); }
                reservation.IsSecurityDeposit = propertyPayment.isSecurityDeposit;
                //if (reservation.IsSecurityDeposit != propertyPayment.isSecurityDeposit)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsSecurityDeposit, propertyPayment.isSecurityDeposit), propertyPayment.BookingId.ToInt()); }
              
                if (reservation.SecurityDepositDueOn != propertyPayment.SecurityDepositDueOn)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Security deposit due date from {0} to {1}", reservation.SecurityDepositDueOn.ToDateTime().ToString("MMM dd,yyyy"), propertyPayment.SecurityDepositDueOn.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt()); }
                reservation.SecurityDepositDueOn = propertyPayment.SecurityDepositDueOn;
                reservation.IsLastMonthPayment = propertyPayment.isLastMonthPayment;
                //if (reservation.IsLastMonthPayment != propertyPayment.isLastMonthPayment)
                //{ Core.API.AllSite.ContractChangesLogs.Add(string.Format("", reservation.IsLastMonthPayment, propertyPayment.isLastMonthPayment), propertyPayment.BookingId.ToInt()); }
               
                if (reservation.LastPaymentMonth != propertyPayment.LastPaymentMonth)
                { Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract change Last payment month from {0} to {1}", reservation.LastPaymentMonth, propertyPayment.LastPaymentMonth), propertyPayment.BookingId.ToInt()); }
                reservation.LastPaymentMonth = propertyPayment.LastPaymentMonth;

                reservation.IsMonthToMonth = propertyPayment.isMonthToMonth;
                reservation.IsFixedMonthToMonth = propertyPayment.isFixedMonthToMonth;
                db.Entry(reservation).State = EntityState.Modified;
                db.SaveChanges();

                //try
                //{
                    var renters = JsonConvert.DeserializeObject<List<Renter>>(propertyPayment.RenterString);
                    var deletedRenter = db.Renters.Where(x => x.BookingId == propertyPayment.BookingId).Select(x => x.Id).ToList();
                    foreach (var renter in renters)
                    {
                        renter.BookingId = propertyPayment.BookingId.ToInt();
                        var temp = db.Renters.Where(x =>(x.Id == renter.Id && x.BookingId == renter.BookingId)).FirstOrDefault();
                    if (temp == null)
                    {
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Create Renter:{0}, Address:{1}, Phone Number:{2}, Email:{3}", renter.Firstname + " " + renter.Lastname, renter.Address, renter.PhoneNumber, renter.Email), propertyPayment.BookingId.ToInt());
                        db.Renters.Add(renter);
                        db.SaveChanges();
                       
                       
                        var thread = db.CommunicationThreads.Where(x => x.CompanyId == property.CompanyId && x.RenterId == renter.Id).FirstOrDefault();
                        if (thread == null)
                        {

                            CommunicationThread workerThread = new CommunicationThread();
                            db.CommunicationThreads.Add(workerThread);
                            workerThread.RenterId = renter.Id;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = property.CompanyId;
                            workerThread.IsRenter = true;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                        Emails.AddOrUpdate(temp.Id, renter.Email);
                        Contacts.AddOrUpdate(temp.Id, renter.PhoneNumber);
                    }
                    else
                    {
                        Emails.AddOrUpdate(temp.Id, renter.Email);
                        Contacts.AddOrUpdate(temp.Id, renter.PhoneNumber);
                        deletedRenter.Remove(temp.Id);
                        if (temp.Firstname != renter.Firstname ||temp.Lastname !=renter.Lastname || temp.Address != renter.Address /*|| temp.PhoneNumber != renter.PhoneNumber*/ || temp.Email != renter.Email || temp.IsActive != renter.IsActive || temp.StartDate != renter.StartDate || temp.EndDate != renter.EndDate)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Change Renter:{0} to {1}, Address:{2} to {3}, Phone Number:{4} to {5}, Email:{6} to {7} , Start Date:{8} to {9} End Date: {10} to {11}", temp.Firstname+" "+temp.Lastname, renter.Firstname+" "+renter.Lastname, temp.Address, renter.Address, temp.PhoneNumber, renter.PhoneNumber, temp.Email, renter.Email, temp.StartDate.ToDateTime().ToString("MMM d,yyyy"), renter.StartDate.ToDateTime().ToString("MMM d,yyyy"), temp.EndDate.ToDateTime().ToString("MMM d,yyyy"), renter.EndDate.ToDateTime().ToString("MMM d,yyyy")), propertyPayment.BookingId.ToInt());
                            temp.Firstname = renter.Firstname;
                            temp.Lastname = renter.Lastname;
                            temp.Email = renter.Email;
                            temp.PhoneNumber = renter.PhoneNumber;
                            temp.Address = renter.Address;
                            temp.IsActive = renter.IsActive;
                            temp.StartDate = renter.StartDate;
                            temp.EndDate = renter.EndDate;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        var thread = db.CommunicationThreads.Where(x => x.CompanyId == property.CompanyId && x.RenterId == temp.Id).FirstOrDefault();
                        if (thread == null)
                        {
                            CommunicationThread workerThread = new CommunicationThread();
                            db.CommunicationThreads.Add(workerThread);
                            workerThread.RenterId = temp.Id;
                            workerThread.ThreadId = PasswordGenerator.Generate(20);
                            workerThread.CompanyId = property.CompanyId;
                            workerThread.IsRenter = true;
                            db.CommunicationThreads.Add(workerThread);
                            db.SaveChanges();
                        }
                    }
                    }
                    foreach (var rentId in deletedRenter)
                    {
                        var temp = db.Renters.Find(rentId);
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Delete Rent effectivity Name:{0}", temp.Firstname + " " + temp.Lastname), propertyPayment.BookingId.ToInt());
                        db.Renters.Remove(temp);
                        db.SaveChanges();
                    }


                    var rents = JsonConvert.DeserializeObject<List<MonthlyRent>>(propertyPayment.RentString);
                    var deletedRent = db.MonthlyRents.Where(x => x.BookingId == propertyPayment.BookingId).Select(x => x.Id).ToList();
                    foreach (var rent in rents)
                    {
                        rent.BookingId = propertyPayment.BookingId.ToInt();

                        var temp = db.MonthlyRents.Where(x => x.Id == rent.Id && x.BookingId == rent.BookingId).FirstOrDefault();
                        if (temp == null)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Create Rent Amount effectivity Effective Date:{0}, Amount:{1}", rent.EffectiveDate.ToString("MMM dd,yyyy"), rent.Amount.ToString("0.##")), propertyPayment.BookingId.ToInt());
                            db.MonthlyRents.Add(rent);
                            db.SaveChanges();
                        }
                        else
                        {
                            deletedRent.Remove(temp.Id);
                            if (temp.Amount != rent.Amount || temp.EffectiveDate != rent.EffectiveDate)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Change Rent Amount effectivity Effective Date:{0} to {1}, Amount:{2} to {3}", temp.EffectiveDate.ToString("MMM dd,yyyy"), rent.EffectiveDate.ToString("MMM dd,yyyy"), temp.Amount.ToString("0.##"), rent.Amount.ToString("0.##")), propertyPayment.BookingId.ToInt());
                                temp.Amount = rent.Amount;
                                temp.EffectiveDate = rent.EffectiveDate;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    foreach (var rentId in deletedRent)
                    {
                        var temp = db.MonthlyRents.Find(rentId);
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Delete Rent Amount effectivity Effective Date:{0}, Amount:{1}", temp.EffectiveDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount.ToString("0.##")), propertyPayment.BookingId.ToInt());
                        db.MonthlyRents.Remove(temp);
                        db.SaveChanges();
                    }

                    var deletedPostdated = db.PostDated.Where(x => x.BookingId == propertyPayment.BookingId).Select(x => x.Id).ToList();
                    var postDated = JsonConvert.DeserializeObject<List<PostDated>>(propertyPayment.PostDatedString);
                    foreach (var postdated in postDated)
                    {
                        postdated.BookingId = propertyPayment.BookingId.ToInt();
                        postdated.DateEnd = new DateTime(postdated.DateEnd.Year, postdated.DateEnd.Month, DateTime.DaysInMonth(postdated.DateEnd.Year, postdated.DateEnd.Month));
                        var temp = db.PostDated.Where(x => x.Id == postdated.Id && x.BookingId == postdated.BookingId).FirstOrDefault();
                        if (temp == null)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Create Post date Start Date:{0}, End Date:{1}, Amount:{2}", postdated.DateStart.ToString("MMM dd,yyyy"), postdated.DateEnd.ToString("MMM dd,yyyy"), postdated.Amount.ToString("0.##")), propertyPayment.BookingId.ToInt());
                            db.PostDated.Add(postdated);
                            db.SaveChanges();
                        }
                        else
                        {
                            deletedPostdated.Remove(temp.Id);
                            if (temp.Amount != postdated.Amount || temp.DateStart != postdated.DateStart || temp.DateEnd != postdated.DateEnd)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Change Post date Start Date:{0} to {1},End Date:{2} to {3}, Amount:{4} to {5}", temp.DateStart.ToString("MMM dd,yyyy"), postdated.DateStart.ToString("MMM dd,yyyy"), temp.DateEnd.ToString("MMM dd,yyyy"), postdated.DateEnd.ToString("MMM dd,yyyy"), temp.Amount.ToString("0.##"), postdated.Amount.ToString("0.##")), propertyPayment.BookingId.ToInt());
                                temp.Amount = postdated.Amount;
                                temp.DateStart = postdated.DateStart;
                                temp.DateEnd = postdated.DateEnd;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }
                    }
                    foreach (var postId in deletedPostdated)
                    {
                        var temp = db.PostDated.Find(postId);
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Delete post dated {0} to {1}", temp.DateStart.ToString("MMM dd,yyyy"), temp.DateEnd.ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                        db.PostDated.Remove(temp);
                        db.SaveChanges();
                    }

                    //JM 10/10/19 Additional fee and Utilities
                    var deletedFee = db.BookingAdditionalFees.Where(x => x.BookingId == propertyPayment.BookingId).Select(x => x.Id).ToList();
                    var AdditionalFees = JsonConvert.DeserializeObject<List<BookingAdditionalFee>>(propertyPayment.AdditionalFeeString);
                    foreach (var fee in AdditionalFees)
                    {
                        fee.BookingId = propertyPayment.BookingId.ToInt();
                        var temp = db.BookingAdditionalFees.Where(x => (x.Description == fee.Description || x.Id == fee.Id) && x.BookingId == fee.BookingId).FirstOrDefault();
                        if (temp == null)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Created Monthly Billing Description:{0}, Amount:{1}, Due Date:{2}", fee.Description, fee.Amount.ToDecimal().ToString("0.##"), fee.DueOn.ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                            db.BookingAdditionalFees.Add(fee);
                            db.SaveChanges();
                        }
                        else
                        {
                            deletedFee.Remove(temp.Id);
                            if (temp.Amount != fee.Amount || temp.Description != fee.Description || temp.DueOn != fee.DueOn || temp.Frequency != fee.Frequency)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Change Month Billing Description:{0} to {1}, Amount:{2} to {3}, Due Date: {4} to {5}", temp.Description, fee.Description, temp.Amount.ToDecimal().ToString("0.##"), fee.Amount.ToDecimal().ToString("0.##"), temp.DueOn.ToString("MMM dd,yyyy"), fee.DueOn.ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                                temp.Amount = fee.Amount;
                                temp.Description = fee.Description;
                                temp.DueOn = fee.DueOn;
                                temp.Frequency = fee.Frequency;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        //JM 11/4/19 Create transaction base on fee
                        if (fee.Type == 1)
                        {
                            var tempIncome = db.Income.Where(x =>
                                  x.BookingAdditionalFeeId == fee.Id).FirstOrDefault();
                            if (tempIncome == null)
                            {
                                Income income = new Income();
                                income.Description = fee.Description;
                                income.PropertyId = Convert.ToInt32(property.Id);
                                income.Amount = fee.Amount.Value;
                                income.DueDate = fee.DueOn;
                                income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                                income.Status = 1;
                                income.CompanyId = property.CompanyId;
                                income.DateRecorded = DateTime.UtcNow;
                                income.IsBatchLoad = false;
                                income.BookingId = reservation.Id;
                                income.IsActive = true;
                                income.BookingAdditionalFeeId = fee.Id;
                                db.Income.Add(income);
                                db.SaveChanges();
                            }
                            else
                            {
                                tempIncome.Description = fee.Description;
                                tempIncome.DueDate = fee.DueOn;
                                tempIncome.Amount = fee.Amount.Value;
                                db.Entry(tempIncome).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    foreach (var feeId in deletedFee)
                    {
                        var temp = db.BookingAdditionalFees.Find(feeId);
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("{0} deleted monthly billing", temp.Description), propertyPayment.BookingId.ToInt());

                        if (temp.Type == 1)
                        {
                            var tempIncome = db.Income.Where(x =>
                                     x.BookingAdditionalFeeId == feeId).FirstOrDefault();
                            if (tempIncome != null)
                            {
                                tempIncome.IsActive = false;
                                Core.API.AllSite.Incomes.CreateBatchPaymentFromCancelledIncome(tempIncome.Id);
                                db.Entry(tempIncome).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            //Remove income created by move in cost 
                            var tempIncome = db.Income.Where(x =>
                                     x.BookingAdditionalFeeId == feeId && x.DueDate >= DateTime.Now).ToList();
                            if (tempIncome.Count > 0)
                            {
                               foreach(var income in tempIncome)
                                {
                                    income.IsActive = false;
                                    db.Entry(income).State = EntityState.Modified;
                                    Core.API.AllSite.Incomes.CreateBatchPaymentFromCancelledIncome(income.Id);
                                    db.SaveChanges();
                                }
                                db.Income.RemoveRange(tempIncome);
                                db.SaveChanges();
                            }
                            db.BookingAdditionalFees.Remove(temp);
                            db.SaveChanges();
                        }
                    }

                    if (propertyPayment.isLastMonthPayment)
                    {
                        //var lastMonthList = db.Income.Where(x => x.BookingId == reservation.Id && x.Description.Contains("Last Month Payment")).ToList();
                        //foreach (var last in lastMonthList)
                        //{
                        //    last.IsActive = false;
                        //    db.Entry(last).State = EntityState.Modified;
                        //    db.SaveChanges();
                        //}
                        var tempcount = 1;
                        while (tempcount <= propertyPayment.LastPaymentMonth)
                        {
                            Income income = new Income();
                            income.Description = "Last Month Payment " + tempcount;
                            income.PropertyId = Convert.ToInt32(property.Id);
                            income.Amount = decimal.Parse(rents.OrderBy(x => x.EffectiveDate).FirstOrDefault().Amount.ToString());
                            income.DueDate = propertyPayment.CheckedInDate;
                            income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                            income.Status = 2;
                            income.CompanyId = property.CompanyId;
                            income.DateRecorded = DateTime.UtcNow;
                            income.IsBatchLoad = false;
                            income.BookingId = propertyPayment.BookingId.ToInt();
                            income.IsActive = true;
                            var temp = db.Income.Where(x => x.Description == income.Description && x.BookingId == reservation.Id).FirstOrDefault();
                            if (temp == null)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount.ToDecimal().ToString("0,##"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                                db.Income.Add(income);
                                db.SaveChanges();
                            }
                            else
                            {
                                if (temp.Description != income.Description || temp.DueDate != income.DueDate || temp.Amount != income.Amount)
                                {
                                    Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction changes Description: {0} to {1} Due Date:{2} to {3} Amount:{4} to {5} ", temp.Description, income.Description, temp.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount.ToDecimal().ToString("0,##"), income.Amount.ToDecimal().ToString("0,##")), propertyPayment.BookingId.ToInt());
                                    temp.Description = income.Description;
                                    temp.DueDate = income.DueDate;
                                    temp.Amount = income.Amount;
                                    temp.IsActive = true;
                                    db.Entry(temp).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            tempcount++;
                        }

                    }

                    var dt = new DateTime(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month, 1);
                    if (propertyPayment.CheckedInDate != dt)
                    {
                        var lastDayMonth = DateTime.DaysInMonth(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month);
                        var daydiff = Math.Abs((lastDayMonth - propertyPayment.CheckedInDate.Day) + 1);
                        var endOfMonth = new DateTime(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month, lastDayMonth);
                        Income income = new Income();
                        income.Description = "Pro Rated - " + propertyPayment.CheckedInDate.ToString("MMM dd") + "-" + lastDayMonth + "," + propertyPayment.CheckedInDate.Year;
                        income.PropertyId = Convert.ToInt32(property.Id);
                        income.Amount = Math.Round((decimal.Parse(rents.OrderBy(x => x.EffectiveDate).FirstOrDefault().Amount.ToString()) / (DateTime.DaysInMonth(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month)) * daydiff.ToDecimal()), 2);
                        income.DueDate = propertyPayment.CheckedInDate;
                        income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                        income.Status = 1;
                        income.CompanyId = property.CompanyId;
                        income.DateRecorded = DateTime.UtcNow;
                        income.IsBatchLoad = false;
                        income.IsActive = true;
                        income.BookingId = propertyPayment.BookingId.ToInt();
                        var temp = db.Income.Where(x => (x.Description.Contains("Pro Rated") && x.BookingId == reservation.Id)).FirstOrDefault();
                        if (temp == null)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt()); db.Income.Add(income);
                            db.SaveChanges();
                        }
                        else
                        {
                            if (temp.Description != income.Description || temp.DueDate != income.DueDate || temp.Amount !=income.Amount.Value)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction changes Description: {0} to {1} Due Date:{2} to {3} Amount:{4} to {5} ", temp.Description, income.Description, temp.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount, income.Amount), propertyPayment.BookingId.ToInt());
                                temp.Amount = income.Amount;
                                temp.Description = income.Description;
                                temp.DueDate = income.DueDate;
                                temp.IsActive = true;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    //Ask kendrick
                    //var MonthlyRent = db.Income.Where(x => (x.Description == "Rent" || x.Description.Contains("Post Dated") || x.Description == "Advance Payment") && (x.DueDate > DateTime.Now) && x.BookingId == propertyPayment.BookingId).ToList();
                    //foreach (var rent in MonthlyRent)
                    //{
                    //    rent.Amount = (decimal)rents.Where(x => x.EffectiveDate <= rent.DueDate).OrderByDescending(x => x.EffectiveDate).FirstOrDefault().Amount;
                    //    db.Entry(rent).State = EntityState.Modified;
                    //    db.SaveChanges();
                    //}
                    DateTime start = propertyPayment.RecurringRentStartDate.Value < propertyPayment.CheckedInDate ? new DateTime(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month, propertyPayment.RecurringRentStartDate.Value.Day) : DateTime.Parse(propertyPayment.RecurringRentStartDate.ToString());
                    var end = DateTime.Parse(propertyPayment.RecurringRentStartDate.ToString());

                    var lastRent =db.Income.Where(x => (x.Description == "Rent" || x.Description.Contains("Post Dated") || x.Description == "Advance Payment") && x.BookingId == reservation.Id).OrderByDescending(x=>x.DueDate).FirstOrDefault();
                    if (lastRent != null)
                    {
                        end = lastRent.DueDate;
                    }
                    if (postDated.Count != 0 || propertyPayment.CheckedOutDate != null)
                    {
                        end = propertyPayment.CheckedOutDate == null ? postDated.OrderByDescending(x => x.DateEnd).Select(x => x.DateEnd).FirstOrDefault() : DateTime.Parse(propertyPayment.CheckedOutDate.ToString());
                    }
                    else if (propertyPayment.isAdvancePayment && propertyPayment.AdvancePaymentMonth != null)
                    {
                        end = start.AddMonths(propertyPayment.AdvancePaymentMonth.Value);
                    }
                    int monthCount = 0;
                    if (propertyPayment.isAdvancePayment)
                    {
                        while (monthCount < propertyPayment.AdvancePaymentMonth)
                        {
                            Income income = new Income();
                            income.Description = "Advance Payment";
                            income.PropertyId = Convert.ToInt32(property.Id);
                            income.Amount = (decimal)rents.Where(x => x.EffectiveDate <= start).OrderByDescending(x => x.EffectiveDate).FirstOrDefault().Amount;
                            income.DueDate = start;
                            income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                            income.Status = 2;
                            income.CompanyId = property.CompanyId;
                            income.DateRecorded = DateTime.UtcNow;
                            income.IsBatchLoad = false;
                            income.IsActive = true;
                            income.BookingId = propertyPayment.BookingId.ToInt();
                            var temp = db.Income.Where(x => (x.Description == "Rent" || x.Description.Contains("Post Dated") || x.Description == "Advance Payment") && (x.DueDate.Year == income.DueDate.Year && x.DueDate.Month == income.DueDate.Month) && x.BookingId == reservation.Id).FirstOrDefault();
                            if (temp == null)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount.ToDecimal().ToString("0,##"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                                db.Income.Add(income);
                                db.SaveChanges();
                            }
                            else
                            {
                                if (temp.Description != income.Description || temp.DueDate != income.DueDate || temp.Amount != income.Amount)
                                {
                                    Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction changes Description: {0} to {1} Due Date:{2} to {3} Amount:{4} to {5} ", temp.Description, income.Description, temp.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount.ToDecimal().ToString("0,##"), income.Amount.ToDecimal().ToString("0,##")), propertyPayment.BookingId.ToInt());
                                    temp.Amount = income.Amount;
                                    temp.Description = income.Description;
                                    temp.DueDate = income.DueDate;
                                    temp.IsActive = true;
                                    db.Entry(temp).State = EntityState.Modified;
                                    db.SaveChanges();
                                }

                            }
                            monthCount++;
                            start = start.AddMonths(1);
                        }
                    }

                    for (DateTime counter = start; counter <= end; counter = counter.AddMonths(1))
                    {
                        bool isPostdated = false;
                        decimal postdateAmount = 0;
                        if (postDated.Count > 0)
                        {
                            foreach (var postdated in postDated)
                            {
                                if (counter >= postdated.DateStart && counter <= postdated.DateEnd)
                                {
                                    postdateAmount = postdated.Amount;
                                    isPostdated = true;
                                }
                            }
                        }
                        Income income = new Income();
                        income.Description = isPostdated ? "Post Dated - " + postdateAmount : "Rent";
                        income.PropertyId = Convert.ToInt32(property.Id);
                        income.Amount = (decimal)rents.Where(x => x.EffectiveDate <= counter).OrderByDescending(x => x.EffectiveDate).FirstOrDefault().Amount;
                        income.DueDate = counter;
                        income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                        income.Status = 1;
                        income.CompanyId = property.CompanyId;
                        income.DateRecorded = DateTime.UtcNow;
                        income.IsBatchLoad = false;
                        income.IsActive = true;
                        income.BookingId = propertyPayment.BookingId.ToInt();
                        var temp = db.Income.Where(x => (x.Description == "Rent" || x.Description.Contains("Post Dated") || x.Description == "Advance Payment") && (x.DueDate.Year == income.DueDate.Year && x.DueDate.Month == income.DueDate.Month) && x.BookingId == reservation.Id).FirstOrDefault();
                        if (temp == null && (propertyPayment.isMonthToMonth && income.Description == "Rent" ? false : true))
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount.ToDecimal().ToString("0,##"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                            db.Income.Add(income);
                            db.SaveChanges();
                        }
                        else if (temp !=null/* &&temp.DueDate < propertyPayment.CheckedOutDate*/)
                        {
                            if (temp.Description != income.Description || temp.DueDate != income.DueDate || temp.Amount != income.Amount)
                            {
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction changes Description: {0} to {1} Due Date:{2} to {3} Amount:{4} to {5} ", temp.Description, income.Description, temp.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount.ToDecimal().ToString("0,##"), income.Amount.ToDecimal().ToString("0,##")), propertyPayment.BookingId.ToInt());
                                temp.Amount = income.Amount;
                                temp.Description = income.Description;
                                temp.DueDate = income.DueDate;
                                temp.IsActive = true;
                                db.Entry(temp).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }
                        start = start.AddMonths(1);

                    }

                //}
                //catch (Exception e){

                //}

                if (propertyPayment.isSecurityDeposit == true)
                {
                    Income income = new Income();
                    income.Description = "Security Deposit";
                    income.PropertyId = Convert.ToInt32(property.Id);
                    income.Amount = decimal.Parse(propertyPayment.SecurityDepositAmount.ToString());
                    income.DueDate = DateTime.Parse(propertyPayment.SecurityDepositDueOn.ToString());
                    income.IncomeTypeId = (int)Core.Enumerations.IncomeType.SecurityDeposit;
                    income.Status = (int)Core.Enumerations.IncomeType.SecurityDeposit;
                    income.CompanyId = property.CompanyId;
                    income.DateRecorded = DateTime.UtcNow;
                    income.IsBatchLoad = false;
                    income.IsActive = true;
                    income.BookingId = propertyPayment.BookingId.ToInt();
                    var temp = db.Income.Where(x => x.Description ==income.Description && x.BookingId == reservation.Id).FirstOrDefault();
                    if (temp == null)
                    {
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount.ToDecimal().ToString("0,##"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), propertyPayment.BookingId.ToInt());
                        db.Income.Add(income);
                        db.SaveChanges();
                    }
                    else
                    {
                        if (temp.Description != income.Description || temp.DueDate != income.DueDate || temp.Amount != income.Amount)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction changes Description: {0} to {1} Due Date:{2} to {3} Amount:{4} to {5} ", temp.Description, income.Description, temp.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), temp.Amount.ToDecimal().ToString("0,##"), income.Amount.ToDecimal().ToString("0,##")), propertyPayment.BookingId.ToInt());
                            temp.Amount = income.Amount;
                            temp.Description = income.Description;
                            temp.DueDate = income.DueDate;
                            temp.IsActive = true;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                var toCancelIncomes = db.Income.Where(x =>x.IsActive &&(propertyPayment.isFixedMonthToMonth == false && propertyPayment.isMonthToMonth == false) && (x.DueDate<propertyPayment.CheckedInDate ||x.DueDate > propertyPayment.CheckedOutDate) && (x.Description == "Rent" || x.Description.Contains("Post Dated") || x.Description == "Advance Payment" || x.Description.Contains("Pro Rated") ) && x.BookingId==propertyPayment.BookingId).ToList();
                foreach(var income in toCancelIncomes)
                {
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("{0} - {1} - Transaction Cancelled", income.DueDate.ToDateTime().ToString("MMM dd,yyyy"), income.Description), propertyPayment.BookingId.ToInt());
                        income.IsActive = false;
                        db.Entry(income).State = EntityState.Modified;
                        db.SaveChanges();
                    Core.API.AllSite.Incomes.CreateBatchPaymentFromCancelledIncome(income.Id);
                }

            }
                return Json(new { success = propertyPayment.BookingId }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region EndContract
        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult EndContract(int bookingId,DateTime endDate)
        {
            using(var db = new ApplicationDbContext())
            {
                var reservation = db.Inquiries.Where(x => x.Id == bookingId).FirstOrDefault();
                reservation.IsActive = false;
                reservation.CheckOutDate = endDate;
                db.Entry(reservation).State = EntityState.Modified;
                db.SaveChanges();
                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Contract End {0}",endDate.ToString("MMM d,yyyy")), bookingId);

                var futureIncomes = db.Income.Where(x => x.DueDate > endDate && x.BookingId==bookingId).ToList();
                foreach (var income in futureIncomes)
                {
                    income.IsActive = false;
                    db.Entry(income);
                    db.SaveChanges();
                }
           
            }
            return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpGet]
        [CheckSessionTimeout]
        public JsonResult GetUnpaidTransaction(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<Income>();
                var incomes = db.Income.Where(x => x.BookingId == bookingId && x.IsActive && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).ToList();
                foreach (var income in incomes)
                {
                    var temp = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                    //JM 09/14/19  amount minus payment to get balance
                    if (temp.Sum(x => x.Amount) < income.Amount)
                    {
                        income.Amount = income.Amount - temp.Sum(x => x.Amount);
                        list.Add(income);
                    }
                }
                return Json(new { success = true, transactions = list.OrderByDescending(x => x.DueDate) }, JsonRequestBehavior.AllowGet);
            }
        }
        #region PropertyPayment
        [HttpPost]
        public async Task<ActionResult> PropertyPayment(LocalPropertyPayment propertyPayment)
        {
            try
            {
                int bookingId = 0;
                using (var db = new ApplicationDbContext())
                {
                  
                    //Guest guest = inboxTemp != null? db.Guests.Where(x => x.Id == inboxTemp.GuestId).FirstOrDefault():null;
                    var threadId = Guid.NewGuid().ToString();
                    var reservationId = Guid.NewGuid().ToString();
                    var confirmationCode = Guid.NewGuid().ToString();

                    List<Income> objList = new List<Income>();

                    var property = db.Properties.Where(x => x.Id == propertyPayment.PropertyId).FirstOrDefault();
                    decimal totalAmount = objList.Where(x => x.PropertyId == property.Id).Sum(x => x.Amount.Value);
                    //ADDING BOOKING//
                    int repeatMonth = (propertyPayment.isMonthToMonth == true ? 1 : 0);
                    Inquiry iq = new Inquiry
                    {
                        RepeatMonth = repeatMonth,
                        SiteType = 0,
                        HostId = 0,
                        ConfirmationCode = confirmationCode,
                        GuestCount = 1,
                        PropertyId = int.Parse(property.ListingId.ToString()),
                        CheckInDate = DateTime.Parse(propertyPayment.CheckedInDate.ToString()),
                        CheckOutDate = (propertyPayment.CheckedOutDate != null ? DateTime.Parse(propertyPayment.CheckedOutDate.ToString()) : (DateTime?)null),
                        BookingDate = DateTime.UtcNow,
                        InquiryDate = DateTime.UtcNow,
                        ServiceFee = totalAmount,
                        CleaningFee = 0,
                        BookingStatusCode = "A",
                        GuestId = 0,//guest.Id,
                        ThreadId = threadId,
                        ReservationId = reservationId,
                        BookingCancelDate = (DateTime?)null,
                        BookingConfirmDate = (DateTime?)null,
                        //CompanyId = property.CompanyId,
                        NetRevenueAmount = totalAmount,
                        IsActive = true,
                        LateFeeAmount = propertyPayment.LateFeeAmount,
                        LateFeeApplyDays = propertyPayment.LateFeeApplyDays,
                        IsMonthToMonth = propertyPayment.isMonthToMonth,
                        RecurringRentStartDate = propertyPayment.RecurringRentStartDate,
                        //RentAmount = propertyPayment.RentAmount,
                        IsFixedMonthToMonth = propertyPayment.isFixedMonthToMonth,
                        SecurityDepositAmount = propertyPayment.SecurityDepositAmount,
                        AdvancePaymentMonth = propertyPayment.AdvancePaymentMonth,
                        IsAdvancePayment = propertyPayment.isAdvancePayment,
                        IsLateFees = propertyPayment.isLateFees,
                        IsPostDatedPayment = propertyPayment.isPostDatedPayment,
                        IsSecurityDeposit = propertyPayment.isSecurityDeposit,
                        //PostPaymentEndDate = propertyPayment.PostPaymentEndDate,
                        //PostPaymentStartDate=propertyPayment.PostPaymentStartDate,
                        SecurityDepositDueOn = propertyPayment.SecurityDepositDueOn,
                        IsLastMonthPayment = propertyPayment.isLastMonthPayment,
                        LastPaymentMonth = propertyPayment.LastPaymentMonth
                    };

                    bookingId = objIList.AddBooking(iq);
                    Core.API.AllSite.ContractChangesLogs.Add(
                        "Contract Start Date:"+propertyPayment.CheckedInDate.ToDateTime().ToString("MMM d,yyyy") +
                        "\nContract End Date:"+(propertyPayment.CheckedOutDate ==null?"No End":propertyPayment.CheckedOutDate.ToDateTime().ToString("MMM d,yyyy"))+ 
                        "\nContract type:"+(propertyPayment.isFixedMonthToMonth ==true?"Fixed term with month to month":propertyPayment.isMonthToMonth==true?"Month to month":"Fixed term")+
                        "\nRecurring Date:" + propertyPayment.RecurringRentStartDate.ToDateTime().ToString("MMM d,yyyy")+
                        "\nHas Advance Payment:" + propertyPayment.isAdvancePayment+
                        "\nHas Last Month Payment:" + propertyPayment.isLastMonthPayment+
                        "\nLast Month:" + propertyPayment.LastPaymentMonth+
                        "\nHas Postdated:" + propertyPayment.isPostDatedPayment+
                        "\nHas Automatic Late fee:" + propertyPayment.isLateFees+
                        "\nLate payment amount:" + propertyPayment.LateFeeAmount+
                        "\nLate fee apply days:" + propertyPayment.LateFeeApplyDays+
                        "\nHas Security deposit:" + propertyPayment.isSecurityDeposit+
                        "\nSecurity deposit amount:" + propertyPayment.SecurityDepositAmount
                        , bookingId);
                    try
                    {
                        
                            Inbox objInbox = new Inbox();
                            objInbox.PropertyId = iq.PropertyId;
                            objInbox.ThreadId = threadId;
                            objInbox.HostId = 0;
                            objInbox.IsArchived = false;
                            objInbox.HasUnread = false;
                            objInbox.GuestId = 0;//guest.Id;
                            objInbox.LastMessageAt = DateTime.UtcNow;
                            objInbox.CheckInDate = iq.CheckInDate;
                            objInbox.CheckOutDate = iq.CheckOutDate != null ? iq.CheckOutDate : (DateTime?)null;
                            objInbox.Status = "A";
                            objInbox.SiteType = 0;
                            //Total Amount//
                            objInbox.Subtotal = totalAmount;
                            objInbox.StatusType = 0;
                            objInbox.ServiceFee = 0;
                            objInbox.RentalFee = 0;
                            objInbox.IsInquiryOnly = false;
                            objInbox.TotalPayout = 0;
                            //Total Amount//
                            objInbox.GuestPay = 0;
                            //Total Amount//
                            objInbox.GuestFee = 0;
                            //objInbox.CompanyId = iq.CompanyId;
                            objInbox.CanWithdrawPreApprovalInquiry = false;
                            objInbox.CanDeclineInquiry = false;
                            objInbox.CanPreApproveInquiry = false;
                            objInbox.CleaningFee = 0;
                            objInbox.IsSpecialOfferSent = false;
                            objInbox.GuestCount = iq.GuestCount;
                            objInbox.Adult = 0;
                            objInbox.Children = 0;
                            db.Inbox.Add(objInbox);
                            db.SaveChanges();
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    try
                    {
                        var renters = JsonConvert.DeserializeObject<List<Renter>>(propertyPayment.RenterString);
                        foreach (var renter in renters)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Create Renter:{0}, Address:{1}, Phone Number:{2}, Email:{3}", renter.Firstname + " " + renter.Lastname, renter.Address, renter.PhoneNumber, renter.Email), bookingId);
                            renter.BookingId = bookingId;
                            renter.Id = 0;
                            db.Renters.Add(renter);
                            db.SaveChanges();
                            //Crea phone number
                            Emails.AddOrUpdate(renter.Id, renter.Email);
                            Contacts.AddOrUpdate(renter.Id, renter.PhoneNumber);
                            var thread = db.CommunicationThreads.Where(x => x.CompanyId == property.CompanyId && x.RenterId == renter.Id).FirstOrDefault();
                            if (thread == null)
                            {

                                CommunicationThread workerThread = new CommunicationThread();
                                db.CommunicationThreads.Add(workerThread);
                                workerThread.RenterId = renter.Id;
                                workerThread.ThreadId = PasswordGenerator.Generate(20);
                                workerThread.CompanyId = property.CompanyId;
                                workerThread.IsRenter = true;
                                db.CommunicationThreads.Add(workerThread);
                                db.SaveChanges();
                            }
                        }
                        var rents = JsonConvert.DeserializeObject<List<MonthlyRent>>(propertyPayment.RentString);
                        foreach (var rent in rents)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Create Rent Amount effectivity Effective Date:{0}, Amount:{1}", rent.EffectiveDate.ToString("MMM dd,yyyy"), rent.Amount.ToString("0.##")), bookingId);
                            rent.BookingId = bookingId;
                            rent.Id = 0;
                            db.MonthlyRents.Add(rent);
                            db.SaveChanges();
                        }
                        var postDated = JsonConvert.DeserializeObject<List<PostDated>>(propertyPayment.PostDatedString);
                        foreach (var postdated in postDated)
                        {  Core.API.AllSite.ContractChangesLogs.Add(string.Format("Create Post date Start Date:{0}, End Date:{1}, Amount:{2}", postdated.DateStart.ToString("MMM dd,yyyy"), postdated.DateEnd.ToString("MMM dd,yyyy"), postdated.Amount.ToString("0.##")), bookingId);
                            postdated.BookingId = bookingId;
                            postdated.Id = 0;
                            postdated.DateEnd = new DateTime(postdated.DateEnd.Year, postdated.DateEnd.Month, DateTime.DaysInMonth(postdated.DateEnd.Year, postdated.DateEnd.Month));
                            db.PostDated.Add(postdated);
                            db.SaveChanges();
                        }


                        var AdditionalFees = JsonConvert.DeserializeObject<List<BookingAdditionalFee>>(propertyPayment.AdditionalFeeString);
                        foreach (var fee in AdditionalFees)
                        {
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Created Billing Description:{0}, Amount:{1}, Due Date:{2}", fee.Description, fee.Amount.ToDecimal().ToString("0.##"), fee.DueOn.ToString("MMM dd,yyyy")), bookingId);
                            fee.BookingId = bookingId;
                            fee.Id = 0;
                            var tempduplicate = db.BookingAdditionalFees.Where(x => x.BookingId == bookingId && x.Description == fee.Description).FirstOrDefault();
                            if (tempduplicate == null)
                            {
                                db.BookingAdditionalFees.Add(fee);
                                db.SaveChanges();
                                //JM 11/4/19 Create transaction base on fee
                                if (fee.Type == 1)
                                {
                                    Income income = new Income();
                                    income.Description = fee.Description;
                                    income.PropertyId = Convert.ToInt32(property.Id);
                                    income.Amount = fee.Amount.Value;
                                    income.DueDate = fee.DueOn;
                                    income.IncomeTypeId = (int)Core.Enumerations.IncomeType.TenantBill;
                                    income.Status = 1;
                                    income.CompanyId = property.CompanyId;
                                    income.DateRecorded = DateTime.UtcNow;
                                    income.IsBatchLoad = false;
                                    income.BookingId = bookingId;
                                    income.IsActive = true;
                                    income.BookingAdditionalFeeId = fee.Id;
                                    db.Income.Add(income);
                                    db.SaveChanges();
                                    Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), bookingId);

                                }
                            }

                        }
                      
                        if (propertyPayment.isLastMonthPayment)
                        {
                            var tempcount = 1;
                            while (tempcount <= propertyPayment.LastPaymentMonth)
                            {
                                Income income = new Income();
                                income.Description = "Last Month Payment " + tempcount;
                                income.PropertyId = Convert.ToInt32(property.Id);
                                income.Amount = decimal.Parse(rents.OrderBy(x => x.EffectiveDate).FirstOrDefault().Amount.ToString());//decimal.Parse(propertyPayment.RentAmount.ToString());
                                income.DueDate = propertyPayment.CheckedInDate;
                                income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                                income.Status = 2;
                                income.CompanyId = property.CompanyId;
                                income.DateRecorded = DateTime.UtcNow;
                                income.IsBatchLoad = false;
                                income.BookingId = bookingId;
                                income.IsActive = true;
                                db.Income.Add(income);
                                db.SaveChanges();
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), bookingId);
                                tempcount++;
                            }

                        }
                        var dt = new DateTime(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month, 1);
                        if (propertyPayment.CheckedInDate != dt)
                        {
                            var lastDayMonth = DateTime.DaysInMonth(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month);
                            var daydiff = Math.Abs((lastDayMonth - propertyPayment.CheckedInDate.Day) + 1);
                            var endOfMonth = new DateTime(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month, lastDayMonth);
                            Income income = new Income();
                            income.Description = "Pro Rated - " + propertyPayment.CheckedInDate.ToString("MMM dd") + "-" + lastDayMonth + "," + propertyPayment.CheckedInDate.Year;
                            income.PropertyId = Convert.ToInt32(property.Id);
                            income.Amount = (decimal.Parse(rents.OrderBy(x => x.EffectiveDate).FirstOrDefault().Amount.ToString()) / (DateTime.DaysInMonth(propertyPayment.CheckedInDate.Year, propertyPayment.CheckedInDate.Month)) * daydiff.ToDecimal());
                            income.DueDate = propertyPayment.CheckedInDate;
                            income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                            income.Status = 1;
                            income.CompanyId = property.CompanyId;
                            income.DateRecorded = DateTime.UtcNow;
                            income.IsBatchLoad = false;
                            income.IsActive = true;
                            objList.Add(income);
                            Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), bookingId);
                        }

                        DateTime start = DateTime.Parse(propertyPayment.RecurringRentStartDate.ToString());
                        DateTime end = DateTime.Parse(propertyPayment.RecurringRentStartDate.ToString());
                        if (postDated.Count != 0 || propertyPayment.CheckedOutDate != null)
                        {
                            end = propertyPayment.CheckedOutDate == null ? postDated.OrderByDescending(x => x.DateEnd).Select(x => x.DateEnd).FirstOrDefault() : DateTime.Parse(propertyPayment.CheckedOutDate.ToString());
                        }
                        else if (propertyPayment.isAdvancePayment && propertyPayment.AdvancePaymentMonth != null)
                        {
                            end = start.AddMonths(propertyPayment.AdvancePaymentMonth.Value);
                        }

                        int monthCount = 0;
                        if (propertyPayment.isAdvancePayment)
                        {
                            while (monthCount < propertyPayment.AdvancePaymentMonth)
                            {
                                Income income = new Income();
                                income.Description = "Advance Payment";
                                income.PropertyId = Convert.ToInt32(property.Id);
                                income.Amount = (decimal)rents.Where(x => x.EffectiveDate <= start).OrderByDescending(x => x.EffectiveDate).FirstOrDefault().Amount;
                                income.DueDate = start;
                                income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                                income.Status = 2;
                                income.CompanyId = property.CompanyId;
                                income.DateRecorded = DateTime.UtcNow;
                                income.IsBatchLoad = false;
                                income.BookingId = bookingId;
                                income.IsActive = true;
                                db.Income.Add(income);
                                db.SaveChanges();
                                start = start.AddMonths(1);
                                monthCount++;
                                Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), bookingId);
                            }
                        }
                       
                            for (DateTime counter = start; counter <= end; counter = counter.AddMonths(1))
                            {
                                bool isPostdated = false;
                                decimal postdateAmount = 0;
                                if (postDated.Count > 0)
                                {
                                    foreach (var postdated in postDated)
                                    {
                                        if (counter >= postdated.DateStart && counter <= postdated.DateEnd)
                                        {
                                            postdateAmount = postdated.Amount;
                                            isPostdated = true;
                                        }
                                    }
                                }
                                Income income = new Income();
                                income.Description = isPostdated ? "Post Dated - " + postdateAmount : "Rent";
                                income.PropertyId = Convert.ToInt32(property.Id);
                                income.Amount = (decimal)rents.Where(x => x.EffectiveDate <= counter).OrderByDescending(x => x.EffectiveDate).FirstOrDefault().Amount;
                                income.DueDate = counter;
                                income.IncomeTypeId = (int)Core.Enumerations.IncomeType.Rent;
                                income.Status = 1;
                                income.CompanyId = property.CompanyId;
                                income.DateRecorded = DateTime.UtcNow;
                                income.IsBatchLoad = false;
                                income.IsActive = true;
                                income.BookingId = bookingId;
                                if ((propertyPayment.isMonthToMonth && income.Description=="Rent" ?false:true))
                                {
                                    db.Income.Add(income);
                                    db.SaveChanges();
                                    Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), bookingId);
                                }
                            }
                        

                        //LIST INCOME//
                    }
                    catch (Exception e)
                    { 
                    }


                    if (propertyPayment.isSecurityDeposit == true)
                    {
                        Income income = new Income();
                        income.Description = "Security Deposit";
                        income.PropertyId = Convert.ToInt32(property.Id);
                        income.Amount = decimal.Parse(propertyPayment.SecurityDepositAmount.ToString());
                        income.DueDate = DateTime.Parse(propertyPayment.SecurityDepositDueOn.ToString());
                        income.IncomeTypeId = (int)Core.Enumerations.IncomeType.SecurityDeposit;
                        income.Status = 1;
                        income.CompanyId = property.CompanyId;
                        income.DateRecorded = DateTime.UtcNow;
                        income.IsBatchLoad = false;
                        income.IsActive = true;
                        objList.Add(income);
                        Core.API.AllSite.ContractChangesLogs.Add(string.Format("Transaction Created Description:{0} Amount:{1} Due Date:{2}", income.Description, income.Amount, income.DueDate.ToDateTime().ToString("MMM dd,yyyy")), bookingId);
                        db.Income.Add(income);
                    }
                    //ADDING INCOME TOTAL//
                    foreach (var item in objList)
                    {
                        item.BookingId = bookingId;
                    }
                    db.Income.AddRange(objList);
                    db.SaveChanges();
                    MonthlyRentJobScheduler.Stop("Rentals" + property.CompanyId);
                    MonthlyRentJobScheduler.Start(property.CompanyId);
                    return Json(new { success = true, bookingId = bookingId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        //public async Task<ActionResult> Test()
        //{
        //    using (var db = new ApplicationDbContext()) { 
        //    for(var date =new DateTime(2022,7,1);date < DateTime.Now.AddMonths(2); date=date.AddDays(1)) {
        //            db.PropertyBookingDate.Add(new PropertyBookingDate()
        //            {

        //                Date = date,
        //                PropertyId = 15271473,
        //                IsAvailable=true,
        //                Price=470,
        //                SiteType=1,
        //            });
        //            db.SaveChanges();
        //        }
          
        //    }
        //        //using (var sm = new Wyze.ScrapeManager())
        //        //{
        //        //    var contextUrl = System.Web.HttpContext.Current.Request.Url;
        //        //    var url = contextUrl.Scheme + "://" + contextUrl.Authority;
        //        //    for (int x = 0; x < 100; x++)
        //        //    {
        //        //        var loginresult = sm.LogIn("vancouverbb99@gmail.com", "FakeAccount1234", url, SessionHandler.CompanyId, false);
        //        //        if (loginresult.Success)
        //        //        {
        //        //        }
        //        //        else
        //        //        {

        //        //        }
        //        //    }


        //        //}
        //        return Content("Test");
        //}

        //string UploadImage(HttpPostedFileBase file, string Destination)
        //{
        //    System.IO.Directory.Exists(Server.MapPath(Destination));
        //    var fileName = Path.GetFileName(file.FileName);
        //    var fileExtension = Path.GetExtension(file.FileName);
        //    string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
        //    file.SaveAs(Server.MapPath(url));
        //    return url.Replace("~", "");
        //}

        //public ActionResult Test()
        //{
        //    //using(var db = new ApplicationDbContext())
        //    //{ var dt = "1:29:38 +08 2020-08-14".ToDateTime();
        //    //    var sms = db.CommunicationSMS.Add(new CommunicationSMS() {From = "+16044465514",To= "+17782004483",ThreadId= "fdYhvzRzV7cqqq7ZkqpT",UserId=0,CreatedDate= dt});
        //    //    db.SaveChanges();
        //    //    db.MMSImages.Add(new MMSImage() { ImageUrl= "/Images/MMS/MEe6db2ffc0a1413542003d576419aac3b.jpg", CommunicationSMSId=sms.Id });
        //    //    db.SaveChanges();
        //    //}
        //    //XElement booksFromFile = XElement.Load(@"C:\Users\Respontage\Desktop\Country.xml");
        //    //var xml = booksFromFile.ToSafeString();
        //    //XmlDocument doc = new XmlDocument();
        //    //doc.LoadXml(xml);
        //    //var json = JsonConvert.SerializeXmlNode(doc, Formatting.None, true);
        //    //var ob = JObject.Parse(json);
        //    //using (var db = new ApplicationDbContext())
        //    //{
        //    //    var count = (JArray)ob["country_state_city"];
        //    //    var countries = count.Where(x => x["name"].ToSafeString() == "Canada" || x["name"].ToSafeString() == "United States");
        //    //    foreach (var country in countries)
        //    //    {
        //    //        var countryname = country["name"].ToSafeString();
        //    //        var countryData = new Country() { Name = countryname };
        //    //        db.Countries.Add(countryData);
        //    //        db.SaveChanges();
        //    //        if (country["states"] is JArray)
        //    //        {
        //    //            var states = (JArray)country["states"];
        //    //            foreach (var state in states)
        //    //            {
        //    //                var statename = state["name"].ToSafeString();
        //    //                var stateData = new State() { Name = statename, CountryId = countryData.Id };
        //    //                db.States.Add(stateData);
        //    //                db.SaveChanges();
        //    //                if (state["cities"] is JArray)
        //    //                {
        //    //                    var cities = (JArray)state["cities"];
        //    //                    foreach (var city in cities)
        //    //                    {
        //    //                        var cityname = city["name"].ToSafeString();
        //    //                        var cityData = new Core.Database.Entity.City() { Name = cityname, StateId = stateData.Id };
        //    //                        db.Cities.Add(cityData);
        //    //                        db.SaveChanges();
        //    //                    }
        //    //                }
        //    //                else if (state["cities"] is JObject)
        //    //                {
        //    //                    var city = (JObject)state["cities"];
        //    //                    var cityname = city["name"].ToSafeString();
        //    //                    var cityData = new Core.Database.Entity.City() { Name = cityname, StateId = stateData.Id };
        //    //                    db.Cities.Add(cityData);
        //    //                    db.SaveChanges();
        //    //                }
        //    //            }
        //    //        }
        //    //        else if (country["states"] is JObject)
        //    //        {
        //    //            var state = (JObject)country["states"];
        //    //            var statename = state["name"].ToSafeString();
        //    //            var stateData = new State() { Name = statename, CountryId = countryData.Id };
        //    //            db.States.Add(stateData);
        //    //            db.SaveChanges();
        //    //            if (state["cities"] is JArray)
        //    //            {
        //    //                var cities = (JArray)state["cities"];
        //    //                foreach (var city in cities)
        //    //                {
        //    //                    var cityname = city["name"].ToSafeString();
        //    //                    var cityData = new Core.Database.Entity.City() { Name = cityname, StateId = stateData.Id };
        //    //                    db.Cities.Add(cityData);
        //    //                    db.SaveChanges();
        //    //                }
        //    //            }
        //    //            else
        //    //            {
        //    //                var city = (JObject)state["cities"];
        //    //                var cityname = city["name"].ToSafeString();
        //    //                var cityData = new Core.Database.Entity.City() { Name = cityname, StateId = stateData.Id };
        //    //                db.Cities.Add(cityData);
        //    //                db.SaveChanges();
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    return Content("");
        //}
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                //If user is owner
                if ((Core.Helper.Company.CompanyType.IsLongTerm() || Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt())))
                {
                    //Scheduler scheduler = new Scheduler();
                    //scheduler.Run(SessionHandler.CompanyId);
                    return RedirectToAction("Index", "Rentals");
                }
                //If user admin,manager
                else if (GlobalVariables.UserWorkerId == 0)
                {
                    //Run Schedulers
                    //Scheduler scheduler = new Scheduler();
                    //scheduler.Run(SessionHandler.CompanyId);
                    // scheduler.RunScrapper();
                    ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();
                    ViewBag.Properties = objIList.GetProperties();
                    ViewBag.IsUserWorker = GlobalVariables.UserWorkerId != 0 ? true : false;
                    ViewBag.CompanyTypes = new CommonRepository().CompanyTypes();
                    return View(GetAdminTaskList().OrderBy(x => x.Inquiry.CheckInDate));
                }
                //If user is worker
                else
                {
                    return View("~/Views/Worker/Index.cshtml", GetWorkerTaskList().OrderBy(x => x.Inquiry.CheckInDate));
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                HttpContext.Session.Abandon();
                ViewBag.SessionState = SessionHandler.IsSessionExpired;
                SessionHandler.IsSessionExpired = false;
                ViewBag.CompanyTypes = new CommonRepository().CompanyTypes();
                ViewBag.SubscriptionPlans = new CommonRepository().SubscriptionPlans();
                return View();
            }
        }

        [NonAction]
        public List<Core.Models.Task.TasksViewModel> GetWorkerTaskList()
        {
            #region Worker Task page
            List<Core.Models.Task.TasksViewModel> model = new List<Core.Models.Task.TasksViewModel>();
            //Get Full Set of Inquiry Task
            using (var db = new ApplicationDbContext())
            {
                //JM 10/8/18 Start new Code
                List<int> propertyParentIds = new List<int>();
                var Properties = db.Properties.Where(x => x.IsParentProperty == false).ToList();

                foreach (var property in Properties)
                {
                    List<Inquiry> Inquiries = new List<Inquiry>();
                    List<long> propertyChildId = new List<long>();
                    //JM 03/05/Add Inquiry with parent property
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();

                    if (parentChild!=null)
                    {
                        if (!propertyParentIds.Contains(parentChild.ParentPropertyId))
                        {
                            propertyChildId = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parentChild.ParentPropertyId select p.ListingId).ToList();
                            //db.Properties.Where(x => x.ParentPropertyId == property.ParentPropertyId).Select(x => x.ListingId).ToList();
                            propertyParentIds.Add(parentChild.ParentPropertyId);
                        }

                        foreach (var p in propertyChildId)
                        {
                            Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == p && x.BookingStatusCode == "A" && x.CheckInDate > DateTime.Now));
                        }

                    }
                    else
                    {//JM 03/05/Add Inquiry without parent property
                        Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.BookingStatusCode == "A" && x.CheckInDate > DateTime.Now));
                    }


                    Inquiries = Inquiries.OrderBy(x => x.CheckInDate).ToList();
                    int count = Inquiries.Count;
                    for (int x = 0; x < count; x++)
                    {
                        Inquiry previousReservation = new Inquiry();
                        if (x > 0)
                        {
                            previousReservation = Inquiries[x - 1];
                        }

                        Inquiry futureReservation = new Inquiry();
                        if (x < count - 1)
                        {
                            futureReservation = Inquiries[x + 1];
                        }

                        var currentReservation = Inquiries[x];

                        var propertyInfo = db.Properties.Where(y => y.ListingId == currentReservation.PropertyId).FirstOrDefault();
                        var guestInfo = db.Guests.FirstOrDefault(y => y.Id == currentReservation.GuestId);
                        if (guestInfo == null)
                        {

                        }
                        if (propertyInfo.TimeZoneName != null)
                        {
                            currentReservation.CheckInDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                            currentReservation.CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                        }

                        var localId = Core.API.AllSite.Properties.GetLocalId(currentReservation.PropertyId);
                        var futurelocalId = Core.API.AllSite.Properties.GetLocalId(futureReservation.PropertyId);
                        var prevlocalId = Core.API.AllSite.Properties.GetLocalId(previousReservation.PropertyId);

                        List<Core.Models.Task.Task> inquiryTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> turnoverTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> bookingTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> checkInTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> checkOutTasks = new List<Core.Models.Task.Task>();

                        var inquiryTasksData = (from task in db.Tasks
                                                join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.INQUIRY
                                                select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();

                        foreach (var task in inquiryTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            inquiryTasks.Add(tempTask);
                        }
                        var turnoverTasksData = (from task in db.Tasks
                                                 join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                 where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.TURNOVER
                                                 select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.TURNOVER).ToList();

                        foreach (var task in turnoverTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            turnoverTasks.Add(tempTask);
                        }
                        var bookingTasksData = (from task in db.Tasks
                                                join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.BOOKING
                                                select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.BOOKING).ToList();

                        foreach (var task in bookingTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            bookingTasks.Add(tempTask);
                        }
                        var checkInTasksData = (from task in db.Tasks
                                                join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_IN
                                                select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_IN).ToList();
                        foreach (var task in checkInTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            checkInTasks.Add(tempTask);
                        }
                        var checkOutTasksData = (from task in db.Tasks
                                                 join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                 where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_OUT
                                                 select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_OUT).ToList();
                        foreach (var task in checkOutTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            checkOutTasks.Add(tempTask);
                        }

                        var workersAssigned = db.WorkerAssignments.Where(y => y.ReservationId == currentReservation.Id && y.PropertyId == localId && y.WorkerId ==GlobalVariables.UserWorkerId).OrderBy(y => y.StartDate).FirstOrDefault();

                        Core.Models.Task.AssignedWorkerInfo assignedWorker = null;

                        if (workersAssigned != null)
                        {
                            var workerInfo = db.Workers.FirstOrDefault(y => y.Id == workersAssigned.WorkerId);
                            if (workerInfo != null)
                            {
                                assignedWorker = new Core.Models.Task.AssignedWorkerInfo();
                                assignedWorker.FirstName = workerInfo.Firstname;
                                assignedWorker.LastName = workerInfo.Lastname;
                                assignedWorker.StartDate = workersAssigned.StartDate;
                                assignedWorker.EndDate = workersAssigned.EndDate;
                                assignedWorker.Mobile = workerInfo.Mobile;
                            }
                        }

                        var preCheckInTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                            join workerJobType in db.WorkerJobTypes
                                                            on workerAssignments.JobTypeId equals workerJobType.Id
                                                            where
                                                                 workerAssignments.WorkerId == GlobalVariables.UserWorkerId &&
                                                                 (workerAssignments.ReservationId == previousReservation.Id &&
                                                                  workerAssignments.PropertyId == prevlocalId &&
                                                                  workerAssignments.StartDate >= previousReservation.CheckOutDate &&
                                                                  workerJobType.ClassificationId == 1)
                                                                  ||
                                                                  (workerAssignments.WorkerId == GlobalVariables.UserWorkerId && workerAssignments.ReservationId == currentReservation.Id &&
                                                                  workerAssignments.PropertyId == localId &&
                                                                  workerAssignments.StartDate <= currentReservation.CheckInDate &&
                                                                  workerJobType.ClassificationId == 1) //Turnover Classification
                                                            select workerAssignments).ToList();

                        var checkInWorkerList = (from workerAssignments in db.WorkerAssignments
                                                 join workerJobType in db.WorkerJobTypes
                                                 on workerAssignments.JobTypeId equals workerJobType.Id
                                                 where
                                                       workerAssignments.WorkerId == GlobalVariables.UserWorkerId &&
                                                       workerAssignments.PropertyId == localId &&
                                                       workerAssignments.ReservationId == currentReservation.Id &&
                                                       DbFunctions.TruncateTime(workerAssignments.StartDate) == DbFunctions.TruncateTime(currentReservation.CheckInDate) &&
                                                       workerJobType.ClassificationId == 3 //Turnover Classification
                                                 select workerAssignments).ToList();

                        var checkOutWorkerList = (from workerAssignments in db.WorkerAssignments
                                                  join workerJobType in db.WorkerJobTypes
                                                  on workerAssignments.JobTypeId equals workerJobType.Id
                                                  where
                                                         workerAssignments.ReservationId == currentReservation.Id &&
                                                         workerAssignments.WorkerId == GlobalVariables.UserWorkerId &&
                                                         workerAssignments.PropertyId == localId &&
                                                         workerAssignments.ReservationId == currentReservation.Id &&
                                                     DbFunctions.TruncateTime(workerAssignments.StartDate) == DbFunctions.TruncateTime(currentReservation.CheckOutDate) &&
                                                        workerJobType.ClassificationId == 3 //Turnover Classification
                                                  select workerAssignments).ToList();

                        var postCheckOutTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                              join workerJobType in db.WorkerJobTypes
                                                              on workerAssignments.JobTypeId equals workerJobType.Id
                                                              where
                                                                   workerAssignments.WorkerId == GlobalVariables.UserWorkerId &&
                                                                  (workerAssignments.ReservationId == currentReservation.Id &&
                                                                    workerAssignments.PropertyId == localId &&
                                                                    workerAssignments.StartDate >= currentReservation.CheckOutDate &&
                                                                    workerJobType.ClassificationId == 1)
                                                                    ||
                                                                    (workerAssignments.WorkerId == GlobalVariables.UserWorkerId &&
                                                                    workerAssignments.ReservationId == futureReservation.Id &&
                                                                    workerAssignments.PropertyId == futurelocalId &&
                                                                    workerAssignments.StartDate <= futureReservation.CheckInDate &&
                                                                    workerJobType.ClassificationId == 1)//Turnover Classification
                                                              select workerAssignments).ToList();
    
                        List<Core.Models.Task.AssignedWorkerInfo> preCheckInTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (preCheckInTurnoverWorkerList.Count > 0)
                        {
                            foreach (var preCheckInTurnoverWorker in preCheckInTurnoverWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == preCheckInTurnoverWorker.WorkerId);
                                if (worker != null)
                                {
                                    //JM 03/05/19 Get the details of assignment
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.Id = preCheckInTurnoverWorker.Id;
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = preCheckInTurnoverWorker.StartDate;
                                    WorkerInfo.EndDate = preCheckInTurnoverWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = preCheckInTurnoverWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = preCheckInTurnoverWorker.InDayStatus.ToInt();
                                    preCheckInTurnoverWorkersInfo.Add(WorkerInfo);
                                }
                            }
                        }

                        List<Core.Models.Task.AssignedWorkerInfo> checkInWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (checkInWorkerList.Count > 0)
                        {
                            foreach (var checkInWorker in checkInWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == checkInWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = checkInWorker.StartDate;
                                    WorkerInfo.EndDate = checkInWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = checkInWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = checkInWorker.InDayStatus.ToInt();
                                    checkInWorkersInfo.Add(WorkerInfo);
                                }
                            }
                        }

                        List<Core.Models.Task.AssignedWorkerInfo> checkOutWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (checkOutWorkerList.Count > 0)
                        {
                            foreach (var checkOutWorker in checkOutWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == checkOutWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = checkOutWorker.StartDate;
                                    WorkerInfo.EndDate = checkOutWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = checkOutWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = checkOutWorker.InDayStatus.ToInt();
                                    checkOutWorkersInfo.Add(WorkerInfo);
                                }
                            }
                        }

                        List<Core.Models.Task.AssignedWorkerInfo> postCheckOutTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (postCheckOutTurnoverWorkerList.Count > 0)
                        {
                            foreach (var postCheckOutTurnoverWorker in postCheckOutTurnoverWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == postCheckOutTurnoverWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = postCheckOutTurnoverWorker.StartDate;
                                    WorkerInfo.EndDate = postCheckOutTurnoverWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = postCheckOutTurnoverWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = postCheckOutTurnoverWorker.InDayStatus.ToInt();
                                    postCheckOutTurnoverWorkersInfo.Add(WorkerInfo);

                                }
                            }
                        }

                        var sortedNoteList = new List<Note>(db.tbNote.Where(y => y.ReservationId == currentReservation.Id));
                        List<Core.Models.Task.TravelEventsViewModel> travelEventList = new List<Core.Models.Task.TravelEventsViewModel>();

                        foreach (var note in sortedNoteList)
                        {
                            Core.Models.Task.TravelEventsViewModel travelEvent = new Core.Models.Task.TravelEventsViewModel
                            {
                                Id = note.Id,
                                ReservationId = note.ReservationId,
                                NoteType = note.NoteType,
                                InquiryNote = note.InquiryNote,
                                FlightNumber = note.FlightNumber,
                                ArrivalDateTime = note.ArrivalDateTime,
                                DepartureDateTime = note.DepartureDateTime,
                                DepartureCityCode = note.DepartureCityCode,
                                LeavingDate = note.LeavingDate,
                                ArrivalAirportCode = note.ArrivalAirportCode,
                                MeetingPlace = note.MeetingPlace,
                                MeetingDateTime = note.MeetingDateTime,
                                People = note.People,
                                Airline = note.Airline,
                                NumberOfPeople = note.NumberOfPeople,
                                CarType = note.CarType,
                                CarColor = note.CarColor,
                                PlateNumber = note.PlateNumber,
                                SourceCity = note.SourceCity,
                                CruiseName = note.CruiseName,
                                AirportCodeDeparture = note.AirportCodeDeparture,
                                AirportCodeArrival = note.AirportCodeArrival,
                                IdentifiableFeature = note.IdentifiableFeature,
                                DepartureCity = note.DepartureCity,
                                DeparturePort = note.DeparturePort,
                                ArrivalPort = note.ArrivalPort,
                                Name = note.Name,
                                TerminalStation = note.TerminalStation,
                                ComparisonDate = travelEventsDepartureTypes.Contains(note.NoteType) ? note.DepartureDateTime : (note.NoteType == (int)Core.Enumerations.NoteType.MEETING ? note.MeetingDateTime : note.ArrivalDateTime)
                                ,
                                Checkin = note.Checkin,
                                Checkout = note.Checkout,
                                ContactPerson = note.ContactPerson
                            };

                            travelEventList.Add(travelEvent);
                        }

                        var sortedTravelEvents = new List<Core.Models.Task.TravelEventsViewModel>(travelEventList.OrderBy(y => y.ComparisonDate));


                        Core.Models.Task.TasksViewModel record = new Core.Models.Task.TasksViewModel
                        {
                            InquiryTask = inquiryTasks,
                            TurnoverTask = turnoverTasks,
                            BookingTask = bookingTasks,
                            CheckInTask = checkInTasks,
                            CheckOutTask = checkOutTasks,
                            PropertyInfo = propertyInfo,
                            GuestInfo = guestInfo,
                            Inquiry = currentReservation,
                            TravelEvents = sortedTravelEvents,
                            //AssignedWorkers = assignedWorker,
                            PreCheckInTurnoverWorkers = preCheckInTurnoverWorkersInfo,
                            CheckInWorkers = checkInWorkersInfo,
                            CheckOutWorkers = checkOutWorkersInfo,
                            PostCheckOutTurnoverWorkers = postCheckOutTurnoverWorkersInfo
                        };
                        model.Add(record);

                    }


                }
                if (GlobalVariables.UserWorkerId != 0)
                {
                    var assignmentReservationIds = db.WorkerAssignments.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).Select(x => x.ReservationId).ToList();


                    foreach (var id in assignmentReservationIds)
                    {
                        model.RemoveAll(x => x.Inquiry.Id != id && !assignmentReservationIds.Contains(x.Inquiry.Id));
                    }
                }
                return model;
            }
            #endregion
        }

        [NonAction]
        public List<Core.Models.Task.TasksViewModel> GetAdminTaskList()
        {
            #region Admin Task page
            List<Core.Models.Task.TasksViewModel> model = new List<Core.Models.Task.TasksViewModel>();
            //Get Full Set of Inquiry Task
            using (var db = new ApplicationDbContext())
            {
                //JM 10/8/18 Start new Code
                List<int> propertyParentIds = new List<int>();
                var Properties = (from h in db.Hosts
                                  join hc in db.HostCompanies on h.Id equals hc.HostId
                                  join p in db.Properties on h.Id equals p.HostId
                                  where p.IsParentProperty == false && hc.CompanyId == SessionHandler.CompanyId
                                  select p).ToList();
                // db.Properties.Where(x => x.IsParentProperty == false && x.CompanyId == SessionHandler.CompanyId).ToList();

                foreach (var property in Properties)
                {
                    List<Inquiry> Inquiries = new List<Inquiry>();
                    List<long> propertyChildId = new List<long>();
                 
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();

                    if (parentChild != null)
                    {
                        if (!propertyParentIds.Contains(parentChild.ParentPropertyId))
                        {
                            propertyChildId = (from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where pc.ParentPropertyId == parentChild.ParentPropertyId select p.ListingId).ToList();
                            //db.Properties.Where(x => x.ParentPropertyId == property.ParentPropertyId).Select(x => x.ListingId).ToList();
                            propertyParentIds.Add(parentChild.ParentPropertyId);
                        }
                        var tempId = propertyChildId.Count >0?propertyChildId[0].ToInt():0;
                        var tempInquiry = db.Inquiries.Where(x => x.PropertyId == tempId && x.BookingStatusCode == "A" && x.CheckInDate < DateTime.Now).OrderByDescending(x => x.CheckInDate).FirstOrDefault();
                        foreach (var p in propertyChildId)
                        {
                            var t = db.Inquiries.Where(x => x.PropertyId == p && x.BookingStatusCode == "A" && x.CheckInDate < DateTime.Now).OrderByDescending(x => x.CheckInDate).FirstOrDefault();
                            if (t != null)
                            {
                                if (tempInquiry!=null && t.CheckInDate > tempInquiry.CheckInDate)
                                {
                                    tempInquiry = t;
                                }
                            }

                            Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == p && x.BookingStatusCode == "A" && x.CheckInDate > DateTime.Now));
                        }
                        if (tempInquiry != null)
                            Inquiries.Add(tempInquiry);
                    }
                    else
                    {
                        var tempInquiry = db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.BookingStatusCode == "A" && x.CheckInDate < DateTime.Now).OrderByDescending(x=>x.CheckInDate).FirstOrDefault();
                        if (tempInquiry != null)
                        {
                            Inquiries.Add(tempInquiry);
                        }
                        Inquiries.AddRange(db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.BookingStatusCode == "A" && x.CheckInDate > DateTime.Now));
                    }


                    Inquiries = Inquiries.OrderBy(x => x.CheckInDate).ToList();
                    int count = Inquiries.Count;
                    for (int x = 0; x < count; x++)
                    {
                        Inquiry previousReservation = new Inquiry();
                        if (x > 0)
                        {
                            previousReservation = Inquiries[x - 1];
                        }

                        Inquiry futureReservation = new Inquiry();
                        if (x < count - 1)
                        {
                            futureReservation = Inquiries[x + 1];
                        }

                        var currentReservation = Inquiries[x];

                        var propertyInfo = db.Properties.Where(y => y.ListingId == currentReservation.PropertyId).FirstOrDefault();
                        var guestInfo = db.Guests.FirstOrDefault(y => y.Id == currentReservation.GuestId);
                        if (guestInfo == null)
                        {

                        }
                        if (propertyInfo.TimeZoneName != null)
                        {
                            currentReservation.CheckInDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckInDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                            currentReservation.CheckOutDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)currentReservation.CheckOutDate, TimeZoneInfo.FindSystemTimeZoneById(propertyInfo.TimeZoneName));
                        }

                        var localId = Core.API.AllSite.Properties.GetLocalId(currentReservation.PropertyId);
                        var futurelocalId = Core.API.AllSite.Properties.GetLocalId(futureReservation.PropertyId);
                        var prevlocalId = Core.API.AllSite.Properties.GetLocalId(previousReservation.PropertyId);

                        List<Core.Models.Task.Task> inquiryTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> turnoverTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> bookingTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> checkInTasks = new List<Core.Models.Task.Task>();
                        List<Core.Models.Task.Task> checkOutTasks = new List<Core.Models.Task.Task>();

                        var inquiryTasksData = (from task in db.Tasks
                                                join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.INQUIRY
                                                select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();

                        foreach (var task in inquiryTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            inquiryTasks.Add(tempTask);
                        }
                        var turnoverTasksData = (from task in db.Tasks
                                                 join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                 where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.TURNOVER
                                                 select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.TURNOVER).ToList();

                        foreach (var task in turnoverTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            turnoverTasks.Add(tempTask);
                        }
                        var bookingTasksData = (from task in db.Tasks
                                                join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.BOOKING
                                                select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.BOOKING).ToList();

                        foreach (var task in bookingTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            bookingTasks.Add(tempTask);
                        }
                        var checkInTasksData = (from task in db.Tasks
                                                join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_IN
                                                select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_IN).ToList();
                        foreach (var task in checkInTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            checkInTasks.Add(tempTask);
                        }
                        var checkOutTasksData = (from task in db.Tasks
                                                 join inqTask in db.InquiryTasks on task.Id equals inqTask.TaskId
                                                 where inqTask.ConfirmationCode == currentReservation.ConfirmationCode && inqTask.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_OUT
                                                 select new { Id = inqTask.Id, Title = task.Title, Description = task.Description, task.ImageUrl, Status = inqTask.Status, DateUpdated = inqTask.DateUpdated }).ToList();
                        //db.InquiryTasks.Where(y => y.ConfirmationCode == currentReservation.ConfirmationCode && y.TemplateTypeId == (int)Core.Enumerations.TaskTemplateType.CHECK_OUT).ToList();
                        foreach (var task in checkOutTasksData)
                        {
                            Core.Models.Task.Task tempTask = new Core.Models.Task.Task();
                            tempTask.Title = task.Title;
                            tempTask.Description = task.Description;
                            tempTask.Id = task.Id.ToString();
                            tempTask.ImageUrl = task.ImageUrl;
                            tempTask.Status = task.Status.ToBoolean();
                            var subs = (from ist in db.InquirySubTask join st in db.SubTasks on ist.SubTaskId equals st.Id where ist.InquiryTaskId == task.Id select new { Id = ist.Id, Title = st.Title, Description = st.Description, Status = ist.Status, ImageUrl = st.ImageUrl, UserId = ist.UserId, LastUpdateDate = ist.LastUpdateDate }).ToList();

                            foreach (var item in subs)
                            {
                                string UpdateBy = (item.UserId != null ? db.Users.Where(y => y.Id == item.UserId).FirstOrDefault().FirstName : null);
                                var sub = new Core.Models.Task.SubTask { Id = item.Id.ToString(), Title = item.Title, Description = item.Description, Status = item.Status, ImageUrl = item.ImageUrl, LastUpdateDate = item.LastUpdateDate, UpdateBy = UpdateBy };
                                tempTask.SubTasks.Add(sub);
                            }
                            checkOutTasks.Add(tempTask);
                        }

                        var workersAssigned = db.WorkerAssignments.Where(y => y.ReservationId == currentReservation.Id && y.PropertyId == localId).OrderBy(y => y.StartDate).FirstOrDefault();

                        Core.Models.Task.AssignedWorkerInfo assignedWorker = null;

                        if (workersAssigned != null)
                        {
                            var workerInfo = db.Workers.FirstOrDefault(y => y.Id == workersAssigned.WorkerId);
                            if (workerInfo != null)
                            {
                                assignedWorker = new Core.Models.Task.AssignedWorkerInfo();
                                assignedWorker.FirstName = workerInfo.Firstname;
                                assignedWorker.LastName = workerInfo.Lastname;
                                assignedWorker.StartDate = workersAssigned.StartDate;
                                assignedWorker.EndDate = workersAssigned.EndDate;
                                assignedWorker.Mobile = workerInfo.Mobile;
                            }
                        }

                        var preCheckInTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                            join workerJobType in db.WorkerJobTypes
                                                            on workerAssignments.JobTypeId equals workerJobType.Id
                                                            where
                                                                 (workerAssignments.ReservationId == previousReservation.Id &&
                                                                  workerAssignments.PropertyId == prevlocalId &&
                                                                  workerAssignments.StartDate >= previousReservation.CheckOutDate &&
                                                                  workerJobType.ClassificationId == 1)
                                                                  ||
                                                                  (workerAssignments.ReservationId == currentReservation.Id &&
                                                                  workerAssignments.PropertyId == localId &&
                                                                  workerAssignments.StartDate <= currentReservation.CheckInDate &&
                                                                  workerJobType.ClassificationId == 1) //Turnover Classification
                                                            select workerAssignments).ToList();

                        var checkInWorkerList = (from workerAssignments in db.WorkerAssignments
                                                 join workerJobType in db.WorkerJobTypes
                                                 on workerAssignments.JobTypeId equals workerJobType.Id
                                                 where
                                                       workerAssignments.PropertyId == localId &&
                                                       workerAssignments.ReservationId ==currentReservation.Id &&
                                                       DbFunctions.TruncateTime(workerAssignments.StartDate) == DbFunctions.TruncateTime(currentReservation.CheckInDate) &&
                                                       //workerAssignments.EndDate <= currentReservation.CheckOutDate &&
                                                       workerJobType.ClassificationId == 3 //Turnover Classification
                                                 select workerAssignments).ToList();

                        var checkOutWorkerList = (from workerAssignments in db.WorkerAssignments
                                                  join workerJobType in db.WorkerJobTypes
                                                  on workerAssignments.JobTypeId equals workerJobType.Id
                                                  where 
                                                        workerAssignments.PropertyId == localId &&
                                                        workerAssignments.ReservationId == currentReservation.Id &&
                                                        DbFunctions.TruncateTime(workerAssignments.StartDate) == DbFunctions.TruncateTime(currentReservation.CheckOutDate) &&
                                                        //DbFunctions.TruncateTime(workerAssignments.EndDate) <= DbFunctions.TruncateTime(currentReservation.CheckOutDate) &&
                                                        workerJobType.ClassificationId == 3 //Turnover Classification
                                                  select workerAssignments).ToList();

                        var postCheckOutTurnoverWorkerList = (from workerAssignments in db.WorkerAssignments
                                                              join workerJobType in db.WorkerJobTypes
                                                              on workerAssignments.JobTypeId equals workerJobType.Id
                                                              where
                                                                  (workerAssignments.ReservationId == currentReservation.Id &&
                                                                    workerAssignments.PropertyId == localId &&
                                                                    workerAssignments.StartDate >= currentReservation.CheckOutDate &&
                                                                    workerJobType.ClassificationId == 1)
                                                                    ||
                                                                    (workerAssignments.ReservationId == futureReservation.Id &&
                                                                    workerAssignments.PropertyId == futurelocalId &&
                                                                    workerAssignments.StartDate <= futureReservation.CheckInDate &&
                                                                    workerJobType.ClassificationId == 1)//Turnover Classification
                                                              select workerAssignments).ToList();

                        //var preCheckInTurnoverWorker = preCheckInTurnoverWorkerList.Any(y => y.AssignmentStatus == 1) ? preCheckInTurnoverWorkerList.OrderByDescending(y => y.StartDate).FirstOrDefault(y => y.AssignmentStatus == 1) : preCheckInTurnoverWorkerList.OrderByDescending(y => y.StartDate).tol();
                        //var checkInWorker = checkInWorkerList.Any(y => y.AssignmentStatus == 3) ? checkInWorkerList.FirstOrDefault(y => y.AssignmentStatus == 3) : checkInWorkerList.FirstOrDefault();
                        //var checkOutWorker = checkOutWorkerList.Any(y => y.AssignmentStatus == 3) ? checkOutWorkerList.FirstOrDefault(y => y.AssignmentStatus == 3) : checkOutWorkerList.FirstOrDefault();
                        //var postCheckOutTurnoverWorker = postCheckOutTurnoverWorkerList.Any(y => y.AssignmentStatus == 1) ? postCheckOutTurnoverWorkerList.FirstOrDefault(y => y.AssignmentStatus == 1) : postCheckOutTurnoverWorkerList.FirstOrDefault();

                        //var preCheckInTurnoverWorker = db.WorkerAssignments.FirstOrDefault(y => y.InquiryId == item.InquiryId && y.PropertyId == item.PropertyId && /*y.WorkerCategory == (int)WorkerCategory.TURNOVER && y.AssignmentStatus &&*/ y.StartDate < item.CheckInDate);
                        //var postCheckOutTurnoverWorker = db.WorkerAssignments.FirstOrDefault(y => y.InquiryId == item.InquiryId && y.PropertyId == item.PropertyId && /*y.WorkerCategory == (int)WorkerCategory.TURNOVER && y.AssignmentStatus &&*/ y.StartDate >= item.CheckOutDate);

                        List<Core.Models.Task.AssignedWorkerInfo> preCheckInTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (preCheckInTurnoverWorkerList.Count > 0)
                        {
                            foreach (var preCheckInTurnoverWorker in preCheckInTurnoverWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == preCheckInTurnoverWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.Id = preCheckInTurnoverWorker.Id;
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = preCheckInTurnoverWorker.StartDate;
                                    WorkerInfo.EndDate = preCheckInTurnoverWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = preCheckInTurnoverWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = preCheckInTurnoverWorker.InDayStatus.ToInt();
                                    preCheckInTurnoverWorkersInfo.Add(WorkerInfo);
                                }
                            }
                        }

                        List<Core.Models.Task.AssignedWorkerInfo> checkInWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (checkInWorkerList.Count > 0)
                        {
                            foreach (var checkInWorker in checkInWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == checkInWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.Id = checkInWorker.Id;
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = checkInWorker.StartDate;
                                    WorkerInfo.EndDate = checkInWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = checkInWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = checkInWorker.InDayStatus.ToInt();
                                    checkInWorkersInfo.Add(WorkerInfo);
                                }
                            }
                        }

                        List<Core.Models.Task.AssignedWorkerInfo> checkOutWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (checkOutWorkerList.Count > 0)
                        {
                            foreach (var checkOutWorker in checkOutWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == checkOutWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.Id = checkOutWorker.Id;
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = checkOutWorker.StartDate;
                                    WorkerInfo.EndDate = checkOutWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = checkOutWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = checkOutWorker.InDayStatus.ToInt();
                                    checkOutWorkersInfo.Add(WorkerInfo);
                                }
                            }
                        }

                        List<Core.Models.Task.AssignedWorkerInfo> postCheckOutTurnoverWorkersInfo = new List<Core.Models.Task.AssignedWorkerInfo>();

                        if (postCheckOutTurnoverWorkerList.Count > 0)
                        {
                            foreach (var postCheckOutTurnoverWorker in postCheckOutTurnoverWorkerList)
                            {
                                var worker = db.Workers.FirstOrDefault(y => y.Id == postCheckOutTurnoverWorker.WorkerId);
                                if (worker != null)
                                {
                                    var WorkerInfo = new Core.Models.Task.AssignedWorkerInfo();
                                    WorkerInfo.Id = postCheckOutTurnoverWorker.Id;
                                    WorkerInfo.FirstName = worker.Firstname;
                                    WorkerInfo.LastName = worker.Lastname;
                                    WorkerInfo.StartDate = postCheckOutTurnoverWorker.StartDate;
                                    WorkerInfo.EndDate = postCheckOutTurnoverWorker.EndDate;
                                    WorkerInfo.Mobile = worker.Mobile;
                                    WorkerInfo.Status = postCheckOutTurnoverWorker.Status.ToInt();
                                    WorkerInfo.InDayStatus = postCheckOutTurnoverWorker.InDayStatus.ToInt();
                                    postCheckOutTurnoverWorkersInfo.Add(WorkerInfo);

                                }
                            }
                        }

                        var sortedNoteList = new List<Note>(db.tbNote.Where(y => y.ReservationId == currentReservation.Id));
                        List<Core.Models.Task.TravelEventsViewModel> travelEventList = new List<Core.Models.Task.TravelEventsViewModel>();

                        foreach (var note in sortedNoteList)
                        {
                            Core.Models.Task.TravelEventsViewModel travelEvent = new Core.Models.Task.TravelEventsViewModel
                            {
                                Id = note.Id,
                                ReservationId = note.ReservationId,
                                NoteType = note.NoteType,
                                InquiryNote = note.InquiryNote,
                                FlightNumber = note.FlightNumber,
                                ArrivalDateTime = note.ArrivalDateTime,
                                DepartureDateTime = note.DepartureDateTime,
                                DepartureCityCode = note.DepartureCityCode,
                                LeavingDate = note.LeavingDate,
                                ArrivalAirportCode = note.ArrivalAirportCode,
                                MeetingPlace = note.MeetingPlace,
                                MeetingDateTime = note.MeetingDateTime,
                                People = note.People,
                                Airline = note.Airline,
                                NumberOfPeople = note.NumberOfPeople,
                                CarType = note.CarType,
                                CarColor = note.CarColor,
                                PlateNumber = note.PlateNumber,
                                SourceCity = note.SourceCity,
                                CruiseName = note.CruiseName,
                                AirportCodeDeparture = note.AirportCodeDeparture,
                                AirportCodeArrival = note.AirportCodeArrival,
                                IdentifiableFeature = note.IdentifiableFeature,
                                DepartureCity = note.DepartureCity,
                                DeparturePort = note.DeparturePort,
                                ArrivalPort = note.ArrivalPort,
                                Name = note.Name,
                                TerminalStation = note.TerminalStation,
                                ComparisonDate = travelEventsDepartureTypes.Contains(note.NoteType) ? note.DepartureDateTime : (note.NoteType == (int)Core.Enumerations.NoteType.MEETING ? note.MeetingDateTime : note.ArrivalDateTime)
                                ,
                                Checkin = note.Checkin,
                                Checkout = note.Checkout,
                                ContactPerson = note.ContactPerson
                            };

                            travelEventList.Add(travelEvent);
                        }

                        var sortedTravelEvents = new List<Core.Models.Task.TravelEventsViewModel>(travelEventList.OrderBy(y => y.ComparisonDate));


                        Core.Models.Task.TasksViewModel record = new Core.Models.Task.TasksViewModel
                        {
                            InquiryTask = inquiryTasks,
                            TurnoverTask = turnoverTasks,
                            BookingTask = bookingTasks,
                            CheckInTask = checkInTasks,
                            CheckOutTask = checkOutTasks,
                            PropertyInfo = propertyInfo,
                            GuestInfo = guestInfo,
                            Inquiry = currentReservation,
                            TravelEvents = sortedTravelEvents,
                            //AssignedWorkers = assignedWorker,
                            PreCheckInTurnoverWorkers = preCheckInTurnoverWorkersInfo,
                            CheckInWorkers = checkInWorkersInfo,
                            CheckOutWorkers = checkOutWorkersInfo,
                            PostCheckOutTurnoverWorkers = postCheckOutTurnoverWorkersInfo
                        };
                        if(x>0)
                        model.Add(record);

                    }


                }
                if (GlobalVariables.UserWorkerId != 0)
                {
                    var assignmentReservationIds = db.WorkerAssignments.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).Select(x => x.ReservationId).ToList();


                    foreach (var id in assignmentReservationIds)
                    {
                        model.RemoveAll(x => x.Inquiry.Id != id && !assignmentReservationIds.Contains(x.Inquiry.Id));
                    }
                }

                return model;
            }
            #endregion
        }

        // added profit loss functionality from other developer 9/30/17
        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult GetGraphList(int status, int indexPage)
        {
            ICommonRepository objIList = new CommonRepository();

            int companyid = SessionHandler.CompanyId;
            var result = objIList.GetPropertiesListForReport(companyid, status, indexPage);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult GetTotalMonth(int status, int indexPage)
        {
            ICommonRepository objIList = new CommonRepository();
            int companyid = Convert.ToInt32(SessionHandler.CompanyId);
            var result = objIList.GetTotalMonth(companyid, status, indexPage);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public void GetAirbnbHostAccountDetails()
        //{
        //    try
        //    {
        //        Trace.WriteLine("Get airbnb host account started..");
        //        SiteConstants.accounts.Clear();
        //        using (var db = new Core.Database.Context.ApplicationDbContext())
        //        {
        //            db.Hosts.Where(x => x.CompanyId == SiteConstants.CompanyId).ToList().ForEach(item =>
        //            {
        //                AirbnbAccount a = new AirbnbAccount();
        //                a.Id = item.Id;
        //                a.Username = item.Username;
        //                a.Password = Security.Decrypt(item.Password);

        //                var entity = db.UserSessions.Where(x => x.OwnerId == item.Id).Where(x => x.IsActive == true).FirstOrDefault();
        //                if (entity != null)
        //                {
        //                    using (ScrapeManager sm = new ScrapeManager())
        //                    {
        //                        var isloaded = sm.ValidateTokenAndLoadCookies(entity.SessionId);
        //                        if (isloaded)
        //                        {
        //                            a.SessionToken = entity.SessionId;
        //                            SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                        }
        //                        else
        //                        {
        //                            db.UserSessions.RemoveRange(db.UserSessions.Where(x => x.OwnerId == entity.OwnerId).ToList());
        //                            db.SaveChanges();
        //                            var result = sm.Login(a.Username, a.Password, true);
        //                            a.SessionToken = result.SessionToken;
        //                            SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    using (ScrapeManager sm = new ScrapeManager())
        //                    {
        //                        var result = sm.Login(a.Username, a.Password, true);
        //                        a.SessionToken = result.SessionToken;
        //                        SiteConstants.accounts.Add(item.Id.ToString(), a);
        //                    }
        //                }
        //            });
        //        }
        //        Trace.WriteLine("Get airbnb host account done..");
        //    }
        //    catch(Exception e)
        //    {
        //        Trace.WriteLine(e.ToString());
        //    }
        //}

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult LoadDashboardMessages()
        {
            if (User.Identity.IsAuthenticated)
            {
                var inquiry = objIList.DashboardMessages("I");
                var booking = objIList.DashboardMessages("B");
                var active = objIList.DashboardMessages("A");
                return Json(new { success = true, inquiry = objIList.GetDashboardMessageHtml(inquiry), booking = objIList.GetDashboardMessageHtml(booking), active = objIList.GetDashboardMessageHtml(active)}, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, message = "Unauthorized User" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult CheckNewMessage()
        {
            try
            {
                var inquiry = objIList.DashboardMessages("I");
                var booking = objIList.DashboardMessages("B");
                var active = objIList.DashboardMessages("A");
                return Json(new { success = true, inquiry = objIList.GetDashboardMessageHtml(inquiry), booking = objIList.GetDashboardMessageHtml(booking), active = objIList.GetDashboardMessageHtml(active) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) { return Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet); }
        }

        // added functionality to insert/update/delete worker job types 9/30/17
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetAllJobTypes(string type)
        {
            using (var db = new ApplicationDbContext())
            {
                List<JobTypeTableData> job = new List<JobTypeTableData>();
                var data = db.WorkerJobTypes.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Type == 1 || x.Type == 2).ToList();
                if (type != "all")
                {
                    int jobType = int.Parse(type);
                    data = data.Where(x => x.Type == jobType).ToList();
                }
                data.ForEach(x =>
                {
                    JobTypeTableData temp = new JobTypeTableData();
                    temp.Id = x.Id.ToString();
                    temp.Name = x.Name;
                    temp.Type = x.Type == 1 ? "Private" : x.Type == 2 ? "Public" : "";
                    temp.ClassificationType = db.WorkerClassifications.FirstOrDefault(y => y.Id == x.ClassificationId).Name;  // Add by Danial 10-3-2018
                    temp.Actions = x.Type == 1 ? ("<a class=\"ui mini button edit-job-type-btn\" title=\"Edit\">" +
                                          "<i class=\"icon edit\"></i>" +
                                          "</a>" +
                                          "<a class=\"ui mini button delete-job-type-btn\" title=\"Delete\">" +
                                              "<i class=\"icon ban\"></i>" +
                                              "</a>") : "";
                    job.Add(temp);
                });
                return Json(new { data = job.OrderBy(x => x.Name) }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddJobType(string name, int classificationId)
        {
            using (var db = new ApplicationDbContext())
            {
                WorkerJobType worker = new WorkerJobType
                {
                    Type = 1,
                    Name = name,
                    CompanyId = SessionHandler.CompanyId,
                    ClassificationId = classificationId
                };
                db.WorkerJobTypes.Add(worker);
                db.SaveChanges();
            }
            return Json(new { success = true, message = "Job Type Added" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UpdateJobType(string name, int id, int classificationId)
        {
            using (var db = new ApplicationDbContext())
            {
                var job = db.WorkerJobTypes.Find(id);
                job.Name = name;
                job.ClassificationId = classificationId;
                job.CompanyId = SessionHandler.CompanyId;
                db.Entry(job).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true, message = "Job Type Updated" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult DeleteJobType(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var b = db.WorkerJobTypes.Where(x => x.Id == id).FirstOrDefault();
                db.WorkerJobTypes.Remove(b);
                db.SaveChanges();
            }
            return Json(new { success = true, message = "Job Type Deleted" }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult WorkerJobDetails(int id)
        {

            bool isSuccess = false;
            WorkerJobType worker = new WorkerJobType();
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    worker = db.WorkerJobTypes.FirstOrDefault(i => i.Id == id);
                    if (worker != null) isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, worker = worker }, JsonRequestBehavior.AllowGet);
        }
        // end worker job type

        public ActionResult Manager1()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Manager2()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        public ActionResult Account1()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        public ActionResult Account2()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        #region "Account Management"

        [HttpGet]
        public ActionResult GetLogin()
        {
            return PartialView("_PartialGetLogin");
        }

        [HttpPost]
        public ActionResult GetLogin(User tbUser)
        {
            try
            {
                GlobalVariables.Username = tbUser.UserName;
                GlobalVariables.Password = tbUser.Password;
                ICommonRepository objCommon = new CommonRepository();
                //encrypting the password for security
                tbUser.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], tbUser.Password.Trim(), true);
                var result = objCommon.Login(tbUser);
                if (result != null)
                {
                    if (result.ValidStatus == false)
                    {
                        return Json("V1", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //TokenManager tokenManager = new TokenManager();
                        //var token = tokenManager.GenerateToken(result.Id.ToString());   
                        FormsAuthenticationTicket(result.Id, result.RoleName);
                        GlobalVariables.UserWorkerId = result.WorkerId.ToInt();
                        GlobalVariables.UserId = result.Id;
                        //SessionHandler.CompanyId = result.CompanyId;
                        SessionHandler.IsSessionExpired = false;

                        return Json("S1", JsonRequestBehavior.AllowGet);
                      
                    }
                }
                else
                {
                    return Json("A1", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetRegister()
        {
            return PartialView("_PartialGetRegister");
        }

        public async Task<ActionResult> Logout()
        {
            using (HttpClient client = new HttpClient())
            {
                //List<int> sites = new List<int>() { 1, 2, 3, 4 };
                //foreach (var siteType in sites)
                //{
                    //client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(1));
                    //var reqParams = new Dictionary<string, string>();
                    //reqParams.Add("userId", User.Identity.Name);
                    //reqParams.Add("companyId", SessionHandler.CompanyId.ToString());
                    ////reqParams.Add("siteType", siteType.ToString());
                    //var reqContent = new FormUrlEncodedContent(reqParams);
                    //var result = await client.PostAsync("Home/Logout", reqContent);
                    //if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    //{

                    //}
                //}
            }

            FormsAuthentication.SignOut();
            Session.Abandon();
            return Redirect("/");
        }

        public ActionResult ValidateAccount(string username)
        {
            objIList.UpdateValidStatus(Convert.ToInt32(username));
            return Redirect("/");
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetRegister(UserRoleModel objUser)
        {
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                //encrypting the password for security
                string userPassword = objUser.Password;
                objUser.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], objUser.Password.Trim(), true);

                // setting the ParentId of subaccount.
                if (User.Identity.IsAuthenticated)
                {
                    if (User.Identity.Name.Trim() != "")
                    {
                        //objUser.CompanyId = Convert.ToInt32(User.Identity.Name);
                    }
                }
                //checking the the already existing or not.
                var resultValid = objCommon.ValidUser(objUser.UserName);
                if (resultValid != null)
                {
                    return Json("A1", JsonRequestBehavior.AllowGet);
                }

                User objtbUser = new User();
                objtbUser.FirstName = objUser.FirstName;
                objtbUser.LastName = objUser.LastName;
                objtbUser.Password = objUser.Password;
                objtbUser.TimeZone = objUser.TimeZone;
                objtbUser.UserName = objUser.UserName;
                objtbUser.ValidStatus = objUser.ValidStatus;
                objtbUser.Status = objUser.Status;
                objtbUser.ConfirmEmail = objUser.ConfirmEmail;
                objtbUser.CompanyId = objUser.CompanyId;
                objtbUser.Status = true;
                objUser.ValidStatus = false;
                objtbUser.CreatedDate = DateTime.Now;
                //saving the detail of register user into db
                var result = objCommon.SaveUser(objtbUser);

                //inserting role of user into rolelink table
                if (objUser.CompanyId == 0)
                {
                    tbRoleLink objRoleLink = new tbRoleLink();
                    objRoleLink.RoleId = 1;
                    objRoleLink.UserId = result.Id;
                    objCommon.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);
                }
                else
                {
                    for (int i = 0; i < objUser.RoleId.Count(); i++)
                    {
                        tbRoleLink objRoleLink = new tbRoleLink();
                        objRoleLink.RoleId = objUser.RoleId[i];
                        objRoleLink.UserId = result.Id;
                        objCommon.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);
                    }
                }
                if (result != null)
                {
                    //sending email to username for check the valid email.
                    if (objUser.CompanyId == 0)
                    {
                        Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p>Hi " + objtbUser.FirstName + ' ' + objtbUser.LastName + ",</p><p> Thank you for creating new account on messager website. <br style='clear:both' /> <p>Please click on following link to validate the account</p> <br style='clear:both' /> <a href='" + string.Format("{0}://{1}{2}",
                                                           HttpContext.Request.Url.Scheme,
                                                           HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                           (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                           ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>Click to validate the account</a> </div>", objUser.UserName.Trim(), "Respontage.com");
                    }
                    else
                    {
                        Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p>Hi " + objtbUser.FirstName + ' ' + objtbUser.LastName + ",</p><p> Thank you for creating new account on messager website. <br style='clear:both' /> <p>Please click on following link to validate the account, Your password is " + userPassword + "</p> <br style='clear:both' /> <a href='" + string.Format("{0}://{1}{2}",
                                                        HttpContext.Request.Url.Scheme,
                                                        HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                        (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                        ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>Click to validate the account</a> </div>", objUser.UserName.Trim(), "Respontage.com");
                    }
                    return Json("S1", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("E1", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        private void FormsAuthenticationTicket(int loginId, string rolename)
        {
            FormsAuthenticationTicket tk = default(FormsAuthenticationTicket);
            tk = new FormsAuthenticationTicket(1, loginId.ToString(), DateTime.Now, DateTime.Now.AddHours(4), false, rolename);
            string st = null;
            st = FormsAuthentication.Encrypt(tk);
            HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, st);
            Response.Cookies.Add(ck);
        }

        #endregion

        #region "WORKER"
        [CheckSessionTimeout]
        public ActionResult Worker(int companyId)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated)
            {
                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Worker") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //it is used to check the valid account to access this page
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //getting page url list for user as per role
                ViewBag.WorkerList = objIList.GetWorkers();
                ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();
                ViewBag.WorkerClassificationTypes = objIList.GetWorkerClassifications();
                ViewBag.TemplateList = objIList.GetWorkerTemplateMessages();
                //ViewBag.Properties = objIList.GetProperties();
                List<PropertyAutoAssignModel> PropertyList = new List<PropertyAutoAssignModel>();
                using (var db = new ApplicationDbContext())
                {
                    var localProperties = new List<Property>();
                    var filterProperties = new List<Property>();
                    #region Get filter of property 
                    //only Single,Sync,Multi units are included
                    var properties = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where (hc.CompanyId == SessionHandler.CompanyId && p.SiteType != 3) && p.IsActive
                            select p).ToList();
                    properties.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());
                    foreach (var local in properties)
                    {
                        var t = db.ParentChildProperties.Where(x => x.ChildPropertyId == local.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (t == null && local.IsParentProperty == false)
                            filterProperties.Add(local);
                        else if (local.IsParentProperty && local.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts)
                        {
                            filterProperties.Add(local);
                        }
                    }
                    ViewBag.Properties = filterProperties;
                    #endregion

                    #region Non booking property
                    //only Multi unit and local chiild or single property included
                    var templocals = db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 0).ToList();
                    foreach (var local in templocals)
                    {
                        //var t = db.ParentChildProperties.Where(x => x.ChildPropertyId == local.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (local.IsParentProperty == false)
                            localProperties.Add(local);
                        else if (local.IsParentProperty && local.ParentType == (int)Core.Enumerations.ParentPropertyType.MultiUnit)
                        {
                            localProperties.Add(local);
                        }
                    }
                    ViewBag.LocalProperties = localProperties;
                    #endregion

                    List<WorkerAndJobTypeModal> workerAndJobTypeModals = new List<WorkerAndJobTypeModal>();
                    var temp = (from w in db.Workers join wj in db.WorkerJobs on w.Id equals wj.WorkerId join jt in db.WorkerJobTypes on wj.JobTypeId equals jt.Id join jc in db.WorkerClassifications on jt.ClassificationId equals jc.Id select new { Worker = w, JobType = jt }).ToList();
                    foreach (var item in temp)
                    {
                        workerAndJobTypeModals.Add(new WorkerAndJobTypeModal() { JobType = item.JobType, Worker = item.Worker });

                    }
                    ViewBag.WorkerJobType = workerAndJobTypeModals;
                    //JM 03/15/19 Uncomment this to use all Classification in Property Auto Assignment
                    //ViewBag.Classifications = db.WorkerClassifications.ToList();
                    List<Property> Properties = new List<Property>();
                    ViewBag.Classifications = db.WorkerClassifications.Where(x => x.Id == 1 || x.Id == 3).ToList();
                    //JM 10/30/19 Check if have parent sync
                    var localProp = db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.IsParentProperty ==false).ToList();
                    foreach(var p in localProp)
                    {
                        var pc = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType ==(int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (pc == null)
                            Properties.Add(p);
                    }

                    //Get all properties
                    Properties.AddRange((from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where (hc.CompanyId == SessionHandler.CompanyId && p.SiteType != 3) && p.IsActive
                            select p).ToList());
                    Properties.AddRange(db.Properties.Where(x => (x.IsParentProperty && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());
                    
                    foreach (var p in Properties)
                    {
                        //Check property is child and have parent
                        var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        if (parentChild != null && p.IsParentProperty == false)
                        {
                            continue;
                        }
                     
                        PropertyAutoAssignModel AutomatedAssignment = new PropertyAutoAssignModel();
                        AutomatedAssignment.Property = p;
                        AutomatedAssignment.Host = db.Hosts.Where(x => x.Id == p.HostId).FirstOrDefault();
                        var autoAssign = db.PropertyDefaultWorkers.Where(x => x.PropertyId == p.Id).ToList();
                        foreach (var a in autoAssign)
                        {
                            AssignWorker assigned = new AssignWorker();
                            assigned.Id = a.Id;
                            assigned.Worker = db.Workers.Where(x => x.Id == a.WorkerId).FirstOrDefault();
                            assigned.JobType = db.WorkerJobTypes.Where(x => x.Id == a.JobTypeId).FirstOrDefault();
                            assigned.Type = a.Type;
                            assigned.IsAutoDelete = a.IsAutoDelete;
                            AutomatedAssignment.Worker.Add(assigned);
                        }

                        if (p.IsParentProperty)
                        {
                            var Childs = db.ParentChildProperties.Where(x => x.ParentPropertyId == p.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).Select(x => x.ChildPropertyId).ToList();
                            foreach (var childId in Childs)
                            {
                                var childproperty = db.Properties.Where(x => x.Id == childId).FirstOrDefault();
                                PropertyAutoAssignModel ChildAutomatedAssignment = new PropertyAutoAssignModel();
                                ChildAutomatedAssignment.Property = childproperty;
                                ChildAutomatedAssignment.Host = db.Hosts.Where(x => x.Id == childproperty.HostId).FirstOrDefault();
                                var childautoAssign = db.PropertyDefaultWorkers.Where(x => x.PropertyId == childproperty.Id).ToList();
                                foreach (var a in childautoAssign)
                                {
                                    AssignWorker assigned = new AssignWorker();
                                    assigned.Id = a.Id;
                                    assigned.Worker = db.Workers.Where(x => x.Id == a.WorkerId).FirstOrDefault();
                                    assigned.JobType = db.WorkerJobTypes.Where(x => x.Id == a.JobTypeId).FirstOrDefault();
                                    assigned.Type = a.Type;
                                    assigned.IsAutoDelete = a.IsAutoDelete;
                                    ChildAutomatedAssignment.Worker.Add(assigned);
                                }
                                AutomatedAssignment.ChildAutoAssignModel.Add(ChildAutomatedAssignment);
                            }
                        }
                        PropertyList.Add(AutomatedAssignment);
                    }
                }
                return View(PropertyList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }


        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult AddWorker()
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();
            return PartialView("_PartialAddWorker");
        }

        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddWorker(FormCollection f, HttpPostedFileWrapper ImageUrl)
        {
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                Worker worker = new Worker();
                worker.Firstname = f["Firstname"].ToString();
                worker.Lastname = f["Lastname"].ToString();
                worker.Address = f["Address"].ToString();
                worker.Bonus = f["bonus"].ToDecimal();
                if (f["fixed-rate"].ToString() != null)
                {
                    worker.FixedRate = f["fixed-rate"].ToDecimal();
                    worker.Frequency = f["frequency-rate"].ToInt();
                }
                worker.CompanyId = SessionHandler.CompanyId;

                if (ImageUrl != null)
                {
                    worker.ImageUrl = UploadImage(ImageUrl, "~/Images/Worker/");
                }
                int workerId = objIList.AddWorker(worker);
              
                if (workerId != 0)
                {

                    try
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            var thread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && x.WorkerId == worker.Id).FirstOrDefault();
                            if (thread == null)
                            {
                       
                                CommunicationThread workerThread = new CommunicationThread();
                                db.CommunicationThreads.Add(workerThread);
                                workerThread.WorkerId = worker.Id;
                                workerThread.ThreadId = PasswordGenerator.Generate(20);
                                workerThread.CompanyId = SessionHandler.CompanyId;
                                db.CommunicationThreads.Add(workerThread);
                                db.SaveChanges();

                            }
                            List<string> RetainerRates = f["retainer_rate"] == null ? new List<string>() : f["retainer_rate"].Split(',').ToList();
                            List<string> RetainerFrequency = f["retainer_frequency"] == null ? new List<string>() : f["retainer_frequency"].Split(',').ToList();
                            List<string> RetainerDates = f["retainer_date"] == null ? new List<string>() : f["retainer_date"].Split(',').ToList();
                            for (int x = 0; x < RetainerRates.Count; x++)
                            {
                                if (RetainerDates[x] != "" && RetainerRates[x] != "" && RetainerFrequency[x] != "")
                                {
                                    var date = RetainerDates[x].ToDateTime();
                                    var effectiveDate = new DateTime(date.Year, date.Month, 1);
                                    var temp = db.WorkerRetainers.Where(y => y.WorkerId == worker.Id && y.EffectiveDate == effectiveDate).FirstOrDefault();
                                    if (temp == null)
                                    {
                                        WorkerRetainer wr = new WorkerRetainer();
                                        wr.EffectiveDate = effectiveDate;
                                        wr.Rate = RetainerRates[x].ToDecimal();
                                        wr.Frequency = RetainerFrequency[x].ToInt();
                                        wr.WorkerId = worker.Id;
                                        db.WorkerRetainers.Add(wr);
                                        db.SaveChanges();
                                    }
                                }
                            }
                            List<string> ContactInfos = f["contactNumber"] == null ? new List<string>() { } : f["contactNumber"].Split(',').ToList();

                            foreach (var item in ContactInfos)
                            {
                                var temp = db.WorkerContactInfos.Where(x => x.WorkerId == workerId && x.Value == item).FirstOrDefault();
                                if (temp == null)
                                {
                                    WorkerContactInfo contact = new WorkerContactInfo()
                                    {
                                        WorkerId = workerId,
                                        Type = 1,
                                        Value = item
                                    };
                                    db.WorkerContactInfos.Add(contact);
                                    db.SaveChanges();

                                }
                            }

                            List<int> worker_type_id_list = f["worker_job_id"] == null ? new List<int>() { } : f["worker_job_id"].Split(',').Select(x => x.ToInt()).ToList<int>();
                            List<int> worker_payment_type = f["payment_type"] == null ? new List<int>() : f["payment_type"].Split(',').Select(x => x.ToInt()).ToList<int>();
                            List<string> worker_payment_rate = f["payment_rate"] == null ? new List<string>() : f["payment_rate"].Split(',').ToList();

                            int count = 0;
                            foreach (var item in worker_type_id_list)
                            {
                                try
                                {
                                    WorkerJob wj = new WorkerJob();
                                    wj.JobTypeId = worker_type_id_list[count].ToInt();
                                    wj.PaymentType = worker_payment_type[count].ToInt();
                                    wj.PaymentRate = worker_payment_rate[count].ToDecimal();

                                    wj.WorkerId = workerId;
                                    db.WorkerJobs.Add(wj);
                                    count++;
                                }
                                catch { }
                            }
                            db.SaveChanges();
                        }

                        User objtbUser = new User();
                        objtbUser.FirstName = worker.Firstname;
                        objtbUser.LastName = worker.Lastname;
                        objtbUser.UserName = f["Username"].ToString();
                        //objtbUser.TimeZone = objUser.TimeZone;
                        objtbUser.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], f["Password"].ToString().Trim(), true);

                        objtbUser.ValidStatus = false;
                        objtbUser.Status = true;
                        objtbUser.ConfirmEmail = false;
                        objtbUser.CompanyId = SessionHandler.CompanyId;
                        objtbUser.Status = true;
                        objtbUser.ValidStatus = false;
                        objtbUser.CreatedDate = DateTime.Now;
                        objtbUser.WorkerId = workerId;
                        //saving the detail of register user into db
                        var result = objIList.SaveUser(objtbUser);



                        tbRoleLink objRoleLink = new tbRoleLink();
                        objRoleLink.RoleId = 4;
                        objRoleLink.UserId = result.Id;
                        objIList.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);


                        if (result != null)
                        {
                            //sending email to username for check the valid email.
                            if (SessionHandler.CompanyId == 0)
                            {
                                Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                                         HttpContext.Request.Url.Scheme,
                                                         HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                         (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                         ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>link</a> to verify your email.</p></div>", objtbUser.UserName.Trim(), "Respontage.com");
                            }
                            else
                            {
                                Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                                        HttpContext.Request.Url.Scheme,
                                                        HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                        (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                        ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>link</a> to verify your email. </p><p>Here is your temporary password - <span style=\"color: #f95c41\">" + f["Password"].ToString() + "</span>.</p></div>", objtbUser.UserName.Trim(), "Respontage.com");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                    }



                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult EditWorker(int intWorkerId)
        {
            ICommonRepository objIList = new CommonRepository();
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                using (var db = new ApplicationDbContext())
                {
                var user = db.Users.Where(x => x.WorkerId == intWorkerId).FirstOrDefault();
                ViewBag.HasUserAccount = (user != null ? true : false);
                ViewBag.WorkerJobTypes = objIList.GetWorkerJobTypes();
                ViewBag.WorkerJobs = objIList.GetWorkerJobAssignments(intWorkerId);
                ViewBag.ContactNumber = db.WorkerContactInfos.Where(x => x.WorkerId == intWorkerId).ToList();
                ViewBag.Retainers = db.WorkerRetainers.Where(x => x.WorkerId == intWorkerId).OrderByDescending(x=>x.EffectiveDate).ToList();
                Worker objWorker = objIList.GetWorker(intWorkerId);
                return PartialView("_PartialEditWorker", objWorker);
                }
            }
            catch (Exception e)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

      
      
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult EditWorker(FormCollection f, HttpPostedFileWrapper ImageUrl)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                Worker worker = new Worker();
                worker.Id = f["Id"].ToInt();
                worker.Firstname = f["Firstname"].ToString();
                worker.Lastname = f["Lastname"].ToString();
                worker.Address = f["Address"].ToString();
                worker.Bonus = f["bonus"].ToDecimal();
                if (ImageUrl != null)
                {
                    worker.ImageUrl=UploadImage(ImageUrl, "~/Images/Worker/");
                }
                if (f["fixed-rate"].ToString() != null)
                {
                    worker.FixedRate = f["fixed-rate"].ToDecimal();
                    worker.Frequency = f["frequency-rate"].ToInt();
                }

                //if (f["retainer-rate"].ToString() != null)
                //{
                //    worker.RetainerRate = f["retainer-rate"].ToDecimal();
                //    worker.RetainerFrequency = f["retainer-frequency"].ToInt();
                //}


                List<WorkerJob> workerJobs = new List<WorkerJob>();

                List<string> worker_type_id_list = f["worker_job_id"] == null ? new List<string>() { } : f["worker_job_id"].Split(',').ToList();
                List<string> worker_payment_type = f["payment_type"] == null ? new List<string>() : f["payment_type"].Split(',').ToList();
                List<string> worker_payment_rate = f["payment_rate"] == null ? new List<string>() : f["payment_rate"].Split(',').ToList();
                
            
                int count = 0;
                foreach (var item in worker_type_id_list)
                {
                    try
                    {
                        WorkerJob wj = new WorkerJob();
                        wj.JobTypeId = worker_type_id_list[count].ToInt();
                        wj.PaymentType = worker_payment_type[count].ToInt();
                        wj.PaymentRate = worker_payment_rate[count].ToDecimal();

                        workerJobs.Add(wj);
                        count++;
                    }
                    catch { }
                }

                using (var db = new ApplicationDbContext())
                {

                    List<string> ContactInfos = f["contactNumber"] == null ? new List<string>() { } : f["contactNumber"].Split(',').ToList();
                    db.WorkerContactInfos.RemoveRange(db.WorkerContactInfos.Where(x => x.WorkerId == worker.Id).ToList());
                    db.SaveChanges();
                    foreach (var item in ContactInfos)
                    {
                        var temp = db.WorkerContactInfos.Where(x => x.WorkerId == worker.Id && x.Value == item).FirstOrDefault();
                        if (temp == null)
                        {
                            WorkerContactInfo contact = new WorkerContactInfo()
                            {
                                WorkerId = worker.Id,
                                Type = 1,
                                Value = item
                            };
                            db.WorkerContactInfos.Add(contact);
                            db.SaveChanges();

                        }
                    }

                    List<string> RetainerRates = f["retainer_rate"] == null ? new List<string>() : f["retainer_rate"].Split(',').ToList();
                    List<string> RetainerFrequency = f["retainer_frequency"] == null ? new List<string>() : f["retainer_frequency"].Split(',').ToList();
                    List<string> RetainerDates = f["retainer_date"] == null ? new List<string>() : f["retainer_date"].Split(',').ToList();
                   
                    db.WorkerRetainers.RemoveRange(db.WorkerRetainers.Where(x => x.WorkerId == worker.Id).ToList());
                    db.SaveChanges();
                    for (int x = 0; x < RetainerRates.Count; x++)
                    {
                        if (RetainerDates[x] != "" && RetainerRates[x] != "" && RetainerFrequency[x] != "")
                        {
                            var date = RetainerDates[x].ToDateTime();
                            var effectiveDate = new DateTime(date.Year, date.Month, 1);
                            var temp = db.WorkerRetainers.Where(y => y.WorkerId == worker.Id && y.EffectiveDate == effectiveDate).FirstOrDefault();
                            if (temp == null)
                            {
                                WorkerRetainer wr = new WorkerRetainer();
                                wr.EffectiveDate = new DateTime(date.Year, date.Month, 1);
                                wr.Rate = RetainerRates[x].ToDecimal();
                                wr.Frequency = RetainerFrequency[x].ToInt();
                                wr.WorkerId = worker.Id;
                                db.WorkerRetainers.Add(wr);
                                db.SaveChanges();
                            }
                        }
                    }
                    var user = db.Users.Where(x => x.WorkerId == worker.Id).FirstOrDefault();
                    if (user != null)
                    {

                        user.FirstName = worker.Firstname;
                        user.LastName = worker.Lastname;
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        if (f["Username"].ToString() != "" && f["Password"].ToString() != "")
                        {
                            User objtbUser = new User();
                            objtbUser.FirstName = worker.Firstname;
                            objtbUser.LastName = worker.Lastname;
                            objtbUser.UserName = f["Username"].ToString();
                            //objtbUser.TimeZone = objUser.TimeZone;
                            objtbUser.Password = Helper.EncryptDecrypt.Encrypt(System.Configuration.ConfigurationManager.AppSettings["EncryptID"], f["Password"].ToString().Trim(), true);

                            objtbUser.ValidStatus = false;
                            objtbUser.Status = true;
                            objtbUser.ConfirmEmail = false;
                            objtbUser.CompanyId = SessionHandler.CompanyId;
                            objtbUser.Status = true;
                            objtbUser.ValidStatus = false;
                            objtbUser.CreatedDate = DateTime.Now;
                            objtbUser.WorkerId = worker.Id;
                            //saving the detail of register user into db
                            var result = objIList.SaveUser(objtbUser);



                            tbRoleLink objRoleLink = new tbRoleLink();
                            objRoleLink.RoleId = 4;
                            objRoleLink.UserId = result.Id;
                            objIList.SaveRoleLink(objRoleLink.RoleId, objRoleLink.UserId);


                            if (result != null)
                            {
                                //sending email to username for check the valid email.
                                if (SessionHandler.CompanyId == 0)
                                {
                                    Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                                              HttpContext.Request.Url.Scheme,
                                                              HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                              (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                              ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>link</a> to verify your email.</p></div>", objtbUser.UserName.Trim(), "Respontage.com");
                                }
                                else
                                {
                                    Service.Gmail.GmailApi.Send(Service.Gmail.GmailApi.RespontageGmailLogin(), "<div style='padding:10px;line-height:18px;font-family:'Lucida Grande',Verdana,Arial,sans-serif;font-size:12px;color:#444444'> <p> Respontage.com new account, click this <a href='" + string.Format("{0}://{1}{2}",
                                                          HttpContext.Request.Url.Scheme,
                                                          HttpContext.Request.ServerVariables["HTTP_HOST"],
                                                          (HttpContext.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Request.ApplicationPath
                                                          ) + "/Home/ValidateAccount?username=" + objtbUser.Id.ToString() + "'>link</a> to verify your email. </p><p>Here is your temporary password - <span style=\"color: #f95c41\">" + f["Password"].ToString() + "</span>.</p></div>", objtbUser.UserName.Trim(), "Respontage.com");
                                }

                            }
                        }
                    }
                }
                return Json(new { success = objIList.UpdateWorker(worker, workerJobs) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult DeleteWorker(int WorkerId)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                objCommon.DeleteWorker(WorkerId);
                return Json("S1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult LoadAvailableWorkers(string propertyId, string jobTypeId, string StartDate, string EndDate)
        {
            if (StartDate != null && EndDate != null && jobTypeId != null && propertyId != null)
            {
                DateTime sd = Convert.ToDateTime(StartDate);
                DateTime ed = Convert.ToDateTime(EndDate);
                int sessionCompanyId = SessionHandler.CompanyId;
                int job_type_id = 0;
                int.TryParse(jobTypeId, out job_type_id);

                using (var db = new ApplicationDbContext())
                {
                    var results = (from w in db.Workers
                                   join j in db.WorkerJobs
                                       on new { w.Id, JobTypeId = job_type_id }
                                       equals new { Id = j.WorkerId, j.JobTypeId }
                                   join t in db.WorkerJobTypes on new { JobTypeId = j.JobTypeId } equals new { JobTypeId = t.Id }
                                   where
                                       w.CompanyId == sessionCompanyId
                                   select new
                                   {
                                       w.Id,
                                       w.Firstname,
                                       w.Lastname,
                                       Job = t.Name,
                                       j.PaymentType,
                                       j.PaymentRate
                                   }).ToList();
                    return Json(new { workers = results }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { workers = "" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetAvailableWorkers(int jobTypeId, string startDate, string endDate,bool IsNonBooking =false)
        {
            DateTime start = new DateTime(), end = new DateTime();
            List<int> AvailableWorkerIds = new List<int>();
            if (startDate != "" && endDate != "")
            {
                start = startDate.ToDateTime();
                end = endDate.ToDateTime();
            }
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var workers = db.Workers.ToList();
                    foreach (var worker in workers)
                    {
                        var schedule = db.WorkerSchedules.Where(x => start >= x.StartDate && start <= x.EndDate && (x.WorkerId ==worker.Id)).FirstOrDefault();
                        var dayOfweek = start.DayOfWeek;
                        if (schedule != null)
                        {
                            var Days = schedule.AvailDay.Split(',').ToList();
                            int index = Days.IndexOf(dayOfweek.ToString());
                            var StartTimes = schedule.AvailTimeStart.Split(',');
                            var EndTimes = schedule.AvailTimeEnd.Split(',');
                            var startTime = StartTimes[index];
                            var endTime = EndTimes[index];
                            
                            var unavailability = db.WorkerAvailability.Where(x => x.WorkerId == worker.Id && x.Type == false && 
                                                (start >= x.StartDate && start <= x.EndDate) ).FirstOrDefault();

                            var availability = db.WorkerAvailability.Where(x => x.WorkerId == worker.Id && x.Type == true &&
                                                (start >= x.StartDate && start <= x.EndDate)).FirstOrDefault();
                            if ((endTime !="" && startTime !="") || availability !=null) {
                                if (startTime.ToDateTime().TimeOfDay <= start.TimeOfDay && endTime.ToDateTime().TimeOfDay <= end.TimeOfDay && unavailability == null)
                                {
                                    AvailableWorkerIds.Add(worker.Id);
                                }
                                //else if (availability != null)
                                //{
                                //    AvailableWorkerIds.Add(worker.Id);
                                //}
                            }
                        }
                        else
                        {

                            var scheduleCount = db.WorkerAvailability.Where(x => x.WorkerId == worker.Id).ToList().Count;
                            if (scheduleCount == 0)
                            {
                                AvailableWorkerIds.Add(worker.Id);
                            }
                        }
                      


                        
                    }


                    List<AvailableWorker> aw = new List<AvailableWorker>();
                    var results = (from w in db.Workers
                                   join j in db.WorkerJobs on new { w.Id, JobTypeId = jobTypeId } equals new { Id = j.WorkerId, j.JobTypeId }
                                   join t in db.WorkerJobTypes on new { JobTypeId = j.JobTypeId } equals new { JobTypeId = t.Id }
                                   where w.CompanyId == SessionHandler.CompanyId && AvailableWorkerIds.Contains(w.Id)
                                   select new
                                   {
                                       w.Id,
                                       Name = w.Firstname + " " + w.Lastname,
                                       Job = t.Name,
                                       PaymentType = (j.PaymentType == 1 ? "Hourly" : j.PaymentType == 2 ? "Daily" : j.PaymentType == 3 ? "Per Appointment" : j.PaymentType == 4 ? "Fixed" : ""),
                                       j.PaymentRate,
                                       //Dates = (
                                       //"<div class='four wide field'>" +
                                       //     "<div class='ui calendar dt'>" +
                                       //         "<div class='ui input left icon'>" +
                                       //             "<i class='calendar icon'></i>" +
                                       //             "<input type='text' id='worker-start-date-t-" + w.Id + "' name='StartDate' placeholder='Start Date' />" +
                                       //         "</div>" +
                                       //     "</div>" +
                                       // "</div>" +
                                       // "<div class='four wide field'>" +
                                       //     "<div class='ui calendar dt'> " +
                                       //         "<div class='ui input left icon'>" +
                                       //             "<i class='calendar icon'></i>" +
                                       //             "<input type='text' id='worker-end-date-t-" + w.Id + "' name='EndDate' placeholder='End Date' />" +
                                       //         "</div>" +
                                       //     "</div>" +
                                       // "</div>"),
                                       Action = IsNonBooking ==false ?("<td class=\"collapsing\">" +
                                                        "<div class=\"ui toggle checkbox\">" +
                                                            "<input type=\"checkbox\" class=\"chk-assign\" onchange='assignWorker(this)'> <label></label>" +
                                                        "</div>" +
                                                     "</td>"): ("<td class=\"collapsing\">" +
                                                        "<div class=\"ui toggle checkbox\">" +
                                                            "<input type=\"checkbox\" class=\"non-booking-chk-assign\" onchange='NonBookingAssignWorker(this)'> <label></label>" +
                                                        "</div>" +
                                                     "</td>")
                                   }).ToList();
                    return Json(new { data = results }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { data = new List<AvailableWorker>() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetWorkerAssignments(int inquiryId)
        {
            int sessionCompanyId = SessionHandler.CompanyId;
            using (var db = new ApplicationDbContext())
            {

                var results = (from a in db.WorkerAssignments
                               join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                               join j in db.WorkerJobs on new { WorkerId = a.WorkerId, a.JobTypeId } equals new { j.WorkerId, j.JobTypeId }
                               join t in db.WorkerJobTypes on a.JobTypeId equals t.Id
                               join p in db.Properties on new { PropertyId = a.PropertyId } equals new { PropertyId = p.Id }
                               where a.ReservationId == inquiryId
                               select new
                               {
                                   a.Id,
                                   Name = (w.Firstname + " " + w.Lastname),
                                   Job = t.Name,
                                   StartDate = a.StartDate,
                                   EndDate = a.EndDate,
                                   Status = a.Status,
                                   IndayStatus = a.InDayStatus,
                                   Description = a.Description
                               })
                              .ToList();

                var result = new List<WorkerAssignmentNoteModel>();
                foreach (var r in results)
                {
                    var workerAssignment = db.WorkerAssignments.Where(x => x.Id == r.Id).FirstOrDefault();
                    var Notes = db.ReimbursementNotes.Where(x => x.AssignmentId == workerAssignment.Id).ToList();
                    var noteDetails = "";
                    foreach (var Note in Notes)
                    {
                        noteDetails += Note.Note + " - " + Note.Amount + Environment.NewLine;
                    }

                    var item = new WorkerAssignmentNoteModel();
                    item.Id = r.Id;
                    item.Name = r.Name;
                    item.Job = r.Job;
                    item.StartDate = r.StartDate.ToString("MMM d, yyyy h:mmtt (dddd)");
                    item.EndDate = r.EndDate.ToString("MMM d, yyyy h:mmtt (dddd)");
                    item.Notes = noteDetails;
                    item.Action = ("<a class='ui mini button edit-worker-assignment' title='Edit'><i class='icon edit'></i></a>" +
                                   "<a class='ui mini button delete-worker-assignment' title='Delete'><i class='icon delete'></i></a>");
                    item.Status = r.Status;
                    item.Description = r.Description;
                    var status = "";
                    status +=r.Status == 0 ? "Pending" : r.Status == 1 ? "Accepted" : "Decline/Cancel";
                    status += r.IndayStatus == 0 ? "" : r.IndayStatus == 1 ? " - On the way" : r.IndayStatus == 2 ?" - Started":" - Complete";
                    item.AssignmentStatus = status;
                    result.Add(item);
                    //results.Select(x => new
                    //{
                    //    Id = x.Id,
                    //    Name = x.Name,
                    //    Job = x.Job,
                    //    StartDate = (x.StartDate.ToString("MMM d, yyyy h:mm tt")),
                    //    EndDate = (x.EndDate.ToString("MMM d, yyyy h:mm tt")),
                    //    Notes = noteDetails,
                    //    Action = ("<a class='ui mini button delete-worker-assignment' title='Delete'><i class='icon delete'></i></a>")
                    //});
                }


                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetWorkerAssignment(string assignmentId)
        {
            int sessionCompanyId = SessionHandler.CompanyId;
            int id = Int32.Parse(assignmentId);
            using (var db = new ApplicationDbContext())
            {
                var result = (from a in db.WorkerAssignments
                              join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                              join j in db.WorkerJobs on new { WorkerId = w.Id, a.JobTypeId } equals new { j.WorkerId, j.JobTypeId }
                              join p in db.Properties on new { PropertyId = a.PropertyId } equals new { PropertyId = p.Id }
                              where a.Id == id
                              select new
                              {
                                  a.Id,
                                  a.WorkerId,
                                  j.JobTypeId,
                                  a.StartDate,
                                  a.EndDate,
                                  PropertyId = p.Id
                              }).FirstOrDefault();

                var availableWorker = (from w in db.Workers
                                       join j in db.WorkerJobs on new { w.Id, JobTypeId = result.JobTypeId } equals new { Id = j.WorkerId, j.JobTypeId }
                                       join t in db.WorkerJobTypes on new { JobTypeId = j.JobTypeId } equals new { JobTypeId = t.Id }
                                       where w.CompanyId == sessionCompanyId
                                       select new
                                       {
                                           w.Id,
                                           w.Firstname,
                                           w.Lastname,
                                           Job = t.Name,
                                           j.PaymentType,
                                           j.PaymentRate
                                       }).ToList();

                return Json(new { workerAssignment = result, availableWorker = availableWorker }, JsonRequestBehavior.AllowGet);
            }
        }

       

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AssignWorker(WorkerAssignment model, string ReinburstmentNote)
        {
            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"].ToString();
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"].ToString();

            TwilioClient.Init(accountSid, authToken);
            var js = new JavaScriptSerializer();
            var NoteList = js.Deserialize<List<ReimbursementNote>>(ReinburstmentNote);
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    model.CompanyId = SessionHandler.CompanyId;
                    db.WorkerAssignments.Add(model);
                    db.SaveChanges();
                    objIList.CreateWorkerAssignmentExpense(model.Id);
                    objIList.AddReinburstmentNotes(NoteList, model.Id);
                    var assignments = db.WorkerAssignments.Where(x => x.Id == model.Id).ToList();
                    var phoneNumber = "";
                    if (assignments != null)
                    {
                        foreach (var assignment in assignments)
                        {
                            var property = db.Properties.Where(x => x.Id == assignment.PropertyId).FirstOrDefault();
                            var parentproperty = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                            var rule = db.WorkerMessageRules.Where(x => x.PropertyId == property.Id && x.Type == 3).FirstOrDefault();
                            if (rule != null)
                            {
                            var template = db.WorkerTemplateMessages.Where(x => x.Id == rule.WorkerTemplateMessageId).FirstOrDefault();
                            var reservation = db.Inquiries.Where(x => x.Id == assignment.ReservationId).FirstOrDefault();
                            var worker = db.Workers.Where(x => x.Id == assignment.WorkerId).FirstOrDefault();
                            var workerContact = db.WorkerContactInfos.Where(x => x.WorkerId == worker.Id).FirstOrDefault();
                            var twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId ==SessionHandler.CompanyId && x.Id == property.TwilioNumberId).FirstOrDefault();
                            var guest = db.Guests.Where(x => x.Id == reservation.GuestId).FirstOrDefault();
                            if (twilioNumber != null)
                            {
                                phoneNumber = twilioNumber.PhoneNumber;
                            }
                            else
                            {
                                var host = db.Hosts.Where(x => x.Id == property.HostId).FirstOrDefault();
                                twilioNumber = db.TwilioNumbers.Where(x => x.CompanyId == SessionHandler.CompanyId && x.Id == host.TwilioNumberId).FirstOrDefault();
                                if (twilioNumber != null)
                                {
                                    phoneNumber = twilioNumber.PhoneNumber;
                                }
                            }
                            var workerThread = db.CommunicationThreads.Where(x => x.CompanyId ==SessionHandler.CompanyId && x.WorkerId == assignment.WorkerId).FirstOrDefault();
                            if (workerThread == null)
                            {
                                workerThread = new CommunicationThread();
                                db.CommunicationThreads.Add(workerThread);
                                workerThread.WorkerId = worker.Id;
                                workerThread.ThreadId = PasswordGenerator.Generate(20);
                                workerThread.CompanyId = SessionHandler.CompanyId;
                                db.CommunicationThreads.Add(workerThread);
                                db.SaveChanges();
                            }
                            var message = template.Message;
                            var replacements = new Dictionary<string, string>
                            {
                                { "[Guest Name]", guest.Name },
                                { "[Worker Name]", worker.Firstname },
                                { "[Parent Property]", parentproperty!=null?db.Properties.Where(x=>x.Id==parentproperty.ParentPropertyId).FirstOrDefault().Name:property.Name },
                                { "[Listing Name]", property.Name },
                                { "[Guest Count]", reservation.GuestCount.ToString()},
                                { "[Check In Date]", reservation.CheckInDate.ToDateTime().ToString("MMM dd,yyyy") },
                                { "[Check Out Date]", reservation.CheckOutDate.ToDateTime().ToString("MMM dd,yyyy") },
                                { "[Assignment Start]", assignment.StartDate.ToDateTime().ToString("MMM dd,yyyy hhtt")},
                                { "[Assignment End]", assignment.EndDate.ToDateTime().ToString("MMM dd,yyyy hhtt") }
                            };
                            foreach (var replacement in replacements)
                            {
                                message = message.Replace(replacement.Key, replacement.Value);
                            }

                            var smsMessage = MessageResource.Create(
                             from: new Twilio.Types.PhoneNumber(phoneNumber),
                             to: new Twilio.Types.PhoneNumber(workerContact.Value),
                             body: message);

                            CommunicationSMS sms = new CommunicationSMS
                            {
                                ThreadId = workerThread.ThreadId,
                                Message = message,
                                From = phoneNumber,
                                To = workerContact.Value,
                                CreatedDate = DateTime.Now,
                                UserId = 1

                            };
                            db.CommunicationSMS.Add(sms);
                            db.SaveChanges();
                            }
                        }
                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e) { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
            }
        }



        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult EditAssignWorker(WorkerAssignment model)
        {
            using (var db = new ApplicationDbContext())
            {
                bool isSucess = false;
                var temp = db.WorkerAssignments.Find(model.Id);
                if (temp != null)
                {
                    temp.StartDate = model.StartDate;
                    temp.EndDate = model.EndDate;
                    temp.WorkerId = model.WorkerId;
                    temp.PropertyId = model.PropertyId;
                    temp.JobTypeId = model.JobTypeId;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    isSucess = db.SaveChanges() > 0;
                }
                return Json(new { success = isSucess }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetWorkers(int WorkerTypeId, string search)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                IList<Worker> objWorker = objCommon.GetWorkers(WorkerTypeId, search);
             
                    List<WorkerDetails> WorkerList = new List<WorkerDetails>();
                using (var db = new ApplicationDbContext()) {
                    foreach (var w in objWorker)
                    {
                        var Mobiles = db.WorkerContactInfos.Where(x => x.WorkerId == w.Id).ToList();
                        string contacts = "";
                        foreach(var item in Mobiles)
                        {
                            contacts += item.Value +",";
                        }
                         WorkerDetails workerDetails = new WorkerDetails
                         {

                             Firstname = w.Firstname,
                             Lastname = w.Lastname,
                             Mobile = contacts,
                             Action = "<a class=\"ui mini button js-btn_edit-new-worker\" name=\"" + w.Id + "\" data-tooltip=\"Edit\"> <i class=\"icon edit\"></i> </a> <a class=\"ui mini button js-btn_create-worker-schedule\" name=\"" + w.Id + "\"  data-tooltip=\"View Schedule\"> <i class=\"icon checked calendar\"></i> </a> <a class=\"ui mini button availability-btn\" name=\"" + w.Id + "\" data-tooltip=\"Unavailability\"> <i class=\"icon delete calendar\"></i> </a> <a class=\"ui mini button js-btn_delete-worker-schedule\" name=\"" + w.Id + "\" data-tooltip=\"Delete\"> <i class=\"icon delete\"></i></a>"

                         };
                        WorkerList.Add(workerDetails);
                    } }
                return Json(new { data = WorkerList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetWorkerAvailability(int workerId)
        {
            using (var db = new ApplicationDbContext())
            {
               var result= db.WorkerAvailability.Where(x => x.WorkerId == workerId).Select(x=> new {StartDate = x.StartDate.ToString(),EndDate=x.EndDate.ToString(),Type =(x.Type==false?"Unavailable":"Available")}).ToList();
                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddWorkerAvailability(int workerId,string startDate,string endDate,string type)
        {
            using(var db = new ApplicationDbContext())
            {
                var item = new WorkerAvailability() {
                    WorkerId = workerId,
                    StartDate = startDate.ToDateTime(),
                    EndDate = endDate.ToDateTime(),
                    Type = type.ToBoolean()
               };

                db.WorkerAvailability.Add(item);
                db.SaveChanges();
            }
            return Json("S1", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult AddSchedule()
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return PartialView("_PartialAddSchedule");
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult AddSchedule(InboxModelWorker objSchedule)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                string strDay = "";
                string strSTime = "";
                string strETime = "";
                for (int i = 0; i < objSchedule.AvailDay.Count(); i++)
                {
                    strDay += objSchedule.AvailDay[i].ToString() + ",";
                    strSTime += objSchedule.AvailTimeStart[i].ToString() + ",";
                    strETime += objSchedule.AvailTimeEnd[i].ToString() + ",";
                }
                WorkerSchedule obj = new WorkerSchedule();
                obj.WorkerId = objSchedule.WorkerId;
                obj.CompanyId = objSchedule.CompanyId;
                obj.AvailDay = strDay;
                obj.AvailTimeStart = strSTime;
                obj.AvailTimeEnd = strETime;
                obj.StartDate = objSchedule.StartDate;
                obj.EndDate = objSchedule.EndDate;
                obj.Status = objSchedule.Status;
                int intScheduleId = objCommon.AddSchedule(obj);
                return Json("S1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult EditSchedule(int intScheduleId)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                WorkerSchedule objSchedule = objCommon.GetSchedule(intScheduleId);
                return PartialView("_PartialEditSchedule", objSchedule);
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult EditSchedule(InboxModelWorker objSchedule)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                string strDay = "";
                string strSTime = "";
                string strETime = "";
                for (int i = 0; i < objSchedule.AvailDay.Count(); i++)
                {
                    strDay += objSchedule.AvailDay[i].ToString() + ",";
                    strSTime += objSchedule.AvailTimeStart[i].ToString() + ",";
                    strETime += objSchedule.AvailTimeEnd[i].ToString() + ",";
                }
                WorkerSchedule obj = new WorkerSchedule();
                obj.WorkerId = objSchedule.WorkerId;
                obj.Id = objSchedule.Id;
                obj.CompanyId = objSchedule.CompanyId;
                obj.AvailDay = strDay;
                obj.AvailTimeStart = strSTime;
                obj.AvailTimeEnd = strETime;
                obj.StartDate = objSchedule.StartDate;
                obj.EndDate = objSchedule.EndDate;
                obj.Status = objSchedule.Status;
                objCommon.UpdateSchedule(objSchedule.Id, obj);
                return Json("S1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult DeleteSchedule(int ScheduleId)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                objCommon.DeleteSchedule(ScheduleId);
                return Json("S1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult GetSchedules(int WorkerId)
        {
            ICommonRepository objIList = new CommonRepository();
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ICommonRepository objCommon = new CommonRepository();
                IList<WorkerSchedule> objSchedule = objCommon.GetSchedules(WorkerId);
                return Json(objSchedule, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("E1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult CheckIfAssigned(int workerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return Json(new { isAssigned = db.WorkerAssignments.Where(x => x.WorkerId == workerId).Count() > 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult EditAssignWorker(int assignmentId)
        {
            using (var db = new ApplicationDbContext())
            {
                var Assignment = db.WorkerAssignments.Where(x => x.Id == assignmentId).FirstOrDefault();
                if(Assignment != null)
                {
                    //var timezone = db.Properties.Where(x => x.Id == Assignment.PropertyId).FirstOrDefault().TimeZoneName;
                    var Worker = db.Workers.Where(x => x.Id == Assignment.WorkerId).FirstOrDefault();
                    var Notes = db.ReimbursementNotes.Where(x => x.AssignmentId == assignmentId).ToList();
                    return Json(new { success = true,Assignment=Assignment,Notes=Notes,Worker=Worker }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult EditAssignReimburseWorker(int assignmentId,string startDate,string endDate,string description,string Notes)
        {
            var js = new JavaScriptSerializer();
            var NoteList = js.Deserialize<List<ReimbursementNote>>(Notes);
            using (var db = new ApplicationDbContext())
            {
                var Assignment = db.WorkerAssignments.Where(x => x.Id == assignmentId).FirstOrDefault();
                if(Assignment != null)
                {
                    Assignment.StartDate = startDate.ToDateTime();
                    Assignment.EndDate = endDate.ToDateTime();
                    Assignment.Description = description;
                    db.SaveChanges();
                    objIList.CreateWorkerAssignmentExpense(assignmentId);
                    objIList.AddReinburstmentNotes(NoteList, assignmentId);
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

            }


                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult UnassignWorker(int assignmentId)
        {
            using (var db = new ApplicationDbContext())
            {
                bool isSuccess = false;
                var temp = db.WorkerAssignments.Find(assignmentId);
                if (temp != null)
                {
                    db.WorkerAssignments.Remove(temp);
                    var reinburst = db.ReimbursementNotes.Where(x => x.AssignmentId == assignmentId).ToList();

                    db.ReimbursementNotes.RemoveRange(reinburst);
                    isSuccess = db.SaveChanges() > 0;
                    objIList.DeleteWorkerAssignmentExpense(assignmentId);
                    return Json(new { success = isSuccess }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetJobSummary(int workerId, int propertyId, string month)
        {
            var date = month == "" ? DateTime.Now : month.ToDateTime();
            using (var db = new ApplicationDbContext())
            {
                var workers = (from w in db.Workers where workerId == 0 ? true : w.Id == workerId select w).ToList();
                foreach(var worker in workers)
                {
                    var  reimbursement=(from a in db.WorkerAssignments join r in db.ReimbursementNotes on a.Id equals r.AssignmentId join w in db.Workers on a.WorkerId equals w.Id where ((a.WorkerId == worker.Id) && (propertyId == 0 ? true : a.PropertyId == propertyId) 
                             && a.StartDate.Year==date.Year && a.StartDate.Month==date.Month)
                            select new {Name =w.Firstname + w.Lastname, r.Note,r.Amount,Date =a.StartDate,r.AssignmentId }).ToList();
                }
                

            }

                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Reload Calendar
        public ActionResult RealoadCalendar(string userId)
        {
            Core.SignalR.Hubs.CalendarHub.ReloadCalendar(userId);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult RefreshBookingList(string userId)
        {
            BookingHub.RefreshBookingList(userId);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefreshInboxList(string userId)
        {
            Core.SignalR.Hubs.InboxHub.RefreshInboxList(userId);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefreshPropertyList(string userId)
        {
            Core.SignalR.Hubs.PropertyHub.RefreshPropertyList(userId);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);

        }

    }
}