﻿using Core.Database.Context;
using Core.Helper;
using MessagerSolution.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class SearchPeopleController : Controller
    {
        [HttpGet]
        public async Task<ActionResult> Search(string firstname = null, string lastname = null, string city = null, string age = null, string email = null)
        {
            List<PeopleViewModel> data = new List<PeopleViewModel>();
            JObject message = null;
            //It trgigger the Login in API Project
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10.0);
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl(1));
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("firstname", firstname);
                reqParams.Add("lastname", lastname);
                reqParams.Add("city", city);
                reqParams.Add("age", age);
                reqParams.Add("email", city);

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("SearchPeople/Search", reqContent);
                if (result != null && result.Content != null)
                {
                    //It parse the response of API call
                    message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    var success = message["success"];
                    var viewModel = message["data"]; //as JObject;
                    data = viewModel.ToObject<List<PeopleViewModel>>();

                }
            }
            return Json(new { success = true, data = data }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetGuestDetails(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                var guest = db.Guests.Where(x => x.Id == inbox.GuestId).FirstOrDefault();
                var temp = db.GuestEmails.Where(x => x.GuestId == guest.GuestId).FirstOrDefault();
                var email = "";
                if (temp != null)
                {
                    email = temp.Email;
                }
                return Json(new { guest = guest, email = email }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}