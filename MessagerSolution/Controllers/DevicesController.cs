﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using MessagerSolution.Models.Devices;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class DevicesController : Controller
    {
        private Core.Sites.Smartthings.ScrapeManager _Smartthings = null;
        public Core.Sites.Smartthings.ScrapeManager Smartthings
        {
            get
            {
                if (_Smartthings == null)
                    _Smartthings = new Core.Sites.Smartthings.ScrapeManager();

                return _Smartthings;
            }
            set
            {
                _Smartthings = value;
            }
        }
        public ActionResult Authentication(string code)
        {
            Smartthings.Authentication(code,SessionHandler.SmartthingsEmail,SessionHandler.CompanyId);
            return Content("Devices succesfully added to your account");
        }
        [HttpPost]
        public ActionResult SmartthingsSaveEmail(string email)
        {
            SessionHandler.SmartthingsEmail = email;
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<ActionResult> Login(string username,string password,string site)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("username", username);
                reqParams.Add("password", password);
                reqParams.Add("companyId", SessionHandler.CompanyId.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Devices/Login", reqContent);
                if (result != null && result.Content != null)
                {
                    //It parse the response of API call
                    //message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetDevices()
        {
            using(var db = new ApplicationDbContext())
            {
                List<DevicesViewModel> devicesViewModels = new List<DevicesViewModel>();
              
                var accounts = db.DeviceAccounts.Where(x=>x.Username !="" &&x.CompanyId ==SessionHandler.CompanyId).ToList();
                foreach(var account in accounts)
                {
                    var devices = db.Devices.Where(x=>x.AccountId == account.Id).ToList();
                    foreach (var device in devices)
                    {
                        device.Property = db.Properties.Where(x => x.ListingId == device.ListingId).FirstOrDefault();
                        device.Locks = db.Locks.Where(x => x.IsLock && x.LocalDeviceId == device.Id).ToList();

                    }
                    devicesViewModels.Add(new DevicesViewModel()
                    {
                        Devices = devices,
                        Account = account,
                    });
                    
                }
               
               var properties = (from h in db.Hosts
                              join hc in db.HostCompanies on h.Id equals hc.HostId
                              join p in db.Properties on h.Id equals p.HostId
                              where (hc.CompanyId == SessionHandler.CompanyId && p.SiteType != 3) && p.IsActive
                              select p).ToList();
                properties.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());

                return Json(new { success=true, accounts = devicesViewModels,properties = properties }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public async Task<ActionResult> GetPincodes(int lockId)
        {
            using (var db = new ApplicationDbContext())
            {
                var pincodes = db.LockPincodes.Where(x => x.LockId == lockId).ToList();
                return Json(new { success = true, pincodes }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public async Task<ActionResult> DeletePincode(int lockId,string pincodeId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("lockId", lockId.ToString());
                reqParams.Add("pincodeId", pincodeId);

                var reqContent = new FormUrlEncodedContent(reqParams);
                JObject message = null;
                var result = await client.PostAsync("Devices/DeletePinCode", reqContent);
                if (result != null && result.Content != null)
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var pincodes = db.LockPincodes.Where(x => x.LockId == lockId).ToList();
                        return Json(new { success = true, pincodes }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = false, pincode = new List<LockPincode>() }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpGet]
        public async Task<ActionResult> AddPincode(string name,string code,int lockId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("lockId", lockId.ToString());
                reqParams.Add("name", name);
                reqParams.Add("code", code);
                var reqContent = new FormUrlEncodedContent(reqParams);
                var result = await client.PostAsync("Devices/AddPincode", reqContent);
                if (result != null && result.Content != null)
                {   
                    using (var db = new ApplicationDbContext())
                    {
                        var pincodes = db.LockPincodes.Where(x => x.LockId == lockId).ToList();
                        return Json(new { success = true, pincodes }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = false, pincode = new List<LockPincode>() }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpGet]
        public async Task<ActionResult> LockUnlock(string lockId,bool islock)
        {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("lockId", lockId);
                    reqParams.Add("islock", islock.ToString());

                    var reqContent = new FormUrlEncodedContent(reqParams);
                    JObject message = null;
                    var result = await client.PostAsync("Devices/LockUnlock", reqContent);
                    if (result != null && result.Content != null)
                    {

                    }
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
        }
        [HttpPost]
        public ActionResult DeviceAssignProperty(int deviceId, long propertyId)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = db.Devices.Where(x => x.Id == deviceId).FirstOrDefault();
                temp.ListingId = propertyId;
                db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }
    }
}