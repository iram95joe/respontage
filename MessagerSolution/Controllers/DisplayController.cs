﻿using DocumentFormat.OpenXml.Packaging;
using Gnostice.Documents.Controls.HTML;
using Microsoft.Ajax.Utilities;
using Spire.Doc;
using Spire.Doc.Documents;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class DisplayController : Controller
    {
        private static Airbnb.ScrapeManager _AirbnbScrapeManager = null;

        private static VRBO.ScrapeManager _VrboScrapeManager = null;

        public static Airbnb.ScrapeManager AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb.ScrapeManager();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }
        // GET: Display
        public ActionResult Index()
        {
          
            return View() ;
        }
        public ActionResult Test()
        {
            var contextUrl = System.Web.HttpContext.Current.Request.Url;
            var url = contextUrl.Scheme + "://" + contextUrl.Authority;
            var file = url + "/Documents/DocumentTemplates/Eviction Notice.html";
         
            Document doc = new Document(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.docx"));
            doc.SaveToFile(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.html"));
            AirbnbScrapper.Test(file);

            //doc.SaveToFile(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.rtf"));
            //doc.LoadFromFile(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.docx"), FileFormat.Html, XHTMLValidationType.None);
            var byteArray = Helper.WordDocument.GetFileBytes(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.docx"));
            //using (MemoryStream memoryStream = new MemoryStream())
            //{
            //    memoryStream.Write(byteArray, 0, byteArray.Length);
            //    using (WordprocessingDocument wDoc =
            //        WordprocessingDocument.Open(memoryStream, true))
            //    {

            //    }
            //    DirectoryInfo convertedDocsDirectory =
            //            new DirectoryInfo(Server.MapPath("~" + "/Documents/DocumentTemplates/"));
            //    if (!convertedDocsDirectory.Exists)
            //        convertedDocsDirectory.Create();
            //    Helper.WordDocument.ConvertToHtml(memoryStream.ToArray(), convertedDocsDirectory, "Eviction Notice.docx");
            //}

            //doc.SaveToFile(Server.MapPath("~" + "/Documents/DocumentTemplates/Test.docx"));
            //doc.SaveToFile(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.html"));
            //Process.Start(Server.MapPath("~" + "/Documents/DocumentTemplates/Eviction Notice.html"));
            //Section s = doc.AddSection();
            //Paragraph para = s.AddParagraph();

            //Insert Hmtl styled text to paragraph
            //string htmlString = "<b>Hello World</b>";
            //para.AppendHTML(htmlString);


            //doc.SaveToFile(Server.MapPath("~/Documents/Notices/Test.pdf"), FileFormat.PDF);
            return Json(new { success = "" }, JsonRequestBehavior.AllowGet);
        }
    }
}