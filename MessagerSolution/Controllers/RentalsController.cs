﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Models;
using MessagerSolution.Models.Notices;
using MessagerSolution.Models.Rentals;
using MlkPwgen;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebGrease.Css.Extensions;
using System.Reflection;
using Spire.Doc;
using Spire.Doc.Documents;
using DocumentFormat.OpenXml.ExtendedProperties;
using OpenXmlPowerTools;
using Spire.Doc.Fields;
using MessagerSolution.Helper.PropertyOwner;

namespace MessagerSolution.Controllers
{
    public class RentalsController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        IMessageRepository messageRepo = new MessageRepository(); 
        private static Airbnb.ScrapeManager _AirbnbScrapeManager = null;
        public static Airbnb.ScrapeManager AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb.ScrapeManager();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }
        // GET: Rentals
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.ExpenseCategory = objIList.GetExpenseCategoryList();// new SelectList(objIList.GetExpenseCategoryList(), "Id", "ExpenseCategoryType");
                ViewBag.PaymentMethods = new SelectList(objIList.GetPaymentMethods(), "Id", "Name");
                ViewBag.PropertyList = objIList.GetNotParentProperty();
                ViewBag.Workers = objIList.GetWorkers();
                ViewBag.JobTypes = new SelectList(objIList.GetWorkerJobTypes(), "Id", "Name");

                using (var db = new ApplicationDbContext())
                {//JM 7/10/2020 Filter is base on Role of user, the owner property will use if role is Owner
                    var filterProperties = new List<Property>();
                    #region Get filter of property 
                    var isOwner = Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt());
                    if (isOwner == false)
                    {
                        //only Single,Sync,Multi units are included
                        var properties = (from h in db.Hosts
                                          join hc in db.HostCompanies on h.Id equals hc.HostId
                                          join p in db.Properties on h.Id equals p.HostId
                                          where (hc.CompanyId == SessionHandler.CompanyId && p.SiteType != 3) && p.IsActive
                                          select p).ToList();
                        properties.AddRange(db.Properties.Where(x => (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false) && x.CompanyId == SessionHandler.CompanyId && x.IsActive).ToList());
                        foreach (var local in properties)
                        {
                            var t = db.ParentChildProperties.Where(x => x.ChildPropertyId == local.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                            if (t == null && local.IsParentProperty == false)
                                filterProperties.Add(local);
                            else if (local.IsParentProperty && local.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts)
                            {
                                filterProperties.Add(local);
                            }
                        }
                    }
                    else
                    {
                        filterProperties.AddRange(Core.Helper.User.Owners.GetProperties(User.Identity.Name.ToInt()));
                    }
                    #endregion
                    ViewBag.Properties = filterProperties;
                    ViewBag.Vendors = db.Vendors.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    var incomeTypeList = db.IncomeTypes.ToList();
                    ViewBag.IncomeTypeList = new SelectList(incomeTypeList, "Id", "TypeName");
                }

                return View();
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult LoadRenterInbox(string search, string bookingId)
        {
            return Json(new { result = messageRepo.GetRenterInboxList(User.Identity.Name.ToInt(), search, bookingId.ToInt()) }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult LoadRenterInboxMessage(string threadId, int renterId)
        {
            using (var db = new ApplicationDbContext())
            {
                var contactInfos = db.RenterContacts.Where(x => x.RenterId == renterId).ToList();
                var emails = db.RenterEmails.Where(x => x.RenterId == renterId).ToList();
                return Json(new { success = true, Messages = messageRepo.GetCommunicationInboxMessages(threadId), contactInfos = contactInfos,emails }, JsonRequestBehavior.AllowGet);
            }
        }
        public string FileIcon(string url)
        {
            var extention = url.Split('.').Last();
            if (extention == "pdf")
            {
                return "file pdf outline";
            }
            else if (extention == "docx" || extention == "docm" || extention == "dot")
            {
                return "file word outline";
            }
            else if (extention == "xlsx" || extention == "xlsm" || extention == "xlsb")
            {
                return "file excel outline";
            }
            else
            {
                return "file alternate";
            }
        }
        public bool IsImage(string url)
        {
            var extention = url.Split('.').Last();
            if (extention == "jpg" || extention == "png" || extention == "gif")
                return true;
            else
                return false;
        }
        [HttpGet]
        public ActionResult GetDocuments(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var data = new List<RenterDocumentsViewModel>();
                var documents = db.RenterDocuments.Where(x => x.BookingId == bookingId).ToList();
                foreach (var document in documents)
                {
                    string files = "";
                    foreach (var f in document.Files)
                    {
                        files += "<a target =\"_blank\" href=\"" + f + "\"><i class=\"huge icon " + FileIcon(f) + "\"></i></a>";
                    }

                    data.Add(new RenterDocumentsViewModel()
                    {
                        Id = document.Id,
                        Description = document.Description,
                        Files = files
                    });
                }
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveDocuments(int bookingId, string description, HttpPostedFileWrapper[] Files)
        {
            using (var db = new ApplicationDbContext())
            {
                var urls = new List<string>();
                if (Files != null)
                {
                    foreach (var f in Files)
                    {
                        urls.Add(UploadFile(f, "~/Images/RenterDocuments/"));
                    }
                }
                db.RenterDocuments.Add(new RenterDocument() { BookingId = bookingId, Description = description, FileUrls = string.Join(",", urls) });
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddContact(int renterId,string phoneNumber)
        {
            using(var db = new ApplicationDbContext())
            {
                var contacts = db.RenterContacts.Where(x => x.RenterId == renterId).ToList();
                contacts.ForEach(x => x.IsDefault = false);
                db.SaveChanges();
                db.RenterContacts.Add(new RenterContact() { Contact = phoneNumber, RenterId = renterId });
                db.SaveChanges();
            }
            return Json(new { success = true}, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetContract(int propertyId)
        {
            using(var db = new ApplicationDbContext())
            {
                var listingId = db.Properties.Where(x => x.Id == propertyId).FirstOrDefault().ListingId;
                List<RentalContractModel> contracts = new List<RentalContractModel>();
                var rentals = (from reservation in db.Inquiries join renter in db.Renters on reservation.Id equals renter.BookingId where reservation.PropertyId == listingId select new { BookingId = reservation.Id, RenterName= renter.Firstname + " " + renter.Lastname, Checkin = reservation.CheckInDate}).ToList();
                foreach(var rental in rentals)
                {
                    contracts.Add(new RentalContractModel() { BookingId = rental.BookingId, Details = rental.RenterName + " - " + rental.Checkin.Value.ToString("MMM dd,yyyy") });
                }
                return Json(new { contracts }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult RenterThreadInfo(string threadId, int renterId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var renter = db.Renters.Where(x => x.Id == renterId).FirstOrDefault();
                    return Json(new { renter = renter }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult GetRenter(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var renter = db.Renters.Where(x => x.BookingId == bookingId).ToList();
                return Json(new { renter }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetRentApplied(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            { 
                var rents = db.MonthlyRents.Where(x => x.BookingId == bookingId).ToList();
                return Json(new { rents }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetPostDated(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            { var postdated = db.PostDated.Where(x => x.BookingId == bookingId).ToList();
                return Json(new { postdated }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetAdditionalBookingFee(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var movein = db.BookingAdditionalFees.Where(x => x.BookingId == bookingId && x.Type == 1).ToList();
                var utilities = db.BookingAdditionalFees.Where(x => x.BookingId == bookingId && x.Type == 2).ToList();
                return Json(new { movein, utilities }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetHistory(string bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var id = bookingId.ToInt();
                if (id == 0)
                {
                    return Json(new { data = new List<ContractChangesLog>() }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var history = db.ContractChangesLogs.Where(x => x.BookingId == id).Select(x => new { DateCreated = x.DateCreated.ToString(), Message = x.Message }).OrderByDescending(x => x.DateCreated).ToList();
                    return Json(new { data = history }, JsonRequestBehavior.AllowGet);
                }
            }

        }
        [HttpGet]
        public ActionResult GetBookingDetails(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var reservation = db.Inquiries.Where(x => x.Id == bookingId).FirstOrDefault();
                    return Json(new { success = true, reservation }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpGet]
        public ActionResult GetRentals()
        {
            using (var db = new ApplicationDbContext())
            {//Check if user is owner
               var isOwner= Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt());
                List<RentalsViewModel> rentals = new List<RentalsViewModel>();
                //if owner get all properties connected to owner regardless of its company
                var properties =isOwner? Core.Helper.User.Owners.GetProperties(User.Identity.Name.ToInt()):  db.Properties.Where(x => x.SiteType == 0 && x.IsParentProperty == false && x.CompanyId == SessionHandler.CompanyId).ToList();
                properties = properties.Where(x => x.SiteType ==0 && x.IsParentProperty==false).ToList();
                foreach (var property in properties)
                {
                    RentalsViewModel model = new RentalsViewModel();
                    var status = "Available";
                    decimal remainingBalance = 0.0M;
                    var dt = DateTime.Now;
                    var reservation = db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.CheckInDate <= dt && x.IsActive).OrderByDescending(x => x.CheckInDate).FirstOrDefault();
                    if (reservation == null)
                    {
                        reservation = db.Inquiries.Where(x => x.PropertyId == property.ListingId && x.IsActive).OrderByDescending(x => x.CheckInDate).FirstOrDefault();
                    }
                    if (reservation != null)
                    {
                        var lastDayMonth = DateTime.DaysInMonth(dt.Year, dt.Month);
                        dt = new DateTime(dt.Year, dt.Month, lastDayMonth);
                        var incomes = db.Income.Where(x => x.BookingId == reservation.Id && x.DueDate <= dt).ToList();
                        foreach (var income in incomes)
                        {
                            var payment = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();

                            remainingBalance += income.Amount.HasValue ? (income.Amount.Value - payment.Sum(x => x.Amount)) : 0;

                        }
                        if (remainingBalance != 0)
                        {
                            status = "Unpaid - " + remainingBalance;
                        }

                        else if (reservation.CheckOutDate != null && reservation.CheckOutDate.Value.Year == dt.Year && reservation.CheckOutDate.Value.Month == dt.Month)
                        {
                            status = "Ending Contract";
                        }

                        else
                        {
                            status = "Current";
                        }
                        if (reservation != null)
                        {
                            var renters = db.Renters.Where(x => x.BookingId == reservation.Id && x.IsActive).ToList();
                            foreach (var renter in renters)
                            {
                                model.Guest += renter.Firstname + " " + renter.Lastname + "<br>";
                            }
                        }
                    }


                    model.Property = property.Name;
                    model.Status = status;
                    model.Actions = (reservation != null ? "<a href=\"/Home/PropertyPayment?pid=" + property.Id + "&rid=" + reservation.Id + "\" class=\"ui mini button\" title=\"Details\">" +
                                        "<i class=\"icon file alternate\"></i>" : "") +
                                    "</a>" +
                                    (!isOwner?"<a href=\"/Home/PropertyPayment?pid=" + property.Id + "\" class=\"ui mini button\" title=\"Create Contract\">" +
                                        "<i class=\"icon edit\"></i>" +
                                    "</a>":"");

                    rentals.Add(model);
                }
                return Json(new { data = rentals.OrderByDescending(x => x.Guest) }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        private string GetSite(int siteType)
        {
            switch (siteType)
            {
                case 1: return "Airbnb";
                case 2: return "Vrbo";
                case 3: return "Booking.com";
                case 4: return "Homeaway";
                default: return "Local";
            }
        }
        public JsonResult GetScheduledExpensesEvent(int propertyId)
        {
            var toAppendPropertyId = propertyId;
            using (var db = new ApplicationDbContext())
            {
                List<WorkerExpenseMaintenancePaymentViewModel> expensePaymentData = new List<WorkerExpenseMaintenancePaymentViewModel>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (parentChild == null)
                {
                    var tempExpenses = db.PropertyScheduledExpenses.Where(x => x.PropertyId == propertyId && x.Type == 0 && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.RecurringDate).ToList();
                    foreach (var tempMaintenance in tempExpenses)
                    {
                        expensePaymentData.Add(new WorkerExpenseMaintenancePaymentViewModel()
                        {
                            ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == tempMaintenance.ExpenseCategoryId).FirstOrDefault(),
                            PaymentMethod = db.PaymentMethods.Where(x => x.Id == tempMaintenance.PaymentMethodId).FirstOrDefault(),
                            Maintenance = tempMaintenance,
                        });
                    }
                }
                else
                {
                    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).ToList();
                    toAppendPropertyId = parentChild.ParentPropertyId;
                    foreach (var p in propertyChild)
                    {
                        var tempExpenses = db.PropertyScheduledExpenses.Where(x => x.PropertyId == p.ChildPropertyId && x.Type == 0 && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.RecurringDate).ToList();
                        foreach (var tempMaintenance in tempExpenses)
                        {
                            expensePaymentData.Add(new WorkerExpenseMaintenancePaymentViewModel()
                            {
                                ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == tempMaintenance.ExpenseCategoryId).FirstOrDefault(),
                                PaymentMethod = db.PaymentMethods.Where(x => x.Id == tempMaintenance.PaymentMethodId).FirstOrDefault(),
                                Maintenance = tempMaintenance,
                            });
                        }
                    }
                }
                return Json(new { ExpensePaymentData = expensePaymentData, propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetScheduledMaintenanceEvent(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var toAppendPropertyId = propertyId;
                List<WorkerExpenseMaintenancePaymentViewModel> maintenancePaymentData = new List<WorkerExpenseMaintenancePaymentViewModel>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (parentChild == null)
                {
                    var tempMaintenances = db.PropertyScheduledExpenses.Where(x => x.PropertyId == propertyId && x.Type == 1 && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.RecurringDate).ToList();
                    foreach (var tempMaintenance in tempMaintenances)
                    {
                        maintenancePaymentData.Add(new WorkerExpenseMaintenancePaymentViewModel()
                        {
                            ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == tempMaintenance.ExpenseCategoryId).FirstOrDefault(),
                            PaymentMethod = db.PaymentMethods.Where(x => x.Id == tempMaintenance.PaymentMethodId).FirstOrDefault(),
                            Vendor = db.Vendors.Where(x => x.Id == tempMaintenance.VendorId).FirstOrDefault(),
                            Maintenance = tempMaintenance,
                            Worker = db.Workers.Where(x => x.Id == tempMaintenance.WorkerId).FirstOrDefault(),
                            JobType = db.WorkerJobTypes.Where(x => x.Id == tempMaintenance.JobTypeId).FirstOrDefault()
                        });
                    }
                }
                else
                {
                    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).ToList();
                    toAppendPropertyId = parentChild.ParentPropertyId;
                    foreach (var p in propertyChild)
                    {
                        var tempMaintenances = db.PropertyScheduledExpenses.Where(x => x.PropertyId == p.ChildPropertyId && x.Type == 1 && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.RecurringDate).ToList();
                        foreach (var tempMaintenance in tempMaintenances)
                        {
                            maintenancePaymentData.Add(new WorkerExpenseMaintenancePaymentViewModel()
                            {
                                ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == tempMaintenance.ExpenseCategoryId).FirstOrDefault(),
                                PaymentMethod = db.PaymentMethods.Where(x => x.Id == tempMaintenance.PaymentMethodId).FirstOrDefault(),
                                Vendor = db.Vendors.Where(x => x.Id == tempMaintenance.VendorId).FirstOrDefault(),
                                Maintenance = tempMaintenance,
                                Worker = db.Workers.Where(x => x.Id == tempMaintenance.WorkerId).FirstOrDefault(),
                                JobType = db.WorkerJobTypes.Where(x => x.Id == tempMaintenance.JobTypeId).FirstOrDefault()
                            });
                        }
                    }
                }
                return Json(new { Maintenances = maintenancePaymentData, propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIncome(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var toAppendPropertyId = propertyId;
                #region Income
                List<IncomeDetail> objIncomes = new List<IncomeDetail>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (parentChild == null)
                {
                    var result0 = db.Income.Where(x => (x.PropertyId == propertyId && x.IsActive) && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.DueDate).ToList();
                    foreach (var item in result0)
                    {
                        IncomeDetail objIncome = new IncomeDetail();

                        objIncome.BookingId = item.BookingId;
                        objIncome.Id = item.Id;

                        var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                        decimal dueAmount = 0;
                        dueAmount = payments.Sum(x => x.Amount);
                        if (payments.Count > 0)
                        {
                            foreach (var payment in payments)
                            {
                                objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                            }
                        }
                        else { objIncome.PaymentDetails = "-"; }
                        objIncome.Payments = dueAmount;
                        objIncome.Amount = (item.Amount.ToDecimal());
                        objIncome.IncomeTypeId = item.IncomeTypeId;
                        objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments < objIncome.Amount) ? "Partial" : objIncome.Payments > objIncome.Amount ? "Over Paid" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                        objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                        objIncome.DueDate = item.DueDate;
                        objIncome.Description = item.Description == null ? "" : item.Description;
                        objIncome.IsActive = item.IsActive;
                        objIncome.DateRecorded = item.DateRecorded;
                        var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                        //var syncParent = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        objIncome.PropertyName = tempProperty.Name;

                        objIncome.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                        var tempInquiry = db.Inquiries.SingleOrDefault(t => t.Id == item.BookingId);
                        if (tempInquiry != null)
                        {
                            var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                            var renter = db.Renters.Where(x => x.BookingId == tempInquiry.Id).ToList();
                            if (tempGuest != null) { objIncome.GuestName = tempGuest.Name; }
                            else if (renter.Count > 0)
                            {
                                renter.ForEach(x => objIncome.GuestName += x.Firstname + " " + x.Lastname + ",");
                            }
                            objIncome.BookingCode = tempInquiry.ConfirmationCode;
                            //var siteType = siteTypeList.SingleOrDefault(t => t.Code == tempInquiry.SiteTypeCode);
                            //if (siteType != null) { temp.SiteType = siteType.Description; }
                        }
                        objIncome.Actions = "<a class=\"ui mini button edit-income-btn\" title=\"Edit\">" +
                                  "<i class=\"icon edit\"></i>" +
                                  "</a>";
                        objIncomes.Add(objIncome);
                    }
                }
                else
                {
                    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).ToList();
                    toAppendPropertyId = parentChild.ParentPropertyId;
                    foreach (var p in propertyChild)
                    {
                        var result0 = db.Income.Where(x => (x.PropertyId == p.ChildPropertyId && x.IsActive) && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.DueDate).ToList();
                        foreach (var item in result0)
                        {
                            IncomeDetail objIncome = new IncomeDetail();

                            objIncome.BookingId = item.BookingId;
                            objIncome.Id = item.Id;

                            var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id && x.IsRejected == false).ToList();
                            decimal dueAmount = 0;
                            dueAmount = payments.Sum(x => x.Amount);
                            if (payments.Count > 0)
                            {
                                foreach (var payment in payments)
                                {
                                    objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                                }
                            }
                            else { objIncome.PaymentDetails = "-"; }
                            objIncome.Payments = dueAmount;
                            objIncome.Amount = (item.Amount.ToDecimal());
                            objIncome.IncomeTypeId = item.IncomeTypeId;
                            objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Amount != objIncome.Payments) ? "Partial" : objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                            objIncome.Balance = item.Amount.ToDecimal() - dueAmount;
                            objIncome.DueDate = item.DueDate;
                            objIncome.Description = item.Description == null ? "" : item.Description;
                            objIncome.IsActive = item.IsActive;
                            objIncome.DateRecorded = item.DateRecorded;
                            var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                            objIncome.PropertyName = tempProperty.Name;
                            objIncome.SiteType = ((Core.Enumerations.SiteType)tempProperty.SiteType).ToString();
                            var tempInquiry = db.Inquiries.SingleOrDefault(t => t.Id == item.BookingId);
                            if (tempInquiry != null)
                            {
                                var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempInquiry.GuestId);
                                var renter = db.Renters.Where(x => x.BookingId == tempInquiry.Id).ToList();
                                if (tempGuest != null) { objIncome.GuestName = tempGuest.Name; }
                                else if (renter.Count > 0)
                                {
                                    renter.ForEach(x => objIncome.GuestName += x.Firstname + " " + x.Lastname + ",");
                                }
                                objIncome.BookingCode = tempInquiry.ConfirmationCode;
                                //var siteType = siteTypeList.SingleOrDefault(t => t.Code == tempInquiry.SiteTypeCode);
                                //if (siteType != null) { temp.SiteType = siteType.Description; }
                            }
                            objIncome.Actions = "<a class=\"ui mini button edit-income-btn\" title=\"Edit\">" +
                                          "<i class=\"icon edit\"></i>" +
                                          "</a>";
                            objIncomes.Add(objIncome);
                        }
                    }
                }
                #endregion
                return Json(new { Incomes = objIncomes.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetExpense(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                List<ExpenseList> el = new List<ExpenseList>();
                var toAppendPropertyId = propertyId;

                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (parentChild == null)
                {

                    var expenses = db.Expenses.Where(x => x.PropertyId == propertyId && x.IsActive && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.DueDate).ToList();
                    expenses.ForEach(item =>
                    {
                        var tempBooking = item.BookingId == 0 ? null : db.Inquiries.Where(x => x.Id == item.BookingId).FirstOrDefault();
                        var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Id).ToList();
                        ExpenseList temp = new ExpenseList();
                        temp.Amount = string.Format("{0:#,0.00}", item.Amount);
                        temp.BookingCode = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.ConfirmationCode;
                        var expenseCategory = db.ExpenseCategories.Where(x => x.Id == item.ExpenseCategoryId).FirstOrDefault();
                        temp.Category = expenseCategory != null ? expenseCategory.ExpenseCategoryType : "-";
                        temp.CreatedDate = item.DateCreated.ToString("MMM d, yyyy");
                        temp.Description = item.Description;
                        temp.DueDate = item.DueDate.ToString("MMM d, yyyy");


                        temp.IsPaid = item.IsPaid;
                        temp.FeeName = db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault() == null ? "-" : db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault().Fee_Type;
                        if (tempBooking != null)
                        {
                            var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempBooking.GuestId);
                            var renter = db.Renters.Where(x => x.BookingId == tempBooking.Id).ToList();

                            if (tempGuest != null) { temp.Guest = tempGuest.Name; }
                            else if (renter.Count > 0)
                            {
                                renter.ForEach(x => temp.Guest += x.Firstname + " " + x.Lastname + ",");
                            }
                        }
                        temp.PaymentDate = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToString("MMM d,yyyy") : "-") : "-";// item.Expenses.PaymentDate.HasValue ? item.Expenses.PaymentDate.Value.ToString("MMM d, yyyy") : "-";
                        var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                        temp.PropertyName = tempProperty.Name;  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                        temp.Status = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? "Paid" : "Unpaid") : "Unpaid";
                        temp.Balance = Math.Abs(item.Amount.ToDecimal() - payments.Sum(x => x.Amount.ToDecimal()));
                        temp.StayDate = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.CheckInDate.Value.ToString("MMM d-") + (tempBooking.CheckOutDate.Value.Month == tempBooking.CheckInDate.Value.Month ? tempBooking.CheckOutDate.Value.ToString("d yyyy") : tempBooking.CheckOutDate.Value.ToString("MMM d yyyy"));
                        temp.Id = item.Id;
                        temp.Actions = "<a class=\"ui mini button add-expense-payment-btn\" title=\"Add Payment\">" +
                                            "<i class=\"dollar icon\"></i>" +
                                            "<a class=\"ui mini button edit-expense-btn\" title=\"Edit\">" +
                                        "<i class=\"icon edit\"></i>" +
                                        "</a>" +
                                        "</a>" + "<a class=\"ui mini button delete-expense-btn\" title=\"Delete\">" +
                                            "<i class=\"icon trash\"></i>" +
                                        "</a>";
                        el.Add(temp);
                    });
                    return Json(new { Expenses = el.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    toAppendPropertyId = parentChild.ParentPropertyId;
                    var propertyChildId = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).Select(x => x.ChildPropertyId).ToList();
                    var expenses = db.Expenses.Where(x => propertyChildId.Contains(x.PropertyId) && x.IsActive && x.CompanyId == SessionHandler.CompanyId).OrderBy(x => x.DueDate).ToList();
                    expenses.ForEach(item =>
                    {
                        var tempBooking = item.BookingId == 0 ? null : db.Inquiries.Where(x => x.Id == item.BookingId).FirstOrDefault();
                        var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Id).ToList();
                        ExpenseList temp = new ExpenseList();
                        temp.Amount = string.Format("{0:#,0.00}", item.Amount);
                        temp.BookingCode = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.ConfirmationCode;
                        var expenseCategory = db.ExpenseCategories.Where(x => x.Id == item.ExpenseCategoryId).FirstOrDefault();
                        temp.Category = expenseCategory != null ? expenseCategory.ExpenseCategoryType : "-";
                        temp.CreatedDate = item.DateCreated.ToString("MMM d, yyyy");
                        temp.Description = item.Description;
                        temp.DueDate = item.DueDate.ToString("MMM d, yyyy");


                        temp.IsPaid = item.IsPaid;
                        temp.FeeName = db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault() == null ? "-" : db.FeeTypes.Where(x => x.Id == item.FeeTypeId).FirstOrDefault().Fee_Type;
                        if (tempBooking != null)
                        {
                            var tempGuest = db.Guests.SingleOrDefault(t => t.Id == tempBooking.GuestId);
                            var renter = db.Renters.Where(x => x.BookingId == tempBooking.Id).ToList();

                            if (tempGuest != null) { temp.Guest = tempGuest.Name; }
                            else if (renter.Count > 0)
                            {
                                renter.ForEach(x => temp.Guest += x.Firstname + " " + x.Lastname + ",");
                            }
                        }
                        temp.PaymentDate = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? payments.OrderByDescending(x => x.CreatedAt).FirstOrDefault().CreatedAt.ToString("MMM d,yyyy") : "-") : "-";// item.Expenses.PaymentDate.HasValue ? item.Expenses.PaymentDate.Value.ToString("MMM d, yyyy") : "-";
                        var tempProperty = db.Properties.Where(y => y.Id == item.PropertyId).FirstOrDefault();
                        temp.PropertyName = tempProperty.Name;  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                        temp.Status = item.Amount != 0 ? (item.Amount == payments.Sum(x => x.Amount) ? "Paid" : "Unpaid") : "Unpaid";
                        temp.Balance = Math.Abs(item.Amount.ToDecimal() - payments.Sum(x => x.Amount.ToDecimal()));
                        temp.StayDate = item.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.CheckInDate.Value.ToString("MMM d-") + (tempBooking.CheckOutDate.Value.Month == tempBooking.CheckInDate.Value.Month ? tempBooking.CheckOutDate.Value.ToString("d yyyy") : tempBooking.CheckOutDate.Value.ToString("MMM d yyyy"));
                        temp.Id = item.Id;
                        temp.Actions = "<a class=\"ui mini button add-expense-payment-btn\" title=\"Add Payment\">" +
                                            "<i class=\"dollar icon\"></i>" +
                                            "<a class=\"ui mini button edit-expense-btn\" title=\"Edit\">" +
                                        "<i class=\"icon edit\"></i>" +
                                        "</a>" +
                                        "</a>" + "<a class=\"ui mini button delete-expense-btn\" title=\"Delete\">" +
                                            "<i class=\"icon trash\"></i>" +
                                        "</a>";
                        el.Add(temp);
                    });
                    return Json(new { Expenses = el.OrderByDescending(x => x.DueDate), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);

                }
            }
        }
        public JsonResult GetMaintenanceEntries(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var toAppendPropertyId = propertyId;
                List<MaintenanceEntryViewModel> MaintenanceEntries = new List<MaintenanceEntryViewModel>();
                var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == propertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (parentChild == null)
                {
                    var maintenanceEnties = db.MaintenanceEntries.Where(x => x.PropertyId == propertyId && x.IsActive && x.CompanyId == SessionHandler.CompanyId).ToList();
                    foreach (var maintenanceEntry in maintenanceEnties)
                    {
                        var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                        var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                        MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                        {
                            Id = maintenanceEntry.Id,
                            Description = maintenanceEntry.Description,
                            DueDate = maintenanceEntry.DueDate,
                            Amount = maintenanceEntry.Amount.ToDecimal(),
                            Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                            Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                            DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "-",
                            DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "-",
                            Actions = "<a class=\"ui mini button edit-maintenance-entry-btn\" title=\"Edit\">" +
                                      "<i class=\"icon edit\"></i>" +
                                      "</a>" +
                                      "<a class=\"ui mini button delete-maintenance-entry\" title=\"Delete\">" +
                                      "<i class=\"icon trash\"></i>" +
                                      "</a>"
                        });
                    }
                }
                else
                {
                    var propertyChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == parentChild.ParentPropertyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).ToList();
                    toAppendPropertyId = parentChild.ParentPropertyId;
                    foreach (var p in propertyChild) {
                        var maintenanceEnties = db.MaintenanceEntries.Where(x => x.PropertyId == p.ChildPropertyId && x.IsActive && x.CompanyId == SessionHandler.CompanyId).ToList();
                        foreach (var maintenanceEntry in maintenanceEnties)
                        {
                            var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                            var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                            MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                            {
                                Id = maintenanceEntry.Id,
                                Description = maintenanceEntry.Description,
                                DueDate = maintenanceEntry.DueDate,
                                Amount = maintenanceEntry.Amount.ToDecimal(),
                                Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                                Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                                DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "-",
                                DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "-",
                                Actions = "<a class=\"ui mini button edit-maintenance-entry-btn\" title=\"Edit\">" +
                                          "<i class=\"icon edit\"></i>" +
                                          "</a>" +
                                          "<a class=\"ui mini button delete-maintenance-entry\" title=\"Delete\">" +
                                          "<i class=\"icon trash\"></i>" +
                                          "</a>"
                            });
                        }
                    }
                }
                return Json(new { MaintenanceEntries = MaintenanceEntries.OrderByDescending(x => x.DueDate).ToList(), propertyId = toAppendPropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetPropertyMaintenance(string propertyIds)
        {
            List<int> pIds = string.IsNullOrWhiteSpace(propertyIds) ? new List<int>() : JsonConvert.DeserializeObject<List<int>>(propertyIds);
            var data = new List<PropertyMaintenanceViewModel>();
            var Properties = new List<Property>();
            var LocalChildIds = new List<int>();
            var isOwner = Core.Helper.User.Owners.IsOwner(User.Identity.Name.ToInt());
            var companyId = SessionHandler.CompanyId;
            using (var db = new ApplicationDbContext())
            {
                var properties =isOwner? Core.Helper.User.Owners.GetProperties(User.Identity.Name.ToInt()).Where(x=> (pIds.Contains(0) ? true : pIds.Contains(x.Id))).ToList() : db.Properties.Where(x => x.CompanyId == companyId && (pIds.Contains(0) ? true : pIds.Contains(x.Id))).ToList();

                
                foreach (var property in properties)
                {
                    List<IncomeDetail> objIncomes = new List<IncomeDetail>();
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == property.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    if (parentChild != null && property.IsParentProperty == false)
                    {
                        continue;
                    }

                    Properties.Add(property);
                    List<int> childPropertyIds = db.ParentChildProperties.Where(x => x.ParentPropertyId == property.Id && x.CompanyId == companyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).Select(x => x.ChildPropertyId).ToList();
                    if (childPropertyIds.Count == 0)
                    {
                        childPropertyIds.Add(property.Id);
                    }
                    LocalChildIds.Add((from p in db.Properties join pc in db.ParentChildProperties on p.Id equals pc.ChildPropertyId where p.SiteType == 0 && pc.ParentPropertyId == property.Id && pc.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync select p.Id).FirstOrDefault());

                    //JM remove this for modification of code
                    //#region Income
                    //var result0 = db.Income.Where(x => (childPropertyIds.Contains(x.PropertyId)&& x.IsActive) && x.CompanyId== companyId).OrderBy(x => x.DueDate).ToList();

                    //foreach (var item in result0)
                    //{
                    //    IncomeDetail objIncome = new IncomeDetail();

                    //    objIncome.BookingId = item.BookingId;
                    //    objIncome.Id = item.Id;

                    //    var payments = db.IncomePayment.Where(x => x.IncomeId == item.Id).ToList();
                    //    decimal dueAmount = 0;
                    //    dueAmount = payments.Sum(x => x.Amount);
                    //    if (payments.Count > 0)
                    //    {
                    //        foreach (var payment in payments)
                    //        {
                    //            objIncome.PaymentDetails += ((int)IncomePaymentType.Credit == payment.Type ? "Credit" : "Payment") + " - " + payment.Description + " - " + payment.Amount + "<a onclick=\"GetIncomePayment(" + payment.Id + ")\"> - Details</a> </br>";
                    //        }
                    //    }
                    //    else { objIncome.PaymentDetails = "-"; }
                    //    objIncome.Payments = dueAmount;
                    //    objIncome.Amount = (item.Amount.ToDecimal());
                    //    objIncome.IncomeTypeId = item.IncomeTypeId;
                    //    objIncome.Status = (item.IsActive == false ? "Canceled" : (objIncome.Payments > 0 && objIncome.Payments <objIncome.Amount  ) ? "Partial" : objIncome.Payments> objIncome.Amount?"Over Paid": objIncome.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund ? "Refund" : objIncome.Amount == objIncome.Payments ? "Paid" : "Unpaid");
                    //    objIncome.Balance = Math.Abs(item.Amount.ToDecimal() - dueAmount);
                    //    objIncome.DueDate = item.DueDate;
                    //    objIncome.Description = item.Description;
                    //    objIncome.IsActive = item.IsActive;
                    //    objIncome.Actions = "<a class=\"ui mini button edit-income-btn\" title=\"Edit\">" +
                    //                  "<i class=\"icon edit\"></i>" +
                    //                  "</a>";
                    //    objIncomes.Add(objIncome);
                    //}

                    //#endregion

                    //#region Maintenance
                    ////JM 08/04/19 Get all data related to Maintenance
                    //List<WorkerExpenseMaintenancePaymentViewModel> maintenancePaymentData = new List<WorkerExpenseMaintenancePaymentViewModel>();
                    //var tempMaintenances = db.PropertyScheduledExpenses.Where(x => childPropertyIds.Contains(x.PropertyId) && x.Type == 1 && x.CompanyId == companyId).OrderBy(x => x.RecurringDate).ToList();

                    //foreach (var tempMaintenance in tempMaintenances)
                    //{
                    //    maintenancePaymentData.Add(new WorkerExpenseMaintenancePaymentViewModel()
                    //    {
                    //        ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == tempMaintenance.ExpenseCategoryId).FirstOrDefault(),
                    //        PaymentMethod = db.PaymentMethods.Where(x => x.Id == tempMaintenance.PaymentMethodId).FirstOrDefault(),
                    //        Vendor = db.Vendors.Where(x => x.Id == tempMaintenance.VendorId).FirstOrDefault(),
                    //        Maintenance = tempMaintenance,
                    //        Worker = db.Workers.Where(x => x.Id == tempMaintenance.WorkerId).FirstOrDefault(),
                    //        JobType = db.WorkerJobTypes.Where(x => x.Id == tempMaintenance.JobTypeId).FirstOrDefault()
                    //    });
                    //}
                    //#endregion
                    //List<WorkerExpenseMaintenancePaymentViewModel> expensePaymentData = new List<WorkerExpenseMaintenancePaymentViewModel>();
                    //var tempExpenses = db.PropertyScheduledExpenses.Where(x => childPropertyIds.Contains(x.PropertyId) && x.Type == 0 && x.CompanyId == companyId).OrderBy(x => x.RecurringDate).ToList();
                    //foreach (var tempMaintenance in tempExpenses)
                    //{
                    //    expensePaymentData.Add(new WorkerExpenseMaintenancePaymentViewModel()
                    //    {
                    //        ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == tempMaintenance.ExpenseCategoryId).FirstOrDefault(),
                    //        PaymentMethod = db.PaymentMethods.Where(x => x.Id == tempMaintenance.PaymentMethodId).FirstOrDefault(),
                    //        Maintenance = tempMaintenance,
                    //    });
                    //}
                    ////JM 11/8/19 Desplay Maintenance Entries created from maintenance scheduler
                    //List<MaintenanceEntryViewModel> MaintenanceEntries = new List<MaintenanceEntryViewModel>();
                    //var maintenanceEnties = db.MaintenanceEntries.Where(x => x.PropertyId == property.Id && x.IsActive && x.CompanyId == companyId).ToList();
                    //foreach (var maintenanceEntry in maintenanceEnties)
                    //{
                    //    var worker = db.Workers.Where(x => x.Id == maintenanceEntry.WorkerId).FirstOrDefault();
                    //    var vendor = db.Vendors.Where(x => x.Id == maintenanceEntry.VendorId).FirstOrDefault();
                    //    MaintenanceEntries.Add(new MaintenanceEntryViewModel()
                    //    {
                    //        Id = maintenanceEntry.Id,
                    //        Description = maintenanceEntry.Description,
                    //        DueDate = maintenanceEntry.DueDate,
                    //        Amount = maintenanceEntry.Amount.ToDecimal(),
                    //        Name = worker != null ? worker.Firstname + " " + worker.Lastname : vendor != null ? vendor.Name : "-",
                    //        Status = (maintenanceEntry.Status == 0 ? "Not yet start" : maintenanceEntry.Status == 1 ? "Started" : "Completed"),
                    //        DateStart = maintenanceEntry.DateStart != null ? maintenanceEntry.DateStart.ToDateTime().ToString("MMM d,yyyy") : "-",
                    //        DateEnd = maintenanceEntry.DateEnd != null ? maintenanceEntry.DateEnd.ToDateTime().ToString("MMM d,yyyy") : "-",
                    //        Actions = "<a class=\"ui mini button edit-maintenance-entry-btn\" title=\"Edit\">" +
                    //                  "<i class=\"icon edit\"></i>" +
                    //                  "</a>" +
                    //                  "<a class=\"ui mini button delete-maintenance-entry\" title=\"Delete\">" +
                    //                  "<i class=\"icon trash\"></i>" +
                    //                  "</a>"
                    //    });
                    //}
                    //  data.Add(new PropertyMaintenanceViewModel()
                    //{
                    //    Property = property,
                    //    Maintenances = maintenancePaymentData,
                    //    MonthlyExpenses = expensePaymentData,
                    //    Incomes = objIncomes.OrderByDescending(x => x.DueDate).ToList(),
                    //    Expenses = db.Expenses.Where(x => childPropertyIds.Contains(x.PropertyId) && x.IsActive && x.CompanyId == companyId).OrderByDescending(x => x.DueDate).ToList(),
                    //    MaintenanceEntries = MaintenanceEntries.OrderByDescending(x => x.DueDate).ToList()
                    //});

                }
            }

            var json = Json(new { success = true, Properties, LocalChildIds }, JsonRequestBehavior.AllowGet);

            json.MaxJsonLength = int.MaxValue;
            return json;
        }
        [HttpPost]
        public JsonResult DeleteMaintenanceEntry(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                int PropertyId = 0;
                var temp = db.MaintenanceEntries.Where(x => x.Id == id).FirstOrDefault();
                if (temp != null)
                {
                    temp.IsActive = false;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    PropertyId = temp.PropertyId;
                    var expense = db.Expenses.Where(x => x.MaintenanceEntryId == id).FirstOrDefault();
                    if (expense != null)
                    {
                        expense.IsActive = false;
                        db.Entry(expense).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }

                }

                return Json(new { success = true, PropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult MaintenanceEntryDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var maintenance = db.MaintenanceEntries.Where(x => x.Id == id).FirstOrDefault();
                var property = db.Properties.Where(x => x.Id == maintenance.PropertyId).FirstOrDefault();
                List<string> ImageUrls = db.MaintenanceEntryFiles.Where(x => x.MaintenanceEntryId == maintenance.Id).Select(x => x.FileUrl).ToList();
                var notes = db.MaintenanceEntryNotes.Where(x => x.MaintenanceEntryId == maintenance.Id).ToList();
                return Json(new { success = true, maintenance, property, images = ImageUrls, notes }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult EditMaintenanceEntry(int Id, decimal? Amount, DateTime DueDate, string Description, int? WorkerId, int? JobTypeId, int? VendorId, string NoteString, HttpPostedFileWrapper[] Images, DateTime? DateStart, DateTime? DateEnd, int Status)
        {
            using (var db = new ApplicationDbContext())
            {
                var maintenance = db.MaintenanceEntries.Where(x => x.Id == Id).FirstOrDefault();
                maintenance.DueDate = DueDate;
                maintenance.Amount = Amount;
                maintenance.Description = Description;
                maintenance.WorkerId = WorkerId;
                maintenance.JobTypeId = JobTypeId;
                maintenance.VendorId = VendorId;
                maintenance.DateStart = DateStart;
                maintenance.DateEnd = DateEnd;
                maintenance.Status = Status;
                db.Entry(maintenance).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                var expense = db.Expenses.Where(x => x.MaintenanceEntryId == maintenance.Id).FirstOrDefault();

                if (expense != null)
                {
                    expense.Description = maintenance.Description;
                    expense.Amount = maintenance.Amount;
                    db.Entry(expense).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                if (Images != null)
                {
                    foreach (var image in Images)
                    {
                        var Image = new MaintenanceEntryFile() { MaintenanceEntryId = maintenance.Id, FileUrl = UploadFile(image, "~/Images/MaintenanceEntry/") };
                        db.MaintenanceEntryFiles.Add(Image);
                        db.SaveChanges();
                    }
                }

                var js = new JavaScriptSerializer();
                var notes = js.Deserialize<List<MaintenanceEntryNote>>(NoteString);
                var removeNoteIds = db.MaintenanceEntryNotes.Where(x => x.MaintenanceEntryId == maintenance.Id).Select(x => x.Id).ToList();
                foreach (var note in notes)
                {
                    removeNoteIds.Remove(note.Id);
                    var temp = db.MaintenanceEntryNotes.Where(x => x.Id == note.Id).FirstOrDefault();
                    if (temp == null)
                    {
                        note.MaintenanceEntryId = maintenance.Id;
                        db.MaintenanceEntryNotes.Add(note);
                    }
                    else
                    {
                        temp.Note = note.Note;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                //JM 10/22/19 Remove deleted notes
                foreach (var removeId in removeNoteIds)
                {
                    db.MaintenanceEntryNotes.Remove(db.MaintenanceEntryNotes.Where(x => x.Id == removeId).FirstOrDefault());
                    db.SaveChanges();
                }
                return Json(new { success = true, maintenance }, JsonRequestBehavior.AllowGet);

            }
        }
        string UploadFile(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }
        [HttpGet]
        public JsonResult GetScheduledExpense(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.PropertyScheduledExpenses.Where(x => x.Id == Id).FirstOrDefault();
                var property = db.Properties.Where(x => x.Id == temp.PropertyId).FirstOrDefault();
                return Json(new { success = true, details = temp, property = property }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        public JsonResult SaveScheduleMaintenanceExpense(PropertyScheduledExpense data)
        {
            var companyId = SessionHandler.CompanyId;
            using (var db = new ApplicationDbContext())
            {
                if (data.Id != 0)
                {
                    var temp = db.PropertyScheduledExpenses.Where(x => x.Id == data.Id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Interval = data.Interval;
                        temp.JobTypeId = data.JobTypeId;
                        temp.Amount = data.Amount;
                        temp.RecurringDate = data.RecurringDate;
                        temp.RecurringEndDate = data.RecurringEndDate;
                        temp.Description = data.Description;
                        temp.Frequency = data.Frequency;
                        temp.ExpenseCategoryId = data.ExpenseCategoryId;
                        temp.PaymentMethodId = data.PaymentMethodId;
                        temp.WorkerId = data.WorkerId;
                        temp.JobTypeId = data.JobTypeId;
                        temp.VendorId = data.VendorId;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        var dataTemp = db.Expenses.Where(x =>
                                      x.PropertyScheduledExpenseId == temp.Id).FirstOrDefault();
                        if (temp.Type == 1 && dataTemp!=null)
                        {
                            var maintenanceEntry = new MaintenanceEntry()
                            {
                                Amount = temp.Amount,
                                Description = temp.Description,
                                DueDate = temp.RecurringDate,
                                PropertyId = temp.PropertyId,
                                JobTypeId = temp.JobTypeId,
                                VendorId = temp.VendorId,
                                WorkerId = temp.WorkerId,
                                PropertyScheduledExpenseId = temp.Id,
                                CompanyId = temp.CompanyId

                            };
                            Core.API.AllSite.MaintenanceEntries.Update(dataTemp.MaintenanceEntryId.Value, maintenanceEntry);

                            dataTemp.DueDate = temp.RecurringDate;
                            dataTemp.Description = temp.Description;
                            dataTemp.Amount = temp.Amount;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    data.CompanyId = companyId;
                    db.PropertyScheduledExpenses.Add(data);
                    db.SaveChanges();

                    #region Ontime
                    if (data.Frequency == (int)MaintenanceFrequency.Onetime)
                    {
                        #region Create MAintence and Worker Assignment
                        var assignmentId = 0;
                        var maintenanceEntryId = 0;
                        if (data.Type == 1)
                        {
                            if (data.WorkerId != null && data.JobTypeId != null)
                            {
                                WorkerAssignment assignment = new WorkerAssignment()
                                {
                                    CompanyId = companyId,
                                    WorkerId = data.WorkerId.ToInt(),
                                    JobTypeId = data.JobTypeId.ToInt(),
                                    StartDate = data.RecurringDate,
                                    EndDate = data.RecurringDate,
                                    PropertyId = data.PropertyId,

                                };
                                db.WorkerAssignments.Add(assignment);
                                db.SaveChanges();
                                assignmentId = assignment.Id;
                            }
                            var maintenanceEntry = new MaintenanceEntry()
                            {
                                Amount = data.Amount,
                                Description = data.Description,
                                DueDate = data.RecurringDate,
                                PropertyId = data.PropertyId,
                                JobTypeId = data.JobTypeId,
                                VendorId = data.VendorId,
                                WorkerId = data.WorkerId,
                                PropertyScheduledExpenseId = data.Id,
                                CompanyId = companyId
                            };
                            maintenanceEntryId = Core.API.AllSite.MaintenanceEntries.Add(maintenanceEntry);
                        }
                        #endregion
                        #region Create Expense
                        Expense model = new Expense
                        {
                            Amount = data.Amount,
                            Description = data.Description,
                            ExpenseCategoryId = data.ExpenseCategoryId,
                            PropertyId = data.PropertyId,
                            DueDate = data.RecurringDate,
                            PaymentDate = null,
                            IsCapitalExpense = false,
                            IsStartupCost = false,
                            Frequency = data.Frequency,
                            DateLastJobRun = null,
                            DateCreated = DateTime.Now,
                            DateNextJobRun = null,
                            PaymentMethodId = data.PaymentMethodId,
                            CompanyId = data.CompanyId,
                            IsPaid = false,
                            WorkerAssignmentId = assignmentId,
                            PropertyScheduledExpenseId = data.Id,
                            MaintenanceEntryId = maintenanceEntryId

                        };
                        db.Expenses.Add(model);
                        db.SaveChanges();
                        #endregion
                    }
                    #endregion
                }

            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetNotices()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    List<NoticeViewModel> data = new List<NoticeViewModel>();
                    var notices = db.Notices.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    foreach (var notice in notices)
                    {
                        var propertyIds = db.PropertyNotices.Where(x => x.NoticeId == notice.Id).Select(x => x.Id).ToList();
                        var renterIds = db.RenterNotices.Where(x => x.NoticeId == notice.Id).Select(x=>x.RenterId).ToList();
                        NoticeViewModel model = new NoticeViewModel()
                        {
                            Id = notice.Id,
                            Description = notice.Description,
                            DateOfNotice = notice.DateOfNotice.ToString("MMM dd,yyyy"),
                            NoticeType = db.NoticeTypes.Where(x => x.Id == notice.NoticeTypeId).FirstOrDefault().Name,
                            NoticeDeliveryMethod = db.NoticeDeliveryMethods.Where(x => x.Id == notice.NoticeDeliveryMethodId).FirstOrDefault().Name,
                            Properties = string.Join("<br>,", db.Properties.Where(x => propertyIds.Contains(x.Id)).Select(x => x.Name)),
                            Renters = string.Join("<br>,", db.Renters.Where(x => renterIds.Contains(x.Id)).Select(x => x.Firstname + " " + x.Lastname)),
                            AttachmentPaths = db.NoticeDocuments.Where(x => x.NoticeId == notice.Id).Select(x => "<a target=\"_blank\" href=\"" + x.AttachmentPath + "\"><i class=\"large icon file\"></i></a>").ToList(),
                            Actions = "<button class=\"ui mini button edit-notice\" data-tooltip=\"Edit\">" +
                                            "<i class=\"icon edit outline\"></i></button>" +
                                            "<button class=\"ui mini button delete-notice\" data-tooltip=\"Delete\">" +
                                            "<i class=\"icon trash\"></i></button>"
                        };
                        data.Add(model);
                    }
                    return Json(new { success = true, data }, JsonRequestBehavior.AllowGet);
                }
            } catch (Exception e)
            {

            }
            return Json(new { success = true, data = new List<NoticeViewModel>() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetNoticeType()
        { using (var db = new ApplicationDbContext())
            {
                var noticeTypes = db.NoticeTypes.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new { success = true, noticeTypes }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetNoticeDeliveryMethod()
        {
            using (var db = new ApplicationDbContext())
            {
                var noticeDeliveryMethods = db.NoticeDeliveryMethods.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new { success = true, noticeDeliveryMethods }, JsonRequestBehavior.AllowGet);
            }
        }
        //Microsoft.Office.Interop.Word.Document wordDocument { get; set; }
        [HttpPost]
        public JsonResult CreateNotice(Notice notice,string PropertyIds,string RenterIds,HttpPostedFileWrapper[] Files, int documentId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    int generatedDocumentId = 0;
                    var propertyIds = PropertyIds == "[null]" ? new List<int>() : JsonConvert.DeserializeObject<List<int>>(PropertyIds);
                    var renterIds = RenterIds == "[null]" ? new List<int>() : JsonConvert.DeserializeObject<List<int>>(RenterIds);
                    notice.CompanyId = SessionHandler.CompanyId;
                    if (notice.Id == 0)
                    {

                        db.Notices.Add(notice);
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Entry(notice).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    foreach(var id in renterIds)
                    {
                        db.RenterNotices.Add(new RenterNotice() { NoticeId = notice.Id, RenterId = id });
                        db.SaveChanges();
                    }
                    foreach(var id in propertyIds)
                    {
                        db.PropertyNotices.Add(new PropertyNotice() {NoticeId = notice.Id,PropertyId = id });
                        db.SaveChanges();
                    }
                    var template = db.NoticeDocumentTemplates.Where(x => x.Id == documentId).FirstOrDefault();
                    if (template != null)
                    {
                        List<string> variables = new List<string>();
                        var filename = template.Path.Split('/').Last();
                        var file = "/Documents/Notices/" + notice.Description + " " + filename;
                        var newFile = Server.MapPath("~" + file);
                        var fileLocation = Server.MapPath("~" + template.Path);
                        Document word = new Document(fileLocation);
                        var renter = db.Renters.Where(x => renterIds.Contains(x.Id)).FirstOrDefault();
                        var property = db.Properties.Where(x => propertyIds.Contains(x.Id)).FirstOrDefault();
                        variables.Add(renter.Lastname);
                        variables.Add(renter.Firstname);
                        variables.Add(string.IsNullOrWhiteSpace(property.Address) ? "[Property address]" : property.Address);
                        variables.Add(notice.DateOfNotice.ToString("MMM dd,yyyy"));
                        variables.Add(notice.DateOfIncident.ToString("MMM dd,yyyy hh:mm tt"));
                        word.Replace("[Lastname]", renter.Lastname, true, true);
                        word.Replace("[Firstname]", renter.Firstname, true, true);
                        word.Replace("[Property address]", string.IsNullOrWhiteSpace(property.Address)? "[Property address]" : property.Address, true, true);
                        word.Replace("[Date of notice]", notice.DateOfNotice.ToString("MMM dd,yyyy"), true, true);
                        word.Replace("[Date of incident]", notice.DateOfIncident.ToString("MMM dd,yyyy hh:mm tt"), true, true);
                        word.SaveToFile(newFile);
                        var f = new NoticeDocument() { NoticeId = notice.Id, AttachmentPath = file };
                        db.NoticeDocuments.Add(f);
                        db.SaveChanges();
                        generatedDocumentId = f.Id;
                    }
                    if (Files != null)
                    {
                        foreach (var file in Files)
                        { var data = new NoticeDocument() { NoticeId = notice.Id, AttachmentPath = UploadFile(file, "~/Documents/Notices/") };
                            db.NoticeDocuments.Add(data);
                            db.SaveChanges();
                        }
                    }

                    return Json(new { success = true, generatedId = generatedDocumentId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteNotice(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Notices.Where(x => x.Id == id).FirstOrDefault();
                var documents = db.NoticeDocuments.Where(x => x.NoticeId == temp.Id).ToList();
                db.NoticeDocuments.RemoveRange(documents);
                db.Notices.Remove(temp);
                db.SaveChanges();
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetNoticeDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var notice = db.Notices.Where(x => x.Id == id).FirstOrDefault();
                var attachmentPaths = db.NoticeDocuments.Where(x => x.NoticeId == notice.Id).Select(x => "<a target=\"_blank\" href=\"" + x.AttachmentPath + "\"><i class=\"large icon file\"></i></a>").ToList();
                var propertyIds = db.PropertyNotices.Where(x => x.NoticeId == notice.Id).Select(x=>x.PropertyId).ToList();
                var renterIds = db.RenterNotices.Where(x => x.NoticeId == notice.Id).Select(x=>x.RenterId).ToList();
                return Json(new { success = true, notice, attachmentPaths,propertyIds,renterIds}, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetRenters()
        {

            using (var db = new ApplicationDbContext())
            {
                var renters = (from r in db.Renters join i in db.Inquiries on r.BookingId equals i.Id join p in db.Properties on i.PropertyId equals p.ListingId where p.CompanyId == SessionHandler.CompanyId select r).ToList();
                return Json(new { success = true, renters }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CreateNoticeDeliveryMethod(string name) {
            using (var db = new ApplicationDbContext())
            {
                db.NoticeDeliveryMethods.Add(new NoticeDeliveryMethod() { Name = name, CompanyId = SessionHandler.CompanyId });
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CreateNoticeType(string name)
        {
            using (var db = new ApplicationDbContext())
            {
                db.NoticeTypes.Add(new NoticeType() { Name = name, CompanyId = SessionHandler.CompanyId });
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetNoticeDocuments()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<NoticeDocumentViewModel>();
                var documents = db.NoticeDocumentTemplates.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach (var document in documents)
                {
                    list.Add(new NoticeDocumentViewModel() { Id = document.Id, Name = document.Name, Actions =
                                        "<button class=\"ui mini button edit-notice-template\" data-tooltip=\"Edit\">" +
                                        "<i class=\"icon edit outline\"></i></button>"
                                        + "<a href=\"" + document.Path + "\"><button class=\"ui mini button\" data-tooltip=\"Download\">" +
                                        "<i class=\"icon download\"></i></button></a>" +
                                        "<button class=\"ui mini button delete-notice-template\" data-tooltip=\"Delete\">" +
                                        "<i class=\"icon trash\"></i></button>"
                    });
                }
                return Json(new { success = true, data = list }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTemplateDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var template = db.NoticeDocumentTemplates.Where(x => x.Id == id).FirstOrDefault();
                Document doc = new Document(Server.MapPath("~" + template.Path));
                var filename = "/Documents/DocumentHtml/" + template.Name + ".html";
                var htmlpath = Server.MapPath("~" + filename);
                doc.SaveToFile(htmlpath);
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority + filename;
                template.Html = AirbnbScrapper.Test(url);
                return Json(new { success = true, template }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetNoticeDocumentDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var document = db.NoticeDocuments.Where(x => x.Id == id).FirstOrDefault();
                Document doc = new Document(Server.MapPath("~" + document.AttachmentPath));
                var filename = document.AttachmentPath.Split('/').Last().Replace(".docx", ".html");
                var filepath = "/Documents/DocumentHtml/" + filename;
                var htmlpath = Server.MapPath("~" + filepath);
                doc.SaveToFile(htmlpath);
                var contextUrl = System.Web.HttpContext.Current.Request.Url;
                var url = contextUrl.Scheme + "://" + contextUrl.Authority + filepath;

                document.Html = AirbnbScrapper.Test(url);
                return Json(new { success = true, document }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveGeneratedDocument(int id,string content)
        {
            using(var db= new ApplicationDbContext())
            {
                var temp = db.NoticeDocuments.Where(x => x.Id == id).FirstOrDefault();
                if (temp != null)
                {
                    Document doc = new Document();
                    var file = temp.AttachmentPath.Replace(".docx",".pdf");
                    var path = Server.MapPath("~" + file);
                    Section s = doc.AddSection();
                    Paragraph para = s.AddParagraph();
                    para.AppendBookmarkStart("content");
                    para.AppendHTML(content.Trim());
                    para.AppendBookmarkEnd("content");
                    doc.SaveToFile(path);
                    temp.AttachmentPath = file;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CreateTemplate(CreateTemplateModel Template,HttpPostedFileWrapper[] Files)
        {

            if (Files != null)
            {
                foreach (var File in Files)
                {
                    if (File != null)
                    {
                        var fileName = Path.GetFileName(File.FileName);
                        string url = "~/Documents/DocumentTemplates/" + fileName;
                        File.SaveAs(Server.MapPath(url));
                        url = url.Replace("~", "");
                        using (var db = new ApplicationDbContext())
                        {
                            db.NoticeDocumentTemplates.Add(new NoticeDocumentTemplate() { Name = Path.GetFileNameWithoutExtension(File.FileName), Path = url, CompanyId = SessionHandler.CompanyId });
                            db.SaveChanges();

                        }
                    }
                }
            }
            using (var db = new ApplicationDbContext())
            {
                var temp = db.NoticeDocumentTemplates.Where(x => x.Id == Template.Id).FirstOrDefault();
                if (Template.DocumentName != "" && Template.DocumentName != null)
                {

                    var file = "";
                    var path = "";
                    Document doc = new Document();
                    file = "/Documents/DocumentTemplates/" + Template.DocumentName + ".docx";
                    path = Server.MapPath("~" + file);
                    Section s = doc.AddSection();
                    Paragraph para = s.AddParagraph();
                    para.AppendBookmarkStart("content");
                    para.AppendHTML(Template.Content.Trim());
                    para.AppendBookmarkEnd("content");
                    doc.SaveToFile(path, FileFormat.Docx);
                    
                 

                    if (Template.Id == 0)
                    {
                        db.NoticeDocumentTemplates.Add(new NoticeDocumentTemplate() { Name = Template.DocumentName, Path = file, CompanyId = SessionHandler.CompanyId });
                        db.SaveChanges();
                    }
                    else
                    {
                      
                        temp.Name = Template.DocumentName;
                        temp.Path = file;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteNoticeTemplate(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                db.NoticeDocumentTemplates.Remove(db.NoticeDocumentTemplates.Where(x => x.Id == id).FirstOrDefault());
                db.SaveChanges();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteScheduledExpense(int id)
        {
            using(var db = new ApplicationDbContext())
            {
                int PropertyId = 0;
                var expense = db.PropertyScheduledExpenses.Where(x => x.Id == id).FirstOrDefault();
                PropertyId = expense.PropertyId;
                db.PropertyScheduledExpenses.Remove(expense);
                db.SaveChanges();
                return Json(new { success = true, PropertyId }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetInvoices(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var invoices = db.Invoices.Where(x => x.BookingId == bookingId).ToList();
                return Json(new { data = invoices }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}