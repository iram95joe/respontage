﻿using MessagerSolution.Helper;
using MessagerSolution.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MessagerSolution.Enumerations.Cashflow;
using MessagerSolution.Models.Reports.Cashflow;
using Core.Repository;
using Core.Database.Entity;
using Core.Database.Context;
using Core.Helper;
using Core;
using System.Web.Script.Serialization;

namespace MessagerSolution.Controllers
{
    public class CashflowController : Controller
    {
        //
        // GET: /Cashflow/

        ICommonRepository objIList = new CommonRepository();

        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Index(int companyId)
        {
            List<CashFlows> CashFlowList = new List<CashFlows>();
            List<CashFlowType> cashFlowTypeList = new List<CashFlowType>();
            List<Property> propertyList = new List<Property>();
            var propertyFilter = new List<Property>();
            var propertyForCreateCashflow = new List<Property>();
            using (var db = new ApplicationDbContext())
            {
                CashFlowList = (from c in db.CashFlows
                                join ct in db.CashFlowTypes on c.CashFlowTypeId equals ct.Id
                                join p in db.Properties on c.PropertyId equals p.ListingId
                                where c.CompanyId == companyId
                                select c).OrderByDescending(x => x.PaymentDate).ToList();

                propertyList = (from h in db.Hosts
                                join hc in db.HostCompanies on h.Id equals hc.HostId
                                join p in db.Properties on h.Id equals p.HostId
                                where hc.CompanyId == SessionHandler.CompanyId
                                select p).ToList();
                propertyList.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 0).ToList());
                foreach (var p in propertyList)
                {
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                    if (parentChild == null)
                    {
                        propertyFilter.Add(p);
                    }
                }
                foreach (var p in propertyList)
                {
                    if ((p.IsParentProperty && p.ParentType != (int)Core.Enumerations.ParentPropertyType.Sync) || p.IsParentProperty == false)
                    {
                        propertyForCreateCashflow.Add(p);
                    }
                }
                cashFlowTypeList = db.CashFlowTypes.ToList();
                var owners = (from user in db.Users join roleLink in db.tbRoleLink on user.Id equals roleLink.UserId join role in db.tbRole on roleLink.RoleId equals role.Id join ownerCompany in db.OwnerCompanies on user.Id equals ownerCompany.OwnerId where role.Id == 5 && ownerCompany.CompanyId == SessionHandler.CompanyId select new { Id = user.Id, Name = user.FirstName + " " + user.LastName }).ToList();
                ViewBag.Owners = new SelectList(owners, "Id", "Name");
            }

            ViewBag.PropertyListForFilter = propertyFilter;
            ViewBag.PropertyList = new SelectList(propertyForCreateCashflow, "Id", "Name");
            ViewBag.CashFlowTypeList = new SelectList(cashFlowTypeList, "Id", "CashFlowTypeName");
            return View();
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult GetAllCashFlowRecord(string propertyFilter)
        {
            using (var db = new ApplicationDbContext())
            {

                var propertyList = db.Properties.ToList();
                var cashFlowTypeList = db.CashFlowTypes.ToList();

                List<CashFlowViewModel> cl = new List<CashFlowViewModel>();

                var cashFlow = db.CashFlows.Join(db.CashFlowTypes, x => x.CashFlowTypeId, y => y.Id, (x, y) => new { Cashflow = x, CashflowType = y })
                    .Join(db.Properties, x => x.Cashflow.PropertyId, y => y.Id, (x, y) => new { CashFlow = x.Cashflow, CashflowType = x.CashflowType, Property = y })
                    .Where(x => x.CashFlow.CompanyId == SessionHandler.CompanyId)
                    .ToList();

                if (propertyFilter != "all")
                {
                    int propertyId = int.Parse(propertyFilter);
                    cashFlow = cashFlow.Where(x => x.Property.Id == propertyId /*|| x.Property.ParentPropertyId == propertyId*/).ToList();

                }

                cashFlow.ForEach(item =>
                {
                    CashFlowViewModel temp = new CashFlowViewModel();

                    temp.Id = item.CashFlow.Id;
                    var tempType = cashFlowTypeList.Where(x => x.Id == item.CashflowType.Id).FirstOrDefault();
                    temp.CashFlowTypeName = tempType.CashFlowTypeName;
                    temp.Amount = item.CashFlow.Amount;
                    var tempPropertyName = propertyList.Where(x => x.Id == item.CashFlow.PropertyId).FirstOrDefault();
                    var parentproperty = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempPropertyName.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    temp.PropertyName = parentproperty == null ? tempPropertyName.Name : propertyList.Where(x => x.Id == parentproperty.ParentPropertyId).FirstOrDefault().Name;
                    temp.PaymentDate = item.CashFlow.PaymentDate.ToDateTime().ToString("MMM d, yyyy");
                    temp.Description = item.CashFlow.Description;
                    var user = (from owner in db.Users where owner.Id == item.CashFlow.OwnerId select new { Name = owner.FirstName + " " + owner.LastName }).FirstOrDefault();
                    temp.Owner = user != null ? user.Name : "";
                    temp.Actions = "<a class=\"ui mini button edit-cashflow-btn\" title=\"Edit\">" +
                                        "<i class=\"icon edit\"></i>" +
                                        "</a>" +
                                        "<a class=\"ui mini button delete-cashflow-btn\" title=\"Delete\">" +
                                            "<i class=\"icon ban\"></i>" +
                                        "</a>";
                    cl.Add(temp);
                });

                return Json(new { data = cl.OrderByDescending(x => x.PaymentDate.ToDateTime()).ToList() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        [CheckSessionTimeout]
        public ActionResult Details(int id)
        {
            bool isSuccess = false;
            CashFlows cash = new CashFlows();
            try
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                using (var db = new ApplicationDbContext())
                {
                    cash = db.CashFlows.SingleOrDefault(i => i.CompanyId == companyId && i.Id == id);
                    if (cash != null) isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, cashflow = cash }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Update(int Id, int cashtype, int property, double amount, DateTime payment, string description, int ownerId)
        {
            using (var db = new ApplicationDbContext())
            {
                var cash = db.CashFlows.Find(Id);

                try
                {

                    cash.Amount = amount;
                    cash.PaymentDate = payment;
                    cash.CashFlowTypeId = cashtype;
                    cash.PropertyId = property;
                    cash.Description = description;
                    cash.OwnerId = ownerId;
                    db.Entry(cash).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { success = true, cashflow = cash, message = "Cashflow details has been updated" }, JsonRequestBehavior.AllowGet);

                }

                catch (Exception ex)
                {
                    Trace.Write(ex.ToString());
                    return Json(new { success = false, message = "Error" }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [HttpPost]
        [CheckSessionTimeout]

        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var b = db.CashFlows.Where(x => x.Id == id).FirstOrDefault();

                    db.CashFlows.Remove(b);
                    db.SaveChanges();

                    return Json(new { success = true, message = "CashFlow has been deleted" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                Trace.Write(ex.ToString());
                return Json(new { success = false, message = "Error" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        [CheckSessionTimeout]
        public ActionResult Add(int PropertyId, int CashFlowTypeId, DateTime PaymentDate, double amount, string description, int ownerId)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var temp = db.CashFlows.Where(x => x.PropertyId == PropertyId && x.CashFlowTypeId == (int)CashFlowTypeEnum.StartingCash && x.PaymentDate.Month == PaymentDate.Month && PaymentDate.Year == x.PaymentDate.Year && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                    if (temp == null)
                    {
                        CashFlows flow = new CashFlows();
                        flow.CompanyId = SessionHandler.CompanyId;
                        flow.DateCreated = DateTime.Now;
                        flow.Amount = amount;
                        flow.PaymentDate = PaymentDate;
                        flow.CashFlowTypeId = CashFlowTypeId;
                        flow.PropertyId = PropertyId;
                        flow.Description = description;
                        flow.OwnerId = ownerId;
                        db.CashFlows.Add(flow);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Cash Flow has been added" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "Cash start already added for " + PaymentDate.ToString("MMMM yyyy") }, JsonRequestBehavior.AllowGet);

                    }

                }

                catch (Exception ex)
                {
                    Trace.Write(ex.ToString());

                    return Json(new { success = false, message = "Error" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        // for cashflow report
        [HttpPost]
        public ActionResult GetCashFlowInfoByMonth(DateTime month, string propertyIds, bool includeChild)
        {
            int status = (int)Enumerations.Cashflow.StatusEnum.FAIL;
            var js = new JavaScriptSerializer();
            var Ids = js.Deserialize<List<int>>(propertyIds);
            List<ProcessDateModel> monthYearRange = new List<ProcessDateModel>();
            List<CashFlowReportModel> cashFlowReportData = new List<CashFlowReportModel>();
            try
            {
                foreach (var propertyId in Ids)
                {
                    decimal startingCash = 0;
                    List<CashFlowReportModel> cashFlowReportList = new List<CashFlowReportModel>();
                    var DateRange = new List<DateTime>();
                    var incomeData = objIList.GetIncomes(propertyId, includeChild);
                    var expenseData = objIList.GetExpenses(propertyId, includeChild);
                    var cashFlowData = objIList.GetCashFlows(propertyId, includeChild);
                    var beginingDate = new DateTime(month.Year, month.Month,
                                    DateTime.DaysInMonth(month.Year, month.Month));
                    var endDate = beginingDate;
                    var pIds = new List<int>();
                    pIds.Add(propertyId);
                    if (includeChild)
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            var parentChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).FirstOrDefault();
                            if (parentChild != null)
                            {
                                pIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                            }
                        }
                    }
                    var incomebeginDate = incomeData.Where(x => pIds.Contains(x.PropertyId)).Select(x => x.DueDate).DefaultIfEmpty().Min();
                    var expensebeginDate = expenseData.Where(x => pIds.Contains(x.PropertyId)).Select(x => x.DueDate).DefaultIfEmpty().Min();
                    var cashflowbeginDate = cashFlowData.Where(x => pIds.Contains(x.PropertyId)).Select(x => x.PaymentDate).DefaultIfEmpty().Min();

                    if (incomebeginDate != DateTime.MinValue && incomebeginDate < beginingDate)
                    {
                        beginingDate = new DateTime(incomebeginDate.Year, incomebeginDate.Month, 1);
                    }
                    if (expensebeginDate != DateTime.MinValue && expensebeginDate < beginingDate)
                    {
                        beginingDate = new DateTime(expensebeginDate.Year, expensebeginDate.Month, 1);
                    }
                    if (cashflowbeginDate != DateTime.MinValue && cashflowbeginDate < beginingDate)
                    {
                        beginingDate = new DateTime(cashflowbeginDate.Year, cashflowbeginDate.Month, 1);
                    }
                    var startingCashData = cashFlowData.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.StartingCash)).FirstOrDefault();
                    if (startingCashData != null)
                    {
                        var dtFirstday = new DateTime(startingCashData.PaymentDate.Year, startingCashData.PaymentDate.Month, 1);
                        if (dtFirstday <= beginingDate)
                        {
                            startingCash = startingCashData.Amount.ToDecimal();
                        }
                    }
                    while (beginingDate <= endDate)
                    {
                        DateRange.Add(beginingDate);
                        beginingDate = beginingDate.AddMonths(1);
                    }
                    foreach (var date in DateRange)
                    {
                        var incomeInfo = new List<Income>(incomeData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund));
                        var expenseInfo = new List<Expense>(expenseData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel));
                        var cashFlowInfo = new List<CashFlows>(cashFlowData.Where(x => pIds.Contains(x.PropertyId) && x.PaymentDate.Month == date.Month && x.PaymentDate.Year == date.Year));
                        var incomeRefundInfo = new List<Income>(incomeData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund));
                        var expenseRefundInfo = new List<Expense>(expenseData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel));

                        if (incomeInfo.Any() || expenseInfo.Any() || cashFlowInfo.Any())
                        {
                            //startingCash = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.StartingCash)).Sum(x => x.Amount).ToDecimal();

                            decimal totalExpense = 0;
                            decimal totalExpenseRefund = 0;
                            decimal totalIncome = 0;
                            decimal totalIncomeRefund = 0;
                            string NewInvestmentSummary = "<h2>New Investment</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string PartnerInvestmentSummary = "<h2>Partner Investment</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string WriteOffSummary = "<h2>Write Off</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string CashOutSummary = "<h2>Cash Out</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string PartnerDrawSummary = "<h2>Partner Draw</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string PartnerInvestmentDrawSummary = "<h2>Partner Investment Draw</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string IncomeSummary = "<h2>Income</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                            string ExpenseSummary = "<h2>Expense</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";

                            using (var db = new ApplicationDbContext())
                            {
                                //JM 7/15/2020 Calculate Income
                                foreach (var income in incomeInfo)
                                {
                                    var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                                    if (payments.Count > 0)
                                    {
                                        IncomeSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                        totalIncome += payments.Sum(x => x.Amount);
                                    }
                                }
                                //JM 7/15/2020 Calculate Expense
                                foreach (var expense in expenseInfo)
                                {
                                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                                    if (payments.Count > 0)
                                    {
                                        ExpenseSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                        totalExpense += payments.Sum(x => x.Amount);
                                    }

                                }

                                //JM 7/15/2020 Calculate Income refund
                                //IncomeSummary += "<h2>Refund</h2>";
                                foreach (var income in incomeRefundInfo)
                                {
                                    var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                                    if (payments.Count > 0)
                                    {
                                        IncomeSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                        totalIncomeRefund += payments.Sum(x => x.Amount);
                                    }
                                }
                                //JM 7/15/2020 Calculate Expense refund
                                //ExpenseSummary += "<h2>Refund</h2>";
                                foreach (var expense in expenseRefundInfo)
                                {
                                    var payments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                                    if (payments.Count > 0)
                                    {
                                        ExpenseSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                        totalExpenseRefund += payments.Sum(x => x.Amount);
                                    }
                                }

                            }
                            //incomeInfo.Where(x => x.Status == 2).Sum(x => x.Amount.Value);
                            //expenseInfo.Where(x => x.IsPaid).Sum(x => x.Amount.Value);
                            decimal SuggestedStartingCash = 0;
                            var thisMonthCashflow = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.StartingCash)).ToList();
                            if (thisMonthCashflow.Count > 0)
                            {
                                SuggestedStartingCash = thisMonthCashflow.Sum(x => x.Amount).ToDecimal();
                            }
                            var shareExpense = 0;
                            var nettCashFlow = (totalIncome - totalIncomeRefund) - (totalExpense - totalExpenseRefund);
                            var totalNewInvestment = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.NewInvestment)).Sum(x => x.Amount).ToDecimal();
                            var totalPartnerInvestment = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestment)).Sum(x => x.Amount).ToDecimal();
                            var totalWriteOff = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.WriteOff)).Sum(x => x.Amount).ToDecimal();
                            var totalCashOut = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.CashOut)).Sum(x => x.Amount).ToDecimal();
                            var totalPartnerDraw = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.OwnerDraw)).Sum(x => x.Amount).ToDecimal();
                            var totalPartnerInvestmentDraw = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestmentDraw)).Sum(x => x.Amount).ToDecimal();
                            var totalEndingCash = (startingCash + totalNewInvestment + totalPartnerInvestment) -
                                                  (totalWriteOff + totalCashOut + totalPartnerDraw +
                                                   totalPartnerInvestmentDraw);

                            //Get All Summary
                            using (var db = new ApplicationDbContext())
                            {
                                (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.NewInvestment) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => NewInvestmentSummary += "<tr>" + x.summary + "</tr>");
                                (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestment) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => PartnerInvestmentSummary += "<tr>" + x.summary + "</tr>");
                                (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.WriteOff) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => WriteOffSummary += "<tr>" + x.summary + "</tr>");
                                (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.CashOut) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => CashOutSummary += "<tr>" + x.summary + "</tr>");
                                (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.OwnerDraw) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => PartnerDrawSummary += "<tr>" + x.summary + "</tr>");
                                (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestmentDraw) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => PartnerInvestmentDrawSummary += "<tr>" + x.summary + "</tr>");
                            }

                            if (nettCashFlow >= 0)
                            {
                                totalEndingCash += nettCashFlow;
                            }
                            else
                            {
                                totalEndingCash -= Math.Abs(nettCashFlow);
                            }
                            cashFlowReportList.Add(new CashFlowReportModel
                            {
                                SuggestedStartingCash = SuggestedStartingCash,
                                Property = Core.API.AllSite.Properties.GetName(propertyId),
                                IncomeSummary = js.Serialize(IncomeSummary + "</tbody></table>"),
                                ExpenseSummary = js.Serialize(ExpenseSummary + "</tbody></table>"),
                                CashOutSummary = js.Serialize(CashOutSummary + "</tbody></table>"),
                                NewInvestmentSummary = js.Serialize(NewInvestmentSummary + "</tbody></table>"),
                                PartnerDrawSummary = js.Serialize(PartnerDrawSummary + "</tbody></table>"),
                                PartnerInvestmentDrawSummary = js.Serialize(PartnerInvestmentDrawSummary + "</tbody></table>"),
                                PartnerInvestmentSummary = js.Serialize(PartnerInvestmentSummary + "</tbody></table>"),
                                WriteOffSummary = js.Serialize(WriteOffSummary + "</tbody></table>"),
                                Date = date,
                                Month = date.Month,
                                Year = date.Year,
                                StartingCash = startingCash,
                                NewInvestment = totalNewInvestment,
                                PartnerInvestment = totalPartnerInvestment,
                                WriteOff = totalWriteOff,
                                CashOut = totalCashOut,
                                PartnerDraw = totalPartnerDraw,
                                PartnerInvestmentDraw = totalPartnerInvestmentDraw,
                                TotalIncome = totalIncome,
                                TotalExpense = totalExpense,
                                TotalIncomeRefund = totalIncomeRefund,
                                TotalExpenseRefund = totalExpenseRefund,
                                ShareExpense = shareExpense,
                                NettCashFlow = nettCashFlow,
                                EndingCash = totalEndingCash
                            });
                            startingCash = totalEndingCash;
                        }
                    }
                    if (cashFlowReportList.Count > 0)
                    {
                        var cashflow = cashFlowReportList.Where(x => x.Month == month.Month && x.Year == month.Year).FirstOrDefault();
                        if (cashflow != null)
                        {
                            cashFlowReportData.Add(cashflow);
                        }
                    }
                    //fullTotalIncome = cashFlowReportList.Sum(x => x.TotalIncome);
                    //fullTotalExpense = cashFlowReportList.Sum(x => x.TotalExpense);
                    //fullTotalShareExpense = cashFlowReportList.Sum(x => x.ShareExpense);
                    //fullTotalNetCashFlow = cashFlowReportList.Sum(x => x.NettCashFlow);
                    status = (int)Enumerations.Cashflow.StatusEnum.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            var response = new { cashFlowReportData = cashFlowReportData.OrderByDescending(x => x.Date), status };
            return Json(response);
        }
        [HttpPost]
        public ActionResult GetCashFlowInfo(int propertyId, string dateFrom, string dateTo, bool includeChild)
        {
            int status = (int)Enumerations.Cashflow.StatusEnum.FAIL;
            int totalPageCount = 0;
            var startDate = dateFrom.ToDateTime();
            if (dateTo == "")
            {
                dateTo = dateFrom;
            }
            var endDate = dateTo.ToDateTime();
            var DateRange = new List<DateTime>();
            var js = new JavaScriptSerializer();
            List<ProcessDateModel> monthYearRange = new List<ProcessDateModel>();
            List<CashFlowReportModel> cashFlowReportList = new List<CashFlowReportModel>();
            decimal startingCash = 0, fullTotalIncome = 0, fullTotalExpense = 0, fullTotalShareExpense = 0, fullTotalNetCashFlow = 0;

            try
            {
                var incomeData = objIList.GetIncomes(propertyId, includeChild);
                var expenseData = objIList.GetExpenses(propertyId, includeChild);
                var cashFlowData = objIList.GetCashFlows(propertyId, includeChild);
                var beginingDate = startDate;
                var pIds = new List<int>();
                pIds.Add(propertyId);
                if (includeChild)
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var parentChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).FirstOrDefault();
                        if (parentChild != null)
                        {
                            pIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                        }
                    }
                }
                var incomebeginDate = incomeData.Where(x => pIds.Contains(x.PropertyId)).Select(x => x.DueDate).DefaultIfEmpty().Min();
                var expensebeginDate = expenseData.Where(x => pIds.Contains(x.PropertyId)).Select(x => x.DueDate).DefaultIfEmpty().Min();
                var cashflowbeginDate = cashFlowData.Where(x => pIds.Contains(x.PropertyId)).Select(x => x.PaymentDate).DefaultIfEmpty().Min();
                if (startDate.Date == DateTime.MinValue || endDate.Date == DateTime.MinValue)
                {
                    beginingDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    endDate = new DateTime(beginingDate.Year, beginingDate.Month, DateTime.DaysInMonth(beginingDate.Year, beginingDate.Month));

                }
                if (incomebeginDate != DateTime.MinValue && incomebeginDate < beginingDate)
                {
                    beginingDate = new DateTime(incomebeginDate.Year, incomebeginDate.Month, 1);
                }
                if (expensebeginDate != DateTime.MinValue && expensebeginDate < beginingDate)
                {
                    beginingDate = new DateTime(expensebeginDate.Year, expensebeginDate.Month, 1);
                }
                if (cashflowbeginDate != DateTime.MinValue && cashflowbeginDate < beginingDate)
                {
                    beginingDate = new DateTime(cashflowbeginDate.Year, cashflowbeginDate.Month, 1);
                }
                var startingCashData = cashFlowData.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.StartingCash)).FirstOrDefault();
                if (startingCashData != null)
                {
                    var dtFirstday = new DateTime(startingCashData.PaymentDate.Year, startingCashData.PaymentDate.Month, 1);
                    if (dtFirstday <= beginingDate)
                    {
                        startingCash = startingCashData.Amount.ToDecimal();
                    }
                }
                while (beginingDate <= endDate)
                {
                    DateRange.Add(beginingDate);
                    beginingDate = beginingDate.AddMonths(1);
                }
                foreach (var date in DateRange)
                {
                    var incomeInfo = new List<Income>(incomeData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund));
                    var expenseInfo = new List<Expense>(expenseData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel));
                    var cashFlowInfo = new List<CashFlows>(cashFlowData.Where(x => pIds.Contains(x.PropertyId) && x.PaymentDate.Month == date.Month && x.PaymentDate.Year == date.Year));
                    var incomeRefundInfo = new List<Income>(incomeData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund));
                    var expenseRefundInfo = new List<Expense>(expenseData.Where(x => pIds.Contains(x.PropertyId) && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel));
                    //if (date.Month == 6 && date.Year==2019)
                    //{

                    //}
                    if (incomeInfo.Any() || expenseInfo.Any() || cashFlowInfo.Any())
                    {
                        //startingCash = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.StartingCash)).Sum(x => x.Amount).ToDecimal();
                        decimal totalExpense = 0;
                        decimal totalExpenseRefund = 0;
                        decimal totalIncome = 0;
                        decimal totalIncomeRefund = 0;
                        string NewInvestmentSummary = "<h2>New Investment</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string PartnerInvestmentSummary = "<h2>Partner Investment</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string WriteOffSummary = "<h2>Write Off</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string CashOutSummary = "<h2>Cash Out</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string PartnerDrawSummary = "<h2>Partner Draw</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string PartnerInvestmentDrawSummary = "<h2>Partner Investment Draw</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Owner</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string IncomeSummary = "<h2>Income</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        string ExpenseSummary = "<h2>Expense</h2><table class=\"ui celled table summary-table\"><thead><tr><th>Category</th><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        using (var db = new ApplicationDbContext())
                        {
                            //JM 7/15/2020 Calculate Income
                            foreach (var income in incomeInfo)
                            {
                                var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                                if (payments.Count > 0)
                                {
                                    IncomeSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                    totalIncome += payments.Sum(x => x.Amount);
                                }
                            }
                            //JM 7/15/2020 Calculate Expense
                            foreach (var expense in expenseInfo)
                            {
                                var payments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                                if (payments.Count > 0)
                                {
                                    ExpenseSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                    totalExpense += payments.Sum(x => x.Amount);
                                }

                            }

                            //JM 7/15/2020 Calculate Income refund
                            //IncomeSummary += "<h2>Refund</h2>";
                            foreach (var income in incomeRefundInfo)
                            {
                                var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                                if (payments.Count > 0)
                                {
                                    IncomeSummary += "<tr><td>" + db.IncomeTypes.Where(x => x.Id == income.IncomeTypeId).Select(x => x.TypeName).FirstOrDefault() + "</td><td>" + income.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                    totalIncomeRefund += payments.Sum(x => x.Amount);
                                }
                            }
                            //JM 7/15/2020 Calculate Expense refund
                            //ExpenseSummary += "<h2>Refund</h2>";
                            foreach (var expense in expenseRefundInfo)
                            {
                                var payments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                                if (payments.Count > 0)
                                {
                                    ExpenseSummary += "<tr><td>" + db.ExpenseCategories.Where(x => x.Id == expense.ExpenseCategoryId).Select(x => x.ExpenseCategoryType).FirstOrDefault() + "</td><td>" + expense.Description + "</td><td>" + payments.Sum(x => x.Amount) + "</td></tr>";
                                    totalExpenseRefund += payments.Sum(x => x.Amount);
                                }
                            }

                        }
                        decimal SuggestedStartingCash = 0;
                        var thisMonthCashflow = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.StartingCash)).ToList();
                        if (thisMonthCashflow.Count > 0)
                        {
                            SuggestedStartingCash = thisMonthCashflow.Sum(x => x.Amount).ToDecimal();
                        }
                        var shareExpense = 0;
                        var nettCashFlow = Math.Abs(totalIncome - totalIncomeRefund) - Math.Abs(totalExpense - totalExpenseRefund);
                        var totalNewInvestment = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.NewInvestment)).Sum(x => x.Amount).ToDecimal();
                        var totalPartnerInvestment = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestment)).Sum(x => x.Amount).ToDecimal();
                        var totalWriteOff = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.WriteOff)).Sum(x => x.Amount).ToDecimal();
                        var totalCashOut = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.CashOut)).Sum(x => x.Amount).ToDecimal();
                        var totalPartnerDraw = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.OwnerDraw)).Sum(x => x.Amount).ToDecimal();
                        var totalPartnerInvestmentDraw = cashFlowInfo.Where(x => x.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestmentDraw)).Sum(x => x.Amount).ToDecimal();
                        var totalEndingCash = (startingCash + totalNewInvestment + totalPartnerInvestment) -
                                              (totalWriteOff + totalCashOut + totalPartnerDraw +
                                               totalPartnerInvestmentDraw);
                        //Get All Summary
                        using (var db = new ApplicationDbContext())
                        {
                            (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.NewInvestment) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => NewInvestmentSummary += "<tr>" + x.summary + "</tr>");
                            (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestment) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => PartnerInvestmentSummary += "<tr>" + x.summary + "</tr>");
                            (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.WriteOff) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => WriteOffSummary += "<tr>" + x.summary + "</tr>");
                            (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.CashOut) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => CashOutSummary += "<tr>" + x.summary + "</tr>");
                            (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.OwnerDraw) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => PartnerDrawSummary += "<tr>" + x.summary + "</tr>");
                            (from cashflow in cashFlowInfo where cashflow.CashFlowTypeId.Equals((int)CashFlowTypeEnum.PartnerInvestmentDraw) select new { summary = "<td>" + (from owner in db.Users where owner.Id == cashflow.OwnerId select new { owner = owner.FirstName + " " + owner.LastName }).FirstOrDefault() + "</td><td>" + cashflow.Description + "</td><td>" + cashflow.Amount + "</td>" }).ToList().ForEach(x => PartnerInvestmentDrawSummary += "<tr>" + x.summary + "</tr>");
                        }
                        if (nettCashFlow >= 0)
                        {
                            totalEndingCash += nettCashFlow;
                        }
                        else
                        {
                            totalEndingCash -= Math.Abs(nettCashFlow);
                        }



                        cashFlowReportList.Add(new CashFlowReportModel
                        {
                            SuggestedStartingCash = SuggestedStartingCash,
                            IncomeSummary = js.Serialize(IncomeSummary + "</tbody></table>"),
                            ExpenseSummary = js.Serialize(ExpenseSummary + "</tbody></table>"),
                            CashOutSummary = js.Serialize(CashOutSummary + "</tbody></table>"),
                            NewInvestmentSummary = js.Serialize(NewInvestmentSummary + "</tbody></table>"),
                            PartnerDrawSummary = js.Serialize(PartnerDrawSummary + "</tbody></table>"),
                            PartnerInvestmentDrawSummary = js.Serialize(PartnerInvestmentDrawSummary + "</tbody></table>"),
                            PartnerInvestmentSummary = js.Serialize(PartnerInvestmentSummary + "</tbody></table>"),
                            WriteOffSummary = js.Serialize(WriteOffSummary + "</tbody></table>"),
                            Date = date,
                            Month = date.Month,
                            Year = date.Year,
                            StartingCash = startingCash,
                            NewInvestment = totalNewInvestment,
                            PartnerInvestment = totalPartnerInvestment,
                            WriteOff = totalWriteOff,
                            CashOut = totalCashOut,
                            PartnerDraw = totalPartnerDraw,
                            PartnerInvestmentDraw = totalPartnerInvestmentDraw,
                            TotalIncome = totalIncome,
                            TotalExpense = totalExpense,
                            TotalIncomeRefund = totalIncomeRefund,
                            TotalExpenseRefund = totalExpenseRefund,
                            ShareExpense = shareExpense,
                            NettCashFlow = nettCashFlow,
                            EndingCash = totalEndingCash
                        });
                        startingCash = totalEndingCash;
                    }
                }

                fullTotalIncome = cashFlowReportList.Sum(x => x.TotalIncome);
                fullTotalExpense = cashFlowReportList.Sum(x => x.TotalExpense);
                fullTotalShareExpense = cashFlowReportList.Sum(x => x.ShareExpense);
                fullTotalNetCashFlow = cashFlowReportList.Sum(x => x.NettCashFlow);

                totalPageCount = (int)Math.Ceiling((double)cashFlowReportList.Count / SiteConstants.TotalRecordPerPage);

                //if (pageNumber != 1)
                //{
                //    int totalRecordsToSkip = (pageNumber - 1) * SiteConstants.TotalRecordPerPage;
                //    cashFlowReportList =
                //        new List<CashFlowReportModel>(
                //            cashFlowReportList.OrderBy(x => x.Month)
                //                .Skip(totalRecordsToSkip)
                //                .Take(SiteConstants.TotalRecordPerPage));
                //}
                //else
                //{
                //    cashFlowReportList = new List<CashFlowReportModel>(cashFlowReportList.Take(SiteConstants.TotalRecordPerPage));
                //}

                status = (int)Enumerations.Cashflow.StatusEnum.SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            var response = new { cashFlowReportList = cashFlowReportList.Where(x => x.Date >= startDate && x.Date <= endDate).OrderByDescending(x => x.Date), status };
            return Json(response);
        }

        [HttpPost]
        public ActionResult GetDashboardTotal(string propertyList, string dateFrom, string dateTo, bool includeChild)
        {
            int status = (int)Enumerations.Cashflow.StatusEnum.FAIL;
            string[] selectedProperties = propertyList.Split(',');
            List<int> fullPropertyList = selectedProperties.Select(item => Int32.Parse(item)).ToList();
            List<Income> incomeList = new List<Income>();
            List<Expense> expenseList = new List<Expense>();
            List<Income> incomeRefundList = new List<Income>();
            List<Expense> expenseRefundList = new List<Expense>();
            List<CashFlows> cashFlowList = new List<CashFlows>();

            List<ProcessDateModel> monthYearRange = new List<ProcessDateModel>();
            decimal fullTotalIncomeRefund = 0, fullTotalIncome = 0, fullTotalExpenseRefund = 0, fullTotalExpense = 0, fullTotalShareExpense = 0, fullTotalNetCashFlow = 0;
            try
            {

                var startDate = dateFrom.ToDateTime();
                var endDate = dateTo.ToDateTime();
                var DateRange = new List<DateTime>();
                while (startDate <= endDate)
                {
                    DateRange.Add(startDate);
                    startDate = startDate.AddMonths(1);
                }

                foreach (var property in fullPropertyList)
                {
                    var incomeData = objIList.GetIncomes(property, includeChild);
                    var expenseData = objIList.GetExpenses(property, includeChild);
                    foreach (var date in DateRange)
                    {
                        incomeList.AddRange(incomeData.Where(x => x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund));
                        expenseList.AddRange(expenseData.Where(x => x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId != (int)Core.Enumerations.ExpenseCategory.RefundCancel));
                        incomeRefundList.AddRange(incomeData.Where(x => x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.IncomeTypeId == (int)Core.Enumerations.IncomeType.Refund));
                        expenseRefundList.AddRange(expenseData.Where(x => x.DueDate.Month == date.Month && x.DueDate.Year == date.Year && x.ExpenseCategoryId == (int)Core.Enumerations.ExpenseCategory.RefundCancel));


                        //    incomeList.AddRange(objIList.GetIncomes().Where(x => x.PropertyId == property && x.DueDate.Month==date.Month && x.DueDate.Year == date.Year));
                        //    expenseList.AddRange(objIList.GetExpenses().Where(x => x.PropertyId == property && x.DueDate.Month == date.Month && x.DueDate.Year == date.Year));
                        //cashFlowList.AddRange(objIList.GetCashFlows().Where(x => x.PropertyId == property && x.PaymentDate.Year == date.Month && x.PaymentDate.Year == date.Year));
                    }
                }

                using (var db = new ApplicationDbContext())
                {
                    foreach (var income in incomeList)
                    {
                        var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                        fullTotalIncome += payments.Sum(x => x.Amount);
                    }
                    foreach (var expense in expenseList)
                    {
                        var payments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                        fullTotalExpense += payments.Sum(x => x.Amount);
                    }
                    foreach (var income in incomeRefundList)
                    {
                        var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                        fullTotalIncomeRefund += payments.Sum(x => x.Amount);
                    }
                    foreach (var expense in expenseRefundList)
                    {
                        var payments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                        fullTotalExpenseRefund += payments.Sum(x => x.Amount);
                    }
                }
                fullTotalNetCashFlow = (fullTotalIncome - fullTotalIncomeRefund) - (fullTotalExpense - fullTotalExpenseRefund) - Math.Abs(fullTotalShareExpense);

                status = (int)Enumerations.Cashflow.StatusEnum.SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            var response = new { status, fullTotalIncome = (fullTotalIncome - fullTotalIncomeRefund), fullTotalExpense = (fullTotalExpense - fullTotalExpenseRefund), fullTotalShareExpense, fullTotalNetCashFlow };
            return Json(response);
        }
    }
}