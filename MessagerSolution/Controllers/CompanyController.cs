﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using MessagerSolution.Models.Properties;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            using (var db = new ApplicationDbContext())
            {
                return View(db.Companies.Where(x => x.Id == SessionHandler.CompanyId).FirstOrDefault());
            }
        }
        [HttpPost]
        public ActionResult UpdateDetails(Company company, HttpPostedFileWrapper Image, HttpPostedFileWrapper BackgroundImage)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Companies.Where(x => x.Id == SessionHandler.CompanyId).FirstOrDefault();
                if (temp != null)
                {
                    if (Image != null)
                        temp.Logo = UploadImage(Image, "~/Images/Company/");
                    if (BackgroundImage != null)
                        temp.BackgroundImage = UploadImage(BackgroundImage, "~/Images/Company/");
                    temp.CompanyName = company.CompanyName;
                    temp.Email = company.Email;
                    temp.Contact = company.Contact;
                    temp.About = company.About;
                    temp.Facebook = company.Facebook;
                    temp.Twitter = company.Twitter;
                    temp.Address = company.Address;
                    temp.BackgroundColor = company.BackgroundColor;
                    temp.TrimColor = company.TrimColor;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }

        [HttpGet]
        [Route("property/home")]
        public ActionResult PropertySearch(int companyId)
        {
            using (var db = new ApplicationDbContext())
            {
                ViewBag.Company = db.Companies.Where(x => x.Id == companyId).FirstOrDefault();
                return View();
            }
        }
        [HttpGet]
        public ActionResult GetAvailableProperty(int companyId, string bedroom, string bathroom, string size, string sleeps)
        {
            using (var db = new ApplicationDbContext())
            {
                int bedrooms = bedroom.ToInt();
                int bathrooms = bathroom.ToInt();
                int? sizes = string.IsNullOrWhiteSpace(size) ? (int?)null : (int?)size.ToInt();
                int sleep = sleeps.ToInt();
                List<DisplayPropertyModel> properties = new List<DisplayPropertyModel>();
                var data = db.Properties.Where(x => x.CompanyId == companyId && x.IsActive && x.SiteType == 0 && x.IsParentProperty == false && (x.Bedrooms >= bedrooms && x.Bathrooms >= bathrooms && (x.Size == null ? true : x.Size >= sizes) && x.Sleeps >= sleep)).ToList();
                foreach (var p in data)
                {
                    properties.Add(new DisplayPropertyModel()
                    {
                        Id = p.Id,
                        Amenities = (from amenity in db.Amenities join pAmenity in db.PropertyAmenities on amenity.Id equals pAmenity.AmenityId where pAmenity.PropertyId == p.Id select amenity).ToList(),
                        Images = db.PropertyImage.Where(x => x.PropertyId == p.Id).Select(x => x.FileUrl).ToList(),
                        Name = p.Name,
                        Bathrooms = p.Bathrooms.ToString(),
                        Bedrooms = p.Bedrooms.ToString(),
                        Price = p.Price,
                        Size = p.Size,
                        Sleeps = p.Sleeps
                    });
                }
                return Json(new { properties }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCompanies()
        {
            using(var db = new ApplicationDbContext())
            {
                return Json(new {data=db.Companies.ToList() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ChangeCompanyCharge(int companyId,bool isCharge)
        {
            using (var db = new ApplicationDbContext())
            {
                var company = db.Companies.Where(x => x.Id == companyId).FirstOrDefault();
                company.IsChargeCommunication = isCharge;
                db.Entry(company).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { data = db.Companies.ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}