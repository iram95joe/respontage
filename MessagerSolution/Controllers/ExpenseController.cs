﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Repository;
using MessagerSolution.Helper;
using MessagerSolution.Models;
using MessagerSolution.Models.Expense;
using MlkPwgen;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace MessagerSolution.Controllers
{
    public class ExpenseController : Controller
    {
        ICommonRepository objIList = new CommonRepository();
        //
        // GET: /Expense/
        public ActionResult Index(int companyId)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Expense") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //it is used to check the valid account to access this page
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            var propertyList = new List<Property>();
            var propertyFilter = new List<Property>();
            var propertyForCreateExpense = new List<Property>();
            using (var db = new ApplicationDbContext())
            {

                propertyList = (from h in db.Hosts
                                join hc in db.HostCompanies on h.Id equals hc.HostId
                                join p in db.Properties on h.Id equals p.HostId
                                where hc.CompanyId == SessionHandler.CompanyId
                                select p).ToList();
                propertyList.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 0).ToList());

                foreach (var p in propertyList)
                {
                    var parentChild = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                    if (parentChild == null)
                    {
                        propertyFilter.Add(p);
                    }
                }
            }
            ViewBag.PropertyListFilter = propertyFilter;
            ViewBag.PropertyList = propertyList;// objIList.GetAllProperty();
            ViewBag.ExpenseCategory = new SelectList(objIList.GetExpenseCategoryList(), "Id", "ExpenseCategoryType");
            ViewBag.PaymentMethods = new SelectList(objIList.GetPaymentMethods(), "Id", "Name");
            ViewBag.SiteTypeList = new SelectList(objIList.GetAllSiteType(), "Code", "Description");
            return View();
        }

        [HttpGet]
        public ActionResult GetExpenseCategory()
        {

            return Json(new { expenseCategory = objIList.GetExpenseCategoryList() }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult GetAssignmentExpenses(string assignmentIds, string reimbursementIds)
        {
            var ids = JsonConvert.DeserializeObject<List<int>>(assignmentIds);
            var reimburseids = JsonConvert.DeserializeObject<List<int>>(reimbursementIds);
            using (var db = new ApplicationDbContext())
            {
                int paymentMethodId = objIList.GetWorkerDefaultPaymentMethodId();
                int expenseCategoryId = objIList.GetExpenseCategoryId("Labor");
                int reimbusementExpenseCategoryId = objIList.GetExpenseCategoryId("Reimburse");
                var expenselist = new List<Expense>();
                foreach (var assignmentId in ids)
                {
                    //var temp = db.Expenses.Where(x => x.WorkerAssignmentId == assignmentId && x.ExpenseCategoryId == expenseCategoryId).FirstOrDefault();

                    //if (temp == null)
                    //{
                    objIList.CreateWorkerAssignmentExpense(assignmentId);
                    //}
                }
                foreach (var reimburseId in reimburseids)
                {
                    var temp = db.Expenses.Where(x => x.ReimbursementId == reimburseId && x.ExpenseCategoryId == reimbusementExpenseCategoryId).FirstOrDefault();
                    if (temp == null)
                    {
                        var reimbursement = db.ReimbursementNotes.Where(x => x.Id == reimburseId).FirstOrDefault();
                        //objIList.AddReinburstmentNotes(reimbursement, reimbursement.AssignmentId);
                        var assignment = (from a in db.WorkerAssignments
                                          join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                                          join j in db.WorkerJobs on new { a.WorkerId, a.JobTypeId } equals new { j.WorkerId, j.JobTypeId }
                                          where a.Id == reimbursement.AssignmentId
                                          select new
                                          {
                                              WorkerName = w.Firstname + " " + w.Lastname,
                                              DateAssigned = a.StartDate,
                                              a.EndDate,
                                              a.PropertyId,
                                              j.PaymentRate,
                                              j.PaymentType

                                          }).FirstOrDefault();
                        Expense e = new Expense();
                        e.Description = assignment.WorkerName + " - " + reimbursement.Note;
                        e.ExpenseCategoryId = reimbusementExpenseCategoryId;
                        e.PropertyId = assignment.PropertyId;
                        e.DueDate = assignment.DateAssigned;
                        e.PaymentMethodId = paymentMethodId;
                        e.DateLastJobRun = null;
                        e.DateCreated = DateTime.Now;
                        e.DateNextJobRun = null;
                        e.CompanyId = SessionHandler.CompanyId;
                        e.Amount = reimbursement.Amount;
                        e.WorkerAssignmentId = reimbursement.AssignmentId;
                        e.ReimbursementId = reimbursement.Id;
                        db.Expenses.Add(e);
                        db.SaveChanges();
                    }
                }
                var Expenses = db.Expenses.Where(x => (ids.Contains(x.WorkerAssignmentId) && x.ExpenseCategoryId == expenseCategoryId) || reimburseids.Contains(x.ReimbursementId.Value)).ToList();
                foreach (var expense in Expenses)
                {
                    var temp = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                    //decimal dueAmount = 0;
                    if (temp.Sum(x => x.Amount) < expense.Amount)
                    {
                        expense.Amount = expense.Amount - temp.Sum(x => x.Amount);
                        expenselist.Add(expense);
                    }

                }
                return Json(new { expenses = expenselist }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult GetPaymentMethods()
        {
            return Json(new { paymentMethods = objIList.GetPaymentMethods() }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        [CheckSessionTimeout]
        public JsonResult GetBatchPayment(int id)
        {
            using (var db = new ApplicationDbContext())
            {

                var batchPayment = db.ExpenseBatchPayments.Where(x => x.Id == id).FirstOrDefault();
                var payments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batchPayment.Id).ToList();
                List<BatchExpensePaymentViewModel> list = new List<BatchExpensePaymentViewModel>();
                var expenselist = new List<Expense>();
                foreach (var payment in payments)
                {
                    BatchExpensePaymentViewModel temp = new BatchExpensePaymentViewModel();
                    var Expense = db.Expenses.Where(x => x.Id == payment.ExpenseId).FirstOrDefault();
                    temp.Balance = Expense.Amount.Value;
                    temp.Amount = payment.Amount;
                    temp.ExpenseId = Expense.Id;
                    temp.ExpensePaymentId = payment.Id;
                    temp.Description = Expense.Description;
                    temp.DueDate = Expense.DueDate;
                    list.Add(temp);
                }
                var Expenses = db.Expenses.ToList();
                foreach (var expense in Expenses)
                {
                    var temp = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                    //decimal dueAmount = 0;
                    if (temp.Sum(x => x.Amount) < expense.Amount)
                    {
                        expense.Amount = expense.Amount - temp.Sum(x => x.Amount);
                        expenselist.Add(expense);
                    }

                }
                List<string> ImageUrls = db.IncomePaymentFiles.Where(x => x.BatchPaymentId == batchPayment.Id).Select(x => x.FileUrl).ToList();
                return Json(new { batchpayment = batchPayment, incomingpayment = list, monthlyrent = expenselist, images = ImageUrls }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetBatchPaymentList()
        {
            using (var db = new ApplicationDbContext())
            {
                var batchPayment = db.ExpenseBatchPayments.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                return Json(new { batchPayment }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetExpensePayments()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var batchs = db.ExpenseBatchPayments.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    var payments = (from payment in db.ExpensePayment join expense in db.Expenses on payment.ExpenseId equals expense.Id where expense.CompanyId == SessionHandler.CompanyId && payment.ExpenseBatchPaymentId == null select payment).ToList();
                    List<ExpensePaymentViewModel> list = new List<ExpensePaymentViewModel>();
                    foreach (var batch in batchs)
                    {
                        ExpensePaymentViewModel temp = new ExpensePaymentViewModel();
                        temp.Id = batch.Id;
                        temp.Description = batch.Description;
                        temp.Date = batch.PaymentDate;// batch.PaymentDate.ToString("MMMM d,yyyy");
                        temp.Action = "";
                        var expensePayments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batch.Id).ToList();
                        decimal dueAmount = 0;
                        dueAmount = expensePayments.Sum(x => x.Amount);

                        var totalpay = expensePayments.Sum(x => x.Amount);
                        var unallocated = batch.Amount - totalpay;
                        temp.Status = unallocated == 0 ? "Full-allocated" : batch.Amount == unallocated ? "Unallocated" : "Partial-allocated";
                        temp.Amount = batch.Amount;
                        temp.Balance = unallocated;
                        var expensepayments = db.ExpensePayment.Where(x => x.ExpenseBatchPaymentId == batch.Id).ToList();
                        if (expensepayments.Count > 0)
                        {
                            foreach (var pay in expensepayments)
                            {
                                var expense = db.Expenses.Where(x => x.Id == pay.ExpenseId).FirstOrDefault();
                                if (expense != null)
                                {
                                    var property = db.Properties.Where(x => x.Id == expense.PropertyId).FirstOrDefault();
                                    var tempayments = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                                    tempayments.Sum(x => x.Amount);
                                    temp.ExpensesPaid += (property != null ? "(" + property.Name + ")" : "") + expense.DueDate.ToString("MMM d,yyyy") + " - " + expense.Description + " - " + pay.Amount + "</br>";
                                }

                            }
                        }
                        else
                        {
                            temp.ExpensesPaid = "-";
                        }
                        temp.Action = "<button title=\"Allocate Payment\" class=\"ui mini button allocate-btn\"><i class=\"align edit icon\"></i></button>";
                        list.Add(temp);
                    }
                    foreach (var payment in payments)
                    {
                        try
                        {
                            var expense = db.Expenses.Where(x => x.Id == payment.ExpenseId).FirstOrDefault();
                            var property = db.Properties.Where(x => x.Id == expense.PropertyId).FirstOrDefault();
                            ExpensePaymentViewModel temp = new ExpensePaymentViewModel();
                            temp.Amount = payment.Amount;
                            temp.Description = payment.Description;
                            temp.Date = payment.CreatedAt;//payment.CreatedAt.ToString("MMMM d,yyyy");
                            temp.ExpensesPaid += "(" + property.Name + ")" + expense.DueDate.ToString("MMM d,yyyy") + " - " + expense.Description + " - " + payment.Amount + "</br>";
                            temp.Status = "Complete";
                            temp.Action = "";
                            list.Add(temp);
                        }
                        catch { }
                    }
                    return Json(new { data = list.OrderByDescending(x => x.Date) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { data = new List<ExpensePaymentViewModel>() }, JsonRequestBehavior.AllowGet);
            }
        }
        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            System.IO.Directory.Exists(Server.MapPath(Destination));
            //var file = Model.Tasks[0].Image;
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(Server.MapPath(url));
            return url.Replace("~", "");

        }
        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult WorkerCreateBatchPayment(int batchId, string description, string paymentDate, HttpPostedFileWrapper[] Images, string amount, string payment)
        {
            using (var db = new ApplicationDbContext())
            {
                var batchPaymentId = 0;
                if (batchId == 0)
                {
                    var batchPayment = new ExpenseBatchPayment()
                    {
                        Amount = amount.ToDecimal(),
                        Description = description,
                        PaymentDate = paymentDate.ToDateTime(),
                        CompanyId = SessionHandler.CompanyId
                    };
                    db.ExpenseBatchPayments.Add(batchPayment);
                    db.SaveChanges();
                    batchPaymentId = batchPayment.Id;
                }
                else
                {
                    var temp = db.ExpenseBatchPayments.Where(x => x.Id == batchId).FirstOrDefault();
                    temp.Amount = amount.ToDecimal();
                    temp.Description = description;
                    temp.PaymentDate = paymentDate.ToDateTime();
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    batchPaymentId = temp.Id;
                }
                var data = JArray.Parse(payment);
                if (Images != null)
                {
                    foreach (var image in Images)
                    {
                        var ExpensePaymentImage = new ExpensePaymentFile() { BatchPaymentId = batchPaymentId, FileUrl = UploadImage(image, "~/Images/ExpensePayments/") };
                        db.ExpensePaymentFiles.Add(ExpensePaymentImage);
                        db.SaveChanges();
                    }
                }
                foreach (var expense in data)
                {
                    if (expense["ExpenseId"].ToInt() != 0)
                    {
                        if (expense["Amount"].ToString() != "" && expense["Amount"].ToString() != null && expense["Amount"].ToString() != "0")
                        {
                            if (expense["Category"].ToSafeString() == "Fixed" || expense["Category"].ToSafeString() == "Retainer" || expense["Category"].ToSafeString() == "Bonus")
                            {
                                var tempId = expense["ExpenseId"].ToInt();
                                var tempExpense = db.Expenses.Where(x => x.Id == tempId).FirstOrDefault();
                                tempExpense.Amount = expense["Amount"].ToDecimal();
                                db.Entry(tempExpense).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            if (expense["PaymentId"].ToString() == "0")
                            {
                                ExpensePayment objIncomePayment = new ExpensePayment();
                                objIncomePayment.Amount = expense["Amount"].ToDecimal();
                                objIncomePayment.CreatedAt = paymentDate.ToDateTime();
                                objIncomePayment.Description = description;
                                objIncomePayment.ExpenseId = expense["ExpenseId"].ToInt();
                                objIncomePayment.ExpenseBatchPaymentId = batchPaymentId;
                                db.ExpensePayment.Add(objIncomePayment);
                                db.SaveChanges();
                            }
                            else
                            {
                                var tempid = expense["PaymentId"].ToInt();
                                var tempPayment = db.ExpensePayment.Where(x => x.Id == tempid).FirstOrDefault();
                                tempPayment.Amount = expense["Amount"].ToDecimal();
                                tempPayment.Description = description;
                                tempPayment.CreatedAt = paymentDate.ToDateTime();
                                db.Entry(tempPayment).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }
                        else if ((expense["Amount"].ToString() == "" || expense["Amount"].ToString() == null || expense["Amount"].ToString() == "0") && expense["PaymentId"].ToString() != "0")
                        {
                            if (expense["Category"].ToSafeString() == "Retainer" || expense["Category"].ToSafeString() == "Bonus")
                            {
                                var tempId = expense["ExpenseId"].ToInt();
                                var tempexp = db.Expenses.Where(x => x.Id == tempId).FirstOrDefault();
                                db.Expenses.Remove(tempexp);
                            }
                            var id = expense["PaymentId"].ToInt();
                            db.ExpensePayment.Remove(db.ExpensePayment.Where(x => x.Id == id).FirstOrDefault());
                            db.SaveChanges();
                        }
                    }
                    else if ((expense["Amount"].ToString() != "" && expense["Amount"].ToString() != null && expense["Amount"].ToString() != "0"))
                    {
                        int expenseCategoryId = objIList.GetExpenseCategoryId(expense["Category"].ToString());
                        int paymentMethodId = objIList.GetWorkerDefaultPaymentMethodId();
                        var dt = expense["DueDate"].ToDateTime();
                        DateTime dueDate = new DateTime(dt.Year, dt.Month,
                                         DateTime.DaysInMonth(dt.Year, dt.Month));
                        var desc = expense["Description"].ToString();

                        var temp = db.Expenses.Where(x => x.Description == desc && x.DueDate.Month == dt.Month && x.DueDate.Year == dt.Year && x.ExpenseCategoryId == expenseCategoryId).FirstOrDefault();
                        if (temp == null)
                        {
                            Expense e = new Expense();
                            e.Description = expense["Description"].ToString();
                            e.ExpenseCategoryId = expenseCategoryId;
                            e.PropertyId = 0;
                            e.DueDate = dueDate;
                            e.PaymentMethodId = paymentMethodId;
                            e.DateLastJobRun = null;
                            e.DateCreated = DateTime.Now;
                            e.DateNextJobRun = null;
                            e.CompanyId = SessionHandler.CompanyId;
                            e.DateCreated = paymentDate.ToDateTime();
                            //e.IsPaid = false;
                            e.Amount = expense["Amount"].ToDecimal();//expense["Category"].ToString()=="Bonus"? expense["Amount"].ToDecimal(): expense["ExpenseAmount"].ToDecimal();// assignment.PaymentRate;
                            e.WorkerAssignmentId = 0;//workerAssignmentId;
                            db.Expenses.Add(e);
                            db.SaveChanges();
                            ExpensePayment objIncomePayment = new ExpensePayment();
                            objIncomePayment.Amount = expense["Amount"].ToDecimal();
                            objIncomePayment.CreatedAt = paymentDate.ToDateTime();
                            objIncomePayment.Description = description;
                            objIncomePayment.ExpenseId = e.Id;
                            objIncomePayment.ExpenseBatchPaymentId = batchPaymentId;
                            db.ExpensePayment.Add(objIncomePayment);
                            db.SaveChanges();
                        }
                        else
                        {
                            temp.DueDate = dueDate;
                            temp.Amount = expense["Amount"].ToDecimal();
                            db.SaveChanges();
                            ExpensePayment objIncomePayment = new ExpensePayment();
                            objIncomePayment.Amount = expense["Amount"].ToDecimal();
                            objIncomePayment.CreatedAt = paymentDate.ToDateTime();
                            objIncomePayment.Description = description;
                            objIncomePayment.ExpenseId = temp.Id;
                            objIncomePayment.ExpenseBatchPaymentId = batchPaymentId;
                            db.ExpensePayment.Add(objIncomePayment);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckSessionTimeout]
        public JsonResult CreateBatchPayment(int batchId, string description, string paymentDate, HttpPostedFileWrapper[] Images, string amount, string payment)
        {
            using (var db = new ApplicationDbContext())
            {
                var batchPaymentId = 0;
                if (batchId == 0)
                {
                    var batchPayment = new ExpenseBatchPayment()
                    {
                        Amount = amount.ToDecimal(),
                        Description = description,
                        PaymentDate = paymentDate.ToDateTime(),
                        CompanyId = SessionHandler.CompanyId
                    };
                    db.ExpenseBatchPayments.Add(batchPayment);
                    db.SaveChanges();
                    batchPaymentId = batchPayment.Id;
                }
                else
                {
                    var temp = db.ExpenseBatchPayments.Where(x => x.Id == batchId).FirstOrDefault();
                    temp.Amount = amount.ToDecimal();
                    temp.Description = description;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    batchPaymentId = temp.Id;
                }
                var data = JArray.Parse(payment);
                if (Images != null)
                {
                    foreach (var image in Images)
                    {
                        db.ExpensePaymentFiles.Add(new ExpensePaymentFile() { BatchPaymentId = batchPaymentId, FileUrl = UploadImage(image, "~/Images/ExpensePayments/") });
                        db.SaveChanges();
                    }
                }
                foreach (var expense in data)
                {
                    if (expense["Amount"].ToString() != "" && expense["Amount"].ToString() != null && expense["Amount"].ToString() != "0")
                    {
                        if (expense["PaymentId"].ToString() == "0")
                        {
                            ExpensePayment objIncomePayment = new ExpensePayment();
                            objIncomePayment.Amount = expense["Amount"].ToDecimal();
                            objIncomePayment.CreatedAt = paymentDate.ToDateTime();
                            objIncomePayment.Description = description;
                            objIncomePayment.ExpenseId = expense["ExpenseId"].ToInt();
                            objIncomePayment.ExpenseBatchPaymentId = batchPaymentId;
                            db.ExpensePayment.Add(objIncomePayment);
                            db.SaveChanges();
                        }
                        else
                        {
                            var tempid = expense["PaymentId"].ToInt();
                            var tempPayment = db.ExpensePayment.Where(x => x.Id == tempid).FirstOrDefault();
                            tempPayment.Amount = expense["Amount"].ToDecimal();
                            tempPayment.Description = description;
                            db.Entry(tempPayment).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                    }
                    else if ((expense["Amount"].ToString() == "" || expense["Amount"].ToString() == null || expense["Amount"].ToString() == "0") && expense["PaymentId"].ToString() != "0")
                    {
                        var id = expense["PaymentId"].ToInt();
                        db.ExpensePayment.Remove(db.ExpensePayment.Where(x => x.Id == id).FirstOrDefault());
                        db.SaveChanges();
                    }
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUnpaidExpensesByProperty(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<Expense>();
                var expenses = db.Expenses.Where(x => x.PropertyId == propertyId).ToList();
                foreach (var expense in expenses)
                {
                    var temp = db.ExpensePayment.Where(x => x.ExpenseId == expense.Id).ToList();
                    if (temp.Sum(x => x.Amount) < expense.Amount)
                    {
                        expense.Amount = expense.Amount - temp.Sum(x => x.Amount);
                        list.Add(expense);
                    }
                }
                return Json(new { success = true, expenses = list }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EditFeeType(FeeType model)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSaved = false;
            if (objIList.UpdateFeeType(model))
            {
                isSaved = true;
            }
            return Json(new { success = isSaved }, JsonRequestBehavior.AllowGet);
        }

        // POST: /FeeType/Delete
        [HttpPost]
        public ActionResult DeleteFeeType(int id)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool success = false;
            try
            {
                if (objIList.DeleteFeeType(id))
                {
                    success = true;
                }
            }
            catch { }
            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        // POST: /FeeType/Create
        [HttpPost]
        public ActionResult CreateFeeType(string feeType, int type)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool success = false;
            try
            {
                FeeType model = new FeeType();
                model.Fee_Type = feeType;
                model.Type = type;
                model.CompanyId = type == 1 ? SessionHandler.CompanyId : 0;
                if (objIList.InsertFeeType(model))
                {
                    success = true;
                }
            }
            catch { }
            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FindFeeTypeById(int id)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSuccess = false;
            FeeType model = new FeeType();
            try
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                using (var db = new ApplicationDbContext())
                {
                    model = db.FeeTypes.FirstOrDefault(i => i.CompanyId == companyId && i.Id == id);
                    if (model != null) isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, fee_type = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFeeTypeList(string type)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        List<FeeTypeTableData> tempData = new List<FeeTypeTableData>();
                        var data = db.FeeTypes
                            .Where(x => (x.CompanyId == SessionHandler.CompanyId && x.Type == 1) || x.Type == 2)
                            .ToList();
                        if (type != "all")
                        {
                            int feeType = int.Parse(type);
                            data = data.Where(x => x.Type == feeType).ToList();
                        }
                        data.ForEach(x =>
                        {
                            FeeTypeTableData temp = new FeeTypeTableData();
                            temp.Id = x.Id.ToString();
                            temp.Name = x.Fee_Type;

                            temp.Type = x.Type == 1 ? "Private" : x.Type == 2 ? "Public" : "";
                            temp.Actions = x.Type == 1 ? ("<a class=\"ui mini button edit-fee-type-btn\" title=\"Edit\">" +
                                            "<i class=\"icon edit\"></i>" +
                                            "</a>" +
                                            "<a class=\"ui mini button delete-fee-type-btn\" title=\"Delete\">" +
                                                "<i class=\"icon ban\"></i>" +
                                                "</a>") : "";
                            tempData.Add(temp);
                        });
                        return Json(new { data = tempData.OrderBy(x => x.Name) }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult GetExpenseList(string propertyFilter, string dueDateFilter, string paymentDateFilter, string statusFilter, bool includeChild)
        {
            if (User.Identity.IsAuthenticated)
            {
                List<ExpenseList> el = null;
                try
                {
                    el = new List<ExpenseList>();
                    using (var db = new ApplicationDbContext())
                    {
                        var bookings = db.Inquiries
                            .Join(db.Hosts, x => x.HostId, y => y.Id, (x, y) => new { Host = y, Inquiry = x, })
                            .Join(db.HostCompanies, x => x.Host.Id, y => y.HostId, (x, y) => new { HostCompany = y, Host = x.Host, Inquiry = x.Inquiry, })
                            .Join(db.Guests, x => x.Inquiry.GuestId, y => y.Id, (x, y) => new { Inquiry = x.Inquiry, Guest = y, HostCompany = x.HostCompany, Host = x.Host })
                            .Where(x => x.HostCompany.CompanyId == SessionHandler.CompanyId && x.Inquiry.CheckInDate != null && x.Inquiry.CheckOutDate != null)
                            .ToList();

                        var feeTypes = db.FeeTypes.ToList();

                        var properties = (from h in db.Hosts
                                          join hc in db.HostCompanies on h.Id equals hc.HostId
                                          join p in db.Properties on h.Id equals p.HostId
                                          where hc.CompanyId == SessionHandler.CompanyId
                                          select p).ToList();
                        properties.AddRange(db.Properties.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList());

                        var expenses = db.Expenses
                            .Join(db.ExpenseCategories, x => x.ExpenseCategoryId, y => y.Id, (x, y) => new { Expenses = x, ExpenseCategory = y })
                            .Join(db.Properties, x => x.Expenses.PropertyId, y => y.Id, (x, y) => new { Expenses = x.Expenses, ExpenseCategory = x.ExpenseCategory, Property = y })
                            .Where(x => x.Expenses.CompanyId == SessionHandler.CompanyId && x.Expenses.IsActive)
                            .ToList();
                        string[] propertyFilters = null;
                        List<int> propertyIds = new List<int>();
                        try { propertyFilters = propertyFilter.Split(','); }
                        catch (Exception e) { propertyFilters = null; }
                        //JM 10/29/19 Get Child property of parent
                        foreach (var id in propertyFilters)
                        {
                            var pId = id.ToInt();
                            propertyIds.Add(pId);
                            var tempProperty = db.Properties.Where(x => x.IsParentProperty && x.Id == pId).FirstOrDefault();
                            if (tempProperty != null)
                            {
                                if (includeChild)
                                {
                                    propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == pId && x.CompanyId == SessionHandler.CompanyId).Select(x => x.ChildPropertyId).ToList());
                                }
                            }

                        }
                        var RetainerFixed = db.Expenses.Where(x => x.PropertyId == 0 && x.CompanyId == SessionHandler.CompanyId).ToList();
                        if (!propertyFilters.Contains("all") && !propertyFilters.Contains("0"))
                        {
                            int propertyId = int.Parse(propertyFilter);
                            expenses = expenses.Where(x => propertyIds.ToList().Contains(x.Property.Id)).ToList();
                            RetainerFixed = RetainerFixed.Where(x => propertyIds.ToList().Contains(x.PropertyId))
                                                      .ToList();
                        }
                        if (!string.IsNullOrEmpty(dueDateFilter))
                        {
                            try
                            {
                                DateTime dt;
                                DateTime.TryParse(dueDateFilter, out dt);
                                expenses = expenses.Where(x => x.Expenses.DueDate.Month == dt.Month)
                                                   .Where(x => x.Expenses.DueDate.Year == dt.Year)
                                                   .ToList();
                                RetainerFixed = RetainerFixed.Where(x => x.DueDate.Month == dt.Month)
                                                       .Where(x => x.DueDate.Year == dt.Year)
                                                       .ToList();
                            }
                            catch { }
                        }
                        if (!string.IsNullOrEmpty(paymentDateFilter))
                        {
                            try
                            {//Remove because payment date is now computed base on payments table
                                DateTime dt;
                                DateTime.TryParse(paymentDateFilter, out dt);
                                expenses = expenses.Where(x => x.Expenses.PaymentDate?.Month == dt.Month)
                                                   .Where(x => x.Expenses.PaymentDate?.Year == dt.Year)
                                                   .ToList();
                                RetainerFixed = RetainerFixed.Where(x => x.PaymentDate?.Month == dt.Month)
                                                       .Where(x => x.PaymentDate?.Year == dt.Year)
                                                       .ToList();
                            }
                            catch { }
                        }
                        if (statusFilter != "all")
                        {
                            var tempStatus = Convert.ToBoolean(statusFilter);
                            expenses = expenses.Where(x => x.Expenses.IsPaid == tempStatus).ToList();
                        }

                        expenses.ForEach(item =>
                        {
                            var tempBooking = item.Expenses.BookingId == 0 ? null : bookings.Where(x => x.Inquiry.Id == item.Expenses.BookingId).FirstOrDefault();
                            var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Expenses.Id).ToList();
                            ExpenseList temp = new ExpenseList();
                            temp.Amount = string.Format("{0:#,0.00}", item.Expenses.Amount);
                            temp.BookingCode = item.Expenses.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.Inquiry.ConfirmationCode;
                            temp.Category = item.ExpenseCategory.ExpenseCategoryType;
                            temp.CreatedDate = item.Expenses.DateCreated.ToString("MMM d, yyyy");
                            temp.Description = item.Expenses.Description;
                            temp.DueDate = item.Expenses.DueDate.ToString("MMM d, yyyy");
                            temp.FeeName = feeTypes.Where(x => x.Id == item.Expenses.FeeTypeId).FirstOrDefault() == null ? "-" : feeTypes.Where(x => x.Id == item.Expenses.FeeTypeId).FirstOrDefault().Fee_Type;
                            temp.Guest = tempBooking == null ? "-" : tempBooking.Guest.Name;
                            var tempProperty = properties.Where(y => y.Id == item.Expenses.PropertyId).FirstOrDefault();
                            var syncParent = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                            temp.PropertyName = syncParent != null ? db.Properties.Where(x => x.Id == syncParent.ParentPropertyId).Select(x => x.Name).FirstOrDefault() : tempProperty.Name;  //tempProperty.ParentPropertyId == 0 ? tempProperty.Name : propertyList.Where(x => x.Id == tempProperty.ParentPropertyId).FirstOrDefault().Name;
                            temp.PaymentDate = payments.Count > 0 ? payments.OrderByDescending(x => x.CreatedAt).Select(x => x.CreatedAt).FirstOrDefault().ToString("MMM dd,yyyy") : "";
                            temp.Status = payments.Sum(x => x.Amount) == 0.0M ? "" : payments.Sum(x => x.Amount) == item.Expenses.Amount.ToDecimal() ? "Paid" : payments.Sum(x => x.Amount) > 0 ? "Partial" : "New";
                            temp.StayDate = item.Expenses.BookingId == 0 ? "-" : tempBooking == null ? "-" : tempBooking.Inquiry.CheckInDate.Value.ToString("MMM d-") + (tempBooking.Inquiry.CheckOutDate.Value.Month == tempBooking.Inquiry.CheckInDate.Value.Month ? tempBooking.Inquiry.CheckOutDate.Value.ToString("d yyyy") : tempBooking.Inquiry.CheckOutDate.Value.ToString("MMM d yyyy"));
                            temp.Id = item.Expenses.Id;
                            temp.Actions = "<a class=\"ui mini button add-expense-payment-btn\" title=\"Add Payment\">" +
                                                "<i class=\"dollar icon\"></i>" +
                                                "<a class=\"ui mini button edit-expense-btn\" title=\"Edit\">" +
                                            "<i class=\"icon edit\"></i>" +
                                            "</a>" +
                                            "</a>" + "<a class=\"ui mini button delete-expense-btn\" title=\"Delete\">" +
                                                "<i class=\"icon trash\"></i>" +
                                            "</a>";
                            el.Add(temp);
                        });
                        foreach (var r in RetainerFixed)
                        {
                            var ExpenseCategory = db.ExpenseCategories.Where(x => x.Id == r.ExpenseCategoryId).FirstOrDefault().ExpenseCategoryType;
                            ExpenseList temp = new ExpenseList();
                            var payments = db.ExpensePayment.Where(x => x.ExpenseId == r.Id).ToList();
                            temp.Amount = string.Format("{0:#,0.00}", r.Amount);
                            temp.Category = ExpenseCategory;
                            temp.CreatedDate = r.DateCreated.ToString("MMM d, yyyy");
                            temp.PropertyName = "";
                            temp.DueDate = r.DueDate.ToString("MMM d, yyyy");
                            temp.Description = r.Description;
                            temp.PaymentDate = payments.Count > 0 ? payments.OrderByDescending(x => x.CreatedAt).Select(x => x.CreatedAt).FirstOrDefault().ToString("MMM dd,yyyy") : "";
                            temp.Status = payments.Sum(x => x.Amount) == 0.0M ? "" : payments.Sum(x => x.Amount) == r.Amount.ToDecimal() ? "Paid" : payments.Sum(x => x.Amount) > 0 ? "Partial" : "New";
                            temp.Id = r.Id;
                            temp.Actions = "<a class=\"ui mini button add-expense-payment-btn\" title=\"Add Payment\">" +
                                                  "<i class=\"dollar icon\"></i>" +
                                                  "<a class=\"ui mini button edit-expense-btn\" title=\"Edit\">" +
                                              "<i class=\"icon edit\"></i>" +
                                              "</a>" +
                                              "</a>" + "<a class=\"ui mini button delete-expense-btn\" title=\"Delete\">" +
                                                  "<i class=\"icon trash\"></i>" +
                                              "</a>";

                            el.Add(temp);
                        }
                        var jsonData = Json(new { data = el.OrderByDescending(x => x.CreatedDate.ToDateTime()).ToList() }, JsonRequestBehavior.AllowGet);
                        jsonData.MaxJsonLength = int.MaxValue;
                        return jsonData;
                    }
                }
                catch (Exception e)
                {
                    return Json(new { data = el }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult GetExpenseCategoryList(string type)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        List<ExpenseCategoryTableData> tempData = new List<ExpenseCategoryTableData>();
                        var data = db.ExpenseCategories
                            .Where(x => (x.CompanyId == SessionHandler.CompanyId && x.Type == 1) || x.Type == 2)
                            .ToList();
                        if (type != "all")
                        {
                            int expenseType = int.Parse(type);
                            data = data.Where(x => x.Type == expenseType).ToList();
                        }
                        data.ForEach(x =>
                        {
                            ExpenseCategoryTableData temp = new ExpenseCategoryTableData();
                            temp.Id = x.Id.ToString();
                            temp.Name = x.ExpenseCategoryType;
                            temp.Type = x.Type == 1 ? "Private" : x.Type == 2 ? "Public" : "";
                            temp.Actions = x.Type == 1 ? ("<a class=\"ui mini button edit-expense-category-btn\" title=\"Edit\">" +
                                            "<i class=\"icon edit\"></i>" +
                                            "</a>" +
                                            "<a class=\"ui mini button delete-expense-category-btn\" title=\"Delete\">" +
                                                "<i class=\"icon ban\"></i>" +
                                                "</a>") : "";
                            tempData.Add(temp);
                        });
                        return Json(new { data = tempData.OrderBy(x => x.Name) }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult GenerateExpense(int type, string propertyFilter, string dt = "", string month = "", string year = "", string from = "", string to = "")
        {
            using (var db = new ApplicationDbContext())
            {
                int count = 0;
                try
                {
                    var bookingList = (from h in db.Hosts
                                       join hc in db.HostCompanies on h.Id equals hc.HostId
                                       join i in db.Inquiries on h.Id equals i.HostId
                                       join g in db.Guests on i.GuestId equals g.Id
                                       where hc.CompanyId == SessionHandler.CompanyId
                                       select new { Booking = i, Guest = g }).ToList();
                    //var bookingList = db.Inquiries
                    //    .Where(x => x.BookingStatusCode.ToUpper() == "A")
                    //    .Where(x => x.CompanyId == SessionHandler.CompanyId)
                    //    .Join(db.Guests, x => x.GuestId, y => y.Id, (x, y) => new { Booking = x, Guest = y })
                    //    .ToList();
                    if (propertyFilter != "all")
                    {

                        var p = int.Parse(propertyFilter);
                        var propertyId = Core.API.Airbnb.Properties.GetAirbnbId(propertyFilter.ToInt());
                        bookingList = bookingList.Where(x => x.Booking.PropertyId == propertyId).ToList();
                    }

                    if (type == 1) // daily
                    {
                        try
                        {
                            var tempDate = Convert.ToDateTime(dt);
                            bookingList = bookingList.Where(x => x.Booking.BookingDate.HasValue && x.Booking.BookingDate.ToDateTime().Date == tempDate.Date).ToList();

                        }
                        catch { }
                    }
                    else if (type == 2) // monthly
                    {
                        try
                        {
                            var tempDate = Convert.ToDateTime(month);
                            bookingList = bookingList.Where(x => x.Booking.BookingDate.HasValue && x.Booking.BookingDate.ToDateTime().Year == tempDate.Year && x.Booking.BookingDate.ToDateTime().Month == tempDate.Month).ToList();
                        }
                        catch { }
                    }
                    else if (type == 3) // yearly
                    {
                        try
                        {
                            var tempDate = int.Parse(year);
                            bookingList = bookingList.Where(x => x.Booking.BookingDate.HasValue && x.Booking.BookingDate.ToDateTime().Year == tempDate).ToList();
                        }
                        catch { }
                    }
                    else if (type == 4) // range
                    {
                        try
                        {
                            var tempFrom = Convert.ToDateTime(from);
                            var tempTo = Convert.ToDateTime(to);
                            bookingList = bookingList.Where(x => x.Booking.BookingDate.HasValue && (x.Booking.BookingDate.ToDateTime().Date >= tempFrom.Date && x.Booking.BookingDate.ToDateTime().Date <= tempTo.Date)).ToList();
                        }
                        catch { }
                    }

                    //JM Temporary remove 9/25/18
                    //var expenseList = db.Expenses.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                    //var airbnbFeeId = objIList.GetAirbnbFeeId();
                    //bookingList.ForEach(x =>
                    //{
                    //    if(expenseList.FirstOrDefault(t => t.BookingId == x.Booking.Id) == null)
                    //    {
                    //        objIList.AddExpense(x.Booking.Id, x.Booking.PropertyId, x.Guest.Name, x.Booking.ServiceFee, airbnbFeeId, true, false, "", x.Booking.BookingDate, x.Booking.CheckInDate);

                    //    }
                    //    var workerAssignmentIds = db.WorkerAssignments.Where(y => y.ReservationId == x.Booking.ReservationId).Select(y => y.Id);

                    //    foreach (var workerAssigmentId in workerAssignmentIds)
                    //    {
                    //        if (objIList.CreateWorkerAssignmentExpense(workerAssigmentId))
                    //        {
                    //            count++;
                    //        }
                    //    }



                    //});
                    return Json(new { success = true, count = count }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { success = false, count = count }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // POST: /ExpenseCategory/Create
        [HttpPost]
        public ActionResult CreateExpenseCategory(string categoryName, int type, int companyTerm)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool success = false;
            try
            {
                ExpenseCategory model = new ExpenseCategory();
                model.ExpenseCategoryType = categoryName;
                model.Type = type;
                model.CompanyId = type == 1 ? SessionHandler.CompanyId : 0;
                model.CompanyTerm = companyTerm;
                if (objIList.InsertExpenseCategory(model))
                {
                    success = true;
                }
            }
            catch { }
            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        // POST: /ExpenseCategory/Edit
        [HttpPost]
        public ActionResult EditExpenseCategory(ExpenseCategory model)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSaved = false;
            if (objIList.UpdateExpenseCategory(model))
            {
                isSaved = true;
            }
            return Json(new { success = isSaved }, JsonRequestBehavior.AllowGet);
        }

        // POST: /ExpenseCategory/Delete
        [HttpPost]
        public ActionResult DeleteExpenseCategory(int id)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool success = false;
            try
            {
                if (objIList.DeleteExpenseCategory(id))
                {
                    success = true;
                }
            }
            catch { }
            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FindExpenseCategoryById(int id)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSuccess = false;
            ExpenseCategory model = new ExpenseCategory();
            try
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                using (var db = new ApplicationDbContext())
                {
                    model = db.ExpenseCategories.FirstOrDefault(i => i.CompanyId == companyId && i.Id == id);
                    if (model != null) isSuccess = true;
                }
            }
            catch { }
            return Json(new { success = isSuccess, expense_category = model }, JsonRequestBehavior.AllowGet);
        }
        // POST: /Expense/Create
        [HttpPost]
        public ActionResult Create(Expense expense, HttpPostedFileWrapper[] Images)
        {
            bool success = false;
            try
            {
                if (User.Identity.IsAuthenticated == false)
                {
                    return Redirect("/");
                }
                //it is used to check the valid account to access this page
                if (SessionHandler.CompanyId != 0)
                {
                    if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                    {
                        return Redirect("/");
                    }
                }
                else
                {
                    return Redirect("/");
                }
                Expense model = new Expense
                {
                    Amount = expense.Amount,
                    Description = expense.Description,
                    ExpenseCategoryId = expense.ExpenseCategoryId,
                    PropertyId = expense.PropertyId,
                    DueDate = expense.DueDate,
                    IsCapitalExpense = expense.IsCapitalExpense,
                    IsStartupCost = expense.IsStartupCost,
                    Frequency = expense.Frequency,
                    DateLastJobRun = null,
                    DateCreated = DateTime.Now,
                    DateNextJobRun = null,
                    PaymentMethodId = expense.PaymentMethodId,
                    CompanyId = SessionHandler.CompanyId,
                    IsPaid = expense.IsPaid,
                    PaymentDate = expense.PaymentDate
                };

                if (objIList.InsertExpense(model, Images) != 0)
                {
                    success = true;
                }
            }
            catch { }
            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        // POST: /Expense/Details
        [HttpPost]
        public ActionResult Details(int id)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSuccess = false;
            Expense model = new Expense();
            try
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                using (var db = new ApplicationDbContext())
                {
                    model = db.Expenses.SingleOrDefault(i => i.CompanyId == companyId && i.Id == id);
                    if (model != null) isSuccess = true;
                    var Files = db.ExpenseFiles.Where(x => x.ExpenseId == model.Id).ToList();
                    return Json(new { success = isSuccess, expense = model, Files }, JsonRequestBehavior.AllowGet);
                }
            }
            catch { }
            return Json(new { success = isSuccess, expense = model }, JsonRequestBehavior.AllowGet);
        }

        // POST: /Expense/Edit
        [HttpPost]
        public ActionResult Edit(Expense model, HttpPostedFileWrapper[] Images, string fileIds)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/");
            }
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return Redirect("/");
                }
            }
            else
            {
                return Redirect("/");
            }
            bool isSaved = false;
            using (var db = new ApplicationDbContext())
            {
                var files = db.ExpenseFiles.Where(x => x.ExpenseId == model.Id).ToList();
                var Ids = JsonConvert.DeserializeObject<List<int>>(fileIds);
                files = files.Where(x => !Ids.Contains(x.Id)).ToList();
                db.ExpenseFiles.RemoveRange(files);
                db.SaveChanges();
            }

            isSaved = objIList.EditExpense(model, Images) != 0 ? true : false;

            return Json(new { success = isSaved }, JsonRequestBehavior.AllowGet);
        }
        // POST: /Expense/Delete/
        [HttpPost]
        public ActionResult Delete(int id)
        {
            bool success = false;
            try
            {
                if (User.Identity.IsAuthenticated == false)
                {
                    return Redirect("/");
                }
                //it is used to check the valid account to access this page
                if (SessionHandler.CompanyId != 0)
                {
                    if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                    {
                        return Redirect("/");
                    }
                }
                else
                {
                    return Redirect("/");
                }
                if (objIList.DeleteExpense(id))
                {
                    success = true;
                }
            }
            catch { }
            return Json(new { success = success }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FindExpenseById(int expenseId)
        {
            var status = 0;
            object Expense = null;
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect("/Home/Index");
            }
            //ICommonRepository objCommon = new CommonRepository();
            //it is used to check the valid account to access this page
            if (SessionHandler.CompanyId != 0)
            {
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                Expense = objIList.FindExpensesById(expenseId);
                status = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            var jsonData = new { Expense, status };
            return Json(jsonData);
        }

        [HttpGet]
        public ActionResult GenerateExcelFile(int companyId, int populate = 0)
        {

            if (User.Identity.IsAuthenticated)
            {
                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Properties") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //it is used to check the valid account to access this page
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            using (var db = new ApplicationDbContext())
            {

                var booleanList = new[]
                {
                    new {
                        Value = true, Name = "Paid"
                    },
                    new {
                        Value = false, Name = "New"
                    }
                };
                string[] columns = {
                    "Id", "Description", "Expense Category", "Confirmation Code","Fee Type","Property","Amount","Status","Payment Method","Due Date","Date Payment"
                };
                var expenseCategoryList = db.ExpenseCategories.Where(x => x.CompanyId == companyId).ToList();
                var propertyList = (from h in db.Hosts
                                    join hc in db.HostCompanies on h.Id equals hc.HostId
                                    join p in db.Properties on h.Id equals p.HostId
                                    where hc.CompanyId == SessionHandler.CompanyId && p.Type == 2
                                    select p).ToList();
                //db.Properties.Where(x => x.CompanyId == companyId || x.Type == 2).ToList();
                var feeTypeList = db.FeeTypes.Where(x => x.CompanyId == companyId || x.Type == 2).ToList();
                var bookList = db.BookingStatus.ToList();
                var paymentMethodList = db.PaymentMethods.ToList();
                List<ExpenseList> expenseList = new List<ExpenseList>();

                if (populate > 0)
                {
                    var bookings = db.Inquiries.ToList();

                    var expenses = db.Expenses
                        .Join(db.ExpenseCategories, x => x.ExpenseCategoryId, y => y.Id, (x, y) => new { Expenses = x, ExpenseCategory = y })
                        .Join(db.Properties, x => x.Expenses.PropertyId, y => y.Id, (x, y) => new { Expenses = x.Expenses, ExpenseCategory = x.ExpenseCategory, Property = y })
                        .Where(x => x.Expenses.CompanyId == companyId)
                        .ToList();

                    expenses.ForEach(item =>
                    {
                        var tempBooking = item.Expenses.BookingId == 0 ? null : bookings.Where(x => x.Id == item.Expenses.BookingId).FirstOrDefault();
                        var payments = db.ExpensePayment.Where(x => x.ExpenseId == item.Expenses.Id).ToList();
                        ExpenseList temp = new ExpenseList();
                        temp.Amount = string.Format("{0:#,0.00}", item.Expenses.Amount);
                        temp.BookingCode = item.Expenses.BookingId == 0 ? "" : tempBooking == null ? "" : tempBooking.ConfirmationCode;
                        temp.Category = item.ExpenseCategory.ExpenseCategoryType;
                        temp.CreatedDate = item.Expenses.DateCreated.ToString("yyyy-MM-dd");
                        temp.Description = item.Expenses.Description;
                        temp.DueDate = item.Expenses.DueDate.ToString("yyyy-MM-dd");
                        temp.FeeName = feeTypeList.Where(x => x.Id == item.Expenses.FeeTypeId).FirstOrDefault() == null ? "" : feeTypeList.Where(x => x.Id == item.Expenses.FeeTypeId).FirstOrDefault().Fee_Type;

                        temp.PaymentDate = payments.Sum(x => x.Amount) == item.Expenses.Amount.ToDecimal() ? payments.OrderByDescending(x => x.CreatedAt).Select(x => x.CreatedAt).FirstOrDefault().ToString("yyyy-MM-dd") : "";
                        var tempProperty = propertyList.Where(x => x.Id == item.Expenses.PropertyId).FirstOrDefault();
                        var parentproperty = db.ParentChildProperties.Where(x => x.ChildPropertyId == tempProperty.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                        temp.PropertyName = parentproperty == null ? tempProperty.Name : propertyList.Where(x => x.Id == parentproperty.ParentPropertyId).FirstOrDefault().Name;
                        temp.Status = payments.Sum(x => x.Amount) == item.Expenses.Amount.ToDecimal() ? "Paid" : "New";
                        temp.PaymentMethod = paymentMethodList.Where(x => x.Id == item.Expenses.PaymentMethodId).FirstOrDefault() == null ? "" : paymentMethodList.Where(x => x.Id == item.Expenses.PaymentMethodId).FirstOrDefault().Name;
                        temp.Id = item.Expenses.Id;
                        expenseList.Add(temp);
                    });
                }

                ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;

                // Data
                if (populate > 0)
                {
                    for (var i = 0; i < expenseList.Count; i++)
                    {
                        int k = 1;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].Id;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].Description;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].Category;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].BookingCode;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].FeeName;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].PropertyName;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].Amount;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].Status;
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].PaymentMethod;
                        workSheet.Cells[i + 2, k].Style.Numberformat.Format = "yyyy-mm-dd";
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].DueDate;
                        workSheet.Cells[i + 2, k].Style.Numberformat.Format = "yyyy-mm-dd";
                        workSheet.Cells[i + 2, k++].Value = expenseList[i].PaymentDate;
                    }
                }

                //Settings
                workSheet.Protection.IsProtected = true;
                workSheet.Protection.AllowDeleteRows = true;
                workSheet.Protection.AllowInsertRows = true;
                workSheet.Protection.AllowFormatCells = true;
                workSheet.Protection.AllowFormatColumns = true;
                workSheet.Protection.AllowSelectLockedCells = false;
                workSheet.Protection.SetPassword("password");
                workSheet.View.FreezePanes(2, 1);

                //Header  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                for (var i = 0; i < columns.Length; i++)
                {
                    workSheet.Cells[1, i + 1].Value = columns[i];
                    workSheet.Column(i + 1).AutoFit();
                    if (i != 0)
                    {
                        workSheet.Column(i + 1).Style.Locked = false;
                    }
                }

                //Hidden Columns
                var j = 25;
                for (var i = 0; i < expenseCategoryList.Count; i++)
                {
                    workSheet.Cells[i + 2, j].Value = expenseCategoryList[i].ExpenseCategoryType;
                    workSheet.Cells[i + 2, j + 1].Value = expenseCategoryList[i].Id;
                }
                j += 2;
                for (var i = 0; i < feeTypeList.Count; i++)
                {
                    workSheet.Cells[i + 2, j].Value = feeTypeList[i].Fee_Type;
                    workSheet.Cells[i + 2, j + 1].Value = feeTypeList[i].Id;
                }
                j += 2;
                for (var i = 0; i < propertyList.Count; i++)
                {
                    workSheet.Cells[i + 2, j].Value = propertyList[i].Name;
                    workSheet.Cells[i + 2, j + 1].Value = propertyList[i].ListingId;
                }
                j += 2;
                for (var i = 0; i < booleanList.Length; i++)
                {
                    workSheet.Cells[i + 2, j].Value = booleanList[i].Name;
                    workSheet.Cells[i + 2, j + 1].Value = booleanList[i].Value;
                }
                j += 2;
                for (var i = 0; i < paymentMethodList.Count; i++)
                {
                    workSheet.Cells[i + 2, j].Value = paymentMethodList[i].Name;
                    workSheet.Cells[i + 2, j + 1].Value = paymentMethodList[i].Id;
                }
                j += 2;
                for (var i = 12; i < j; i++)
                {
                    workSheet.Column(i).Hidden = true;
                    workSheet.Column(i).Width = 0;
                }

                // Init Data Validation
                var dataValidationRange = new[] {
                    new { DataColumnIndex = 3, ListColumnIndex = 25 , Count = expenseCategoryList.Count },
                    new { DataColumnIndex = 5, ListColumnIndex = 27 , Count = feeTypeList.Count },
                    new { DataColumnIndex = 6, ListColumnIndex = 29 , Count = propertyList.Count },
                    new { DataColumnIndex = 8, ListColumnIndex = 31 , Count = booleanList.Length },
                    new { DataColumnIndex = 9, ListColumnIndex = 33 , Count = paymentMethodList.Count }
                };

                foreach (var data in dataValidationRange)
                {
                    for (var i = 2; i < 100; i++)
                    {
                        var validation = workSheet.Cells[i, data.DataColumnIndex].DataValidation.AddListDataValidation();
                        validation.Formula.ExcelFormula = ExcelRange.GetAddress(2, data.ListColumnIndex, data.Count + 1, data.ListColumnIndex);
                        validation.ShowErrorMessage = true;
                        validation.ErrorStyle = ExcelDataValidationWarningStyle.warning;
                        validation.ErrorTitle = "An invalid value was entered";
                        validation.Error = "Select a value from the list";
                    }
                }


                for (var i = 2; i < 100; i++)
                {
                    int k = 14;
                    foreach (var data in dataValidationRange)
                    {
                        workSheet.Cells[i, k++].Formula =
                            "IF(ISBLANK(" + GetExcelColumnName(data.DataColumnIndex) + i + "), 0 , VLOOKUP(" + GetExcelColumnName(data.DataColumnIndex) + i + ",$" + GetExcelColumnName(data.ListColumnIndex) + "$2:$" + GetExcelColumnName(data.ListColumnIndex + 1) + "$" + (data.Count + 2) + ",2,FALSE))";
                    }
                }

                // Response Excel
                string excelName = populate > 0 ? "Expense " + DateTime.Now.ToString("yyyyMMddHHmmss") : "Expense Excel Sample";
                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

                return null;

            }
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        //  JM 8/13/18 Old code of excel uploader
        [HttpPost]
        public ActionResult GenerateExcelFile()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (objIList.CheckPageRole(Convert.ToInt32(User.Identity.Name), "Properties") == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //it is used to check the valid account to access this page
                if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                {
                    return RedirectToAction("Index", "Home");
                }
                //getting page url list for user as per role
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            var products = new System.Data.DataTable("teste");
            products.Columns.Add("PropertyId", typeof(int));
            products.Columns.Add("PropertyName", typeof(string));

            var objList = objIList.GetAllProperty();
            foreach (var item in objList)
            {
                products.Rows.Add(new Object[] { item.Id, item.Name });
            }

            var products3 = new System.Data.DataTable("teste");
            products3.Columns.Add(" ", typeof(string));
            products3.Columns.Add("ExpenseCategoryType", typeof(string));
            using (var db = new ApplicationDbContext())
            {
                var objList2 = db.ExpenseCategories.ToList();
                foreach (var item in objList2)
                {
                    products3.Rows.Add(new Object[] { " ", item.ExpenseCategoryType });
                }
            }

            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = products;
            grid.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Property_ExpenseCategory_Frequency_PaymentType.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);

            var products6 = new System.Data.DataTable("teste");
            products6.Columns.Add("  ", typeof(string));
            products6.Columns.Add("    ", typeof(string));
            products6.Rows.Add(new Object[] { "  ", "   " });
            products6.Rows.Add(new Object[] { "  ", "   " });
            var grid6 = new System.Web.UI.WebControls.GridView();
            grid6.DataSource = products6;
            grid6.DataBind();
            grid6.RenderControl(htw);

            var products2 = new System.Data.DataTable("teste");
            products2.Columns.Add("  ", typeof(string));
            products2.Columns.Add("PaymentType", typeof(string));
            products2.Rows.Add(new Object[] { " ", "cash" });
            products2.Rows.Add(new Object[] { " ", "bank-deposit" });
            products2.Rows.Add(new Object[] { " ", "cheque" });
            var grid2 = new System.Web.UI.WebControls.GridView();
            grid2.DataSource = products2;
            grid2.DataBind();
            grid2.RenderControl(htw);


            var products4 = new System.Data.DataTable("teste");
            products4.Columns.Add("   ", typeof(string));
            products4.Columns.Add("  ", typeof(string));
            products4.Rows.Add(new Object[] { "  ", "   " });
            var grid4 = new System.Web.UI.WebControls.GridView();
            grid4.DataSource = products4;
            grid4.DataBind();
            grid4.RenderControl(htw);

            //products3.Rows.Add(new Object[] { " ", "electricity" });
            //products3.Rows.Add(new Object[] { " ", "entertainment" });

            var grid3 = new System.Web.UI.WebControls.GridView();
            grid3.DataSource = products3;
            grid3.DataBind();
            grid3.RenderControl(htw);


            var products7 = new System.Data.DataTable("teste");
            products7.Columns.Add("  ", typeof(string));
            products7.Columns.Add("    ", typeof(string));
            products7.Rows.Add(new Object[] { "  ", "   " });
            var grid7 = new System.Web.UI.WebControls.GridView();
            grid7.DataSource = products7;
            grid7.DataBind();
            grid7.RenderControl(htw);


            var products8 = new System.Data.DataTable("teste");
            products8.Columns.Add("FrequencyId", typeof(int));
            products8.Columns.Add("FrequencyName", typeof(string));
            products8.Rows.Add(new Object[] { "1", "Daily" });
            products8.Rows.Add(new Object[] { "2", "Weekly" });
            products8.Rows.Add(new Object[] { "3", "Monthly" });
            products8.Rows.Add(new Object[] { "4", "Quarterly" });
            products8.Rows.Add(new Object[] { "5", "Yearly" });
            var grid8 = new System.Web.UI.WebControls.GridView();
            grid8.DataSource = products8;
            grid8.DataBind();
            grid8.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index", "Expense", new { companyId = SessionHandler.CompanyId });
        }

        //public ActionResult Upload()
        //{
        //    if (!User.Identity.IsAuthenticated || SessionHandler.CompanyId == 0)
        //    {
        //        return Redirect("/");
        //    }
        //    else
        //    {
        //        ViewBag.RolePage = objIList.GetRolePageList(Convert.ToInt32(User.Identity.Name));
        //        return View();
        //    }
        //}
        [HttpGet]
        public ActionResult Upload()
        {
            if (!User.Identity.IsAuthenticated || SessionHandler.CompanyId == 0)
            {
                return Redirect("/");
            }
            else
            {
                return View();
            }
        }

        private bool rowIsNull(ExcelWorksheet worksheet, int row)
        {
            bool test = true;
            for (var i = 1; i <= 11; i++)
            {
                if (worksheet.Cells[row, i].Value != null)
                {
                    return false;
                }
            }
            return test;
        }

        //[HttpPost]
        //public ActionResult Upload(int Mergestatus)
        //{


        //    if (User.Identity.IsAuthenticated)
        //    {
        //        //it is used to check the valid account to access this page
        //        if (SessionHandler.CompanyId != 0)
        //        {
        //            if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name), Convert.ToInt32(SessionHandler.CompanyId)) == false)
        //            {
        //                return RedirectToAction("Index", "Home");
        //            }
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }

        //    //deleting the existing records into database.
        //    if (Mergestatus == 0)
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            int companyIds = SessionHandler.CompanyId;
        //            var objExpense = db.Expenses.Where(x => x.CompanyId == companyIds).ToList();
        //            foreach (var item in objExpense)
        //            {
        //                db.Expenses.Remove(item);
        //                db.SaveChanges();
        //            }
        //        }
        //    }

        //    if (Request.Files["FileUpload1"].ContentLength <= 0 || Request.Files["FileUpload1"].ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        //    {
        //        ViewBag.Error = "Invalid file type";
        //        return View(new { companyId = SessionHandler.CompanyId });
        //    }

        //    byte[] fileData = null;
        //    using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
        //    {
        //        fileData = binaryReader.ReadBytes(Request.Files[0].ContentLength);
        //    }
        //    using (MemoryStream ms = new MemoryStream(fileData))
        //    using (ExcelPackage package = new ExcelPackage(ms))
        //    using (var db = new ApplicationDbContext())
        //    {
        //        if (package.Workbook.Worksheets.Count == 0)
        //        {
        //            ViewBag.Error = "No Sheet found";
        //            return View(new { companyId = SessionHandler.CompanyId });
        //        }
        //        ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
        //        for (var i = 2; i <= 100; i++)
        //        {
        //            List<string> cells = new List<String>();
        //            for (var j = 0; j < 20; j++)
        //            {
        //                if (!String.IsNullOrEmpty(worksheet.Cells[i, j + 1].Formula))
        //                {
        //                    worksheet.Cells[i, j + 1].Calculate();
        //                    cells.Add(worksheet.Cells[i, j + 1].Value.ToString());
        //                }
        //                else
        //                {
        //                    cells.Add(Convert.ToString(worksheet.Cells[i, j + 1].Value));
        //                }
        //            }

        //            Expense expense = new Expense();
        //            if (Mergestatus != 0 && !String.IsNullOrEmpty(cells[0]))
        //            {
        //                expense = db.Expenses.Find(int.Parse(cells[0]));
        //                db.Entry(expense).State = EntityState.Modified;
        //            }
        //            if (String.IsNullOrEmpty(cells[1]))
        //            {
        //                break;
        //            }
        //            expense.Description = cells[1];
        //            expense.ExpenseCategoryId = int.Parse(cells[13]);
        //            if (!String.IsNullOrEmpty(cells[3]))
        //            {
        //                var cell = cells[3];
        //                var inquiry = db.Inquiries.Where(x => x.ConfirmationCode == cell).FirstOrDefault();
        //                if (inquiry != null)
        //                {
        //                    expense.BookingId = inquiry.Id;
        //                }
        //                else
        //                {
        //                    expense.BookingId = 0;
        //                }
        //            }
        //            else
        //            {
        //                expense.BookingId = 0;
        //            }
        //            expense.FeeTypeId = int.Parse(cells[14]);
        //            expense.PropertyId = int.Parse(cells[15]);
        //            expense.Amount = decimal.Parse(cells[6]);
        //            expense.IsPaid = bool.Parse(cells[16]);
        //            expense.PaymentMethodId = int.Parse(cells[17]);
        //            DateTime date;
        //            if (String.IsNullOrEmpty(cells[9]))
        //            {
        //                expense.DueDate = null;
        //            }
        //            else if (DateTime.TryParse(cells[9], out date))
        //            {
        //                expense.DueDate = date;
        //            }
        //            else
        //            {
        //                expense.DueDate = DateTime.FromOADate(long.Parse(cells[9]));
        //            }

        //            if (String.IsNullOrEmpty(cells[10]))
        //            {
        //                expense.PaymentDate = null;
        //            }
        //            else if (DateTime.TryParse(cells[10], out date))
        //            {
        //                expense.PaymentDate = date;
        //            }
        //            else
        //            {
        //                expense.PaymentDate = DateTime.FromOADate(long.Parse(cells[10]));
        //            }

        //            if (Mergestatus == 0 || String.IsNullOrEmpty(cells[0]))
        //            {
        //                expense.UnitId = 0;
        //                expense.PropertyOwnerId = 0;
        //                expense.PropertyManagerId = 0;
        //                expense.RenterId = 0;
        //                expense.WorkerAssignmentId = 0;
        //                expense.BillId = 0;
        //                expense.Frequency = 0;
        //                expense.DueDateDMn = 0;
        //                expense.DueDateDQt = 0;
        //                expense.DueDateDYr = 0;
        //                expense.DueDateMQt = 0;
        //                expense.DueDateMYr = 0;
        //                expense.DateCreated = DateTime.Now;
        //                expense.IsAutoCreate = false;
        //                expense.IsOwnerExpense = false;
        //                expense.IsRenterExpense = false;
        //                expense.IsPreDeducted = true;
        //                expense.CompanyId = SessionHandler.CompanyId;
        //                expense.LeaseId = 0;
        //                expense.IsStartupCost = false;
        //                expense.IsRefund = false;

        //                db.Expenses.Add(expense);
        //            }
        //        }
        //        db.SaveChanges();
        //    }

        //    ViewBag.RolePage = objIList.GetRolePageList(Convert.ToInt32(User.Identity.Name));
        //    ViewBag.Success = "Upload Success";
        //    return View(new { companyId = SessionHandler.CompanyId });


        //}

        [HttpPost]
        public ActionResult Upload(int Mergestatus)
        {


            if (User.Identity.IsAuthenticated)
            {
                //it is used to check the valid account to access this page
                if (SessionHandler.CompanyId != 0)
                {
                    if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name)) == false)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            //deleting the existing records into database.
            if (Mergestatus == 0)
            {
                using (var db = new ApplicationDbContext())
                {
                    int companyIds = SessionHandler.CompanyId;
                    var objExpense = db.Expenses.Where(x => x.CompanyId == companyIds).ToList();
                    foreach (var item in objExpense)
                    {
                        db.Expenses.Remove(item);
                        db.SaveChanges();
                    }
                }
            }

            if (Request.Files["FileUpload1"].ContentLength <= 0 || Request.Files["FileUpload1"].ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                ViewBag.Error = "Invalid file type";
                return View(new { companyId = SessionHandler.CompanyId });
            }

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
            {
                fileData = binaryReader.ReadBytes(Request.Files[0].ContentLength);
            }
            using (MemoryStream ms = new MemoryStream(fileData))
            using (ExcelPackage package = new ExcelPackage(ms))
            using (var db = new ApplicationDbContext())
            {
                if (package.Workbook.Worksheets.Count == 0)
                {
                    ViewBag.Error = "No Sheet found";
                    return View(new { companyId = SessionHandler.CompanyId });

                }
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                List<ExpensesValidation> expensesValidation = new List<ExpensesValidation>();
                bool withError = false;
                bool flag = false;
                for (var i = 2; i <= 100; i++)
                {
                    List<bool> errorValidation = new List<bool>();
                    List<string> errorMessage = new List<string>();

                    flag = true;

                    List<string> cells = new List<String>();
                    for (var j = 0; j < 20; j++)
                    {
                        if (!String.IsNullOrEmpty(worksheet.Cells[i, j + 1].Formula))
                        {
                            worksheet.Cells[i, j + 1].Calculate();
                            cells.Add(worksheet.Cells[i, j + 1].Value.ToString());
                        }
                        else
                        {
                            cells.Add(Convert.ToString(worksheet.Cells[i, j + 1].Value));
                        }
                    }

                    Expense expense = new Expense();
                    if (Mergestatus != 0 && !String.IsNullOrEmpty(cells[0]))
                    {
                        expense = db.Expenses.Find(int.Parse(cells[0]));
                        db.Entry(expense).State = EntityState.Modified;
                    }
                    if (String.IsNullOrEmpty(cells[1]))
                    {
                        break;
                    }

                    // description
                    string description = "";
                    if (string.IsNullOrEmpty(cells[1]))
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Description is empty.");
                        flag = false;
                    }
                    else
                    {
                        expense.Description = cells[1];
                        description = expense.Description;
                        errorValidation.Add(true);
                        errorMessage.Add("valid field.");
                    }
                    // expense category id
                    int expenseCategoryId = 0;
                    bool isIntExpenseCategory = int.TryParse(cells[13], out expenseCategoryId);
                    if (!isIntExpenseCategory)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid field's value, it should be integer.");
                        expense.ExpenseCategoryId = cells[13].ToInt();
                        flag = false;
                    }
                    else
                    {
                        if (objIList.IsExpenseCategoryExist(expenseCategoryId))
                        {
                            errorValidation.Add(true);
                            errorMessage.Add("valid field.");
                            expense.ExpenseCategoryId = int.Parse(cells[13]);
                        }
                        else
                        {
                            errorValidation.Add(false);
                            errorMessage.Add("Invalid field's value, it does not belong to any expense category in the database.");
                            expense.ExpenseCategoryId = cells[13].ToInt();
                            flag = false;
                        }
                    }


                    if (!String.IsNullOrEmpty(cells[3]))
                    {
                        var cell = cells[3];
                        var inquiry = db.Inquiries.Where(x => x.ConfirmationCode == cell).FirstOrDefault();
                        if (inquiry != null)
                        {
                            expense.BookingId = inquiry.Id;
                            errorValidation.Add(true);
                            errorMessage.Add("valid field.");
                        }
                        else
                        {
                            expense.BookingId = 0;
                            errorValidation.Add(false);
                            errorMessage.Add("Invalid Confirmation Code");
                            flag = false;

                        }
                    }
                    else
                    {
                        expense.BookingId = 0;
                        errorValidation.Add(true);
                        errorMessage.Add("valid field");

                    }

                    // fee type id
                    int feeTypeId = 0;
                    bool IsIntBookingId = int.TryParse(cells[14], out feeTypeId);
                    if (!IsIntBookingId)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid field's value, it should be integer.");
                        flag = false;
                    }
                    else
                    {
                        if (expense.BookingId == 0 && feeTypeId != 0)
                        {
                            errorValidation.Add(false);
                            errorMessage.Add("Invalid field's value, fee type should only have value if there is a booking involved.");
                            flag = false;
                        }
                        else if (expense.BookingId == 0 && feeTypeId == 0)
                        {
                            errorValidation.Add(true);
                            errorMessage.Add("valid field.");
                            expense.FeeTypeId = int.Parse(cells[14]);
                        }
                        else
                        {
                            if (feeTypeId == 0)
                            {
                                errorValidation.Add(false);
                                errorMessage.Add("Invalid field's value, please enter a valid fee type id.");
                                flag = false;
                            }
                            else
                            {
                                if (objIList.IsFeeTypeExist(feeTypeId))
                                {
                                    errorValidation.Add(true);
                                    errorMessage.Add("valid field.");
                                    expense.FeeTypeId = int.Parse(cells[14]);
                                }
                                else
                                {
                                    errorValidation.Add(false);
                                    errorMessage.Add("Invalid field's value, it does not belong to any fee type in the database.");
                                    flag = false;
                                }
                            }
                        }
                    }


                    // property id
                    int propertyId;
                    bool isPropertyId = int.TryParse(cells[15], out propertyId);
                    if (!isPropertyId)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid data type, it should be integer.");
                        flag = false;
                    }
                    else
                    {
                        if (objIList.IsPropertyExist(propertyId))
                        {
                            errorValidation.Add(true);
                            errorMessage.Add("valid field.");
                            expense.PropertyId = int.Parse(cells[15]);

                        }
                        else
                        {
                            errorValidation.Add(false);
                            errorMessage.Add("Invalid field's value, it does not belong to any property in the database.");
                            flag = false;
                        }
                    }

                    decimal amount;
                    bool isValidAmount = decimal.TryParse((cells[6]).ToString(), out amount);
                    if (!isValidAmount)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid data type, it should be decimal.");
                        flag = false;
                    }
                    else
                    {
                        expense.Amount = decimal.Parse(cells[6]);
                        errorValidation.Add(true);
                        errorMessage.Add("valid field.");

                    }


                    // is paid
                    bool isPaid;
                    bool isValidIsPaid = bool.TryParse(cells[16], out isPaid);
                    if (!isValidIsPaid)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid datatype, it should be boolean(New or Paid) same as demo record.");
                        flag = false;
                    }
                    else
                    {
                        errorValidation.Add(true);
                        errorMessage.Add("valid field.");
                        expense.IsPaid = bool.Parse(cells[16]);
                    }


                    // payment method id
                    int paymentMethodId = 0;
                    bool isValidPaymentMethodId = int.TryParse(cells[17], out paymentMethodId);
                    if (!isValidPaymentMethodId)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid data type, it should be integer.");
                        flag = false;
                    }
                    else
                    {
                        if (objIList.IsPaymentMethodExist(paymentMethodId))
                        {
                            errorValidation.Add(true);
                            errorMessage.Add("valid field.");
                            expense.PaymentMethodId = int.Parse(cells[17]);
                        }
                        else
                        {
                            errorValidation.Add(false);
                            errorMessage.Add("Invalid field's value, it does not belong to any payment method in the database.");
                            flag = false;
                        }
                    }
                    //DateTime date;
                    //if (String.IsNullOrEmpty(cells[9]))
                    //{
                    //    expense.DueDate = null;
                    //}
                    //else if (DateTime.TryParse(cells[9], out date))
                    //{
                    //    expense.DueDate = date;
                    //}
                    //else
                    //{
                    //    expense.DueDate = DateTime.FromOADate(long.Parse(cells[9]));
                    //}

                    // due date
                    DateTime dueDate;
                    bool isDueDate = DateTime.TryParse(cells[9], out dueDate);
                    if (!isDueDate)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Correct date is required");
                        flag = false;
                    }
                    else
                    {
                        errorValidation.Add(true);
                        errorMessage.Add("valid field.");
                        expense.DueDate = cells[9].ToDateTime();
                    }





                    //if (String.IsNullOrEmpty(cells[10]))
                    //{
                    //    expense.PaymentDate = null;
                    //}
                    //else if (DateTime.TryParse(cells[10], out dueDate))
                    //{
                    //    expense.PaymentDate = dueDate;
                    //    errorValidation.Add(true);
                    //    errorMessage.Add("valid field.");
                    //}
                    //else
                    //{
                    //    expense.PaymentDate = DateTime.FromOADate(long.Parse(cells[10]));
                    //}

                    // payment date
                    if (String.IsNullOrEmpty(cells[10]) && !isPaid)
                    {
                        errorValidation.Add(true);
                        errorMessage.Add("valid field.");
                    }
                    else
                    {
                        DateTime datePayment;
                        bool isDatePayment = DateTime.TryParse(cells[10], out datePayment);
                        if (!isDatePayment)
                        {
                            errorValidation.Add(false);
                            errorMessage.Add("Correct date is required");
                            flag = false;
                        }
                        else
                        {
                            errorValidation.Add(true);
                            errorMessage.Add("valid field.");
                            //expense.PaymentDate = cells[10].ToDateTime();
                        }
                    }
                    // company id
                    bool isCompanyID = SessionHandler.CompanyId == 0 ? false : true;
                    if (!isCompanyID)
                    {
                        errorValidation.Add(false);
                        errorMessage.Add("Invalid datatype, company id is required");
                        flag = false;
                    }
                    else
                    {
                        errorValidation.Add(true);
                        errorMessage.Add("valid field.");
                    }


                    if (Mergestatus == 0 || String.IsNullOrEmpty(cells[0]))
                    {
                        expense.UnitId = 0;
                        expense.PropertyOwnerId = 0;
                        expense.PropertyManagerId = 0;
                        expense.RenterId = 0;
                        expense.WorkerAssignmentId = 0;
                        expense.BillId = 0;
                        expense.Frequency = 0;
                        expense.DueDateDMn = 0;
                        expense.DueDateDQt = 0;
                        expense.DueDateDYr = 0;
                        expense.DueDateMQt = 0;
                        expense.DueDateMYr = 0;
                        expense.DateCreated = DateTime.Now;
                        expense.IsAutoCreate = false;
                        expense.IsOwnerExpense = false;
                        expense.IsRenterExpense = false;
                        expense.IsPreDeducted = true;
                        expense.CompanyId = SessionHandler.CompanyId;
                        expense.LeaseId = 0;
                        expense.IsStartupCost = false;
                        expense.IsRefund = false;
                        db.Expenses.Add(expense);
                    }

                    if (flag != false)
                    {
                        db.SaveChanges();
                    }
                    else
                    {
                        //strRows = (strRows + "," + i.ToString());
                        ExpensesValidation modelValidation = new ExpensesValidation
                        {
                            Description = cells[1],
                            ExpenseCategoryId = cells[2],
                            ConfirmationCode = cells[3],
                            FeeTypeId = cells[4],
                            PropertyId = cells[5],
                            Amount = cells[6],
                            IsPaid = cells[7],
                            PaymentMethodId = cells[8],
                            DueDate = cells[9],//Convert.ToDateTime(cells[9]).ToString("MMM dd, yyyy"),
                            DatePayment = cells[10].ToString(),//Convert.ToDateTime(cells[10]).ToString("MMM dd, yyyy"),
                            ValidErrorStatus = errorValidation,
                            ValidErrorMessage = errorMessage
                        };
                        expensesValidation.Add(modelValidation);
                        withError = true;

                    }

                }
                if (withError == false)
                {
                    ViewBag.Success = "Upload Success";
                    return RedirectToAction("Index", "Expense", new { companyId = SessionHandler.CompanyId });
                }
                else
                {
                    ViewBag.ErrorList = expensesValidation;
                    return View(new { companyId = SessionHandler.CompanyId });
                }

            }
        }


        // JM 8/13/18 Old code of Excel uploder
        //[HttpPost]
        //public ActionResult Upload(int Mergestatus)
        //{
        //    string strRows = "";
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        //it is used to check the valid account to access this page
        //        if (SessionHandler.CompanyId != 0)
        //        {
        //            if (objIList.CheckValidRequest(Convert.ToInt32(User.Identity.Name), Convert.ToInt32(SessionHandler.CompanyId)) == false)
        //            {
        //                return RedirectToAction("Index", "Home");
        //            }
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    if (Request.Files["FileUpload1"].ContentLength > 0)
        //    {
        //        string extension = System.IO.Path.GetExtension(Request.Files["FileUpload1"].FileName).ToLower();
        //        string connString = "";
        //        string[] validFileTypes = { ".xls", ".xlsx", ".csv" };
        //        string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/Uploads"), Request.Files["FileUpload1"].FileName);
        //        if (!Directory.Exists(path1))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/Content/Uploads"));
        //        }
        //        strRows = "";

        //        //deleting the existing records into database.
        //        if (Mergestatus == 0)
        //        {
        //            using (var db = new ApplicationDbContext())
        //            {
        //                int companyIds = SessionHandler.CompanyId;
        //                var objExpense = db.Expenses.Where(x => x.CompanyId == companyIds).ToList();
        //                foreach (var item in objExpense)
        //                {
        //                    db.Expenses.Remove(item);
        //                    db.SaveChanges();
        //                }
        //            }
        //        }


        //        List<bool> errorValidationFinal = new List<bool>();
        //        List<string> errorMessageFinal = new List<string>();
        //        DataTable dt = new DataTable();
        //        List<ExpensesValidation> expensesValidation = new List<ExpensesValidation>();
        //        if (validFileTypes.Contains(extension))
        //        {
        //            if (System.IO.File.Exists(path1))
        //            { System.IO.File.Delete(path1); }
        //            Request.Files["FileUpload1"].SaveAs(path1);
        //            if (extension == ".csv")
        //            {
        //                dt = ReadExcelFile.ConvertCSVtoDataTable(path1);
        //            }
        //            else if (extension.Trim() == ".xls")
        //            {
        //                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path1 + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
        //                dt = ReadExcelFile.ConvertXSLXtoDataTable(path1, connString);
        //            }
        //            else if (extension.Trim() == ".xlsx")
        //            {
        //                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
        //                dt = ReadExcelFile.ConvertXSLXtoDataTable(path1, connString);

        //                int i = 0;
        //                bool flag = true;
        //                foreach (DataRow drow in dt.Rows)
        //                {
        //                    List<bool> errorValidation = new List<bool>();
        //                    List<string> errorMessage = new List<string>();
        //                    flag = true;

        //                    i = (i + 1);

        //                    if (!string.IsNullOrEmpty(drow["Description"].ToString()))
        //                    {
        //                        // description
        //                        string description = "";
        //                        if (string.IsNullOrEmpty(drow["Description"].ToString()))
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Description is empty.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            description = drow["Description"].ToString();
        //                            errorValidation.Add(true);
        //                            errorMessage.Add("valid field.");
        //                        }
        //                        // expense category id
        //                        int expenseCategoryId = 0;
        //                        bool isIntExpenseCategory = int.TryParse(drow["ExpenseCategoryId"].ToString(), out expenseCategoryId);
        //                        if (!isIntExpenseCategory)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid field's value, it should be integer.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            if (objIList.IsExpenseCategoryExist(expenseCategoryId))
        //                            {
        //                                errorValidation.Add(true);
        //                                errorMessage.Add("valid field.");
        //                            }
        //                            else
        //                            {
        //                                errorValidation.Add(false);
        //                                errorMessage.Add("Invalid field's value, it does not belong to any expense category in the database.");
        //                                flag = false;
        //                            }
        //                        }
        //                        // booking idf
        //                        int bookingId = 0;
        //                        bool isIntBookingId = int.TryParse(drow["BookingId"].ToString(), out bookingId);
        //                        if (!isIntBookingId)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid field's value, it should be integer.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            if (bookingId == 0)
        //                            {
        //                                errorValidation.Add(true);
        //                                errorMessage.Add("valid field.");
        //                            }
        //                            else
        //                            {
        //                                if (objIList.IsBookingExist(expenseCategoryId))
        //                                {
        //                                    errorValidation.Add(true);
        //                                    errorMessage.Add("valid field.");
        //                                }
        //                                else
        //                                {
        //                                    errorValidation.Add(false);
        //                                    errorMessage.Add("Invalid field's value, it does not belong to any booking in the database.");
        //                                    flag = false;
        //                                }
        //                            }
        //                        }

        //                        // fee type id
        //                        int feeTypeId = 0;
        //                        bool IsIntBookingId = int.TryParse(drow["FeeTypeId"].ToString(), out feeTypeId);
        //                        if (!IsIntBookingId)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid field's value, it should be integer.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            if (bookingId == 0 && feeTypeId != 0)
        //                            {
        //                                errorValidation.Add(false);
        //                                errorMessage.Add("Invalid field's value, fee type should only have value if there is a booking involved.");
        //                                flag = false;
        //                            }
        //                            else if (bookingId == 0 && feeTypeId == 0)
        //                            {
        //                                errorValidation.Add(true);
        //                                errorMessage.Add("valid field.");
        //                            }
        //                            else
        //                            {
        //                                if (feeTypeId == 0)
        //                                {
        //                                    errorValidation.Add(false);
        //                                    errorMessage.Add("Invalid field's value, please enter a valid fee type id.");
        //                                    flag = false;
        //                                }
        //                                else
        //                                {
        //                                    if (objIList.IsFeeTypeExist(feeTypeId))
        //                                    {
        //                                        errorValidation.Add(true);
        //                                        errorMessage.Add("valid field.");
        //                                    }
        //                                    else
        //                                    {
        //                                        errorValidation.Add(false);
        //                                        errorMessage.Add("Invalid field's value, it does not belong to any fee type in the database.");
        //                                        flag = false;
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        // property id
        //                        int propertyId;
        //                        bool isPropertyId = int.TryParse(drow["PropertyId"].ToString(), out propertyId);
        //                        if (!isPropertyId)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid data type, it should be integer.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            if (objIList.IsPropertyExist(propertyId))
        //                            {
        //                                errorValidation.Add(true);
        //                                errorMessage.Add("valid field.");
        //                            }
        //                            else
        //                            {
        //                                errorValidation.Add(false);
        //                                errorMessage.Add("Invalid field's value, it does not belong to any property in the database.");
        //                                flag = false;
        //                            }
        //                        }

        //                        // amount
        //                        decimal amount;
        //                        bool isValidAmount = decimal.TryParse(drow["Amount"].ToString(), out amount);
        //                        if (!isValidAmount)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid data type, it should be decimal.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            errorValidation.Add(true);
        //                            errorMessage.Add("valid field.");
        //                        }

        //                        // is paid
        //                        bool isPaid;
        //                        bool isValidIsPaid = bool.TryParse(drow["IsPaid"].ToString(), out isPaid);
        //                        if (!isValidIsPaid)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid datatype, it should be boolean(0 or 1) same as demo record.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            errorValidation.Add(true);
        //                            errorMessage.Add("valid field.");
        //                        }

        //                        // payment method id
        //                        int paymentMethodId = 0;
        //                        bool isValidPaymentMethodId = int.TryParse(drow["PaymentMethodId"].ToString(), out paymentMethodId);
        //                        if (!isValidPaymentMethodId)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid data type, it should be integer.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            if (objIList.IsPaymentMethodExist(paymentMethodId))
        //                            {
        //                                errorValidation.Add(true);
        //                                errorMessage.Add("valid field.");
        //                            }
        //                            else
        //                            {
        //                                errorValidation.Add(false);
        //                                errorMessage.Add("Invalid field's value, it does not belong to any payment method in the database.");
        //                                flag = false;
        //                            }
        //                        }

        //                        // due date
        //                        DateTime dueDate;
        //                        bool isDueDate = DateTime.TryParse(drow["DueDate"].ToString(), out dueDate);
        //                        if (!isDueDate)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid datatype, it should be datetime same as demo record.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            errorValidation.Add(true);
        //                            errorMessage.Add("valid field.");
        //                        }

        //                        // payment date
        //                        DateTime datePayment;
        //                        bool isDatePayment = DateTime.TryParse(drow["DatePayment"].ToString(), out datePayment);
        //                        if (!isDatePayment)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid datatype, it should be datetime same as demo record.");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            errorValidation.Add(true);
        //                            errorMessage.Add("valid field.");
        //                        }

        //                        // company id
        //                        bool isCompanyID = SessionHandler.CompanyId == 0 ? false : true;
        //                        if (!isCompanyID)
        //                        {
        //                            errorValidation.Add(false);
        //                            errorMessage.Add("Invalid datatype, company id is required");
        //                            flag = false;
        //                        }
        //                        else
        //                        {
        //                            errorValidation.Add(true);
        //                            errorMessage.Add("valid field.");
        //                        }

        //                        Expense model = new Expense
        //                        {
        //                            Amount = amount,
        //                            Description = description,
        //                            ExpenseCategoryId = expenseCategoryId,
        //                            PropertyId = propertyId,
        //                            DueDate = dueDate,
        //                            PaymentDate = datePayment,
        //                            IsCapitalExpense = false,
        //                            IsStartupCost = false,
        //                            Frequency = 0,
        //                            DateLastJobRun = null,
        //                            DateNextJobRun = null,
        //                            DateCreated = DateTime.Now,
        //                            PaymentMethodId = paymentMethodId,
        //                            CompanyId = SessionHandler.CompanyId,
        //                            IsPaid = isPaid
        //                        };
        //                        if (flag != false)
        //                        {
        //                            objIList.InsertExpense(model);
        //                        }
        //                        else
        //                        {
        //                            strRows = (strRows + "," + i.ToString());
        //                            ExpensesValidation modelValidation = new ExpensesValidation
        //                            {
        //                                Description = drow["Description"].ToString(),
        //                                ExpenseCategoryId = drow["ExpenseCategoryId"].ToString(),
        //                                BookingId = drow["BookingId"].ToString(),
        //                                FeeTypeId = drow["FeeTypeId"].ToString(),
        //                                PropertyId = drow["PropertyId"].ToString(),
        //                                Amount = drow["Amount"].ToString(),
        //                                IsPaid = drow["IsPaid"].ToString(),
        //                                PaymentMethodId = drow["PaymentMethodId"].ToString(),
        //                                DueDate = drow["DueDate"].ToString(),
        //                                DatePayment = drow["DatePayment"].ToString(),
        //                                ValidErrorStatus = errorValidation,
        //                                ValidErrorMessage = errorMessage
        //                            };
        //                            expensesValidation.Add(modelValidation);

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ViewBag.Error = "Please Upload Files in .xls, .xlsx or .csv format";
        //        }
        //        //setting data for Error Grid.
        //        if (strRows != "")
        //        {
        //            ViewBag.Error = "System did not upload the following rows into system due to some of kind issues like (not valid datatype), (not valid relation id(s)), (not enter valid id(s) for login company). You can also see the error on hover of cells which are red.  Rows:- " + strRows;
        //        }
        //        ViewBag.ErrorList = expensesValidation;
        //        if (User.Identity.IsAuthenticated != false)
        //        {
        //            ViewBag.RolePage = objIList.GetRolePageList(Convert.ToInt32(User.Identity.Name));
        //        }
        //    }


        //    if (strRows != "")
        //    {
        //        return View(new { companyId = SessionHandler.CompanyId });
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Expense", new { companyId = SessionHandler.CompanyId });
        //    }
        //}

        public ActionResult DeletePayment(int id)
        {
            try
            {

                using (var db = new ApplicationDbContext())
                {
                    var payment = db.ExpensePayment.Find(id);
                    db.ExpensePayment.Remove(payment);
                    db.SaveChanges();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult EditPayment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var payment = db.ExpensePayment.Find(id);
                var Files = db.ExpensePaymentFiles.Where(x => x.PaymentId == id).ToList();

                return Json(new { success = true, payment = payment, filesUrl = Files }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetExpenseDetails(int Id)
        {

            using (var db = new ApplicationDbContext())
            {
                var expense = db.Expenses.Find(Id);
                var expensePaid = db.ExpensePayment.Where(x => x.ExpenseId == Id).ToList();



                return Json(new { success = true, expense = expense, expense_paid = expensePaid, message = "Expense Details Load" }, JsonRequestBehavior.AllowGet);

            }


        }
        [HttpPost]
        public ActionResult RecordPayment(int Id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    decimal total = 0;
                    var expense = db.Expenses.Find(Id);
                    decimal totalPaid = 0;
                    if (expense != null)
                    {
                        total = expense.Amount.Value;
                        var expensesPaid = db.ExpensePayment.Where(x => x.ExpenseId == Id).ToList();
                        expensesPaid.ForEach(x =>
                        {
                            totalPaid += x.Amount;
                        });

                        if (totalPaid == expense.Amount)
                        {
                            return Json(new { success = true, total_paid = totalPaid, expense_amount = expense.Amount, message = "Payment Status Updated" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (totalPaid > expense.Amount)
                        {
                            return Json(new { success = false, total_paid = totalPaid, expense_amount = expense.Amount, message = "Payment more than expense" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    return Json(new { success = true, message = "Data has been process" }, JsonRequestBehavior.AllowGet);





                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                return Json(new { success = false, message = "Unable to Update" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult AddExpensePayment(int ExpenseId, string Description, decimal Amount, int Type, string PaymentDate, HttpPostedFileWrapper[] Images)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    decimal total = 0.0M;
                    var expense = db.Expenses.Find(ExpenseId);
                    decimal totalPaid = 0;

                    if (expense != null)
                    {
                        total = expense.Amount.Value;
                        var expensesPaid = db.ExpensePayment.Where(x => x.ExpenseId == ExpenseId).ToList();
                        expensesPaid.ForEach(x =>
                        {
                            totalPaid += x.Amount;
                        });
                        //if(Amount <= expense.Amount && totalPaid <= expense.Amount)
                        if (Amount + totalPaid <= total)
                        {
                            ExpensePayment e = new ExpensePayment();
                            e.ExpenseId = ExpenseId;
                            e.Description = Description;
                            e.Amount = Amount;
                            e.Type = Type;
                            e.CreatedAt = PaymentDate.ToDateTime();
                            db.ExpensePayment.Add(e);
                            db.SaveChanges();
                            if (Images != null)
                            {
                                foreach (var image in Images)
                                {
                                    db.ExpensePaymentFiles.Add(new ExpensePaymentFile() { PaymentId = e.Id, FileUrl = UploadImage(image, "~/Images/ExpensePayments/") });
                                    db.SaveChanges();
                                }
                            }

                            return Json(new { success = true, total_paid = totalPaid, expense_amount = expense.Amount, message = "Payment Status Added" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, total_paid = totalPaid, expense_amount = expense.Amount, message = "Payment more than expense" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    return Json(new { success = true, message = "Data has been processed" }, JsonRequestBehavior.AllowGet);



                }

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                return Json(new { success = false, message = "Expense Payment Add Failed" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult EditExpensePayment(int ExpenseId, int paymentId, string Description, decimal Amount, int Type, string paymentDate, HttpPostedFileWrapper[] Images, string fileIds)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    decimal total = 0;
                    var expense = db.Expenses.Find(ExpenseId);
                    decimal totalPaid = 0;

                    if (expense != null)
                    {

                        total = expense.Amount.Value;
                        var expensesPaid = db.ExpensePayment.Where(x => x.ExpenseId == ExpenseId && x.Id != paymentId).ToList();
                        expensesPaid.ForEach(x =>
                        {
                            totalPaid += x.Amount;
                        });
                        //if(Amount <= expense.Amount && totalPaid <= expense.Amount)
                        if (Amount + totalPaid <= total)
                        {
                            var payment = db.ExpensePayment.Find(paymentId);
                            payment.Description = Description;
                            payment.Amount = Amount;
                            payment.Type = Type;
                            payment.CreatedAt = paymentDate.ToDateTime();
                            db.SaveChanges();
                            var files = db.ExpensePaymentFiles.Where(x => x.PaymentId == expense.Id).ToList();
                            var Ids = JsonConvert.DeserializeObject<List<int>>(fileIds);
                            files = files.Where(x => !Ids.Contains(x.Id)).ToList();
                            db.ExpensePaymentFiles.RemoveRange(files);
                            db.SaveChanges();
                            if (Images != null)
                            {
                                foreach (var image in Images)
                                {
                                    db.ExpensePaymentFiles.Add(new ExpensePaymentFile() { PaymentId = payment.Id, FileUrl = UploadImage(image, "~/Images/ExpensePayments/") });
                                    db.SaveChanges();
                                }
                            }

                            return Json(new { success = true, total_paid = totalPaid, expense_amount = expense.Amount, message = "Payment Status Added" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, total_paid = totalPaid, expense_amount = expense.Amount, message = "Payment more than expense" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    return Json(new { success = true, message = "Data has been processed" }, JsonRequestBehavior.AllowGet);



                }

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                return Json(new { success = false, message = "Expense Payment Add Failed" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult GetPaymentMethodList()
        {
            using (var db = new ApplicationDbContext())
            {
                var paymentMethods = new List<PaymentMethodViewModel>();
                var data = db.PaymentMethods.Where(x => x.CompanyId == SessionHandler.CompanyId || x.Type == 0).ToList();
                foreach (var item in data)
                {
                    paymentMethods.Add(new PaymentMethodViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Type = item.Type == 1 ? "Private" : item.Type == 0 ? "Public" : "",
                        Actions = (item.Type == 1 ? "<a class=\"ui mini button edit-payment-method\" title=\"Edit\">" +
                                        "<i class=\"icon edit\"></i>" +
                                        "</a>" +
                                        "<a class=\"ui mini button delete-payment-method\" title=\"Delete\">" +
                                            "<i class=\"icon ban\"></i>" +
                                        "</a>" : "")
                    });
                }
                return Json(new { success = true, data = paymentMethods }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult GetPaymentMethod(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var paymentMethod = db.PaymentMethods.Where(x => x.Id == id).FirstOrDefault();
                return Json(new { success = true, paymentMethod }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CreatePaymentMethod(int id, string name)
        {
            using (var db = new ApplicationDbContext())
            {

                if (id == 0)
                {
                    db.PaymentMethods.Add(new PaymentMethod() { Id = id, Name = name, CompanyId = SessionHandler.CompanyId, Type = 1 });
                    db.SaveChanges();
                }
                else
                {
                    var temp = db.PaymentMethods.Where(x => x.Id == id).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Name = name;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeletePaymentMethod(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.PaymentMethods.Where(x => x.Id == id).FirstOrDefault();
                db.PaymentMethods.Remove(temp);
                db.SaveChanges();

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAirbnbExpenseStatus()
        {
            if (User.Identity.IsAuthenticated)
            {
                int companyId = SessionHandler.CompanyId;

                //SiteConstants.accounts.ToList().ForEach(item =>
                //{
                //    Task task = Task.Factory.StartNew(() => UpdateExpenseFromAirbnb(item, companyId)).ContinueWith(x => { if (x.IsCompleted) x.Dispose(); });
                //});
                return Json(new { success = true, message = "Operation Successful" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, message = "Unauthenticated User" }, JsonRequestBehavior.AllowGet);
        }
    }
}
