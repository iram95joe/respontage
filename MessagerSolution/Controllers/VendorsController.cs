﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MessagerSolution.Controllers
{
    public class VendorsController : Controller
    {
        [HttpGet]
        public ActionResult GetVendors()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = db.Vendors.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
                foreach(var vendor in list)
                {
                    var contacts = db.VendorContactNumbers.Where(x => x.VendorId == vendor.Id).ToList();
                    foreach (var contact in contacts)
                    {
                        vendor.ContactNumbers += contact.Value+ "<br>";
                    }
                    var emails = db.VendorEmails.Where(x => x.VendorId == vendor.Id).ToList();
                    foreach (var email in emails)
                    {
                        vendor.Emails += email.Email+ "<br>";
                    }
                    vendor.Actions = "<a class=\"ui mini button edit-vendor\" title=\"Edit\">" +
                                          "<i class=\"icon edit\"></i>" +
                                          "</a>";
                }
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetVendorDetails(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var vendor = db.Vendors.Where(x => x.Id == id).FirstOrDefault();
                var contacts = db.VendorContactNumbers.Where(x => x.VendorId == vendor.Id).ToList();
                var emails = db.VendorEmails.Where(x => x.VendorId == vendor.Id).ToList();

                return Json(new {success=true, vendor,contacts,emails }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Create(string Name, string Address, string ContactString, string EmailString)
        {
            using (var db = new ApplicationDbContext())
            {
                var vendor = new Vendor() { Name = Name, Address = Address, CompanyId = SessionHandler.CompanyId };
                db.Vendors.Add(vendor);
                db.SaveChanges();
                var js = new JavaScriptSerializer();
                var contactNumbers = js.Deserialize<List<VendorContactNumber>>(ContactString);
                var emails = js.Deserialize<List<VendorEmail>>(EmailString);
                foreach (var contact in contactNumbers)
                {
                    contact.VendorId = vendor.Id;
                    db.VendorContactNumbers.Add(contact);
                    db.SaveChanges();
                }
                foreach (var email in emails)
                {
                    email.VendorId = vendor.Id;
                    db.VendorEmails.Add(email);
                    db.SaveChanges();
                }
                var thread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && x.VendorId ==vendor.Id).FirstOrDefault();
                if (thread == null)
                {

                    CommunicationThread workerThread = new CommunicationThread();
                    db.CommunicationThreads.Add(workerThread);
                    workerThread.VendorId = vendor.Id;
                    workerThread.ThreadId = PasswordGenerator.Generate(20);
                    workerThread.CompanyId = SessionHandler.CompanyId;
                    db.CommunicationThreads.Add(workerThread);
                    db.SaveChanges();

                }
                return Json(new { success = true, vendorId = vendor.Id }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Edit(int Id,string Name, string Address, string ContactString, string EmailString)
        {
            using (var db = new ApplicationDbContext())
            {
                var vendor = db.Vendors.Where(x => x.Id == Id).FirstOrDefault();
                if (vendor != null)
                {
                    vendor.Name = Name;
                    vendor.Address = vendor.Address;
                    db.SaveChanges();
                    var js = new JavaScriptSerializer();
                    var contactNumbers = js.Deserialize<List<VendorContactNumber>>(ContactString);
                    var emails = js.Deserialize<List<VendorEmail>>(EmailString);
                    //JM 10/23/19 Get all the Contact number of vendor
                    var removedContactId = db.VendorContactNumbers.Where(x => x.VendorId == vendor.Id).Select(x => x.Id).ToList();
                    foreach (var contact in contactNumbers)
                    {//Remove id that doesn`t deleted
                        removedContactId.Remove(contact.Id);
                           var temp = db.VendorContactNumbers.Where(x => x.Id == contact.Id).FirstOrDefault();
                        if (temp ==null)
                        {
                            contact.VendorId = vendor.Id;
                            db.VendorContactNumbers.Add(contact);
                            db.SaveChanges();
                        }
                        else
                        {
                            temp.Value = contact.Value;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    //Remove all data in removed list
                    foreach (var removeId in removedContactId)
                    {
                        db.VendorContactNumbers.Remove(db.VendorContactNumbers.Where(x => x.Id == removeId).First());
                        db.SaveChanges();
                    }
                    //JM 10/23/19 Get all the Email of vendor
                    var removedEmailId = db.VendorEmails.Where(x => x.VendorId == vendor.Id).Select(x => x.Id).ToList();
                    foreach (var email in emails)
                    {//Remove id that doesn`t deleted
                        removedEmailId.Remove(email.Id);
                        var temp = db.VendorEmails.Where(x => x.Id == email.Id).FirstOrDefault();
                        if (temp == null)
                        {
                            email.VendorId = vendor.Id;
                            db.VendorEmails.Add(email);
                            db.SaveChanges();
                        }
                        else
                        {
                            temp.Email = email.Email;
                            db.Entry(temp).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    //Remove all data in removed list
                    foreach (var removeId in removedEmailId)
                    {
                        db.VendorEmails.Remove(db.VendorEmails.Where(x=>x.Id==removeId).First());
                        db.SaveChanges();
                    }
                }
                   
                return Json(new { success = true, vendorId = vendor.Id }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}