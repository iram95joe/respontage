﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.Location
{
    public class Address
    {
        public static string Country(int countryId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Countries.Where(x => x.Id == countryId).FirstOrDefault().Name;
                }
            }
            catch{ return ""; }
        }
        public static string State(int stateId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.States.Where(x => x.Id == stateId).FirstOrDefault().Name;

                }
            }
            catch { return ""; }
        }
        public static string City(int cityId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Cities.Where(x => x.Id == cityId).FirstOrDefault().Name;
                }
            }
            catch { return ""; }
        }
    }
}