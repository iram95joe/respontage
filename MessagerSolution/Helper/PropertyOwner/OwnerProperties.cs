﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.PropertyOwner
{
    public class OwnerProperties
    {
        //public static bool IsOwner(int ownerId)
        //{
        //    using(var db = new ApplicationDbContext())
        //    {
        //        var role =db.tbRoleLink.Where(x => x.UserId == ownerId && x.RoleId == 5).FirstOrDefault();
        //        if (role != null)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //}
        //public static List<Property> GetProperties(int ownerId)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var properties = new List<Property>();
        //       var ownerProperties=(from property in db.Properties join ownerproperty in db.OwnerProperties on property.Id equals ownerproperty.PropertyId where ownerproperty.OwnerId == ownerId select property).ToList();
        //        foreach (var p in ownerProperties)
        //        {
        //            var parent = db.ParentChildProperties.Where(x => x.ChildPropertyId == p.Id && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
        //            if (p.IsParentProperty)
        //            {//JM 7/13/2020 Get all the child properties of child property of parent example is when Account parent has Multi unit child properties we need to give owner access to child of multi unit
        //                properties.Add(p);
        //                var childs = (from property in db.Properties join parentChild in db.ParentChildProperties on property.Id equals parentChild.ChildPropertyId where parentChild.ParentPropertyId == p.Id select property).ToList();
        //                foreach (var child in childs)
        //                {
        //                    if (child.IsParentProperty)
        //                    {
        //                        if (child.ParentType != (int)Core.Enumerations.ParentPropertyType.Sync)
        //                            properties.Add(child);

        //                        properties.AddRange((from property in db.Properties join parentChild in db.ParentChildProperties on property.Id equals parentChild.ChildPropertyId where parentChild.ParentPropertyId == child.Id select property).ToList());
        //                    }
        //                    else
        //                    {
        //                        properties.Add(child);
        //                    }
        //                }
        //            }
        //            if (parent != null)
        //            {
        //                properties.Add(db.Properties.Where(x => x.Id == parent.ParentPropertyId).FirstOrDefault());
        //                //JM 7/13/2020 When Airbnb,Vrbo,Homeaway child assign to owner we need to give access to its sync parent and sibling
        //                var childIds = db.ParentChildProperties.Where(x => x.ParentPropertyId == parent.ParentPropertyId).Select(x => x.ChildPropertyId).ToList();
        //                properties.AddRange(db.Properties.Where(x => childIds.Contains(x.Id)).ToList());
        //            }
        //            //else
        //            //{//JM 7/13/2020 Alone property
        //            //    properties.Add(p);
        //            //}
        //        }
        //        foreach(var p in properties)
        //        {
        //            p.Name = p.Name + (p.IsParentProperty ? "(Parent)" : "("+GetSiteType(p.SiteType)+")");
        //        }
        //        return properties;
        //    }
        //}
        private static string GetSiteType(int siteType)
        {
            switch (siteType)
            {
                case 1: return "Airbnb";
                case 2: return "Vrbo";
                case 3: return "Booking.com";
                case 4: return "Homeaway";
                default: return "Local";
            }
        }
    }
}