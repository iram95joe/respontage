﻿using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.ImagesCompress
{
    public class Files
    {
        public static string Upload(HttpPostedFileBase file, string Destination)
        {
            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(Destination));
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(System.Web.HttpContext.Current.Server.MapPath(url));
            url = Destination + CompressImage(System.Web.HttpContext.Current.Server.MapPath(url), 30, System.Web.HttpContext.Current.Server.MapPath(Destination));
            return url.Replace("~", "");

        }
        public static string CompressImage(string InputImage, int Quality, string OutPutDirectory)
        {
            string file = System.IO.Path.GetFileName(InputImage);
            try
            {
                if (System.Web.MimeMapping.GetMimeMapping(InputImage).StartsWith("image/"))
                {
                    using (Bitmap mybitmab = new Bitmap(@InputImage))
                    {
                        ImageCodecInfo jpgEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);

                        System.Drawing.Imaging.Encoder myEncoder =
                               System.Drawing.Imaging.Encoder.Quality;

                        EncoderParameters myEncoderParameters = new EncoderParameters(1);
                        EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, Quality);
                        myEncoderParameters.Param[0] = myEncoderParameter;
                        file = PasswordGenerator.Generate(10) + System.IO.Path.GetExtension(InputImage);
                        string NewOutputPath = @OutPutDirectory + file;
                        mybitmab.Save(NewOutputPath, jpgEncoder, myEncoderParameters);
                        System.IO.File.Delete(InputImage);
                    }
                }
            }catch(Exception e)
            {

            }
            return file;

        }

        private static ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }
}