﻿namespace MessagerSolution.Helper
{
    public enum FrequencyEnum
    {
        Daily,
        Weekly,
        Monthly,
        Quarterly,
        Yearly
    }
}