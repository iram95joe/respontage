﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper
{
    public class MMSImages
    {
        public static void SaveMMS(MMSImage image)
        {
            using(var db = new ApplicationDbContext())
            {
                db.MMSImages.Add(image);
                db.SaveChanges();
            }
        }
    }
}