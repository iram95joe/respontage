﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Text;
//using System.Net.Mail;
//using System.Configuration;
//using System.Net.Mime;
//using System.Diagnostics;
//using S22.Imap;
//using Core.Database.Context;
//using Core.Helper;
//using System.IO;
//using Core.Database.Entity;
//using MlkPwgen;

//namespace MessagerSolution.Helper
//{
//    public class EmailHandler
//    {
//        #region "Private Variables of this class"
//        string _mailTo;
//        string _mailFrom;
//        string _mailSubject;
//        string _mailBody;
//        string _mailAttachmentPath;
//        string _mailCC;
//        string _mailBcc;
//        string _smtpHost = ConfigurationManager.AppSettings["smtpHost"].ToString();
//        string _smtpUser = ConfigurationManager.AppSettings["smtpUserName"].ToString();
//        string _smtpPassword = ConfigurationManager.AppSettings["smtpPassword"].ToString();
//        int _port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpEmailPort"].ToString());
//        #endregion

//        #region "Public Properties"
//        public string mailTo
//        {
//            get
//            {
//                return _mailTo;
//            }
//            set
//            {
//                _mailTo = value;
//            }
//        }
//        public string mailFrom
//        {
//            get
//            {
//                return _mailFrom;
//            }
//            set
//            {
//                _mailFrom = value;
//            }
//        }
//        public string mailSubject
//        {
//            get
//            {
//                return _mailSubject;
//            }
//            set
//            {
//                _mailSubject = value;
//            }
//        }
//        public string mailBody
//        {
//            get
//            {
//                return _mailBody;
//            }
//            set
//            {
//                _mailBody = value;
//            }
//        }
//        public string mailAttachmentPath
//        {
//            get
//            {
//                return _mailAttachmentPath;
//            }
//            set
//            {
//                _mailAttachmentPath = value;
//            }
//        }
//        public string mailCC
//        {
//            get
//            {
//                return _mailCC;
//            }
//            set
//            {
//                _mailCC = value;
//            }
//        }
//        public string mailBcc
//        {
//            get
//            {
//                return _mailBcc;
//            }
//            set
//            {
//                _mailBcc = value;
//            }
//        }

//        #endregion

//        #region "Public Functions"

//        // Public function to send email
//        public void SendEmail()
//        {
//            try
//            {
//                if (string.IsNullOrEmpty(mailFrom))
//                {
//                    mailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"].ToString();
//                }
//                System.Net.Mail.MailMessage objMsg = new MailMessage();
//                objMsg.To.Add(mailTo);
//                objMsg.From = new MailAddress(mailFrom, "Respontage.com");
//                objMsg.IsBodyHtml = true;
//                objMsg.Subject = mailSubject;
//                objMsg.Body = mailBody;
//                objMsg.Priority = System.Net.Mail.MailPriority.Normal;
//                using (SmtpClient client = new SmtpClient("smtp.gmail.com", 587))
//                {
//                    client.EnableSsl = true;
//                    client.UseDefaultCredentials = false;
//                    client.Credentials = new System.Net.NetworkCredential(_smtpUser, _smtpPassword);
//                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
//                    client.Send(objMsg);
//                }
//            }
//            catch (Exception ex)
//            {
//                string error = ex.Message;
//                Trace.WriteLine(ex.ToString());
//            }
//        }


//        #endregion

//        #region Get Old Emails
//        public static void GetOldEmails(string newEmail)
//        {
//            using (var db = new ApplicationDbContext())
//            {
//                try
//                {
//                    var renter = GetRenter(newEmail);
//                    if (renter != null)
//                    {
//                        ImapClient client;
//                        var accounts = db.EmailAccounts.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
//                        foreach (var account in accounts)
//                        {
//                            client = new ImapClient("imap.gmail.com", 993, account.Email, Security.Decrypt(account.Password), AuthMethod.Login, true);
//                            var sentMails = client.Search(SearchCondition.To(newEmail), "[Gmail]/Sent Mail");
//                            var inboxes = client.Search(SearchCondition.From(newEmail), "INBOX");
//                            foreach (var sentmail in sentMails)
//                            {
//                                var email = client.GetMessage(sentmail, true, "[Gmail]/Sent Mail");
//                                GetEmailData(email, renter);


//                            }
//                            foreach (var inbox in inboxes)
//                            {
//                                var email = client.GetMessage(inbox);
//                                GetEmailData(email, renter);
//                            }
//                        }
//                    }
//                }
//                catch (Exception e)
//                {
//                    HtmlLogs.Add("EMAIL" + e.Message + " " + e.InnerException + " " + e.StackTrace);
//                }
//            }
//        }

//        public static void GetEmailData(MailMessage email, Renter renter)
//        {
//            var emailFrom = email.From.Address.ToSafeString();
//            var emailTo = email.To.ToSafeString();
//            var subject = email.Subject.Replace("Fwd:", "").Trim();
//            var date = email.Headers["Date"].ToDateTime();
//            var message = email.Body;
//            var attachments = new List<string>();
//            if (email.Attachments.Count > 0)
//            {
//                foreach (var attachment in email.Attachments)
//                {
//                    string filename = "/Images/EmailAttachment/" + attachment.Name;
//                    var path = System.AppDomain.CurrentDomain.BaseDirectory + "/Images/EmailAttachment/";
//                    System.IO.Directory.CreateDirectory(path);
//                    var copyPath = path + attachment.Name;
//                    using (var destination = File.Create(copyPath))
//                        attachment.ContentStream.CopyTo(destination);

//                    attachments.Add(filename);
//                }
//            }
//            SaveEmail(renter, emailFrom, emailTo, message, date, attachments);
//        }
//        static void SaveEmail(Renter renter, string from, string to, string message, DateTime date, List<string> attachments)
//        {
//            using (var db = new ApplicationDbContext())
//            {
//                var thread = db.CommunicationThreads.Where(x => x.CompanyId == SessionHandler.CompanyId && renter.Id == x.RenterId && x.IsRenter).FirstOrDefault();
//                CommunicationThread workerThread;
//                if (thread == null)
//                {
//                    workerThread = new CommunicationThread();
//                    workerThread.RenterId = renter.Id;
//                    workerThread.ThreadId = PasswordGenerator.Generate(20);
//                    workerThread.CompanyId = SessionHandler.CompanyId;
//                    db.CommunicationThreads.Add(workerThread);
//                    db.SaveChanges();
//                }
//                else
//                {
//                    workerThread = thread;
//                    workerThread.IsEnd = false;
//                    db.Entry(workerThread).State = System.Data.Entity.EntityState.Modified;
//                    db.SaveChanges();
//                }

//                var temp = db.CommunicationEmails.Where(x => x.From == from && x.To == to && message == message && x.CreatedDate == date && x.ThreadId == thread.ThreadId).FirstOrDefault();
//                if (temp == null)
//                {
//                    var Email = new CommunicationEmail()
//                    {
//                        From = from,
//                        To = to,
//                        IsMyMessage = (renter.Email == to ? true : false),
//                        Message = message,
//                        ThreadId = workerThread.ThreadId,
//                        CreatedDate = date
//                    };
//                    db.CommunicationEmails.Add(Email);
//                    db.SaveChanges();
//                    if (attachments.Count > 0)
//                    {
//                        foreach (var attachment in attachments)
//                        {
//                            db.EmailAttachments.Add(new CommunicationEmailAttachment()
//                            {
//                                CommunicationEmailId = Email.Id,
//                                FileUrl = attachment
//                            });
//                            db.SaveChanges();
//                        }
//                    }
//                }
//                else
//                {
//                    if (attachments.Count > 0)
//                    {
//                        foreach (var attachment in attachments)
//                        {
//                            System.IO.File.Delete(System.AppDomain.CurrentDomain.BaseDirectory + attachment);

//                        }
//                    }
//                }

//            }
//        }
//        static Renter GetRenter(string email)
//        {
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    var renter = (from r in db.Renters join reservation in db.Inquiries on r.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where property.CompanyId == SessionHandler.CompanyId && !property.IsParentProperty && property.SiteType == 0 && r.Email == email select r).FirstOrDefault();
//                    return renter;
//                }
//            }
//            catch
//            {
//                return null;
//            }

//        }
//        #endregion

//        #region SyncEmail
//        public static void SyncEmail(int id)
//        {
//            using (var db = new ApplicationDbContext())
//            {
//                try
//                {
//                    ImapClient client;
//                    var account = db.EmailAccounts.Where(x => x.Id == id).FirstOrDefault();
//                    if (account != null)
//                    {
//                        client = new ImapClient("imap.gmail.com", 993, account.Email, Security.Decrypt(account.Password), AuthMethod.Login, true);
//                        var renterEmails = (from renter in db.Renters join email in db.RenterEmails on renter.Id equals email.RenterId join reservation in db.Inquiries on renter.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where property.CompanyId == SessionHandler.CompanyId select new { email.Email, renter }).ToList();
//                        foreach (var renterEmail in renterEmails)
//                        {
//                            var sentMails = client.Search(SearchCondition.To(renterEmail.Email), "[Gmail]/Sent Mail");
//                            var inboxes = client.Search(SearchCondition.From(renterEmail.Email), "INBOX");
//                            foreach (var sentmail in sentMails)
//                            {
//                                var email = client.GetMessage(sentmail, true, "[Gmail]/Sent Mail");
//                                GetEmailData(email, renterEmail.renter);


//                            }
//                            foreach (var inbox in inboxes)
//                            {
//                                var email = client.GetMessage(inbox);
//                                GetEmailData(email, renterEmail.renter);
//                            }
//                        }
//                    }
//                }
//                catch (Exception e)
//                {
//                }

//            }
//        }
//        #endregion
//    }
//}