﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.Renters
{
    public class Emails
    {
        public static void AddOrUpdate(int renterId, string email)
        {
            using (var db = new ApplicationDbContext())
            {
                var emailAddressAttribute= new EmailAddressAttribute();
                if (emailAddressAttribute.IsValid(email))
                {
                    var temp = db.RenterEmails.Where(x => x.RenterId == renterId && x.Email == email).FirstOrDefault();
                    if (temp == null)
                    {
                        db.RenterEmails.Add(new RenterEmail() { RenterId = renterId, Email = email });
                        db.SaveChanges();
                        //EmailHandler.GetOldEmails(email);
                    }
                }
            }
        }
    }
}