﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.Renters
{
    public class Contacts
    {
       
        public static void AddOrUpdate(int renterId, string contact)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.RenterContacts.Where(x => x.RenterId == renterId && x.Contact == contact).FirstOrDefault();
                if (temp == null)
                {
                    db.RenterContacts.Add(new RenterContact() { RenterId = renterId, Contact = contact });
                    db.SaveChanges();
                }
            }
        }
    }
}