﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Helper
{
    public class InboxModelWorker
    {
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public string[] AvailDay { get; set; }
        public string[] AvailTimeStart { get; set; }
        public string[] AvailTimeEnd { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public bool Status { get; set; }
        public int CompanyId { get; set; }
    }
}