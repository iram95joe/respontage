﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Models.Invoice;
using MlkPwgen;
using Spire.Doc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spire.Doc.Documents;
using MessagerSolution.Controllers;
using Spire.Doc.Fields;
using MessagerSolution.Models.Invoice;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace MessagerSolution.Helper
{
    public class InvoiceSchedule
    {
        private readonly Service.EmailService _emailService = new Service.EmailService();
        HttpContext context;

        public InvoiceSchedule(HttpContext context)
        {
            this.context = context;
        }
        public Inquiry GetCurrentReservation(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var listingId = db.Properties.Where(x => x.Id == propertyId).Select(x => x.ListingId).FirstOrDefault();
                return db.Inquiries.Where(x => x.PropertyId == listingId).OrderByDescending(x => x.Id).FirstOrDefault();
            }
        }

        public string GetUnpaidTransaction(int bookingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var now = DateTime.Now;
                var lastDate = now.AddMonths(1);
                var list = new List<RequestAmountViewModel>();
                var incomes = db.Income.Where(x => x.BookingId == bookingId && x.DueDate<=lastDate && x.IsActive && x.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).ToList();
                foreach (var income in incomes)
                {
                    var temp = db.IncomePayment.Where(x => x.IncomeId == income.Id && x.IsRejected == false).ToList();
                    //JM 09/14/19  amount minus payment to get balance
                    if (temp.Sum(x => x.Amount) < income.Amount)
                    {
                        income.Amount = income.Amount - temp.Sum(x => x.Amount);
                        list.Add(new RequestAmountViewModel() { Description = income.Description, DueDate = income.DueDate, Amount = income.Amount.GetValueOrDefault(), IncomeId = income.Id });
                    }
                }
                var serializer = new JavaScriptSerializer();
                return serializer.Serialize(list);
            }
        }
        private decimal GetUnallocatedAmount(int bookingId)
        {
            var result = 0.0M;
            using(var db = new ApplicationDbContext())
            {
                var batchs = db.IncomeBatchPayments.Where(x => x.BookingId == bookingId).ToList();
                foreach(var batch in batchs)
                {
                    var payment = db.IncomePayment.Where(x => x.IncomeBatchPaymentId == batch.Id).Select(x=>x.Amount).ToList();
                 result += batch.Amount- payment.Sum();

                }
                return result;
            }
        }
        public bool CreateInvoice(InvoiceViewModel model)
        {
            var isCreated = false;
            using (var db = new ApplicationDbContext())
            {
                var reservation = db.Inquiries.Where(x => x.Id == model.BookingId).FirstOrDefault();
                var data = (from renter in db.Renters
                            join e in db.RenterEmails on renter.Id equals e.RenterId
                            where renter.BookingId == reservation.Id
                            select new { Email = e, Renter = renter }).FirstOrDefault();
                var deposit = GetUnallocatedAmount(model.BookingId);
                if (string.IsNullOrEmpty(data.Email.Email))
                {

                    isCreated = false;
                }
                else
                {
                    var invoice = new Invoice()
                    {
                        InvoiceScheduleId=model.InvoiceScheduleId,
                        BookingId = model.BookingId,
                        DateCreated = DateTime.Now,
                        TotalAmount = model.RequestPayment.Sum(x => x.Amount)-deposit,
                        PaymentMethod = (int)model.PaymentMethod,
                        RequestPaymentJsonString = model.RequestPaymentJsonString,
                        FileUrl = CreateInvoiceFile(model.RequestPayment, model.BookingId, model.InvoiceTemplateId)
                    };
                    db.Invoices.Add(invoice);
                    db.SaveChanges();

                    var paymentModel = new Models.Invoice.InvoicePaymentRequest
                    {
                        MailSubject = DateTime.Now.ToString("MMMM yyyy") + "Bills",
                        TotalAmount = model.RequestPayment.Sum(x => x.Amount)-deposit,
                        Email = data.Renter.Email,
                        DateCreated = DateTime.Now,
                        Renter = data.Renter.Firstname + " " + data.Renter.Lastname,
                        InvoiceId = invoice.Id,
                        PaymentMethod = model.PaymentMethod

                    };
                    SendInvoiceEmail(paymentModel, EmailTemplateReplaceValue(model.RequestPayment, model.BookingId, model.EmailTemplateId), invoice.FileUrl);
                    isCreated = true;
                }
            }
            return isCreated;
        }
        public string EmailTemplateReplaceValue(List<RequestAmountViewModel> RequestPayment, int bookingId, int templateId)
        {
            using (var db = new ApplicationDbContext())
            {
                var reservation = db.Inquiries.Where(x => x.Id == bookingId).FirstOrDefault();
                var renter = db.Renters.Where(x => x.BookingId == bookingId).FirstOrDefault();
                var property = db.Properties.Where(x => reservation.PropertyId == x.ListingId).FirstOrDefault();
                var company = db.Companies.Where(x => x.Id == property.CompanyId).FirstOrDefault();
                var template = db.EmailTemplates.Where(x => x.CompanyId == property.CompanyId && x.Id == templateId).FirstOrDefault().Description;

                template = template.Replace("[Renter Lastname]", renter.Lastname)
                 .Replace("[Transaction Details]", GetRequestBillDescription(RequestPayment,bookingId))
                 .Replace("[Renter Firstname]", renter.Firstname)
                 .Replace("[Renter Phone]", renter.PhoneNumber)
                 .Replace("[Renter Address]", property.Address)
                 .Replace("[Property Address]", property.Address)
                 .Replace("[Company]", company.CompanyName)
                 .Replace("[Total Amount]", RequestPayment.Sum(x => x.Amount).ToString())
                 .Replace("[Date]", DateTime.Now.ToString("MMMM dd,yyyy"));
                return template;
            }

        }
        public string GetRequestBillDescription(List<RequestAmountViewModel> RequestPayment,int bookingId)
        {
            var deposit = GetUnallocatedAmount(bookingId);
            var today = DateTime.Now;
            var bill = "";
            var previous = 0M;
            var current = 0M;
            bill += "<br/><b>Unpaid Bill</b><br/>";
            RequestPayment.Where(x => x.DueDate < today).Select(x => x.DueDate.ToString("MMM dd,yyyy") + " " + x.Description + " " + x.Amount + "<br/>").ToList().ForEach(x => bill += x);
            previous = RequestPayment.Where(x => x.DueDate < today).Sum(x => x.Amount);
            bill += "<div style=\"border: thin solid gray; width: 300px;\"></div>" +
                 "<b>Total: " + previous + "</b>";
            bill += "<br/><b>Upcomming Bill</b><br/>";
            RequestPayment.Where(x => x.DueDate > today).Select(x => x.DueDate.ToString("MMM dd,yyyy") + " - " + x.Description + " - " + x.Amount + "<br/>").ToList().ForEach(x => bill += x);
            current = RequestPayment.Where(x => x.DueDate > today).Sum(x => x.Amount);
            bill += "<div style=\"border: thin solid gray; width: 300px;\"></div>" +
                  "<b>Total: " + current + "</b></b>";

            bill += "<div style=\"border: thin solid black; width: 300px;\"></div>" +
                   "Total: " + RequestPayment.Sum(x => x.Amount) + "<br/>" +
                   "Deposit:" + deposit +"<br/>"+
                  "<b>TOTAL: " + (RequestPayment.Sum(x => x.Amount)-deposit) + "</b>";
            return bill;
        }
        public string CreateInvoiceFile(List<RequestAmountViewModel> RequestPayment, int bookingId, int templateId)
        {
            using (var db = new ApplicationDbContext())
            {
                var deposit = GetUnallocatedAmount(bookingId);
                var reservation = db.Inquiries.Where(x => x.Id == bookingId).FirstOrDefault();
                var renter = db.Renters.Where(x => x.BookingId == bookingId).FirstOrDefault();
                var property = db.Properties.Where(x => reservation.PropertyId == x.ListingId).FirstOrDefault();
                var company = db.Companies.Where(x => x.Id == property.CompanyId).FirstOrDefault();
                var template = db.InvoiceTemplates.Where(x => x.CompanyId == property.CompanyId && x.Id == templateId).FirstOrDefault();
                var file = "/Documents/Invoice/" + PasswordGenerator.Generate(10) + ".pdf";
                var table = "<table style=\"width:100%;border-collapse: collapse;\">" +
                            "<thead>" +
                                "<tr>" +
                                    "<th>Description</th>" +
                                    "<th>Date</th>" +
                                    "<th>Amount</th>" +
                                "</tr>" +
                            "</thead>" +
                            "<tbody>" +
                              CreateTransactionDetails(RequestPayment,bookingId) +
                            "</tbody>" +
                        "</table>";
             
                var newFile = System.Web.Hosting.HostingEnvironment.MapPath("~" + file);
                var fileLocation = System.Web.Hosting.HostingEnvironment.MapPath("~" + template.FileUrl);
                Document word = new Document(fileLocation);
                //var selection = word.FindString("[Transaction Details]",true,true);
                Section tempSection = word.AddSection();
                Paragraph p4 = tempSection.AddParagraph();
                p4.AppendHTML(table);
                List<DocumentObject> replacement = new List<DocumentObject>();
                foreach (var obj in tempSection.Body.ChildObjects)
                {
                    DocumentObject O = obj as DocumentObject;
                    replacement.Add(O);
                }
                TextSelection[] selections = word.FindAllString("[Transaction Details]", false, true);
                List<TextRangeLocation> locations = new List<TextRangeLocation>();
                foreach (TextSelection selection in selections)
                {
                    locations.Add(new TextRangeLocation(selection.GetAsOneRange()));
                }
                locations.Sort();
                foreach (TextRangeLocation location in locations)
                {
                    ReplaceObj(location, replacement);
                }
                //remove the temp section
                word.Sections.Remove(tempSection);
                word.Replace("[Renter Lastname]", renter.Lastname, true, true);
                word.Replace("[Renter Firstname]", renter.Firstname, true, true);
                word.Replace("[Property Address]", string.IsNullOrWhiteSpace(property.Address) ? "[Property address]" : property.Address, true, true);
                word.Replace("[Date]", DateTime.Now.ToString("MMMM dd,yyyy"), true, true);
                //word.Replace("[Transaction Details]", table, true, true);
                word.Replace("[Total Amount]", (RequestPayment.Sum(x=>x.Amount)-deposit).ToString("F"), true, true);
                word.Replace("[Company]", company.CompanyName, true, true);
                word.Replace("[Renter Address]", property.Address, true, true);
                word.Replace("[Renter Phone]", renter.PhoneNumber, true, true);
                word.HTMLTrackChanges = true;
                word.SaveToFile(System.Web.Hosting.HostingEnvironment.MapPath("~" + file));
                return file;
            }

        }

        private void ReplaceObj(TextRangeLocation location, List<DocumentObject> replacement)
        {
            //will be replaced
            TextRange textRange = location.Text;

            //textRange index
            int index = location.Index;

            //owener paragraph
            Paragraph paragraph = location.Owner;

            //owner text body
            Body sectionBody = paragraph.OwnerTextBody;

            //get the index of paragraph in section
            int paragraphIndex = sectionBody.ChildObjects.IndexOf(paragraph);

            int replacementIndex = -1;
            if (index == 0)
            {
                //remove
                paragraph.ChildObjects.RemoveAt(0);

                replacementIndex = sectionBody.ChildObjects.IndexOf(paragraph);
            }
            else if (index == paragraph.ChildObjects.Count - 1)
            {
                paragraph.ChildObjects.RemoveAt(index);
                replacementIndex = paragraphIndex + 1;

            }
            else
            {
                //split owner paragraph
                Paragraph paragraph1 = (Paragraph)paragraph.Clone();
                while (paragraph.ChildObjects.Count > index)
                {
                    paragraph.ChildObjects.RemoveAt(index);
                }
                int i = 0;
                int count = index + 1;
                while (i < count)
                {
                    paragraph1.ChildObjects.RemoveAt(0);
                    i += 1;
                }
                sectionBody.ChildObjects.Insert(paragraphIndex + 1, paragraph1);

                replacementIndex = paragraphIndex + 1;
            }

            //insert replacement
            for (int i = 0; i <= replacement.Count - 1; i++)
            {
                sectionBody.ChildObjects.Insert(replacementIndex + i, replacement[i].Clone());
            }
        }
        private string CreateTransactionDetails(List<RequestAmountViewModel> RequestPayment,int bookingId)
        {
            var deposit = GetUnallocatedAmount(bookingId);
            var today = DateTime.Now;
            var lastDate = today.AddMonths(1);
            var upcommingBill = RequestPayment.Where(x => x.DueDate.Month == lastDate.Month && x.DueDate.Year == lastDate.Year).ToList();
            var lastMonthBill = RequestPayment.Where(x => x.DueDate.Month == today.Month && x.DueDate.Year == today.Year).ToList();
            var details = "";
            var lastmonthTotal = 0.0M;
            var upcommingPrePayment = 0.0M;
            using (var db = new ApplicationDbContext())
            {
               details+= "<tr><td colspan=\"3\">Last Month Payment</td></tr>";
                foreach (var income in lastMonthBill)
                {
                    var payments = db.IncomePayment.Where(x => x.IncomeId == income.IncomeId && !x.IsRejected).ToList();
                    foreach(var payment in payments)
                    {
                        details += "<tr>" +
                             "<td style=\"text-align:center\">" + payment.Description + "</td>" +
                             "<td style=\"text-align:center\">" + payment.CreatedAt.ToString("MMM dd,yyyy") + "</td>" +
                             "<td style=\"text-align:center\">" + payment.Amount + "</td>" +
                         "</tr>";
                        lastmonthTotal += payment.Amount;
                    }
                }
                details += "<tr>" +
                               "<td style=\"text-align:center\"></td>" +
                               "<td style=\"text-align:center;border-top: 1px solid black;\"><b>Total</b></td>" +
                               "<td style=\"text-align:center;border-top: 1px solid black;\">" + lastmonthTotal + "</td>" +
                           "</tr>";

                details+= "<tr><td colspan=\"3\">Advance Payment</td></tr>";
                foreach (var income in upcommingBill)
                {
                    var payments = db.IncomePayment.Where(x => x.IncomeId == income.IncomeId && !x.IsRejected).ToList();
                    foreach (var payment in payments)
                    {
                        details += "<tr>" +
                             "<td style=\"text-align:center\">" + payment.Description + "</td>" +
                             "<td style=\"text-align:center\">" + payment.CreatedAt.ToString("MMM dd,yyyy") + "</td>" +
                             "<td style=\"text-align:center\">" + payment.Amount + "</td>" +
                         "</tr>";
                        upcommingPrePayment += payment.Amount;
                    }
                }
                details += "<tr>" +
                             "<td style=\"text-align:center\"></td>" +
                             "<td style=\"text-align:center;border-top: 1px solid black;\"><b>Total</b></td>" +
                             "<td style=\"text-align:center;border-top: 1px solid black;\">" + upcommingPrePayment + "</td>" +
                         "</tr>";
            }

            //Unpaid Bills
            details += "<tr><td colspan=\"3\">Unpaid bill</td></tr>";
            foreach (var req in RequestPayment.Where(x => x.DueDate < today))
            {
                details += "<tr>" +
                                "<td style=\"text-align:center\">" + req.Description + "</td>" +
                                "<td style=\"text-align:center\">" + req.DueDate.ToString("MMM dd,yyyy") + "</td>" +
                                "<td style=\"text-align:center\">" + req.Amount + "</td>" +
                            "</tr>";
            }
            details+=       "<tr>" +
                                "<td style=\"text-align:center\"></td>" +
                                "<td style=\"text-align:center;border-top: 1px solid black;\"><b>Total</b></td>" +
                                "<td style=\"text-align:center;border-top: 1px solid black;\">" + RequestPayment.Where(x => x.DueDate < today).Sum(x => x.Amount) + "</td>" +
                            "</tr>";

            //Upcomming bills

            details += "<tr><td colspan=\"3\">Upcomming Bill</td></tr>";
            foreach (var req in RequestPayment.Where(x => x.DueDate > today))
            {
                details += "<tr>" +
                                "<td style=\"text-align:center\">" + req.Description + "</td>" +
                                "<td style=\"text-align:center\">" + req.DueDate.ToString("MMM dd,yyyy") + "</td>" +
                                "<td style=\"text-align:center\">" + req.Amount + "</td>" +
                            "</tr>";
            }
            details +=     "<tr>" +
                               "<td style=\"text-align:center\"></td>" +
                               "<td style=\"text-align:center;border-top: 1px solid black;\"><b>Total</b></td>" +
                               "<td style=\"text-align:center;border-top: 1px solid black;\">" + RequestPayment.Where(x => x.DueDate > today).Sum(x => x.Amount) + "</td>" +
                           "</tr>";

            details += "<tr>" +
                             "<td style=\"text-align:center;border-top: 1px solid black;\"></td>" +
                             "<td style=\"text-align:center;border-top: 1px solid black;\">Total</td>" +
                             "<td style=\"text-align:center;border-top: 1px solid black;\">" + RequestPayment.Sum(x => x.Amount) + "</td>" +
                         "</tr>";
            details +=    "<tr>" +
                              "<td style=\"text-align:center\"></td>" +
                              "<td style=\"text-align:center\">Deposit</td>" +
                              "<td style=\"text-align:center\">" +deposit + "</td>" +
                          "</tr>";
            details += "<tr>" +
                            "<td style=\"text-align:center\"></td>" +
                            "<td style=\"text-align:center\"><b>TOTAL</b></td>" +
                            "<td style=\"text-align:center\"><b>" + (RequestPayment.Sum(x => x.Amount)-deposit) + "</b></td>" +
                        "</tr>";
            return details;
        }
        public void SendInvoiceEmail(Models.Invoice.InvoicePaymentRequest data, string template, string attachment)
        {
            //if (!System.IO.File.Exists(path)) return;
            //var template = System.IO.File.ReadAllText(path);
            var callbackUrl = "";
            var urlHelper = new UrlHelper(context.Request.RequestContext);
            if (data.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Paypal)
            {
                //callbackUrl = urlHelper.Action("Paypal", "Payment", new { invoiceId = data.InvoiceId }, protocol: context.Request.Url.Scheme);
                callbackUrl = context.Request.Url + "Payment/Paypal?invoiceId=" + data.InvoiceId;
            }
            else if (data.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Square)
            {
                //callbackUrl = urlHelper.Action("Square", "Payment", new { invoiceId = data.InvoiceId }, protocol: context.Request.Url.Scheme);
                callbackUrl = context.Request.Url + "Payment/Square?invoiceId=" + data.InvoiceId;
            }
            else if (data.PaymentMethod == Core.Enumerations.PaymentRequestMethod.Stripe)
            {
                callbackUrl = context.Request.Url + "Payment/Stripe?invoiceId=" + data.InvoiceId;// urlHelper.Action("Stripe", "Payment", new { invoiceId = data.InvoiceId }, protocol:protocol);
            }

            template += "<br/>" +
             "LinK: <a href=\"" + callbackUrl + "\">Click Here</a><br />" +
             "<br/>";


            var success = _emailService.SendEmail(data.MailSubject, template, data.Email, System.Web.Hosting.HostingEnvironment.MapPath("~" + attachment));
        }
    }
}