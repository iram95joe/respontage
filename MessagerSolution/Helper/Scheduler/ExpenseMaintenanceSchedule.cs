﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.Scheduler
{
    public class ExpenseMaintenanceSchedule
    {
        //Kendrick say that after creating of entries, there is no update of amount from scheduler
        //01/08/2022
        public void CreateExpense(PropertyScheduledExpense maintenance, Expense temp, DateTime duedate)
        {
            using (var db = new ApplicationDbContext())
            {
                int maintenanceEntryId = 0, assignmentId = 0;
                if (maintenance.Type == 1)
                {
                    maintenanceEntryId = CreateMaintenanceEntry(maintenance, duedate, temp);
                    assignmentId = CreateWorkerAssignment(maintenance, maintenanceEntryId, duedate);
                }
                if (temp == null)
                {
                    Expense expense = new Expense
                    {
                        Amount = maintenance.Amount,
                        Description = maintenance.Description,
                        ExpenseCategoryId = maintenance.ExpenseCategoryId,
                        PropertyId = maintenance.PropertyId,
                        DueDate = duedate,
                        PaymentDate = null,
                        IsCapitalExpense = false,
                        IsStartupCost = false,
                        Frequency = maintenance.Frequency,
                        DateLastJobRun = null,
                        DateCreated = DateTime.Now,
                        DateNextJobRun = null,
                        PaymentMethodId = maintenance.PaymentMethodId,
                        CompanyId = maintenance.CompanyId,
                        IsPaid = false,
                        WorkerAssignmentId = assignmentId,
                        PropertyScheduledExpenseId = maintenance.Id,
                        MaintenanceEntryId = maintenanceEntryId

                    };
                    db.Expenses.Add(expense);
                    db.SaveChanges();
                }
                else
                {
                    temp.WorkerAssignmentId = assignmentId;
                    temp.MaintenanceEntryId = maintenanceEntryId;
                    temp.DueDate = duedate;
                    temp.Description = maintenance.Description;
                    //temp.Amount = maintenance.Amount;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public int CreateWorkerAssignment(PropertyScheduledExpense maintenance, int maintenanceEntryId, DateTime duedate)
        {
            using (var db = new ApplicationDbContext())
            {
                if (maintenanceEntryId == 0)
                    return 0;
                if (maintenance.WorkerId != null && maintenance.JobTypeId != null)
                {
                    var temp = db.WorkerAssignments.Where(x => x.MaintenanceEntryId == maintenanceEntryId).FirstOrDefault();
                    if (temp == null)
                    {
                        WorkerAssignment assignment = new WorkerAssignment()
                        {
                            CompanyId = maintenance.CompanyId,
                            WorkerId = maintenance.WorkerId.ToInt(),
                            JobTypeId = maintenance.JobTypeId.ToInt(),
                            StartDate = duedate,
                            EndDate = duedate,
                            PropertyId = maintenance.PropertyId,
                            MaintenanceEntryId = maintenanceEntryId

                        };
                        db.WorkerAssignments.Add(assignment);
                        db.SaveChanges();
                        return assignment.Id;
                    }
                    else
                    {
                        temp.WorkerId = maintenance.WorkerId.ToInt();
                        temp.JobTypeId = maintenance.JobTypeId.ToInt();
                        temp.StartDate = duedate;
                        temp.EndDate = duedate;
                        temp.PropertyId = maintenance.PropertyId;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return 0;
            }
        }
        public int CreateMaintenanceEntry(PropertyScheduledExpense maintenance, DateTime duedate, Expense expense)
        {
            int maintenanceEntryId = 0;
            using (var db = new ApplicationDbContext())
            {
                if (maintenance.Type == 1)
                {
                    var maintenanceEntry = new MaintenanceEntry()
                    {
                        Amount = maintenance.Amount,
                        Description = maintenance.Description,
                        DueDate = duedate,
                        PropertyId = maintenance.PropertyId,
                        JobTypeId = maintenance.JobTypeId,
                        VendorId = maintenance.VendorId,
                        WorkerId = maintenance.WorkerId,
                        PropertyScheduledExpenseId = maintenance.Id,
                        CompanyId = maintenance.CompanyId
                    };

                    maintenanceEntryId = expense == null ? Core.API.AllSite.MaintenanceEntries.Add(maintenanceEntry) : Core.API.AllSite.MaintenanceEntries.Update(expense.MaintenanceEntryId.Value, maintenanceEntry);
                }
            }
            return maintenanceEntryId;
        }
    }
}