﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.Scheduler
{
    public class NotificationSchedule
    {
        public void Run(int companyId)
        {
            using(var db = new ApplicationDbContext())
            {
                var notifications = db.Notifications.Where(x => x.CompanyId == companyId).ToList();
                foreach(var notification in notifications)
                {
                    NotificationScheduleType type = (NotificationScheduleType)notification.ScheduleType;
                    var propertyIds = GetProperyIds(notification.Id);
                    foreach(var propertyId in propertyIds)
                    {
                        var reservations = GetReservations(propertyId);
                        foreach(var reservation in reservations)
                        {
                            switch (type)
                            {
                                case NotificationScheduleType.LastRentIncrease:
                                    CreateLastIncreaseNotification(notification,propertyId, reservation.Id);
                                    break;
                                case NotificationScheduleType.LatePayment:
                                    CreateLatePaymentNotification(notification, propertyId, reservation.Id);
                                    break;
                                case NotificationScheduleType.RenewOfContract:
                                    CreateRenewContractNotification(notification, propertyId, reservation);
                                    break;
                            }
                        }
                    }
                }
            }
        }

        public List<int> GetProperyIds(int notificationId)
        {
            using(var db = new ApplicationDbContext())
            {
                return db.NotificationProperties.Where(x => x.NotificationId == notificationId).Select(x=>x.PropertyId).ToList();
            }

        }
        public List<Inquiry> GetReservations(int propertyId)
        {
            using (var db = new ApplicationDbContext()) {
                return (from reservation in db.Inquiries join property in db.Properties on reservation.PropertyId equals property.ListingId where property.Id == propertyId select reservation).OrderByDescending(x=>x.CheckInDate).ToList();
            }
        }
        public MonthlyRent GetLastIncreaseDate(int reservationId)
        {
            using (var db = new ApplicationDbContext()) {
               return db.MonthlyRents.Where(x => x.BookingId == reservationId && x.EffectiveDate <=DateTime.Now).FirstOrDefault();
            }
        }
        public List<Income> GetUnpaidIncome(int reservationId)
        {
            using (var db = new ApplicationDbContext()) {
                var incomes = new List<Income>();
                var result =db.Income.Where(x => x.BookingId == reservationId).ToList();
                foreach(var temp in result)
                {
                    var payment = db.IncomePayment.Where(x => x.IncomeId == temp.Id).Sum(x=>x.Amount);
                    if (payment != temp.Amount)
                    {
                        incomes.Add(temp);
                    }
                }
                return incomes;
            }
        }

        public void CreateRenewContractNotification(Notification notification, int propertyId, Inquiry reservation)
        {
            string details = notification.Description;
            details = details.Replace("[Details]", "Contract start:"+ reservation.CheckInDate.Value.ToString("MMMM dd, yyyy"));
            CreateSchedulerEntries(reservation.CheckInDate.Value, details,propertyId, notification,null,null ,reservation.Id);
        }
        public void CreateLastIncreaseNotification(Notification notification,int propertyId, int reservationId)
        {
            var LastIncrease = GetLastIncreaseDate(reservationId);
            string details = notification.Description;
            details =details.Replace("[Details]", LastIncrease.EffectiveDate.ToString("MMMM yyyy") + " - " + LastIncrease.Amount);
            CreateSchedulerEntries(LastIncrease.EffectiveDate,details,propertyId, notification,null,LastIncrease.Id,null);
        }
        public void CreateLatePaymentNotification(Notification notification, int propertyId, int reservationId)
        {
            var UnpaidIncomes = GetUnpaidIncome(reservationId);
            foreach(var income in UnpaidIncomes)
            {
                string details = notification.Description;
                details.Replace("[Details]", income.Description + " - " + income.DueDate);
                CreateSchedulerEntries(income.DueDate,details,propertyId, notification,income.Id);
            }

        }

        public static void CreateEntries(Notification notification,DateTime CreatedDate, string details,int propertyId,int? incomeId,int?rentId,int?reservationId)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = db.NotificationEntries.Where(x => x.NotificationId == notification.Id && x.DateCreated == CreatedDate &&x.IncomeId == incomeId && x.RentId == rentId && x.ReservationId==reservationId && x.PropertyId==propertyId).FirstOrDefault();
                if (temp == null)
                {
                    db.NotificationEntries.Add(new NotificationEntry() {Description= details,PropertyId = propertyId, DateCreated=CreatedDate,NotificationId=notification.Id ,IncomeId = incomeId ,RentId = rentId ,ReservationId = reservationId });
                    db.SaveChanges();
                }
            }
        }
       
        public static void CreateSchedulerEntries(DateTime RecurringDate,string details,int propertyId,Notification notification, int? incomeId=null, int? rentId=null, int? reservationId=null)
        {

            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                db.Database.ExecuteSqlCommand("SET DEADLOCK_PRIORITY NORMAL;");
               var frequency = (NotificationFrequency)notification.Frequency;
                    var duedate = DateTime.Now;
                    var date = DateTime.Now;
                    switch (frequency)
                    {
                        case NotificationFrequency.Day:
                            break;
                        case NotificationFrequency.Week:
                            //duedate = RecurringDate;
                            double diff = (date.Date - duedate.Date).TotalDays / 7;
                            var numberOf = Math.Floor(diff);
                            List<DateTime> daysInWeek = new List<DateTime>();
                            DayOfWeek Day = numberOf > -1 ? date.DayOfWeek : duedate.DayOfWeek;
                            int Days = Day - DayOfWeek.Sunday; //here you can set your Week Start Day

                            DateTime WeekStartDate = numberOf > -1 ? date.AddDays(-Days) : duedate.AddDays(-Days);
                            for (int i = 0; i < 7; i++)
                            {
                                daysInWeek.Add(WeekStartDate.AddDays(i).Date);
                            }

                            var dayofWeeK = RecurringDate.DayOfWeek;
                            duedate = daysInWeek.Where(x => x.DayOfWeek == dayofWeeK).Select(x => x.Date).FirstOrDefault();
                            //Check if the current week is within interval of weeks
                            diff = (duedate.Date - RecurringDate.Date).TotalDays / 7;
                            //Check if the current week is within interval of weeks
                            diff = (duedate.Date - RecurringDate.Date).TotalDays / 7;
                            numberOf = Math.Floor(diff);
                            var result = numberOf % notification.Interval;
                            if (result == 0 || numberOf == -1)
                            {
                                CreateEntries(notification, duedate,details,propertyId,incomeId,rentId,reservationId);
                            }
                            break;
                        case NotificationFrequency.Month:
                            var intervalMonth = notification.Interval;
                            double dtspan = 0;
                            //Due date is recurring date
                            duedate = RecurringDate;
                            if (date > duedate)
                            {
                                    do
                                    {
                                        dtspan = Math.Abs(new DateTime(duedate.Year, duedate.Month, 1).Subtract(new DateTime(date.Year, date.Month, 1)).Days / (365.25 / 12));

                                        if (Math.Floor(dtspan) >= intervalMonth)
                                        {
                                            //
                                            duedate = duedate.AddMonths(intervalMonth);
                                        }
                                    } while (dtspan >= intervalMonth);
                                    //It will set the last day of month for duedate.ie when recurring day is 31 then month is february then last day will be 28 or 29
                                    var recurringday = RecurringDate.Day;
                                    var lastDayMonth = DateTime.DaysInMonth(duedate.Year, duedate.Month);
                                    if (lastDayMonth < recurringday)
                                    {
                                        recurringday = lastDayMonth;
                                    }
                                    duedate = new DateTime(duedate.Year, duedate.Month, recurringday);
                                   CreateEntries(notification, duedate,details,propertyId, incomeId, rentId, reservationId);
                            }
                            break;
                        case NotificationFrequency.Year:
                            duedate = RecurringDate;
                            var resultInterval = (date.AddMonths(1).Year - duedate.Year) % notification.Interval;
                            if (resultInterval == 0)
                            {
                                int year = date.Year;
                                if (date.AddMonths(1).Year > date.Year && date.Month != RecurringDate.Month)
                                {
                                    year = date.AddMonths(1).Year;
                                }
                                var lastdayMonthyearly = DateTime.DaysInMonth(year, RecurringDate.Month);
                            var feedayyearly = RecurringDate.Day;//RecurringDate.Day;
                                if (lastdayMonthyearly < feedayyearly)
                                {
                                    feedayyearly = lastdayMonthyearly;
                                }
                                //duedate = new DateTime(year, RecurringDate.Month, feedayyearly);
                                CreateEntries(notification, duedate,details, propertyId, incomeId, rentId, reservationId);
                            }
                            break;
                    }
            }
        }
    }
}