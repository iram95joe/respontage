﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper
{
    public class HttpWebRequestError
    {
        public string URL { get; set; }
        public string HTML { get; set; }
        public string StatusCode { get; set; }
        public string Token { get; set; }
        public string MethodName { get; set; }

        public string ToJSON()
        {
            return JsonConvert.SerializeObject(this);
        }

    }

    public class XpathError
    {
        public string URL { get; set; }
        public string HTML { get; set; }
        public string Token { get; set; }
        public string MethodName { get; set; }
        public string XpathSelector { get; set; }
        public string ToJSON()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}