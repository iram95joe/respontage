﻿using Airbnb;
using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Helper;
using Core;
using MessagerSolution.Models;
using Core.Models;
using Core.Enumerations;

namespace MessagerSolution.Helper
{
    public class HostSessionCookieManager
    {
        #region LOAD HOSTS COOKIES
        public static void LoadHostCookies()
        {
            SiteConstants.hostAccounts.Clear();

            using (var db = new ApplicationDbContext())
            {
                var result = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select h).ToList();
                foreach (var item in result)
                {
                    HostAccount h = new HostAccount();
                    h.Id = item.Id;
                    h.SiteHostId = item.SiteHostId;
                    h.Username = item.Username;
                    h.Password = Security.Decrypt(item.Password);
                    h.SiteType = item.SiteType == 1 ? SiteType.Airbnb : item.SiteType == 2 ? SiteType.VRBO : 0;

                    var hostSessionCookie = db.HostSessionCookies.Where(x => x.HostId == item.Id && x.IsActive).FirstOrDefault();
                    if (result != null) // already have session cookies saved in db
                    {
                        if (h.SiteType == SiteType.Airbnb)
                        {
                            using (Airbnb.ScrapeManager sm = new Airbnb.ScrapeManager())
                            {
                                var isloaded = sm.ValidateTokenAndLoadCookies(hostSessionCookie.Token);
                                if (isloaded) // check if token is valid
                                {
                                    h.SessionToken = hostSessionCookie.Token;
                                    SiteConstants.hostAccounts.Add(h);
                                }
                                else // generate new token if not valid
                                {
                                    db.HostSessionCookies.RemoveRange(db.HostSessionCookies.Where(x => x.HostId == item.Id).ToList());
                                    db.SaveChanges();
                                    var loginResult = sm.Login(h.Username, h.Password, true);
                                    if (loginResult.Success)
                                    {
                                        h.SessionToken = loginResult.SessionToken;
                                        SiteConstants.hostAccounts.Add(h);
                                    }
                                }
                            }
                        }
                        else if (h.SiteType == SiteType.VRBO)
                        {
                            using(VRBO.ScrapeManager sm = new VRBO.ScrapeManager())
                            {

                            }
                        }
                    }
                    else // cookies not yet saved to db 
                    {
                        if (h.SiteType == SiteType.Airbnb)
                        {
                            using (Airbnb.ScrapeManager sm = new Airbnb.ScrapeManager())
                            {
                                var loginResult = sm.Login(h.Username, h.Password, true);
                                if (loginResult.Success)
                                {
                                    h.SessionToken = loginResult.SessionToken;
                                    SiteConstants.hostAccounts.Add(h);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region CHECK IF HAS VALID SESSION COOKIE
        public static bool HasValidSession(string hostId)
        {
            var result = SiteConstants.hostAccounts.Where(x => x.SiteHostId == hostId).FirstOrDefault();
            return (result != null);
        }
        public static bool HasValidSession(int hostId)
        {
            var result = SiteConstants.hostAccounts.Where(x => x.Id == hostId).FirstOrDefault();
            return (result != null);
        }
        #endregion

        #region GET SESSION COOKIE
        public static string GetHostSessionToken(string hostId)
        {
            var result = SiteConstants.hostAccounts.Where(x => x.SiteHostId == hostId).FirstOrDefault();
            return (result != null) ? result.SessionToken : string.Empty;
        }
        public static string GetHostSessionToken(int hostId)
        {
            var result = SiteConstants.hostAccounts.Where(x => x.Id == hostId).FirstOrDefault();
            return (result != null) ? result.SessionToken : string.Empty;
        }
        #endregion

        #region ADD HOST COOKIE
        public static bool Add(HostAccount account)
        {
            var temp = SiteConstants.hostAccounts.Where(x => x.Id == account.Id).FirstOrDefault();
            if (temp == null)
            {
                SiteConstants.hostAccounts.Add(account);
                return true;
            }
            return false;
        }
        #endregion
    }
}