﻿//using Core.Database.Context;
//using Core.Database.Entity;
//using MessagerSolution.Context;
//using MessagerSolution.Entity;
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;

//namespace MessagerSolution.Helper
//{
//    public class AutomatedResponse
//    {
//        #region FIRST MESSAGE AND UPON INQUIRY - DONE - TESTED
//        public static void FirstMessageAndUponInquiry(ScrapeManager sm, ApplicationDbContext db, string token, List<Inbox> scrapedInbox)
//        {
//            try
//            {
//                var unreadInbox = (from i in scrapedInbox
//                                   where (i.Status == "I" || i.Status == "B")
//                                   select i).ToList();

//                foreach (var inbox in unreadInbox)
//                {
//                    var messages = (from m in db.Messages
//                                    where m.ThreadId == inbox.ThreadId
//                                    select m).ToList();

//                    var sent = (from s in messages
//                                where s.IsMyMessage == true
//                                select s).ToList();

//                    var received = (from r in messages
//                                    where r.IsMyMessage == false
//                                    select r).ToList();

//                    if (sent.Count == 0 && received.Count == 1)
//                    {
//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        join p in db.Properties on inbox.PropertyId equals p.PropertyId
//                                        where r.PropertyId == p.Id && r.Type == (inbox.Status == "I" ? 6 : inbox.Status == "B" ? 1 : 0)
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inbox.ThreadId.ToString());

//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            sm.SendMessageToInbox(token, inbox.ThreadId.ToString(), message);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region BEFORE CHECK IN - DONE - TESTED
//        public static void BeforeCheckIn(ScrapeManager sm, ApplicationDbContext db, string token, List<Inquiry> inquiries)
//        {
//            try
//            {
//                foreach (var inquiry in inquiries)
//                {
//                    var isSent = (from d in db.DeliveredAutomatedMessage
//                                  where d.EventType == 2 && d.InquiryId == inquiry.InquiryId && d.IsSent
//                                  select d).FirstOrDefault() != null;

//                    if (isSent == false)
//                    {
//                        DateTime dateNow = DateTime.Now;
//                        DateTime checkInDate = inquiry.CheckInDate.Value;
//                        TimeSpan diff = checkInDate.Subtract(dateNow);

//                        int localPropertyId = Airbnb.API.Property.GetLocalId(db, inquiry.PropertyId);

//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == localPropertyId && r.Type == 2 && r.DateDifference == diff.Days
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());

//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            var isAutoReplySent = sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message).Success;
//                            if (isAutoReplySent)
//                            {
//                                DeliveredAutomatedMessage automatedMessage = new DeliveredAutomatedMessage
//                                {
//                                    InquiryId = inquiry.InquiryId,
//                                    EventType = 2,
//                                    IsSent = true
//                                };
//                                db.DeliveredAutomatedMessage.Add(automatedMessage);
//                                db.SaveChanges();
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region AFTER CHECK IN - DONE
//        public static void AfterCheckIn(ScrapeManager sm, ApplicationDbContext db, string token, List<Inquiry> inquiries)
//        {
//            try
//            {
//                foreach (var inquiry in inquiries)
//                {
//                    var isSent = (from d in db.DeliveredAutomatedMessage
//                                  where d.EventType == 3 && d.InquiryId == inquiry.InquiryId && d.IsSent
//                                  select d).FirstOrDefault() != null;

//                    if (isSent == false)
//                    {
//                        DateTime dateNow = DateTime.Now;
//                        DateTime checkInDate = inquiry.CheckInDate.Value;
//                        TimeSpan diff = dateNow.Subtract(checkInDate);

//                        int localPropertyId = Airbnb.API.Property.GetLocalId(db, inquiry.PropertyId);

//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == localPropertyId && r.Type == 3 && r.DateDifference == diff.Days
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());
//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            var isAutoReplySent = sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message).Success;
//                            if (isAutoReplySent)
//                            {
//                                DeliveredAutomatedMessage automatedMessage = new DeliveredAutomatedMessage
//                                {
//                                    InquiryId = inquiry.InquiryId,
//                                    EventType = 3,
//                                    IsSent = true
//                                };
//                                db.DeliveredAutomatedMessage.Add(automatedMessage);
//                                db.SaveChanges();
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region BEFORE CHECK OUT - DONE - TESTED
//        public static void BeforeCheckOut(ScrapeManager sm, ApplicationDbContext db, string token, List<Inquiry> inquiries)
//        {
//            try
//            {
//                foreach (var inquiry in inquiries)
//                {
//                    var isSent = (from d in db.DeliveredAutomatedMessage
//                                  where d.EventType == 4 && d.InquiryId == inquiry.InquiryId && d.IsSent
//                                  select d).FirstOrDefault() != null;

//                    if (isSent == false)
//                    {
//                        DateTime dateNow = DateTime.Now;
//                        DateTime checkOutDate = inquiry.CheckOutDate.Value;
//                        TimeSpan diff = checkOutDate.Subtract(dateNow);

//                        int localPropertyId = Airbnb.API.Property.GetLocalId(db, inquiry.PropertyId);

//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == localPropertyId && r.Type == 4 && r.DateDifference == diff.Days
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());

//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            var isAutoReplySent = sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message).Success;
//                            if (isAutoReplySent)
//                            {
//                                DeliveredAutomatedMessage automatedMessage = new DeliveredAutomatedMessage
//                                {
//                                    InquiryId = inquiry.InquiryId,
//                                    EventType = 4,
//                                    IsSent = true
//                                };
//                                db.DeliveredAutomatedMessage.Add(automatedMessage);
//                                db.SaveChanges();
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region AFTER CHECK OUT - TESTED
//        public static void AfterCheckOut(ScrapeManager sm, ApplicationDbContext db, string token, List<Inquiry> inquiries)
//        {
//            try
//            {
//                foreach (var inquiry in inquiries)
//                {
//                    var isSent = (from d in db.DeliveredAutomatedMessage
//                                  where d.EventType == 5 && d.InquiryId == inquiry.InquiryId && d.IsSent
//                                  select d).FirstOrDefault() != null;

//                    if (isSent == false)
//                    {
//                        DateTime dateNow = DateTime.Now;
//                        DateTime checkOutDate = inquiry.CheckOutDate.Value;
//                        TimeSpan diff = dateNow.Subtract(checkOutDate);

//                        Trace.WriteLine(diff.Days);
//                        Trace.WriteLine(inquiry.PropertyId);

//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == inquiry.PropertyId && r.Type == 5 && diff.Days >= 1 && diff.Days <= 7
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());
//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            var isAutoReplySent = sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message).Success;
//                            if (isAutoReplySent)
//                            {
//                                DeliveredAutomatedMessage automatedMessage = new DeliveredAutomatedMessage
//                                {
//                                    InquiryId = inquiry.InquiryId,
//                                    EventType = 5,
//                                    IsSent = true
//                                };
//                                db.DeliveredAutomatedMessage.Add(automatedMessage);
//                                db.SaveChanges();
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region CONFIRM RESERVATION - DONE
//        public static void ConfirmReservation(ScrapeManager sm, string token, string reservationCode)
//        {
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    var inquiry = (from i in db.Inquiries where i.ConfirmationCode == reservationCode select i).FirstOrDefault();
//                    if (inquiry != null)
//                    {
//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == inquiry.PropertyId && r.Type == 7
//                                        select t)
//                                        .FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());
//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };
//                            // Replace                              
//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region ALTER RESERVATION - DONE - TESTED
//        public static void AlterReservation(ScrapeManager sm, string token, string reservationCode)
//        {
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    var inquiry = (from i in db.Inquiries where i.ConfirmationCode == reservationCode select i).FirstOrDefault();
//                    if (inquiry != null)
//                    {
//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == inquiry.PropertyId && r.Type == 9
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());

//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region DECLINE RESERVATION - DONE
//        public static void Cancellation(ScrapeManager sm, string token, string threadId)
//        {
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    var inquiry = (from i in db.Inquiries where i.ThreadId == threadId select i).FirstOrDefault();
//                    if (inquiry != null)
//                    {
//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == inquiry.PropertyId && r.Type == 8
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, inquiry.ThreadId.ToString());
//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };
//                            // Replace                              
//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            sm.SendMessageToInbox(token, inquiry.ThreadId.ToString(), message);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion

//        #region PRE-APPROVE - DONE
//        public static void PreApproval(ScrapeManager sm, string token, string threadId)
//        {
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    var inquiry = (from i in db.Inquiries where i.ThreadId == threadId select i).FirstOrDefault();
//                    if (inquiry != null)
//                    {
//                        var template = (from r in db.MessageRules
//                                        join t in db.TemplateMessages on r.TemplateMessageId equals t.Id
//                                        where r.PropertyId == inquiry.PropertyId && r.Type == 10
//                                        select t).FirstOrDefault();

//                        if (template != null)
//                        {
//                            var variables = sm.MessageThreadMetadata(token, threadId.ToString());
//                            var replacements = new Dictionary<string, string>
//                            {
//                                { "[GuestName]", variables.GuestName },
//                                { "[GuestEmail]", variables.GuestEmail },
//                                { "[GuestPhone]", variables.GuestPhone },
//                                { "[Check-In]", variables.Checkin },
//                                { "[Check-Out]", variables.Checkout },
//                                { "[ConfirmationCode]", variables.ConfirmationCode },
//                                { "[NoOfGuest]", variables.NoOfGuests },
//                                { "[ListingName]", variables.ListingName },
//                                { "[ListingAddress]", variables.ListingAddress },
//                                { "[CleaningFee]", variables.CleaningFee },
//                                { "[Subtotal]", variables.Subtotal },
//                                { "[YouEarn]", variables.YouEarn }
//                            };

//                            var message = template.Message;
//                            foreach (var replacement in replacements)
//                            {
//                                message = message.Replace(replacement.Key, replacement.Value);
//                            }
//                            sm.SendMessageToInbox(token, threadId.ToString(), message);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }
//        #endregion
//    }
//}