﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Database.Entity;
using SignRequest.Api;
using SignRequest.Client;
using SignRequest.Model;
using SignRequest;
using System.Net;

namespace MessagerSolution.Helper.SignRequestSettings
{
    public class SignRequestSettings
    {
        public string SignRequestApiToken => Properties.Settings.Default.SignRequestApiToken;

        public string DefaultSender => Properties.Settings.Default.DefaultSender;

        public string ServerPath = "";
    }
    public class SignRequestService
    {
        private readonly SignRequestSettings _signRequestSettings;

        public SignRequestService(SignRequestSettings signRequestSettings)
        {
            _signRequestSettings = signRequestSettings;
        }
        public Document CreateDocumentFromUrl(string url)
        {
            try
            {
                Configuration.Default.AddApiKey("Authorization", _signRequestSettings.SignRequestApiToken);
                Configuration.Default.AddApiKeyPrefix("Authorization", "Token");

                var apiInstance = new DocumentsApi();
                var data = new Document { FileFromUrl = url };
                var result = apiInstance.DocumentsCreate(data);
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public SignRequestQuickCreate CreateSignRequest(string content, string fileName, string signerEmail, Host host)
        {
            try
            {
                Configuration.Default.AddApiKey("Authorization", _signRequestSettings.SignRequestApiToken);
                Configuration.Default.AddApiKeyPrefix("Authorization", "Token");
                Configuration.Default.Username = "Kendrick";
                Configuration.Default.UserAgent = "Khoe";
                var apiInstance = new SignrequestQuickCreateApi();
                var data = new SignRequestQuickCreate(
                    fromEmail: host.Username,
                    signers: new List<Signer> { new Signer(email: signerEmail) }
                )
                { FileFromContent = content, FileFromContentName = fileName, Subject = "Rental Contract", FromEmail = host.Username, FromEmailName = "Rental Contract" };
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var result = apiInstance.SignrequestQuickCreateCreate(data);

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public SignRequest.Model.SignRequest GetSignRequest(string uuid)
        {
            try
            {
                Configuration.Default.AddApiKey("Authorization", _signRequestSettings.SignRequestApiToken);
                Configuration.Default.AddApiKeyPrefix("Authorization", "Token");

                var apiInstance = new SignrequestsApi();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var result = apiInstance.SignrequestsRead(uuid);

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Document GetDocumentByUiid(string uuid)
        {
            try
            {
                Configuration.Default.AddApiKey("Authorization", _signRequestSettings.SignRequestApiToken);
                Configuration.Default.AddApiKeyPrefix("Authorization", "Token");

                var apiInstance = new DocumentsApi();
                var result = apiInstance.DocumentsRead(uuid);
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}