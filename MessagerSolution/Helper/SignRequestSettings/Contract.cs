﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Helper.SignRequestSettings
{
    public class Contract
    {
        private SignRequestService _signRequestService = new SignRequestService(new SignRequestSettings() { ServerPath = "~/Contracts/" });
        
        public void Add(ContractTemplate contract)
        {
            using(var db = new ApplicationDbContext())
            {
                try
                {
                    db.ContractTemplates.Add(contract);
                    db.SaveChanges();
                }
                catch(Exception e)
                {

                }

            }
        }
        public bool Send(int bookingId,int documentId, string Email,int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var reservation = db.Inquiries.Where(x => x.Id== bookingId).FirstOrDefault();
                var property = db.Properties.Where(x => x.ListingId == reservation.PropertyId).FirstOrDefault();
                var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                var guest = db.Guests.Where(x => x.Id == reservation.GuestId).FirstOrDefault();
                var host = db.Hosts.Where(x => x.Id == reservation.HostId).FirstOrDefault();
                var template = db.ContractDocuments.Where(x => x.Id == documentId).FirstOrDefault();
                Core.API.AllSite.Guests.SaveEmail(guest.GuestId, Email);
                
                var fileLocation = System.Web.HttpContext.Current.Server.MapPath("~" + template.AttachmentPath);
                var bytes = System.IO.File.ReadAllBytes(fileLocation);
                var file = Convert.ToBase64String(bytes);

                var signRequest = _signRequestService.CreateSignRequest(file, template.AttachmentPath,
                     Email, host);

                reservation.SignRequestUiid = signRequest.Uuid;
                db.Entry(reservation).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
        }
    }
}