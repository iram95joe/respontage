﻿namespace MessagerSolution.Helper
{
    public class Config
    {
        public const int TotalItemShowPerPage = 10;
        public const string ForwardArrow = ">";
    }
}