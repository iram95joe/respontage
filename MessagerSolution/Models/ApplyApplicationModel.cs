﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Models
{
    public class ApplyApplicationModel
    {
        public int PropertyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Income { get; set; }
        public int ContractDuration { get; set; }

        public List<string> ResidenceAddress { get; set; }
        public List<string> ResidenceFromDate { get; set; }
        public List<string> ResidenceToDate { get; set; }
        public List<string> ResidenceContactPerson { get; set; }
        public List<string> ResidenceContactNumber { get; set; }

        public List<string> JobCompanyName { get; set; }
        public List<string> JobCompanyAddress { get; set; }
        public List<string> JobWebsite { get; set; }
        public List<string> JobFromDate { get; set; }
        public List<string> JobToDate { get; set; }
        public List<string> JobContactPerson { get; set; }
        public List<string> JobContactNumber { get; set; }
    }
}