﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Rentals
{
    public class WorkerExpenseMaintenancePaymentViewModel
    {
        public PropertyScheduledExpense Maintenance { get; set; }
        public Vendor Vendor { get; set; }
        public Core.Database.Entity.Worker Worker { get; set; }
        public WorkerJobType JobType { get; set; }
        public ExpenseCategory ExpenseCategory{get;set;}
        public PaymentMethod PaymentMethod { get; set; }


    }
}