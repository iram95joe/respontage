﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Rentals
{
    public class PropertyMaintenanceViewModel
    {
        public Property Property { get; set; }
        public List<WorkerExpenseMaintenancePaymentViewModel> Maintenances { set; get; }
        public List<WorkerExpenseMaintenancePaymentViewModel> MonthlyExpenses { set; get; }
        public List<IncomeDetail> Incomes { get; set; }
        public List<Core.Database.Entity.Expense> Expenses { get; set; }
        //public List<IncomeDetail> ScheduledIncomes { get; set; }
        //public List<Core.Database.Entity.Expense> ScheduledExpenses { get; set; }
        public List<MaintenanceEntryViewModel> MaintenanceEntries { get; set; }

    }
}