﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Rentals
{
    public class RenterDocumentsViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Files { get; set; }
        public string Actions => "";
    }
}