﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Rentals
{
    public class MaintenanceEntryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DueDate{get;set;}
        public decimal Amount { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }
        public int PropertyId { get; set; }
        public int WorkerId { get; set; }
        public int JobTypeId { get; set; }
        public int VendorId { get; set; }

    }
}