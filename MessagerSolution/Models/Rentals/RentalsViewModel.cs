﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Rentals
{
    public class RentalsViewModel
    {
        public string Property { get; set; }
        public string Guest { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }
    }
}