﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Rentals
{
    public class RentalContractModel
    {
        public int BookingId { get; set; }
        public string Details { get; set; }
    }
}