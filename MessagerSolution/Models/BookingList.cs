﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class BookingList
    {
        public int Id { get; set; }
        public int? HostingAccountId { get; set; }
        public string ThreadId { get; set; }
        public string ConfirmationCode { get; set; }
        public int PropertyId { get; set; }
        public int ParentPropertyId { get; set; }
        public string PropertyName { get; set; }
        public string Site { get; set; }
        public string GuestId { get; set; }
        public string GuestName { get; set; }
        public DateTime ?InquiryDate { get; set; }
        public DateTime ?BookingDate { get; set; }
        public DateTime ?CheckInDate { get; set; }
        public DateTime ?CheckOutDate { get; set; }
        public string ReservationCost { get; set; }
        public string TotalPayout { get; set; }
        public string Payments { get; set; }
        public string Balance { get; set; }
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }
        public bool IsAltered { get; set; }
    }
}