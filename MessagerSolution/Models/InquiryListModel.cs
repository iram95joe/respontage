﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Database.Entity;

namespace MessagerSolution.Models
{
    public class InquiryListModel
    {
        public Inquiry Inquiry { get; set; }
        public Property Property { get; set; }
        public Guest Guest { get; set; }
    }
}