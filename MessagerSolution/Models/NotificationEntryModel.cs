﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class NotificationEntryModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Property { get; set; }
        public DateTime Date { get; set; }
        public string ScheduleType { get; set; }
        public int Status { get; set; }
        public string Actions =>/*"<div class=\"ui mini buttons\">"+*/
                                  "<button class=\"ui mini blue update-notification-status\" data-tooltip=\"Ongoing\" status-value=\"1\"><i class=\"tasks icon\"></i></button>" +
                                  "<button class=\"ui mini green update-notification-status\" data-tooltip=\"Complete\" status-value=\"2\"><i class=\"check circle outline icon\"></i></button>" +
                                  "<button class=\"ui mini red update-notification-status\" data-tooltip=\"Ignore\" status-value=\"3\"><i class=\"ban icon\"></i></button>";
                                  /*"</div>"*/
    }
}