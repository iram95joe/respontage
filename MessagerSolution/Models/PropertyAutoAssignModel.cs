﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class PropertyAutoAssignModel
    {
        public Core.Database.Entity.Property Property { get; set; }
        public Core.Database.Entity.Host Host { get; set; }
        public List<AssignWorker> Worker = new List<AssignWorker>();
        public List<PropertyAutoAssignModel> ChildAutoAssignModel = new List<PropertyAutoAssignModel>();
    }
    public class AssignWorker {
        public int Id { get; set; }
        public Core.Database.Entity.Worker Worker { get; set; }
        public Core.Database.Entity.WorkerJobType JobType { get; set; }
        public TimeSpan? TimeStart { get; set; }
        public TimeSpan? TimeEnd { get; set; }
        public int ?Type{get;set;}
        public bool IsAutoDelete { get; set; }
    }
    public class WorkerAndJobTypeModal
    {
        public Core.Database.Entity.WorkerJobType JobType { get; set; }
        public Core.Database.Entity.Worker Worker { get; set; }

    }
}