﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class UserRoleModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int CompanyId { get; set; }
        public string TimeZone { get; set; }
        public bool ConfirmEmail { get; set; }
        public bool ValidStatus { get; set; }
        public bool Status { get; set; }
        public int[] RoleId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CompanyType { get; set; }
        public string CompanyName { get; set; }
        public string PropertyIds { get; set; }
        public string CreditCardNonce { get; set; }
        public int SubscriptionPlanId { get; set; }
    }
}