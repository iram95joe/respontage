﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class LoginUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int CompanyId { get; set; }
        public bool ConfirmEmail { get; set; }
        public bool ValidStatus { get; set; }
        public bool Status { get; set; }
        public string RoleName { get; set; }
    }
}