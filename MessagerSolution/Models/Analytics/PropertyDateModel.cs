﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Analytics
{
    public class PropertyDateModel
    {
        public int Id { get; set; }
        public string Checkbox { get; set; }
        public string Date { get; set; }
        public bool IsAvailable { get; set; }
        public string Availability { get; set; }
        public double Price { get; set; }
        public string Actions { get; set; }
        public string RulesApplied { get; set; }
        public string SuggestedAmount { get; set; }
    }
}