﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Devices
{
    public class VideoViewModel
    {
        public DeviceAccount Account { get; set; }
        public Video Video { get; set; }
        public Device Device{ get; set; }
    }
}