﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Database.Entity;
namespace MessagerSolution.Models.Devices
{
    public class DevicesViewModel
    {
        public DeviceAccount Account { get; set; }
        public List<Device> Devices { get; set; }
    }
}