﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Devices
{
    public class VeraPinCode
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string PinCode { get; set; }
    }
}