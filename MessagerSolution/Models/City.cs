﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace MessagerSolution.Models
{
    public class City
    {
        public string Address { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}