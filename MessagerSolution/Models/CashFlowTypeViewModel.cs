﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MessagerSolution.Models
{
    public class CashFlowTypeViewModel
    {
        public int CashFlowTypeId { get; set; }
        public string CashFlowTypeName { get; set; }
    }
}