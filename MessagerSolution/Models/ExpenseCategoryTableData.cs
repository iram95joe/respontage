﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class ExpenseCategoryTableData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Actions { get; set; }
    }
}