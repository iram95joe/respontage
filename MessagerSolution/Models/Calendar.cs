﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class ResourceData
    {
        public string id { get; set; }
        public string title { get; set; }
        public List<ResourceChildData> children { get; set; }
    }

    public class ResourceChildData
    {
        public string id { get; set; }
        public string title { get; set; }
        public string eventColor { get; set; }
    }

    public class Event
    {
        public string Id { get; set; }
        public string RenterId { get; set; }
        public string PropertyId { get; set; }
        public string GuestName { get; set; }
        public string GuestCount { get; set; }
        public string InquiryDate { get; set; }
        public string InquiryPrice { get; set; }
    }

    public class EventData
    {
        public string eventId { get; set; }
        public string hostId { get; set; }
        public string threadId { get; set; }
        public string guestId { get; set; }
        public string resourceId { get; set; }
        public string scheduleId { get; set; }
        public string type { get; set; }
        public string noteType { get; set; }
        public string note { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public double price { get; set; }
        public bool isAvailable { get; set; }
        public string date { get; set; }
        public string propertyId { get; set; }
        public string propertyName { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int bedrooms { get; set; }
        public int siteType { get; set; }
        public string vrboResId { get; set; }
        public long? roomId { get; set; }
        public bool hasFootage { get; set; }
        public string timezoneId { get; set; }
        public string BookingStatus { get; set; }
        public int AssignmentStatus { get; set; }
    }
}