﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MessagerSolution.Models.Logs
{
    [DataContract]
    public class LogModel
    {
        public LogModel(string page,double percent)
        {
            this.Page = page;
            this.Percent = percent;
        }
       
        [DataMember(Name = "y")]
        public double Percent { get; set; }
        [DataMember(Name = "label")]
        public string Page { get; set; }
    }
}