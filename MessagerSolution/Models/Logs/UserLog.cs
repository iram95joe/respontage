﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Logs
{
    public class UserLog
    {
        public string Username { get; set; }
        public List<PageDetails> PagesVisited { get; set; }
    }
}