﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Logs
{
    public class PageDetails
    {
        public string Page { get; set; }
        public List<DateTime> DateAccess { get; set; }
    }
}