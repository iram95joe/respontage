﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Models
{
    public class PropertyListing
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MonthlyExpense { get; set; }
        public string Type { get; set; }
        public int ActiveBooking { get; set; }
        public string SyncParent { get; set; }
        public string AccountParent { get; set; }
        public string MultiUnitParent { get; set; }
        public string IsActive { get; set; }
        public string ActiveDateStart { get; set; }
        public string ActiveDateEnd { get; set; }
        public string Actions { get; set; }
        public string Site { get; set; }
        public string Host { get; set; }
    }
}