﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Reports.Cashflow
{
    public class SearchDateModel
    {
        public int DateFromMonth { get; set; }
        public int DateFromYear { get; set; }
        public int DateToMonth { get; set; }
        public int DateToYear { get; set; }
    }
}