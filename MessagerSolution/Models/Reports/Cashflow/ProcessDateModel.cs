﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Reports.Cashflow
{
    public class ProcessDateModel
    {
        public int CurrentMonth { get; set; }
        public int CurrentYear { get; set; }
        //public DateTime CurrentDate { get; set; }
    }
}