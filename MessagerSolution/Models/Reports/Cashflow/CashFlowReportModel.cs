﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Reports.Cashflow
{
    public class CashFlowReportModel
    {
        public string Property { get; set; }
        public DateTime Date { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal StartingCash { get; set; }
        public decimal NewInvestment { get; set; }
        public decimal PartnerInvestment { get; set; }
        public decimal WriteOff { get; set; }
        public decimal CashOut { get; set; }
        public decimal PartnerDraw { get; set; }
        public decimal PartnerInvestmentDraw { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalExpense { get; set; }
        public decimal ShareExpense { get; set; }
        public decimal NettCashFlow { get; set; }
        public decimal EndingCash { get; set; }
        public decimal TotalIncomeRefund { get; set; }
        public decimal TotalExpenseRefund { get; set; }
        public string NewInvestmentSummary { get; set; }
        public string PartnerInvestmentSummary { get; set; }
        public string WriteOffSummary { get; set; }
        public string CashOutSummary { get; set; }
        public string PartnerDrawSummary { get; set; }
        public string PartnerInvestmentDrawSummary { get; set; }
        public string IncomeSummary { get; set; }
        public string ExpenseSummary { get; set; }
        public decimal SuggestedStartingCash { get; set; }
    }
}