﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Reports.Commission
{
    public class CommissionReportViewModel
    {
        public string PropertyName { get; set; }
        public string CommissionType { get; set; }
        public decimal Unit { get; set; }
        public decimal NetProfit { get; set; }
        public decimal Total { get; set; }
    }
}