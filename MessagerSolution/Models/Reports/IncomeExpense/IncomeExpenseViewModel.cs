﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Reports.IncomeExpense
{
    public class IncomeExpenseViewModel
    {
        public string Property { get; set; }
        public DateTime Date { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal IncomeAmount { get; set; }
        public decimal ExpenseAmount { get; set; }
        public decimal IncomeRefund { get; set; }
        public decimal ExpenseRefund { get; set; }
        public decimal StartupCost { get; set; }
        public decimal StartupCostRefund { get; set; }
        //public decimal CapitalExpense { get; set; }
        public decimal OperatingNetProfit { get; set; }
        public decimal NetProfit { get; set; }

        public string IncomeSummary { get; set; }
        public string IncomeRefundSummary { get; set; }
        public string ExpenseSummary { get; set; }
        public string ExpenseRefundSummary { get; set; }
        public string StartUpCostSummary { get; set; }
        public string StartupCostRefundSummary { get; set; }
    }
}