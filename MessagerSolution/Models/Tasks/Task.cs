﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Tasks
{
    public class Task
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public HttpPostedFileWrapper Image { get; set; }
        public string ImageUrl { get; set; }
        public bool Status { get; set; }
        public List<SubTask> SubTasks { get; set; }
        public List<SubTask> SubTaskList = new List<SubTask>();


    }
}