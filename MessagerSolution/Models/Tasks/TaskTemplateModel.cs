﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Tasks
{
    public class TaskTemplateModel
    {
        public string Id { get; set; }
        public string Name{ get; set; }
        public string Type { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Task> TaskList = new List<Task>();

    }
}