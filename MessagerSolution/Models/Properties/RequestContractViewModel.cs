﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Properties
{
    public class RequestContractViewModel
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PropertyName { get; set; }
        public int PropertyId { get; set; }
        public DateTime RequestDate { get; set; }
        public int Status { get; set; }
        public string RenterName { get; set; }
        public string Actions => "<button class=\"ui mini blue view-request-booking\" data-tooltip=\"View\"><i class=\"edit icon\"></i></button>";
    }
}