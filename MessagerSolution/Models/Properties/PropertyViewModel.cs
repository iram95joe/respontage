﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Properties
{
    public class PropertyViewModel
    {
        public Property Property { get; set; }
        public List<int> ChildPropertyIds { get; set; }
        public List<PropertyFee> PropertyFees { get; set; }
    }
}