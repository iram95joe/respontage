﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Properties
{
    public class DisplayPropertyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Address { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int? Size { get; set; }
        public int Sleeps { get; set; }
        public List<string> Images { get; set; }
        public List<Amenity> Amenities { get; set; }
    }
}