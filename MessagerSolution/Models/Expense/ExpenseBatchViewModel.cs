﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Expense
{
    public class ExpenseBatchViewModel
    {
        public ExpenseBatchPayment ExpenseBatchPayment { get; set; }
        public List<BatchExpensePaymentViewModel> ExpensePaymentList { get; set; }
    }
}