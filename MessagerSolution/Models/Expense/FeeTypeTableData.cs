﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Expense
{
    public class FeeTypeTableData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string SiteType { get; set; }
        public string Actions { get; set; }
    }
}