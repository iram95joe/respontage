﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Expense
{
    public class ExpensePaymentViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public string ExpensesPaid { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }
    }
}