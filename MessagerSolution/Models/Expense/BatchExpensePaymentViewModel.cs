﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Expense
{
    public class BatchExpensePaymentViewModel
    {
        public int ExpenseId { get; set; }
        public int ExpensePaymentId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Balance { get; set; }
        public string Category { get; set; }
    }
}