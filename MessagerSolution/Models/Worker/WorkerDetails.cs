﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class WorkerDetails
    {
       public string Firstname { get; set; }
       public string Lastname { get; set; }
       public string Mobile { get; set; }
       public string Action { get; set; }
    }
}