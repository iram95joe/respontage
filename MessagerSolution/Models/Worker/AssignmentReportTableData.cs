﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class AssignmentReportTableData
    {
        public string WorkerName { get; set; }
        public string AssignmentStartDate { get; set; }
        public string AssignmentEndDate { get; set; }
        public string Property { get; set; }
        public string Job { get; set; }
        public string Unit { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public string InquiryId { get; set; }
        public string ThreadId { get; set; }
        public string HostId { get;set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string Checkbox { get; set; } = "<div class=\"ui checkbox\"><input class=\"worker-assignment-checkbox\" type=\"checkbox\"><label></label></div>";
        public int AssignmentId { get; set; }
        public string ExpenseStatus { get; set; }

    }
}