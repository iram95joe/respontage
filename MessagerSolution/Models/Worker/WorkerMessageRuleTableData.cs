﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class WorkerMessageRuleTableData
    {
        public int Id { get; set; }
        public string PropertyName { get; set; }
        public string Time { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Actions { get; set; }
    }
}