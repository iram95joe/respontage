﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class JobTypeTableData
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string ClassificationType { get; set; }  //Add by Danial 10-3-2018

        public string Actions { get; set; }
    }
}