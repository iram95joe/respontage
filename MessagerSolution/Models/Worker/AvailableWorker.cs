﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class AvailableWorker
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Job { get; set; }
        public string PaymentType { get; set; }
        public string PaymentRate { get; set; }
    }

    public class AvailableWorker2
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
    }
}