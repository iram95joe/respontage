﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class WorkerSummary
    {
        public Core.Database.Entity.Worker Worker { get; set; }
        public List<JobTypeSummary> JobType = new List<JobTypeSummary>();
        public string Retainer { get; set; }
        public string Fixed { get; set; }
    }
    public class JobTypeSummary
    {
        public string Name { get; set; }
        public string Total { get; set; }
        public string TotalReimbursement { get; set; }
    }
}