﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class ReimbursementReportTableData
    {
        public string Name { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public string Date { get; set; }
        public string InquiryId { get; set; }
        public string ThreadId { get; set; }
        public string HostId { get; set; }
        public int ReimbursementId { get; set; }
        public string Checkbox { get; set; } = "<div class=\"ui checkbox\"><input class=\"worker-reimbursement-checkbox\" type=\"checkbox\"><label></label></div>";
        public string ExpenseStatus { get; set; }
    }
}