﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Database.Entity;

namespace MessagerSolution.Models.Worker
{
    public class WorkerReportByJobType
    {
        public Core.Database.Entity.Worker Worker { get; set; }
        public List<JobTypeReport> JobTypReports = new List<JobTypeReport>();
    }
    public class JobTypeReport
    {
        public string JobType { get; set; }
        public decimal Amount { get; set; }
        public int Count { get; set; }
        public decimal Total { get; set; }
    }
}