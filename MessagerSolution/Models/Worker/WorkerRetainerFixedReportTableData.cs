﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class WorkerRetainerFixedReportTableData
    {
        public string Name {get;set;}
        public string Amount{get;set;}
        public string Date{get;set;}
        public string Type{get;set;}
        public bool HasExpense { get; set; }
        public string Checkbox { get; set; } = "<div class=\"ui checkbox\"><input class=\"fixed-retainer-checkbox\" type=\"checkbox\"><label></label></div>";
        public string ExpenseStatus { get; set; }
    }
}