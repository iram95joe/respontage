﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class RetainerExpenseModel
    {
        public string Worker{ get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
    }
}