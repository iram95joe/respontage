﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Worker
{
    public class WorkerAssignmentNoteModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Job { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Notes { get; set; }
        public int Status { get; set; }
        public string AssignmentStatus { get; set; }
        public string Action { get; set;}
        public string Description { get; set; }

    }
}