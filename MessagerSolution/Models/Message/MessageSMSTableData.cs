﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class MessageSMSTableData
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string Message { get; set; }
        public string Actions { get; set; }
        public string Checkbox { get; set; } = "<div class=\"ui checkbox\"><input class=\"unknown-number-checkbox\" type=\"checkbox\"><label></label></div>";
        public DateTime? CreatedDate { get; set; }
        public Core.Enumerations.CommunicationType Type {get;set;}
    }
}