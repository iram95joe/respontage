﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class BlockedPhoneNumberTable
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? BlockedDate { get; set; }
        public string Actions { get; set; }
    }
}