﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class MessageEntry
    {
        public long Id { get; set; }
        public string MessageId { get; set; }
        public string ThreadId { get; set; }
        public string MessageContent { get; set; }
        public bool IsMyMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }

        public int Type { get; set; }
        public string Source { get; set; }
        public string RecordingUrl { get; set; }
        public string ResourceSid { get; set; }
        public int? Duration { get; set; }
        public List<string> Images { get; set; }
    }
}