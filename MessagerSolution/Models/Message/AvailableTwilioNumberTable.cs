﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class AvailableTwilioNumberTable
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string FriendlyName { get; set; }
        public string Type { get; set; }
        public string BasePrice { get; set; }
        public string CurrentPrice { get; set; }
        public string VoiceEnabled { get; set; }
        public string SMSEnabled { get; set; }
        public string MMSEnabled { get; set; }
        public string FaxEnabled { get; set; }
        public string Actions { get; set; }
    }
}