﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class MessageRuleTableData
    {
        public int Id { get; set; }
        public string PropertyName { get; set; }
        public string EventType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Actions { get; set; }
    }
}