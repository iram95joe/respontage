﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class TwilioNumberTable
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string CallForwardingNumber { get; set; }
        public string CallLength { get; set; }
        public string VoicemailMessage { get; set; }
        public string CallforwardingMessage { get; set; }
        public string FriendlyName { get; set; }
        public string Type { get; set; }
        public string Capabilities { get; set; }
        public string Actions { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}