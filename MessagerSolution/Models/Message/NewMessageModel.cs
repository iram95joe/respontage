﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Message
{
    public class NewMessageModel
    {
        public string Name { get; set; }
        public string Image { set; get; }
        public string LastMessage { get; set; }
        public string ThreadId { get; set; }
        public DateTime? LastMessageAt { get; set; }
        public bool IsReply { get; set; }
        public bool IsEnd { get; set; }
        public int Type { get; set; }
    }
}