﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Invoice
{
    public class InvoiceScheduleViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string EmailTemplate { get; set; }
        public string InvoiceTemplate { get; set; }
        public string PaymentMethod { get; set; }
        public string Properties { get; set; }
        public string Actions => "<button class=\"ui mini button edit-invoice-schedule\" data-tooltip=\"Edit\"><i class=\"icon edit outline\"></i></button><button class=\"ui mini button delete-invoice-schedule\" data-tooltip=\"Delete\"><i class=\"icon trash\"></i></button>";
    }
    
}