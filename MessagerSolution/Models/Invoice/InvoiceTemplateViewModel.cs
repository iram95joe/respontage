﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Invoice
{
    public class InvoiceTemplateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Actions { get; set; }
    }
}