﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Invoice
{
    public class InvoiceEntryViewModel
    {
        public DateTime DateCreated { get; set; }
        public string Renter { get; set; }
        public string Property { get; set; }
        public string PaymentMethod { get; set; }
        public string File { get; set; }
        public string Status { get; set; }
    }
}