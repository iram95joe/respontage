﻿using Core.Models.Invoice;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Invoice
{
    public class InvoiceViewModel
    {
        public int BookingId { get; set; }
        public int InvoiceTemplateId { get; set; }
        public int EmailTemplateId { get; set; }
        public Core.Enumerations.PaymentRequestMethod PaymentMethod { get; set; }
        public string RequestPaymentJsonString { get; set; }
        public List<RequestAmountViewModel> RequestPayment => JsonConvert.DeserializeObject<List<RequestAmountViewModel>>(RequestPaymentJsonString);
        public int InvoiceScheduleId { get; set; }
    }
}