﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Invoice
{
    public class InvoicePaymentRequest
    {
        public int InvoiceId { get; set; }
        public string Renter { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime DateCreated { get; set; }
        public string MailSubject { get; set; }
        public Core.Enumerations.PaymentRequestMethod PaymentMethod { get; set; }
        public string CreditCardNonce { get; set; }
        public string Currency { get; set; }
    }
}