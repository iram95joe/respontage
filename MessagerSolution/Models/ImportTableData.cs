﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class ImportTableData
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Site { get; set; }
        public int SiteType { get; set; }
        public string Name { get; set; }
        public string AutoSync { get; set; }
        public string Actions { get; set; }
    }
}