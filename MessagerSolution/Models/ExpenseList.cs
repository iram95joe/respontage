﻿using Core.Database.Entity;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class ExpenseList
    {
        public int Id { get; set; }
        public int ExpenseCategoryId { get; set; }
        public int PropertyId { get; set; }
        public int PaymentMethodId { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Guest { get; set; }
        public string BookingCode { get; set; }
        public string StayDate { get; set; }
        public string FeeName { get; set; }
        public string Amount { get; set; }
        public string PropertyName { get; set; }
        public string Status { get; set; }
        public string DueDate { get; set; }
        public string PaymentDate { get; set; }
        public string CreatedDate { get; set; }
        public string Actions { get; set; }
        public string PaymentMethod { get; set; }
        public bool IsPaid { get; set; }
        public decimal Balance { get; set; }
        public List<ExpensePayment> Payments { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}