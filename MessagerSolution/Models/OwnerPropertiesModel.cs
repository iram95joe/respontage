﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class OwnerPropertiesModel
    {
        public int OwnerId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Properties { get; set; }
        public string Actions { get; set; } = "<button class=\"ui mini button edit-owner\" data-tooltip=\"Edit\">" +
                                        "<i class=\"icon edit\"></i></button>";
    }
}