﻿using Core.Database.Entity;
using System.Collections.Generic;

namespace MessagerSolution.Models
{
    public class TemplateListing
    {
        public int Id { get; set; }

        public string TemplateName { get; set; }

        public string Actions { get; set; }
    }
}