﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Income
{
    public class IncomeBatchViewModel
    {
        public IncomeBatchPayment IncomeBatchPayment { get; set; }
        public List<BatchIncomePaymentViewModel> IncomePaymentList { get; set; }
    }
}