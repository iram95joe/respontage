﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Income
{
    public class BatchIncomePaymentViewModel
    {
        public int IncomeId { get; set; }
        public int IncomePaymentId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Balance { get; set; }
    }
}