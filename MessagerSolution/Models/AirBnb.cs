﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    #region ACCOUNT
    public class AirbnbAccount
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SessionToken { get; set; }
    }
    #endregion

    #region SCRAPE TASK
    public class AirbnbScrapeTask
    {
        public bool IsAutoSync { get; set; }
        public bool IsSuccess { get; set; }
    }
    #endregion

    #region LOGIN
    public class LoginResult
    {
        public bool Success { get; set; }
        public string SessionToken { get; set; }
        public AirlockViewModel AirLock { get; set; }
    }
    #endregion

    #region AIRLOCK
    public class AirlockViewModel
    {
        public string Code { get; set; }
        public string LockId { get; set; }
        public string UserId { get; set; }
        public string PhoneId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string ProfileImageUrl { get; set; }
        public bool IsNew { get; set; }
        public AirlockChoice SelectedChoice { get; set; }
    }

    public enum AirlockChoice
    {
        SMS,
        Call,
        Email
    }
    #endregion

    #region MESSAGE
    public class InboxListing
    {
        public int HostAccountId { get; set; }
        public long ThreadId { get; set; }
        public bool HasUnread { get; set; }
        public bool IsIncoming { get; set; }
        public string GuestName { get; set; }
        public string GuestProfilePictureUrl { get; set; }
        public long? InquiryId { get; set; }
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
        public long GuestId { get; set; }
        public string LastMessage { get; set; }
        public DateTime LastMessageAt { get; set; }
    }

    public class MessageThreadDetail
    {
        public long? InquiryId { get; set; }
        public string PropertyName { get; set; }
        public int GuestCount { get; set; }
        public string GuestName { get; set; }
        public string GuestProfilePictureUrl { get; set; }
        public string Status { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string ConfirmationCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsAltered { get; set; }
        public bool IsSpecialOfferSent { get; set; }
        public string HostName { get; set; }
        public string HostPicture { get; set; }
    }
    #endregion

    #region SCRAPE RESULT
    public class ScrapeResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int TotalRecords { get; set; }
    }
    #endregion

    #region Property
    public class ListingDetail
    {
        public string Id { get; set; }
        public string ListingUrl { get; set; }
        public string Title { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string MinStay { get; set; }
        public string Internet { get; set; }
        public string Pets { get; set; }
        public string WheelChair { get; set; }
        public string Description { get; set; }
        public string Reviews { get; set; }
        public string Sleeps { get; set; }
        public string Bedrooms { get; set; }
        public string Bathrooms { get; set; }
        public string PropertyType { get; set; }
        public string AccomodationType { get; set; }
        public string Smoking { get; set; }
        public string AirCondition { get; set; }
        public string SwimmingPool { get; set; }

        public string PriceNightlyMin { get; set; }
        public string PriceNightlyMax { get; set; }
        public string PriceWeeklyMin { get; set; }
        public string PriceWeeklyMax { get; set; }
        public string Images { get; set; }
        public string Status { get; set; }

    }
    #endregion

    #region TRANSACTION HISTORY
    public class TransactionHistory
    {
        public TransactionHistory()
        {
            Reservations = new List<TransactionReservation>();
        }
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public string PayoutId { get; set; }

        public List<TransactionReservation> Reservations { get; set; }

    }
    public class TransactionReservation
    {
        public string ListingId { get; set; }
        public string ListingName { get; set; }
        public string GuestName { get; set; }
        public string ReservationCode { get; set; }
        public long ReservationId { get; set; }
    }
    #endregion

    #region SPECIAL OFFER
    public class SpecialOfferSummary
    {
        public bool IsSuccess { get; set; }
        public decimal Subtotal { get; set; }
        public decimal GuestPays { get; set; }
        public decimal HostEarns { get; set; }
        public bool IsAvailable { get; set; }
    }
    #endregion

    #region INBOX MESSAGE
    public class InboxMessage
    {
        public string Sender { get; set; }
        public string Message { get; set; }
        public string Timestamp { get; set; }
        public string ProfilePath { get; set; }
        public string ProfileImage { get; set; }
        public string MessageId { get; set; }
        public bool IsMyMessage { get; set; }
        public string ProfileId { get; set; }
    }
    #endregion
}