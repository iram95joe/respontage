﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Communication
{
    public class CommunicationUsageViewModel
    {
        public string Type { get; set; }
        public string Content { get; set; }
        public string To { get; set; }
        public string SendBy { get; set; }
    }
}