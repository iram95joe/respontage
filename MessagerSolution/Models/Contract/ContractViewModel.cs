﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Contract
{
    public class ContractViewModel
    {
        public int Id { get; set; }
        public string Actions { get; set; }
        public string Name { get; set; }
    }
}