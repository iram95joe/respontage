﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Videos
{
    public class VideoModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public string VideoUrl { get; set; }
        public string TimeZoneId { get; set; }
        public bool IsWatched { get; set; }
        public bool IsSaved { get; set; }
    }
}