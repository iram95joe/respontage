﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Videos
{
    public class PropertyVideosModel
    {
        public string PropertyName { get; set; }
        public List<VideoModel> Videos { get; set; } = new List<VideoModel>();
    }
}