﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Videos
{
    public class VideoResult
    {
        public string Filename { get; set; }
        public int InCount { get; set; }
        public int OutCount { get; set; }
    }
}