﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class ChildPropertyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SiteType { get; set; }
        public bool IsSyncProperty { get; set; }
        public bool IsParent { get; set; }
        public string Host { get; set; }
        public bool IsChild { get; set; }
    }
}