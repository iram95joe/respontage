﻿using System.Collections.Generic;

namespace MessagerSolution.Models
{
    public class TravelGroupInfo
    {
        public long Id { get; set; }
        public int InquiryId { get; set; }
        public string HashValue { get; set; }
        public string TotalPersonInGroup { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public string People { get; set; }
        public int TravelType { get; set; }
        public int NoteType { get; set; }  //In DB is store as NoteType (Note Type Enum Value)
        public string TravelDateTime { get; set; }
        public string AirportCodeArrival { get; set; }
        public string AirportCodeDeparture { get; set; }
        public string Airline { get; set; }
        public string FlightNumber { get; set; }
        public string CarType { get; set; }
        public string CarPlateNumber { get; set; }
        public string CarColor { get; set; }
        public string DepartureCity { get; set; }
        public string CruiseName { get; set; }
        public string CruiseDeparturePort { get; set; }
        public string CruiseArrivalPort { get; set; }
        public string Checkin { get; set; }
        public string Checkout { get; set; }
        public string Name { get; set; }                 //Use for Bus & Train note type ONLY
        public string TerminalStation { get; set; }
    }


    public class Response
    {
        public bool Status { get; set; }

        public List<TravelGroupInfo> TravelGroupInfoList { get; set; }
    }

    public class GetFormTravelEventResponse
    {
        public bool Status { get; set; }

        public TravelGroupInfo TravelGroupInfo { get; set; }
    }
}