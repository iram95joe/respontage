﻿using System.Collections.Generic;

namespace MessagerSolution.Models
{
    public class TravelEventFormViewModel
    {
        public List<TravelGroupInfo> TravelGroupInfoList { get; set; }

        public List<Core.Database.Entity.TravelType> ArrivalTravelTypes { get; set; }

        public List<Core.Database.Entity.TravelType> DepartureTravelTypes { get; set; }
    }
}