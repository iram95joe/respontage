﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Notices
{
    public class CreateTemplateModel
    {
        public int Id { get; set; }
        public string DocumentName { get; set; }
        public string Content { get; set; }
        public int PropertyId { get; set; }
    }
}