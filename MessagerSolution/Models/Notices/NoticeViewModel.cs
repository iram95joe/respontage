﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models.Notices
{
    public class NoticeViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string NoticeType { get; set; }
        public string NoticeDeliveryMethod { get; set; }
        public string Properties { get; set; }
        public string Renters { get; set; }
        public string DateOfNotice { get; set; }
        public List<string> AttachmentPaths { get; set; }
        public string Actions { get; set; }
    }
}