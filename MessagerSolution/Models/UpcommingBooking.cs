﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class UpcommingBooking
    {
        public int Id { get; set; }
        public string Property { get; set; }
        public string Site { get; set; }
        public string GuestName { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string Actions { get; set; }
    }
}