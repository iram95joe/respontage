﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class ExpensesValidation
    {
        public string Description { get; set; }
        public string ExpenseCategoryId { get; set; }
        public string ConfirmationCode { get; set; }
        public string FeeTypeId { get; set; }
        public string PropertyId { get; set; }
        public string Amount { get; set; }
        public string IsPaid { get; set; }
        public string PaymentMethodId { get; set; }
        public string DueDate { get; set; }
        public string DatePayment { get; set; }
        public string CompanyId { get; set; }
        public List<bool> ValidErrorStatus { get; set; }
        public List<string> ValidErrorMessage { get; set; }
    }
}