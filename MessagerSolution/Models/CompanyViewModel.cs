﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MessagerSolution.Models
{
    public class CompanyViewModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}