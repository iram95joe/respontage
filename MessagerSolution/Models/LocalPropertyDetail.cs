﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Database.Entity;

namespace MessagerSolution.Models
{
    public class LocalPropertyDetail
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyAddress { get; set; }

        public string PropertyLatitude { get; set; }
        public string PropertyLongitude { get; set; }

        public string PropertyHeadline { get; set; }
        public HttpPostedFileBase[] files { get; set; }
        public string PropertyDescription { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPhone { get; set; }

        public List<string> CategoryName { get; set; }
        public List<string> SubCategoryName { get; set; }
        public List<string> ControlType { get; set; }
        public List<string> SubCategoryContent { get; set; }
    }

    public class LocalPropertyPayment
    {
        public int? InboxId { get; set; }
        public int PropertyId { get; set; }
        public int? BookingId { get; set; }
        public DateTime CheckedInDate { get; set; }
        public DateTime? CheckedOutDate { get; set; }
        public bool isMonthToMonth { get; set; }
        public HttpPostedFileBase files { get; set; }
        public DateTime? RecurringRentStartDate { get; set; }
        public bool isLateFees { get; set; }
        public int? LateFeeApplyDays { get; set; }
        public decimal? LateFeeAmount { get; set; }

        public bool isSecurityDeposit { get; set; }
        public decimal? SecurityDepositAmount { get; set; }
        public DateTime? SecurityDepositDueOn { get; set; }

        public string AdditionalFeeString { get; set; }
        public string RentString { get; set; }
        public string PostDatedString { set; get; }
        public string RenterString { get; set; }
        public bool isPostDatedPayment { get; set; }
        public bool isAdvancePayment { get; set; }
        public int? AdvancePaymentMonth { get; set; }
        public bool isFixedMonthToMonth { get; set; }
        public bool isLastMonthPayment { get; set; }
        public int? LastPaymentMonth { get; set; }
        public List<IncomeDetail> Income { get; set; }
        public List<Core.Database.Entity.Expense> Expense { get; set; }
    }

    public class ApplicationList
    {
        public int ApplicationId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string GuestName { get; set; }
        public string ApplicationStatus { get; set; }

        public string Income { get; set; }
    }


    public class ResidenceList
    {
        public string ResidenceAddress { get; set; }
        public string ResidenceFromDate { get; set; }
        public string ResidenceToDate { get; set; }
        public string ResidenceContactPerson { get; set; }
        public string ResidenceContactNumber { get; set; }
    }
    public class JobList
    {
        public string JobCompanyName { get; set; }
        public string JobCompanyAddress { get; set; }
        public string JobWebsite { get; set; }
        public string JobFromDate { get; set; }
        public string JobToDate { get; set; }
        public string JobContactPerson { get; set; }
        public string JobContactNumber { get; set; }
    }
    public class ApplicationDetails
    {
        public int ApplicationId { get; set; }
        public int PropertyId { get; set; }
        public int GuestId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Income { get; set; }

        public List<Core.Database.Entity.JobHistory> JobList { get; set; }
        public List<Core.Database.Entity.ResidenceHistory> ResidenceList { get; set; }
    }

    public class IncomeDetail
    {
        public int Id { get; set; }
        public int IncomeTypeId { get; set; }
        public int BookingId { get; set; }
        public int PropertyId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal Payments { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateRecorded { get; set; }
        public bool IsBatchLoad { get; set; }
        public int CompanyId { get; set; }
        public bool IsActive { get; set; }
        public decimal RunningBalance { get; set; }
        public string PaymentDetails { get; set; }
        public string Actions { get; set; }
        public string SiteType { get; set; }
        public string BookingCode { get; set; }
        public string PropertyName { get; set; }
        public string GuestName { get; set; }
        public string PaymentDate { get; set; }
    }
    public class ContractHistoryDetail
    {
        public int? ContractId { get; set; }
        public int ApplicationId { get; set; }
        public int PropertyId { get; set; }
        public int GuestId { get; set; }
        public string MonthlyAmount { get; set; }
        public string StartDate { get; set; }
        public int ForMonth { get; set; }
        public string EndDate { get; set; }

        public List<string> MonthName { get; set; }
        public List<string> Amount { get; set; }
        public List<string> PaidStatus { get; set; }
    }
}