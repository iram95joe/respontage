﻿using System.Collections;

public class file_manager_in_lib
{
    public ArrayList trans_type_id;
    public ArrayList code;
    public ArrayList file_type;
    public ArrayList file_name;
    public ArrayList file_description;
    public ArrayList file_path;
    public ArrayList folder_name;
    public ArrayList created_by;
    public ArrayList date_created;
    public ArrayList warehouse_id;
    public ArrayList company_id;
    public file_manager_in_lib()
    {
        trans_type_id = new ArrayList();
        code = new ArrayList();
        file_type = new ArrayList();
        file_name = new ArrayList();
        file_description = new ArrayList();
        file_path = new ArrayList();
        folder_name = new ArrayList();
        created_by = new ArrayList();
        date_created = new ArrayList();
        warehouse_id = new ArrayList();
        company_id = new ArrayList();
    }
}
