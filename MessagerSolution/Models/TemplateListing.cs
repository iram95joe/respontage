﻿using Core.Database.Entity;
using System.Collections.Generic;

namespace MessagerSolution.Models
{
    public class TaskTemplateViewModel
    {
        public TaskTemplate TaskTemplate { get; set; }
        public List<Task> Tasks { get; set; }
    }

    public class InquiryTaskViewModel : TaskTemplateViewModel
    {
        public int TotalTaskCount { get; set; }
        public int CompletedTaskCount { get; set; }
        public int PendingTaskCount { get; set; }
    }

    public class InquiryTaskRequestModel
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int TaskStatus { get; set; }
    }

    public class InquiryCustomTaskRequestModel
    {
        public string TemplateName { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public int TaskStatus { get; set; }

    }
}