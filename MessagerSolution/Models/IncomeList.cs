﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MessagerSolution.Models
{
    public class IncomeList
    {
        public int Id { get; set; }
        //public string DateRecorded { get; set; }
        public string IncomeType { get; set; }
        public string SiteType { get; set; }
        public string BookingCode { get; set; }
        public string PropertyName { get; set; }
        public string GuestName { get; set; }
        public decimal Amount { get; set; }
        public decimal Receivables { get; set; }
        public string ReceivablesSummary { get; set; }
        public decimal Balance { get; set; }
        public string PaymentDate { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }
        public string Description { get; set; }
        public string DueDate { get; set; }
        //public Entity.Property Property { get; set; }
        //public Entity.Guest Guest { get; set; }
        //public Entity.Inquiry Booking { get; set; }

    }
}