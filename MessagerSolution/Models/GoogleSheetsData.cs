﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagerSolution.Models
{
    public class GoogleSheetsData
    {
        public string Name { get; set; }

        public string Feedback { get; set; }

        public string Photo { get; set; }
    }
}
