﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MessagerSolution.Models
{
    public class CashFlowViewModel
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public string PaymentDate { get; set; }
        public int CashFlowType { get; set; }
        public string CashFlowTypeName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string Description { get; set; }
        public string Actions { get; set; }
        public string Owner { get; set; }

    }
}