﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MessagerSolution.Models
{
    public class GroupMessage
    {
        public long ThreadId { get; set; }
        public int HostId { get; set; }
        public long? GuestId { get; set; }
        public string GuestName { get; set; }
        public string GuestPicture { get; set; }
        public bool IsIncoming { get; set; }
        public string Status { get; set; }
        public bool IsAltered { get; set; }
        public bool IsActive { get; set; }
        public string ReservationCode { get; set; }
        public DateTime? StartStayDate { get; set; }
        public string MessageContent { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool ReadStatus { get; set; }
    }

    public class RolePageUrl
    {
        public string PageUrl { get; set; }
        public string PageName { get; set; }
        public string IconPath { get; set; }
    }
}