﻿using MessagerSolution.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MessagerSolution.JobScheduler
{
    public class InvoiceJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" }
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public async static void Start(int companyId, System.Web.HttpContext context)
        {
            scheduler = await factory.GetScheduler();

            await scheduler.Start();
            var jobKey = new JobKey("Company" + companyId);
            if (await scheduler.CheckExists(jobKey) == false)
            {
                IJobDetail job = JobBuilder.Create<InvoiceJob>()
              .WithIdentity("Company" + companyId)
              .UsingJobData("CompanyId", companyId)
              .Build();
                job.JobDataMap.Put("HttpContext", context);
                ITrigger trigger = TriggerBuilder.Create()
                   .StartNow()
                  .WithSimpleSchedule(x => x.RepeatForever().WithIntervalInHours(24))
                     .Build();
                await scheduler.ScheduleJob(job, trigger);
            }


        }
        public static void Stop(string key)
        {
            try
            {
                scheduler.DeleteJob(new JobKey(key));
            }
            catch { }
        }
    }
}