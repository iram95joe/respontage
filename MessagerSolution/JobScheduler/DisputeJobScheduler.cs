﻿using MessagerSolution.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MessagerSolution.JobScheduler
{
    public class DisputeJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" }
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public async static void Start(int companyId)
        {
            Stop("Dispute" + companyId);
            scheduler = await factory.GetScheduler();

            await scheduler.Start();
            var jobKey = new JobKey("Dispute" + companyId);
            if (await scheduler.CheckExists(jobKey) == false)
            {
                IJobDetail job = JobBuilder.Create<DisputeJob>()
              .WithIdentity("Dispute" + companyId)
              .UsingJobData("CompanyId", companyId)
              .Build();
                ITrigger trigger = TriggerBuilder.Create()
                   .StartNow()
                  .WithSimpleSchedule(x => x.RepeatForever().WithIntervalInHours(6))
                     .Build();
                await scheduler.ScheduleJob(job, trigger);
            }
        }
        private static void Stop(string key)
        {
            try
            {
                scheduler.DeleteJob(new JobKey(key));
            }
            catch { }
        }
    }
}