﻿using Core.Database.Entity;
using MessagerSolution.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MessagerSolution.JobScheduler
{
    public class WorkerNotificationJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
            {"quartz.threadPool.threadCount", "1000"}

        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public static async void Start(WorkerMessageRule rule)
        {
            scheduler = await factory.GetScheduler();

            await scheduler.Start();
            var jobKey = new JobKey(rule.Id.ToString());
            if (await scheduler.CheckExists(jobKey) == false)
            {
                IJobDetail job = JobBuilder.Create<WorkerNotificationJob>()
                .UsingJobData("ruleId", rule.Id)
                .UsingJobData("propertyId", rule.PropertyId)
                .UsingJobData("templateId", rule.WorkerTemplateMessageId)
                .WithIdentity(rule.Id.ToString())
                .Build();

                var trigger = TriggerBuilder.Create()
             .WithDailyTimeIntervalSchedule(s => s
                 .OnEveryDay()
                 .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(rule.Time.Value.Hours, rule.Time.Value.Minutes)))
                //.EndingDailyAfterCount(1))
                .Build();
                await scheduler.ScheduleJob(job, trigger);
            }
        }

        public static void Stop(string key)
        {
            try
            {
                scheduler.DeleteJob(new JobKey(key));
            }
            catch { }
        }
    }
}