﻿using MessagerSolution.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MessagerSolution.JobScheduler
{
    public class GmailJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
              {"quartz.threadPool.threadCount", "1000" }
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public async static void Start(int companyId)
        {
            var key = "gmail" + companyId;
            Stop(key);
            scheduler = await factory.GetScheduler();

            await scheduler.Start();
            var jobKey = new JobKey(key);
            if (await scheduler.CheckExists(jobKey) == false)
            {
                IJobDetail job = JobBuilder.Create<GmailJob>()
                 .UsingJobData("companyId", companyId)
                .WithIdentity(key)
                .Build();

                ITrigger trigger = TriggerBuilder.Create()
                  .WithSimpleSchedule(x => x.RepeatForever().WithIntervalInHours(1))
                     .Build();
                await scheduler.ScheduleJob(job, trigger);
            }
        }

        private static void Stop(string key)
        {
            try
            {
                scheduler.DeleteJob(new JobKey(key));
            }
            catch (Exception e)
            {

            }
        }
    }
}