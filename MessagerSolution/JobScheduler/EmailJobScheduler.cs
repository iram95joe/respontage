﻿using MessagerSolution.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MessagerSolution.JobScheduler
{
    public class EmailJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" }
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public async static void Start(int emailId,int companyId)
        {
            scheduler = await factory.GetScheduler();
            var key = "email"+emailId;
            await scheduler.Start();
            var jobKey = new JobKey(key);
            if (await scheduler.CheckExists(jobKey) == false)
            {
                IJobDetail job = JobBuilder.Create<EmailJob>()
                .UsingJobData("emailId", emailId)
                .UsingJobData("companyId", companyId)
                .WithIdentity(key)
                .Build();

                ITrigger trigger = TriggerBuilder.Create().Build();

                await scheduler.ScheduleJob(job, trigger);
            }
        }

        public static void Stop(string key)
        {
            try
            {
                scheduler.DeleteJob(new JobKey(key));
            }
            catch { }
        }
    }
}