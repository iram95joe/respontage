﻿using MessagerSolution.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MessagerSolution.JobScheduler
{
    public class WyzeJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
              {"quartz.threadPool.threadCount", "1000" }
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public async static void Start(string key, string email, string password, int accountId, string Url,int companyId)
        {
            //Stop(key);
            scheduler = await factory.GetScheduler();

            await scheduler.Start();
            var jobKey = new JobKey(key);
            if (await scheduler.CheckExists(jobKey) == false)
            {
                IJobDetail job = JobBuilder.Create<WyzeJob>()
                .UsingJobData("email", email)
                .UsingJobData("password", password)
                .UsingJobData("accountId", accountId)
                 .UsingJobData("Url", Url)
                 .UsingJobData("companyId", companyId)
                .WithIdentity(key)
                .Build();

                ITrigger trigger = TriggerBuilder.Create()
                       .WithSimpleSchedule(x => x.RepeatForever().WithIntervalInMinutes(10))
                          .Build();
                //.WithDailyTimeIntervalSchedule
                //(
                //    s => s.WithIntervalInSeconds(60).OnEveryDay()
                //)
                //.Build();
                await scheduler.ScheduleJob(job, trigger);
            }
        }

        public static void Stop(string key)
        {
            try
            {
                scheduler.DeleteJob(new JobKey(key));
            }
            catch (Exception e)
            {

            }
        }
    }
}