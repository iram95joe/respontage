﻿using Core.Database.Context;
using Core.Database.Entity;
using MessagerSolution.Helper.SignRequestSettings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using TinyIoC;

namespace MessagerSolution.Tools
{
    public static class HangfireUtilities
    {
        public static void UpdateSignRequest()
        {
            var applicationDbContext = TinyIoCContainer.Current.Resolve<ApplicationDbContext>();
            var signRequestService = TinyIoCContainer.Current.Resolve<SignRequestService>();
            var settings = TinyIoCContainer.Current.Resolve<SignRequestSettings>();
            var rents = applicationDbContext.Inquiries.Where(filter => filter.IsDocumentSigned == false && filter.SignRequestUiid != null).ToList();

            foreach (var rent in rents)
            {
                var result = signRequestService.GetSignRequest(rent.SignRequestUiid);
                if (result != null)
                {
                    var documentStringUiid = result.Document.Replace("https://signrequest.com/api/v1/documents/", "").Replace("/", "");
                    var guest = applicationDbContext.Guests.Where(x => x.Id == rent.GuestId).FirstOrDefault();
                    var guestEmail = applicationDbContext.GuestEmails.Where(x => x.GuestId == guest.GuestId).FirstOrDefault();
                    if (guestEmail != null)
                    {
                        var signer = result.Signers.FirstOrDefault(filter => filter.Email == guestEmail.Email);
                        var document = signRequestService.GetDocumentByUiid(documentStringUiid);
                        if (signer?.Signed == null) continue;
                        if (!signer.Signed.Value) continue;
                        if (signer.SignedOn == null) continue;

                        rent.IsDocumentSigned = true;
                        var documentPath = "/Documents/SignedContracts/" + rent.ConfirmationCode + ".pdf";
                        rent.SignedDocumentDownloadLink = documentPath;
                        using (var client = new WebClient())
                        {
                            try
                            {
                                client.DownloadFile(document.Pdf, System.Web.HttpContext.Current.Server.MapPath(documentPath));
                                var contract = new ContractDocument() { BookingId = rent.Id, AttachmentPath = documentPath, Type = (int)Core.Enumerations.ContractType.Signed };
                                applicationDbContext.ContractDocuments.Add(contract);
                                applicationDbContext.SaveChanges();
                            }
                            catch (Exception e)
                            {

                            }
                        }
                        rent.DateTimeSigned = signer.SignedOn.Value.ToString(CultureInfo.InvariantCulture);
                        applicationDbContext.Entry(rent).State = EntityState.Modified;
                        applicationDbContext.SaveChanges();
                    }
                }
            }
        }
    }
}