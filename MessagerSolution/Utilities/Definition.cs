﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
//using System.Security.Cryptography.Xml;
using System.Web;
namespace MessagerSolution.Utilities
{
    public class Definition
    {
        public static class MonthValue
        {
            public const int January = 1;
            public const int February = 2;
            public const int March = 3;
            public const int April = 4;
            public const int May = 5;
            public const int June = 6;
            public const int July = 7;
            public const int August = 8;
            public const int September = 9;
            public const int October = 10;
            public const int November = 11;
            public const int December = 12;
        }

        public enum PaymentStatus
        {
            New = 1,
            Paid = 2
        }
        //public enum MonthValue
        //{
        //    January = 1,
        //    February = 2,
        //    March = 3,
        //    April = 4,
        //    May = 5,
        //    June = 6,
        //    July = 7,
        //    August = 8,
        //    September = 9,
        //    October = 10,
        //    November = 11,
        //    December = 12
        //}

        public static string Description(Enum value)
        {
            // variables  
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            // return  
            return attributes.Length == 0 ? value.ToString() : ((DescriptionAttribute)attributes[0]).Description;
        }
    }
}