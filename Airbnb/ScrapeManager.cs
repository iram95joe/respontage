﻿using BaseScrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Repository;
using Core.SignalR.Hubs;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Xml;
using Core.Database.Context;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using Core.Database.Entity;
using NLog;
using Airbnb.Models;
using Core.Helper;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Reflection;
using System.Data.Entity;
using Core.API.Airbnb;
using Core.Models;

namespace Airbnb
{
    public class ScrapeManager : CommunicationEngine, IDisposable
    {
        #region VARIABLES
        private CommunicationPage commPage = null;
        ICommonRepository objIList = new CommonRepository();
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        Logger logger = LogManager.GetCurrentClassLogger();
        static object waitLock = new object();
        #endregion

        #region CONSTRUCTOR
        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://airbnb.com");
        }
        #endregion

        public string Test(string file)
        {
            commPage.RequestURL = file;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            ProcessRequest(commPage);
            var html = commPage.Html;
            var start = html.IndexOf("<div");
            var to = html.IndexOf("</body>");
            return html.Substring(start, to - start);


        }

        #region LOGIN
        public string LoginWithApi(string username, string password)
        {
            string url = "https://api.airbnb.com/v1/authorize";
            commPage = new CommunicationPage(url);
            commPage.ReqType = CommunicationPage.RequestType.POST;
            var parms = new Dictionary<string, string>();
            parms.Add("client_id", "d306zoyjsyarp7ifhu67rjxn52tv0t20");
            parms.Add("locale", "en-US");
            parms.Add("currency", "USD");

            parms.Add("grant_type", "password");
            parms.Add("username", username);
            parms.Add("password", password);
            commPage.PersistCookies = true;
            commPage.Parameters = parms;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(commPage.Html);
                var token = ob["access_token"].ToSafeString();

                return token;
            }
            return string.Empty;
        }


        public LoginResult Login(string username, string password, bool forceLogin = false, bool checkAirlockOnly = false)
        {
            var result = new LoginResult();
            if (!forceLogin && commPage != null && commPage.IsLoggedIn)
            {
                result.Success = true;
                return result;
            }
            //string url = "https://www.airbnb.com/login";
            //if (clearPreviousSession)
            //    commPage = new CommunicationPage(url);
            //else
            //    commPage = InitializeCommunication(email, url);

            //var retryCount = 1;
            //Retry:

            string url = "https://www.airbnb.com/login";
            commPage = new CommunicationPage(url);
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                if (xd != null)
                {
                    var metadata = xd.SelectSingleNode("//input[@name='metadata1']").GetAttributeFromNode("value");
                    var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "class", "login-form ");
                    hiddenFields.Add("email", username.Trim());
                    hiddenFields.Add("password", password.Trim());
                    hiddenFields["utf8"] = "✓";
                    commPage.RequestURL = "https://www.airbnb.com/authenticate";
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "https://www.airbnb.com/login";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);

                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("dashboard") || commPage.Uri.AbsoluteUri.Contains("hosting"))
                    {
                        commPage.IsLoggedIn = true;
                        var tempCommPage = commPage;
                        tempCommPage.Html = "";

                        var cks = GetAllCookies(commPage.COOKIES);
                        var cookieList = cks.ToList();
                        var serializedCookies = JsonConvert.SerializeObject(cookieList);

                        if (!checkAirlockOnly)
                        {

                            var hostId = Core.API.Airbnb.Hosts.GetHostId(username, password);
                            var saveResult = Core.API.Airbnb.HostSessionCookies.Add(hostId, serializedCookies);
                            result.SessionToken = saveResult.HostSessionCookie.Token;
                        }

                        result.Success = true;
                        return result;
                    }
                    //else if ((int)commPage.StatusCode == 420)
                    //{
                    //    var ob = JObject.Parse(commPage.Html);
                    //    var captcha_site_key = ob["airlock"]["friction_data"][0]["data"]["site_key"].ToSafeString();
                    //    var lockId = ob["airlock"]["id"].ToSafeString();




                    //    var captchaResponse = Scrapper._2Captcha.SolveCaptcha(Core.Constants.CaptchaAPIKey, captcha_site_key, commPage.Uri.AbsoluteUri);
                    //    if (captchaResponse.Item1 == true)
                    //    {
                    //        ProcessCaptchaLock(new CaptchaViewModel { CaptchaResponse = captchaResponse.Item2, LockId = lockId, SiteKey = captcha_site_key, Email = email, Password = password });
                    //        if (retryCount < 3)
                    //        {
                    //            retryCount++;
                    //            goto ResponseCheck;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        ElmahLogger.LogInfo("Captcha not resolved Error: " + captchaResponse.Item2);
                    //    }

                    //}
                    else if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("airlock"))
                    {
                        try
                        {
                            xd = commPage.ToXml(true);
                            var userName = "";
                            var userProfileImageUrl = "";
                            var imgNode = xd.SelectSingleNode("//img[contains(@src,'profile_pic')]");
                            if (imgNode != null)
                            {
                                userName = imgNode.GetAttributeFromNode("alt");
                                userProfileImageUrl = imgNode.GetAttributeFromNode("src");
                            }

                            var json = xd.SelectSingleNode("//script[@data-hypernova-key='user_challengesbundlejs']").GetInnerTextFromNode().HtmlDecode().Trim();
                            if (!string.IsNullOrEmpty(json))
                            {
                                json = json.Replace("//\r\n", "");
                                json = json.Replace("<!--", "");
                                json = json.Replace("-->", "");
                                var ob = JObject.Parse(json);

                                var lockId = ob["bootstrapData"]["id"].ToSafeString();
                                var currentUserId = ob["bootstrapData"]["user_id"].ToSafeString();
                                var list = ob["bootstrapData"]["friction_data"].ToList();
                                var required = list.Where(f => f["name"].ToSafeString() == "phone_verification_via_text").FirstOrDefault();

                                var phoneNumId = required["data"]["phone_numbers"][0]["id"].ToSafeString();
                                var phoneNum = required["data"]["phone_numbers"][0]["obfuscated"].ToSafeString();
                                var emailNode = list.Where(f => f["name"].ToSafeString() == "email_code_verification").FirstOrDefault();
                                var Email = emailNode["data"]["obfuscated_email_address"].ToSafeString();
                                result.Success = false;
                                result.AirLock = new AirlockViewModel()
                                {
                                    LockId = lockId,
                                    PhoneId = phoneNumId,
                                    UserId = currentUserId,
                                    Email = Email,
                                    PhoneNumber = phoneNum,
                                    Name = userName,
                                    ProfileImageUrl = userProfileImageUrl,
                                    IsNew = true
                                };
                            }
                            else
                            {
                                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-airlock_data']");
                                var rawJson = meta.GetAttributeFromNode("content").ToSafeString();
                                json = meta.GetAttributeFromNode("content").HtmlDecode();
                                var ob = JObject.Parse(json);
                                var lockId = ob["id"].ToSafeString();
                                var currentUserId = ob["user_id"].ToSafeString();
                                var phoneNumId = ob["friction_init_data"]["phone_verification_via_text"]["phone_numbers"][0]["id"].ToSafeString();
                                var phoneNum = ob["friction_init_data"]["phone_verification_via_text"]["phone_numbers"][0]["obfuscated"].ToSafeString();
                                var Email = ob["friction_init_data"]["email_code_verification"]["obfuscated_email_address"].ToSafeString();
                                result.Success = false;
                                result.AirLock = new AirlockViewModel()
                                {
                                    LockId = lockId,
                                    PhoneId = phoneNumId,
                                    UserId = currentUserId,
                                    Email = Email,
                                    PhoneNumber = phoneNum,
                                    Name = userName,
                                    ProfileImageUrl = userProfileImageUrl,
                                    IsNew = false
                                };
                            }

                            return result;
                        }
                        catch (Exception e)
                        {
                            Trace.WriteLine(e.ToString());
                        }
                    }
                }
            }
            result.Success = false;
            return result;
        }
        #endregion

        #region PROCESS AIRLOCK
        public bool ProcessAirlockSelection(string lockId, string userId, string phoneId, int choice, bool isNew)
        {
            try
            {
                string airlockUrl = commPage.Uri.AbsoluteUri;
                var u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=v1", lockId);
                var tt = string.Format("{0}", lockId);
                var pJson = "";
                if (isNew)
                {
                    switch (choice)
                    {
                        case 1:
                            pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":\"{0}\"}}}},\"attempt\":true,\"enable_throw_errors\":true}}", phoneId);

                            break;
                        case 2:
                            pJson = string.Format("{{\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":\"{0}\"}}}},\"attempt\":true,\"enable_throw_errors\":true}}", phoneId);

                            break;
                        case 3:
                            pJson = "{\"friction\":\"email_code_verification\",\"attempt\":true,\"enable_throw_errors\":true}";
                            break;
                        default:
                            pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":\"{0}\"}}}},\"attempt\":true,\"enable_throw_errors\":true}}", phoneId);

                            break;
                    }
                }
                else
                {
                    switch (choice)
                    {

                        case 1:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{2}}}}}}}", lockId, userId, phoneId);
                            break;
                        case 2:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{2}}}}}}}", lockId, userId, phoneId);
                            break;
                        case 3:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"email_code_verification\",\"friction_data\":{{\"optionSelection\":{{}}}}}}", lockId, userId);
                            pJson = pJson.Replace("^^", "");
                            break;
                        default:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{2}}}}}}}", lockId, phoneId);
                            break;
                    }
                }

                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.RequestURL = u;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                CommPage.JsonStringToPost = pJson;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                CommPage.RequestHeaders.Add("X-CSRF-Token", token);
                commPage.Referer = airlockUrl;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Trace.WriteLine("Error in process airlock selection " + e.ToString());
            }
            return false;

        }

        public Tuple<bool, string> ProcessAirlock(string code, string lockId, string userId, string phoneId, int choice, bool isNew, string username, string password)
        {
            try
            {
                var u = "";
                if (isNew)
                    u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=v1", lockId);
                else
                    u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", lockId);

                var pJson = "";
                if (isNew == false)
                {
                    pJson = string.Format("{{\"user_id\":{2},\"action_name\":\"account_login\",\"id\":{0},\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{3}}},\"response\":{{\"code\":\"{1}\"}}}}}}", lockId, code, userId, phoneId);
                    if (choice == 3)
                    {
                        pJson = string.Format("{{\"user_id\":{2},\"action_name\":\"account_login\",\"id\":{0},\"friction\":\"email_code_verification\",\"friction_data\":{{ \"optionSelection\":{{}},\"response\":{{ \"code\":\"{1}\"}}}}}}", lockId, code, userId);

                    }
                    else if (choice == 2)
                    {
                        pJson = string.Format("{{\"user_id\":{2},\"action_name\":\"account_login\",\"id\":{0},\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{3}}},\"response\":{{\"code\":\"{1}\"}}}}}}", lockId, code, userId, phoneId);
                    }
                }

                else
                {
                    pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}},\"response\":{{\"code\":\"{1}\"}}}},\"enable_throw_errors\":true}}", phoneId, code);
                    if (choice == 3)
                    {
                        pJson = string.Format("{{\"friction\":\"email_code_verification\",\"friction_data\":{{\"response\":{{\"code\":\"{0}\"}}}},\"enable_throw_errors\":true}}", code);

                    }
                    else if (choice == 2)
                    {
                        pJson = string.Format("{{\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}},\"response\":{{\"code\":\"{1}\"}}}},\"enable_throw_errors\":true}}", phoneId, code);

                    }
                }
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();
                commPage.RequestURL = u;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                CommPage.JsonStringToPost = pJson;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                CommPage.RequestHeaders.Add("X-CSRF-Token", token);
                commPage.Referer = string.Format("https://www.airbnb.com/airlock?al_id={0}", lockId);//airlockUrl;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var result = false;
                    if (isNew)
                    {
                        result = ob["airlock"]["status"].ToInt() == 2;
                    }
                    else
                    {
                        result = ob["airlock"]["friction_statuses"]["phone_verification"].ToSafeString().ToBoolean() == true
                                  || ob["airlock"]["friction_statuses"]["phone_verification_via_text"].ToSafeString().ToBoolean() == true
                                  || ob["airlock"]["friction_statuses"]["phone_verification_via_call"].ToSafeString().ToBoolean() == true
                                  || ob["airlock"]["friction_statuses"]["email_code_verification"].ToSafeString().ToBoolean() == true;
                    }

                    if (result == true)
                    {
                        commPage.IsLoggedIn = true;

                        long siteHostId = long.Parse(userId);
                        var cks = GetAllCookies(commPage.COOKIES);
                        var cookieList = cks.ToList();
                        var serializedCookies = JsonConvert.SerializeObject(cookieList);
                        bool successScrapeHost = ScrapeGuestAccount(token, username, password, true);//ScrapeHostProfile(username, password, true);

                        if (successScrapeHost)
                        {
                            var hostId = Core.API.Airbnb.Hosts.GetHostId(username, password);
                            var saveResult = Core.API.Airbnb.HostSessionCookies.Add(hostId, serializedCookies);

                            return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);
                        }
                        else
                        {
                            return new Tuple<bool, string>(false, "");
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("Error in process airlock : " + e.ToString());
            }
            return new Tuple<bool, string>(false, "");

        }
        #endregion

        //#region GET HOST ACCOUNT INFO
        //public bool ScrapeHostProfile(string token, string username, string password, bool isImporting = false)
        //{
        //    //var token = LoginWithApi(username, password);
        //    var isLoaded = ValidateTokenAndLoadCookies(token);

        //    if (isLoaded) 
        //    {
        //        string url = "https://api.airbnb.com/v1/account/active";
        //        commPage.RequestURL = url;
        //        commPage.ReqType = CommunicationPage.RequestType.GET;
        //        var parms = new Dictionary<string, string>();
        //        var headers = new Dictionary<string, string>();
        //        parms.Add("client_id", "d306zoyjsyarp7ifhu67rjxn52tv0t20");
        //        headers.Add("X-Airbnb-OAuth-Token", token);
        //        commPage.PersistCookies = true;
        //        commPage.Parameters = parms;
        //        commPage.RequestHeaders = headers;
        //        ProcessRequest(commPage);

        //        if (commPage.IsValid)
        //        {
        //            var ob = JObject.Parse(commPage.Html);
        //            if (ob["result"].ToSafeString() == "success")
        //            {
        //                var hostId = ob["user"]["user"]["id"].ToSafeString();
        //                var firstname = ob["user"]["user"]["first_name"].ToSafeString();
        //                var lastname = ob["user"]["user"]["last_name"].ToSafeString();
        //                string profilePicture = ob["user"]["user"]["has_profile_pic"].ToBoolean() ? ob["user"]["user"]["picture_url"].ToSafeString() : "";

        //                Core.API.Airbnb.Hosts.Add(hostId, firstname, lastname, profilePicture, username, password);

        //                if (isImporting)
        //                    ImportHub.ShowHostName(GlobalVariables.UserId.ToString(), firstname, lastname, "Airbnb");

        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}
        //#endregion

        #region GET GUEST ACCOUNT INFO

        public bool ScrapeGuestAccount(string token, string username, string password, bool isImporting = false)
        {
            var isLoaded = ValidateTokenAndLoadCookies(token);

            if (isLoaded)
            {
                try
                {
                    string siteHostId = ScrapeSiteHostId(token);
                    string url = string.Format("https://api.airbnb.com/v2/users/{0}?client_id=3092nxybyb0otqw18e8nh5nty&_format=v1_legacy_show", siteHostId);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var name = ob["user"]["first_name"].ToSafeString();
                        var profile_picture = ob["user"]["has_profile_pic"].ToBoolean() ? ob["user"]["thumbnail_url"].ToSafeString() : null;
                        var id = ob["user"]["id"].ToSafeString();

                        using (var db = new ApplicationDbContext())
                        {
                            var temp = db.Guests.Where(x => x.GuestId == id).FirstOrDefault();

                            if (temp == null)
                            {
                                Guest g = new Guest
                                {
                                    GuestId = id,
                                    Name = name,
                                    ProfilePictureUrl = profile_picture
                                };
                                db.Guests.Add(g);
                            }
                            else
                            {
                                temp.GuestId = id;
                                temp.Name = name;
                                temp.ProfilePictureUrl = profile_picture;
                                db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                            }
                            db.SaveChanges();
                        }
                        return true;
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public List<Reviews> HostReviews(string guestId)
        {
            try
            {
                List<Reviews> hostReviews = new List<Reviews>();

                commPage.RequestURL = string.Format("https://api.airbnb.com/v2/reviews?role=all&reviewee_id={0}&key=d306zoyjsyarp7ifhu67rjxn52tv0t20", guestId);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviews = JArray.Parse(ob["reviews"].ToString());

                    foreach (var review in reviews)
                    {
                        hostReviews.Add(new Reviews
                        {
                            Name = review["author"]["first_name"].ToString(),
                            ProfilePictureUrl = review["author"]["picture_url"].ToString(),
                            Comments = review["comments"].ToString(),
                            CreatedAt = review["created_at"].ToDateTime()
                        });
                    }
                }

                return hostReviews;
            }
            catch (Exception e)
            {

            }

            return new List<Reviews>();
        }

        public List<GuestReviews> GuestReviewsToHost(string token, string siteHostId)
        {
            List<GuestReviews> guestReviewList = new List<GuestReviews>();
            var isLoggedIn = ValidateTokenAndLoadCookies(token);

            if (isLoggedIn)
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var auth_token = cc["_csrf_token"].Value.DecodeURL();

                commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/reviews?_format=for_web_host_stats&_order=recent&reviewee_id={0}&role=guest&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", siteHostId);
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviewNode = ((JArray)ob["reviews"]);

                    foreach (JObject item in reviewNode)
                    {
                        GuestReviews guestReview = new GuestReviews();

                        guestReview.Name = item["reviewer"]["first_name"].ToSafeString();
                        guestReview.Photo = item["reviewer"]["picture_url"].ToSafeString();
                        guestReview.Review = item["comments"].ToSafeString();
                        guestReview.Rating = item["rating"].ToSafeString().ToInt();
                        guestReview.Property = item["reservation"]["listing"]["name"].ToSafeString();

                        guestReviewList.Add(guestReview);
                    }
                }
            }

            return guestReviewList;
        }

        #endregion

        #region SCRAPE SITE HOST ID

        // I.G. (2018-05-12) - Added Scrapping of Host Id 
        private string ScrapeSiteHostId(string token)
        {
            var isLoaded = ValidateTokenAndLoadCookies(token);

            if (isLoaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/users/edit";
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        XmlDocument xm = commPage.ToXml();

                        string siteHostId = 
                            xm.GetElementById("update_form").Attributes["action"].Value.Replace("/update/", "");

                        return siteHostId;
                    }

                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return null;
        }

        #endregion

        #region CALENDAR SET PRICE AND AVAILABILITY

        public ScrapeResult UpdateAvailability(string token, string listingId, bool available, string dateFrom, string dateTo, string price)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://www.airbnb.com/manage-listing/{0}/calendar", listingId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        // var xd = commPage.ToXml();
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();



                        commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/calendars/{0}/{1}/{2}?_format=host_calendar_detailed&_price_tips=false&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", listingId, dateFrom, dateTo);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                        /**/
                        CommPage.JsonStringToPost = string.Format("{{\"availability\":\"{1}\",\"daily_price\":{0},\"notes\":\"\",\"demand_based_pricing_overridden\":false}}", price, available ? "available" : "unavailable");
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        /**/
                        CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                        /**/
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        commPage.Referer = string.Format("https://www.airbnb.com/manage-listing/{0}/calendar", listingId);
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            if (ob["calendar"] != null)
                            {
                                sresult.Message = "Success!";
                                sresult.Success = true;
                            }
                            else
                            {
                                sresult.Message = "An error occurred.";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check your details and try again.";
            }

            return sresult;
        }

        #endregion

        #region SCRAPE CALENDAR PRICE AND AVAILABILITY

        public List<PropertyBookingDate> ScrapeCalendar(string listingId, int noOfMonths)
        {
            var calendarList = new List<PropertyBookingDate>();

            string url = string.Format("https://www.airbnb.com/api/v2/calendar_months?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en&listing_id={0}&month={1}&year={2}&count={3}&_format=with_conditions", listingId, DateTime.Now.Month, DateTime.Now.Year, noOfMonths);
            try
            {
                if (commPage == null)
                    commPage = new CommunicationPage(url);

                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {

                    var ob = JObject.Parse(commPage.Html);
                    var months = (JArray)ob["calendar_months"];
                    if (months != null)
                    {

                        foreach (var month in months)
                        {
                            var days = (JArray)month["days"];
                            foreach (var day in days)
                            {
                                var dt = day["date"].ToSafeString().ToDateTime();
                                if (dt.Date < DateTime.Now.Date)
                                {
                                    continue;
                                }
                                var isAvlbl = day["available"].ToSafeString().ToBoolean();
                                var price = day["price"]["local_price"].ToSafeString() + " CAD";
                                var cal = new PropertyBookingDate
                                {
                                    PropertyId = Convert.ToInt64(listingId),
                                    Date = dt,
                                    IsAvailable = isAvlbl,
                                    Price = Convert.ToDouble(price.Replace("CAD", "")),
                                    SiteType = 1,
                                    //CompanyId = GlobalVariables.CompanyId
                                };
                                calendarList.Add(cal);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return calendarList;
        }

        #endregion

        #region SCRAPE INBOX
        public ScrapeResult ScrapeInbox(string token, int hostId, bool newOnly = false, bool inquiriesOnly = false, bool isImporting = false)
        {
            var result = new ScrapeResult();
            var count = 0;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    using (var db = new ApplicationDbContext())
                    {
                        Tuple<List<Inbox>, List<Guest>> scrapeMessageResult = ScrapeMessage(token, newOnly, inquiriesOnly);
                        List<Inbox> inboxList = scrapeMessageResult.Item1;
                        List<Guest> guestList = scrapeMessageResult.Item2;
                        List<Message> messageList = new List<Message>();
                        Core.API.Airbnb.Guests.AddRange(guestList);

                        List<System.Threading.Tasks.Task> tasks = new List<System.Threading.Tasks.Task>();
                        foreach (var inbox in inboxList)
                        {
                            tasks.Add(System.Threading.Tasks.Task.Factory.StartNew(() =>
                            {
                                using (var sm = new ScrapeManager())
                                {
                                    if (sm.LoadCookies(token))
                                    {
                                        var messages = sm.ScrapeMessageDetails(inbox.ThreadId, inbox);
                                        lock (waitLock) { messageList.AddRange(messages); }

                                        if (isImporting)
                                        {
                                            Trace.WriteLine(messages.Count);
                                            count += 1;
                                            GlobalVariables.ImportedMessagesCount = count;
                                            ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                        }
                                    }
                                }
                            }));
                        }

                        System.Threading.Tasks.Task.WaitAll(tasks.ToArray());

                        foreach (var inbox in inboxList)
                        {
                            inbox.HostId = hostId;

                            var inboxIsSaved = Core.API.Airbnb.Inboxes.AddOrUpdate(db, inbox, newOnly);
                            if (inboxIsSaved)
                            {
                                Core.API.Airbnb.Messages.AddIfNotExist(db, inbox.ThreadId, messageList.Where(x => x.ThreadId == inbox.ThreadId).ToList());
                            }
                        }
                        db.BulkSaveChanges();
                        result.Success = true;
                        result.TotalRecords = inboxList.Count;
                        result.Message = "Operation successful";

                        foreach (var task in tasks)
                        {
                            task.Dispose();
                        }
                        tasks.Clear();

                        //AutomatedResponse.FirstMessageAndUponInquiry(this, db, token, inboxList);
                    }
                }
                catch (Exception e)
                {
                    result.Message = e.Message;
                    Trace.WriteLine(e);
                }
            }
            else
            {
                result.Message = "Could not login, Please check you details and try again.";
            }
            return result;
        }

        #endregion


        
            #region SCRAPE MESSAGE
            public Tuple<List<Inbox>, List<Guest>> ScrapeMessage(string token, bool newOnly, bool inquiryOnly)
        {
            int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(token);
            //if (host.Value == null || host.Value.Id == 0) return new Tuple<List<Inbox>, List<Guest>>(new List<Inbox>(), new List<Guest>());

            List<Inbox> inboxList = new List<Inbox>();
            List<Guest> guestList = new List<Guest>();
            //int hostId = host.Value.Id.ToInt();
            int offset = 0;
            bool hasMore = false;
            do
            {
                string url = string.Format("https://www.airbnb.com/api/v2/threads?_format=for_mobile_inbox&_offset={0}&role={1}&selected_inbox_type=host&include_help_threads=true&include_support_messaging_threads=true&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", offset, "all");
                hasMore = false;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    try
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var ar_tempt = (JArray)ob["threads"];
                        var ar = ar_tempt.ToList();
                        if (inquiryOnly)
                        {
                            ar = ar.Where(f => f["status"].ToSafeString() == "special_offer" || f["status"].ToSafeString() == "inquiry").ToList();
                        }
                        if (ar.Count > 0)
                        {
                            hasMore = true;
                            offset += 10;
                            foreach (var item in ar)
                            {
                                var threadId = item["id"].ToLong();
                                var inquiryId = item["inquiry_reservation"].ToSafeString() == "" ? 0 : item["inquiry_reservation"]["id"].ToLong();
                                var isArchived = item["archived"].ToBoolean();
                                var guestName = item["other_user"]["first_name"].ToSafeString();
                                var guestUserId = item["other_user"]["id"].ToLong();
                                var guestProfilePictureUrl = item["other_user"]["picture_url"].ToSafeString();
                                var lastMessageAt = item["last_message_at"].ToDateTime();
                                var readStatus = item["unread"].ToBoolean();
                                var messageContent = item["text_preview"].ToSafeString();
                                var listingName = item["listing"].SelectToken("name") == null ? "" : item["listing"]["name"].ToSafeString();
                                var listingId = item["listing"].SelectToken("id") == null ? 0 : item["listing"]["id"].ToLong();
                                var status = item["status_string"].ToSafeString();
                                var isInquiryOnly = status == "Special Offer" || status == "Inquiry";
                                var isSpecialOfferSent = status == "Special Offer";
                                if (status == "Accepted") { status = "A"; }
                                else if (status == "Pending") { status = "B"; }
                                else if (status == "Canceled") { status = "BC"; }
                                else if (status == "Declined") { status = "C"; }
                                else if (status == "Inquiry") { status = "I"; }
                                else if (status == "Special Offer") { status = "I"; }
                                else if (status == "Not Possible") { status = "NP"; }
                                else if (status == "Pre-Approved") { status = "I"; }

                                if (guestUserId != 0 && threadId != 0 && listingId != 0 && /*hostId != 0 &&*/ GlobalVariables.CompanyId != 0)
                                {
                                    Inbox inbox = new Inbox();
                                    inbox.HostId = hostId;
                                    inbox.ThreadId = threadId.ToString();
                                    inbox.IsArchived = isArchived;
                                    inbox.Status = status;
                                    inbox.HasUnread = readStatus;
                                    inbox.IsInquiryOnly = isInquiryOnly;
                                    inbox.IsSpecialOfferSent = isSpecialOfferSent;
                                    inbox.GuestId = guestUserId.ToInt();
                                    inbox.LastMessage = messageContent;
                                    inbox.LastMessageAt = lastMessageAt;
                                    inbox.PropertyId = listingId;
                                    inbox.InquiryId = inquiryId.ToString();
                                    //inbox.CompanyId = GlobalVariables.CompanyId;
                                    inbox.SiteType = 1;
                                    if (inbox.LastMessageAt != null) { inbox.LastMessageAt = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inbox.LastMessageAt.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")); }
                                    inboxList.Add(inbox);

                                    Guest guest = new Guest();
                                    guest.GuestId = guestUserId.ToString();
                                    //guest.HostId = hostId;
                                    //guest.CompanyId = GlobalVariables.CompanyId;
                                    guest.Name = guestName;
                                    guest.ProfilePictureUrl = guestProfilePictureUrl;
                                    guestList.Add(guest);
                                }
                            }
                            if (newOnly && offset == 10) { hasMore = false; break; }
                        }
                        else
                        {
                            hasMore = false;
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                    }
                }
            } while (hasMore);
            return new Tuple<List<Inbox>, List<Guest>>(inboxList, guestList);
        }
        #endregion

        #region SCRAPE MESSAGE DETAILS
        public List<Message> ScrapeMessageDetails(string threadId, Inbox inbox = null)
        {
            try
            {
                List<Message> messages = new List<Message>();
                string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    var ar = (JArray)ob["appData"]["posts"];
                    var currentUserId = ob["appData"]["currentUserId"].ToLong();
                    var guestId = ob["appData"]["guestId"].ToSafeString();

                    if (inbox != null)
                    {
                        var statusType = ob["appData"]["statusType"].ToInt();
                        inbox.StatusType = statusType;
                        if (statusType == 1)
                        {
                            inbox.CanPreApproveInquiry = true;
                            inbox.CanDeclineInquiry = true;
                        }
                        else if (statusType == 13)
                        {
                            inbox.CanWithdrawPreApprovalInquiry = true;
                        }

                        #region Inquiry Related Data for inbox object
                        var inquiryNode = ob["inquiry"];

                        if (inquiryNode != null && inquiryNode.HasValues)
                        {
                            try
                            {
                                inbox.CheckInDate = inquiryNode["checkin_date"].ToDateTime();
                                inbox.CheckOutDate = inquiryNode["checkout_date"].ToDateTime();
                            }
                            catch
                            {
                                inbox.CheckInDate = null;
                                inbox.CheckOutDate = null;
                            }
                        }

                        var reservationNode = ob["appData"]["reservationInfo"];

                        if (reservationNode != null && reservationNode.HasValues)
                        {
                            inbox.GuestCount = string.IsNullOrEmpty(reservationNode["number_of_guests"].ToSafeString()) ? 0 : reservationNode["number_of_guests"].ToInt();
                        }

                        var paymentNode = ob["appData"]["paymentInfo"];

                        if (paymentNode != null && paymentNode.HasValues)
                        {
                            try
                            {
                                inbox.RentalFee = paymentNode["basePrice"].ToDecimal();
                                inbox.CleaningFee = paymentNode["extrasPrice"].ToDecimal();
                                inbox.GuestFee = paymentNode["guestFeeFormatted"].ToDecimal();
                                inbox.ServiceFee = paymentNode["hostFeeFormatted"].ToDecimal();
                                inbox.Subtotal = paymentNode["subtotalPriceString"].ToDecimal();
                                inbox.GuestPay = paymentNode["guestPays"].ToDecimal();
                                inbox.TotalPayout = paymentNode["hostEarns"].ToDecimal();
                            }
                            catch { }
                        }
                        #endregion
                    }

                    if (ar.Count > 0)
                    {
                        foreach (var item in ar)
                        {
                            var userId = item["user_id"].ToLong();
                            var myMessage = false;
                            if (userId == currentUserId)
                            {
                                myMessage = true;
                            }

                            var message = item["message"].ToSafeString();
                            var createdAt = item["created_at"].ToDateTime();
                            var messageId = item["id"].ToSafeString();
                            if (!string.IsNullOrEmpty(message))
                            {
                                Message tm = new Message
                                {
                                    ThreadId = threadId,
                                    MessageId = messageId,
                                    MessageContent = message,
                                    IsMyMessage = myMessage,
                                    CreatedDate = createdAt
                                };
                                if (tm.CreatedDate != null) { tm.CreatedDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(tm.CreatedDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")); }
                                messages.Add(tm);
                            }
                        }
                    }
                }
                return messages;
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString()); return new List<Message>();
            }
        }
        #endregion

        #region SEND MESSAGE
        public ScrapeResult SendMessageToInbox(string sessionToken, string threadId, string messageText)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/z/q/" + threadId;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        var token = xd.SelectSingleNode("//input[@name='authenticity_token']").GetAttributeFromNode("value").ToSafeString();
                        commPage.RequestURL = "https://www.airbnb.com/messaging/qt_reply_v2/" + threadId;
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>() { { "message", messageText } };
                        commPage.Parameters.Add("authenticity_token", token);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, */*; q=0.01");
                        commPage.Referer = "https://www.airbnb.com/z/q/" + threadId;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var status = ob["status"].ToSafeString();
                            sresult.Message = status;
                            sresult.Success = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                    Debug.WriteLine("Send Message Airbnb : " + e.Message);
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }
        #endregion

        #region RESERVATION

        //public ScrapeResult ScrapeReservations(string token, int hostId, bool pendingOnly = false,
        //    bool upcomingOnly = false, string reservationCode = "", bool isImporting = false)
        //{
        //    var sresult = new ScrapeResult();
        //    int inquiryCount = 0;
        //    List<Inquiry> inquiryList = new List<Inquiry>();
        //    List<Guest> guestList = new List<Guest>();
        //    var isloaded = ValidateTokenAndLoadCookies(token);
        //    if (isloaded)
        //    {
        //        string url = upcomingOnly ? "https://www.airbnb.com/my_reservations" : "https://www.airbnb.com/my_reservations?all=1";
        //        try
        //        {
        //            do
        //            {
        //                commPage.RequestURL = url;
        //                commPage.PersistCookies = true;
        //                commPage.ReqType = CommunicationPage.RequestType.GET;
        //                commPage.Referer = "";
        //                commPage.RequestHeaders = new Dictionary<string, string>();
        //                url = "";
        //                ProcessRequest(commPage);
        //                if (commPage.IsValid)
        //                {
        //                    XmlDocument xd = commPage.ToXml();
        //                    if (xd != null)
        //                    {
        //                        var rows = xd.SelectNodes("//tr[@class='reservation']");
        //                        foreach (XmlNode row in rows)
        //                        {
        //                            var id = long.Parse(row.GetAttributeFromNode("data-reservation-id"));
        //                            var code = row.GetAttributeFromNode("data-reservation-code");
        //                            XmlNodeList cols = row.SelectNodes(".//td");
        //                            var airbnbStatus = cols[0].SelectSingleNode(".//span[contains(@class,'label')]").GetInnerTextFromNode();
        //                            var bookingStatus = "";

        //                            if (pendingOnly && airbnbStatus != "Pending") { continue; }
        //                            if (!string.IsNullOrEmpty(reservationCode) && code != reservationCode) { continue; }

        //                            var name = cols[1].SelectSingleNode(".//a").GetInnerTextFromNode();
        //                            var date = cols[1].FirstChild.GetInnerTextFromNode();
        //                            var address = cols[1].LastChild.PreviousSibling.PreviousSibling.PreviousSibling.GetInnerTextFromNode() + ", " + cols[1].LastChild.PreviousSibling.GetInnerTextFromNode();
        //                            var listingLink = cols[1].SelectSingleNode(".//a").GetAttributeFromNode("href");
        //                            var profileLink = "";
        //                            try { profileLink = cols[2].SelectSingleNode(".//a[contains(@class,'media-photo')]").GetAttributeFromNode("href"); }
        //                            catch { }
        //                            if (string.IsNullOrEmpty(profileLink))
        //                            {
        //                                try { profileLink = cols[2].SelectSingleNode(".//a[contains(@class,'pull-left')]").GetAttributeFromNode("href"); }
        //                                catch { }
        //                            }

        //                            var profileImgLink = cols[2].SelectSingleNode(".//img").GetAttributeFromNode("src");

        //                            var userName = cols[2].SelectSingleNode(".//img").GetAttributeFromNode("alt");
        //                            var total = cols[3].FirstChild.GetInnerTextFromNode().Replace("total", "");
        //                            var detailLink = cols[3].SelectSingleNode(".//a[contains(.,'Print Confirmation')]").GetAttributeFromNode("href");
        //                            var changeOrCancelLink = cols[3].SelectSingleNode(".//a[contains(.,'Change or Cancel')]").GetAttributeFromNode("href");
        //                            var messageThreadLink = cols[3].SelectSingleNode(".//a[contains(.,'Message History')]").GetAttributeFromNode("href");
        //                            var guestId = ScrapeHelper.SplitSafely(profileLink, "/", getLast: true).ToLong();
        //                            var propertyId = ScrapeHelper.SplitSafely(listingLink, "/", getLast: true).ToLong();

        //                            if (airbnbStatus == "Accepted") { bookingStatus = "A"; }
        //                            else if (airbnbStatus == "Pending") { bookingStatus = "B"; }
        //                            else if (airbnbStatus == "Canceled") { bookingStatus = "BC"; }
        //                            else if (airbnbStatus == "Declined") { bookingStatus = "C"; }
        //                            else if (airbnbStatus == "Inquiry") { bookingStatus = "I"; }

        //                            //var host = GlobalVariables.HostAccounts.Where(x => x.Value.SessionToken == token).FirstOrDefault().Value;

        //                            var inquiry = new Core.Database.Entity.Inquiry
        //                            {
        //                                ReservationId = id.ToString(),
        //                                HostId = hostId,// host == null ? 0 : host.Id.ToInt(),
        //                                PropertyId = propertyId.ToInt(),
        //                                GuestId = guestId.ToInt(),
        //                                ConfirmationCode = code,
        //                                ReservationCost = Utilities.ExtractDecimal(total),
        //                                BookingStatusCode = bookingStatus,
        //                                IsAltered = false,
        //                                IsActive = string.IsNullOrEmpty(changeOrCancelLink) ? false : true,
        //                                //CompanyId = GlobalVariables.CompanyId,
        //                                SiteType = 1
        //                            };

        //                            bool hasReservationDetailLink = false;
        //                            if (!string.IsNullOrEmpty(detailLink))
        //                            {
        //                                hasReservationDetailLink = true;
        //                                detailLink = "https://www.airbnb.com" + detailLink;
        //                                ScrapeInquiryDetails(detailLink, inquiry);
        //                            }

        //                            if (!string.IsNullOrEmpty(messageThreadLink))
        //                            {
        //                                messageThreadLink = "https://www.airbnb.com" + messageThreadLink;
        //                                GetInquiryDetails(messageThreadLink, inquiry, hasReservationDetailLink, date);
        //                            }

        //                            // start convert to pacific utc
        //                            if (inquiry.BookingDate != null) { inquiry.BookingDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inquiry.BookingDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")); }
        //                            if (inquiry.BookingConfirmDate != null) { inquiry.BookingConfirmDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inquiry.BookingConfirmDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")); }
        //                            if (inquiry.InquiryDate != null) { inquiry.InquiryDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inquiry.InquiryDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")); }
        //                            if (inquiry.CheckInDate != null) { inquiry.CheckInDate = inquiry.CheckInDate.ToDateTime().Date + objIList.GetCheckInTime(inquiry.PropertyId); }
        //                            if (inquiry.CheckOutDate != null) { inquiry.CheckOutDate = inquiry.CheckOutDate.ToDateTime().Date + objIList.GetCheckOutTime(inquiry.PropertyId); }
        //                            // end convert

        //                            inquiryList.Add(inquiry); // add to list
        //                            //Core.API.Airbnb.Inquiries.AddOrUpdate(inquiry, true);
        //                            //Core.API.Airbnb.Inquiries.AddOrUpdate(inquiry, true);

        //                            using (var sm = new Airbnb.ScrapeManager())
        //                            {
        //                                List<long> propertyIds = new List<long>();
        //                                propertyIds.Add(propertyId);

        //                                using (var db = new ApplicationDbContext())
        //                                {
        //                                    int parentPropertyId =
        //                                        (from p in db.Properties
        //                                         where
        //                                              p.ListingId == propertyId
        //                                         select p.ParentPropertyId)
        //                                         .FirstOrDefault();

        //                                    if (parentPropertyId != 0)
        //                                    {
        //                                        (from p in db.Properties
        //                                         where
        //                                              p.ParentPropertyId == parentPropertyId &&
        //                                              p.ListingId != propertyId
        //                                         select p.ListingId)
        //                                         .ToList()
        //                                         .ForEach(p => propertyIds.Add(p));
        //                                    }

        //                                    foreach (var pId in propertyIds)
        //                                    {
        //                                        var price = (from pbd in db.PropertyBookingDate where pbd.PropertyId == pId select pbd.Price).FirstOrDefault();

        //                                        if (inquiry.BookingStatusCode == "A" || inquiry.BookingStatusCode == "B" ||
        //                                        inquiry.BookingStatusCode == "I")
        //                                        {
        //                                            Airbnb.Models.ScrapeResult sr =
        //                                            sm.UpdateAvailability(Core.API.Airbnb.Hosts.GetToken(hostId), pId.ToString(),
        //                                              false, Convert.ToDateTime(inquiry.CheckInDate.ToSafeString()).ToString("yyyy-MM-dd"),
        //                                               Convert.ToDateTime(inquiry.CheckOutDate.ToSafeString()).ToString("yyyy-MM-dd"), price.ToSafeString());
        //                                        }
        //                                        else if (inquiry.BookingStatusCode == "BC" || inquiry.BookingStatusCode == "C")
        //                                        {
        //                                            Airbnb.Models.ScrapeResult sr =
        //                                            sm.UpdateAvailability(Core.API.Airbnb.Hosts.GetToken(hostId), pId.ToString(),
        //                                              true, Convert.ToDateTime(inquiry.CheckInDate.ToSafeString()).ToString("yyyy-MM-dd"),
        //                                               Convert.ToDateTime(inquiry.CheckOutDate.ToSafeString()).ToString("yyyy-MM-dd"), price.ToSafeString());
        //                                        }
        //                                    }
        //                                }
        //                            }

        //                            if (isImporting)
        //                            {
        //                                inquiryCount += 1;
        //                                GlobalVariables.ImportedBookingCount = inquiryCount;
        //                                ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
        //                            }
        //                        }

        //                        url = xd.SelectSingleNode("//li[@class='next next_page']/a").GetAttributeFromNode("href");
        //                        if (!string.IsNullOrEmpty(url))
        //                        {
        //                            url = "https://www.airbnb.com" + url;
        //                        }
        //                    }
        //                }
        //            } while (!string.IsNullOrEmpty(url));

        //            if (inquiryList.Count() > 0)
        //            {
        //                //var activeBookings = (from b in inquiryList
        //                //                      where b.IsActive && b.BookingStatusCode == "A"
        //                //                      select b).ToList();

        //                // for automated reply
        //                //AutomatedResponse.BeforeCheckIn(this, db, token, activeBookings);
        //                //AutomatedResponse.AfterCheckIn(this, db, token, activeBookings);
        //                //AutomatedResponse.BeforeCheckOut(this, db, token, activeBookings);
        //                //AutomatedResponse.AfterCheckOut(this, db, token, activeBookings);
        //                // end

        //                sresult.Success = true;
        //                sresult.Message = "Operation Successful";
        //                sresult.TotalRecords = inquiryList.Count();
        //            }
        //            else
        //            {
        //                sresult.Message = "An error occurred.";
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            sresult.Message = e.Message;
        //            Trace.WriteLine("Error in scrapping inquiries.");
        //            Trace.WriteLine(e);
        //        }
        //    }
        //    else
        //    {
        //        sresult.Message = "Could not login, Please check you details and try again.";
        //    }
        //    return sresult;
        //}

        public void ScrapeInquiryDetails(string url, Inquiry inquiries)
        {
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-shared_itinerary_app']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);
                        inquiries.SumPerNightAmount = Utilities.ExtractDecimal(ob["base_price"].ToSafeString());
                        inquiries.TotalPayout = Utilities.ExtractDecimal(ob["total_price"].ToSafeString());
                        inquiries.NetRevenueAmount = Utilities.ExtractDecimal(ob["total_price"].ToSafeString());
                        inquiries.CleaningFee = Utilities.ExtractDecimal(ob["extras_price"].ToSafeString());
                        inquiries.ServiceFee = Utilities.ExtractDecimal(ob["host_fee"].ToSafeString());
                        inquiries.Nights = ob["nights"].ToInt();
                        inquiries.CheckInDate = ob["checkin_date"].ToDateTime();
                        inquiries.CheckOutDate = ob["checkout_date"].ToDateTime();
                    }
                }
            }
            catch (Exception e) { Trace.WriteLine(e); }
        }

        public void GetInquiryDetails(string url, Inquiry inquiry, bool hasReservationDetailLink, string dateRange)
        {
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);

                        var isReservationAccepted = ob["is_reservation_accepted"].ToSafeString().ToBoolean();
                        var appData = ob["appData"];
                        var StatusType = appData["statusType"].ToInt();
                        if (StatusType == 5)
                        {
                            var alterationNode = appData["alterationRequestInfo"];
                            inquiry.IsAltered = alterationNode != null ? true : false;
                        }

                        var ar = (JArray)ob["appData"]["posts"];
                        if (inquiry.BookingStatusCode.ToUpper() == "B") // pending
                        {
                            var non_response = ob["non_response"].ToSafeString();
                            if (!string.IsNullOrEmpty(non_response))
                            {
                                var dt = non_response.ToDateTime();
                                var origDt = dt.AddDays(-1);
                                inquiry.BookingDate = origDt.ToDateTime();
                            }
                        }

                        if (inquiry.CheckInDate == null && inquiry.CheckOutDate == null)
                        {
                            var tuple = ScrapeDates(dateRange);
                            if (tuple != null)
                            {
                                inquiry.CheckInDate = tuple.Item1;
                                inquiry.CheckOutDate = tuple.Item2;
                            }
                            else
                            {
                                try
                                {
                                    inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["start_date"]);
                                    inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["end_date"]);
                                }
                                catch (Exception err1)
                                {
                                    if (inquiry.BookingStatusCode == "A")
                                    {
                                    }
                                    var ctx = JObject.Parse((ob["tracking"]["context"]).ToString());
                                    inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["context"]["checkin_date"]);
                                    inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["context"]["checkout_date"]);
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["start_date"]);
                                inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["end_date"]);
                            }
                            catch (Exception err1)
                            {
                                if (inquiry.BookingStatusCode == "A")
                                {
                                }
                                var ctx = JObject.Parse((ob["tracking"]["context"]).ToString());
                                inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["context"]["checkin_date"]);
                                inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["context"]["checkout_date"]);
                            }
                        }

                        try
                        {
                            var dtStrt = inquiry.CheckInDate.ToDateTime();
                            var dtEnd = inquiry.CheckOutDate.ToDateTime();
                            inquiry.Nights = (dtEnd - dtStrt).Days.ToInt();
                        }
                        catch (Exception)
                        {

                        }

                        if (ar.Count > 0)
                        {
                            // booking confirm date
                            if (ar.Where(f => f["template_name"].ToSafeString() == "25" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault() != null)
                            {
                                var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "25" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault();
                                inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                inquiry.BookingDate = inquiry.BookingConfirmDate;
                            }
                            else if (ar.Where(f => f["template_name"].ToSafeString() == "25" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault() != null)
                            {
                                var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "25" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault();
                                inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                inquiry.BookingDate = inquiry.BookingConfirmDate;
                            }
                            else if (ar.Where(f => f["template_name"].ToSafeString() == "44" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault() != null)
                            {
                                var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "44" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault();
                                inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                inquiry.BookingDate = inquiry.BookingConfirmDate;
                            }
                            else if (ar.Where(f => f["template_name"].ToSafeString() == "44" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault() != null)
                            {
                                var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "44" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault();
                                inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                inquiry.BookingDate = inquiry.BookingConfirmDate;
                            }

                            // booking cancel date
                            var reservationCanceledPost = ar.Where(f => f["template_name"].ToSafeString() == "24" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault();
                            if (reservationCanceledPost != null)
                            {
                                inquiry.BookingCancelDate = reservationCanceledPost["created_at"].ToDateTime();
                            }
                            // booking inquiry date
                            var reservationPost = ar.Where(f => f["link_type"].ToSafeString() == "Hosting" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).LastOrDefault();
                            if (reservationPost != null)
                            {
                                inquiry.InquiryDate = reservationPost["created_at"].ToDateTime();
                            }
                            else
                            {
                                reservationPost = ar.Where(f => f["link_type"].ToSafeString() == "Hosting").LastOrDefault();
                                if (reservationPost != null)
                                {
                                    inquiry.InquiryDate = reservationPost["created_at"].ToDateTime();
                                }
                                else
                                {
                                    reservationPost = ar.LastOrDefault();
                                    if (reservationPost != null)
                                    {
                                        inquiry.InquiryDate = reservationPost["created_at"].ToDateTime();
                                    }
                                }
                            }
                        }

                        if (!hasReservationDetailLink)
                        {
                            var paymentInfo = ob["appData"]["paymentInfo"];
                            if (paymentInfo != null)
                            {
                                inquiry.SumPerNightAmount = Utilities.ExtractDecimal(paymentInfo["basePrice"].ToSafeString());
                                inquiry.TotalPayout = Utilities.ExtractDecimal(paymentInfo["hostEarns"].ToSafeString());
                                inquiry.CleaningFee = Utilities.ExtractDecimal(paymentInfo["extrasPrice"].ToSafeString());
                            }
                        }

                        var reservationInfo = ob["appData"]["reservationInfo"];
                        if (reservationInfo != null)
                        {
                            inquiry.GuestCount = reservationInfo["number_of_guests"].ToInt();
                        }
                        long threadId = 0;
                        long.TryParse(ob["thread_id"].ToSafeString(), out threadId);
                        inquiry.ThreadId = threadId.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString() + " " + e.StackTrace);
            }
        }

        public static Tuple<DateTime, DateTime> ScrapeDates(string dateRange)
        {
            var dates = dateRange.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (dates.Length == 2)
            {
                var year1 = Utilities.SplitSafely(dates[0], ",", 1);
                var year2 = Utilities.SplitSafely(dates[1], ",", 1);

                var month1 = Utilities.SplitSafely(dates[0], " ", 0);
                var date1 = Utilities.SplitSafely(dates[0], " ", 1).Trim(',');

                var month2 = "";
                var date2 = "";

                var rawDate2WithoutYear = Utilities.SplitSafely(dates[1], ",", 0);
                var date2parts = rawDate2WithoutYear.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (date2parts.Length == 2)
                {
                    month2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 0);
                    date2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 1);
                }
                else
                {
                    date2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 0);
                }

                if (string.IsNullOrEmpty(year1))
                    year1 = year2;
                if (string.IsNullOrEmpty(month2))
                    month2 = month1;

                var datefrom = string.Format("{0} {1} {2}", month1, date1, year1);
                var dateto = string.Format("{0} {1} {2}", month2, date2, year2);

                var dtfrom = datefrom.ToDateTime();
                var dtto = dateto.ToDateTime();
                return new Tuple<DateTime, DateTime>(dtfrom, dtto);
            }
            return null;
        }

        #endregion

        #region INQUIRY ACTIONS
        public bool PreApproveInquiry(string threadId)
        {
            string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            commPage.RequestHeaders = new Dictionary<string, string>();
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);
                var inquiryPostId = ob["appData"]["inquiryPostId"].ToSafeString();

                if (!string.IsNullOrEmpty(inquiryPostId))
                {
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    var auth_token = cc["_csrf_token"].Value.DecodeURL();

                    commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    var parms = new Dictionary<string, string>();
                    parms.Add("message", "");
                    parms.Add("template", "1");
                    parms.Add("inquiry_post_id", inquiryPostId);
                    commPage.Parameters = parms;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    if (commPage.IsValid && commPage.Html.Contains("has been sent"))
                    {
                        UpdateThreadStatuses(threadId);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool WithdrawPreapprovalInquiry(string threadId)
        {
            string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);
                var removOfferUrl = ob["ajax_urls"]["remove_offer"].ToSafeString();

                if (!string.IsNullOrEmpty(removOfferUrl))
                {
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    var token = cc["_csrf_token"].Value.DecodeURL();

                    commPage.RequestURL = string.Format("https://www.airbnb.com{0}", removOfferUrl);
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    var parms = new Dictionary<string, string>();
                    parms.Add("_method", "post");
                    parms.Add("authenticity_token", token);

                    commPage.Parameters = parms;
                    commPage.PersistCookies = true;

                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-CSRF-Token", token);
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    commPage.RequestHeaders = new Dictionary<string, string>();

                    if (commPage.IsValid && commPage.Uri.AbsolutePath.Contains("/z/q/"))
                    {
                        UpdateThreadStatuses(threadId);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool DeclineInquiry(string threadId, string message)
        {
            string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            commPage.RequestHeaders = new Dictionary<string, string>();
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);
                var inquiryPostId = ob["appData"]["inquiryPostId"].ToSafeString();

                if (!string.IsNullOrEmpty(inquiryPostId))
                {
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    var auth_token = cc["_csrf_token"].Value.DecodeURL();

                    commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    var parms = new Dictionary<string, string>();
                    parms.Add("message", message);
                    parms.Add("template", "9");
                    parms.Add("inquiry_post_id", inquiryPostId);
                    parms.Add("decline_reason", "other");
                    commPage.Parameters = parms;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    if (commPage.IsValid && commPage.Html.Contains("has been sent"))
                    {
                        UpdateThreadStatuses(threadId);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool UpdateThreadStatuses(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (inbox != null)
                {
                    List<InboxMessage> messages = new List<InboxMessage>();
                    string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);
                        var ar = (JArray)ob["appData"]["posts"];
                        var statusType = ob["appData"]["statusType"].ToSafeString();
                        bool canPreApproveInquiry = false;
                        bool canDeclineInquiry = false;
                        bool canWithdrawPreApprovalInquiry = false;
                        if (statusType == "1")
                        {
                            canPreApproveInquiry = true;
                            canDeclineInquiry = true;
                        }
                        else if (statusType == "13")
                        {
                            canWithdrawPreApprovalInquiry = true;
                        }
                        inbox.CanPreApproveInquiry = canPreApproveInquiry;
                        inbox.CanDeclineInquiry = canDeclineInquiry;
                        inbox.CanWithdrawPreApprovalInquiry = canWithdrawPreApprovalInquiry;
                        db.Entry(inbox).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return false;
            }
        }
        #endregion

        #region BOOKING ACTIONS
        public ScrapeResult AcceptOffer(string sessionToken, string reservationCode, string message)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/z/a/" + reservationCode;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    sresult.Message = "Compage not valid";

                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("authenticity_token", token);
                        commPage.Parameters.Add("utf8", "✓");
                        commPage.Parameters.Add("message", message);
                        commPage.Parameters.Add("tos_confirm", "1");
                        commPage.Parameters.Add("decision", "accept");
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.Referer = "https://www.airbnb.com/z/a/" + reservationCode;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var success = ob["success"].ToSafeString();
                            sresult.Message = "Operation successful.";
                            sresult.Success = true;
                            return sresult;
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public ScrapeResult DeclineOffer(string sessionToken, string reservationCode, string reason)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/z/a/" + reservationCode;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    sresult.Message = "Compage not valid";
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = "https://www.airbnb.com/reservation/approve";
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("code", reservationCode);
                        commPage.Parameters.Add("decline_reason", reason);
                        commPage.Parameters.Add("block_calendar", "true");
                        commPage.Parameters.Add("decision", "decline");
                        commPage.Parameters.Add("authenticity_token", token);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.Referer = "https://www.airbnb.com/z/a/" + reservationCode;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            if (commPage.Uri.AbsolutePath.Contains("my_reservations"))
                            {
                                sresult.Message = "Operation successful.";
                                sresult.Success = true;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public Tuple<bool, string, string, string> GetAlterReservationDetail(string sessionToken, string reservationCode, DateTime newCheckInDate, DateTime newCheckoutDate, decimal price, string listingId, int guests)
        {
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var chkInDateStr = newCheckInDate.ToString("MM/dd/yyyy");
                        var chkOutdateStr = newCheckoutDate.ToString("MM/dd/yyyy");
                        var xd = commPage.ToXml();

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();

                        var url = string.Format("https://www.airbnb.com/reservation_alterations/ajax_price_and_availability?"
                            + "authenticity_token={0}&code={1}&pricing%5Bhosting_id%5D={2}&pricing%5Bguests%5D={3}&pricing%5Bstart_date%5D={4}&pricing%5Bend_date%5D={5}&price={6}",
                            token, reservationCode, listingId, guests, chkInDateStr, chkOutdateStr, price);

                        commPage.RequestURL = url;
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var success = ob["status"].ToSafeString();
                            var message = ob["availability_response"].ToSafeString();
                            var total = ob["total_price"].ToSafeString();
                            var priceDifference = ob["price_difference_string"].ToSafeString();
                            return new Tuple<bool, string, string, string>(success == "success", message, priceDifference, total);
                        }
                    }
                }
                catch
                {

                }
            }
            return new Tuple<bool, string, string, string>(false, "Could not login, Please check you details and try again.", "", "");
        }
        public bool ProcessAlterReservation(string reservationCode, DateTime checkInDate, DateTime checkoutDate, decimal price, string listingId, int guests)
        {
            try
            {
                var chkInDateStr = checkInDate.ToString("MM/dd/yyyy");
                var chkOutdateStr = checkoutDate.ToString("MM/dd/yyyy");
                var xd = commPage.ToXml();
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();
                var url = "https://www.airbnb.com/reservation_alterations";
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("code", reservationCode);
                commPage.Parameters.Add("pricing[hosting_id]", listingId);
                commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                commPage.Parameters.Add("pricing[start_date]", chkInDateStr);
                commPage.Parameters.Add("pricing[end_date]", chkOutdateStr);
                commPage.Parameters.Add("price", price.ToSafeString());
                commPage.Parameters.Add("authenticity_token", token);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation_alterations/"))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {

            }
            return false;
        }

        public bool CancelAlteration(string sessionToken, string reservationCode)
        {
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    string alterationId = "";
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-alter_cancel_data']");
                            var json = meta.GetAttributeFromNode("content").HtmlDecode();
                            var ob = JObject.Parse(json);
                            var alts = (JArray)ob["reservation"]["reservation_alterations"];
                            if (alts != null)
                            {
                                alterationId = alts[0]["reservation_alteration"]["id"].ToSafeString();
                            }
                        }
                    }

                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    var token = cc["_csrf_token"].Value.DecodeURL();

                    var url = "https://www.airbnb.com/reservation_alterations/cancel/" + alterationId;
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.POST;

                    commPage.Parameters = new Dictionary<string, string>();
                    commPage.Parameters.Add("utf8", "✓");
                    commPage.Parameters.Add("authenticity_token", token);
                    commPage.RequestHeaders = new Dictionary<string, string>();

                    commPage.Referer = string.Format("https://www.airbnb.com/reservation_alterations/{0}", alterationId);
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                }
            }
            return false;
        }
        #endregion

        #region Special Offer
        public SpecialOfferSummary GetSpecialOfferSummary(string token, string listingId, string guestId, DateTime startDate, DateTime enddate, int guests)
        {
            var summary = new SpecialOfferSummary();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    summary.IsAvailable = IsAvailableForDate(listingId, startDate, enddate);
                    if (summary.IsAvailable)
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?_format=for_detailed_booking_info_on_web_p3&_intents=p3_book_it&listing_id={0}&check_in={1}&check_out={2}&guests={3}&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", listingId, startDate.Date.ToString("yyyy-MM-dd"), enddate.Date.ToString("yyyy-MM-dd"), guests);
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var amount = ((JArray)ob["pricing_quotes"])[0]["p3_display_rate_with_cleaning_fee"]["amount"].ToSafeString().ToDecimal();
                            summary.Subtotal = amount;

                            commPage.RequestURL = string.Format("https://www.airbnb.com/rooms/pricing/?pricing[start_date]={1}&pricing[end_date]={2}&pricing[hosting_id]={0}&pricing[price]={3}&pricing[is_inline]=true&pricing[guest_id]={4}&pricing[number_of_guests]={5}&pricing[currency]=CAD", listingId, startDate.Date.ToString("yyyy-MM-dd"), enddate.Date.ToString("yyyy-MM-dd"), amount, guestId, guests);
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();

                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            CommPage.RequestHeaders.Add("X-CSRF-Token", token);

                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                summary.GuestPays = ob["guest_pays_native"].ToSafeString().ToDecimal();
                                summary.HostEarns = ob["host_earns_native"].ToSafeString().ToDecimal();
                            }
                        }
                    }
                    summary.IsSuccess = true;
                    return summary;
                }
                catch { }
            }
            return summary;
        }

        public bool SendSpecialOffer(string token, string listingId, string threadId, int guests, DateTime startDate, DateTime enddate, string price)
        {
            var summary = new SpecialOfferSummary();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    summary.IsAvailable = IsAvailableForDate(listingId, startDate, enddate);
                    if (summary.IsAvailable)
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("authenticity_token", auth_token);
                        commPage.Parameters.Add("pricing[hosting_id]", listingId);
                        commPage.Parameters.Add("pricing[start_date]", startDate.Date.ToString("MM/dd/yyyy"));
                        commPage.Parameters.Add("pricing[end_date]", enddate.Date.ToString("MM/dd/yyyy"));
                        commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                        commPage.Parameters.Add("pricing[unit]", "nightly");
                        commPage.Parameters.Add("template", "2");
                        commPage.Parameters.Add("message", "");
                        commPage.Parameters.Add("pricing[price]", price.ToSafeString());

                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            if (ob != null && ob["status"].ToSafeString() == "Your message has been sent.")
                            {
                                return true;
                            }
                        }
                    }
                }
                catch { }
            }
            return false;
        }

        public string GetRemoveOfferUrl(string id)
        {
            string url = string.Format("https://www.airbnb.com/z/q/{0}", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);
                var removeOfferUrl = ob["ajax_urls"]["remove_offer"].ToSafeString();
                if (!string.IsNullOrEmpty(removeOfferUrl))
                {
                    removeOfferUrl = "https://www.airbnb.com" + removeOfferUrl;
                    return removeOfferUrl;
                }
            }
            return string.Empty;
        }

        public bool WithdrawSpecialOffer(string url)
        {
            try
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.PersistCookies = true;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("_method", "post");
                commPage.Parameters.Add("authenticity_token", token);

                commPage.Referer = "https://www.airbnb.com/z/q/326933359";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsolutePath.Contains("/z/q/"))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
        #endregion

        #region PROPERTIES
        public ScrapeResult ScrapeMyListings(string token, int hostId, bool isImporting = false)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            var count = 0;
            if (isloaded)
            {
                string url = "https://www.airbnb.com/rooms";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-index']");
                            var json = meta.GetAttributeFromNode("content").HtmlDecode();

                            if (!string.IsNullOrEmpty(json)) // old page
                            {
                                var ob = JObject.Parse(json);
                                var ar = (JArray)ob["unlistedListings"];
                                if (ar.Count > 0)
                                {
                                    foreach (var item in ar)
                                    {
                                        var id = item["id"].ToSafeString();
                                        var link = "https://www.airbnb.com/rooms/" + id;
                                        var status = "Unlisted";
                                        var property = ScrapeListingDetails(hostId, link, status);

                                        if (property != null)
                                        {
                                            Core.API.Airbnb.Properties.AddOrUpdate(property);
                                            if (isImporting)
                                            {
                                                count += 1;
                                                GlobalVariables.ImportedPropertyCount = count;
                                                ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                            }
                                        }
                                    }
                                }

                                var activeArr = (JArray)ob["activeListings"];
                                if (activeArr.Count > 0)
                                {
                                    foreach (var item in activeArr)
                                    {
                                        var id = item["id"].ToSafeString();
                                        var link = "https://www.airbnb.com/rooms/" + id;
                                        var status = "Active";

                                        var property = ScrapeListingDetails(hostId, link, status);

                                        if (property != null)
                                        {
                                            Core.API.Airbnb.Properties.AddOrUpdate(property);
                                            if (isImporting)
                                            {
                                                count += 1;
                                                GlobalVariables.ImportedPropertyCount = count;
                                                ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                            }
                                        }
                                    }
                                }

                                var inactiveArr = (JArray)ob["inactiveListings"];
                                if (inactiveArr.Count > 0)
                                {
                                    foreach (var item in inactiveArr)
                                    {
                                        var id = item["id"].ToSafeString();
                                        var link = "https://www.airbnb.com/rooms/" + id;
                                        var status = "Inactive";
                                        var property = ScrapeListingDetails(hostId, link, status);

                                        if (property != null)
                                        {
                                            Core.API.Airbnb.Properties.AddOrUpdate(property);
                                            if (isImporting)
                                            {
                                                count += 1;
                                                GlobalVariables.ImportedPropertyCount = count;
                                                ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                            }
                                        }
                                    }
                                }

                                var incompArr = (JArray)ob["incompleteListings"];
                                if (incompArr.Count > 0)
                                {
                                    foreach (var item in incompArr)
                                    {
                                        var id = item["id"].ToSafeString();
                                        var link = "https://www.airbnb.com/rooms/" + id;
                                        var status = "Incomplete";
                                        var property = ScrapeListingDetails(hostId, link, status);

                                        if (property != null)
                                        {
                                            Core.API.Airbnb.Properties.AddOrUpdate(property);
                                            if (isImporting)
                                            {
                                                count += 1;
                                                GlobalVariables.ImportedPropertyCount = count;
                                                ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            else // new page
                            {
                                meta = xd.SelectSingleNode("//script[@data-hypernova-key='list_of_listingsbundlejs']");
                                json = meta.GetInnerTextFromNode().HtmlDecode().Trim();
                                json = json.Replace("//\r\n", "");
                                json = json.Replace("<!--", "");
                                json = json.Replace("-->", "");
                                var ob = JObject.Parse(json);
                                var ar = (JArray)ob["bootstrapData"]["rooms_listings"];

                                foreach (var item in ar)
                                {
                                    var id = item["address_limitation"]["listing_id"].ToSafeString();
                                    var link = "https://www.airbnb.com/rooms/" + id;
                                    var status = item["listing_state"].ToSafeString();

                                    var property = ScrapeListingDetails(hostId, link, status);

                                    if (property != null)
                                    {
                                        Core.API.Airbnb.Properties.AddOrUpdate(property);
                                        if (isImporting)
                                        {
                                            count += 1;
                                            GlobalVariables.ImportedPropertyCount = count;
                                            ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                        }
                                    }
                                }
                            }

                            sresult.TotalRecords = count;
                            sresult.Success = true;
                            sresult.Message = "Operation successful!";
                        }
                    }
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e);
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }
            return sresult;
        }

        public Property ScrapeListingDetails(int hostId, string url, string status)
        {
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        var idNode = xd.SelectSingleNode("//div[@data-hypernova-key and not(contains(@data-hypernova-key,'login'))]");

                        if (idNode != null)
                        {
                            var id = idNode.GetAttributeFromNode("data-hypernova-key").ToSafeString();
                            var json = xd.SelectSingleNode("//script[@data-hypernova-key='" + id + "']").GetInnerTextFromNode().HtmlDecode().Trim();

                            if (string.IsNullOrEmpty(json))
                            {
                                //TODO: notify
                                return null;
                            }
                            else
                            {
                                json = json.Replace("//\r\n", "");
                                json = json.Replace("<!--", "");
                                json = json.Replace("-->", "");
                                var ob = JObject.Parse(json);
                                Trace.WriteLine(json);
                                JToken topNode = null;
                                if (ob["bootstrapData"]["reduxData"]["homePDP"] != null)
                                {
                                    topNode = ob["bootstrapData"]["reduxData"]["homePDP"]["listingInfo"]["listing"];

                                    var guestControlsNode = topNode["guest_controls"];

                                    var isPetsAllowed = guestControlsNode["allows_pets"].ToBoolean();
                                    var isSmokingAllowed = guestControlsNode["allows_smoking"].ToBoolean();

                                    var listingAmenitiesNode = topNode["listing_amenities"];

                                    var wifiNode = listingAmenitiesNode.FirstOrDefault(o => o["name"] != null && o["name"].ToSafeString() == "Wifi");
                                    var isWifiAvailable = wifiNode != null ? wifiNode["is_present"].ToBoolean() : false;

                                    var airconNode = listingAmenitiesNode.FirstOrDefault(o => o["name"] != null && o["name"].ToSafeString() == "Air conditioning");
                                    var isAirconditioned = airconNode != null ? airconNode["is_present"].ToBoolean() : false;

                                    var description = topNode["sectioned_description"]["summary"].ToSafeString();
                                    var minStay = topNode["min_nights"].ToInt();
                                    var listingId = topNode["id"].ToLong();
                                    var name = topNode["name"].ToSafeString();
                                    var nightlyMin = GetPrice(listingId);
                                    var bathrooms = Utilities.ExtractNumber(topNode["bathroom_label"].ToSafeString());
                                    var bedrooms = Utilities.ExtractNumber(topNode["bedroom_label"].ToSafeString());
                                    var sleeps = Utilities.ExtractNumber(topNode["bed_label"].ToSafeString());
                                    var propertyType = topNode["room_and_property_type"].ToSafeString();
                                    var accomodationType = topNode["localized_room_type"].ToSafeString();
                                    var address = topNode["p3_summary_address"].ToSafeString();

                                    TimeSpan checkin = new TimeSpan();
                                    TimeSpan checkout = new TimeSpan();
                                    checkin = Utilities.ExtractCheckInTime(topNode["localized_check_in_time_window"].ToSafeString());
                                    checkout = Utilities.ExtractCheckOutTime(topNode["localized_check_out_time"].ToSafeString());
                                    var reviewCount = topNode["visible_review_count"].ToInt();
                                    DateTime lastReviewAt;
                                    var sortedReviewsNode = (JArray)topNode["sorted_reviews"];
                                    if (sortedReviewsNode != null) lastReviewAt = sortedReviewsNode[0]["created_at"].ToDateTime();

                                    var longitude = topNode["lng"].ToSafeString();
                                    var latitude = topNode["lat"].ToSafeString();

                                    return new Property()
                                    {
                                        HostId = hostId,
                                        SiteType = 1,
                                        Address = address,
                                        ListingId = listingId,
                                        AccomodationType = accomodationType,
                                        Bathrooms = bathrooms,
                                        Bedrooms = bedrooms,
                                        Description = description.HtmlDecode(),
                                        Internet = isWifiAvailable.ToString(),
                                        ListingUrl = url,
                                        MinStay = minStay,
                                        Pets = isPetsAllowed.ToString(),
                                        PriceNightlyMin = nightlyMin.ToSafeString(),
                                        PropertyType = propertyType,
                                        Reviews = reviewCount,
                                        Sleeps = sleeps,
                                        Smoking = isSmokingAllowed.ToString(),
                                        Name = name,
                                        Longitude = longitude,
                                        Latitude = latitude,
                                        AirCondition = isAirconditioned.ToString(),
                                        Status = status,
                                        CheckInTime = checkin,
                                        CheckOutTime = checkout
                                    };
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }
            return null;
        }

        public Property AirBnBScrapeProperties(string url, string status)
        {
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        string longitude = "";
                        string latitude = "";
                        try
                        {
                            latitude = xd.SelectSingleNode("//meta[@property='airbedandbreakfast:location:latitude']").GetAttributeFromNode("content");
                            longitude = xd.SelectSingleNode("//meta[@property='airbedandbreakfast:location:longitude']").GetAttributeFromNode("content");
                        }
                        catch { }

                        var idNode = xd.SelectSingleNode("//div[@data-hypernova-key and not(contains(@data-hypernova-key,'login'))]");
                        var json = "";
                        if (idNode != null)
                        {
                            var id = idNode.GetAttributeFromNode("data-hypernova-key").ToSafeString();
                            json = xd.SelectSingleNode("//script[@data-hypernova-key='" + id + "']").GetInnerTextFromNode().HtmlDecode().Trim();
                        }

                        if (string.IsNullOrEmpty(json))
                        {
                            //TODO: notify
                            return null;
                        }

                        json = json.Replace("//\r\n", "");
                        json = json.Replace("<!--", "");
                        json = json.Replace("-->", "");
                        var ob = JObject.Parse(json);
                        JToken topNode = null;
                        if (ob["slideshowProps"] != null)
                        {
                            topNode = ob["slideshowProps"];
                        }
                        else
                        {
                            topNode = ob["bootstrapData"]["slideshowProps"];
                        }
                        if (topNode == null)
                        {
                            topNode = ob["bootstrapData"]["reduxData"]["marketplacePdp"]["listingInfo"]["listing"];
                            if (topNode != null)
                                return PropertyDetailV3(topNode, url, status);
                        }
                        if (topNode == null)
                        {
                            topNode = ob["bootstrapData"];
                        }
                        if (string.IsNullOrEmpty(latitude))
                        {
                            latitude = topNode["listing"]["lat"].ToSafeString();
                            longitude = topNode["listing"]["lng"].ToSafeString();
                        }

                        var description = topNode["listing"]["summary"].ToSafeString();
                        var amenitiesArr = (JArray)topNode["listing"]["listing_amenities"];

                        var petNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "12").FirstOrDefault();
                        var pets = "Not allowed";
                        var petsbool = petNode["is_present"].ToBoolean();
                        if (petsbool == true)
                            pets = "Allowed";

                        var wheelChairNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "6").FirstOrDefault();
                        var wheelchair = "Not allowed";
                        var wheelchairbool = wheelChairNode["is_present"].ToBoolean();
                        if (wheelchairbool == true)
                            wheelchair = "Allowed";

                        var swimmingPoolNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "7").FirstOrDefault();
                        var swimmingPool = "No";
                        var swimmingPoolbool = swimmingPoolNode["is_present"].ToBoolean();
                        if (swimmingPoolbool == true)
                            swimmingPool = "Yes";

                        var smokingNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "11").FirstOrDefault();
                        var smoking = "Not allowed";
                        var smokingbool = smokingNode["is_present"].ToBoolean();
                        if (smokingbool == true)
                            smoking = "Allowed";

                        var airConditionNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "5").FirstOrDefault();
                        var airCondition = "No";
                        var airConditionbool = airConditionNode["is_present"].ToBoolean();
                        if (airConditionbool == true)
                            airCondition = "Yes";

                        var internetNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "4").FirstOrDefault();
                        var internet = "No";
                        var internetbool = internetNode["is_present"].ToBoolean();
                        if (internetbool == true)
                            internet = "Yes";

                        var minStay = topNode["listing"]["min_nights"].ToInt();
                        var listingId = topNode["listing"]["id"].ToLong();
                        var title = topNode["listing"]["name"].ToSafeString();
                        var nightlyMin = GetPrice(listingId);

                        var bathrooms = 0;
                        var bedrooms = 0;
                        var sleeps = 0;
                        var propertyType = "";
                        var accomodationType = "";

                        var spaceArray = (JArray)topNode["listing"]["space_interface"];
                        var bth = spaceArray.Where(f => f["label"].ToSafeString() == "Bathrooms:").FirstOrDefault();
                        if (bth != null)
                            bathrooms = bth["value"].ToInt();

                        var bed = spaceArray.Where(f => f["label"].ToSafeString() == "Bedrooms:").FirstOrDefault();
                        if (bed != null)
                            bedrooms = bed["value"].ToInt();

                        var propType = spaceArray.Where(f => f["label"].ToSafeString() == "Property type:").FirstOrDefault();
                        if (propType != null)
                            propertyType = propType["value"].ToSafeString();

                        var accomType = spaceArray.Where(f => f["label"].ToSafeString() == "Room type:").FirstOrDefault();
                        if (accomType != null)
                            accomodationType = accomType["value"].ToSafeString();

                        var slps = spaceArray.Where(f => f["label"].ToSafeString() == "Beds:").FirstOrDefault();
                        if (slps != null)
                            sleeps = slps["value"].ToInt();

                        TimeSpan checkin = new TimeSpan();
                        TimeSpan checkout = new TimeSpan();
                        var chkin = spaceArray.Where(f => f["label"].ToSafeString() == "Check In:").FirstOrDefault();
                        if (chkin != null)
                            checkin = Utilities.ExtractCheckInTime(chkin["value"].ToSafeString());

                        var chkout = spaceArray.Where(f => f["label"].ToSafeString() == "Check Out:").FirstOrDefault();
                        if (chkout != null)
                            checkout = Utilities.ExtractCheckOutTime(chkout["value"].ToSafeString());

                        var reviews = topNode["listing"]["review_details_interface"]["review_count"].ToInt();
                        DateTime lastReviewAt = new DateTime();
                        var arrReviews = (JArray)topNode["listing"]["sorted_reviews"];
                        if (arrReviews != null)
                        {
                            lastReviewAt = arrReviews[0]["created_at"].ToDateTime();
                        }

                        return new Property()
                        {
                            ListingId = listingId,
                            AccomodationType = accomodationType,
                            Bathrooms = bathrooms,
                            Bedrooms = bedrooms,
                            Description = description.HtmlDecode(),
                            Internet = internet,
                            ListingUrl = url,
                            MinStay = minStay,
                            Pets = pets,
                            PriceNightlyMin = nightlyMin.ToSafeString(),
                            PropertyType = propertyType,
                            Reviews = reviews,
                            Sleeps = sleeps,
                            Smoking = smoking,
                            Name = title.HtmlDecode(),
                            WheelChair = wheelchair,
                            Longitude = string.IsNullOrEmpty(longitude) ? null : longitude,
                            Latitude = string.IsNullOrEmpty(latitude) ? null : latitude,
                            AirCondition = airCondition,
                            SwimmingPool = swimmingPool,
                            Status = status,
                            CheckInTime = checkin,
                            CheckOutTime = checkout
                        };
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }
            return null;
        }

        public Property PropertyDetailV3(JToken topNode, string url, string status)
        {
            try
            {
                string longitude = "";
                string latitude = "";
                latitude = topNode["lat"].ToSafeString();
                longitude = topNode["lng"].ToSafeString();

                var description = topNode["summary"].ToSafeString();
                var amenitiesArr = (JArray)topNode["listing_amenities"];

                var petNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "12").FirstOrDefault();
                var pets = "Not allowed";
                var petsbool = petNode["is_present"].ToBoolean();
                if (petsbool == true)
                    pets = "Allowed";

                var wheelChairNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "6").FirstOrDefault();
                var wheelchair = "Not allowed";
                var wheelchairbool = wheelChairNode["is_present"].ToBoolean();
                if (wheelchairbool == true)
                    wheelchair = "Allowed";

                var swimmingPoolNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "7").FirstOrDefault();
                var swimmingPool = "No";
                var swimmingPoolbool = swimmingPoolNode["is_present"].ToBoolean();
                if (swimmingPoolbool == true)
                    swimmingPool = "Yes";

                var smokingNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "11").FirstOrDefault();
                var smoking = "Not allowed";
                var smokingbool = smokingNode["is_present"].ToBoolean();
                if (smokingbool == true)
                    smoking = "Allowed";

                var airConditionNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "5").FirstOrDefault();
                var airCondition = "No";
                var airConditionbool = airConditionNode["is_present"].ToBoolean();
                if (airConditionbool == true)
                    airCondition = "Yes";

                var internetNode = amenitiesArr.Where(f => f["id"].ToSafeString() == "4").FirstOrDefault();
                var internet = "No";
                var internetbool = internetNode["is_present"].ToBoolean();
                if (internetbool == true)
                    internet = "Yes";

                var minStay = topNode["min_nights"].ToInt();
                var listingId = topNode["id"].ToLong();
                var title = topNode["name"].ToSafeString();
                var nightlyMin = GetPrice(listingId);

                var bathrooms = 0;
                var bedrooms = 0;
                var sleeps = 0;
                var propertyType = "";
                var accomodationType = "";
                TimeSpan checkin = new TimeSpan();
                TimeSpan checkout = new TimeSpan();

                var spaceArray = (JArray)topNode["space_interface"];
                if (spaceArray != null)
                {
                    var bth = spaceArray.Where(f => f["label"].ToSafeString() == "Bathrooms:").FirstOrDefault();
                    if (bth != null)
                        bathrooms = bth["value"].ToInt();

                    var bed = spaceArray.Where(f => f["label"].ToSafeString() == "Bedrooms:").FirstOrDefault();
                    if (bed != null)
                        bedrooms = bed["value"].ToInt();

                    var propType = spaceArray.Where(f => f["label"].ToSafeString() == "Property type:").FirstOrDefault();
                    if (propType != null)
                        propertyType = propType["value"].ToSafeString();

                    var accomType = spaceArray.Where(f => f["label"].ToSafeString() == "Room type:").FirstOrDefault();
                    if (accomType != null)
                        accomodationType = accomType["value"].ToSafeString();

                    var slps = spaceArray.Where(f => f["label"].ToSafeString() == "Beds:").FirstOrDefault();
                    if (slps != null)
                        sleeps = slps["value"].ToInt();

                    var chkin = spaceArray.Where(f => f["label"].ToSafeString() == "Check In:").FirstOrDefault();
                    if (chkin != null)
                        checkin = Utilities.ExtractCheckInTime(chkin["value"].ToSafeString());

                    var chkout = spaceArray.Where(f => f["label"].ToSafeString() == "Check Out:").FirstOrDefault();
                    if (chkout != null)
                        checkout = Utilities.ExtractCheckOutTime(chkout["value"].ToSafeString());
                }
                else
                {
                    bathrooms = Utilities.ExtractNumber(topNode["bathroom_label"].ToSafeString());
                    bedrooms = Utilities.ExtractNumber(topNode["bedroom_label"].ToSafeString());
                    propertyType = topNode["room_and_property_type"].ToSafeString();
                    sleeps = Utilities.ExtractNumber(topNode["bed_label"].ToSafeString());
                    checkin = Utilities.ExtractCheckInTime(topNode["localized_check_in_time_window"].ToSafeString());
                    checkout = Utilities.ExtractCheckOutTime(topNode["localized_check_out_time"].ToSafeString());
                }

                var reviews = topNode["review_details_interface"]["review_count"].ToInt();

                return new Property()
                {
                    ListingId = listingId,
                    AccomodationType = accomodationType,
                    Bathrooms = bathrooms,
                    Bedrooms = bedrooms,
                    Description = description.HtmlDecode(),
                    Internet = internet,
                    ListingUrl = url,
                    MinStay = minStay,
                    Pets = pets,
                    PriceNightlyMin = nightlyMin.ToSafeString(),
                    PropertyType = propertyType,
                    Reviews = reviews,
                    Sleeps = sleeps,
                    Smoking = smoking,
                    Name = title.HtmlDecode(),
                    WheelChair = wheelchair,
                    Longitude = longitude,
                    Latitude = latitude,
                    AirCondition = airCondition,
                    SwimmingPool = swimmingPool,
                    Status = status,
                    CheckInTime = checkin,
                    CheckOutTime = checkout
                };
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }
            return null;
        }


        public string GetPrice(long propertyId, string checkin = "", string checkout = "")
        {
            try
            {
                string url = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?guests=1&listing_id={0}&_format=for_dateless_booking_info_on_web_p3&_interaction_type=pageload&_intents=p3_book_it&_parent_request_uuid=5e7baaa5-bf39-4ee6-8319-37f930b2ab5e&_p3_impression_id=p3_1490352774_Ymuf7NI5Bk956LLB&show_smart_promotion=0&number_of_adults=1&number_of_children=0&number_of_infants=0&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", propertyId);
                if (!string.IsNullOrEmpty(checkout) && !string.IsNullOrEmpty(checkin))
                {
                    url = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?guests=1&listing_id={0}&_format=for_detailed_booking_info_on_web_p3_with_message_data&_interaction_type=pageload&_intents=p3_book_it&_parent_request_uuid=07a8f796-adcb-4863-a0ca-5ae04ef06d0e&_p3_impression_id=p3_1490559375_zW9jEkRLeySfE5eH&show_smart_promotion=0&check_in={1}&check_out={2}&number_of_adults=1&number_of_children=0&number_of_infants=0&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", propertyId, checkin, checkout);
                }
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var price = ob["pricing_quotes"][0]["rate"]["amount"].ToSafeString();
                    var currency = ob["pricing_quotes"][0]["rate"]["currency"].ToSafeString();
                    return price + " " + currency;
                }
            }
            catch
            {

            }
            return string.Empty;
        }

        #endregion



        #region SHARED
        public bool IsAvailableForDate(string listingId, DateTime startDate, DateTime endDate)
        {
            try
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/ajax_special_offer_dates_available?hosting_id={0}&start_date={1}&end_date={2}", listingId, startDate.Date.ToString("yyyy-MM-dd"), endDate.Date.ToString("yyyy-MM-dd"));
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                CommPage.RequestHeaders.Add("X-CSRF-Token", token);

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob["available"].ToSafeString().ToBoolean();
                }
            }
            catch { }
            return false;
        }
        #endregion

        #region COOKIES
        public static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }
        public bool ValidateTokenAndLoadCookies(string token)
        {
            var hasValidCookies = LoadCookies(token);
            if (hasValidCookies)
            {
                string url = "https://www.airbnb.com/users/edit";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var userId = xd.SelectSingleNode("//input[@name='user_id']").GetAttributeFromNode("value");
                            if (!string.IsNullOrEmpty(userId)) return true;
                            else
                            {
                                using (var db = new ApplicationDbContext())
                                {
                                    var entity = db.HostSessionCookies.Where(x => x.Token == token).FirstOrDefault();
                                    if (entity != null)
                                    {
                                        entity.IsActive = false;
                                        db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception) { }
            }
            return false;
        }
        public bool LoadCookies(string token)
        {
            try
            {
                var result = false;
                var session = Core.API.Airbnb.HostSessionCookies.UserSessionForId(token);
                if (session != null)
                {
                    try
                    {
                        //var cookies = JsonConvert.DeserializeObject<List<Cookie>>(session.Cookies);
                        //CookieCollection cc = new CookieCollection();
                        //cookies.ForEach(cookie => { cc.Add(cookie); });
                        //CookieContainer cont = new CookieContainer();
                        //cont.Add(cc);
                        var cookies = DeSerializeCookiesFromString(session.Cookies);
                        commPage.COOKIES = cookies;
                        result = true;
                    }
                    catch (Exception e)
                    {
                        ElmahLogger.LogInfo(string.Format("Error {0}", e));
                    }
                }
                else
                {
                    commPage.COOKIES = null;
                }
                return result;
            }
            catch { }
            return false;
        }

        private static CookieContainer DeSerializeCookiesFromString(string cookies)
        {
            try
            {
                using (var stream = new MemoryStream(Convert.FromBase64String(cookies)))
                {
                    return (CookieContainer)new BinaryFormatter().Deserialize(stream);
                }
            }
            catch (Exception e)
            {
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                return new CookieContainer();
            }
        }

        #endregion

        #region DISPOSE
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
        #endregion

        #region ProcessCatchaLock
        public bool ProcessCaptchaLock(CaptchaViewModel captchaModel)
        {

            try
            {
                string airlockUrl = commPage.Uri.AbsoluteUri;
                var u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=v1", captchaModel.LockId);
                var pJson = "";

                pJson = string.Format("{{\"friction\":\"captcha\",\"friction_data\":{{\"response\":{{\"captcha_response\":\"{0}\"}}}},\"enable_throw_errors\":true}}", captchaModel.CaptchaResponse);



                ElmahLogger.LogInfo(string.Format("ProcessCaptchaLock>> existing HTML>> {0} ======== POSTJSON>> {1}", commPage.Html, pJson));

                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();
                commPage.Referer = "https://www.airbnb.com/airlock?al_id=" + captchaModel.LockId;
                commPage.RequestURL = u;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                commPage.JsonStringToPost = pJson;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json");
                //CommPage.RequestHeaders.Add("Content-Type", "application/json");
                commPage.RequestHeaders.Add("X-CSRF-Token", token);

                //commPage.Referer = airlockUrl;
                ProcessRequest(commPage);
                ElmahLogger.LogInfo(string.Format("ProcessCaptchaLock>> HTML>> {0} ======== ISVALIDRESPONSE>> {1}", commPage.Html, commPage.IsValid));

                if (commPage.StatusCode == HttpStatusCode.OK)
                {
                    //If json status = 2 then the captcha is supposed to be true
                    commPage.RequestURL = "https://www.airbnb.com/airlock/redirect?url=https://www.airbnb.com/login&flash=true";
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);


                    commPage.RequestURL = "https://www.airbnb.com/authenticate";

                    var hiddenFields = new Dictionary<string, string>(); //BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "class", "login-form ");
                    cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    token = cc["_csrf_token"].Value.DecodeURL();

                    hiddenFields = new Dictionary<string, string>();
                    hiddenFields.Add("email", captchaModel.Email);
                    hiddenFields.Add("password", captchaModel.Password);

                    hiddenFields["from"] = "email_login";
                    hiddenFields["airlock_id"] = "";
                    hiddenFields["authenticity_token"] = token;
                    hiddenFields["utf8"] = "✓";

                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");

                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "https://www.airbnb.com/login";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);


                    return true;

                }
            }
            catch (Exception ex)
            {

                //Core.Helpers.ElmahLogger.LogError(ex, GetCurrentMethod());
            }
            return false;

        }
        #endregion

        #region ANALYTICS

        public List<Core.Models.Analytics> GetAnalyticsForMonth(string latitude, string longitude, DateTime date, int rooms, string excludedListings)
        {
            commPage.RequestURL = string.Format("{0}/api/get/monthly/analytics?latitude={1}&longitude={2}&currentDate={3}&propertycountgetavailable=true&averagepriceonlyavailable=false&rooms={4}{5}", GlobalVariables.ApiAnalyticsUrl, latitude.ToDouble(), longitude.ToDouble(), date.ToString("yyyy-MM-dd"), rooms, excludedListings);
            commPage.ReqType = CommunicationPage.RequestType.GET;

            ProcessRequest(commPage);

            if (commPage.IsValid)
            {
                //var ob = JObject.Parse(commPage.Html);
            }
                return new List<Analytics>();

        }

        public Core.Models.Analytics GetAnalytics(string latitude, string longitude, DateTime date, int rooms, string excludedListings,int siteType,bool includeSiteByOnly)
        {
            try
            {
                commPage.RequestURL = string.Format("{0}/api/airbnb/getanalytics?includeSiteByOnly={1}&siteType={2}&latitude={3}&longitude={4}&date={5}&propertycountgetavailable=true&averagepriceonlyavailable=false&rooms={6}{7}", GlobalVariables.ApiAnalyticsUrl, includeSiteByOnly, siteType, latitude.ToDouble(), longitude.ToDouble(), date.ToString("yyyy-MM-dd"), rooms, excludedListings);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    Core.Models.Analytics analytics = new Core.Models.Analytics()
                    {
                        AvailabilityRateByDate = ob["analytics"]["AvailabilityRateByDate"].ToDouble(),
                        AvailabilityRateByDateForXRooms = ob["analytics"]["AvailabilityRateByDateForXRooms"].ToDouble(),
                        AvailabilityRateByMonth = ob["analytics"]["AvailabilityRateByMonth"].ToDouble(),
                        AvailabilityRateByMonthForXRooms = ob["analytics"]["AvailabilityRateByMonthForXRooms"].ToDouble(),
                        AveragePriceByDate = ob["analytics"]["AveragePriceByDate"].ToDouble(),
                        AveragePriceByDateForXRooms = ob["analytics"]["AveragePriceByDateForXRooms"].ToDouble(),
                        AveragePriceByMonth = ob["analytics"]["AveragePriceByMonth"].ToDouble(),
                        AveragePriceByMonthForXRooms = ob["analytics"]["AveragePriceByMonthForXRooms"].ToDouble(),
                        OccupancyRateByDate = ob["analytics"]["OccupancyRateByDate"].ToDouble(),
                        OccupancyRateByDateForXRooms = ob["analytics"]["OccupancyRateByDateForXRooms"].ToDouble(),
                        OccupancyRateByMonth = ob["analytics"]["OccupancyRateByMonth"].ToDouble(),
                        OccupancyRateByMonthForXRooms = ob["analytics"]["OccupancyRateByMonthForXRooms"].ToDouble(),
                        PropertyCountByDate = ob["analytics"]["PropertyCountByDate"].ToInt(),
                        PropertyCountByDateForXRooms = ob["analytics"]["PropertyCountByDateForXRooms"].ToInt(),
                        TotalPropertyCountByDate = ob["analytics"]["TotalPropertyCountByDate"].ToInt(),
                        TotalPropertyCountByDateForXRooms = ob["analytics"]["TotalPropertyCountByDateForXRooms"].ToInt(),
                        PropertyCountByBedrooms = ob["analytics"]["PropertyCountByBedrooms"].ToString(),
                        NearPropertyCountByBedrooms= ob["analytics"]["NearPropertyCountByBedrooms"].ToString()
                    };

                    return analytics;
                }
            }
            catch (Exception e)
            {
                return new Core.Models.Analytics();
            }

            return new Core.Models.Analytics();
        }

        #endregion
        public List<Comparables> SearchComparables(string longitude,string latitude,DateTime date,string bedrooms,string miles,bool isPrivateRoomOnly,bool isAirbnbOnly)
        {
            commPage.RequestURL = string.Format("{0}/api/searchcomparables?longitude={1}&latitude={2}&date={3}&bedrooms={4}&miles={5}&isPrivateRoomOnly={6}&isAirbnbOnly={7}", GlobalVariables.ApiAnalyticsUrl, longitude, latitude, date.ToString("yyyy-MM-dd"), bedrooms, miles, isPrivateRoomOnly,isAirbnbOnly);
            commPage.ReqType = CommunicationPage.RequestType.GET;
            ProcessRequest(commPage);
            var ob = JObject.Parse(commPage.Html);
            var json = ob["comparables"].ToString();
            var model = JsonConvert.DeserializeObject<List<Comparables>>(json);
            return model;
        }

        public List<CityPropertyViewModel> GetCityProperties(int companyId)
        {
            commPage.RequestURL = string.Format("{0}/api/getcityproperties?companyId={1}", GlobalVariables.ApiAnalyticsUrl,companyId);
            commPage.ReqType = CommunicationPage.RequestType.GET;
            ProcessRequest(commPage);
            var ob = JObject.Parse(commPage.Html);
            var json = ob["cityProperties"].ToString();
            var model = JsonConvert.DeserializeObject<List<CityPropertyViewModel>>(json);
            return model;
        }
        public bool SearchLocation(string location,int companyId,int userId)
        {
            commPage.RequestURL = string.Format("{0}/api/searchlocation", GlobalVariables.ApiAnalyticsUrl);
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = new Dictionary<string, string>();
            commPage.Parameters.Add("location", location);
            commPage.Parameters.Add("companyId", companyId.ToString());
            commPage.Parameters.Add("userId", userId.ToString());
           
            ProcessRequest(commPage);
            var ob = JObject.Parse(commPage.Html);
            return ob["success"].ToBoolean();
        }
        #region COMPARABLES
        public ComparablesAndBedrooms GetComparables(bool isPrivateRoomOnly, int siteType,bool includeSiteByOnly,string latitude, string longitude, int rooms, DateTime date, 
            int miles = 40, int withinMeters = 100000)
        {
            List<Comparables> unfilteredComparables = new List<Comparables>();
            List<ExcludedComparable> excludedComparables = new List<ExcludedComparable>();
            List<Comparables> comparables = new List<Comparables>();
            List<int> bedrooms = new List<int>();

            try
            {
                commPage.RequestURL = string.Format("{0}/api/comparables?siteType={1}&includeSiteByOnly={2}&longitude={3}&latitude={4}&rooms={5}&date={6}&miles={7}&isPrivateRoomOnly={8}&withinMeters={9}", GlobalVariables.ApiAnalyticsUrl,siteType, includeSiteByOnly, longitude, latitude, rooms, date.ToString("yyyy-MM-dd"),miles, isPrivateRoomOnly, withinMeters);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var arrays = JArray.Parse(ob["comparables"].ToString());
                    //var bed = JArray.Parse(ob["bedrooms"].ToString()).ToList();
                    //foreach(var b in bed)
                    //{
                    //    bedrooms.Add(b.ToInt());
                    //}
                    foreach (var arr in arrays)
                    {
                        bool excluded = false;
                        string listingId = arr["ListingId"].ToString();
                        double distance = arr["Distance"].ToString()
                            .Replace("mile away", "").Replace("miles away", "").ToDouble();
                        ExcludedComparable comparableFound = new ExcludedComparable();

                        using (var db = new ApplicationDbContext())
                        {
                            comparableFound = db.ExcludedComparables.Where(x => x.ListingId == listingId).FirstOrDefault();

                            if (comparableFound != null)
                                excluded = true;
                        }
                      
                        if (excluded)
                        {
                            excludedComparables.Add(comparableFound);
                        }
                       
                        else if (!excluded && distance <= miles)
                        {
                            comparables.Add(new Comparables
                            {   Ratings = arr["Ratings"].ToSafeString(),
                                AverageOccupancies = arr["AverageOccupancies"].ToString(),
                                Bathrooms = arr["Bathrooms"].ToDouble(),
                                Bedrooms = arr["Bedrooms"].ToInt(),
                                Distance = arr["Distance"].ToString(),
                                Price = arr["Price"].ToString(),
                                HostAbout = arr["HostAbout"].ToString(),
                                HostAddress = arr["HostAddress"].ToString(),
                                HostImageUrl = arr["HostImageUrl"].ToString(),
                                HostMemberSince = arr["HostMemberSince"].ToString(),
                                HostName = arr["HostName"].ToString(),
                                Latitude = arr["Latitude"].ToString(),
                                ListingId = listingId,
                                ListingImage = arr["ListingImage"].ToString(),
                                ListingName = arr["ListingName"].ToString(),
                                ListingUrl = arr["ListingUrl"].ToString(),
                                Longitude = arr["Longitude"].ToString(),
                                Site = arr["Site"].ToString(),
                                IsAvailable = arr["IsAvailable"].ToBoolean(),
                                Fees = arr["Fees"].ToString(),
                                IsPrivateRoom = arr["IsPrivateRoom"].ToBoolean(),
                                DateUpdated= arr["DateUpdated"].ToString(),
                            }) ;
                        }

                        unfilteredComparables.Add(new Comparables
                        {
                            Bathrooms = arr["Bathrooms"].ToDouble(),
                            Bedrooms = arr["Bedrooms"].ToInt(),
                            Distance = arr["Distance"].ToString(),
                            Price = arr["Price"].ToString(),
                            HostAbout = arr["HostAbout"].ToString(),
                            HostAddress = arr["HostAddress"].ToString(),
                            HostImageUrl = arr["HostImageUrl"].ToString(),
                            HostMemberSince = arr["HostMemberSince"].ToString(),
                            HostName = arr["HostName"].ToString(),
                            Latitude = arr["Latitude"].ToString(),
                            ListingId = listingId,
                            ListingImage = arr["ListingImage"].ToString(),
                            ListingName = arr["ListingName"].ToString(),
                            ListingUrl = arr["ListingUrl"].ToString(),
                            Longitude = arr["Longitude"].ToString(),
                            Site = arr["Site"].ToString(),
                            IsAvailable = arr["IsAvailable"].ToBoolean(),
                             DateUpdated = arr["DateUpdated"].ToString()
                        });
                    }
                    return new ComparablesAndBedrooms()
                    {
                        Bedrooms = bedrooms,
                        Comparables = comparables.OrderBy(x => x.ListingName).ToList(),
                        ExcludedComparables = excludedComparables.OrderBy(x => x.ListingId).ToList(),
                        UnfilteredComparables = unfilteredComparables.OrderBy(x => x.ListingName).ToList()
                    };
                }
            }
            catch (Exception e)
            {
                return new ComparablesAndBedrooms();
            }

            return new ComparablesAndBedrooms();
        }

        #endregion
    }
}
