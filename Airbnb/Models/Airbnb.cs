﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbnb.Models
{
    #region LOGIN
    public class LoginResult
    {
        public bool Success { get; set; }
        public string SessionToken { get; set; }
        public AirlockViewModel AirLock { get; set; }
        public CaptchaViewModel Captcha { get; set; }
    }
    #endregion

    #region AIRLOCK
    public class AirlockViewModel
    {
        public string Code { get; set; }
        public string LockId { get; set; }
        public string UserId { get; set; }
        public string PhoneId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string ProfileImageUrl { get; set; }
        public bool IsNew { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public AirlockChoice SelectedChoice { get; set; }
    }
    public class CaptchaViewModel
    {
        public string SiteKey { get; set; }
        public string LockId { get; set; }
        public string CaptchaResponse { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
    public enum AirlockChoice
    {
        SMS,
        Call,
        Email
    }
    #endregion

    #region SCRAPE RESULT
    public class ScrapeResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int TotalRecords { get; set; }
    }
    #endregion
    
    #region SPECIAL OFFER

    public class SpecialOfferSummary
    {
        public bool IsSuccess { get; set; }
        public decimal Subtotal { get; set; }
        public decimal GuestPays { get; set; }
        public decimal HostEarns { get; set; }
        public bool IsAvailable { get; set; }
    }

    #endregion
}
