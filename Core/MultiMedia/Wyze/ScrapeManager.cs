﻿using BaseScrapper;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Helpers;
using Core.Helpers.Devices;
using Core.Models;
using Core.Models.MultiMedia;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Wyze
{
        public class ScrapeManager : CommunicationEngine, IDisposable
    {
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://auth-prod.api.wyze.com");
        }

        public CommunicationPage commPage = null;
        #region LOGIN
        //JM 6/8/2020 maybe we can use this on future
        //public string ComputeHash(string input, HashAlgorithm algorithm)
        //{
        //    Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

        //    Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

        //    return BitConverter.ToString(hashedBytes);
        //}
        public string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

       
        public LogInResult LogIn(string email, string password,string url,int companyId,bool isImport =true)
        {
            LogInResult result = new LogInResult();
            result.Success = false;
            try
            {
                    //var p = 11a9051921a5a1163b88ffef146b5d78;
                    commPage.RequestURL = "https://auth-prod.api.wyze.com/user/login";
                    commPage.JsonStringToPost = string.Format("{{\"email\":\"{0}\",\"password\":\"{1}\"}}", email, MD5Hash(MD5Hash(MD5Hash(password))));
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Phone-Id", "c1f8fb8c-1442-4782-89f3-f7ffce28b061");
                    commPage.RequestHeaders.Add("X-API-Key", "RckMFKbsds5p6QY3COEXc2ABwNTYY0q18ziEiSEm");

                    ProcessRequest(commPage);
                    HtmlLogs.Add("Wyze Login Email:" + email + " " + commPage.IsValid + " " + commPage.Html);
                 
                
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    result.Success = true;
                    result.AccessToken = ob["access_token"].ToString();
                    result.AccountId = ob["user_id"].ToString();

                    var accountId = Devices.AddAccount(new Core.Database.Entity.DeviceAccount() { Username = email, Password = password, SiteType = (int)Core.Enumerations.DevicesSiteType.Wyze,CompanyId=companyId });
                    //GetDevices(result.AccessToken);
                    HtmlLogs.Add("Login json:" + accountId + " " + commPage.Html);
                    if (isImport)
                    {
                            DeviceList(result.AccessToken, accountId);
                        Videos.Delete(accountId, DateTime.Now.AddDays(-2));
                        GetEventList(result.AccessToken, url, accountId);
                       
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Logger.WriteLog(string.Format("Error on Blink log in with username = '{0}', message = {1}, innerexception = {2}, stacktrace = {3}", email, e.Message, e.InnerException, e.StackTrace));
                result.Success = false;
                return result;
            }

            return result;
        }

        #endregion

        #region GetDevices
        public void DeviceList(string token,int accountId)
        {
            commPage.RequestURL = "https://api.wyzecam.com:8443/app/device/get_device_list";
            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
            commPage.JsonStringToPost = string.Format("{{\"sv\":\"01463873df804629b15694df13126d31\",\"sc\":\"01dd431d098546f9baf5233724fa2ee2\",\"ts\":1525365683583,\"app_ver\":\"com.hualai.WyzeCam___1.1.52\",\"phone_id\":\"bc151f39-787b-4871-be27-5a20fd0a1937\",\"access_token\":\"{0}\"}}",token);
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("access_token", token);
            commPage.RequestHeaders.Add("apikey", "nHtOAABMsnTbOmg74g3zBsFuHx4iVi5G");
            commPage.RequestHeaders.Add("appinfo", "wyze_android_2.10.80");
            commPage.RequestHeaders.Add("phoneid", "c1f8fb8c-1442-4782-89f3-f7ffce28b061");
            commPage.RequestHeaders.Add("requestid", "9bbc773ead0d839185ce83e7cc8ca48b");
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(commPage.Html);
                var data = (JArray)ob["data"]["device_info_list"];
                foreach(var device in data)
                {
                    //JM 6/8/2020
                    if (device["product_type"].ToString() == "Camera")
                    {
                        var mac = device["mac"].ToString();
                        var name = device["nickname"].ToString();
                        var product_type = device["product_type"].ToString();
                        var localDeviceId = Devices.AddDevice(new Device() { DeviceId = mac, Name = name, AccountId = accountId });
                    }

                }
                HtmlLogs.Add("Device List:" + accountId + " " + commPage.Html);
            }
        }
        #endregion

        #region GetUserDetails
        void GetUserDetails(string token) {
            commPage.RequestURL = "https://wyze-platform-service.wyzecam.com/app/v2/platform/get_user_profile";
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("access_token", token);
            commPage.RequestHeaders.Add("apikey", "nHtOAABMsnTbOmg74g3zBsFuHx4iVi5G");
            commPage.RequestHeaders.Add("appinfo", "wyze_android_2.10.80");
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(commPage.Html);
            }
        }
        #endregion

        #region Get Event List
        public async Task<bool> GetEventList(string token,string Url,int accountId)
        {
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var dateStart = DateTime.Now.AddDays(-1);
                var dateEnd = DateTime.Now.AddDays(1);
                var currentDate = DateTime.Now;

                var begintime = (long)(dateStart - new DateTime(1970, 1, 1)).TotalMilliseconds;
                var endtime = (long)(dateEnd - new DateTime(1970, 1, 1)).TotalMilliseconds;
                var currenttime = (long)(currentDate - new DateTime(1970, 1, 1)).TotalMilliseconds;

                commPage.RequestURL = "https://api.wyzecam.com/app/v2/device/get_event_list";
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                commPage.JsonStringToPost = string.Format("{{\"access_token\":\"{0}\",\"app_name\":\"com.hualai\",\"app_ver\":\"com.hualai___2.10.80\",\"app_version\":\"2.10.80\",\"phone_id\":\"c1f8fb8c-1442-4782-89f3-f7ffce28b061\",\"phone_system_type\":\"2\",\"sc\":\"a626948714654991afd3c0dbd7cdb901\",\"sv\":\"bdcb412e230049c0be0916e75022d3f3\",\"ts\":{1},\"device_mac_list\":[],\"event_value_list\":[],\"event_tag_list\":[],\"event_type\":\"1\",\"begin_time\":{2},\"end_time\":{3},\"count\":20,\"order_by\":2}}", token, currenttime, begintime, endtime);
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);
                HtmlLogs.Add("Wyze GetVideo AccountId:" + accountId + " " + commPage.Html);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var data = (JArray)ob["data"]["event_list"];
                    foreach (var videoEvent in data)
                    {
                        var mac = videoEvent["device_mac"].ToString();
                        var eventId = videoEvent["event_id"].ToString();
                        var files = (JArray)videoEvent["file_list"];
                        var event_ts = (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(videoEvent["event_ts"].ToString())).ToLocalTime();
                        var fileName = Guid.NewGuid().ToString();
                        System.IO.Directory.CreateDirectory(AppUrl.VideoRoot);
                        var saveFile = AppUrl.VideoRoot + fileName + ".mp4";
                        var url = "";
                        var thumbnailUrl = "";
                        using (WebClient client = new WebClient())
                        {
                            using (var db = new ApplicationDbContext())
                            {
                                var temp = db.Videos.Where(x => x.SiteVideoId == eventId).FirstOrDefault();
                                if (temp == null && files.Count > 1)
                                {
                                    url = files[1]["url"].ToString();
                                    thumbnailUrl = files[0]["url"].ToString();
                                    client.DownloadFile(url, saveFile);
                                    //client.DownloadFile(thumbnailUrl, saveFile.Replace("mp4", "jpg"));
                                    var VideoUrl = string.Format("/Multimedia Videos/{0}", fileName + ".mp4");
                                    var localDeviceId = db.Devices.Where(x => x.DeviceId == mac).FirstOrDefault().Id;
                                    Videos.Add(new Core.Database.Entity.Video()
                                    {
                                        ServerPath = saveFile,
                                        OriginalVideoUrl = url,
                                        VideoUrl = VideoUrl,
                                        Details = "",
                                        AccountId = accountId,
                                        DateCreated = event_ts,
                                        LocalDeviceId = localDeviceId,
                                        SiteVideoId = eventId
                                    });
                                }
                            }
                        }
                        //foreach (var file in files)
                        //{
                        //    var url = file["url"].ToString();
                        //    var fileId = file["file_id"].ToString();
                        //}
                    }
                }
                return true;
            });
            return true;
        }
        #endregion


        #region Delete
        public bool DeleteVideo(string token,string device_mac,string event_id,long ts)
        {
            try
            {
                commPage.RequestURL = "https://api.wyzecam.com/app/v2/device/delete_event_list";
                commPage.JsonStringToPost = string.Format("{{\"access_token\":\"{0}\",\"app_name\":\"com.hualai\",\"app_ver\":\"com.hualai___2.10.80\",\"app_version\":\"2.10.80\",\"phone_id\":\"c1f8fb8c-1442-4782-89f3-f7ffce28b061\",\"phone_system_type\":\"2\",\"sc\":\"a626948714654991afd3c0dbd7cdb901\",\"sv\":\"c674c2a93c904e0f9876800ed42c6cf2\",\"ts\":{1},\"event_list\":[{{\"device_mac\":\"{2}\",\"event_id_list\":[\"{3}\"]}}],\"event_type\":\"1\"}}", token, ts, device_mac, event_id);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                ProcessRequest(commPage);
                return true;
            }catch(Exception e)
            {
                HtmlLogs.Add("Wyze Delete DeviceMac:" + device_mac + " " + e.InnerException + e.Message);
                HtmlLogs.Add("Wyze Delete DeviceMac:" + device_mac + " " + commPage.Html);
                return false;
            }
        }
        #endregion
        #region NETWORKS

        public List<string> Networks(string accessToken)
        {
            try
            {
                commPage.RequestURL = "https://rest-prod.immedia-semi.com/networks";
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("TOKEN_AUTH", accessToken);

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var networks = new List<string>();
                    var data = ob["networks"];
                    foreach(var network in data)
                    {
                        networks.Add(network["id"].ToString());
                    }
                    return networks;
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }

            return new List<string>();
        }


        #endregion

        #region SCRAPEVIDEOS

        //public bool ScrapeVideos(LogInResult result,int accountId,string Url)
        //{
        //    try
        //    {
        //        bool hasMore = true;
        //        int pageNumber = 0;
        //        do
        //        {
        //            var vidUrl = $"https://rest-{result.Region}.immedia-semi.com/api/v1/accounts/{result.AccountId}/media/changed?since=2020-01-01T23:11:21+0000&page={pageNumber}";
        //            commPage.RequestURL = vidUrl;
        //            commPage.ReqType = CommunicationPage.RequestType.GET;
        //            commPage.RequestHeaders = new Dictionary<string, string>();
        //            commPage.RequestHeaders.Add("TOKEN_AUTH", result.AccessToken);
        //            ProcessRequest(commPage);

        //            if (commPage.IsValid)
        //            {
        //                var ob = JObject.Parse(commPage.Html);
        //                var videos = JArray.Parse(ob["media"].ToString());
        //                string url = "";
        //                string videoUrl = "";
        //                string deviceName = "";
        //                DateTime dateCreated = new DateTime();
        //                string saveFile = "";
        //                int newVideos = 0;
        //                string thumbnailUrl = "";
        //                string deviceId = "" ;
        //                int videoId = 0;
        //                if (videos.Count > 0)
        //                {
        //                    var leastDate = DateTime.Now.AddDays(-5);
        //                    foreach (var video in videos)
        //                    {
        //                        try
        //                        {
        //                            using (WebClient client = new WebClient())
        //                            {
        //                                client.Headers.Add("TOKEN_AUTH", result.AccessToken);
        //                                videoUrl = video["media"].ToString();
        //                                thumbnailUrl = $"https://rest-{result.Region}.immedia-semi.com" +video["thumbnail"].ToString()+".jpg";
        //                                deviceId = video["device_id"].ToString();
        //                                deviceName = video["device_name"].ToString().Replace("|", ",");
        //                                dateCreated = video["created_at"].ToDateTime();
        //                                var fileName = Guid.NewGuid().ToString();
        //                                url = $"https://rest-{result.Region}.immedia-semi.com" + videoUrl;
        //                                videoId = video["id"].ToInt();
        //                                var isdeleted = video["deleted"].ToBoolean();
        //                                //var path = System.Web.HttpContext.Current.Server.MapPath(AppUrl.VideoRoot);
        //                                System.IO.Directory.CreateDirectory(AppUrl.VideoRoot);
        //                                saveFile = AppUrl.VideoRoot + fileName+".mp4";
        //                                using(var db = new ApplicationDbContext())
        //                                {
        //                                    var temp = db.Videos.Where(x => x.OriginalVideoUrl == url).FirstOrDefault();
        //                                    if (temp == null && isdeleted==false && dateCreated.Date > leastDate.Date)
        //                                    {
        //                                        client.DownloadFile(url, saveFile);
        //                                        client.DownloadFile(thumbnailUrl, saveFile.Replace("mp4", "jpg"));
        //                                        var VideoUrl = Url + string.Format("/footage/{0}", fileName + ".mp4") + "/";
        //                                        newVideos++;
        //                                        var localDeviceId = Devices.AddDevice(new Device() { DeviceId = deviceId, Name = deviceName, AccountId = accountId });
        //                                        Videos.Add(new Core.Database.Entity.Video()
        //                                        {
        //                                            ServerPath = saveFile,
        //                                            OriginalVideoUrl = url,
        //                                            VideoUrl = VideoUrl,
        //                                            Details = "",
        //                                            AccountId = accountId,
        //                                            DateCreated = dateCreated,
        //                                            LocalDeviceId = localDeviceId,
        //                                            SiteVideoId = videoId
        //                                        });
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        catch (Exception e)
        //                        {

        //                        }
        //                    }
        //                    pageNumber++;
        //                }
        //                else
        //                {
        //                    hasMore = false;
        //                }
        //            }
        //        } while (hasMore);
        //        //Logger.WriteLog(string.Format("Scraped {0} Blink videos ({1} new videos) on {2}", videos.Count, newVideos, listingId));

        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }

        //    return false;
        //}

        #endregion

        #region DISPOSE

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion
    }
}
