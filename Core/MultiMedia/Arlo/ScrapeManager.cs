﻿using MultiMedia.Arlo;
using MultiMedia.Arlo.Models;
using BaseScrapper;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Helpers;
using Core.Models;
using Microsoft.Win32.SafeHandles;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Core.Models.MultiMedia;
using Core.Helpers.Devices;
using Core.Helper;

namespace MultiMedia.Arlo
{
    public class ScrapeManager : CommunicationEngine, IDisposable
    { 
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://arlo.netgear.com/");
        }

        public CommunicationPage commPage = null;

        #region LOGIN

        public LogInResult LogIn(string email, string password,string url,int companyId,bool isImport=true)
        {
            LogInResult result = new LogInResult();
            result.Success = false;

            try
            {
                commPage.RequestURL = "https://arlo.netgear.com";
                commPage.PersistCookies = true;

                ProcessRequest(commPage);

                commPage.RequestURL = "https://arlo.netgear.com/hmsweb/login/v2";
                commPage.JsonStringToPost = string.Format("{{\"email\":\"{0}\",\"password\":\"{1}\"}}", email, System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(password)));
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/plain, */*");
                commPage.RequestHeaders.Add("Password-Encoded", "true");
                commPage.Referer = "https://arlo.netgear.com";

                ProcessRequest(commPage);
                HtmlLogs.Add("Arlo Delete Email:" + email + " " + commPage.Html);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    var token = ob["data"]["token"].ToString();

                    result.AccessToken = token;
                    result.Success = true;
                    if (isImport)
                    {
                        var accountId = Core.Helpers.Devices.Devices.AddAccount(new Core.Database.Entity.DeviceAccount() { Username = email, Password = password, SiteType= (int)Core.Enumerations.DevicesSiteType.Arlo,CompanyId =companyId });
                            Core.Helpers.Devices.Videos.Delete(accountId, DateTime.Now.AddDays(-2));
                            ScrapeVideos(url, accountId, token, DateTime.Now.AddDays(-2), DateTime.Now);
                    }
                    return result;
                }

            }
            catch (Exception e)
            {
                Logger.WriteLog(string.Format("Error on Arlo log in with username = '{0}', message = {1}, innerexception = {2}, stacktrace = {3}", email, e.Message, e.InnerException, e.StackTrace));
                return result;
            }

            return result;
        }

        #endregion

        #region SCRAPEVIDEOS

        public async Task<bool> ScrapeVideos(string Url,int accountId,string accessToken, DateTime startDate, DateTime endDate)
        {
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    commPage.RequestURL = "https://arlo.netgear.com/hmsweb/users/library";
                    commPage.JsonStringToPost = string.Format("{{\"dateFrom\":\"{0}\",\"dateTo\":\"{1}\"}}", startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/plain, */*");
                    commPage.RequestHeaders.Add("Authorization", accessToken);

                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        HtmlLogs.Add("Arlo GetVideo AccountId:" + accountId + " " + commPage.Html);
                        if (ob["success"].ToBoolean() == true)
                        {
                            List<ArloVideo> videos = new List<ArloVideo>();
                            //JM 3/13/20 Get all data of videos from server
                            var data = JArray.Parse(ob["data"].ToString());
                            foreach (var video in data)
                            {
                                var dateCreated = video["localCreatedDate"].ToString();
                                var utcDateCreated = video["utcCreatedDate"].ToString();
                                dateCreated = dateCreated.Remove(dateCreated.Length - 3);
                                utcDateCreated = utcDateCreated.Remove(utcDateCreated.Length - 3);

                                System.DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);

                                videos.Add(new ArloVideo
                                {
                                    CreatedDate = video["createdDate"].ToString(),
                                    UtcCreatedDate = video["utcCreatedDate"].ToString(),
                                    UniqueId = video["uniqueId"].ToString(),
                                    VideoUrl = video["presignedContentUrl"].ToString(),
                                    ThumbnailUrl = video["presignedThumbnailUrl"].ToString(),
                                    DeviceName = video["name"].ToString(),
                                    DeviceId = video["deviceId"].ToString(),
                                    DateCreated = dt.AddSeconds(dateCreated.ToLong()).ToDateTime().ToLocalTime(),
                                    UtcDateCreated = dt.AddSeconds(utcDateCreated.ToLong()).ToLocalTime(),
                                });
                            }

                            string camera = "";
                            string date = "";
                            string time = "";
                            string saveFile = "";
                            int newVideos = 0;

                            foreach (var video in videos)
                            {
                                using (var wc = new System.Net.WebClient())
                                {
                                    camera = video.DeviceName;
                                    //date = video.UtcDateCreated.ToString("yyyy-MM-dd");
                                    //time = video.UtcDateCreated.ToString("hh.mm tt") + ".mp4";
                                    var fileName = Guid.NewGuid().ToString();
                                    //var path = System.Web.HttpContext.Current.Server.MapPath(AppUrl.VideoRoot);
                                    System.IO.Directory.CreateDirectory(AppUrl.VideoRoot);
                                    saveFile = AppUrl.VideoRoot + fileName + ".mp4";
                                    using (var db = new ApplicationDbContext())
                                    {//JM 3/13/20  check if video is existing on database
                                        var localDeviceId = Devices.AddDevice(new Device() { DeviceId = video.DeviceId, Name = video.DeviceName, AccountId = accountId });
                                        var temp = db.Videos.Where(x =>x.LocalDeviceId == localDeviceId && x.DateCreated == video.DateCreated).FirstOrDefault();
                                        if (temp == null)
                                        {
                                            wc.DownloadFile(video.VideoUrl, saveFile);
                                            //wc.DownloadFile(video.ThumbnailUrl, saveFile.Replace("mp4", "jpg"));
                                            var VideoUrl = string.Format("/Multimedia Videos/{0}", fileName + ".mp4");
                                            //JM 3/13/20 Save data to database and server
                                             Videos.Add(new Core.Database.Entity.Video()
                                            {
                                                SiteVideoId=video.UniqueId,
                                                ServerPath = saveFile,
                                                OriginalVideoUrl = video.VideoUrl,
                                                VideoUrl = VideoUrl,
                                                Details = video.DeviceName + "," + video.CreatedDate + "," + video.UtcCreatedDate,
                                                AccountId = accountId,
                                                LocalDeviceId = localDeviceId,
                                                DateCreated = video.DateCreated
                                            });
                                            newVideos++;
                                        }
                                        else
                                        {

                                        }
                                    }



                                }
                            }

                            return true;
                        }
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
                return true;
            });
            return true;
        }

        #endregion

        #region DELETE VIDEO
        public bool DeleteVideo(string accessToken,string createdDate, string deviceId,string utcCreatedDate)
        {
            try
            {
                commPage.RequestURL = "https://myapi.arlo.com/hmsweb/users/library/recycle";
                CommPage.JsonStringToPost = string.Format("{{\"data\":[{{\"createdDate\":\"{0}\",\"utcCreatedDate\":\"{2}\",\"deviceId\":\"{1}\"}}]}}", createdDate, deviceId, utcCreatedDate);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/plain, */*");
                commPage.RequestHeaders.Add("Authorization", accessToken);
                commPage.Referer = "https://my.arlo.com/";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob["success"].ToBoolean();
                }
                return true;
            }catch(Exception e)
            {
                HtmlLogs.Add("Arlo Delete DeviceId:" + deviceId + " " + e.InnerException + e.Message);
                HtmlLogs.Add("Arlo Delete DeviceId:" + deviceId + " " + commPage.Html);
                return false;
            }
        }
        #endregion

        #region DISPOSE

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion
    }
}
