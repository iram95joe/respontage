﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMedia.Arlo.Models
{
    public class ArloVideo
    {
        public string UniqueId { get; set; }

        public string VideoUrl { get; set; }

        public string ThumbnailUrl { get; set; }

        public string DeviceName { get; set; }
        public string DeviceId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime UtcDateCreated { get; set; }
        public string UtcCreatedDate { set; get; }
        public string CreatedDate { set; get; }
    }
}
