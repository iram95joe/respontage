﻿using BaseScrapper;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Helpers;
using Core.Helpers.Devices;
using Core.Models;
using Core.Models.MultiMedia;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Blink
{
    public class ScrapeManager : CommunicationEngine, IDisposable
    {
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://rest-prod.immedia-semi.com/");
        }

        public CommunicationPage commPage = null;

        #region HOMESCREEN

        public List<string> HomeScreen(string accessToken)
        {
            try
            {
                commPage.RequestURL = "https://rest-prod.immedia-semi.com/homescreen";
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("TOKEN_AUTH", accessToken);

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    //System.Net.WebClient client = new System.Net.WebClient();
                    //client.Headers = new System.Net.WebHeaderCollection();
                    //client.Headers.Add("TOKEN_AUTH", accessToken);

                    //string url = "https://prod.immedia-semi.com/media/production/account/86417/network/105025/camera/329177/clip_Fjrw_AkK_2018_01_21__23_36PM";

                    //client.DownloadFile(url, "C:\\Users\\Ivyrson\\Downloads\\Arlo\\Arlo Scraped Videos\\clip_Fjrw_AkK_2018_01_21__23_36PM.mp4");
                    //commPage.RequestURL = "https://rest-prod.immedia-semi.com/media/production/account/86417/network/105025/camera/329177/clip_Fjrw_AkK_2018_01_21__23_36PM";
                    //commPage.ReqType = CommunicationPage.RequestType.GET;
                    //commPage.RequestHeaders = new Dictionary<string, string>();
                    //commPage.RequestHeaders.Add("TOKEN_AUTH", accessToken);

                    //ProcessRequest(commPage);

                    //if (commPage.IsValid)
                    //{

                    //}

                    return new List<string>();
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }

            return new List<string>();
        }

        #endregion

        #region LOGIN

        public void Test()
        {
            commPage.RequestURL = "https://rest-prod.immedia-semi.com/";
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.RequestHeaders = new Dictionary<string, string>();
            ProcessRequest(commPage);
            HtmlLogs.Add("StatusCode:" + commPage.StatusCode + " Uri:" + commPage.Uri + " Isvalid:" + commPage.IsValid + " UserAgent:" + commPage.UserAgent.ToSafeString() + " " + commPage.Html);


        }
        public LogInResult LogIn(string email, string password,string url,int companyId,bool isImport= true,string userId =null)
        {
            LogInResult result = new LogInResult();
            result.Success = false;
            try
            {
                var account = Devices.GetAccount(email, (int)Core.Enumerations.DevicesSiteType.Blink);
                if (account != null)
                {
                    commPage.RequestURL = "https://rest-prod.immedia-semi.com/api/v5/account/login";
                    commPage.JsonStringToPost = account == null ? string.Format("{{\"email\":\"{0}\",\"password\":\"{1}\"}}", email, password) : string.Format("{{\"email\":\"{0}\",\"password\":\"{1}\",\"unique_id\":\"{2}\"}}", email, password, account.ClientId);
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    HtmlLogs.Add("Blink Login Email:" + email + " StatusCode:" + commPage.StatusCode + " Uri:" + commPage.Uri + " Isvalid:" + commPage.IsValid + " UserAgent:" + commPage.UserAgent.ToSafeString() + " " + commPage.Html);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        result.Success = true;
                        result.HasSecurity = ob["account"]["account_verification_required"].ToBoolean();
                        result.AccessToken = ob["auth"]["token"].ToString();
                        result.Region = ob["account"]["tier"].ToString();
                        result.AccountId = ob["account"]["account_id"].ToString();
                        result.ClientId = ob["account"]["client_id"].ToString();

                        if (result.HasSecurity)
                        {
                            var accountId = Devices.AddAccount(new Core.Database.Entity.DeviceAccount() { Region = result.Region, AccountId = result.AccountId, ClientId = result.ClientId, Token = result.AccessToken, Username = email, Password = password, SiteType = (int)Core.Enumerations.DevicesSiteType.Blink, CompanyId = companyId });
                            result.localId = accountId;
                            if (userId != null)
                            {
                                Core.SignalR.Hubs.GlobalAirlockHub.BlinkSecurity(userId.ToInt(), accountId);
                            }
                            return result;
                        }
                        if (isImport)
                        {
                            var accountId = Devices.AddAccount(new Core.Database.Entity.DeviceAccount() { Region = result.Region, AccountId = result.AccountId, ClientId = result.ClientId, Token = result.AccessToken, Username = email, Password = password, SiteType = (int)Core.Enumerations.DevicesSiteType.Blink, CompanyId = companyId });
                            Videos.Delete(accountId, DateTime.Now.AddDays(-2));
                            ScrapeVideos(result, accountId, url);
                        }
                        return result;
                    }
                }
                else
                {
                    result.Success = true;
                    result.AccessToken = account.Token;
                    result.Region = account.Region;
                    result.AccountId = account.AccountId;
                    result.ClientId = account.ClientId;
                    if (isImport)
                    {
                        var accountId = Devices.AddAccount(new Core.Database.Entity.DeviceAccount() { Region = result.Region, AccountId = result.AccountId, ClientId = result.ClientId, Token = result.AccessToken, Username = email, Password = password, SiteType = (int)Core.Enumerations.DevicesSiteType.Blink, CompanyId = companyId });
                        Videos.Delete(accountId, DateTime.Now.AddDays(-2));
                        ScrapeVideos(result, accountId, url);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteLog(string.Format("Error on Blink log in with username = '{0}', message = {1}, innerexception = {2}, stacktrace = {3}", email, e.Message, e.InnerException, e.StackTrace));
                result.Success = false;
                return result;
            }
            
            return result;
        }

        public bool Security(int id,string code)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.DeviceAccounts.Where(x => x.Id == id).FirstOrDefault();
                commPage.RequestURL = string.Format("https://rest-{0}.immedia-semi.com/api/v4/account/{1}/client/{2}/pin/verify", user.Region, user.AccountId, user.ClientId);
                commPage.JsonStringToPost = string.Format("{{\"pin\":\"{0}\"}}",code);
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("TOKEN_AUTH", user.Token);
                ProcessRequest(commPage);
                var ob = JObject.Parse(commPage.Html);
                return ob["valid"].ToBoolean();
            }
        }
        #endregion

        #region Delete
        public bool DeleteVideo(LogInResult result, string siteVideoId)
        {
            //can be provided a list of video Ids
            try
            {
                var vidUrl = $"https://rest-{result.Region}.immedia-semi.com/api/v1/accounts/{result.AccountId}/media/delete";
                commPage.RequestURL = vidUrl;
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                var jsonPost = "{\"media_list\":[" + siteVideoId + "]}";
                commPage.JsonStringToPost = jsonPost;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("TOKEN_AUTH", result.AccessToken);
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var msg = ob["message"].ToSafeString().ToLower();
                    if (msg.Contains("successfully deleted"))
                        return true;
                }
            }
            catch (Exception e)
            {
                HtmlLogs.Add("Blink Delete AccountId:"+result.AccountId +" "+ e.InnerException + e.Message);
                HtmlLogs.Add("Blink Delete AccountId:" + result.AccountId+" "+commPage.Html);
                return false;
            }

            return false;
        }
        #endregion
        #region NETWORKS

        public List<string> Networks(string accessToken)
        {
            try
            {
                commPage.RequestURL = "https://rest-prod.immedia-semi.com/networks";
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("TOKEN_AUTH", accessToken);

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var networks = new List<string>();
                    var data = ob["networks"];
                    foreach(var network in data)
                    {
                        networks.Add(network["id"].ToString());
                    }
                    return networks;
                }
            }
            catch (Exception e)
            {
                return new List<string>();
            }

            return new List<string>();
        }


        #endregion

        #region SCRAPEVIDEOS

        public async Task<bool> ScrapeVideos(LogInResult result,int accountId,string Url)
        {
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    bool hasMore = true;
                    int pageNumber = 0;
                    do
                    {
                        var vidUrl = $"https://rest-{result.Region}.immedia-semi.com/api/v1/accounts/{result.AccountId}/media/changed?since=2020-01-01T23:11:21+0000&page={pageNumber}";
                        commPage.RequestURL = vidUrl;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("TOKEN_AUTH", result.AccessToken);
                        ProcessRequest(commPage);
                        HtmlLogs.Add("Blink GetVideo AccountId:" + accountId+ " " + commPage.Html);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var videos = JArray.Parse(ob["media"].ToString());
                            string url = "";
                            string videoUrl = "";
                            string deviceName = "";
                            DateTime dateCreated = new DateTime();
                            string saveFile = "";
                            int newVideos = 0;
                            string thumbnailUrl = "";
                            string deviceId = "";
                            string videoId = "";
                            if (videos.Count > 0)
                            {
                                var leastDate = DateTime.Now.AddDays(-2);
                                foreach (var video in videos)
                                {
                                    try
                                    {
                                        using (WebClient client = new WebClient())
                                        {
                                            client.Headers.Add("TOKEN_AUTH", result.AccessToken);
                                            videoUrl = video["media"].ToString();
                                            //thumbnailUrl = $"https://rest-{result.Region}.immedia-semi.com" + video["thumbnail"].ToString() + ".jpg";
                                            deviceId = video["device_id"].ToString();
                                            deviceName = video["device_name"].ToString().Replace("|", ",");
                                            dateCreated = video["created_at"].ToDateTime();
                                            var fileName = Guid.NewGuid().ToString();
                                            url = $"https://rest-{result.Region}.immedia-semi.com" + videoUrl;
                                            videoId = video["id"].ToString();
                                            var isdeleted = video["deleted"].ToBoolean();
                                            //var path = System.Web.HttpContext.Current.Server.MapPath(AppUrl.VideoRoot);
                                            System.IO.Directory.CreateDirectory(AppUrl.VideoRoot);
                                            saveFile = AppUrl.VideoRoot + fileName + ".mp4";
                                            using (var db = new ApplicationDbContext())
                                            {
                                                var temp = db.Videos.Where(x => x.OriginalVideoUrl == url).FirstOrDefault();
                                                if (temp == null && isdeleted == false && dateCreated.Date > leastDate.Date)
                                                {
                                                    client.DownloadFile(url, saveFile);
                                                    //client.DownloadFile(thumbnailUrl, saveFile.Replace("mp4", "jpg"));
                                                    var VideoUrl =string.Format("/Multimedia Videos/{0}", fileName + ".mp4");
                                                    newVideos++;
                                                    var localDeviceId = Devices.AddDevice(new Device() { DeviceId = deviceId, Name = deviceName, AccountId = accountId });
                                                    Videos.Add(new Core.Database.Entity.Video()
                                                    {
                                                        ServerPath = saveFile,
                                                        OriginalVideoUrl = url,
                                                        VideoUrl = VideoUrl,
                                                        Details = "",
                                                        AccountId = accountId,
                                                        DateCreated = dateCreated,
                                                        LocalDeviceId = localDeviceId,
                                                        SiteVideoId = videoId
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                                pageNumber++;
                            }
                            else
                            {
                                hasMore = false;
                            }
                        }
                    } while (hasMore);
                    //Logger.WriteLog(string.Format("Scraped {0} Blink videos ({1} new videos) on {2}", videos.Count, newVideos, listingId));

                }
                catch (Exception e)
                {
                    return false;
                }
                return true;
            });
            return true;
        }

        #endregion

        #region DISPOSE

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion
    }
}
