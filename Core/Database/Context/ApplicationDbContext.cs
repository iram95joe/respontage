﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Core.Database.Entity;

namespace Core.Database.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("Prod") {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CommunicationChargeRate>().Property(x => x.Amount).HasPrecision(18, 6);
            modelBuilder.Entity<CommunicationCharge>().Property(x => x.Amount).HasPrecision(18, 6);
            modelBuilder.Entity<CommunicationLoad>().Property(x => x.Amount).HasPrecision(18, 6);
            //  modelBuilder.Entity<WorkerRetainer>()
            // .HasRequired(c => c.Worker)
            // .WithMany(d => d.WorkerRetainers)
            // .HasForeignKey(c => c.WorkerId);

            //  modelBuilder.Entity<WorkerContactInfo>()
            // .HasRequired(c => c.Worker)
            // .WithMany(d => d.WorkerContactInfos)
            // .HasForeignKey(c => c.WorkerId);

            //  modelBuilder.Entity<WorkerJob>()
            //.HasRequired(c => c.Worker)
            //.WithMany(d => d.WorkerJobs)
            //.HasForeignKey(c => c.WorkerId);
        }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<AdvertisementAccount> AdvertisementAccounts { get; set; }
        public DbSet<CampaignAdvertisementChannel> CampaignAdvertisementChannels { get; set; }
        public DbSet<CampaignAdvertisement> CampaignAdvertisements { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<AdvertisementTemplate> AdvertisementTemplates { get; set; }
        public DbSet<InvoiceTemplate> InvoiceTemplates { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceSchedule> InvoiceSchedules { get; set; }
        public DbSet<BlockedPhoneNumber> BlockedPhoneNumbers { get; set; }
        public DbSet<GuestContactInfo> GuestContactInfos { get; set; }
        public DbSet<TwilioNumber> TwilioNumbers { get; set; }
        public DbSet<SMS> SMS { get; set; }
        public DbSet<Call> Calls { get; set; }
        public DbSet<BookingStatus> BookingStatus { get; set; }
        public DbSet<SiteType> SiteType { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Inbox> Inbox { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<TemplateMessage> TemplateMessages { get; set; }
        public DbSet<Note> tbNote { get; set; }
        public DbSet<Inquiry> Inquiries { get; set; }
        public DbSet<Role> tbRole { get; set; }
        public DbSet<tbRolePage> tbRolePage { get; set; }
        public DbSet<tbRoleLink> tbRoleLink { get; set; }
        public DbSet<Host> Hosts { get; set; }
        public DbSet<HostSessionCookie> HostSessionCookies { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<PropertyBookingDate> PropertyBookingDate { get; set; }
        public DbSet<PropertySyncSettings> PropertySyncSettings { get; set; }
        public DbSet<PropertyType> PropertyType { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<PropertyFee> PropertyFees { get; set; }
        public DbSet<FeeType> FeeTypes { get; set; }
        public DbSet<Income> Income { get; set; }
        public DbSet<IncomePayment> IncomePayment { get; set; }
        public DbSet<IncomeType> IncomeTypes { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<ExpenseCategory> ExpenseCategories { get; set; }
        public DbSet<ExpensePayment> ExpensePayment { get; set; }
        public DbSet<CreditCard> CreditCard { get; set; }
        public DbSet<CashFlowType> CashFlowTypes { get; set; }
        public DbSet<CashFlows> CashFlows { get; set; }
        public DbSet<Worker> Workers { get; set; }
        public DbSet<WorkerJob> WorkerJobs { get; set; }
        public DbSet<WorkerJobType> WorkerJobTypes { get; set; }
        public DbSet<WorkerAssignment> WorkerAssignments { get; set; }
        public DbSet<WorkerException> WorkerExceptions { get; set; }
        public DbSet<WorkerSchedule> WorkerSchedules { get; set; }
        public DbSet<WorkerType> WorkerTypes { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<DefaultRate> DefaultRates { get; set; }
        public DbSet<MessageRule> MessageRules { get; set; }
        public DbSet<PaymentHistory> PaymentHistory { get; set; }
        public DbSet<PaymentAccount> PaymentAccount { get; set; }
        public DbSet<TaskTemplate> TaskTemplates { get; set; }
        public DbSet<TravelType> TravelTypes { get; set; }
        public DbSet<HashDictionary> HashDictionaries { get; set; }
        public DbSet<InquiryTask> InquiryTasks { get; set; }
        public DbSet<Core.Database.Entity.Task> Tasks { get; set; }
        public DbSet<PropertyTemplate> PropertyTemplates { get; set; }
        public DbSet<WorkerClassification> WorkerClassifications { get; set; }

        public DbSet<ReimbursementNote> ReimbursementNotes { get; set; }
        public DbSet<WorkerAvailability> WorkerAvailability { get; set; }
        public DbSet<GuestEmail> GuestEmails { get; set; }
        public DbSet<SubTask> SubTasks { get; set; }
        public DbSet<InquirySubTask> InquirySubTask { get; set; }
        public DbSet<CommunicationMessage> CommunicationMessages { get; set; }
        public DbSet<CommunicationThread> CommunicationThreads { get; set; }
        public DbSet<WorkerContactInfo> WorkerContactInfos { get; set; }
        public DbSet<WorkerEmail> WorkerEmails { get; set; }
        public DbSet<CommunicationSMS> CommunicationSMS { get; set; }
        public DbSet<CommunicationCall> CommunicationCalls { get; set; }
        public DbSet<WorkerRetainer> WorkerRetainers { get; set; }
        public DbSet<AlterHistory> AlterHistory { get; set; }
        public DbSet<PriceRuleSettings> PriceRuleSettings { get; set; }
        public DbSet<DeviceAccount> DeviceAccounts { get; set; }
        public DbSet<EmailData> EmailData { get; set; }
        public DbSet<CctvFootageNote> CctvFootageNotes { get; set; }
        public DbSet<WorkerMessageRule> WorkerMessageRules { get; set; }
        public DbSet<WorkerTemplateMessage> WorkerTemplateMessages { get; set; }
        public DbSet<LockInbox> LockInboxes { get; set; }
        public DbSet<ExcludedComparable> ExcludedComparables { get; set; }

        public DbSet<PropertyDefaultWorker> PropertyDefaultWorkers { set; get; }

        public DbSet<Outbox> Outboxes { get; set; }

        public DbSet<HostCompany> HostCompanies { get; set; }

        //Rentals
        public DbSet<PropertyCategory> PropertyCategory { get; set; }
        public DbSet<PropertyImage> PropertyImage { get; set; }
        public DbSet<PropertyAmenity> PropertyAmenities { get; set; }
        public DbSet<Amenity> Amenities { get; set; }

        public DbSet<PropertyBaseRate> PropertyBaseRates { get; set; }
        public DbSet<LastminuteDiscount> LastminuteDiscount { get; set; }
        public DbSet<StandardFee> StandardFee { get; set; }
        public DbSet<EarlyBirdDiscount> EarlyBirdDiscounts { get; set; }

        public DbSet<JobHistory> JobHistory { get; set; }
        public DbSet<ResidenceHistory> ResidenceHistory { get; set; }
        public DbSet<ApplicationDetail> ApplicationDetail { get; set; }
        public DbSet<ContractChangesLog> ContractChangesLogs { get; set; }
        public DbSet<Renter> Renters { get; set; }
        public DbSet<RenterContact> RenterContacts { get; set; }
        public DbSet<RenterEmail> RenterEmails { get; set; }

        //Maintenace
        public DbSet<PropertyScheduledExpense> PropertyScheduledExpenses { get; set; }
        public DbSet<MaintenanceEntry> MaintenanceEntries { get; set; }
        public DbSet<MaintenanceEntryNote> MaintenanceEntryNotes { get; set; }
        public DbSet<MaintenanceEntryFile> MaintenanceEntryFiles { get; set; }


        //Income Payment
        public DbSet<IncomePaymentFile> IncomePaymentFiles { get; set; }
        public DbSet<IncomeBatchPayment> IncomeBatchPayments { get; set; }
        public DbSet<BookingAdditionalFee> BookingAdditionalFees { get; set; }

        //Rent and Post Dated
        public DbSet<PostDated> PostDated { get; set; }
        public DbSet<MonthlyRent> MonthlyRents { get; set; }

        //Expense Payment
        public DbSet<ExpenseBatchPayment> ExpenseBatchPayments { get; set; }
        public DbSet<ExpensePaymentFile> ExpensePaymentFiles { get; set; }

        //Vendors
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorContactNumber> VendorContactNumbers { get; set; }
        public DbSet<VendorEmail> VendorEmails {get;set;}

        //Parent properties
        public DbSet<ParentChildProperty> ParentChildProperties { get; set; }
        public DbSet<SearchCriteria> SearchCriterias { get; set; }
        public DbSet<Review> Reviews { get; set; }
        //Videos
        public DbSet<Video> Videos { get; set; }
        public DbSet<VideoNote> VideoNotes { get; set; }
        //Vera
        public DbSet<Device> Devices { get; set; }
        public DbSet<Lock> Locks { get; set; }
        public DbSet<WorkerReportSearchCriteria> WorkerReportSearchCriterias { get; set; }
        public DbSet<MMSImage> MMSImages { get; set; }
        public DbSet<LockPincode> LockPincodes { get; set; }
        //Notices
        public DbSet<Notice> Notices { get; set; }
        public DbSet<RenterNotice> RenterNotices { get; set; }
        public DbSet<PropertyNotice> PropertyNotices { get; set; }
        public DbSet<NoticeType> NoticeTypes { get; set; }
        public DbSet<NoticeDocument> NoticeDocuments { get; set; }
        public DbSet<NoticeDeliveryMethod> NoticeDeliveryMethods { get; set; }
        public DbSet<NoticeDocumentTemplate> NoticeDocumentTemplates { get; set; }
        public DbSet<OwnerProperty> OwnerProperties { get; set; }
        public DbSet<OwnerCompany> OwnerCompanies { get; set; }

        //Reservation Contract
        public DbSet<ContractTemplate> ContractTemplates { get; set; }
        public DbSet<ContractDocument> ContractDocuments { get; set; }
        public DbSet<IncomeFile> IncomeFiles { get; set; }
        public DbSet<ExpenseFile> ExpenseFiles { get; set; }
        public DbSet<RoleApplicationPage> RoleApplicationPages { get; set; }
        public DbSet<ApplicationPage> ApplicationPages { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyType> CompanyTypes { get; set; }
        public DbSet<CompanyTypeCompany> CompanyTypeCompanies { get; set; }
        public DbSet<CompanyTypePage> CompanyTypePages { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<EmailAccount> EmailAccounts { get; set; }
        public DbSet<CommunicationEmail> CommunicationEmails { get; set; }
        public DbSet<CommunicationEmailAttachment> EmailAttachments { get; set; }
        //Notification
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationRenter> NotificationRenters { get; set; }
        public DbSet<NotificationProperty> NotificationProperties { get; set; }
        public DbSet<NotificationEntry> NotificationEntries { get; set; }
        public DbSet<HtmlLog> HtmlLogs { get; set; }

        public DbSet<RenterDocument> RenterDocuments { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<RenterRequestAttachmentDescription> RenterRequestAttachmentDescriptions { get; set; }
        public DbSet<RenterRequestFile> RenterRequestFiles { get; set; }
        public DbSet<ContractRequest> ContractRequests { get; set; }
        public DbSet<RenterRequest> RenterRequests { get; set; }

        public DbSet<SubscriptionPlan> SubscriptionPlans { get; set; }
        public DbSet<UserSubscription> UserSubscriptions { get; set; }
        public DbSet<CommunicationLoad> CommunicationLoads { get; set; }
        public DbSet<CommunicationCharge> CommunicationCharges { get; set; }
        public DbSet<CommunicationChargeRate> CommunicationChargeRates { get; set; }
    }
}
