﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CompanyTypes")]
    public class CompanyType
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
