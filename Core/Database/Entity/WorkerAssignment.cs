﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
namespace Core.Database.Entity
{
    [Table("WorkerAssignment")]
    public class WorkerAssignment
    {
        [Key]
        public int Id { get; set; }

        public int WorkerId { get; set; }

        public int JobTypeId { get; set; }

        public int PropertyId { get; set; }

        public int ReservationId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int CompanyId { get; set; }
        //JM 03/05/19 Status are 0=pending,1=accepted,2=decline
        public int Status { get; set; }
        //JM 03/05/19 InDayStatus are 0=on the way,1=start,2=finish
        public int InDayStatus { get; set; }
        public string Description { get; set; }
        public int? DefaultWorkerId { get; set; }
        public int MaintenanceEntryId { get; set; }
    }
}
