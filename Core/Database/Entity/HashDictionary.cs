﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("HashDictionary")]
    public class HashDictionary
    {
        [Key]
        public int Id { get; set; }

        public string HashValue { get; set; }

        public DateTime ExpiryDateTime { get; set; }
    }
}
