﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("PropertyType")]
    public class PropertyType
    {
        [Key]
        public int Id { get; set; }
        public string Property_Type { get; set; }
    }
}