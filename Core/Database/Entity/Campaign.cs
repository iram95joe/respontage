﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("Campaigns")]
    public class Campaign
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        [NotMapped]
        public CampaignAdvertisement Advertisement { get; set; }
      
    }
}
