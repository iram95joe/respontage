﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationEmails")]
    public class CommunicationEmail
    {
        public int Id { get; set; }
        public string ThreadId { get; set; }
        public string Message { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsMyMessage { get; set; }
    }
}
