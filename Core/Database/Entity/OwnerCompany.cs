﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("OwnerCompanies")]
    public class OwnerCompany
    {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public int CompanyId { get; set; }
    }
}
