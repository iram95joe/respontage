﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("TravelType")]
    public class TravelType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool ForArrival { get; set; }

        public bool ForDeparture { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedTime { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime? UpdatedTime { get; set; }

        public int TravelNoteId { get; set; }
    }
}
