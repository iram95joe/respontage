﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Inquiry")]
    public class Inquiry
    {
        [Key]
        public int Id { get; set; }
        public int HostId { get; set; }
        public string ReservationId { get; set; }
        public int PropertyId { get; set; }
        public int GuestId { get; set; }
        public string ConfirmationCode { get; set; }
        public string BookingStatusCode { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public int Nights { get; set; }
        public decimal NightsFee { get; set; }
        public string NightsFeeSummary { get; set; }
        public decimal ReservationCost { get; set; }
        public decimal SumPerNightAmount { get; set; }
        public decimal CleaningFee { get; set; }
        public decimal ServiceFee { get; set; }
        public string SummaryFee { get; set; }
        public decimal TotalPayout { get; set; }
        public DateTime? PayoutDate { get; set; }
        public decimal NetRevenueAmount { get; set; }
        public DateTime? InquiryDate { get; set; }
        public DateTime? BookingDate { get; set; }
        public DateTime? BookingCancelDate { get; set; }
        public DateTime? BookingConfirmDate { get; set; }
        public DateTime? BookingConfirmCancelDate { get; set; }
        public int SiteType { get; set; }
        public int GuestCount { get; set; }
        public int Adult { get; set; }
        public int Children { get; set; }
        public bool IsActive { get; set; }
        public bool IsImported { get; set; }
        public bool IsAltered { get; set; }
        public string ThreadId { get; set; }
        public string Type { get; set; }
        public string Fees { get; internal set; }
        public string RateDetails { get; set; }
        public bool ?Paid { get; set; }
        public int? Source { get; set; }
        public bool IsApplyAutoAssignWorker { get; set; }

        //JM 08/23/19 For Longterm Rentals
        public int RepeatMonth { get; set; }
        public bool IsLateFees { get; set; }
        public int? LateFeeApplyDays { get; set; }
        public decimal? LateFeeAmount { get; set; }
        public bool IsMonthToMonth { get; set; }
        public DateTime? RecurringRentStartDate { get; set; }
        public bool IsFixedMonthToMonth { get; set; }

        public bool IsPostDatedPayment { get; set; }
        public bool IsAdvancePayment { get; set; }
        public int? AdvancePaymentMonth { get; set; }
        public decimal? AdvancePaymentAmount { get; set; }

        public bool IsSecurityDeposit { get; set; }
        public decimal? SecurityDepositAmount { get; set; }
        public DateTime? SecurityDepositDueOn { get; set; }
        public bool IsLastMonthPayment { get; set; }
        public int? LastPaymentMonth { get; set; }
        //Contract
        public string SignRequestUiid { get; set; }
        public string SignedDocumentDownloadLink { get; set; }
        public bool IsDocumentSigned { get; set; }
        public string DateTimeSigned { get; set; }
    }
}