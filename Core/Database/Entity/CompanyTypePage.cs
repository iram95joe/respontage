﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CompanyTypePages")]
    public class CompanyTypePage
    {
        public int Id { get; set; }
        public int CompanyTypeId { get; set; }
        public int ApplicationPageId { get; set; }
    }
}
