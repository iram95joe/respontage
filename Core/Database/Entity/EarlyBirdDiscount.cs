﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("EarlyBirdDiscount")]
    public class EarlyBirdDiscount
    {
        [Key]
        public int EarlyBirdDiscountId { get; set; }
        public int PropertyId { get; set; }
        public int MonthNumber { get; set; }
        public decimal? Discount { get; set; }
    }
}