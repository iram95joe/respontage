﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("EmailAccounts")]
    public class EmailAccount
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int CompanyId { get; set; }
        [NotMapped]
        public string Actions => "<button class=\"ui mini button sync-email\" data-tooltip=\"Sync\">" +
                                            "<i class=\"icon cloud download\"></i></button>" +
                                            "<button class=\"ui mini button edit-email\" data-tooltip=\"Edit\">" +
                                            "<i class=\"icon edit outline\"></i></button>" +
                                            "<button class=\"ui mini button delete-email\" data-tooltip=\"Delete\">" +
                                            "<i class=\"icon trash\"></i></button>";
    }
}
