﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CompanyTypeCompany")]
    public class CompanyTypeCompany
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int CompanyTypeId { get; set; }
    }
}
