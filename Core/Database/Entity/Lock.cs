﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.Database.Entity
{
    [Table("Locks")]
    public class Lock
    {
        public int Id { get; set; }
        public string LockId { get; set; }
        public string Name { get; set; }
        public int LocalDeviceId { get; set; }
        public bool IsLock { get; set; }
    }
}
