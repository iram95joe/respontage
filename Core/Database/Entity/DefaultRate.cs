﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("DefaultRate")]
    public class DefaultRate
    {
        [Key]
        public int Id { get; set; }

        public double StandardRate { get; set; }

        public int CompanyId { get; set; }
    }
}