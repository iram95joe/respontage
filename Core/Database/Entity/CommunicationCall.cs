﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationCalls")]
    public class CommunicationCall
    {
        [Key]
        public int Id { get; set; }
        public string ThreadId { get; set; }
        public Int32 Length { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string FromCountry { get; set; }
        public string RecordingSid { get; set; }
        public DateTime? RecordingStartTime { get; set; }
        public int? RecordingDuration { get; set; }
        public int? CallDuration { get; set; }
        public string RecordingUrl { get; set; }
        public string CallSid { get; set; }
        public string ResourceSid { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set;}
    }
}
