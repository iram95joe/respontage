﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Note")]
    public class Note
    {
        [Key]
        public long Id { get; set; }

        public int ReservationId { get; set; }

        public int NoteType { get; set; }

        public string InquiryNote { get; set; }

        public string FlightNumber { get; set; }

        public DateTime? ArrivalDateTime { get; set; }

        public DateTime? DepartureDateTime { get; set; }

        public string DepartureCityCode { get; set; }

        public DateTime? LeavingDate { get; set; }

        public string ArrivalAirportCode { get; set; }

        public string MeetingPlace { get; set; }

        public DateTime? MeetingDateTime { get; set; }

        public string People { get; set; }

        public string Airline { get; set; }

        public string NumberOfPeople { get; set; }

        public string CarType { get; set; }

        public string CarColor { get; set; }

        public string PlateNumber { get; set; }

        public string SourceCity { get; set; }

        public string CruiseName { get; set; }

        public string AirportCodeDeparture { get; set; }

        public string AirportCodeArrival { get; set; }

        public string IdentifiableFeature { get; set; }

        public string DepartureCity { get; set; }

        public string DeparturePort { get; set; }

        public string ArrivalPort { get; set; }

        public string TerminalStation { get; set; }

        public string Name { get; set; }

        public string ContactPerson { get; set; }

        public string ContactNumber { get; set; }

        public string HashValue { get; set; }

        public DateTime? Checkin { get; set; }

        public DateTime? Checkout { get; set; }
    }
}
