﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("IncomePaymentFiles")]
    public class IncomePaymentFile
    {
        public int Id { get; set; }
        public int? PaymentId { get; set; }
        public int ?BatchPaymentId { get; set; }
        public string FileUrl { get; set; }
    }
}
