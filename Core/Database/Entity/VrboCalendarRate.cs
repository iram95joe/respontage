﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel;

namespace Core.Database.Entity
{
    [Table("VrboCalendarRate")]
    public class VrboCalendarRate
    {
        [Key]
        public long Id { get; set; }

        public long ListingId { get; set; }

        public string ListingTriad { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public float Price { get; set; }

        public int Minstay { get; set; }

        public bool IsExpired { get; set; }

        public string Type { get; set; }

        public DateTime Created { get; set; }
    }
}
