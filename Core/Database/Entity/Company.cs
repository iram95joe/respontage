using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Companies")]
    public partial class Company
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public string Contact { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string About { get; set; }
        public string Address { get; set; }
        public string BackgroundColor { get; set; }
        public string TrimColor { get; set; }
        public string BackgroundImage { get; set; }
        public bool IsChargeCommunication { get; set; }
    }
}
