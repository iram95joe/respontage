using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Cashflow")]
    public class CashFlows
    {
        [Key]
        public int Id { get; set; }
        public int CashFlowTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public double Amount { get; set; }
        public int CompanyId { get; set; }
        public int PropertyId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }
    }
}
