﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("MessageRule")]
    public class MessageRule
    {
        [Key]
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int TemplateMessageId { get; set; }
        public int Type { get; set; }
        public int DateDifference { get; set; }
        public bool IsSendToSMS { get; set; }
        public bool IsSendToSite { get; set; }
        public bool IsSendToEmail { get; set; }
        public bool IsActive { get; set; }
    }
}
