﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("WorkerRetainer")]
    public class WorkerRetainer
    {
        [Key]
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public decimal Rate { get; set; }
        public int Frequency { get; set; }
        [DisplayFormat(DataFormatString = "{MMM yyyy}")]
        public DateTime EffectiveDate { get; set; }
        //public Worker Worker { get; set; }
    }
}
