﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Database.Entity
{
    [Table("WorkerJob")]
    public class WorkerJob
    {
        [Key]
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public int JobTypeId { get; set; }
        public int PaymentType { get; set; }
        public decimal PaymentRate { get; set; }
        //public Worker Worker { get; set; }

    }
}