﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel;

namespace Core.Database.Entity
{
    [Table("PropertyTemplate")]
    public class PropertyTemplate
    {
        [Key]
        public int Id { get; set; }

        public int PropertyId { get; set; }

        public int TemplateId { get; set; }
    }
}
