﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationSMS")]
    public class CommunicationSMS
    {
        public int Id { get; set; }
        public string ThreadId { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string MessageSid { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
