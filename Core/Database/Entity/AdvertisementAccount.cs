﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("AdvertisementAccounts")]
    public class AdvertisementAccount
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int AdvertisementSite { get; set; }
        public string AccessToken { get; set; }
        public int CompanyId { get; set; }
        public string AccountSiteId { get; set; }
        public string Name => Firstname + " " + Lastname;
        public string Site => ((Core.Enumerations.AdvertismentSite)AdvertisementSite).ToString();
        public string Actions => "";
    }
}
