﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("IncomeType")]
    public class IncomeType
    {
        [Key]
        public int Id { get; set; }
        public string TypeName { get; set; }
        public bool IsByCompany { get; set; } = true;
        public int CompanyId { get; set; }
        public int CompanyTerm { get; set; } = (int)Enumerations.CompanyType.Shared;
    }
}
