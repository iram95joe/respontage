using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Inclusive_Exclusive")]
    public class Inclusive_Exclusive
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public long PropertyId { get; set; }

        //public virtual Properties Properties { get; set; }
    }
}