﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ContractDocuments")]
    public class ContractDocument
    {
        public int Id { get; set; }
        public string AttachmentPath { get; set; }
        public int BookingId { get; set; }
        public int Type { get; set; }
        [NotMapped]
        public string Html { get; set; }
    }
}
