﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("LockPincodes")]
    public class LockPincode
    {
        public int Id { get; set; }
        public int LockId { get; set; }
        public int PinId { get; set; }
        public string PinCode { get; set; }
        public string PinName { get; set; }
        public int? ReservationId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
