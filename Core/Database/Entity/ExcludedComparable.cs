﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("ExcludedComparable")]
    public class ExcludedComparable
    {
        [Key]
        public long Id { get; set; }

        public int HostId { get; set; }

        public string ListingId { get; set; }
    }
}
