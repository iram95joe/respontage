﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("PaymentAccount")]
    public partial class PaymentAccount
    {
        public int Id { get; set; }
        public string PaypalClientId { get; set; }
        public string PaypalClientSecret { get; set; }
        public string SquareAccessToken { get; set; }
        public string SquareApplicationId { get; set; }
        public string SquareLocationId { get; set; }
        public string StripePublishableKey { get; set; }
        public string StripeSecretKey { get; set; }
        //This is online method paypal,stripe,square
        public int PaymentMethod { get; set; }
        public int CompanyId { get; set; }
    }
}
