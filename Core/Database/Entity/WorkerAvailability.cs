﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("WorkerAvailability")]
    public class WorkerAvailability
    {
        public int Id { get; set; }
        public int WorkerId {get;set;}
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Type { get; set; }

    }
}
