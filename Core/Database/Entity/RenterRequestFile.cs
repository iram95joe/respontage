﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("RenterRequestFiles")]
    public class RenterRequestFile
    {
        public int Id { get; set; }
        public int RenterRequestId { get; set; }
        public string FileUrls { get; set; }
        public string Description { get; set; }
        public int RenterRequestAttachmentDescriptionId { get; set; }
        [NotMapped]
        public List<string> Files =>string.IsNullOrWhiteSpace(FileUrls) ?new List<string>(): FileUrls.Split(',').ToList();
        [NotMapped] 
        public string AttachmentDescription { get; set; }
    }
}
