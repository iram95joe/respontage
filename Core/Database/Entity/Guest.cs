﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Guest")]
    public class Guest
    {
        [Key]
        public int Id { get; set; }
        public string GuestId { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string GuestType { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public string Rating { get; set; }
        public string JoinDate { get; set; }
    }
}