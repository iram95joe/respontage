﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Message")]
    public class Message
    {
        [Key]
        public long Id { get; set; }
        public string MessageId { get; set; }
        public string ThreadId { get; set; }
        public string MessageContent { get; set; }
        public bool IsMyMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UserId { get; set; }
    }
}
