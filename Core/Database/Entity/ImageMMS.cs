﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("MMSImages")]
    public class MMSImage
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public int ?CommunicationSMSId { get; set; }
        public int ?GuestSMSId { get; set; }
    }
}
