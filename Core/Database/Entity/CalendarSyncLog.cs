﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CalendarSyncLog")]
    public class CalendarSyncLog
    {
        [Key]
        public long Id { get; set; }

        public long CurrentSourceListingId { get; set; }

        public int CurrentSourceParentPropertyId { get; set; }

        public bool IsDoneSyncing { get; set; }

        public DateTime DateCreated { get; set; }

        //public int CompanyId { get; set; }
    }
}
