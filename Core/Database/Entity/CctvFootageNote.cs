﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("CctvFootageNote")]
    public class CctvFootageNote
    {
        [Key]
        public long Id { get; set; }

        public string Notes { get; set; }

        public int InquiryLocalId { get; set; }

        public DateTime ReservationDate { get; set; }

        public string Camera { get; set; }
    }
}
