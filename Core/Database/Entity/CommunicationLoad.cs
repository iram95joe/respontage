﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationLoads")]
    public class CommunicationLoad
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime DateRecorded { get; set; }
        public int CompanyId { get; set; }
        public string SitePaymentId { get; set; }
    }
}
