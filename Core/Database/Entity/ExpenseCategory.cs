﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Database.Entity
{
    [Table("ExpenseCategory")]
    public class ExpenseCategory
    {
        [Key]
        public int Id { get; set; }
        public string ExpenseCategoryType { get; set; }
        //JM 1/28/20 Type 1 is private and need companyId  and type 2 is public
        public int Type { get; set; }
        public int CompanyId { get; set; }
        public int CompanyTerm { get; set; } = (int)Enumerations.CompanyType.Shared;
    }
}