﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("EmailData")]
    public class EmailData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Html { get; set; }
        public string From { get; set; }
        public bool? IsReservation { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
