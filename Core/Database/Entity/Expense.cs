
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Core.Models;

namespace Core.Database.Entity
{
    [Table("Expense")]
    public class Expense
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public int UnitId { get; set; }
        public int PropertyId { get; set; }
        public int BookingId { get; set; }
        public int PropertyOwnerId { get; set; }
        public int PropertyManagerId { get; set; }
        public int RenterId { get; set; }
        public int WorkerAssignmentId { get; set; }
        public int BillId { get; set; }
        public int PaymentMethodId { get; set; }
        public int Frequency { get; set; }
        public int DueDateDMn { get; set; }
        public int DueDateMQt { get; set; }
        public int DueDateDQt { get; set; }
        public int DueDateMYr { get; set; }
        public int DueDateDYr { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateLastJobRun { get; set; }
        public DateTime? DateNextJobRun { get; set; }
        public decimal? Amount { get; set; }
        public bool IsAutoCreate { get; set; }
        public bool IsOwnerExpense { get; set; }
        public bool IsRenterExpense { get; set; }
        public bool IsPreDeducted { get; set; }
        public bool IsPaid { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int FeeTypeId { get; set; }
        public int ExpenseCategoryId { get; set; }
        public string Receipts { get; set; }
        public int LeaseId { get; set; }
        public bool IsStartupCost { get; set; }
        public bool IsCapitalExpense { get; set; }
        public bool IsRefund { get; set; }
        public int CompanyId { get; set; }
        //Maintenance
        public int? PropertyScheduledExpenseId { get; set; }
        public int? MaintenanceEntryId { get; set; }
        public int? ReimbursementId { get; set; }
        public bool IsActive { get; set; } = true;
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}
