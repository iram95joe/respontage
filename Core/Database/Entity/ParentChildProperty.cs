﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ParentChildProperties")]
    public class ParentChildProperty
    {
        public int Id { get; set; }
        public int ParentPropertyId { get; set; }
        public int ChildPropertyId { get; set; }
        public int ParentType { get; set; }
        public int CompanyId { get; set; }
    }
}
