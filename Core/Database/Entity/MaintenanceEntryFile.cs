﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("MaintenanceEntryFiles")]
    public class MaintenanceEntryFile
    {
        public int Id { get; set; }
        public int MaintenanceEntryId { get; set; }
        public string FileUrl { get; set; }
    }
}
