﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("StandardFee")]
    public class StandardFee
    {
        [Key]
        public int StandardFeeId { get; set; }
        public int PropertyId { get; set; }
        public bool IsStandard { get; set; }
        public string StandardFeeName { get; set; }
        public bool IsTaxable { get; set; }
        public string FeeType { get; set; }
        public int Amount { get; set; }
    }
}