﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("UserSubscriptions")]
    public class UserSubscription
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        //Square Details
        public string CustomerId { get; set; }
        public string CardId { get; set; }
        public string SubscriptiondId { get; set; }
        public string PlanId { get; set; }
        public DateTime StartDate { get; set; }
    }
}
