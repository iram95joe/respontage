﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("PropertyAmenities")]
    public class PropertyAmenity
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int AmenityId { get; set; }
    }
}
