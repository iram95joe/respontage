﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel;

namespace Core.Database.Entity
{
    [Table("Property")]
    public class Property
    {
        [Key]
        public int Id { get; set; }
        public string IcalUrl { get; set; }
        public int HostId { get; set; }
        public long ListingId { get; set; }
        public string ListingUrl { get; set; }
        public string Name { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Address { get; set; }
        public int UnitCount { get; set; }
        public int MinStay { get; set; }
        public string Internet { get; set; }
        public string Pets { get; set; }
        public string WheelChair { get; set; }
        public string Description { get; set; }
        public int Reviews { get; set; }
        public int Sleeps { get; set; }
        public int Bedrooms { get; set; }
        public int Bathrooms { get; set; }
        public string PropertyType { get; set; }
        public int Type { get; set; }
        public string AccomodationType { get; set; }
        public string Smoking { get; set; }
        public string AirCondition { get; set; }
        public string SwimmingPool { get; set; }
        public string PriceNightlyMin { get; set; }
        public string PriceNightlyMax { get; set; }
        public string PriceWeeklyMin { get; set; }
        public string PriceWeeklyMax { get; set; }
        public string Images { get; set; }
        public string Status { get; set; }
        public decimal MonthlyExpense { get; set; }
        public string ExpenseNotes { get; set; }
        public DateTime? ActiveDateStart { get; set; }
        public DateTime? ActiveDateEnd { get; set; }
        public int CommissionId { get; set; }
        public decimal CommissionAmount { get; set; }
        public bool AutoCreateIncomeExpense { get; set; }
        public TimeSpan CheckInTime { get; set; }
        public TimeSpan CheckOutTime { get; set; }
        public bool IsLocallyCreated { get; set; }
        public bool IsParentProperty { get; set; }
        public int ParentType { get; set; }
        //public int ParentPropertyId { get; set; }
        public bool IsSyncParent { get; set; }
        public int SiteType { get; set; }
        public int CompanyId { get; set; }
        public string CalendarRemark { get; set; }
        public string Currency { get; set; }
        public string TimeZoneName { get; set; }
        public string TimeZoneId { get; set; }
        public bool IsActive { get; set; }
        public int? TwilioNumberId { get; set; }
        public bool AllowedSelfCheckinCheckout { get; set; }
        public bool? PriceRuleSync { get; set; }

        //For Local type property//
        public int? RefundableDamageDeposit { get; set; }
        public decimal? BaseDiscountsWeekly { get; set; }
        public decimal? BaseDiscountsMonthly { get; set; }
        public int? TaxRate { get; set; }
        public string CurrencyType { get; set; }
        public string ReturnDamageDeposit { get; set; }

        public string Headline { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPhone { get; set; }
        public int? GuestLimit { get; set; }
        public string StreetNumber { get; set; }
        public string Street { get; set; }
        public string UnitNumber { get; set; }
        public string Building { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string CountryCode { get; set; }
        public string Price { get; set; }
        public int? Size { get; set; }
    }
}
