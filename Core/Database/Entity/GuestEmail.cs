﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("GuestEmail")]
    public class GuestEmail
    {
        [Key]
        public int Id { get; set; }
        public string GuestId { get; set; }
        public string Email { get; set; }
    }
}
