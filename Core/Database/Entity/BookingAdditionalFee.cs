﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("BookingAdditionalFees")]
    public class BookingAdditionalFee
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public decimal? Amount { get; set; }
        public string Description { get; set; }
        public DateTime DueOn { get; set; }
        public int Frequency { get; set; } = 1;
        public int Type { set; get; } = 1;
    }
}
