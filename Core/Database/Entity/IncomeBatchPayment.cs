﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("IncomeBatchPayments")]
    public class IncomeBatchPayment
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public int BookingId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int CompanyId { get; set; }
        public bool IsRejected { get; set; }
        public int? InvoiceId { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();

    }
}
