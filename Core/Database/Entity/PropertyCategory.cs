﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("PropertyCategory")]
    public class PropertyCategory
    {
        [Key]
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string ControlType { get; set; }
        public string DisplayValue { get; set; }
    }
}