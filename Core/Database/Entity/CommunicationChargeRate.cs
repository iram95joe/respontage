﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationChargeRates")]
    public class CommunicationChargeRate
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int Type { get; set; }
        public DateTime EffectiveDate { get; set; }
        [NotMapped]
        public string TypeName => ((Core.Enumerations.CommunicationType)Type).ToString();
        [NotMapped]
        public string Actions => "<button class=\"ui mini button edit-rate\" data-tooltip=\"Edit\">" +
                                "<i class=\"icon edit outline\"></i></button>";
    }
}
