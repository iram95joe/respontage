﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("InvoiceTemplates")]
    public class InvoiceTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileUrl { get; set; }
        public int CompanyId { get; set; }
        [NotMapped]
        public string Html { get; set; }
    }
}
