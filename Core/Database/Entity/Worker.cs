﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Worker")]
    public class Worker
    {
        [Key]
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public decimal? FixedRate { get; set; }
        public int ?Frequency { get; set; }
        public decimal? RetainerRate { get; set; }
        public int? RetainerFrequency { get; set; }
        public int CompanyId { get; set; }
        public string ImageUrl { get; set; }
        public decimal? Bonus { get; set; }
        public ICollection<WorkerRetainer> WorkerRetainers { get; set; }
        public ICollection<WorkerContactInfo> WorkerContactInfos { get; set; }
        public ICollection<WorkerJob> WorkerJobs { get; set; }
    }
}
