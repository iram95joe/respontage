﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("PropertyDefaultWorkers")]
    public class PropertyDefaultWorker
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int WorkerId {get;set;}
        public int JobTypeId { get; set; }
        public int ?Type { set; get; }
        public bool IsAutoDelete { get; set; }
    }
}
