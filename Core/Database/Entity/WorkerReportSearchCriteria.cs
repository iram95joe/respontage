﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("WorkerReportSearchCriterias")]
    public class WorkerReportSearchCriteria
    {
        public int Id { get; set; }
        public string PropertyIds { get; set; }
        public string WorkerIds { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
    }
}
