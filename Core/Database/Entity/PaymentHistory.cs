﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("PaymentHistory")]
    public class PaymentHistory
    {
        [Key]
        public long Id { get; set; }

        public string ConfirmationCode { get; set; }

        public byte PaymentType { get; set; }

        public byte PaymentMethod { get; set; }

        public string PaymentName { get; set; }

        public decimal PaymentPrice { get; set; }

        public decimal BalanceAfter { get; set; }

        public bool Paid { get; set; }

        public string PaypalRedirectUrl { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
