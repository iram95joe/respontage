﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("LastMinuteDiscount")]
    public class LastminuteDiscount
    {
        [Key]
        public int LastMinuteDiscountId { get; set; }
        public int PropertyId { get; set; }
        public int DayNumber { get; set; }
        public decimal? Discount { get; set; }
    }
}