﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("MaintenanceEntries")]
    public class MaintenanceEntry
    {
        public int Id { get; set; }
        public decimal ?Amount { get; set; }
        public DateTime DueDate { get; set; }
        public int PropertyId { get; set; }
        public string Description { get; set; }
        public int? WorkerId { get; set; }
        public int? JobTypeId { get; set; }
        public int? VendorId { get; set; }
        public int PropertyScheduledExpenseId { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int Status { get; set; }
        public bool IsActive { get; set; } = true;
        public int CompanyId { get; set; }
    }
}
