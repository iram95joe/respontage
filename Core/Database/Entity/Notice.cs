﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("Notices")]
    public class Notice
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int NoticeTypeId { get; set; }
        public DateTime DateOfNotice { get; set; }
        public DateTime DateOfIncident { get; set; }
        public int CompanyId { get; set; }
        public int NoticeDeliveryMethodId { get; set; }
        public bool IsForRenter { get; set; }
    }
}
