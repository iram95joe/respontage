﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("DeliveredAutomatedMessage")]
    public class DeliveredAutomatedMessage
    {
        [Key]
        public int Id { get; set; }
        public long InquiryId { get; set; }
        public int EventType { get; set; }
        public bool IsSent { get; set; }
    }
}
