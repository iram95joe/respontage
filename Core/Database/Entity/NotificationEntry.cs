﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("NotificationEntries")]
    public class NotificationEntry
    {
        public int Id{get;set;}
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public int NotificationId { get; set; }
        //to check if 
        public int ?IncomeId { get; set; }
        public int? RentId { get; set; }
        public int? ReservationId { get; set; }
        public int PropertyId { get; set; }
        //No progress=0,on progress=1,complete=2
        public int Status { get; set; }
    }
}
