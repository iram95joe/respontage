﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("tbNoteLink")]
    public class tbNoteLink
    {
        [Key]
        public long Id { get; set; }
        public long NoteId { get; set; }
        public string NoteName { get; set; }
        public string NoteContent { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
