﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("NotificationRenter")]
    public class NotificationRenter
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int RenterId { get; set; }
    }
}
