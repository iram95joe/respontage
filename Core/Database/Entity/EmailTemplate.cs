﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("EmailTemplates")]
    public class EmailTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CompanyId { get; set; }
        [NotMapped]
        public string Actions =>"<button class=\"ui mini button edit-email-template\" data-tooltip=\"Edit\">" +
                                "<i class=\"icon edit outline\"></i></button>" +
                                "<button class=\"ui mini button delete-email-template\" data-tooltip=\"Delete\">" +
                                "<i class=\"icon trash\"></i></button>";
    }
}
