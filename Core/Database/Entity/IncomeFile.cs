﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("IncomeFiles")]
    public class IncomeFile
    {
        public int Id { get; set; }
        public int IncomeId { get; set; }
        public string FileUrl { get; set; }
    }
}
