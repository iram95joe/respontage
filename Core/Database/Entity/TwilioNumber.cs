using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("TwilioNumber")]
    public partial class TwilioNumber
    {
        [Key]
        public int Id { get; set; }
        public string Sid { get; set; }
        public int CompanyId { get; set; }
        public string FriendlyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string CallForwardingNumber { get; set; }
        public bool? SmsEnabled { get; set; }
        public bool? MmsEnabled { get; set; }
        public bool? VoiceEnabled { get; set; }
        public bool? FaxEnabled { get; set; }
        public int Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int CallLength { get; set; }
        public string VoicemailMessage { get; set; }
        public string CallforwardingMessage { get; set; }
    }
}
