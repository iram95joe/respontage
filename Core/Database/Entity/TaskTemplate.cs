﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("TaskTemplate")]
    public class TaskTemplate
    {
        [Key]
        public int Id { get; set; }

        public string TemplateName { get; set; }

        public DateTime DateCreated { get; set; }

        public int CompanyId { get; set; }

        public int? TemplateTypeId { get; set; }
    }
}
