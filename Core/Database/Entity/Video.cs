﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("Videos")]
    public class Video
    {
        [Key]
        public int Id { get; set; }
        public string VideoUrl { get; set; }
        public string OriginalVideoUrl { get; set; }
        public string ServerPath { get; set; }
        public int AccountId { get; set; }
        //JM 1/30/20 In Arlo details is order by deviceId,date created,utc date created
        public string Details { get; set; }
        public DateTime DateCreated { get; set; }
        public int LocalDeviceId { get; set; }
        public bool IsSaved { get; set; }
        public string SiteVideoId { get; set; }
        public bool IsWatched { get; set; }
        public bool IsDeleted { get; set; }
        public int InCount { get; set; }
        public int OutCount { get; set; }
        public bool IsProcessed { get; set; }
    }
}
