﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("RenterContacts")]
    public class RenterContact
    {
        public int Id { get; set; }
        public int RenterId { get; set; }
        public string Contact { get; set; }
        public bool IsDefault { get; set; } = true;
    }
}
