﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("PropertyImages")]
    public class PropertyImage
    {
        [Key]
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public string FileUrl { get; set; }
    }
}