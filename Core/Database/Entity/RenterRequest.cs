﻿using Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("RenterRequests")]
    public class RenterRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int ContractRequestId { get; set; }
        public string JobHistory { get; set; }
        public string AddressHistory { get; set; }
        [NotMapped]
        public List<RenterRequestFile> Files { get; set; }
        [NotMapped]
        public RequestRenterFileModel [] RenterFiles { get; set; }
        [NotMapped]
        public List<string> JobList => JsonConvert.DeserializeObject<List<string>>(JobHistory);
        [NotMapped]
        public List<string> AddressList => JsonConvert.DeserializeObject<List<string>>(AddressHistory);

        public string FileIcon(string url)
        {
            var extention = url.Split('.').Last();
            if (extention == "pdf")
            {
                return "file pdf outline";
            }
            else if (extention == "docx" || extention == "docm" || extention == "dot")
            {
                return "file word outline";
            }
            else if (extention == "xlsx" || extention == "xlsm" || extention == "xlsb")
            {
                return "file excel outline";
            }
            else
            {
                return "file alternate";
            }
        }
        public bool IsImage(string url)
        { var extention = url.Split('.').Last();
            if (extention=="jpg" || extention == "png"|| extention == "gif")
            return true;
            else
            return false;
        }
    }
}
