using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("CashFlowType")]
    public partial class CashFlowType
    {
        [Key]
        public int Id { get; set; }
        public string CashFlowTypeName { get; set; }
    }
}
