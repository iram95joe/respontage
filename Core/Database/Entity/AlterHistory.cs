﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("AlterHistory")]
    public class AlterHistory
    {
        [Key]
        public long Id { get; set; }

        public double Price { get; set; }

        public double PriceDifference { get; set; }

        public DateTime CheckInDate { get; set; }

        public DateTime CheckOutDate { get; set; }

        public int Adults { get; set; }

        public int Children { get; set; }

        public int GuestCount { get; set; }

        public int SiteType { get; set; }

        public long ListingId { get; set; }

        public string ThreadId { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}