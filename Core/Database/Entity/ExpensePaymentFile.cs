﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ExpensePaymentFiles")]
    public class ExpensePaymentFile
    {
        public int Id { get; set; }
        public int? PaymentId { get; set; }
        public int? BatchPaymentId { get; set; }
        public string FileUrl { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}
