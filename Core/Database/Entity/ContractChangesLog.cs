﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ContractChangesLogs")]
    public class ContractChangesLog
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public string Message { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
