﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ExpenseBatchPayments")]
    public class ExpenseBatchPayment
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime PaymentDate { get; set; }
        public int CompanyId { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}
