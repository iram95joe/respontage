﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("RenterDocuments")]
    public class RenterDocument
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string FileUrls { get; set; }
        public int BookingId { get; set; }

        [NotMapped]
        public List<string> Files=> string.IsNullOrWhiteSpace(FileUrls) ? new List<string>() : FileUrls.Split(',').ToList();
        [NotMapped]
        public string Actions => "";
    }
}
