﻿using Core.Models.Invoice;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("Invoices")]
    public class Invoice
    {
        public int Id { get; set; }
        public string PaymentUrl { get; set; }
        public string FileUrl { get; set; }
        public DateTime DateCreated { get; set; }
        public int BookingId { get; set; }
        public int Status { get; set; }
        public int PaymentMethod { get; set; }
        public decimal TotalAmount { get;set; }
        public string RequestPaymentJsonString { get; set; }
        public string SitePaymentId { get; set; }
        public int InvoiceScheduleId { get; set; }
        [NotMapped]
        public string StatusName => Status == 0 ? "Sent" : Status == 1 ? "Initiated" : Status == 2? "Pending":"Complete";
        [NotMapped]
        public string PaymentMethodName => ((Core.Enumerations.PaymentRequestMethod)PaymentMethod).ToString();
        [NotMapped]
        public List<RequestAmountViewModel> RequestPayment => JsonConvert.DeserializeObject<List<RequestAmountViewModel>>(RequestPaymentJsonString);
        [NotMapped]
        public string Actions=>"";
    }
}
