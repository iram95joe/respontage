﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("PropertyBookingDate")]
    public class PropertyBookingDate
    {
        [Key]
        public int Id { get; set; }

        public long PropertyId { get; set; }

        public DateTime Date { get; set; }

        public bool IsAvailable { get; set; }

        public double Price { get; set; }

        public byte SiteType { get; set; }

        public string ReservationId { get; set; }
        
        public long? RoomId { get; set; }

        public bool FromIcal { get; set; }

        public string IcalTags { get; set; }
    }
}
