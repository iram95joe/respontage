﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("WorkerEmails")]
    public class WorkerEmail
    {
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public string Email { get; set; }
        public bool IsDefault { get; set; } = true;
    }
}
