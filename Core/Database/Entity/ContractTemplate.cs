﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ContractTemplates")]
    public class ContractTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int CompanyId { get; set; }
        public long ListingId { get; set; }
        //0 is default and 1 for custom
        public int TemplateType { get; set; }
        [NotMapped]
        public string Html { get; set; }
    }
}
