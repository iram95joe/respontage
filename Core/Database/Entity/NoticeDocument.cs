﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("NoticeDocuments")]
    public class NoticeDocument
    {
        public int Id { get; set; }
        public string AttachmentPath { get; set; }
        public int NoticeId { get; set; }
        [NotMapped]
        public string Html { get; set; }
    }
}
