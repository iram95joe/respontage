﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("InquirySubTask")]
    public class InquirySubTask
    {
        public int Id { get; set; }
        public int InquiryTaskId { get; set; }
        public int SubTaskId { get; set; }
        public bool Status { get; set; }
        public int? UserId { get; set; }
        public DateTime? LastUpdateDate { get; set; }

    }
}