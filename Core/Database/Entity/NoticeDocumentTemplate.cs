﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    public class NoticeDocumentTemplate
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        [NotMapped]
        public string Html { get; set; }
    }
}
