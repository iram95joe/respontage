﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("VendorContactNumbers")]
    public class VendorContactNumber
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public string Value { get; set; }
        public bool IsDefault { get; set; }
    }
}
