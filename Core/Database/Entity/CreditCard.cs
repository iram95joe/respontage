﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    public class CreditCard
    {
        [Key]
        public long Id { get; set; }

        public string ReservationId { get; set; }

        public string Type { get; set; }

        public string Number { get; set; }

        public string HolderName { get; set; }

        public string CVC { get; set; }

        public string Expiry { get; set; }

        public string CardAlertHeading { get; set; }

        public string CardAlertDescription { get; set; }

        public string PaymentStatus { get; set; }

        public string CardStatus { get; set; }

        public string CardViewLimit { get; set; }

        public string ReplyActions { get; set; }
    }
}
