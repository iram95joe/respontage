﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("PropertyNotice")]
    public class PropertyNotice
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int NoticeId { get; set; }
    }
}
