﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("ResidenceHistory")]
    public class ResidenceHistory
    {
        [Key]
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string ResidenceAddress { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
    }
}