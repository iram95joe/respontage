﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("WorkerContactInfo")]
    public class WorkerContactInfo
    {
        [Key]
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public int? Type { get; set; }
        public string Value { get; set; }
        public bool IsDefault { get; set; }
        //public Worker Worker { get; set; }
    }
}
