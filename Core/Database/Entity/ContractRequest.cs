﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ContractRequests")]
    public class ContractRequest
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PropertyId { get; set; }
        public DateTime RequestDate { get; set; }
        //0 pending,1 accepted,2 rejected
        public int Status { get; set; }
    }
}
