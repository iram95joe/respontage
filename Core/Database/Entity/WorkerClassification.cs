﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Database.Entity
{
    [Table("WorkerClassification")]
    public class WorkerClassification
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool HasArrivalDeparture { get; set; }
    }
}