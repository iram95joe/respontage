﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Task")]
    public class Task
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public int TemplateId { get; set; }

        public string ImageUrl { get; set; }

        [NotMapped]
        public string PictureName { get; set; } // Use in application level.

        [NotMapped]
        public int Status { get; set; } // Use in application level.

        [NotMapped]
        public int InquiryTaskId { get; set; }

        [NotMapped]
        public string TemplateName { get; set; }
    }
}
