﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("User")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int CompanyId { get; set; }
        public string TimeZone { get; set; }
        public bool ConfirmEmail { get; set; }
        public bool ValidStatus { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? WorkerId { get; set; }
        public string ImageUrl { get; set; }
    }
}
