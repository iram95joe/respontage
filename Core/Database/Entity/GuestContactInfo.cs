﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("GuestContactInfo")]
    public class GuestContactInfo
    {
        [Key]
        public int Id { get; set; }
        public int GuestId { get; set; }
        public int? Type { get; set; }
        public string Value { get; set; }
        public int Source { get; set; }
        public bool IsDefault { get; set; }
    }
}