﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationThreads")]
    public class CommunicationThread
    {
        public int Id { get; set; }
        public string ThreadId { get; set; }
        public int? WorkerId { get; set; }
        public int? VendorId { get; set; }
        public int? RenterId { get; set; }
        public int CompanyId { get; set; }
        public bool IsRenter { get; set; }
        public bool IsEnd { get; set; }
    }
}
