﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("IncomePayment")]
    public class IncomePayment
    {
        [Key]
        public int Id { get; set; }
        public int IncomeId { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public int? IncomeBatchPaymentId { get; set; }
        public bool IsRejected { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}
