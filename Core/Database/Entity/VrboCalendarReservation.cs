﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel;

namespace Core.Database.Entity
{
    [Table("VrboCalendarReservation")]
    public class VrboCalendarReservation
    {
        [Key]
        public long Id { get; set; }

        public long ListingId { get; set; }

        public string ListingTriad { get; set; }

        public string Druid { get; set; }

        public string AvailabilityStatus { get; set; }

        public string Content { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ReservationId { get; set; }

        public string iCallId { get; set; }

        public string ThreadId { get; set; }

        public string Type { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public bool Deleted { get; set; }

        public bool Active { get; set; }
    }
}
