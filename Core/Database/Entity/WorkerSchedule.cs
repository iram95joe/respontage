﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;


namespace Core.Database.Entity
{
    [Table("WorkerSchedule")]
    public class WorkerSchedule
    {
        [Key]
        public int Id { get; set; }
        public int UniqueId { get; set; }
        public int WorkerId { get; set; }
        public string AvailDay { get; set; }
        public string AvailTimeStart { get; set; }
        public string AvailTimeEnd { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Status { get; set; }
        public int CompanyId { get; set; }
    }
}
