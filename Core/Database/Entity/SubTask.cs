﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("SubTask")]
    public class SubTask
    {
       public int Id { get; set; }
       public string Title { get; set; }
       public string Description { get; set; }
       public int TaskId { get; set; }
       public string ImageUrl{ get; set; }

    }
}
