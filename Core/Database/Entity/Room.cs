﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("Room")]
    public class Room
    {
        [Key]
        public int Id { get; set; }

        public long ListingId { get; set; }

        public long RoomId { get; set; }

        public string RoomName { get; set; }
    }
}
