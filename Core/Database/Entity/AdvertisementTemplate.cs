﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("AdvertisementTemplates")]
    public class AdvertisementTemplate
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int CompanyId { get; set; }
        public string Title { get; set; }
        public string GeographicArea { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int Bedrooms { get; set; }
        public int Bathrooms { get; set; }
        public string PostalCode { get; set; }
        public string SurfaceArea { get; set; }
        public int ?HousingTypeId { get; set; }
        public int ?LaundryId { get; set; }
        public int ?ParkingId { get; set; }
        public int ?FlooringId { get; set; }
        public int ?RentPeriodId { get; set; }
        public string FromEMail { get; set; }
        public DateTime ?MoveinDate { get; set; }
        public DateTime? SaleDate1 { get; set; }
        public DateTime? SaleDate2 { get; set; }
        public DateTime? SaleDate3 { get; set; }
        public int AdvertismentSite { get; set; }
        //Lamudi
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int? PropertyType { get; set; }
        public int? PropertySubType { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Brgy { get; set; }
        public string Address { get; set; }
        public string YoutubeUrl { get; set; }
        public string TourUrl { get; set; }
    }
}
