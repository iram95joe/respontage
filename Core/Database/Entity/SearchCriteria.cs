﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("SearchCriterias")]
    public class SearchCriteria
    {
        public int Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string PropertyIds { get; set; }
        public string Name { get; set; }
        public int ReportType { get; set; }
        public bool IsPaid { get; set; }
        public int Type { get; set; }
        public int UserId { get; set; }
    }
}
