﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Enumerations;

namespace Core.Database.Entity
{
    [Table("Notifications")]
    public class Notification
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Interval { get; set; }
        public int Frequency { get; set; }
        public int ScheduleType { get; set; }
        public int CompanyId { get; set; }
        [NotMapped] 
        public string ScheduleTypeName => ((NotificationScheduleType)ScheduleType).ToString().ToSentence();
        [NotMapped]
        public string FrequencyName => ((NotificationFrequency)Frequency).ToString();
        public string Actions=> "<button class=\"ui mini button edit-notification\" data-tooltip=\"Edit\">" +
                                 "<i class=\"icon edit outline\"></i></button>" +
                                 "<button class=\"ui mini button delete-notification\" data-tooltip=\"Delete\">" +
                                 "<i class=\"icon trash\"></i></button>";
    }
}
