﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("ExpenseFiles")]
    public class ExpenseFile
    {
        public int Id { get; set; }
        public int ExpenseId { get; set; }
        public string FileUrl { get; set; }
    }
}
