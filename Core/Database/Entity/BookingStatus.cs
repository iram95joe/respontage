﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Database.Entity
{
    [Table("BookingStatus")]
    public class BookingStatus
    {
        [Key]
        public string Code { get; set; }
        public string Description { get; set; }
    }
}