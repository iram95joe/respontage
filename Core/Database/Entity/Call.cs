﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Call")]
    public class Call
    {
        [Key]
        public int Id { get; set; }
        public long GuestId { get; set; }
        public Int32 Length { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public bool IsMyMessage { get; set; }
        public string FromCountry { get; set; }
        public string RecordingSid { get; set; }
        public DateTime? RecordingStartTime { get; set; }
        public int? RecordingDuration { get; set; }
        public int? CallDuration { get; set; }
        public string RecordingUrl { get; set; }
        public string CallSid { get; set; }
        public string TranscriptionText { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UserId { get; set; }
    }
}
