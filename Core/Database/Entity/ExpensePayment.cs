﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Database.Entity
{
    [Table("ExpensePayment")]
    public class ExpensePayment
    {
        [Key]
        public int Id { get; set; }
        public int ?ExpenseId { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ?ExpenseBatchPaymentId { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }
}