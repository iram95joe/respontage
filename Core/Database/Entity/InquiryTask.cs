﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Database.Entity
{
    [Table("InquiryTask")]
    public class InquiryTask
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public int TaskId { get; set; }

        public int Status { get; set; }

        public string ConfirmationCode { get; set; }

        public int TemplateId { get; set; }

        public int CompanyId { get; set; }

        public string TaskTitle { get; set; }

        public string TaskDescription { get; set; }

        public string TemplateName { get; set; }

        public int TemplateTypeId { get; set; }

        [NotMapped]
        public List<Core.Models.Task.SubTask> SubTasks = new List<Models.Task.SubTask>();
    }
}