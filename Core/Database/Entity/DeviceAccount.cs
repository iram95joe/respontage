﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("DeviceAccounts")]
    public class DeviceAccount
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int SiteType { get; set; }
        public int CompanyId { get; set; }
        //Token,Region,AccountId,ClientId are for Blink
        public string Token { get; set; }
        public string Region { get; set; }
        public string AccountId { get; set; }
        public string ClientId { get; set; }
    }
}
