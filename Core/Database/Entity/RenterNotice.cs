﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("RenterNotices")]
    public class RenterNotice
    {
        public int Id { get; set; }
        public int RenterId { get; set; }
        public int NoticeId { get; set; }
    }
}
