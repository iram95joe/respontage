﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel;

namespace Core.Database.Entity
{
    [Table("PropertySyncSettings")]
    public class PropertySyncSettings
    {
        [Key]
        public long Id { get; set; }
        
        public int PropertyLocalId { get; set; }

        public double? AirbnbAdjustmentValue { get; set; }

        public int? AirbnbSymbolType { get; set; }

        public int? AirbnbAdjustmentType { get; set; }

        public double? VrboAdjustmentValue { get; set; }

        public int? VrboSymbolType { get; set; }

        public int? VrboAdjustmentType { get; set; }

        public double? BookingComAdjustmentValue { get; set; }

        public int? BookingComSymbolType { get; set; }

        public int? BookingComAdjustmentType { get; set; }

        public bool SyncPrice { get; set; }

        public bool SyncAvailability { get; set; }
    }
}
