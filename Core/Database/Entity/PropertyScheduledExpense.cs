﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    //This is the storage of scheduled maintenance and expenses
    [Table("PropertyScheduledExpenses")]
    public class PropertyScheduledExpense
    {
       public int Id { get; set; }
       public decimal Amount { get; set; }
       public DateTime RecurringDate { get; set; }
        public DateTime? RecurringEndDate { get; set; }
       public int PropertyId { get; set; }
       public string Description { get; set; }
       public int Frequency { get; set; }
       public int Interval { get; set; }
       public bool IsDataCreated { get; set; }
       public int ExpenseCategoryId { get; set; }
       public int PaymentMethodId { get; set; }
        public int? WorkerId { get; set; }
        public int? JobTypeId { get; set; }
        public int? VendorId { get; set; }
        //JM 08/04/19 Expense =0,Maintenance = 1
        public int Type { get; set; }
        public int CompanyId { get; set; }
    }
}
