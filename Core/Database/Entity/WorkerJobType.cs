﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Database.Entity
{
    [Table("WorkerJobType")]
    public class WorkerJobType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }

        public int CompanyId { get; set; }

        public int? ClassificationId { get; set; }
    }
}