﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("tbRolePage")]
    public class tbRolePage
    {
        [Key]
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string PageName { get; set; }
        public string PageUrl { get; set; }
        public bool Status { get; set; }
        public string IconPath { get; set; }
        public int PageOrder { get; set; }
    }
}