﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{

    [Table("LockInbox")]
    public class LockInbox
    {
        public int Id { get; set; }
        public string ThreadId{ get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
