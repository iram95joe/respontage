﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    public class Review
    {
        [Key]
        public int Id { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Name { get; set; }

        public string ProfilePictureUrl { get; set; }

        public string Comments { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
