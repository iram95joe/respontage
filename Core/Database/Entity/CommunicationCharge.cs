﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CommunicationCharges")]
    public class CommunicationCharge
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int CompanyId { get; set; }
        public int CommunicationSMSId { get; set; }
        public int CommunicationCallId { get; set; }
    }
}
