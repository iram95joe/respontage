﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("CampaignAdvertisementChannels")]
    public class CampaignAdvertisementChannel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int Status { get; set; }
        public int Views { get; set; }
        public int Requests { get; set; }
        public int CampaignAdvertisementId { get; set; }
        public int AdvertismentSite { get; set; }
        [NotMapped]
        public string Actions => "<a target=\"_blank\" href=\""+Url+"\"><button class=\"ui mini button \" data-tooltip=\"View\">" +
                                 "<i class=\"icon edit outline\"></i></button></a>"+
            "<button class=\"ui mini button delete-channel\" data-tooltip=\"Delete\">" +
                                            "<i class=\"icon trash\"></i></button>";
    }
}
