﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Core.Models;

namespace Core.Database.Entity
{
    [Table("Income")]
    public class Income
    {
        [Key]
        public int Id { get; set; }
        public int IncomeTypeId { get; set; }
        public int BookingId { get; set; }
        public int PropertyId { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public int Status { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateRecorded { get; set; }
        public bool IsBatchLoad { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
        public int? BookingAdditionalFeeId { get; set; }
    }
}
