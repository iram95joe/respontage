﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;


namespace Core.Database.Entity
{
    [Table("WorkerException")]
    public class WorkerException
    {
        [Key]
        public int Id { get; set; }
        public int WorkerId { get; set; }
        public System.DateTime ExceptionStart { get; set; }
        public System.DateTime ExceptionEnd { get; set; }
        public string ExceptionType { get; set; }
        public int CompanyId { get; set; }
    }
}
