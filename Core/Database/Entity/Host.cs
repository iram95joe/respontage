﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("Host")]
    public class Host
    {
        [Key]
        public int Id { get; set; }
        public string SiteHostId { get; set; }
        public int SiteType { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string GmailPassword { get; set; }
        public bool AutoSync { get; set; }
        public int ?TwilioNumberId { get; set; }
    }
}
