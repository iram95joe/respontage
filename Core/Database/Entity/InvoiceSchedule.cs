﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("InvoiceSchedules")]
    public class InvoiceSchedule
    {
        
        public int Id { get; set; }
        public int DayOfMonth { get; set; }
        public string PropertyIds { get; set; }
        public int EmailTemplateId { get; set; }
        public int InvoiceTemplateId { get; set; }
        public int PaymentMethod { get; set; }
        public int CompanyId { get; set; }
        public string Description { get; set; }
    }
}
