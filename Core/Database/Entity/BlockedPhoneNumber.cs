using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("BlockedPhoneNumber")]
    public partial class BlockedPhoneNumber
    {
        [Key]
        public int Id { get; set; }
        public int? CompanyId { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? BlockedDate { get; set; }
    }
}
