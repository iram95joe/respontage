﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
   public class ReimbursementNote
    {
        public int Id { get; set; }
        public int AssignmentId { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
    }
}
