﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("VideoNotes")]
    public class VideoNote
    {
        public int Id {get;set;}
        public int VideoId { get; set; }
        public string Note { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
