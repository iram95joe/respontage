﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database.Entity
{
    [Table("WorkerMessageRules")]
    public class WorkerMessageRule
    {
        [Key]
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public int Type { get; set; }
        public int WorkerTemplateMessageId { get; set; }
        public TimeSpan ?Time { get; set; }
    }
}
