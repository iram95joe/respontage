
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("PropertyFee")]
    public class PropertyFee
    {
        [Key]
        public int Id { get; set; }
        public int FeeTypeId { get; set; }
        public int ComputeBasis { get; set; }
        public bool Show { get; set; }
        public int Method { get; set; }
        public decimal AmountPercent { get; set; }
        public int PropertyId { get; set; }
        public int InclusiveOrExclusive { get; set; }
        public bool IsPreDeducted { get; set; }
        public decimal CleaningFee { get; set; }
        public decimal ServiceFee { get; set; }
    }
}
