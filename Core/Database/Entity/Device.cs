﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
namespace Core.Database.Entity
{
    [Table("Devices")]
    //This devices entity is also a location entity for smartthings
    public class Device
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public int AccountId { get; set; }
        public long? ListingId { get; set; }
        [NotMapped]
        public Property Property { get; set; }
        [NotMapped]
        public List<Lock> Locks { get; set; }

    }
}
