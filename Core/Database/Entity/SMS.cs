﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Core.Database.Entity
{
    [Table("SMS")]
    public class SMS
    {
        [Key]
        public int Id { get; set; }
        public long GuestId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
        public bool IsMyMessage { get; set; }
        public string FromCountry { get; set; }
        public string MessageSid { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ?UserId { get; set; }
    }
}