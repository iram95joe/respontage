﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.ComponentModel;

namespace Core.Database.Entity
{
    [Table("PriceRuleSettings")]
    public class PriceRuleSettings
    {
        [Key]
        public long Id { get; set; }

        public long ListingId { get; set; }

        public int? PriceRuleBasis { get; set; }

        public int? PriceRuleCondition { get; set; }

        public double? PriceRuleConditionPercentage { get; set; }

        public double? PriceRuleAmount { get; set; }

        public int? PriceRuleSign { get; set; }

        public int? PriceRuleAveragePriceBasis { get; set; }

        public int CompanyId { get; set; }
    }
}
