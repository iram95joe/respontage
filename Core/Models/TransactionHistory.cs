﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    #region TRANSACTION HISTORY
    public class TransactionHistory
    {
        public TransactionHistory()
        {
            Reservations = new List<TransactionReservation>();
        }
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public string PayoutId { get; set; }

        public List<TransactionReservation> Reservations { get; set; }

    }
    public class TransactionReservation
    {
        public string ListingId { get; set; }
        public string ListingName { get; set; }
        public string GuestName { get; set; }
        public string ReservationCode { get; set; }
        public long ReservationId { get; set; }
    }
    #endregion
}
