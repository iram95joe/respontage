﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MessageThreadDetail
    {
        public string SiteBaseUrl { get; set; }

        public long InquiryId { get; set; }
        public int LocalPropertyId { get; set; }
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public long GuestId { get; set; }
        public int GuestCount { get; set; }
        public string GuestName { get; set; }
        public string GuestFullName { get; set; }
        public string GuestProfilePictureUrl { get; set; }
        public string Status { get; set; }
        public bool CanPreApproveInquiry { get; set; }
        public bool CanWithdrawPreApprovalInquiry { get; set; }
        public bool IsSpecialOfferSent { get; set; }
        public int HostId { get; set; }
        public string HostName { get; set; }
        public string HostPicture { get; set; }
        public decimal ReservationCost { get; set; }
        public decimal CleaningCost { get; set; }
        public decimal AirbnbFee { get; set; }
        public decimal Payout { get; set; }
        public int SiteType { get; set; }
        public string PropertyAddress { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string TimeZoneId { get; set; }
        public int UserWorkerId { get; set; }
        public List<ThreadInquiry> Inquiries { get; set; }
        public int? Source { get; set; }
        public DateTime? InquiryDate { get; set; }

        public string CctvFootage { get; set; }
        public string Location { get; set; }
        public string Rating { get; set; }
        public string JoinDate { get; set; }
        public int Nights { get; set; }
        public decimal NightsFee { get; set; }
        public string NightsFeeSummary { get; set; }

    }
}
