﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CommunicationMessagesModel
    {
        //public int Id { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool IsMyMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Source { get; set; }
        public string RecordingUrl { get; set; }
        public string ResourceSid { get; set; }
        public int? Duration { get; set; }
        public int Type { get; set; }
        public List<string> Images { get; set; }

    }
}
