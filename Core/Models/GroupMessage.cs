﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class GroupMessage
    {
        public string ThreadId { get; set; }
        public int HostId { get; set; }
        public string GuestId { get; set; }
        public string GuestName { get; set; }
        public string GuestPicture { get; set; }
        public bool IsIncoming { get; set; }
        public string Status { get; set; }
        public bool IsAltered { get; set; }
        public bool IsActive { get; set; }
        public string ReservationCode { get; set; }
        public DateTime? StartStayDate { get; set; }
        public string MessageContent { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool ReadStatus { get; set; }
    }
}
