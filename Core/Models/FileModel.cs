﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public string FileUrl { get; set; }
    }
}
