﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.Reports.Profit
{
    public class Profit
    {
        public class PropertyRevenueMonthly
        {
            public int CompanyId { get; set; }
            public int TotalMonth { get; set; }
            public string MonthName { get; set; }
            public decimal TotalExpense { get; set; }
            public decimal ResultAmount { get; set; }
        }

        public class PropertyDetailMonths
        {
            public string PropertyName { get; set; }
            public int TotalMonth { get; set; }
            public List<PropertyDetailRevenueMonthly> Result { get; set; }
        }

        public class PropertyDetailRevenueMonthly
        {
            public string MonthName { get; set; }
            public decimal ResultAmount { get; set; }
        }
    }
}
