﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.Reports
{
    public class SearchDateModel
    {
        //public int DateFromMonth { get; set; }
        //public int DateFromYear { get; set; }

        //public int DateToMonth { get; set; }

        //public int DateToYear { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

    }
}
