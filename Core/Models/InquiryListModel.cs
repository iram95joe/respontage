﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class InquiryListModel
    {
        public Inquiry Inquiry { get; set; }
        public Property Property { get; set; }
        public Guest Guest { get; set; }
    }

    public class ReservationRates
    {
        public string Date { get; set; }
        public string DateString { get; set; }
        public string RateId { get; set; }
        public string RateName { get; set; }
        public string Price { get; set; }

    }

    public class ReservationFee
    {
        public string FeeName { get; set; }
        public string Rate { get; set; }
        public string Price { get; set; }

    }
}
