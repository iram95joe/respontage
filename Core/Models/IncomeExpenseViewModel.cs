﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class IncomeExpenseViewModel
    {
        public string Property { get; set; }
        public string Amount { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
    }
}
