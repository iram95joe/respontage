﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CommunicationInboxThread
    {

        public int?WorkerId { get; set; }
        public int? VendorId { get; set; }
        public int? RenterId { get; set; }
        public string ThreadId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? LastMessageAt { get; set; }
        public string LastMessage { get; set; }
        public bool IsIncoming { get; set; }
    }
}
