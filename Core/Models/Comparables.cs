﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Comparables
    {
        public bool IsPrivateRoom { get; set; }
        public string Ratings { get; set; }
        public int Bedrooms { get; set; }

        public double Bathrooms { get; set; }

        public string Distance { get; set; }

        public string Price { get; set; }

        public string ListingId { get; set; }

        public string ListingName { get; set; }

        public string ListingUrl { get; set; }

        public string ListingImage { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string HostName { get; set; }

        public string HostAbout { get; set; }

        public string HostAddress { get; set; }

        public string HostImageUrl { get; set; }

        public string HostMemberSince { get; set; }

        public string Site { get; set; }
        public bool IsAvailable { get; set; }
        public string Fees { get; set; }
        public string AverageOccupancies { get; set; }
        public string DateUpdated { get; set; }
    }
}
