﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class InboxMessage
    {
        public Inbox Inbox { get; set; }
        public List<Message> Message { get; set; }
    }

}
