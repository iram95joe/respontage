﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Reviews
    {
        public string Name { get; set; }

        public string ProfilePictureUrl { get; set; }

        public string Comments { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
