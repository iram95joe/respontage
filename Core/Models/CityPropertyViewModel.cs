﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CityPropertyViewModel
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int PropertyCount { get; set; }
        public int PropertyActive { get; set; }
        public int PropertyInActive { get; set; }
    }
}
