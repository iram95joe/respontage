﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class WorkerInboxListing
    {
        public CommunicationInboxThread Thread = new CommunicationInboxThread();
        public List<CommunicationMessagesModel> Messages = new List<CommunicationMessagesModel>();
    }
}
