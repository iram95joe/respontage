﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Database.Entity;

namespace Core.Models
{
    public class WorkerJobAssignment
    {
        public WorkerJob WorkerJob { get; set; }
        public int Count { get; set; }
    }
}
