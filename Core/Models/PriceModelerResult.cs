﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class PriceModelerResult
    {
        public string Analytics { get; set; }

        public int Bedrooms { get; set; }

        public int HostId { get; set; }

        public long CalendarId { get; set; }

        public string Comparables { get; set; }

        public string RuleUsedAndComputations { get; set; }
    }
}
