﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class GuestReviews
    {
        public string Name { get; set; }

        public string Photo { get; set; }

        public string Review { get; set; }

        public int Rating { get; set; }

        public string Property { get; set; }
    }
}
