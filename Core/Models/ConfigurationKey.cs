﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ConfigurationKey
    {
        public string PaypalClientId { get; set; }

        public string PaypalClientSecret { get; set; }

        public string SquareAccessToken { get; set; }

        public string SquareApplicationId { get; set; }

        public string SquareLocationId { get; set; }

        public string StripePublishableKey { get; set; }

        public string StripeSecretKey { get; set; }
    }
}
