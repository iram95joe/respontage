﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.Models
{
    public class RequestRenterFileModel
    {
        public int Id { get; set; }
        public int RenterRequestAttachmentDescriptionId { get; set; }
        public string Description { get; set; }
        public HttpPostedFileWrapper[] Files { get; set; }
    }
}
