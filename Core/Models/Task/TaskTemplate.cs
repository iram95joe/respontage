﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.Task
{
    public class TaskTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Task> Tasks = new List<Task>();
    }
}
