﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models.Task;
namespace Core.Models.Task
{
    public class TasksViewModel
    {
        public Inquiry Inquiry { get; set; }

        public Guest GuestInfo { get; set; }

        public Property PropertyInfo { get; set; }
        public List<Task> InquiryTask { get; set; }
        public List<Task> TurnoverTask { get; set; }
        public List<Task> BookingTask { get; set; }
        public List<Task> CheckInTask { get; set; }
        public List<Task> CheckOutTask { get; set; }

        public List<TravelEventsViewModel> TravelEvents { get; set; }

        public List<AssignedWorkerInfo> AssignedWorkers { get; set; }

        //Add by Joe Mari 10/09/2018
        public List<AssignedWorkerInfo> CheckInWorkers { get; set; }

        //Add by Joe Mari 10/09/2018
        public List<AssignedWorkerInfo> CheckOutWorkers { get; set; }

        //Add by Joe Mari 10/09/2018
        public List<AssignedWorkerInfo> PreCheckInTurnoverWorkers { get; set; }

        //Add by Joe Mari 10/09/2018
        public List<AssignedWorkerInfo> PostCheckOutTurnoverWorkers { get; set; }

    }

    [NotMapped]
    public class TravelEventsViewModel : Note
    {
        public DateTime? ComparisonDate { get; set; } //Use it to sort for Travel Events
    }

    public class AssignedWorkerInfo
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Mobile { get; set; }

        //JM 03/05/19 Status are 0=pending,1=accepted,2=decline
        public int Status { get; set; }
        //JM 03/05/19 InDayStatus are 1=on the way,2=start,3=finish
        public int InDayStatus { get; set; }
    }
}
