﻿using Core.Enumerations;

namespace Core.Models
{
    public class HostAccount
    {
        public int Id { get; set; }
        public string SiteHostId { get; set; }
        public SiteType SiteType { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SessionToken { get; set; }
    }
}