﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ThreadInquiry
    {
        public int Id { get; set; }
        public DateTime? InquiryDate { get; set; }
        public DateTime? ConfirmationDate { get; set; }
        public DateTime? BookingCancelDate { get; set; }
        public DateTime? BookingConfirmCancelDate { get; set; }
        public string InquiryId { get; set; }
        public int LocalPropertyId { get; set; }
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public string ConfirmationCode { get; set; }
        public bool CanPreApproveInquiry { get; set; }
        public bool CanWithdrawPreApprovalInquiry { get; set; }
        public bool IsSpecialOfferSent { get; set; }
        public bool IsActive { get; set; }
        public bool IsAltered { get; set; }
        public string Email { get; set; }
        public long GuestId { get; set; }
        public int GuestCount { get; set; }
        public string Status { get; set; }
        public decimal ReservationCost { get; set; }
        public decimal CleaningCost { get; set; }
        public decimal AirbnbFee { get; set; }
        public decimal Payout { get; set; }
        public int SiteType { get; set; }

        public string FeesSerializedJson { get; set; }
        public string RateDetailsSerializedJson { get; set; }
        public List<Core.Models.ReservationFee> Fees { get; set; }
        public List<Core.Models.ReservationRates> RateDetails { get; set; }
        public List<Core.Database.Entity.PaymentHistory> PaymentHistory { get; set; }
        public List<Core.Database.Entity.AlterHistory> AlterHistory { get; set; }
        public List<Core.Database.Entity.Income> Incomes { get; set; }
        public decimal SumPerNight { get; set; }
        public string PhoneNumber { get; set; }
        public string PropertyAddress { get; set; }
        public string TimeZoneId { get; set; }

        public int ?Source { get; set; }
        public string Location { get; set; }
        public decimal NightsFee { get; set; }
        public string NightsFeeSummary { get; set; }
        public int Nights { get; set; }
        public bool HasWorkerAssignment { get; set; }
        public bool HasTravelEvent { get; set; }

    }
}
