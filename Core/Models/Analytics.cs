﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Analytics
    {
        public double AvailabilityRateByDate { get; set; }

        public double AvailabilityRateByDateForXRooms { get; set; }

        public double AvailabilityRateByMonth { get; set; }

        public double AvailabilityRateByMonthForXRooms { get; set; }

        public double AveragePriceByDate { get; set; }

        public double AveragePriceByDateForXRooms { get; set; }

        public double AveragePriceByMonth { get; set; }

        public double AveragePriceByMonthForXRooms { get; set; }

        public double OccupancyRateByDate { get; set; }

        public double OccupancyRateByDateForXRooms { get; set; }

        public double OccupancyRateByMonth { get; set; }

        public double OccupancyRateByMonthForXRooms { get; set; }

        public int PropertyCountByDate { get; set; }

        public int PropertyCountByDateForXRooms { get; set; }

        public int TotalPropertyCountByDate { get; set; }

        public int TotalPropertyCountByDateForXRooms { get; set; }
        public string PropertyCountByBedrooms { get; set; }
        public string NearPropertyCountByBedrooms { get; set; }

    }

    public class ComparablesAndBedrooms
    {

        public List<int> Bedrooms { get; set; }

        public List<Comparables> Comparables { get; set; }

        public List<Database.Entity.ExcludedComparable> ExcludedComparables { get; set; }

        public List<Comparables> UnfilteredComparables { get; set; }
    }
}
