﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.MultiMedia
{
    public class LogInResult
    {
        public string AccessToken { get; set; }
        public string Message { get; set; }
        public List<string> NetworkIds { get; set; }
        public bool Success { get; set; }
        public string Region { get; set; }
        public string AccountId { get; set; }
        public string ClientId { get; set; }
        public bool HasSecurity { get; set; }
        public int localId { get; set; }
    }
}
