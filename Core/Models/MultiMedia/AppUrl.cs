﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.MultiMedia
{
    public class AppUrl
    {
        //public static string ArloVideoRoot
        //{
        //    get
        //    {
        //        return ConfigurationSettings.AppSettings["ArloVideoRoot"].ToString();
        //    }
        //}

        //public static string BlinkVideoRoot
        //{
        //    get
        //    {
        //        return ConfigurationSettings.AppSettings["BlinkVideoRoot"].ToString();
        //    }
        //}

        public static string VideoRoot
        {
            get
            {
                return ConfigurationSettings.AppSettings["VideoRoot"].ToString();
            }
        }

        public static string RespontageScraperUrl
        {
            get
            {
                return ConfigurationSettings.AppSettings["RespontageScraperUrl"].ToString();
            }
        }

        public static string LoggerPath
        {
            get
            {
                return ConfigurationSettings.AppSettings["LoggerPath"].ToString();
            }
        }
    }
}
