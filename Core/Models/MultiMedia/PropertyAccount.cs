﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.MultiMedia
{
    public class PropertyAccount
    {
        public long Id { get; set; }

        public long ListingId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Site { get; set; }
    }
}
