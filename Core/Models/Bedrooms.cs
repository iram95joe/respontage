﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Bedrooms
    {
        public int Bedroom { get; set; }

        public int Count { get; set; }
    }
}
