﻿using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class InboxListing
    {
        public int HostAccountId { get; set; }
        public string ThreadId { get; set; }
        public bool HasUnread { get; set; }
        public bool IsIncoming { get; set; }
        public string GuestName { get; set; }
        public string GuestProfilePictureUrl { get; set; }
        public long? InquiryId { get; set; }
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
        public int GuestId { get; set; }
        public string LastMessage { get; set; }
        public DateTime LastMessageAt { get; set; }
        public int SiteType { get; set; }
        public List<MessageEntry> Messages = new List<MessageEntry>();
    }
    public class MessageEntry
    {
        public long Id { get; set; }
        public string MessageId { get; set; }
        public string ThreadId { get; set; }
        public string MessageContent { get; set; }
        public bool IsMyMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }

        public int Type { get; set; }
        public string Source { get; set; }
        public string RecordingUrl { get; set; }
        public string ResourceSid { get; set; }
        public int? Duration { get; set; }
    }

}
