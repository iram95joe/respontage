﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RolePageUrl
    {
        public string PageUrl { get; set; }
        public string PageName { get; set; }
        public string IconPath { get; set; }
    }
}
