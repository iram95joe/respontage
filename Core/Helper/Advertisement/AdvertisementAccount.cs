﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper.Advertisement
{
    public class AdvertisementAccount
    {
        public static Core.Database.Entity.AdvertisementAccount GetAccount(string username, string password, int site)
        {
            using (var db = new ApplicationDbContext())
            {
                password = Security.Encrypt(password);
                var account = db.AdvertisementAccounts.Where(x => x.Username == username && x.Password == password && x.AdvertisementSite == site).FirstOrDefault();
                if (account != null)
                {
                    account.Password = Security.Decrypt(account.Password);
                    return account;
                }
                else { return null; }
            }
        }
        public static bool SaveAccount(Core.Database.Entity.AdvertisementAccount account)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.AdvertisementAccounts.Where(x => x.Username == account.Username && x.AccountSiteId == account.AccountSiteId && x.AdvertisementSite == account.AdvertisementSite).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.Password = Security.Encrypt(account.Password);
                        temp.Firstname = account.Firstname;
                        temp.Lastname = account.Lastname;
                        temp.AccessToken = account.AccessToken;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        account.Password = Security.Encrypt(account.Password);
                        db.AdvertisementAccounts.Add(account);
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception e) { return false; }
        }

        public static string GetLamudiAccountToken()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.AdvertisementAccounts.Where(x => x.AdvertisementSite == (int)Core.Enumerations.AdvertismentSite.Lamudi && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault().AccessToken;
            }
        }
        public static Core.Database.Entity.AdvertisementAccount GetAccountByCompany(int siteTpe)
        {
            using(var db = new ApplicationDbContext())
            {
                var account= db.AdvertisementAccounts.Where(x => x.CompanyId == SessionHandler.CompanyId && x.AdvertisementSite == siteTpe).FirstOrDefault();
                account.Password = Security.Decrypt(account.Password);
                return account;
                
            }
        }
    }
}
