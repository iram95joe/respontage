﻿using Core.Database.Context;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper.User
{
    public class Pages
    {
        public static List<RolePageUrl> GetRolePageList(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                var companyId = user.CompanyId;
                var pageIds = (from company in db.Companies
                               join companyTypeCompany in db.CompanyTypeCompanies on company.Id equals companyTypeCompany.CompanyId
                               join companyType in db.CompanyTypes on companyTypeCompany.CompanyTypeId equals companyType.Id
                               join companyTypePage in db.CompanyTypePages on companyType.Id equals companyTypePage.CompanyTypeId
                               where company.Id == companyId
                               select companyTypePage.ApplicationPageId).ToList();

                var ilst = (from c in db.tbRoleLink
                            join d in db.RoleApplicationPages on c.RoleId equals d.RoleId
                            join page in db.ApplicationPages on d.ApplicationPageId equals page.Id
                            where c.UserId == userId && (companyId == 0 ? true : pageIds.Contains(page.Id))
                            select new
                            {
                                PageUrl = page.PageUrl,
                                PageName = page.Name,
                                IconPath = page.Icon,
                                PageOrder = page.PageOrder
                            }).Distinct().OrderBy(x => x.PageOrder).ToList();
                var Result = (from d in ilst
                             select new RolePageUrl
                             {
                                 PageName = d.PageName,
                                 PageUrl = d.PageUrl,
                                 IconPath = d.IconPath
                             }).ToList();
                if (userId == 1)
                {
                    Result.Add(new RolePageUrl
                    {
                        PageName = "Admin",
                        PageUrl = "/Page",
                        IconPath = "icon users"
                    });
                }

                return Result.ToList();
            }
        }
    }
}
