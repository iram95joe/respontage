﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper.User
{
    public class Users
    {
        public static bool IsOwner(int ownerId)
        {
            using (var db = new ApplicationDbContext())
            {
                var role = db.tbRoleLink.Where(x => x.UserId == ownerId && x.RoleId == 5).FirstOrDefault();
                if (role != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool IsWorker(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var role = db.tbRoleLink.Where(x => x.UserId ==userId && x.RoleId == 4).FirstOrDefault();
                if (role != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static Core.Database.Entity.User GetCurrentUserDetail(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.Where(x => x.Id == userId).FirstOrDefault();
            }
        }

    }
}
