﻿using Core.Database.Context;
using Core.Database.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper.PaymentMethod
{
    public class ConfigurationKeys
    {
        public static PaymentAccount PaymentAccount(int id)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var keys = db.PaymentAccount
                            .Where(x => x.Id==id)
                            .First();

                    return keys;
                }
            }
            catch (Exception e)
            {
                return new PaymentAccount();
            }
        }
        public static PaymentAccount ConfigurationKeyMethod(int method,int companyId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var keys =db.PaymentAccount
                            .Where(x => x.CompanyId == companyId && x.PaymentMethod == method)
                            .First();

                    return keys;
                }
            }
            catch (Exception e)
            {
                return new PaymentAccount();
            }
        }

        //this configuration use when renter click our payment request link
        public static PaymentAccount ConfigurationKeyByInvoice(int invoiceId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var data = (from invoice in db.Invoices join reservation in db.Inquiries on invoice.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId where invoice.Id == invoiceId select new { CompanyId = property.CompanyId, PaymentMethod = invoice.PaymentMethod }).FirstOrDefault();
                    var keys = db.PaymentAccount
                            .Where(x => x.CompanyId == data.CompanyId && x.PaymentMethod == data.PaymentMethod)
                            .First();

                    return keys;
                }
            }
            catch (Exception e)
            {
                return new PaymentAccount();
            }
        }

    }
}
