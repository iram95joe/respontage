﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper
{
    public struct DateTimeZone

    {
        public static DateTime ConvertToUTC(DateTime dateTime, long listingId)
        {
            try
            {
                DateTime date = new DateTime();
                using (var db = new ApplicationDbContext())
                {
                    if (dateTime.Year == 1)
                    {

                    }

                    var dateTimeUnspec = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
                    date = TimeZoneInfo.ConvertTimeToUtc(dateTimeUnspec, TimeZoneInfo.FindSystemTimeZoneById(db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault().TimeZoneName));
                    return date;
                }
            }
            catch
            {

            }
            return dateTime;

        }
    }
}
