﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper
{
    public class HtmlLogs
    {
        public static void Add(string Html)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.HtmlLogs.Add(new Database.Entity.HtmlLog { Content = Html, DateTime = DateTime.Now });
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
