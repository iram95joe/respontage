﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper.Company
{
    public class CompanyType
    {
        public static bool IsLongTerm()
        {
            using (var db = new ApplicationDbContext())
            {
                var type = db.CompanyTypeCompanies.Where(x => x.CompanyId == SessionHandler.CompanyId && x.CompanyTypeId ==2).FirstOrDefault();

                if (type != null)
                    return false;
                else
                    return
                        true    ;
            }
        }
    }
}
