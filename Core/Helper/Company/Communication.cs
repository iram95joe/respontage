﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helper.Company
{
    public class Communication
    {
        public static void ChargeUsage(int communicationId,bool isSMS)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    if (db.Companies.Where(x => x.Id == SessionHandler.CompanyId).First().IsChargeCommunication)
                    {
                        var today = DateTime.Now.Date;
                        var amount = db.CommunicationChargeRates.Where(x => x.EffectiveDate <= today && x.Type == (isSMS ? (int)Core.Enumerations.CommunicationType.SMS : (int)Core.Enumerations.CommunicationType.Call)).OrderByDescending(x => x.EffectiveDate).Select(x => x.Amount).FirstOrDefault();
                        var duration = db.CommunicationCalls.Where(x => x.Id == communicationId).Select(x => x.CallDuration).FirstOrDefault();
                        db.CommunicationCharges.Add(new CommunicationCharge()
                        {
                            Amount = isSMS ? amount : amount * duration.Value,
                            CommunicationCallId = isSMS ? 0 : communicationId,
                            CommunicationSMSId = isSMS ? communicationId : 0,
                            CompanyId = SessionHandler.CompanyId
                        });
                        db.SaveChanges();
                    }
                }
            }
            catch { }
        }
    }
}
