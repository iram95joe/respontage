﻿using Core.Database.Context;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Core.Helper
{
    public class Utilities
    {
        public static string GetReservationStatusCode(string status)
        {
            if (status == "INQUIRY" || status == "REPLIED" || status == "QUOTE_SENT" || status == "RESERVATION_REQUEST_EXPIRED") return "I";
            else if (status == "PAYMENT_REQUEST_SENT") return "PAYMENT_REQUEST_SENT";
            else if (status == "RESERVATION_REQUEST_DECLINED") return "IC";
            else if (status == "POST_STAY" || status == "BOOKING" || status == "STAYING") return "A";
            else if (status == "RESERVATION_REQUEST") return "B";
            else if (status == "CANCELLED") return "BC";
            else return status;
        }
        public static string GetDefaultAvatar(string name)
        {
            var initial = string.IsNullOrEmpty(name) ? "" : name[0].ToString().ToUpper();
            return string.IsNullOrEmpty(initial) ? string.Empty : ("/Content/img/eve.png");
        }
        public static TimeSpan ExtractCheckInTime(string input)
        {
            int time = 0;
            string meridiem = "";
            int.TryParse(Regex.Match(input, @"\d+").Value, out time);
            meridiem = input.ToUpper().Contains("AM") ? "AM" : input.ToUpper().Contains("PM") ? "PM" : "";
            if (time != 0 && (meridiem == "AM" || meridiem == "PM"))
            {
                return new TimeSpan(meridiem == "AM" ? time : time + 12, 0, 0);
            }
            else
            {
                return new TimeSpan(16, 0, 0);
            }
        }
        public static TimeSpan ExtractCheckOutTime(string input)
        {
            int time = 0;
            string meridiem = "";
            int.TryParse(Regex.Match(input, @"\d+").Value, out time);
            meridiem = input.ToUpper().Contains("AM") ? "AM" : input.ToUpper().Contains("PM") ? "PM" : "";
            if (time != 0 && (meridiem == "AM" || meridiem == "PM"))
            {
                return new TimeSpan(meridiem == "AM" ? time : time + 12, 0, 0);
            }
            else
            {
                return new TimeSpan(11, 0, 0);
            }
        }

        public static decimal ExtractDecimal(string input)
        {
            decimal result = 0;
            decimal.TryParse(Regex.Match(input, @"\d+").Value, out result);
            return result;
        }

        public static int ExtractNumber(string original)
        {
            int num = 0;
            int.TryParse(new string(original.Where(c => Char.IsDigit(c)).ToArray()), out num);
            return num;
        }

        public static string GetPropertyText(int value)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Properties.Find(value) != null ? db.Properties.Find(value).Name : "";
            }
        }

        public static int GetPaidStatus(string text)
        {
            switch (text)
            {
                case "new": return 0;
                case "paid": return 1;
                default: return 0;
            }
        }

        public static string SplitSafely(string source, string splitCharacter, int requiredNumberofElementZeroBase = -1, bool getLast = false)
        {
            string output = string.Empty;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    string special = "!";

                    source = source.Replace(splitCharacter, special);
                    string[] arr = source.Split(special.ToCharArray());
                    if (arr != null && arr.Length > requiredNumberofElementZeroBase)
                    {
                        if (requiredNumberofElementZeroBase > -1)
                        {
                            output = arr[requiredNumberofElementZeroBase].ToSafeString().Trim();
                        }
                        else if (getLast)
                        {
                            output = arr[arr.Length - 1].ToSafeString().Trim();
                        }

                    }
                }
                catch (Exception)
                {

                }
            }
            return output;
        }
    }
}
