﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Helper
{
    public static class ElmahLogger
    {
        /// <summary>
        /// Log error to Elmah
        /// </summary>
        public static void LogError(Exception ex, string contextualMessage = null)
        {
            try
            {
                // log error to Elmah
                if (contextualMessage != null)
                {
                    // log exception with contextual information that's visible when 
                    // clicking on the error in the Elmah log
                    var annotatedException = new Exception(contextualMessage, ex);
                    ErrorSignal.FromCurrentContext().Raise(annotatedException, HttpContext.Current);
                }
                else
                {
                    ErrorSignal.FromCurrentContext().Raise(ex, HttpContext.Current);
                }

                // send errors to ErrorWS (my own legacy service)
                // using (ErrorWSSoapClient client = new ErrorWSSoapClient())
                // {
                //    client.LogErrors(...);
                // }
            }
            catch (Exception)
            {
                // uh oh! just keep going
            }
        }
        public static void LogInfo(string message)
        {
            try
            {

                // log exception with contextual information that's visible when 
                // clicking on the error in the Elmah log
                var context = HttpContext.Current;
                var annotatedException = new Exception(message);
                annotatedException.Data.Add("test", "data");
                ErrorSignal.FromCurrentContext().Raise(annotatedException, context);



            }
            catch (Exception e)
            {
                // uh oh! just keep going
            }
        }

        public static void LogHTTPWebrequestError(string url, string html, string statusCode, string token, string methodName, string title, string email, string request)
        {
            try
            {
                var messageTitle = "Invalid response from the source website | " + title;
                Helper.HttpWebRequestError error = new HttpWebRequestError
                {
                    HTML = html,
                    MethodName = methodName,
                    StatusCode = statusCode,
                    Token = token,
                    URL = url
                };


                var context = HttpContext.Current;
                context.Request.ServerVariables["ERROR_HTML"] = html;
                context.Request.ServerVariables["ERROR_URL"] = url;
                context.Request.ServerVariables["ERROR_STATUS_CODE"] = statusCode;
                context.Request.ServerVariables["ERROR_TOKEN"] = token;
                context.Request.ServerVariables["ERROR_Email"] = email;
                context.Request.ServerVariables["ERROR_METHOD_NAME"] = methodName;
                context.Request.ServerVariables["ERROR_WHOLE_JSON"] = error.ToJSON();
                context.Request.ServerVariables["ERROR_REQUEST"] = request;

                var annotatedException = new Exception(messageTitle);
                ErrorSignal.FromCurrentContext().Raise(annotatedException, context);


                // send errors to ErrorWS (my own legacy service)
                // using (ErrorWSSoapClient client = new ErrorWSSoapClient())
                // {
                //    client.LogErrors(...);
                // }
            }
            catch (Exception)
            {
                // uh oh! just keep going
            }
        }
        public static void LogXpathError(string message, string url, string html, string statusCode, string token, string xpathSelector, string methodName, string email)
        {
            try
            {
                var messageTitle = string.IsNullOrEmpty(message) ? "Error while scrapping a node" : message;
                Helper.XpathError error = new XpathError
                {
                    HTML = html,
                    MethodName = methodName,
                    XpathSelector = xpathSelector,
                    Token = token,
                    URL = url
                };

                var context = HttpContext.Current;
                context.Request.ServerVariables["ERROR_HTML"] = html;
                context.Request.ServerVariables["ERROR_URL"] = url;
                context.Request.ServerVariables["ERROR_XPATH_SELECTOR"] = xpathSelector;
                context.Request.ServerVariables["ERROR_TOKEN"] = token;
                context.Request.ServerVariables["ERROR_EMAIL"] = email;
                context.Request.ServerVariables["ERROR_METHOD_NAME"] = methodName;
                context.Request.ServerVariables["ERROR_WHOLE_JSON"] = error.ToJSON();

                var annotatedException = new Exception(messageTitle);
                ErrorSignal.FromCurrentContext().Raise(annotatedException, context);


                // send errors to ErrorWS (my own legacy service)
                // using (ErrorWSSoapClient client = new ErrorWSSoapClient())
                // {
                //    client.LogErrors(...);
                // }
            }
            catch (Exception)
            {
                // uh oh! just keep going
            }
        }
    }
}