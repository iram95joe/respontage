﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Database.Entity;
namespace Core.Helpers.Devices
{
    public class Devices
    {
        public static int AddDevice(Device device)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Devices.Where(x => x.DeviceId == device.DeviceId).FirstOrDefault();
                if (temp == null)
                {
                    db.Devices.Add(device);
                    db.SaveChanges();
                    return device.Id;
                }
                else
                {
                    return temp.Id;
                }
            }
        }
        public static DeviceAccount GetAccount(string username,int siteType)
        {
            using(var db= new ApplicationDbContext())
            {
                return db.DeviceAccounts.Where(x => x.Username == username && x.SiteType == siteType).FirstOrDefault();

            }
        }

        public static int AddAccount(DeviceAccount account)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.DeviceAccounts.Where(x => x.Username == account.Username && x.SiteType == account.SiteType).FirstOrDefault();
                if (temp == null)
                {
                    db.DeviceAccounts.Add(account);
                    db.SaveChanges();
                    return account.Id;
                }
                else
                {
                    temp.AccountId = account.AccountId;
                    temp.ClientId = account.ClientId;
                    temp.Region = account.Region;
                    temp.Token = account.Token;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return temp.Id;
                }
            }
        }
        public static void AddLock(Lock @lock)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Locks.Where(x => x.LockId == @lock.LockId && x.LocalDeviceId == @lock.LocalDeviceId).FirstOrDefault();
                if (temp == null)
                {
                    db.Locks.Add(@lock);
                    db.SaveChanges();
                }
                else
                {
                    temp.IsLock = @lock.IsLock;
                    temp.Name = @lock.Name;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
    }
}
