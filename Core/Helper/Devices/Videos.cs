﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helpers.Devices
{
    public class Videos
    {
        public static void Add(Video video)
        {
            using(var db = new ApplicationDbContext())
            {
                var temp = db.Videos.Where(x => x.OriginalVideoUrl == video.OriginalVideoUrl).FirstOrDefault();
                if (temp == null)
                {
                    db.Videos.Add(video);
                    db.SaveChanges();
                }
            }
        }

        public static void Delete(int accountId,DateTime date)
        {
            using (var db = new ApplicationDbContext())
            {
                var dt = date.Date;
                var temp = (from a in db.DeviceAccounts join d in db.Devices on a.Id equals d.AccountId join v in db.Videos on d.Id equals v.LocalDeviceId where a.Id == accountId && DbFunctions.TruncateTime(v.DateCreated) <dt && v.IsSaved==false select v).ToList();
                foreach(var video in temp)
                {
                    try
                    {
                        System.IO.File.Delete(video.ServerPath);
                        //var image = video.ServerPath.Replace(".mp4", ".jpg");
                        //System.IO.File.Delete(image);
                    }
                    catch (Exception e)
                    {
                    }
                }
                db.Videos.RemoveRange(temp);
                db.SaveChanges();
            }
        }
    }
}
