﻿using Core.Models;
using Core.Models.MultiMedia;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Helpers.Devices
{
    public class Logger
    {
        #region WriteLog

        public static void WriteLog(string logs)
        {
            try
            {
                string readText = "";

                try
                {
                    readText = File.ReadAllText(AppUrl.LoggerPath);
                }
                catch (Exception e)
                {

                }

                string createText = 
                    "Message : " + logs + Environment.NewLine +
                    "Data Created : " + DateTime.Now.ToString("MMMM dd, yyyy - hh:mm tt") + Environment.NewLine + 
                    "==============================================================================" + Environment.NewLine + 
                    Environment.NewLine + Environment.NewLine + 
                    readText;

                File.WriteAllText(AppUrl.LoggerPath, createText);
            }
            catch (Exception e)
            {

            }
        }

        #endregion
    }
}
