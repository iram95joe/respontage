﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.Helper
{
    public class SessionHandler
    {
        public static string SmartthingsEmail
        {
            get
            {
                if (HttpContext.Current.Session["SmartthingsEmail"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["SmartthingsEmail"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["SmartthingsEmail"] = value;
            }
        }
        public static int CompanyId
        {
            //using(var db = new ApplicationDbContext()){}

            //HttpContext.Current.User.Identity.Name
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    var userId = HttpContext.Current.User.Identity.Name.ToInt();
                    var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                    if (user != null)
                    {
                        return user.CompanyId;
                    }
                    else { return 0; }
                }
            }
            //set
            //{
            //    HttpContext.Current.Session["CompanyId"] = value;
            //}
        }

        public static bool IsActive()
        {
            return HttpContext.Current.Session["CompanyId"] != null;
        }
        public static bool IsSessionExpired { get; set; }
    }
}
