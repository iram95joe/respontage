﻿using Core.Models;

using System.Collections.Generic;
using System.Configuration;
using System.Web;

namespace Core.Helper
{
    public class GlobalVariables
    {
        #region Subscription setting
        public static string SquareApplicationId
        {
            get
            {
                return "sandbox-sq0idb-ius5x6Goxkc7w6xe5TGNHA";
            }
        }
        public static string SquareLocationId
        {
            get
            {
                return "LAFFXFSV5ZVGT";
            }
        }
        public static string SquareAccessToken
        {
            get { return "EAAAELRV2rV0W0RtObWPCw8ZJJ5HU7th-C09s9pbO3oeXcOeMe73dzNn9iEx_-3p"; }
        }
        #endregion
        public static string GoogleApiKey()
        {
            return ConfigurationSettings.AppSettings["GoogleApiKey"].ToString();
        }


        public static string ApiBaseUrl(int SiteType =1)
        {//JM All commented code in this method is for split scrapper
            return ConfigurationSettings.AppSettings["AirbnbScrapperUrl"].ToString();
            //switch (SiteType) {
            //    case 1:
            //    return ConfigurationSettings.AppSettings["AirbnbScrapperUrl"].ToString();
            //        break;
            //    case 2:
            //        return ConfigurationSettings.AppSettings["VrboScrapperUrl"].ToString();
            //        break;
            //    case 3:
            //        return ConfigurationSettings.AppSettings["BookingComScrapperUrl"].ToString();
            //        break;

            //    case 4:
            //        return ConfigurationSettings.AppSettings["HomeawayScrapperUrl"].ToString();
            //        break;
            //    default:
            //        return "";
            //}
        }
        //Smatthings details
        public static string SmartthingsClientId()
        {
            return ConfigurationSettings.AppSettings["SmartthingsClientId"].ToString();
        }
        public static string SmartthingsClientSecret()
        {
            return ConfigurationSettings.AppSettings["SmartthingsClientSecret"].ToString();
        }
        public static string SmartthingsRedirectUrl()
        {
            return ConfigurationSettings.AppSettings["SmartthingsRedirectUrl"].ToString();
        }

        //Video url
        public static string ApiVideoUrl
        {
            get
            {
                return ConfigurationSettings.AppSettings["ApiVideo"].ToString();
            }
        }

        public static string ApiAnalyticsUrl
        {
            get
            {
                return ConfigurationSettings.AppSettings["ApiAnalytics"].ToString();
            }
        }

        //public static int UserWorkerId = System.Web.HttpContext.Current.Session["UserWorkerId"].ToInt();
        public static int UserWorkerId
        {
            get
            {
                if (HttpContext.Current.Session["UserWorkerId"] == null)
                {
                    return 0;
                }
                else
                {
                    return HttpContext.Current.Session["UserWorkerId"].ToInt();
                }
            }
            set
            {
                HttpContext.Current.Session["UserWorkerId"] = value;
            }
        }
        public static int UserId
        {
            get
            {
                if (HttpContext.Current.Session["UserId"] == null)
                {
                    return 0;
                }
                else
                {
                    return HttpContext.Current.Session["UserId"].ToInt();
                }
            }
            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }

        public static int CompanyId { get; set; }

        public static bool StillImporting { get; set; }

        public static string ImportedHostProfile { get; set; }

        public static int ImportedPropertyCount { get; set; }

        public static int ImportedMessagesCount { get; set; }

        public static int ImportedBookingCount { get; set; }

        public static ImportStage ImportStage { get; set; }

        public static Dictionary<string, HostAccount> HostAccounts = new Dictionary<string, HostAccount>();

        public static string Username { get; set; }

        public static string Password { get; set; }

        public static bool AirlockIsTriggered { get; set; }

        #region ClearImportCounters

        public static void ClearImportCounters()
        {
            StillImporting = false;
            ImportedHostProfile = "";
            ImportedPropertyCount = 0;
            ImportedMessagesCount = 0;
            ImportedBookingCount = 0;
        }

        #endregion
    }

    public enum ImportStage
    {
        HostProfile,
        Property,
        Messages,
        Inquiry
    }
}