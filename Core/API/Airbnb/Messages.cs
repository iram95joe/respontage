﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Airbnb
{
    public class Messages
    {
        public static Message MessageForMessageId(ApplicationDbContext db, string messageId)
        {
            var entity = db.Messages.AsNoTracking().Where(x => x.MessageId == messageId).FirstOrDefault();
            return entity;
        }
        public static List<Message> MessagesForThreadId(ApplicationDbContext db, string threadId)
        {
            var messages = db.Messages.AsNoTracking().Where(x => x.ThreadId == threadId).ToList();
            return messages;
        }
        public static bool AddIfNotExist(ApplicationDbContext db, string threadId, List<Message> messageList)
        {
            try
            {
                var messagesForThread = MessagesForThreadId(db, threadId);
                foreach (var item in messageList.ToList())
                {
                    var entity = messagesForThread.Where(x => x.MessageId == item.MessageId).FirstOrDefault();
                    if (entity != null)
                    {
                        messageList.Remove(item);
                    }
                }
                //db.Messages.AddRange(messageList);
                db.BulkInsert(messageList);
                return true;
            }
            catch { return false; }
        }
    }
}
