﻿//using Core.Database.Context;
//using Core.Database.Entity;
//using Core.Helper;
//using Core.Repository;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Diagnostics;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Core.API.Airbnb
//{
//    public class Inquiries
//    {
//        static ICommonRepository commonRepo = new CommonRepository();

//        public static bool Add(Inquiry model)
//        {
//            using (var db = new ApplicationDbContext())
//            {
//                db.Inquiries.Add(model);
//                if (db.SaveChanges() > 0) return true;
//            }
//            return false;
//        }

//        public static bool AddOrUpdate(Inquiry inquiry, bool clearThisBooking)// bool clearAllActiveBooking)
//        {
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    int incomeTypeId = commonRepo.GetAirbnbIncomeTypeId();
//                    int airbnbFeeId = commonRepo.GetAirbnbFeeId();
//                    //var propertyList = db.Properties.Where(x => x.CompanyId == GlobalVariables.CompanyId).ToList();
//                    //var guestList = db.Guests.ToList();

//                    // set all active reservation status to false
//                    if (clearThisBooking)
//                    {
//                        try
//                        {
//                            var hostId = inquiry.HostId;

//                            if (inquiry.IsActive)
//                            {
//                                inquiry.IsActive = false;
//                                db.Entry(inquiry).State = EntityState.Modified;
//                            }

//                            //var activeBookings = db.Inquiries.Where(x => x.IsActive == true && x.HostId == hostId && x.CompanyId == GlobalVariables.CompanyId).ToList();
//                            //var activeBookings = db.Inquiries.Where(x => x.IsActive && x.CheckOutDate.Value.Date > DateTime.UtcNow.Date).ToList();
//                            //if (activeBookings.Count > 0)
//                            //{
//                            //    foreach (var item in activeBookings)
//                            //    {
//                            //        item.IsActive = false;
//                            //        db.Entry(item).State = EntityState.Modified;
//                            //    }
//                            //    //db.BulkSaveChanges();
//                            //}
//                        }
//                        catch (Exception e)
//                        {
//                            Trace.WriteLine(e.ToString());
//                        }
//                    }
//                    // end

//                    //if (propertyList.Count == 0 || guestList.Count == 0) return false;

//                    var guestName = "";
//                    var tempGuest = db.Guests.Where(x => x.GuestId == inquiry.GuestId.ToString()).FirstOrDefault();
//                    if (tempGuest != null) guestName = tempGuest.Name;

//                    var existingInquiry = db.Inquiries.Where(x => x.ReservationId == inquiry.ReservationId).FirstOrDefault();

//                    if (existingInquiry != null) // if inquiry has already been saved to db
//                    {
//                        existingInquiry.BookingStatusCode = inquiry.BookingStatusCode;
//                        existingInquiry.Nights = inquiry.Nights;
//                        existingInquiry.ReservationCost = inquiry.ReservationCost;
//                        existingInquiry.SumPerNightAmount = inquiry.SumPerNightAmount;
//                        existingInquiry.CleaningFee = inquiry.CleaningFee;
//                        existingInquiry.ServiceFee = inquiry.ServiceFee;
//                        existingInquiry.TotalPayout = inquiry.TotalPayout;
//                        existingInquiry.PayoutDate = inquiry.PayoutDate;
//                        existingInquiry.NetRevenueAmount = inquiry.NetRevenueAmount;
//                        existingInquiry.InquiryDate = inquiry.InquiryDate;
//                        existingInquiry.BookingDate = inquiry.BookingDate;
//                        existingInquiry.BookingCancelDate = inquiry.BookingCancelDate;
//                        existingInquiry.BookingConfirmDate = inquiry.BookingConfirmDate;
//                        existingInquiry.BookingConfirmCancelDate = inquiry.BookingConfirmCancelDate;
//                        existingInquiry.GuestId = tempGuest.Id;
//                        existingInquiry.GuestCount = inquiry.GuestCount;
//                        existingInquiry.IsActive = inquiry.IsActive;
//                        existingInquiry.IsAltered = inquiry.IsAltered;
//                        db.Entry(existingInquiry).State = EntityState.Modified;
//                        db.BulkSaveChanges();

//                        if (existingInquiry.BookingStatusCode == "A" && existingInquiry.IsActive)
//                        {
//                            //commonRepo.CreateFormHashIfNotExist(existingInquiry.InquiryId.ToInt(), item.CheckOutDate.Value.AddDays(2));
//                        }

//                        if (existingInquiry.BookingStatusCode == "A" && (from prop in db.Properties where prop.ListingId == existingInquiry.PropertyId select prop.AutoCreateIncomeExpense).FirstOrDefault()) // accepted booking only
//                        {
//                            var existingIncome = db.Income.Where(x => x.BookingId == existingInquiry.Id).FirstOrDefault();
//                            if (existingIncome != null)
//                            {
//                                existingIncome.Status = 1;
//                                existingIncome.PropertyId = existingInquiry.PropertyId;
//                                db.Entry(existingIncome).State = EntityState.Modified;
//                            }
//                            else
//                            {
//                                Income income = new Income
//                                {
//                                    PropertyId = inquiry.PropertyId,
//                                    IncomeTypeId = incomeTypeId,
//                                    BookingId = existingInquiry.Id,
//                                    Status = 1,
//                                    Amount = inquiry.TotalPayout,
//                                    DateRecorded = inquiry.CheckInDate.HasValue ? inquiry.CheckInDate.Value.AddDays(1) : inquiry.BookingDate.ToDateTime(),
//                                    IsBatchLoad = false,
//                                    CompanyId = GlobalVariables.CompanyId
//                                };
//                                commonRepo.AddIncome(income);
//                            }

//                            var serviceFeeExpense = db.Expenses
//                            .Where(x => x.BookingId == existingInquiry.Id)
//                            .Where(x => x.FeeTypeId == airbnbFeeId)
//                            .FirstOrDefault();

//                            if (serviceFeeExpense != null)
//                            {
//                                serviceFeeExpense.BookingId = existingInquiry.Id;
//                                serviceFeeExpense.PropertyId = existingInquiry.PropertyId;
//                                serviceFeeExpense.Description = guestName;
//                                serviceFeeExpense.Amount = existingInquiry.ServiceFee;
//                                db.Entry(serviceFeeExpense).State = EntityState.Modified;
//                            }
//                            else
//                            {
//                                commonRepo.AddExpense(existingInquiry.Id, inquiry.PropertyId, guestName, inquiry.ServiceFee, airbnbFeeId, true, false, "", inquiry.BookingDate, inquiry.CheckInDate);
//                            }
//                        }
//                    }
//                    else // add inquiry
//                    {
//                        //inquiry.CompanyId = GlobalVariables.CompanyId;
//                        //inquiry.PropertyId = Core.API.Airbnb.Properties.GetLocalId(inquiry.PropertyId);
//                        inquiry.GuestId = Core.API.Airbnb.Guests.GetLocalId(inquiry.GuestId.ToString());
//                        db.Inquiries.Add(inquiry);
//                        db.BulkSaveChanges();

//                        if (inquiry.BookingStatusCode == "A" && inquiry.IsActive)
//                        {
//                            //commonRepo.CreateFormHashIfNotExist(item.InquiryId.ToInt(), item.CheckOutDate.Value.AddDays(2));
//                        }

//                        if (inquiry.BookingStatusCode == "A" && db.Properties.Find(inquiry.PropertyId).AutoCreateIncomeExpense)
//                        {
//                            db.Income.RemoveRange(db.Income.Where(x => x.BookingId == inquiry.Id && x.CompanyId == SessionHandler.CompanyId));
//                            db.Expenses.RemoveRange(db.Expenses.Where(x => x.BookingId == inquiry.Id && x.FeeTypeId == airbnbFeeId && x.CompanyId == SessionHandler.CompanyId));

//                            Income income = new Income
//                            {
//                                PropertyId = inquiry.PropertyId,
//                                IncomeTypeId = incomeTypeId,
//                                BookingId = inquiry.Id,
//                                CompanyId = GlobalVariables.CompanyId,
//                                Amount = inquiry.TotalPayout,
//                                Status = 1,
//                                DateRecorded = inquiry.CheckInDate.HasValue ? inquiry.CheckInDate.Value.AddDays(1) : inquiry.BookingDate.ToDateTime()
//                            };
//                            commonRepo.AddIncome(income);
//                            commonRepo.AddExpense(inquiry.Id, inquiry.PropertyId, guestName, inquiry.ServiceFee, airbnbFeeId, true, false, "", inquiry.BookingDate, inquiry.CheckInDate);
//                        }
//                    }

//                    return true;
//                }
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//                return false;
//            }
//        }
//    }
//}
