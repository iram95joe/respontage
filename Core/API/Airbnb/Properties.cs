﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using NodaTime.TimeZones;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Airbnb
{
    public class Properties
    {
        public static bool AddOrUpdate(Property property)
        {
            using (var db = new ApplicationDbContext())
            {
                var test = db.PropertyType.Where(x => x.Property_Type.Contains(property.PropertyType)).FirstOrDefault();
                if (test == null)
                {
                    db.PropertyType.Add(new PropertyType { Property_Type = property.PropertyType });
                    db.SaveChanges();
                }

                property.Type = db.PropertyType.Where(x => x.Property_Type == property.PropertyType).FirstOrDefault().Id;

                var p = db.Properties.Where(x => x.ListingId == property.ListingId).FirstOrDefault();
                if (p == null)
                {
                    //property.CompanyId = GlobalVariables.CompanyId;
                    property.AutoCreateIncomeExpense = true;
                    property.CommissionId = -1;
                    property.CommissionAmount = 0;
                    if (!string.IsNullOrEmpty(property.Name))
                    {
                        db.Properties.Add(property);
                    }
                }
                else
                {
                    p.ListingId = property.ListingId;
                    p.ListingUrl = property.ListingUrl;
                    p.Name = property.Name;
                    p.Longitude = property.Longitude;
                    p.Latitude = property.Latitude;
                    p.MinStay = property.MinStay;
                    p.Internet = property.Internet;
                    p.Pets = property.Pets;
                    p.WheelChair = property.WheelChair;
                    p.Description = property.Description;
                    p.Reviews = property.Reviews;
                    p.Sleeps = property.Sleeps;
                    p.Bedrooms = property.Bedrooms;
                    p.Bathrooms = property.Bathrooms;
                    p.Type = property.Type;
                    p.PropertyType = property.PropertyType;
                    p.AccomodationType = property.AccomodationType;
                    p.Smoking = property.Smoking;
                    p.AirCondition = property.AirCondition;
                    p.SwimmingPool = property.SwimmingPool;
                    p.PriceNightlyMin = property.PriceNightlyMin;
                    p.PriceNightlyMax = property.PriceNightlyMax;
                    p.PriceWeeklyMin = property.PriceWeeklyMin;
                    p.PriceWeeklyMax = property.PriceWeeklyMax;
                    p.Images = property.Images;
                    p.Status = property.Status;
                    db.Entry(p).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public static int GetLocalId(long airbnbPropertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Where(x => x.ListingId == airbnbPropertyId).FirstOrDefault();
                if (temp != null) { return temp.Id; }
            }
            return 0;
        }
        public static long GetAirbnbId(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Where(x => x.Id == id).FirstOrDefault();
                if (temp != null) { return temp.ListingId; }
            }
            return 0;
        }

        public static string TimeZome(string name)
        {
            var zoneIds = TzdbDateTimeZoneSource.Default.ZoneLocations
              .Where(x =>/*x.Longitude == -123.108605273227 && x.Latitude == 49.2786916097361 */x.ZoneId == name).FirstOrDefault();

            var source = TzdbDateTimeZoneSource.Default;
            var tzone = source.ZoneLocations
                .Where(x => x.CountryCode == zoneIds.CountryCode)
                .Select(tz => source.WindowsMapping.MapZones
                    .FirstOrDefault(x => x.TzdbIds.Contains(
                                         source.CanonicalIdMap.First(y => y.Value == tz.ZoneId).Key)))
                .Where(x => x != null && x.TzdbIds.Contains(name))
                .Select(x => x.WindowsId)
                .Distinct().FirstOrDefault();

            return tzone;
        }

        //public static string GetTimeZone(string timezone)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var property = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();

        //        return TimeZome(property.TimeZoneName);

        //    }

        //}
    }
}
