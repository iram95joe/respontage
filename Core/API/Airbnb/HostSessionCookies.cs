﻿using Core.Database.Context;
using Core.Database.Entity;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Airbnb
{
    public class HostSessionCookies
    {
        public static string Token
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from hsc in db.HostSessionCookies.ToList() where hsc.SiteType == 1 && hsc.IsActive select hsc).Last().Token;

                }
            }
        }

        public static AddSessionUserResult Add(int hostId, string serializedCookies)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = new AddSessionUserResult();
                HostSessionCookie h = new HostSessionCookie()
                {
                    IsActive = true,
                    Cookies = serializedCookies,
                    Token = PasswordGenerator.Generate(10),
                    CreatedAt = DateTime.Now,
                    SiteType = 1,
                    HostId = hostId,
                    //UsageType = 1
                };
                db.HostSessionCookies.Add(h);

                if (db.SaveChanges() > 0)
                {
                    result.IsSaved = true;
                    result.HostSessionCookie = h;
                }
                else
                {
                    result.IsSaved = false;
                }
                return result;
            }
        }

        public static HostSessionCookie UserSessionForId(string token)
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = db.HostSessionCookies.Where(f => f.Token == token).Where(x => x.IsActive).FirstOrDefault();

                if (entity != null)
                {
                    return entity;
                }

                return null;
            }
        }
        public static int GetHostId(string token)
        {
            //var temp = SiteConstants.hostAccounts.Where(x => x.SessionToken == token).FirstOrDefault();
            using (var db = new ApplicationDbContext())
            {
                var hostId = db.HostSessionCookies.Where(x => x.Token == token).Select(x => x.HostId).FirstOrDefault();
                return hostId;
            }
        }
    }

    public class AddSessionUserResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public HostSessionCookie HostSessionCookie { get; set; }
    }
}
