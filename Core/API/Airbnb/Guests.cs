﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Airbnb
{
    public class Guests
    {
        public static void SaveEmail(string GuestId,string Email)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp= db.GuestEmails.Where(x => x.GuestId == GuestId && x.Email == Email).FirstOrDefault();
                if (temp == null)
                {
                    GuestEmail guestEmail = new GuestEmail
                    {
                        GuestId = GuestId,
                        Email=Email

                    };
                    db.GuestEmails.Add(guestEmail);
                    db.SaveChanges();
                    
                }
            }
        }
     
        public static Guest GuestForId(ApplicationDbContext db, string guestId)
        {
            var entity = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return entity;
        }
        public static void AddRange(List<Guest> guestList)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    db.Configuration.AutoDetectChangesEnabled = false;
                    var guests = db.Guests.ToList();
                    foreach (var item in guestList.ToList())
                    {
                        if (guests.Where(x => x.GuestId == item.GuestId).FirstOrDefault() != null)
                        {
                            guestList.Remove(item);
                        }
                    }
                    db.Guests.AddRange(guestList);
                    db.SaveChanges();
                    db.Configuration.AutoDetectChangesEnabled = true;
                }
                catch { }
            }
        }
        public static int GetLocalId(string guestSiteId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var temp = db.Guests.Where(x => x.GuestId == guestSiteId).FirstOrDefault();
                    if (temp != null) { return temp.Id; }
                }
            }
            catch (Exception) { }
            return 0;
        }
    }
}
