﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Airbnb
{
    public class Hosts
    {
        public static List<Host> GetList
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    List<Host> hostWithActiveToken = new List<Host>();
                    var hosts = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId && h.SiteType==1 select h).ToList(); //= db.Hosts.Where(x => x.CompanyId == SessionHandler.CompanyId && x.SiteType == 1).ToList();

                    foreach (var host in hosts)
                    {
                        bool hasValidToken = db.HostSessionCookies.Where(x => x.HostId == host.Id && x.IsActive).Count() > 0;

                        if (hasValidToken)
                            hostWithActiveToken.Add(host);
                    }

                    return hostWithActiveToken;
                }
            }
        }

        #region Add

        public static bool Add(string hostId, string firstname, string lastname, string profilePicture, string username, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Hosts.Where(x => x.SiteHostId == hostId).FirstOrDefault();
                if (temp == null)
                {
                    Host h = new Host();
                    h.SiteHostId = hostId;
                    h.SiteType = (int)((object)Core.Enumerations.SiteType.Airbnb);
                    h.Firstname = firstname;
                    h.Lastname = lastname;
                    h.ProfilePictureUrl = profilePicture;
                    h.Username = username;
                    h.Password = Security.Encrypt(password);
                    h.AutoSync = true;
                    //h.CompanyId = SessionHandler.CompanyId;
                    db.Hosts.Add(h);
                }
                else
                {
                    temp.Firstname = firstname;
                    temp.Lastname = lastname;
                    temp.ProfilePictureUrl = profilePicture;
                    temp.Username = username;
                    temp.Password = Security.Encrypt(password);
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region GetHostId

        public static int GetHostId(string username, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                string pass = Security.Encrypt(password);

                try
                {
                    var asd = (from h in db.Hosts
                               where
                                    h.Username == username &&
                                    h.Password == pass &&
                                    h.SiteType == 1
                               select h.Id).First();

                    return asd;
                }
                catch (Exception e)
                {
                    return 0;
                }
            }
        }

        #endregion

        #region GetHostUsernameAndPassword

        public static Tuple<string, string> GetHostUsernameAndPassword(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = (from h in db.Hosts
                            where
                                 h.Id == hostId
                            select h).First();

                return new Tuple<string, string>(user.Username, Security.Decrypt(user.Password));
            }
        }

        #endregion

        #region GetToken

        public static string GetToken(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from hsc in db.HostSessionCookies.AsQueryable()
                        where
                             hsc.HostId == hostId &&
                             hsc.SiteType == 1
                        orderby hsc.Id descending
                        select hsc.Token).FirstOrDefault();
            }
        }

        #endregion
    }
}
