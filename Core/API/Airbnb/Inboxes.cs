﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Airbnb
{
    public class Inboxes
    {
        public static Inbox InboxForId(ApplicationDbContext db, string threadId)
        {
            var entity = db.Inbox.AsNoTracking().Where(x => x.ThreadId == threadId).FirstOrDefault();
            return entity;
        }
        public static bool AddOrUpdate(ApplicationDbContext db, Inbox model, bool newOnly)
        {
            try
            {
                var entity = InboxForId(db, model.ThreadId);
                if (entity == null)
                {
                    db.Inbox.Add(model);
                }
                else
                {
                    entity.LastMessageAt = model.LastMessageAt;
                    entity.LastMessage = model.LastMessage;
                    if (newOnly) { entity.HasUnread = model.HasUnread; }
                    entity.Status = model.Status;
                    entity.IsInquiryOnly = model.IsInquiryOnly;
                    entity.CheckInDate = model.CheckInDate;
                    entity.CheckOutDate = model.CheckOutDate;
                    entity.GuestCount = model.GuestCount;
                    entity.IsSpecialOfferSent = model.IsSpecialOfferSent;
                    entity.RentalFee = model.RentalFee;
                    entity.CleaningFee = model.CleaningFee;
                    entity.GuestFee = model.GuestFee;
                    entity.ServiceFee = model.ServiceFee;
                    entity.Subtotal = model.Subtotal;
                    entity.GuestPay = model.GuestPay;
                    entity.TotalPayout = model.TotalPayout;
                    entity.StatusType = model.StatusType;
                    entity.CanPreApproveInquiry = model.CanPreApproveInquiry;
                    entity.CanWithdrawPreApprovalInquiry = model.CanWithdrawPreApprovalInquiry;
                    entity.CanDeclineInquiry = model.CanDeclineInquiry;
                    db.Entry(entity).State = EntityState.Modified;
                }
                return true;
            }
            catch (Exception e) { Trace.WriteLine("Exception in scrape inbox " + e.ToString()); return false; }
        }
    }
}
