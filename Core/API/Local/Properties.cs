﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Local
{
    public class Properties
    {
        public static void AddChildToParent(ParentChildProperty p)
        {
            using(var db=new ApplicationDbContext())
            {
                db.ParentChildProperties.Add(p);
                db.SaveChanges();
            }

        }
    }
}
