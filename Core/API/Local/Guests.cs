﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Local
{
    public class Guests
    {
        public static bool SaveEmail(int guestId, string email)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var guest = db.Guests.Where(x => x.Id == guestId).FirstOrDefault();

                    if (string.IsNullOrEmpty(guest.Email))
                    {
                        guest.Email = email;
                        db.BulkSaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static string GetNameById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Guests.Where(g => g.Id == id).Select(g => g.Name).First();
            }
        }

        public static string GetEmailByLocalId(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Guests.Where(g => g.Id == id).Select(x => x.Email).FirstOrDefault();
            }
        }
        public static int GetLocalGuestId(ApplicationDbContext db, string guestId)
        {
            var result = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return result != null ? result.Id : 0;
        }
        public static Guest GetExisting(ApplicationDbContext db, string guestId)
        {
            var result = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return result;
        }
        public static int Add(string id, string firstName, string lastName, string profilePicture = "")
        {
            using (var db = new ApplicationDbContext())
            {
                var existing = GetExisting(db, id);
                if (existing == null)
                {
                    if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(id))
                    {
                        Guest g = new Guest();
                        g.GuestId = string.IsNullOrEmpty(id) ? null : id;
                        g.Name = (firstName + " " + lastName);
                        g.ProfilePictureUrl = string.IsNullOrEmpty(profilePicture) ? null : profilePicture;
                        db.Guests.Add(g);
                        db.SaveChanges();
                        return g.Id;
                    }
                }
                else
                {
                    return existing.Id;
                }
                return 0;
            }
        }
    }
}
