﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Local
{
    public class Inboxes
    {
        public static Inbox ThreadForThreadId(ApplicationDbContext db, string threadId)
        {
            var entity = db.Inbox.Where(f => f.ThreadId == threadId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }
            return null;
        }

        public static AddThreadResult Add(int hostId, DateTime lastMessageAt, bool hasUnreadMessages, string lastMessageSnippet, string guestId, string guestFirstName, string guestLastName, string threadId, bool isInquiryOnly, string status, DateTime stayStartDate, DateTime stayEndDate, long listingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = new AddThreadResult();
                var existing = ThreadForThreadId(db, threadId);

                if (existing == null)
                {
                    int guestLocalId = Core.API.Local.Guests.Add(guestId, guestFirstName, guestLastName);
                    if (guestLocalId != 0)
                    {
                        try
                        {
                            Inbox inbox = new Inbox()
                            {
                                HostId = hostId,
                                GuestId = guestLocalId,
                                LastMessage = lastMessageSnippet,
                                LastMessageAt = lastMessageAt,
                                HasUnread = hasUnreadMessages,
                                ThreadId = threadId,
                                IsInquiryOnly = isInquiryOnly,
                                Status = Utilities.GetReservationStatusCode(status),
                                CheckInDate = stayStartDate,
                                CheckOutDate = stayEndDate,
                                PropertyId = listingId,
                                SiteType = (int)Core.Enumerations.SiteType.Local
                            };
                            db.Inbox.Add(inbox);

                            if (db.SaveChanges() > 0)
                            {
                                result.IsSaved = true;
                                result.Thread = inbox;
                            }
                        }
                        catch (Exception e)
                        {
                            Trace.WriteLine(e.ToString());
                        }
                    }
                }
                else
                {
                    existing.LastMessageAt = lastMessageAt;
                    existing.HasUnread = hasUnreadMessages;
                    existing.LastMessage = lastMessageSnippet;
                    existing.IsInquiryOnly = isInquiryOnly;
                    existing.Status = Utilities.GetReservationStatusCode(status);
                    existing.CheckInDate = stayStartDate;
                    existing.CheckOutDate = stayEndDate;
                    existing.PropertyId = listingId;
                    existing.HostId = hostId;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Thread = existing;
                    }
                }
                return result;
            }
        }
    }
    public class AddThreadResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Inbox Thread { get; set; }
    }
}
