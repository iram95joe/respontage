﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Inquiries
    {
        public static List<Income> GetIncomes(int bookingId)
        {
            using (var db=new ApplicationDbContext())
            {
                var list = new List<Income>();
                var incomes = db.Income.Where(y => y.BookingId == bookingId && y.IsActive && y.IncomeTypeId != (int)Core.Enumerations.IncomeType.Refund).ToList();
                foreach (var income in incomes)
                {
                    var temp = db.IncomePayment.Where(y => y.IncomeId == income.Id && y.IsRejected==false).ToList();
                    //JM 09/14/19  amount minus payment to get balance
                    if (temp.Sum(y => y.Amount) < income.Amount)
                    {
                        income.Amount = income.Amount - temp.Sum(y => y.Amount);
                        list.Add(income);
                    }
                }
                return list;
            }
        }
        #region CctvFootageNotes

        public static List<CctvFootageNote> CctvFootageNotes(int inquiryLocalId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.CctvFootageNotes.Where(x => x.InquiryLocalId == inquiryLocalId).ToList();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion

        #region GetAlterHistory

        public static List<Database.Entity.AlterHistory> GetAlterHistory(string threadId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.AlterHistory.Where(x => x.ThreadId == threadId).ToList();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion

        #region GetByThreadId

        public static Database.Entity.Inquiry GetByThreadId(string threadId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Inquiries.Where(x => x.ThreadId == threadId).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion
    }
}
