﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class PriceRuleSettings
    {
        #region Add

        public static bool Add(Database.Entity.PriceRuleSettings settings)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.PriceRuleSettings.Add(settings);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion

        #region GetByListingId

        public static List<Database.Entity.PriceRuleSettings> GetByListingId(long listingId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.PriceRuleSettings.Where(x => x.ListingId == listingId).ToList();
                }
            }
            catch (Exception e)
            {
                return new List<Database.Entity.PriceRuleSettings>();
            }
        }

        #endregion

        #region Update

        public static bool Update(Database.Entity.PriceRuleSettings settings)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var currentSettings = db.PriceRuleSettings.Where(x => x.Id == settings.Id).FirstOrDefault();

                    currentSettings.PriceRuleAmount = settings.PriceRuleAmount;
                    currentSettings.PriceRuleAveragePriceBasis = settings.PriceRuleAveragePriceBasis;
                    currentSettings.PriceRuleBasis = settings.PriceRuleBasis;
                    currentSettings.PriceRuleCondition = settings.PriceRuleCondition;
                    currentSettings.PriceRuleConditionPercentage = settings.PriceRuleConditionPercentage;
                    currentSettings.PriceRuleSign = settings.PriceRuleSign;

                    db.Entry(currentSettings).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion
    }
}
