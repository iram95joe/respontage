﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Incomes
    {
        public static void CreateBatchPaymentFromCancelledIncome(int Id)
        {
            using(var db = new ApplicationDbContext())
            {
                var income = db.Income.Where(x => x.Id == Id).FirstOrDefault();
                var payments = db.IncomePayment.Where(x => x.IncomeId == income.Id).ToList();
                if (payments.Where(x=>x.IncomeBatchPaymentId==null).Sum(x => x.Amount) > 0)
                {
                    var batchPayment = new IncomeBatchPayment()
                    {
                        Amount = payments.Where(x => x.IncomeBatchPaymentId == null).Sum(x => x.Amount),
                        Description ="Payment from "+ income.Description,
                        BookingId = income.BookingId,
                        PaymentDate = DateTime.Now,
                        CompanyId = SessionHandler.CompanyId
                    };
                    db.IncomeBatchPayments.Add(batchPayment);
                    db.SaveChanges();
                }
                db.IncomePayment.RemoveRange(payments);
                db.SaveChanges();

            }
        }
   
    }
}
