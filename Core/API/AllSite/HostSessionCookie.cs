﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class HostSessionCookie
    {
        public static string GetToken(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from hsc in db.HostSessionCookies.ToList() where hsc.IsActive && hsc.HostId == hostId select hsc).Last().Token;

            }
        }
    }
}
