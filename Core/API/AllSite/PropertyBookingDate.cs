﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class PropertyBookingDate
    {
        #region Update

        public static Tuple<bool, Database.Entity.PropertyBookingDate> Update(Database.Entity.PropertyBookingDate bookingDate)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var pbd = db.PropertyBookingDate.Where(x => x.Id == bookingDate.Id).FirstOrDefault();

                    pbd.Price = bookingDate.Price;

                    db.SaveChanges();

                    return new Tuple<bool, Database.Entity.PropertyBookingDate>(true, pbd);
                }
            }
            catch (Exception e)
            {
                return new Tuple<bool, Database.Entity.PropertyBookingDate>(true, new Database.Entity.PropertyBookingDate());
            }
        }

        #endregion
    }
}
