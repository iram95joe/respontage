﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Hosts
    {
        public static List<Database.Entity.Host> GetList
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == SessionHandler.CompanyId select h).ToList();
                }
            }
        }
    }
}
