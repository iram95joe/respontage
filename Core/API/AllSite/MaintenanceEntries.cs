﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class MaintenanceEntries
    {
        public static int Add(MaintenanceEntry entry)
        {
            using (var db = new ApplicationDbContext())
            {
               
                    db.MaintenanceEntries.Add(entry);
                    db.SaveChanges();
                    return entry.Id;
               
            }
        }
        public static int Update(int Id, MaintenanceEntry entry)
        {
            var id = 0;
            using (var db = new ApplicationDbContext())
            {
                var temp = db.MaintenanceEntries.Where(x => x.Id == Id).FirstOrDefault();
                if (temp != null)
                {
                    id = Id;
                    //temp.Amount = entry.Amount;
                    temp.Description = entry.Description;
                    temp.DueDate = entry.DueDate;
                    temp.VendorId = entry.VendorId;
                    temp.WorkerId = entry.WorkerId;
                    temp.JobTypeId = entry.JobTypeId;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    db.MaintenanceEntries.Add(entry);
                    db.SaveChanges();
                    id = entry.Id;
                }
                return id;

            }
        }
    }
}
