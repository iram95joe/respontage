﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Inbox
    {
        #region GetByThreadId

        public static Database.Entity.Inbox GetByThreadId(string threadId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion
    }
}
