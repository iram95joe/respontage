﻿using Core.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Users
    {
        public static int GetCompanyId(int userId)
        {
            using(var db = new ApplicationDbContext())
            {
                return db.Users.Where(x => x.Id == userId).FirstOrDefault().CompanyId;
            }
        }
    }
}
