﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Guests
    {
        public static void SaveEmail(string GuestId, string Email)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.GuestEmails.Where(x => x.GuestId == GuestId && x.Email == Email).FirstOrDefault();
                if (temp == null)
                {
                    GuestEmail guestEmail = new GuestEmail
                    {
                        GuestId = GuestId,
                        Email = Email

                    };
                    db.GuestEmails.Add(guestEmail);
                    db.SaveChanges();

                }
            }
        }
        public static string GetNameById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Guests.Where(g => g.Id == id).Select(g => g.Name).First();
            }
        }

        public static string GetEmailByLocalId(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Guests.Where(g => g.Id == id).Select(x => x.Email).FirstOrDefault();
            }
        }
        public static int GetLocalGuestId(ApplicationDbContext db, string guestId)
        {
            var result = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return result != null ? result.Id : 0;
        }
        public static Guest GetExisting(ApplicationDbContext db, string guestId)
        {
            var result = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return result;
        }
        public static Database.Entity.Guest Add(string id, string firstName, string lastName, string profilePicture = "")
        {
            using (var db = new ApplicationDbContext())
            {
                var existing = GetExisting(db, id);
                if (existing == null)
                {
                    if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(id))
                    {
                        Guest g = new Guest();
                        g.GuestId = string.IsNullOrEmpty(id) ? null : id;
                        g.Name = (firstName + " " + lastName);
                        g.ProfilePictureUrl = string.IsNullOrEmpty(profilePicture) ? null : profilePicture;
                        Guest guestData = db.Guests.Add(g);
                        db.SaveChanges();
                        return guestData;
                    }
                }
                else
                {
                    return existing;
                }

                return existing;
            }
        }

        #region GetByLocalId

        public static Database.Entity.Guest GetByLocalId(int guestLocalId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Guests.Where(x => x.Id == guestLocalId).FirstOrDefault();
                }
            }
            catch (Exception e)
            {

            }

            return new Database.Entity.Guest();
        }

        #endregion
    }
}
