﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Properties
    {
        #region GetByLocalPropertyId

        public static Database.Entity.Property GetByLocalPropertyId(int localId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Properties.Where(x => x.Id == localId).FirstOrDefault();
            }
        }

        #endregion

        #region GetChildPropertiesByLocalId

        public static List<Database.Entity.Property> GetChildPropertiesByLocalId(int localId)
        {
            using (var db = new ApplicationDbContext())
            {
                var childIds = db.ParentChildProperties.Where(x => x.ParentPropertyId == localId && x.CompanyId == SessionHandler.CompanyId).Select(x => x.ChildPropertyId).ToList();
                return db.Properties.Where(x => childIds.Contains(x.Id)).ToList();
            }
        }

        #endregion

        #region GetByListingId

        public static Database.Entity.Property GetByListingId(long listingId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
            }
        }

        #endregion

        #region GetLocalId

        public static int GetLocalId(long siteListingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Where(x => x.ListingId == siteListingId).FirstOrDefault();

                if (temp != null)
                {
                    return temp.Id;
                }
            }

            return 0;
        }

        #endregion

        #region GetSiteId

        public static long GetSiteId(int localId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Where(x => x.Id == localId).FirstOrDefault();

                if (temp != null)
                {
                    return temp.ListingId;
                }
            }

            return 0;
        }

        #endregion

        #region UpdateCctvFootageNote

        public static List<CctvFootageNote> AddOrUpdateCctvFootageNote(int inquiryLocalId, string note, 
            DateTime date, string camera)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var cctvFootageNote = db.CctvFootageNotes
                        .Where(x => x.InquiryLocalId == inquiryLocalId && DbFunctions.TruncateTime(x.ReservationDate) == date.Date)
                        .FirstOrDefault();

                    if (cctvFootageNote == null)
                    {
                        db.CctvFootageNotes.Add(new CctvFootageNote
                        {
                            InquiryLocalId = inquiryLocalId,
                            Notes = note,
                            ReservationDate = date,
                            Camera = camera
                        });
                    }
                    else
                    {
                        cctvFootageNote.Notes = note;
                        db.Entry(cctvFootageNote).State = EntityState.Modified; 
                    }

                    db.SaveChanges();

                    return db.CctvFootageNotes.Where(x => x.InquiryLocalId == inquiryLocalId).ToList();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #endregion
        public static string GetName(int Id)
        {
            using(var db = new ApplicationDbContext())
            {
                var property = db.Properties.Where(x => x.Id == Id).FirstOrDefault();
                if (property != null)
                {
                    return property.Name;
                }
                else { return ""; }
            }
        }
    }
}
