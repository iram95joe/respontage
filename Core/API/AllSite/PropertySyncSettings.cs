﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class PropertySyncSettings
    {
        #region GetByLocalPropertyId

        public static Database.Entity.PropertySyncSettings GetByLocalPropertyId(int localId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.PropertySyncSettings.Where(x => x.PropertyLocalId == localId).FirstOrDefault();
            }
        }

        #endregion
    }
}
