﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class ContractChangesLogs
    {
        public static void Add(string message,int bookingId)
        {
            using(var db = new ApplicationDbContext())
            {
                var log = new ContractChangesLog()
                {
                    DateCreated = DateTime.Now,
                    BookingId = bookingId,
                    Message = message
                };
               
                db.ContractChangesLogs.Add(log);
                db.SaveChanges();
            }
                
        }
    }
}
