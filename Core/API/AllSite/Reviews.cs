﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.AllSite
{
    public class Reviews
    {
        public static List<Review> GetReviewToGuest(string guestId)
        {
            using(var db = new ApplicationDbContext())
            {
               return db.Reviews.Where(x => x.To == guestId).ToList();
            }
        }
        public static List<Review> GetReviewFromGuest(string guestId)
        {
            using (var db = new ApplicationDbContext())
            {

                var reviews =db.Reviews.Where(x => x.From == guestId).ToList();
                foreach(var review in reviews)
                {
                    var host = db.Reviews.Where(x => x.From == review.To).FirstOrDefault();
                    if (host != null)
                    {
                        review.Name = host.Name;
                        review.ProfilePictureUrl = host.ProfilePictureUrl;
                    }
                    else
                    {
                        review.Name = "N/A";
                        review.ProfilePictureUrl = host.ProfilePictureUrl;
                    }
                }
                return reviews;

            }
        }
    }
}
