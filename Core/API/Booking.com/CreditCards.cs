﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Diagnostics;
using System.Linq;

namespace Core.API.Booking.com
{
    public class CreditCards
    {
        #region CreditCardForReservationId

        public static CreditCard CreditCardForReservationId(ApplicationDbContext db, string reservationId)
        {
            var entity = db.CreditCard.Where(f => f.ReservationId == reservationId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }

            return null;
        }

        #endregion

        #region AddCardDetails

        public static bool AddCardDetails(ApplicationDbContext db, string reservationId, string type, string number,
            string holderName, string expiry, string cvc)
        {
            try
            {
                var existing = CreditCardForReservationId(db, reservationId);

                if (existing != null)
                {
                    existing.Type = type;
                    existing.Number = number;
                    existing.HolderName = holderName;
                    existing.Expiry = expiry;
                    existing.CVC = cvc;
                }
                else
                {
                    db.CreditCard.Add(new CreditCard
                    {
                        ReservationId = reservationId,
                        Type = type,
                        Number = number,
                        HolderName = holderName,
                        Expiry = expiry,
                        CVC = cvc
                    });
                }

                db.BulkSaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region UpdateActionData

        public static bool UpdateActionData(ApplicationDbContext db, string reservationId, string cardStatus, string cardViewLimit, string paymentStatus, string cardAlertHeading, string cardAlertDescription, string replyActions)
        {
            var existing = CreditCardForReservationId(db, reservationId);

            if (existing != null)
            {
                existing.CardAlertHeading = cardAlertHeading;
                existing.CardAlertDescription = cardAlertDescription;
                existing.PaymentStatus = paymentStatus;
                existing.CardStatus = cardStatus;
                existing.CardViewLimit = cardViewLimit;
                existing.ReplyActions = replyActions;

                db.BulkSaveChanges();
                return true;
            }
            return false;
        }

        #endregion
    }
}
