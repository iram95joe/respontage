﻿using Core.Database.Context;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Booking.com
{
    public class HostSessionCookies
    {
        #region Add

        public static AddSessionUserResult Add(int hostId, string cookies, string sessionKey)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = new AddSessionUserResult();
                Core.Database.Entity.HostSessionCookie h = new Core.Database.Entity.HostSessionCookie()
                {
                    IsActive = true,
                    Cookies = cookies,
                    Token = PasswordGenerator.Generate(10),
                    CreatedAt = DateTime.Now,
                    SiteType = 3,
                    HostId = hostId,
                    SessionKey = sessionKey
                };
                db.HostSessionCookies.Add(h);

                if (db.SaveChanges() > 0)
                {
                    result.IsSaved = true;
                    result.HostSessionCookie = h;
                }
                else
                {
                    result.IsSaved = false;
                }
                return result;
            }
        }

        #endregion
    }

    public class AddSessionUserResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Core.Database.Entity.HostSessionCookie HostSessionCookie { get; set; }
    }
}
