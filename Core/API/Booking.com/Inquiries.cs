﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Booking.com
{
    public class Inquiries
    {
        #region AddPaymentRequest

        public static Tuple<bool, long> AddPaymentRequest(Core.Enumerations.PaymentRequestType paymentType, Core.Enumerations.PaymentRequestMethod paymentMethod,
            string paymentName, decimal paymentPrice, decimal balanceAfter, string confirmationCode)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var ph = db.PaymentHistory.Add(new PaymentHistory
                    {
                        ConfirmationCode = confirmationCode,
                        PaymentType = (byte)paymentType,
                        PaymentMethod = (byte)paymentMethod,
                        PaymentName = paymentName,
                        PaymentPrice = paymentPrice,
                        BalanceAfter = balanceAfter,
                        Paid = false,
                        DateCreated = DateTime.UtcNow
                    });

                    db.SaveChanges();

                    return new Tuple<bool, long>(true, ph.Id);
                }
            }
            catch (Exception e)
            {
                return new Tuple<bool, long>(false, 0);
            }
        }

        #endregion
        #region AddPaypalRedirectUrl

        public static bool AddPaypalRedirectUrl(string confirmationCode, long paymentHistoryId,
            string paypalRedirectUrl)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var ph = db.PaymentHistory
                        .Where(x => x.ConfirmationCode == confirmationCode && x.Id == paymentHistoryId)
                        .FirstOrDefault();

                    ph.PaypalRedirectUrl = paypalRedirectUrl;

                    db.BulkSaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion
        #region DeletePaymentRequest

        public static bool DeletePaymentHistory(long paymentHistoryId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var paymentRequest = db.PaymentHistory.Where(x => x.Id == paymentHistoryId).First();

                    db.PaymentHistory.Remove(paymentRequest);
                    db.BulkSaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion
        #region GetByConfirmationCode

        public static Inquiry GetByConfirmationCode(string confirmationCode)
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = db.Inquiries.Where(f => f.ConfirmationCode == confirmationCode).FirstOrDefault();

                if (entity != null)
                {
                    return entity;
                }

                return null;
            }
        }

        #endregion


        #region UpdateIfPaid

        public static bool UpdateIfPaid(string confirmationCode)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var reservation = db.Inquiries
                        .Where(x => x.ConfirmationCode == confirmationCode)
                        .First();

                    var ph = GetPaymentHistory(confirmationCode);
                    var remainingBalance = reservation.TotalPayout - ph.Sum(x => x.PaymentPrice);
                    var hasNotYetPaidRequest = ph.Any(x => x.Paid == false);

                    if (remainingBalance == 0 && !hasNotYetPaidRequest)
                    {
                        reservation.Paid = true;
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion
        #region SettlePaymentRequest

        public static void SettlePaymentRequest(string confirmationCode, long paymentHistoryId)
        {
            using (var db = new ApplicationDbContext())
            {
                var paymentRequest = db.PaymentHistory
                    .Where(x => x.ConfirmationCode == confirmationCode && x.Id == paymentHistoryId)
                    .First();

                paymentRequest.Paid = true;

                db.SaveChanges();
            }
        }

        #endregion
        #region GetPaymentHistory

        public static Core.Database.Entity.PaymentHistory GetPaymentHistory(string confirmationCode, long paymentHistoryId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.PaymentHistory
                        .Where(x => x.ConfirmationCode == confirmationCode && x.Id == paymentHistoryId)
                        .First();
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<Core.Database.Entity.PaymentHistory> GetPaymentHistory(string confirmationCode)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from ph in db.PaymentHistory where ph.ConfirmationCode == confirmationCode select ph).ToList();
                }
            }
            catch (ArgumentNullException e)
            {
                return null;
            }
        }
        #endregion

        #region InquiryForConversationId

        public static Inquiry InquiryForConversationId(ApplicationDbContext db, string threadId)
        {
            var entity = db.Inquiries.Where(f => f.ThreadId == threadId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }
            return null;
        }

        #endregion

        #region InquiryForConfirmationCode

        public static Inquiry InquiryForConfirmationCode(ApplicationDbContext db, string code)
        {
            var entity = db.Inquiries.Where(f => f.ConfirmationCode == code).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }
            return null;
        }

        #endregion

        #region Add

        public static AddInquiryResult Add(int hostId, string code, string reservationId, string status, DateTime stayStartDate, DateTime stayEndDate,
              int nights, long listingId, string guestId, string reservationCost, string cleaningFee, string serviceFee, string totalPayout,
              DateTime inquiryDate, DateTime reservationRequestDatetime, int guestCount, int adults, int children, DateTime reservationConfirmDate, DateTime reservationCancelDate,
              string threadId, string type
            )
        {
            using (var db = new ApplicationDbContext())
            {
                var result = new AddInquiryResult();
                var existing = InquiryForConfirmationCode(db, code);
                if (existing == null)
                {
                    Inquiry inquiry = new Inquiry()
                    {
                        ReservationId = string.IsNullOrEmpty(reservationId) ? null : reservationId,
                        IsImported = true,
                        SiteType = (int)Enumerations.SiteType.BookingCom,
                        BookingDate = reservationRequestDatetime == DateTime.MinValue ? (DateTime?)null : reservationRequestDatetime,
                        CleaningFee = Utilities.ExtractDecimal(cleaningFee),
                        ConfirmationCode = string.IsNullOrEmpty(code) ? null : code,
                        GuestCount = guestCount,
                        Adult = adults,
                        Children = children,
                        GuestId = Core.API.Booking.com.Guests.GetLocalGuestId(db, guestId),
                        InquiryDate = inquiryDate == DateTime.MinValue ? (DateTime?)null : inquiryDate,
                        PropertyId = listingId.ToInt(),
                        Nights = nights.ToInt(),
                        ReservationCost = Utilities.ExtractDecimal(reservationCost),
                        ServiceFee = Utilities.ExtractDecimal(serviceFee),
                        BookingStatusCode = Utilities.GetReservationStatusCode(status),
                        CheckInDate = stayStartDate == DateTime.MinValue ? (DateTime?)null : stayStartDate,
                        CheckOutDate = stayEndDate == DateTime.MinValue ? (DateTime?)null : stayEndDate,
                        TotalPayout = Utilities.ExtractDecimal(totalPayout),
                        BookingCancelDate = reservationCancelDate == DateTime.MinValue ? (DateTime?)null : reservationCancelDate,
                        BookingConfirmDate = reservationConfirmDate == DateTime.MinValue ? (DateTime?)null : reservationConfirmDate,
                        ThreadId = threadId,
                        Type = type,
                        //CompanyId = GlobalVariables.CompanyId, // Don't use SessionHandler.CompanyId here because it's not working on multi-threading
                        HostId = hostId
                    };
                    db.Inquiries.Add(inquiry);

                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            result.IsSaved = true;
                            result.Inquiry = inquiry;
                            BookingHub.RefreshBookingList(GlobalVariables.UserId.ToString());
                        }
                        else
                        {
                            result.IsSaved = false;
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                    }
                }
                else
                {
                    // the next time you'll encounter a null values in db and it gives you an error in the application level,
                    // always remember that it is always an OLD RECORD, that's why you need to replace it, just like these two :
                    existing.ReservationId = string.IsNullOrEmpty(reservationId) ? null : reservationId;
                    existing.ConfirmationCode = string.IsNullOrEmpty(code) ? null : code;

                    existing.BookingDate = reservationRequestDatetime == DateTime.MinValue ? (DateTime?)null : reservationRequestDatetime;
                    existing.CleaningFee = Utilities.ExtractDecimal(cleaningFee);
                    existing.GuestCount = guestCount;
                    existing.GuestId = Core.API.Booking.com.Guests.GetLocalGuestId(db, guestId);
                    existing.Nights = nights.ToInt();
                    existing.ReservationCost = Utilities.ExtractDecimal(reservationCost);
                    existing.ServiceFee = Utilities.ExtractDecimal(serviceFee);
                    existing.CheckOutDate = stayEndDate == DateTime.MinValue ? (DateTime?)null : stayEndDate;
                    existing.CheckInDate = stayStartDate == DateTime.MinValue ? (DateTime?)null : stayStartDate;
                    existing.TotalPayout = Utilities.ExtractDecimal(totalPayout);
                    existing.BookingCancelDate = reservationCancelDate == DateTime.MinValue ? (DateTime?)null : reservationCancelDate;
                    existing.BookingConfirmDate = reservationConfirmDate == DateTime.MinValue ? (DateTime?)null : reservationConfirmDate;
                    existing.ThreadId = threadId;
                    existing.BookingStatusCode = Utilities.GetReservationStatusCode(status);
                    existing.HostId = hostId;
                    existing.Type = type;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;

                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Inquiry = existing;
                        BookingHub.RefreshBookingList(GlobalVariables.UserId.ToString());
                    }
                }
                return result;
            }
        }

        #endregion

        #region HasCreditCardInformation

        public static bool HasCreditCardInformation(string reservationId)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    return (from cc in db.CreditCard where cc.ReservationId == reservationId select true).FirstOrDefault();
                }
                catch (Exception)
                {
                    return true;
                }
            }
        }

        #endregion
    }

    public class AddInquiryResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Inquiry Inquiry { get; set; }
    }
}
