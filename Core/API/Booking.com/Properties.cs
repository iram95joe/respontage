﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Booking.com
{
    public class Properties
    {
        public static List<Core.Database.Entity.Property> List
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where p.SiteType == 3 && hc.CompanyId == SessionHandler.CompanyId select p).ToList();
                }
            }
        }
        #region GetByListingId

        public static Property GetByListingId(long listingId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Properties
                        .Where(x => x.ListingId == listingId)
                        .First();
                }
            }
            catch (Exception e)
            {
                return new Property();
            }
        }

        #endregion
        private static Property PropertyForListingId(ApplicationDbContext db, long propertyId)
        {
            var entity = db.Properties.Where(f => f.ListingId == propertyId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }

            return null;
        }

        private static Room RoomForRoomId(ApplicationDbContext db, long roomId)
        {
            var entity = db.Rooms.Where(f => f.RoomId == roomId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }

            return null;
        }

        #region Add

        public static AddPropertyResult Add(int hostId, long listingId, string listingUrl, string name, string address,
            string longitude, string latitude, int minStay, string internet, string pets, string wheelChair,
            string description, int reviews, string sleeps, int bedrooms, int bathrooms, string accomodationType,
            string smoking, string airCondition, string swimmingPool, string status, string calendarRemark
           )
        {
            using (var db = new ApplicationDbContext())
            {
                var result = new AddPropertyResult();
                var existing = PropertyForListingId(db, listingId);
                if (existing == null)
                {
                    int type = 1;

                    int siteType = (int)Enumerations.SiteType.BookingCom;
                    Property p = new Property();
                    p.HostId = hostId;
                    p.Address = address;
                    p.ListingId = listingId;
                    p.ListingUrl = listingUrl;
                    p.Name = name;
                    p.Longitude = longitude;
                    p.Latitude = latitude;
                    p.MinStay = minStay;
                    p.Internet = internet;
                    p.Pets = pets;
                    p.WheelChair = wheelChair;
                    p.Description = description;
                    p.Reviews = 0;
                    p.Sleeps = 0;
                    p.Bedrooms = bedrooms;
                    p.Bathrooms = bathrooms;
                    p.PropertyType = "house";
                    p.AccomodationType = accomodationType;
                    p.Smoking = smoking;
                    p.AirCondition = airCondition;
                    p.SwimmingPool = swimmingPool;
                    p.Status = status;
                    p.SiteType = siteType;
                    p.CommissionId = -1;
                    p.Type = type;
                    //p.CompanyId = GlobalVariables.CompanyId; // Don't use SessionHandler.CompanyId here because it's not working on multi-threading
                    p.CalendarRemark = calendarRemark;
                    db.Properties.Add(p);

                    if (db.SaveChanges() > 0)
                    {
                        result.IsSaved = true;
                        result.Property = p;
                        PropertyHub.RefreshPropertyList(GlobalVariables.UserId.ToString());
                    }
                    else
                    {
                        result.IsSaved = false;
                    }
                }
                else
                {
                    existing.Name = name;
                    existing.Longitude = longitude;
                    existing.Latitude = latitude;
                    existing.MinStay = minStay;
                    existing.Internet = internet;
                    existing.Pets = pets;
                    existing.WheelChair = wheelChair;
                    existing.Description = description;
                    existing.Reviews = 0;
                    existing.Sleeps = 0;
                    existing.Bedrooms = bedrooms;
                    existing.Bathrooms = bathrooms;
                    existing.PropertyType = "house";
                    existing.AccomodationType = accomodationType;
                    existing.Smoking = smoking;
                    existing.AirCondition = airCondition;
                    existing.SwimmingPool = swimmingPool;
                    existing.Status = status;
                    existing.CalendarRemark = calendarRemark;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Property = existing;
                        PropertyHub.RefreshPropertyList(GlobalVariables.UserId.ToString());
                    }
                }
                return result;
            }
        }

        #endregion

        #region AddRoom

        public static void AddRoom(ApplicationDbContext db, Room room)
        {
            var existing = RoomForRoomId(db, room.RoomId);

            if (existing == null)
            {
                db.Rooms.Add(room);
                db.BulkSaveChanges();
            }
            else
            {
                existing.RoomId = room.RoomId;
                existing.RoomName = room.RoomName;

                db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
                db.BulkSaveChanges();
            }
        }

        #endregion

        #region GetRoom

        public static Dictionary<string, Room> GetRoom()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from r in db.Rooms select r).ToDictionary(p => p.ListingId + "" + p.RoomId);
            }
        }

        public static List<Room> GetRoom(long listingId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from r in db.Rooms where r.ListingId == listingId select r).ToList();
                }
            }
            catch (Exception)
            {
                return new List<Room>();
            }
        }

        #endregion

        #region GetRoomIds

        public static List<long> GetRoomIds(long listingId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from r in db.Rooms where r.ListingId == listingId select r.RoomId).ToList();
            }
        }

        #endregion
    }
    public class AddPropertyResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Property Property { get; set; }
    }
}
