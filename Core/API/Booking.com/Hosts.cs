﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Booking.com
{
    public class Hosts
    {
        //public static Models.ConfigurationKey ConfigurationKeys
        //{
        //    get
        //    {
        //        try
        //        {
        //            using (var db = new ApplicationDbContext())
        //            {
        //                var keys = JsonConvert
        //                    .DeserializeObject<Core.Models.ConfigurationKey>(
        //                        db.PaymentAccount
        //                        .Where(x => x.CompanyId == SessionHandler.CompanyId)
        //                        .Select(x => x.ConfigurationKeys)
        //                        .First()
        //                    );

        //                return keys;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            return new Models.ConfigurationKey();
        //        }
        //    }
        //}
        #region HostForUserId

        public static Host HostForUserId(ApplicationDbContext db, string hostId)
        {
            var entity = db.Hosts.Where(f => f.SiteHostId == hostId).FirstOrDefault();
            return entity != null ? entity : null;
        }

        #endregion

        #region Add

        public static AddHostResult Add(string siteHostId, string firstName, string lastName, string email, string password = "", string imageUrl = "")
        {
            using (var db = new ApplicationDbContext())
            {
                var result = new AddHostResult();
                var existing = HostForUserId(db, siteHostId);
                if (existing == null)
                {
                    Host h = new Host();
                    h.SiteHostId = siteHostId;
                    h.SiteType = (int)((object)Core.Enumerations.SiteType.BookingCom);
                    h.Firstname = firstName;
                    h.Lastname = lastName;
                    h.ProfilePictureUrl = string.IsNullOrEmpty(imageUrl) ? null : imageUrl;
                    h.Username = email;
                    h.Password = string.IsNullOrEmpty(password) ? null : Security.Encrypt(password);
                    h.AutoSync = true;
                    //h.CompanyId = GlobalVariables.CompanyId;
                    db.Hosts.Add(h);

                    if (db.SaveChanges() > 0)
                    {
                        result.IsSaved = true;
                        result.Host = h;
                    }
                    else
                    {
                        result.IsSaved = false;
                    }
                }
                else
                {
                    existing.Firstname = firstName;
                    existing.Lastname = lastName;
                    existing.Username = email;
                    existing.ProfilePictureUrl = string.IsNullOrEmpty(imageUrl) ? null : imageUrl;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Host = existing;
                    }
                }
                return result;
            }
        }

        #endregion

        #region GetHostId

        public static int GetHostId(string username, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                string pass = Security.Encrypt(password);

                try
                {
                    var asd = (from h in db.Hosts
                               where
                                    h.Username == username &&
                                    h.Password == pass &&
                                    h.SiteType == 3
                               select h.Id).First();

                    return asd;
                }
                catch (Exception e)
                {
                    return 0;
                }
            }
        }

        #endregion

        #region GetHostSession

        public static Core.Database.Entity.HostSessionCookie GetHostSession(string token)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var session = (from h in db.HostSessionCookies
                                   where
                                        h.Token == token &&
                                        h.SiteType == 3
                                   select h).FirstOrDefault();

                    return session;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        #endregion

        #region GetHostSession

        public static Core.Database.Entity.HostSessionCookie GetHostSession(string username, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                int hostId = GetHostId(username, password);

                try
                {
                    var session = (from h in db.HostSessionCookies
                                   where
                                        h.HostId == hostId
                                   select h).FirstOrDefault();

                    return session;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        #endregion

        #region GetHostUsernameAndPassword

        public static Tuple<string, string> GetHostUsernameAndPassword(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = (from h in db.Hosts
                            where
                                 h.Id == hostId &&
                                 h.SiteType == 3
                            select h).First();

                return new Tuple<string, string>(user.Username, Security.Decrypt(user.Password));
            }
        }

        #endregion

        #region GetToken

        public static string GetToken(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from hsc in db.HostSessionCookies.AsQueryable()
                        where
                             hsc.HostId == hostId &&
                             hsc.SiteType == 3
                        orderby hsc.Id descending
                        select hsc.Token).FirstOrDefault();
            }
        }

        #endregion

        #region SavePaymentAccountInformation

        //public static bool SavePaymentAccountInformation(Database.Entity.PaymentAccount account)
        //{
        //    try
        //    {
        //        using (var db = new ApplicationDbContext())
        //        {
        //            var existing = db.PaymentAccount.Where(x => x.CompanyId == account.CompanyId).FirstOrDefault();

        //            if (existing == null)
        //            {
        //                db.PaymentAccount.Add(account);
        //            }
        //            else
        //            {
        //                existing.ConfigurationKeys = account.ConfigurationKeys;
        //            }

        //            db.SaveChanges();

        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
        //}

        #endregion
    }

    #region AddHostResult (class)

    public class AddHostResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Host Host { get; set; }
    }

    #endregion
}
