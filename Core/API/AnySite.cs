﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API
{
    public class AnySite
    {
        public static double DefaultRate
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    try
                    {
                        return (from d in db.DefaultRates
                                where
                                     d.CompanyId == GlobalVariables.CompanyId
                                select d.StandardRate).First();
                    }
                    catch (ArgumentNullException err1) { return 0; }
                    catch (InvalidOperationException err2) { return 0; }
                }
            }
            set
            {
                using (var db = new ApplicationDbContext())
                {
                    try
                    {
                        var entity = (from d in db.DefaultRates
                                      where
                                           d.CompanyId == GlobalVariables.CompanyId
                                      select d).First();

                        entity.StandardRate = value;
                    }
                    catch (ArgumentNullException err1)
                    {
                        db.DefaultRates.Add(new DefaultRate
                        {
                            StandardRate = value,
                            CompanyId = GlobalVariables.CompanyId
                        });
                    }
                    catch (InvalidOperationException err2)
                    {
                        db.DefaultRates.Add(new DefaultRate
                        {
                            StandardRate = value,
                            CompanyId = GlobalVariables.CompanyId
                        });
                    }

                    db.SaveChanges();
                }
            }
        }
    }
}
