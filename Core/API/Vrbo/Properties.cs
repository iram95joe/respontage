﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Vrbo
{
    public class Properties
    {
        public static Property PropertyForListingId(ApplicationDbContext db, long propertyId)
        {
            var entity = db.Properties.Where(f => f.ListingId == propertyId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }
            return null;
        }
        public static AddPropertyResult Add(int hostId, long listingId, string listingUrl, string title, string longitude, string latitude,
            int minStay, string internet, string pets, string wheelChair, string description,
            int reviews, string sleeps, int bedrooms, int bathrooms, string propertyType,
            string accomodationType, string smoking, string airCondition, string swimmingPool, string status, 
            string calendarRemark
           )
        {
            using(var db = new ApplicationDbContext())
            {
                var result = new AddPropertyResult();
                var existing = PropertyForListingId(db, listingId);
                if (existing == null)
                {
                    int type = 1;
                    switch (propertyType.ToLower())
                    {
                        case "house": type = 1; break;
                        case "apartment": type = 2; break;
                        case "condominium": type = 3; break;
                        case "townhouse": type = 4; break;
                    } 

                    int siteType = (int)Enumerations.SiteType.VRBO;
                    Property p = new Property();
                    p.HostId = hostId;
                    p.ListingId = listingId;
                    p.ListingUrl = listingUrl;
                    p.Name = title;
                    p.Longitude = longitude;
                    p.Latitude = latitude;
                    p.MinStay = minStay;
                    p.Internet = internet;
                    p.Pets = pets;
                    p.WheelChair = wheelChair;
                    p.Description = description;
                    p.Reviews = 0;
                    p.Sleeps = 0;
                    p.Bedrooms = bedrooms;
                    p.Bathrooms = bathrooms;
                    p.PropertyType = propertyType;
                    p.AccomodationType = accomodationType;
                    p.Smoking = smoking;
                    p.AirCondition = airCondition;
                    p.SwimmingPool = swimmingPool;
                    p.Status = status;
                    p.SiteType = siteType;
                    p.CommissionId = -1;
                    p.Type = type;
                    //p.CompanyId = GlobalVariables.CompanyId; // Don't use SessionHandler.CompanyId here because it's not working on multi-threading
                    p.CalendarRemark = calendarRemark;
                    db.Properties.Add(p);

                    if (db.SaveChanges() > 0)
                    {
                        result.IsSaved = true;
                        result.Property = p;
                        PropertyHub.RefreshPropertyList(GlobalVariables.UserId.ToString());
                    }
                    else
                    {
                        result.IsSaved = false;
                    }
                }
                else
                {
                    existing.Name = title;
                    existing.Longitude = longitude;
                    existing.Latitude = latitude;
                    existing.MinStay = minStay;
                    existing.Internet = internet;
                    existing.Pets = pets;
                    existing.WheelChair = wheelChair;
                    existing.Description = description;
                    existing.Reviews = 0;
                    existing.Sleeps = 0;
                    existing.Bedrooms = bedrooms;
                    existing.Bathrooms = bathrooms;
                    existing.PropertyType = propertyType;
                    existing.AccomodationType = accomodationType;
                    existing.Smoking = smoking;
                    existing.AirCondition = airCondition;
                    existing.SwimmingPool = swimmingPool;
                    existing.Status = status;
                    existing.CalendarRemark = calendarRemark;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Property = existing;
                        PropertyHub.RefreshPropertyList(GlobalVariables.UserId.ToString());
                    }
                }
                return result;
            }
        }
    }
    public class AddPropertyResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Property Property { get; set; }
    }
}
