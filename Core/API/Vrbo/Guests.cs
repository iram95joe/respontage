﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Vrbo
{
    public class Guests
    {
        public static int GetLocalGuestId(ApplicationDbContext db, string guestId)
        {
            var result = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return result != null ? result.Id : 0;
        }
        public static Guest GetExisting(ApplicationDbContext db, string guestId)
        {
            var result = db.Guests.Where(x => x.GuestId == guestId).FirstOrDefault();
            return result;
        }
        public static int Add(string id, string firstName, string lastName, string profilePicture = "")
        {
            using(var db = new ApplicationDbContext())
            {
                var existing = GetExisting(db, id);
                if(existing == null)
                {
                    if(!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(id))
                    {
                        Guest g = new Guest();
                        g.GuestId = string.IsNullOrEmpty(id) ? null : id;
                        g.Name = (firstName + " " + lastName);
                        g.ProfilePictureUrl = string.IsNullOrEmpty(profilePicture) ? null : profilePicture;
                        db.Guests.Add(g);
                        db.SaveChanges();
                        return g.Id;
                    }
                }
                else
                {
                    return existing.Id;
                }
                return 0;
            }
        }
    }
}
