﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Vrbo
{
    public class Hosts
    {
        #region HostForUserId

        public static Host HostForUserId(ApplicationDbContext db, string hostId)
        {
            var entity = db.Hosts.Where(f => f.SiteHostId == hostId).FirstOrDefault();
            return entity != null ? entity : null;
        }

        #endregion

        #region Add

        public static AddHostResult Add(string hostId, string firstName, string lastName, string email, string password = "", string imageUrl = "")
        {
            using(var db = new ApplicationDbContext())
            {
                var result = new AddHostResult();
                var existing = HostForUserId(db, hostId);
                if (existing == null)
                {
                    Host h = new Host();
                    h.SiteHostId = hostId;
                    h.SiteType = (int)((object)Core.Enumerations.SiteType.VRBO);
                    h.Firstname = firstName;
                    h.Lastname = lastName;
                    h.ProfilePictureUrl = string.IsNullOrEmpty(imageUrl) ? null : imageUrl;
                    h.Username = email;
                    h.Password = string.IsNullOrEmpty(password) ? null : Security.Encrypt(password);
                    h.AutoSync = true;
                    //h.CompanyId = SessionHandler.CompanyId;
                    db.Hosts.Add(h);

                    if (db.SaveChanges() > 0)
                    {
                        result.IsSaved = true;
                        result.Host = h;
                    }
                    else
                    {
                        result.IsSaved = false;
                    }
                }
                else
                {
                    existing.Firstname = firstName;
                    existing.Lastname = lastName;
                    existing.Username = email;
                    existing.ProfilePictureUrl = string.IsNullOrEmpty(imageUrl) ? null : imageUrl;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Host = existing;
                    }
                }
                return result;
            } 
        }

        #endregion

        #region GetHostId

        public static int GetHostId(string username, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                string pass = Security.Encrypt(password);

                try
                {
                    var asd = (from h in db.Hosts
                               where
                                    h.Username == username &&
                                    h.Password == pass &&
                                    h.SiteType == 2
                               select h.Id).First();

                    return asd;
                }
                catch (Exception e)
                {
                    return 0;
                }
            }
        }

        #endregion

        #region GetHostUsernameAndPassword

        public static Tuple<string, string> GetHostUsernameAndPassword(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = (from h in db.Hosts
                            where
                                 h.Id == hostId
                            select h).First();

                return new Tuple<string, string>(user.Username, Security.Decrypt(user.Password));
            }
        }

        #endregion

        #region GetToken

        public static string GetToken(int hostId, int usageType = 1)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from hsc in db.HostSessionCookies.AsQueryable()
                        where
                             hsc.HostId == hostId &&
                             hsc.SiteType == 2 
                             //&& hsc.UsageType == usageType
                        orderby hsc.Id descending
                        select hsc.Token).FirstOrDefault();
            }
        }

        #endregion
    }

    #region AddHostResult (class)

    public class AddHostResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Host Host { get; set; }
    }

    #endregion
}
