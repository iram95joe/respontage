﻿using Core.Database.Context;
using Core.Database.Entity;
using System;
using System.Diagnostics;
using System.Linq;

namespace Core.API.Vrbo
{
    public class InboxMessages
    {
        public static Message MessageForVrboMessageId(ApplicationDbContext db, string messageId)
        {
            var entity = db.Messages.Where(f => string.IsNullOrEmpty(messageId) == false && f.MessageId == messageId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }
            return null;
        }
        public static AddMessgeResult Add(string threadId, string messageId, string message, bool isMyMessage, DateTime timestamp)
        {
            using(var db = new ApplicationDbContext())
            {
                var result = new AddMessgeResult();
                var existing = MessageForVrboMessageId(db, messageId);
                if (existing == null)
                {
                    try
                    {
                        Message msg = new Message()
                        {
                            ThreadId = threadId,
                            MessageId = messageId,
                            MessageContent = message,
                            IsMyMessage = isMyMessage,
                            CreatedDate = timestamp,
                        };
                        db.Messages.Add(msg);
                        if (db.SaveChanges() > 0)
                        {
                            result.IsDuplicate = false;
                            result.IsSaved = true;
                            result.Message = msg;
                        }
                        else
                        {
                            result.IsDuplicate = false;
                            result.IsSaved = false;
                        }
                    }
                    catch(Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                    }
                }
                else
                {
                    result.IsDuplicate = true;
                    result.Message = existing;
                }
                return result;
            }
        }
    }
    public class AddMessgeResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Message Message { get; set; }
    }
}
