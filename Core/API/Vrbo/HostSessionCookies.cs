﻿using Core.Database.Context;
using Core.Database.Entity;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.API.Vrbo
{
    public class HostSessionCookies
    {
        public static string Token
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from hsc in db.HostSessionCookies.ToList() where hsc.SiteType == 2 && hsc.IsActive select hsc).Last().Token;
                }
            }
        }

        public static HostSessionCookie HostSessionCookieForToken(string token)
        {
            using(var db = new ApplicationDbContext())
            {
                var entity = db.HostSessionCookies.Where(f => f.Token == token).Where(f => f.IsActive).FirstOrDefault();
                return entity != null ? entity : null;
            }
        }

        public static AddSessionUserResult Add(int hostId, int usageType, string cookies)
        {
            using(var db = new ApplicationDbContext())
            {
                var result = new AddSessionUserResult();
                HostSessionCookie h = new HostSessionCookie()
                {
                    IsActive = true,
                    Cookies = cookies,
                    Token = PasswordGenerator.Generate(10),
                    CreatedAt = DateTime.Now,
                    SiteType = 2,
                    HostId = hostId,
                    //UsageType = usageType
                };
                db.HostSessionCookies.Add(h);

                if (db.SaveChanges() > 0)
                {
                    result.IsSaved = true;
                    result.HostSessionCookie = h;
                }
                else
                {
                    result.IsSaved = false;
                }
                return result;
            }
        }
    }
    public class AddSessionUserResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public HostSessionCookie HostSessionCookie { get; set; }
    }
}
