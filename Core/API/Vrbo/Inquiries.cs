﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Diagnostics;
using System.Linq;

namespace Core.API.Vrbo
{
    public class Inquiries
    {
        public static Inquiry InquiryForConversationId(ApplicationDbContext db, string threadId)
        {
            var entity = db.Inquiries.Where(f => f.ThreadId == threadId).FirstOrDefault();

            if (entity != null)
            {
                return entity;
            }
            return null;
        }
        public static AddInquiryResult Add(int hostId, string code, string reservationId, string status, DateTime stayStartDate, DateTime stayEndDate,
              int nights, long listingId, string guestId, string reservationCost, string cleaningFee, string serviceFee, string totalPayout, 
              DateTime inquiryDate, DateTime reservationRequestDatetime, int guestCount, int adults, int children, DateTime reservationConfirmDate, DateTime reservationCancelDate, 
              string threadId, string type
            )
        {
            using(var db = new ApplicationDbContext())
            {
                var result = new AddInquiryResult();
                var existing = InquiryForConversationId(db,threadId);
                if (existing == null)
                {
                    Inquiry inquiry = new Inquiry()
                    {
                        ReservationId = string.IsNullOrEmpty(reservationId) ? null : reservationId,
                        IsImported = true,
                        SiteType = (int)Enumerations.SiteType.VRBO,
                        BookingDate = reservationRequestDatetime == DateTime.MinValue ? (DateTime?)null : reservationRequestDatetime,
                        CleaningFee = Utilities.ExtractDecimal(cleaningFee),
                        ConfirmationCode = string.IsNullOrEmpty(code) ? null : code,
                        GuestCount = guestCount,
                        Adult = adults,
                        Children = children,
                        GuestId = Core.API.Vrbo.Guests.GetLocalGuestId(db, guestId),
                        InquiryDate = inquiryDate == DateTime.MinValue ? (DateTime?)null : inquiryDate,
                        PropertyId = listingId.ToInt(),
                        Nights = nights.ToInt(),
                        ReservationCost = Utilities.ExtractDecimal(reservationCost),
                        ServiceFee = Utilities.ExtractDecimal(serviceFee),
                        BookingStatusCode = Utilities.GetReservationStatusCode(status),
                        CheckInDate = stayStartDate == DateTime.MinValue ? (DateTime?)null : stayStartDate,
                        CheckOutDate = stayEndDate == DateTime.MinValue ? (DateTime?)null : stayEndDate,
                        TotalPayout = Utilities.ExtractDecimal(totalPayout),
                        BookingCancelDate = reservationCancelDate == DateTime.MinValue ? (DateTime?)null : reservationCancelDate,
                        BookingConfirmDate = reservationConfirmDate == DateTime.MinValue ? (DateTime?)null : reservationConfirmDate,
                        ThreadId = threadId,
                        Type = type,
                        //CompanyId = GlobalVariables.CompanyId, // Don't use SessionHandler.CompanyId here because it's not working on multi-threading
                        HostId = hostId
                    };
                    db.Inquiries.Add(inquiry);

                    try
                    {
                        if (db.SaveChanges() > 0)
                        {
                            result.IsSaved = true;
                            result.Inquiry = inquiry;
                            BookingHub.RefreshBookingList(GlobalVariables.UserId.ToString());
                        }
                        else
                        {
                            result.IsSaved = false;
                        }
                    }
                    catch(Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                    }
                }
                else
                {
                    // the next time you'll encounter a null values in db and it gives you an error in the application level,
                    // always remember that it is always an OLD RECORD, that's why you need to replace it, just like these two :
                    existing.ReservationId = string.IsNullOrEmpty(reservationId) ? null : reservationId;
                    existing.ConfirmationCode = string.IsNullOrEmpty(code) ? null : code;

                    existing.BookingDate = reservationRequestDatetime == DateTime.MinValue ? (DateTime?)null : reservationRequestDatetime;
                    existing.CleaningFee = Utilities.ExtractDecimal(cleaningFee);
                    existing.GuestCount = guestCount;
                    //existing.GuestId = guestId.ToInt();
                    existing.Nights = nights.ToInt();
                    existing.ReservationCost = Utilities.ExtractDecimal(reservationCost);
                    existing.ServiceFee = Utilities.ExtractDecimal(serviceFee);
                    existing.CheckOutDate = stayEndDate == DateTime.MinValue ? (DateTime?)null : stayEndDate;
                    existing.CheckInDate = stayStartDate == DateTime.MinValue ? (DateTime?)null : stayStartDate;
                    existing.TotalPayout = Utilities.ExtractDecimal(totalPayout);
                    existing.BookingCancelDate = reservationCancelDate == DateTime.MinValue ? (DateTime?)null : reservationCancelDate;
                    existing.BookingConfirmDate = reservationConfirmDate == DateTime.MinValue ? (DateTime?)null : reservationConfirmDate;
                    existing.ThreadId = threadId;
                    existing.BookingStatusCode = Utilities.GetReservationStatusCode(status);
                    existing.HostId = hostId;
                    existing.Type = type;
                    db.Entry(existing).State = System.Data.Entity.EntityState.Modified;

                    if (db.SaveChanges() > 0)
                    {
                        result.IsDuplicate = true;
                        result.IsSaved = true;
                        result.Inquiry = existing;
                        BookingHub.RefreshBookingList(GlobalVariables.UserId.ToString());
                    }
                }
                return result;
            }
        }
    }
    public class AddInquiryResult
    {
        public bool IsSaved { get; set; }
        public bool IsDuplicate { get; set; }
        public Inquiry Inquiry { get; set; }
    }
}
