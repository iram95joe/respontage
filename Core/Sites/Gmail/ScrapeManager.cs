﻿using HtmlAgilityPack;
using System;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Core.Sites.Gmail
{
    public class ScrapeManager
    {


        public static XmlDocument ConvertToXml(ref string html, bool dontClean = false)
        {
            try
            {

                //   var stringReader = new StringReader(html);
                //return   FromHtml(stringReader);
                if (!string.IsNullOrEmpty(html))
                {

                    //html = html.Replace("<-", "");
                    //string regEx1 = "<!--(?s:.*?)-->";
                    //html = Regex.Replace(html, regEx1, "");
                }
                HtmlDocument hd = new HtmlDocument();

                hd.LoadHtml(html);

                hd.OptionOutputAsXml = true;

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                hd.Save(sw);

                string xmlStr = sb.ToString();

                XmlDocument xd = new XmlDocument();
                //string regEx = "<!--(?s:.*?)-->";
                //xmlStr = Regex.Replace(xmlStr, regEx, "");
                xmlStr = xmlStr.Replace(" xmlns=\"https://www.w3.org/1999/xhtml\"", "");
                xmlStr = xmlStr.Replace(" xmlns=\"https://www.w3.org/1999/html\"", "");

                xmlStr = xmlStr.Replace(" xmlns=\"http://www.w3.org/1999/xhtml\"", "");
                xmlStr = xmlStr.Replace(" xmlns=\"http://www.w3.org/1999/html\"", "");

                xmlStr = xmlStr.Replace("-=\"\"", "");
                // xmlStr = xmlStr.Replace("(?i)<title[^>]*>", "");
                // xmlStr = Regex.Replace(xmlStr, "(?i)<meta[^>]*>", " ").Replace("\\s+", " ").Trim();

                //   xmlStr= xmlStr.Replace(@"</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>", "");
                //xmlStr=  Regex.Replace(xmlStr, "</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>", "");
                xmlStr = xmlStr.Replace("&AMP;", "&amp;");
                if (dontClean == false)
                {
                    xmlStr = RemoveTroublesomeCharacters(xmlStr);
                    xmlStr = CleanInvalidXmlChars(xmlStr);

                }
                xd.LoadXml(xmlStr);

                return xd;
            }
            catch (Exception e)
            {
                string t = e.Message;
                //- Responder.LogDetail(LogType.Null_XML, e.Message, html.ToHTMLEncoded());
            }

            return null;
        }
        public static string CleanInvalidXmlChars(string text)
        {
            string re = @"[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]";
            return Regex.Replace(text, re, "");
        }

        /// <summary>
        /// Removes control characters and other non-UTF-8 characters
        /// </summary>
        /// <param name="inString">The string to process</param>
        /// <returns>A string with no control characters or entities above 0x00FD</returns>
        public static string RemoveTroublesomeCharacters(string inString)
        {
            if (inString == null) return null;

            StringBuilder newString = new StringBuilder();
            char ch;

            for (int i = 0; i < inString.Length; i++)
            {

                ch = inString[i];
                // remove any characters outside the valid UTF-8 range as well as all control characters
                // except tabs and new lines
                //if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
                //if using .NET version prior to 4, use above logic
                if (XmlConvert.IsXmlChar(ch)) //this method is new in .NET 4
                {
                    newString.Append(ch);
                }
            }
            return newString.ToString();

        }
    }
}
