﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Sites.Smartthings.Models
{
    public class Location
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
