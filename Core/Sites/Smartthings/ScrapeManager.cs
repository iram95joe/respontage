﻿using BaseScrapper;
using Core.Database.Context;
using Core.Helper;
using Core.Helpers.Devices;
using Core.Sites.Smartthings.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Core.Sites.Smartthings
{
    public class ScrapeManager : CommunicationEngine
    {
        public CommunicationPage commPage = null;
        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://account.smartthings.com/login");
        }
        public void OAuthLogin()
        {
            var url = string.Format("https://graph.api.smartthings.com/oauth/authorize?response_type=code&client_id={0}&scope=app&redirect_uri={1}", GlobalVariables.SmartthingsClientId(), GlobalVariables.SmartthingsRedirectUrl());
            System.Diagnostics.Process.Start(url);
        }
        public string Authentication(string code,string email,int companyId)
        {
            //Request token from code given by Smartthings
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("grant_type", "authorization_code");
            parms.Add("client_id", GlobalVariables.SmartthingsClientId());
            parms.Add("client_secret", GlobalVariables.SmartthingsClientSecret());
            parms.Add("redirect_uri", GlobalVariables.SmartthingsRedirectUrl());
            parms.Add("code", code);
            parms.Add("scope", "app");
            commPage.Parameters = parms;
            commPage.RequestURL = "https://graph.api.smartthings.com/oauth/token";
            commPage.ReqType = CommunicationPage.RequestType.POST;
            ProcessRequest(commPage);
            var json = commPage.Html;
            var ob = JObject.Parse(json);
            var accessToken = ob["access_token"].ToString();


            //XmlDocument xd = commPage.ToXml();
            //if (xd != null)
            //{
            //    var email = xd.SelectSingleNode("//td[@class='property-value']").InnerText;
            //}
            var accountId = Devices.AddAccount(new Core.Database.Entity.DeviceAccount() { Username = email, Password = accessToken, SiteType = (int)Core.Enumerations.DevicesSiteType.Smartthings,CompanyId=companyId });

            //JM 4/22/20 Get the application URL and data
            commPage.RequestURL = "https://graph.api.smartthings.com/api/smartapps/endpoints";
            commPage.Parameters = new Dictionary<string, string>();
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("Authorization", "Bearer " + accessToken);
            commPage.ReqType = CommunicationPage.RequestType.GET;
            ProcessRequest(commPage);
            json = commPage.Html;
            var arr = JArray.Parse(json);
            var baseUrl = "";
            foreach (var array in arr)
            {
                var locationId = array["location"]["id"].ToString();
                var locationName = array["location"]["name"].ToString();
                baseUrl = array["base_url"].ToString();
                var Uri = array["uri"].ToString();
                //commPage.RequestURL = baseUrl+"/location";
                //commPage.RequestHeaders = new Dictionary<string, string>();
                //commPage.RequestHeaders.Add("Authorization", "Bearer " + accessToken);
                //commPage.ReqType = CommunicationPage.RequestType.GET;
                //ProcessRequest(commPage);

                //JM 4/22/20 Get all devices allowed by user
                commPage.RequestURL = Uri + "/devices"; //"https://api.smartthings.com/devices?locationId=" + locationId; ; //"https://api.smartthings.com/v1/locations";//baseUrl+ "/locations";/*+ "/devices";*////*baseUrl+*/"https://api.smartthings.com/devices?locationId=" + locationId;// baseUrl+" /devices?access_token=" + accessToken;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Authorization", "Bearer " + accessToken);
                ProcessRequest(commPage);
                json = commPage.Html;
                var dev = JArray.Parse(json);
                //Save parent devices or Hub location
                var localDeviceId = Devices.AddDevice(new Core.Database.Entity.Device() { DeviceId = locationId, Link = Uri, AccountId = accountId, Name = locationName });

                foreach (var device in dev)
                {///JM 4/22/20 get devices details and save
                    commPage.RequestURL = Uri + "/device/" + device["id"].ToString(); //"https://api.smartthings.com/devices?locationId=" + locationId; ; //"https://api.smartthings.com/v1/locations";//baseUrl+ "/locations";/*+ "/devices";*////*baseUrl+*/"https://api.smartthings.com/devices?locationId=" + locationId;// baseUrl+" /devices?access_token=" + accessToken;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Authorization", "Bearer " + accessToken);
                    ProcessRequest(commPage);
                    Devices.AddLock(new Core.Database.Entity.Lock() { LocalDeviceId = localDeviceId, LockId = device["id"].ToString(), Name = device["displayName"].ToString() + "(" + device["name"].ToString() + ")", IsLock = true });
                }
            }
            return accessToken;
        }
        public void LockUnlock(string Url, bool isLock, string token)
        {//JM 4/22/20 Lock or unlock devices 
            //command
            commPage.RequestURL = Url + "/command/" + (isLock ? "lock" : "unlock");
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.Parameters = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("Authorization", "Bearer " + token);
            ProcessRequest(commPage);
        }
        public void AddPincode(int pinId, string pinName, string pinCode, string Url, string token)
        {//JM 4/22/20 Add pincode to specific device
            //note that Pin Id came from use. it is not auto generated.
            commPage.RequestURL = Url + string.Format("/setpincode");
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = new Dictionary<string, string>();
            commPage.Parameters.Add("pinid", pinId.ToString());
            commPage.Parameters.Add("pincode", pinCode);
            commPage.Parameters.Add("pinname", pinName);
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("Authorization", "Bearer " + token);
            ProcessRequest(commPage);
        }
        public void DeletePincode(string pinId, string Url, string token)
        {//JM 4/22/20 Delete pincode of specific device
            commPage.RequestURL = Url + string.Format("/deletepin");
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = new Dictionary<string, string>();
            commPage.Parameters.Add("pinid", pinId.ToString());
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("Authorization", "Bearer " + token);
            ProcessRequest(commPage);
        }
    }
}
