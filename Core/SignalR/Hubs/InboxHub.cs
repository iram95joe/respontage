﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Models;
using Microsoft.AspNet.SignalR;

namespace Core.SignalR.Hubs
{
    public class InboxHub : Hub
    {
        #region RefreshBookingList

        public static void RefreshInboxList(string userId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<InboxHub>();
            var t = hubContext.Clients.User(userId);
            hubContext.Clients.User(userId).RefreshInboxList();
        }
        public static void LockInbox(string userId,LockInbox lockInbox)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<InboxHub>();
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(x => x.Id == lockInbox.UserId).FirstOrDefault();
                var usertyping = user.FirstName + " " + user.LastName + " Replying...";
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId && x.Id != id).Select(x => x.Id.ToString()).ToList();
                var t = hubContext.Clients.Users(userIds);

                hubContext.Clients.Users(userIds).LockThread(lockInbox.ThreadId,usertyping);
            }
        }
        public static void UnLockInbox(string userId, string threadId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<InboxHub>();
            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId && x.Id != id).Select(x => x.Id.ToString()).ToList();
                var t = hubContext.Clients.Users(userIds);

                hubContext.Clients.Users(userIds).UnLockThread(threadId);
            }
        }
        public static void MessageReceived(string userId, List<InboxListing> inboxes)
        {
            try
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<InboxHub>();
                //var t = hubContext.Clients.User(userId);
                //hubContext.Clients.User(userId).MessageReceived(inboxes);
                using (var db = new ApplicationDbContext())
                {
                    var id = userId.ToInt();
                    var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId && x.Id != id).Select(x => x.Id.ToString()).ToList();
                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).MessageReceived(inboxes);
                }
            }
            catch { }
        }

        #endregion

        #region
        public static void WorkerMessageReceived(string threadId ,bool isworker =true)
        {
            using (var db = new ApplicationDbContext())
            {
                var threadCompany = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                //var WorkerUser = db.Users.Where(x => x.WorkerId == threadCompany.WorkerId).FirstOrDefault();
       
                
                var users =(from u in db.Users join tr in db.tbRoleLink on u.Id equals tr.UserId where u.CompanyId ==threadCompany.CompanyId  select u).ToList();

                foreach (var user in users)
                {
                    WorkerInboxListing inbox = new WorkerInboxListing();
                      if (user.WorkerId == null)
                      {
                        var worker = (from w in db.Workers join th in db.CommunicationThreads on w.Id equals th.WorkerId where th.ThreadId == threadId select w).FirstOrDefault();
                        var vendor = (from v in db.Vendors join th in db.CommunicationThreads on v.Id equals th.VendorId where th.ThreadId == threadId select v).FirstOrDefault();
                        var thread = db.CommunicationThreads.Where(x => x.ThreadId == threadId).FirstOrDefault();
                        CommunicationInboxThread inboxThread = new CommunicationInboxThread();
                        if (isworker)
                        {
                            inboxThread.WorkerId = worker.Id;
                            inboxThread.ThreadId = threadId;
                            inboxThread.ImageUrl = string.IsNullOrEmpty(worker.ImageUrl) ? Utilities.GetDefaultAvatar(worker.Firstname) : worker.ImageUrl;
                            inboxThread.Name = worker.Firstname + " " + worker.Lastname;
                        }
                        else
                        {
                            inboxThread.WorkerId = vendor.Id;
                            inboxThread.ThreadId = threadId;
                            inboxThread.ImageUrl = Utilities.GetDefaultAvatar(vendor.Name);
                            inboxThread.Name = vendor.Name;
                        }

                        if (thread != null)
                        {
                            var lastMessage = db.CommunicationMessages.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.DateCreated).Select(x => x).FirstOrDefault();
                            inboxThread.LastMessage = lastMessage.Message;
                            inboxThread.LastMessageAt = lastMessage.DateCreated;
                        }
                        else
                        {
                            inboxThread.LastMessage = "No Message";
                            inboxThread.LastMessageAt = DateTime.Now;
                        }
                        inbox.Thread = inboxThread;
                    }
                     else{
                        var lastUserId = db.CommunicationMessages.Where(x => x.UserId != user.Id && x.ThreadId == threadId).OrderByDescending(x => x.DateCreated).Select(x => x.UserId).FirstOrDefault();
                        var lastUser = db.Users.Where(x => x.Id == lastUserId).FirstOrDefault();
                        CommunicationInboxThread inboxThread = new CommunicationInboxThread();
                        inboxThread.WorkerId = GlobalVariables.UserWorkerId;
                        inboxThread.ThreadId = threadId;
                        inboxThread.ImageUrl = string.IsNullOrEmpty(lastUser.ImageUrl) ? Utilities.GetDefaultAvatar(lastUser.FirstName) : lastUser.ImageUrl;

                        var lastMessage = db.CommunicationMessages.Where(x => x.ThreadId == threadId).OrderByDescending(x => x.DateCreated).Select(x => x).FirstOrDefault();
                        inboxThread.LastMessage = lastMessage.Message;
                        inboxThread.LastMessageAt = lastMessage.DateCreated;
                        inboxThread.Name = lastUser.FirstName + " " + lastUser.LastName;

                        inbox.Thread = inboxThread;
                    }
                    var temp = (from u in db.Users
                                join m in db.CommunicationMessages on u.Id equals m.UserId
                                join th in db.CommunicationThreads on m.ThreadId equals th.ThreadId
                                where th.ThreadId == threadId
                                select new {Message = m.Message, CreatedDate = m.DateCreated, UserId = u.Id, Name = u.FirstName + " " + u.LastName, ImageUrl = u.ImageUrl }).ToList();

                    var sms = (from u in db.Users
                               join m in db.CommunicationSMS on u.Id equals m.UserId
                               join th in db.CommunicationThreads on m.ThreadId equals th.ThreadId
                               where th.ThreadId == threadId
                               select new { Id=m.Id,ResourceSid = m.MessageSid, To = m.To, From = m.From, Message = m.Message, CreatedDate = m.CreatedDate, UserId = u.Id, Name = u.FirstName + " " + u.LastName, ImageUrl = u.ImageUrl }).ToList();

                    var call = (from u in db.Users
                                join m in db.CommunicationCalls on u.Id equals m.UserId
                                join th in db.CommunicationThreads on m.ThreadId equals th.ThreadId
                                where th.ThreadId == threadId
                                select new { CallSid = m.CallSid, ResourceSid = m.ResourceSid, To = m.To, From = m.From, RecordingUrl = m.RecordingUrl, Duration = m.CallDuration == null ? m.RecordingDuration : m.CallDuration, CreatedDate = m.CreatedDate, UserId = u.Id, Name = u.FirstName + " " + u.LastName, ImageUrl = u.ImageUrl }).ToList();

                    foreach (var item in temp)
                    {

                        CommunicationMessagesModel message = new CommunicationMessagesModel();
                        message.Message = item.Message;
                        message.IsMyMessage = (item.UserId ==user.Id? true : false);
                        message.CreatedDate = item.CreatedDate;
                        message.Name = item.Name;
                        message.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl;
                        message.Type = 0;
                        inbox.Messages.Add(message);
                    }
                    foreach (var item in sms)
                    {
                        CommunicationMessagesModel message = new CommunicationMessagesModel();
                        message.Message = item.Message;
                        message.IsMyMessage = (item.UserId == user.Id ? true : false);
                        message.CreatedDate = item.CreatedDate;
                        message.Source = message.IsMyMessage ? item.To : item.From;
                        message.Name = item.Name;
                        message.Images = db.MMSImages.Where(x => x.CommunicationSMSId == item.Id).Select(x => x.ImageUrl).ToList();
                        if (message.Source.Substring(0, 8) == "whatsapp")
                        {
                            message.Type = 3;
                        }
                        else if (message.Source.Substring(0, 9) == "messenger")
                        {
                            message.Type = 4;
                        }
                        else
                        {
                            message.Type = 1;
                        }
                        message.ResourceSid = item.ResourceSid;
                        message.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl;

                        inbox.Messages.Add(message);
                    }


                    foreach (var item in call)
                    {
                        CommunicationMessagesModel message = new CommunicationMessagesModel();

                        message.IsMyMessage = (item.UserId == user.Id ? true : false);
                        message.Source = message.IsMyMessage ? item.To : item.From;
                        message.CreatedDate = item.CreatedDate;
                        message.ResourceSid = item.CallSid;
                        message.RecordingUrl = item.RecordingUrl;
                        message.Duration = item.Duration;

                        message.Name = item.Name;
                        message.Type = 2;
                        message.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl;

                        inbox.Messages.Add(message);
                    }


                    inbox.Messages= inbox.Messages.OrderBy(x => x.CreatedDate).ToList();
                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<InboxHub>();
                    var t = hubContext.Clients.User(user.Id.ToString());
                    hubContext.Clients.User(user.Id.ToString()).WorkerMessageReceived(inbox,isworker);
                }
            }
        }
        #endregion
    }
}