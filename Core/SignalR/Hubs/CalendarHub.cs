﻿using Core.Database.Context;
using Core.Database.Entity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Core.SignalR.Hubs
{
    [HubName("calendarHub")]
    public class CalendarHub : Hub
    {
        #region ReloadCalendar

        public static void ReloadCalendar(string userId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<CalendarHub>();
            //hubContext.Clients.User(userId).ReloadCalendar();
            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                var t = hubContext.Clients.Users(userIds);
                hubContext.Clients.Users(userIds).ReloadCalendar();
            }
        }

        #endregion
    }
}