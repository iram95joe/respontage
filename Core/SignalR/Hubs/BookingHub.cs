﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Core.SignalR.Hubs
{
    public class BookingHub : Hub
    {
        #region RefreshBookingList

        public static void RefreshBookingList(string userId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<BookingHub>();
            var t = hubContext.Clients.User(userId);
            hubContext.Clients.User(userId).RefreshBookingList();
        }

        #endregion
    }
}