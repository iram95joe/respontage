﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Core.SignalR.Hubs
{
    public class PropertyHub : Hub
    {
        #region RefreshPropertyList

        public static void RefreshPropertyList(string userId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<PropertyHub>();
            var t = hubContext.Clients.User(userId);
            hubContext.Clients.User(userId).RefreshPropertyList();
        }

        #endregion
    }
}