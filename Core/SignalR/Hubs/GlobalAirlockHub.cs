﻿using Core.Helper;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Database.Context;

namespace Core.SignalR.Hubs
{
    [HubName("globalAirlockHub")]
    public class GlobalAirlockHub : Hub
    {
        public static void AirlockEnableToastr(string userId, string hostId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();
            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId && x.Id != id).Select(x => x.Id.ToString()).ToList();
                var t = hubContext.Clients.Users(userIds);
                hubContext.Clients.Users(userIds).AirlockEnableToastr(hostId);
            }
        }

        public static void ShowUserProcessAirlock(string userId, int hostId)
        {

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();

            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId && x.Id != id).Select(x => x.Id.ToString()).ToList();
                var user = db.Users.Where(x => x.Id == id).FirstOrDefault();
                var t = hubContext.Clients.Users(userIds);
                hubContext.Clients.Users(userIds).ShowUserProcessAirlock(hostId, user);
            }

        }
        public static void BlinkSecurity(int userId,int accountId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();
            using (var db = new ApplicationDbContext())
            {
                
                    var companyId = db.Users.Where(x => x.Id == userId).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();
                    var account = db.DeviceAccounts.Where(x => x.Id == accountId).FirstOrDefault();
                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).ShowBlinkSecurity(account);
               

            }
        }
        #region ShowAirlock

        public static void ShowAirlock(string userId, string json, bool IsUserShow = false)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();
            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                if (!IsUserShow)
                {
                    var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();
                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).ShowAirlock(json);
                }
                else
                {
                    var t = hubContext.Clients.User(userId);
                    hubContext.Clients.User(userId).ShowAirlock(json);
                }

            }

        }

        public static void ShowBlockUser(string userId, string BlockDetails)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();

            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                var t = hubContext.Clients.Users(userIds);
                hubContext.Clients.Users(userIds).ShowBlockUser(BlockDetails);

            }
        }
        public static void SuccessLoginRemoveToastr(string userId, int hostId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();

            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                var t = hubContext.Clients.Users(userIds);
                hubContext.Clients.Users(userIds).SuccessLoginRemoveToastr(hostId);
            }
        }
        public static void ShowLogin(string userId, string json, bool IsUserShow = false)
        {

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<GlobalAirlockHub>();

            using (var db = new ApplicationDbContext())
            {
                if (!IsUserShow)
                {
                    var id = userId.ToInt();
                    var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).ShowLogin(json);
                }
                else
                {
                    var t = hubContext.Clients.User(userId);
                    hubContext.Clients.User(userId).ShowLogin(json);
                }
            }

        }
        #endregion



    }
}
