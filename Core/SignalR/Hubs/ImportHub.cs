﻿using Core.Database.Context;
using Core.Helper;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.SignalR.Hubs
{
    [HubName("importHub")]
    public class ImportHub : Hub
    {
        #region DoneImporting

        public static void DoneImporting(string userId)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ImportHub>();
            //var t = hubContext.Clients.User(userId);
            //hubContext.Clients.User(userId).DoneImporting();
            using (var db = new ApplicationDbContext())
            {
                var id = userId.ToInt();
                var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                var t = hubContext.Clients.Users(userIds);
                hubContext.Clients.Users(userIds).DoneImporting();
            }
        }

        #endregion

        #region ShowHostName

        public static void ShowHostName(string userId, string importedHost, string hostId = "")
        {
            GlobalVariables.ImportedHostProfile = importedHost;

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ImportHub>();
            //var t = hubContext.Clients.User(userId);
            //hubContext.Clients.User(userId).ShowHostName(GlobalVariables.ImportedHostProfile, hostId);
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var id = userId.ToInt();
                    var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).ShowHostName(GlobalVariables.ImportedHostProfile, hostId);
                }catch(Exception e)
                {

                }
            }
        }

        #endregion

        #region UpdateProgress

        public static void UpdateProgress(string userId, string hostId = "")
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ImportHub>();
            //var t = hubContext.Clients.User(userId);
            //hubContext.Clients.User(userId).UpdateProgress(
            //    GlobalVariables.ImportedHostProfile,
            //    GlobalVariables.ImportedPropertyCount,
            //    GlobalVariables.ImportedMessagesCount,
            //    GlobalVariables.ImportedBookingCount,
            //    hostId);
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var id = userId.ToInt();
                    var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).UpdateProgress(
                    GlobalVariables.ImportedHostProfile,
                    GlobalVariables.ImportedPropertyCount,
                    GlobalVariables.ImportedMessagesCount,
                    GlobalVariables.ImportedBookingCount,
                    hostId);
                }catch(Exception e)
                {

                }
            }
        }

        #endregion

        #region Analytics PropertyCount
        public static void UpdateAnalyticsProgress(string userId, int count,int cityId,string location)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ImportHub>();

            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var id = userId.ToInt();
                    var companyId = db.Users.Where(x => x.Id == id).FirstOrDefault().CompanyId;
                    var userIds = db.Users.Where(x => x.CompanyId == companyId).Select(x => x.Id.ToString()).ToList();

                    var t = hubContext.Clients.Users(userIds);
                    hubContext.Clients.Users(userIds).UpdateAnalyticsProgress(count,cityId,location);
                }
                catch (Exception e)
                {

                }
            }
        }
        #endregion
    }
}