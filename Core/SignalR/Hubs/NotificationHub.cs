﻿using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.SignalR.Hubs
{
    [HubName("notificationHub")]
    public class NotificationHub : Hub
    {
        #region NotifyUser

        public static void NotifyUser(string userId, string message, Enumerations.NotificationType notificationType,Outbox outbox=null)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            var t = hubContext.Clients.User(userId);
            hubContext.Clients.User(userId).NotifyUser(message, notificationType,outbox);
        }

        #endregion

        #region
        public static void DisableBookingActions(int hostId, string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                //var userIds = db.HostCompanies.Where(x => x.HostId == hostId).Select(x => x.CompanyId.ToString()).ToList();
                //var t = hubContext.Clients.Users(userIds);
                //hubContext.Clients.Users(userIds).DisableBookingActions(threadId);
            }
        }
        #endregion
    }
}