﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Extensions
{
    public static class PropertyDBExtensions
    {
        public static string ToSiteTypeText(this int siteType)
        {
            switch (siteType)
            {
                case 0:
                    return "Local";

                case 1:
                    return "Airbnb";

                case 2:
                    return "Vrbo";

                case 3:
                    return "Booking.com";
                case 4:
                    return "Homeaway";
                default:
                    return "";
            }
        }
    }
}
