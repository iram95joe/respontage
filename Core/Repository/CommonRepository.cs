﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Core.Database.Context;
using Core.Database.Entity;
using Core.Helper;
using Core.Models;
using Core.Enumerations;
using static Core.Models.Reports.Profit.Profit;
using Core.Models.Reports;
using IncomeType = Core.Database.Entity.IncomeType;
using System.Threading;
using System.Web;
using System.IO;
using MlkPwgen;

namespace Core.Repository
{
    public class CommonRepository : ICommonRepository
    {
        public List<SubscriptionPlan> SubscriptionPlans()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.SubscriptionPlans.ToList();
            }
        }
        public List<Country> GetCountries()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Countries.ToList();
            }
        }
        public int GetIncomeTypeId(string Type)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.IncomeTypes.FirstOrDefault(x => x.TypeName.ToLower() == Type.ToLower());
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    IncomeType model = new IncomeType();
                    model.TypeName = Type;
                    db.IncomeTypes.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }
        //Add by Danial 10-3-2018

        public List<WorkerClassification> GetWorkerClassifications()
        {
            try
            {
                int sessionCompanyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    return (from d in db.WorkerClassifications
                            select d).ToList();
                }
            }
            catch (Exception) { return new List<WorkerClassification>(); }
        }
        #region Chart
        public List<PropertyRevenueMonthly> GetPropertiesListForReport(int companyId, int status, int indexPage)
        {
            using (var db = new ApplicationDbContext())
            {
                DateTime currentDate = DateTime.Now.Date;
                var minDate = (from h in db.Hosts
                               join hc in db.HostCompanies on h.Id equals hc.HostId
                               join p in db.Properties on h.Id equals p.HostId
                               where hc.CompanyId == companyId && (p.ActiveDateStart <= currentDate && p.IsActive)
                               select p.ActiveDateStart).Min();

                var maxDate = (from i in db.Inquiries join h in db.Hosts on i.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == companyId select i.CheckOutDate).Max();
                //var maxDate = (from c in db.Properties
                //               where c.CompanyId == companyId && (c.ActiveDateStart <= currentDate && c.ActiveDateEnd >= currentDate)
                //               select c.ActiveDateEnd).Max();
                int totalMonth = (Convert.ToDateTime(maxDate).Month - Convert.ToDateTime(minDate).Month) + 12 * (Convert.ToDateTime(maxDate).Year - Convert.ToDateTime(minDate).Year);

                //getting current active property and getting the array of propertyId
                var ilst = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where hc.CompanyId == companyId && (p.ActiveDateStart <= currentDate && p.ActiveDateEnd >= currentDate)
                            select new
                            {
                                PropertyId = p.ListingId,
                                PropertyName = p.Name,
                                CompanyId = companyId,
                                MonthlyExpense = p.MonthlyExpense
                            }).ToList();
                var ids = ilst.Select(x => x.PropertyId).ToArray();

                //getting total revene of all property per month.
                List<PropertyRevenueMonthly> data = new List<PropertyRevenueMonthly>();
                var InqueriesList = (from i in db.Inquiries
                                     join h in db.Hosts on i.HostId equals h.Id
                                     join hc in db.HostCompanies on h.Id equals hc.HostId
                                     where hc.CompanyId == companyId && ids.Contains(i.PropertyId) && i.BookingStatusCode == "A"
                                     select new
                                     {
                                         strDate = i.PayoutDate.Value,
                                         TotalRevenue = (status == 1 ? i.NetRevenueAmount : i.TotalPayout)
                                     }).ToList();
                int pageCounter = 0;
                if (totalMonth > 0 && ilst.Count() > 0)
                {
                    for (int i = (indexPage * 6); i < (totalMonth + 1); i++)
                    {
                        pageCounter = (pageCounter + 1);
                        decimal MonthlyExpense = ilst.Select(x => x.MonthlyExpense).Sum();
                        DateTime currentCondition = Convert.ToDateTime(minDate);
                        if (i > 0)
                        {
                            currentCondition = Convert.ToDateTime(minDate).AddMonths(i);
                        }
                        string current2 = currentCondition.Date.ToString("MM-yyyy");
                        //decimal ResultAmount = (Convert.ToDecimal(InqueriesList.Where(x => Convert.ToDateTime(x.strDate.Value).ToString("MM-yyyy") == current2 && x.PropertyId == itemP2.PropertyId).Select(x => x.TotalRevenue).Sum()) - itemP2.MonthlyExpense.Value);
                        decimal ResultAmount = (Convert.ToDecimal(InqueriesList.Where(x => Convert.ToDateTime(x.strDate).ToString("MM-yyyy") == current2).Select(x => x.TotalRevenue).Sum()) - MonthlyExpense);
                        //adding total row into list per month
                        data.Add(new PropertyRevenueMonthly { TotalMonth = (totalMonth + 1), MonthName = current2, CompanyId = 1, ResultAmount = ResultAmount, TotalExpense = MonthlyExpense });
                        if (pageCounter == 6)
                        {
                            break;
                        }
                    }
                }
                return data;
            }
        }
        public List<PropertyDetailMonths> GetTotalMonth(int companyId, int status, int indexPage)
        {
            using (var db = new ApplicationDbContext())
            {
                DateTime currentDate = DateTime.Now.Date;
                var minDate = (from h in db.Hosts
                               join hc in db.HostCompanies on h.Id equals hc.HostId
                               join p in db.Properties on h.Id equals p.HostId
                               where hc.CompanyId == companyId && (p.ActiveDateStart <= currentDate && p.IsActive)
                               select p.ActiveDateStart).Min();
                //var maxDate = (from c in db.Properties
                //               where c.CompanyId == companyId && (c.ActiveDateStart <= currentDate && c.ActiveDateEnd >= currentDate)
                //               select c.ActiveDateEnd).Max();
                var maxDate = (from i in db.Inquiries join h in db.Hosts on i.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == companyId select i.CheckOutDate).Max();
                //var minDate = new DateTime(2017, 1, 1);
                //var maxDate = new DateTime(2018, 12, 30);
                int totalMonth = (Convert.ToDateTime(maxDate).Month - Convert.ToDateTime(minDate).Month) + 12 * (Convert.ToDateTime(maxDate).Year - Convert.ToDateTime(minDate).Year);


                List<PropertyDetailMonths> data = new List<PropertyDetailMonths>();


                var ilst = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join p in db.Properties on h.Id equals p.HostId
                            where hc.CompanyId == companyId && p.IsActive
                            select new
                            {
                                PropertyId = p.ListingId,
                                PropertyName = p.Name,
                                CompanyId = companyId,
                                MonthlyExpense = p.MonthlyExpense
                            });

                var ids = ilst.Select(x => x.PropertyId).ToArray();

                var InqueriesList = (from i in db.Inquiries
                                     join h in db.Hosts on i.HostId equals h.Id
                                     join hc in db.HostCompanies on h.Id equals hc.HostId
                                     where hc.CompanyId == companyId && ids.Contains(i.PropertyId) && i.BookingStatusCode == "A" && i.PayoutDate.HasValue
                                     select new
                                     {
                                         PropertyId = i.PropertyId,
                                         strDate = i.PayoutDate,
                                         TotalRevenue = (status == 1 ? i.NetRevenueAmount : i.TotalPayout)
                                     }).ToList();

                foreach (var itemP2 in ilst)
                {
                    List<PropertyDetailRevenueMonthly> objResult = new List<PropertyDetailRevenueMonthly>();
                    int pageCounter = 0;
                    for (int i = (indexPage * 6); i < (totalMonth + 1); i++)
                    {
                        pageCounter = (pageCounter + 1);
                        DateTime currentCondition = Convert.ToDateTime(minDate);
                        if (i > 0)
                        {
                            currentCondition = Convert.ToDateTime(minDate).AddMonths(i);
                        }
                        string current2 = currentCondition.Date.ToString("MM-yyyy");
                        //adding total row into list per month
                        decimal ResultAmount = (Convert.ToDecimal(InqueriesList.Where(x => x.strDate.HasValue && Convert.ToDateTime(x.strDate.Value).ToString("MM-yyyy") == current2 && x.PropertyId == itemP2.PropertyId).Select(x => x.TotalRevenue).Sum()) - itemP2.MonthlyExpense);

                        objResult.Add(new PropertyDetailRevenueMonthly { MonthName = current2, ResultAmount = ResultAmount });
                        if (pageCounter == 6)
                        {
                            break;
                        }
                    }
                    data.Add(new PropertyDetailMonths { PropertyName = itemP2.PropertyName, TotalMonth = (totalMonth + 1), Result = objResult });
                }
                return data;
            }
        }

        public List<PropertyDetailRevenueMonthly> GetPropertiesDetailForReport(int companyId, int status, string monthName)
        {
            using (var db = new ApplicationDbContext())
            {
                //getting current active property and getting the array of propertyId
                //getting total revene of all property per month.
                List<PropertyDetailRevenueMonthly> data = new List<PropertyDetailRevenueMonthly>();
                return data;
            }
        }

        #endregion


        public bool CreateWorkerAssignmentExpense(int workerAssignmentId)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var assignment = (from a in db.WorkerAssignments
                                      join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                                      join j in db.WorkerJobs on new { a.WorkerId, a.JobTypeId } equals new { j.WorkerId, j.JobTypeId }
                                      where a.Id == workerAssignmentId
                                      select new
                                      {
                                          WorkerName = w.Firstname + " " + w.Lastname,
                                          DateAssigned = a.StartDate,
                                          a.EndDate,
                                          a.PropertyId,
                                          j.PaymentRate,
                                          j.PaymentType,
                                          w.FixedRate

                                      }).FirstOrDefault();
                        int paymentMethodId = GetWorkerDefaultPaymentMethodId();
                        decimal totalAmount = 0;

                        if (assignment.PaymentType == 1)
                        {
                            totalAmount = string.Format("{0:#,0.00}", Convert.ToDecimal(assignment.EndDate.Subtract(assignment.DateAssigned).TotalMinutes.ToInt() * (assignment.PaymentRate / 60))).ToDecimal();
                        }
                        else if (assignment.PaymentType == 2)
                        {
                            TimeSpan difference = assignment.EndDate.Date - assignment.DateAssigned.Date;
                            int numOfDays = difference.TotalDays.ToInt() == 0 ? 1 : difference.TotalDays.ToInt() > 1 ? difference.TotalDays.ToInt() : 0;
                            //td.Unit = numOfDays + " days";
                            totalAmount = string.Format("{0:#,0.00}", Convert.ToDecimal(numOfDays * assignment.PaymentRate)).ToDecimal();
                        }
                        else if (assignment.PaymentType == 3)
                        {
                            //td.Unit = "1 Appointment";
                            totalAmount = string.Format("{0:#,0.00}", Convert.ToDecimal(assignment.PaymentRate)).ToDecimal();
                        }

                    // generate expense category if not exist
                    #region Create expense category if not exist
                    int expenseCategoryId = GetExpenseCategoryId("Labor");
                        //var result = db.ExpenseCategories.Where(x => x.Type == 1 && x.ExpenseCategoryType.ToLower() == "labor").FirstOrDefault();
                        //if (result != null)
                        //{
                        //    expenseCategoryId = result.Id;
                        //}
                        //else
                        //{
                        //    ExpenseCategory model = new ExpenseCategory();
                        //    model.ExpenseCategoryType = "Labor";
                        //    model.Type = 1;
                        //    db.ExpenseCategories.Add(model);
                        //    db.SaveChanges();
                        //    expenseCategoryId = model.Id;
                        //}
                    #endregion
                    int propertyId = assignment.PropertyId;//Convert.ToInt32(Core.API.Airbnb.Properties.GetAirbnbId(assignment.PropertyId));

                        var expense = db.Expenses.Where(x => x.WorkerAssignmentId == workerAssignmentId && x.ExpenseCategoryId == expenseCategoryId).FirstOrDefault();

                    //JM 9/12/18 check if the expense is existing
                    // generate expense
                    if (expense == null && assignment != null)
                    {
                        Expense e = new Expense();
                        e.Description = assignment.WorkerName;
                        e.ExpenseCategoryId = expenseCategoryId;
                        e.PropertyId = propertyId;
                        e.DueDate = assignment.DateAssigned;
                        e.PaymentMethodId = paymentMethodId;
                        e.DateLastJobRun = null;
                        e.DateCreated = DateTime.Now;
                        e.DateNextJobRun = null;
                        e.CompanyId = SessionHandler.CompanyId;
                        //e.IsPaid = false;
                        e.Amount = totalAmount;// assignment.PaymentRate;
                        e.WorkerAssignmentId = workerAssignmentId;
                        db.Expenses.Add(e);
                        db.SaveChanges();
                        return true;
                    }
                    else if (expense != null && assignment != null)
                    {
                        expense.Amount = totalAmount;
                        expense.Description = assignment.WorkerName;
                        db.Entry(expense).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    return false;
                }
                catch { return false; }
            }
        }

        public bool DeleteWorkerAssignmentExpense(int workerAssignmentId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Expenses.Where(x => x.WorkerAssignmentId == workerAssignmentId).ToList();
                if (temp != null)
                {
                    foreach (var t in temp)
                    {
                        db.Expenses.Remove(t);
                    }
                    return db.SaveChanges() > 0;
                }
                return false;
            }
        }

        public int GetWorkerExpenseCategoryId()
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.ExpenseCategories.Where(x => x.Type == 1 && x.ExpenseCategoryType.ToLower() == "worker salary").FirstOrDefault();
                if (result != null)
                {
                    return result.Id;
                }
                else
                {
                    Database.Entity.ExpenseCategory model = new Database.Entity.ExpenseCategory();
                    model.ExpenseCategoryType = "Worker Salary";
                    model.Type = 1;
                    db.ExpenseCategories.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }
        public List<Income> GetIncomes(int propertyId,bool includeChild)
        {
            using (var db = new ApplicationDbContext())
            {
                var pIds = new List<int>();
                pIds.Add(propertyId);
                if (includeChild)
                {
                    var parentChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).FirstOrDefault();
                    if (parentChild != null)
                    {
                        pIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                    }
                }
                return db.Income.Where(x => pIds.Contains(x.PropertyId)).ToList();
            }
        }

        public List<Expense> GetExpenses(int propertyId, bool includeChild)
        {
            using (var db = new ApplicationDbContext())
            {
                var pIds = new List<int>();
                pIds.Add(propertyId);
                if (includeChild)
                {
                    var parentChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).FirstOrDefault();
                    if (parentChild != null)
                    {
                        pIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                    }
                }
                return db.Expenses.Where(x =>pIds.Contains(x.PropertyId)).ToList();
            }
        }

        public List<CashFlows> GetCashFlows(int propertyId, bool includeChild)
        {
            using (var db = new ApplicationDbContext())
            {
                var pIds = new List<int>();
                pIds.Add(propertyId);
                if (includeChild)
                {
                        var parentChild = db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).FirstOrDefault();
                        if (parentChild != null)
                        {
                            pIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                        }
                }
                return db.CashFlows.Where(x => pIds.Contains(x.PropertyId)).ToList();
            }
        }

        public IList<Worker> GetWorker()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Workers.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
            }
        }
        public TemplateMessage GetTemplate(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.TemplateMessages.Find(id);
                if (temp != null) { return temp; }
                else { return null; }
            }
        }
        public bool IsDuplicateTemplate(TemplateMessage model)
        {
            bool result = false;
            using (var db = new ApplicationDbContext())
            {
                var temp = db.TemplateMessages.FirstOrDefault(x => x.Title == model.Title && x.Id != model.Id);
                if (temp != null) { result = true; }
            }
            return result;
        }
        public bool EditTemplate(TemplateMessage model)
        {
            bool isSuccess = false;
            using (var db = new ApplicationDbContext())
            {
                var temp = db.TemplateMessages.Find(model.Id);
                if (temp != null)
                {
                    temp.Title = model.Title;
                    temp.Message = model.Message;
                    db.Entry(temp).State = EntityState.Modified;
                    isSuccess = db.SaveChanges() > 0 ? true : false;
                }
            }
            return isSuccess;
        }
        public bool DeleteTemplate(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                bool IsSuccess = false;
                var temp = db.TemplateMessages.Find(id);
                if (temp != null)
                {
                    db.TemplateMessages.Remove(temp);
                    IsSuccess = db.SaveChanges() > 0 ? true : false;
                }
                return IsSuccess;
            }
        }
        public TimeSpan GetCheckInTime(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Find(propertyId);
                if (temp != null)
                {
                    return temp.CheckInTime;
                }
                else
                {
                    return new TimeSpan(16, 0, 0);
                }
            }
        }
        public TimeSpan GetCheckOutTime(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Find(propertyId);
                if (temp != null)
                {
                    return temp.CheckOutTime;
                }
                else
                {
                    return new TimeSpan(11, 0, 0);
                }
            }
        }
        public int GetBookingExpenseCategoryId()
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ExpenseCategories.FirstOrDefault(x => x.ExpenseCategoryType.ToLower() == "booking fee");
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    Database.Entity.ExpenseCategory model = new Database.Entity.ExpenseCategory();
                    model.ExpenseCategoryType = "Booking Fee";
                    db.ExpenseCategories.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }
        public bool IsFeeTypeEqualsVat(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                bool isVat = false;
                var temp = db.FeeTypes.Find(id);
                if (temp != null)
                {
                    if (temp.Fee_Type.ToLower().Contains("vat"))
                    {
                        isVat = true;
                    }
                }
                return isVat;
            }
        }
        public int GetSalesTaxExpenseCategoryId()
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ExpenseCategories.FirstOrDefault(x => x.ExpenseCategoryType.ToLower() == "sales tax");
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    Database.Entity.ExpenseCategory model = new Database.Entity.ExpenseCategory();
                    model.ExpenseCategoryType = "Sales Tax";
                    db.ExpenseCategories.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }

        public int GetLocalBookingDefaultPaymentMethodId()
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.PaymentMethods.FirstOrDefault(x => x.Name.ToLower() == "bank deposit");
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    PaymentMethod model = new PaymentMethod();
                    model.Name = "Bank Deposit";
                    db.PaymentMethods.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }

        public int GetWorkerDefaultPaymentMethodId()
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.PaymentMethods.FirstOrDefault(x => x.Name.ToLower() == "bank deposit");
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    PaymentMethod model = new PaymentMethod();
                    model.Name = "Bank Deposit";
                    db.PaymentMethods.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }

        //public bool GetAirbnbPaymentTransaction(List<TransactionHistory> transaction, int companyId)
        //{
        //    bool IsSuccess = true;
        //    var reservations = transaction.Where(x => x.Type == "Reservation" && x.Reservations.Count == 1).ToList();
        //    transaction = transaction.Where(x => x.Type == "Payout" && x.Reservations.Count > 0).ToList();
        //    using (var db = new ApplicationDbContext())
        //    {
        //        try
        //        {
        //            var bookingList = db.Inquiries.Where(x => x.CompanyId == companyId).ToList();
        //            var incomeList = db.Income.Where(x => x.CompanyId == companyId).ToList();
        //            var expenseList = db.Expenses.Where(x => x.CompanyId == companyId).ToList();
        //            foreach (var item in transaction)
        //            {
        //                foreach(var reservation in item.Reservations)
        //                {
        //                    var booking = bookingList.FirstOrDefault(x => x.ReservationId == reservation.ReservationId);
        //                    if (booking != null)
        //                    {
        //                        var bookingId = booking.Id;
        //                        var expense = expenseList.FirstOrDefault(x => x.BookingId == bookingId);
        //                        if (expense != null)
        //                        {
        //                            expense.IsPaid = item.ReleaseDate.HasValue;
        //                            expense.PaymentDate = item.ReleaseDate.ToDateTime();
        //                            db.Entry(expense).State = EntityState.Modified;
        //                        }
        //                        var income = incomeList.FirstOrDefault(x => x.BookingId == bookingId);
        //                        if(income != null)
        //                        {
        //                            var res = reservations.Where(x => x.Reservations.FirstOrDefault().ReservationId == reservation.ReservationId).FirstOrDefault();
        //                            if(res != null) { income.Amount = res.Amount; }
        //                            income.Status = 2;
        //                            income.PaymentDate = item.ReleaseDate.ToDateTime();
        //                            db.Entry(income).State = EntityState.Modified;
        //                        }
        //                    }
        //                }
        //            }
        //            db.BulkSaveChanges();
        //        }
        //        catch { IsSuccess = false; }
        //    }
        //    return IsSuccess;
        //}

        public long GetHostIdFromAirbnb(string token)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.HostSessionCookies.Join(db.Hosts, x => x.HostId, y => y.Id, (x, y) => new { HostSession = x, Host = y }).FirstOrDefault(x => x.HostSession.Token == token && x.HostSession.IsActive);
                if (temp != null)
                {
                    return temp.HostSession.HostId;
                }
                else
                {
                    return 0;
                }
            }
        }

        public bool IsPaymentMethodExist(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.PaymentMethods.FirstOrDefault(x => x.Id == id);
                return temp == null ? false : true;
            }
        }

        public bool IsPropertyExist(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.FirstOrDefault(x => x.ListingId == id);
                return temp == null ? false : true;
            }
        }
        public bool IsFeeTypeExist(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.FeeTypes.FirstOrDefault(x => x.Id == id);
                return temp == null ? false : true;
            }
        }
        public bool IsBookingExist(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = (from h in db.Hosts
                            join hc in db.HostCompanies on h.Id equals hc.HostId
                            join i in db.Inquiries on h.Id equals i.HostId
                            where hc.CompanyId == SessionHandler.CompanyId
                            select i).ToList();
                return temp == null ? false : true;
            }
        }
        public bool IsExpenseCategoryExist(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ExpenseCategories.FirstOrDefault(x => x.Id == id && x.CompanyId == SessionHandler.CompanyId);
                return temp == null ? false : true;
            }
        }

        public int GetAirbnbIncomeTypeId()
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.IncomeTypes.FirstOrDefault(x => x.TypeName.ToLower() == "booking");
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    Database.Entity.IncomeType model = new Database.Entity.IncomeType();
                    model.TypeName = "Booking";
                    db.IncomeTypes.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }

        public int GetAirbnbFeeId()
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.FeeTypes.FirstOrDefault(x => x.Fee_Type.ToLower() == "airbnb fee");
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    FeeType ft = new FeeType();
                    ft.Fee_Type = "Airbnb Fee";
                    db.FeeTypes.Add(ft);
                    db.SaveChanges();
                    return ft.Id;
                }
            }
        }
        public bool UpdateFeeType(FeeType model)
        {
            bool success = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var record = db.FeeTypes.FirstOrDefault(x => x.Id == model.Id);
                    if (record != null)
                    {
                        record.Fee_Type = model.Fee_Type;
                        record.Type = model.Type;
                        db.Entry(record).State = EntityState.Modified;
                        success = db.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch { }
            return success;
        }

        public bool DeleteFeeType(int id)
        {
            bool success = false;
            try
            {
                int companyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    var currentRow = db.FeeTypes.FirstOrDefault(x => x.Id == id && x.CompanyId == companyId);
                    if (currentRow != null)
                    {
                        db.FeeTypes.Remove(currentRow);
                        success = db.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch { }
            return success;
        }
        public bool InsertFeeType(FeeType model)
        {
            bool success = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.FeeTypes.Add(model);
                    success = db.SaveChanges() > 0 ? true : false;
                }
            }
            catch (Exception e)
            {

            }
            return success;
        }
        public int EditExpense(Expense model, HttpPostedFileWrapper[] Images)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var temp = db.Expenses.SingleOrDefault(x => x.Id == model.Id);
                    temp.Description = model.Description;
                    temp.ExpenseCategoryId = model.ExpenseCategoryId;
                    temp.PropertyId = model.PropertyId;
                    temp.Amount = model.Amount;
                    temp.DueDate = model.DueDate;
                    temp.PaymentMethodId = model.PaymentMethodId;
                    temp.Frequency = model.Frequency;
                    temp.IsStartupCost = model.IsStartupCost;
                    temp.IsCapitalExpense = model.IsCapitalExpense;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            db.ExpenseFiles.Add(new ExpenseFile() { ExpenseId = temp.Id, FileUrl = UploadImage(image, "~/Images/Expense/") });
                            db.SaveChanges();
                        }
                    }
                    return temp.Id;
                
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                    return 0;
                }
            }
        }
        public List<PaymentMethod> GetPaymentMethods()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.PaymentMethods.Where(x=>x.CompanyId == SessionHandler.CompanyId || x.Type ==0).ToList();
            }
        }
        public List<Database.Entity.ExpenseCategory> GetExpenseCategoryList()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.ExpenseCategories.Where(x => x.CompanyId == SessionHandler.CompanyId || x.Type==2).ToList();
            }
        }

        public string GetDashboardMessageHtml(List<GroupMessage> category)
        {
            string temp = "";
            if (category.Count == 0)
            {
                temp = "<tr><td rowspan=\"2\">No messages. </td></tr>";
            }
            category.ForEach(item =>
            {
                temp +=
                    ("<tr style=\"cursor: pointer\" class=\"dashboard-message\" data-host-id=\"" + item.HostId + "\" data-thread-id=\"" + item.ThreadId + "\">" +
                    "<td>" +
                        "<div class=\"comment\">" +
                            "<a class=\"avatar center aligned\">" +
                                "<img src=\"" + item.GuestPicture + "\">" +
                            "</a>" +
                            "<div class=\"content\">" +
                                "<a class=\"author\">" + item.GuestName + "</a>" +
                                "<div class=\"metadata\">" +
                                "<span class=\"date\">" + (item.StartStayDate == null ? "" : item.StartStayDate.ToDateTime().ToString("dd/MM/yyyy")) + "</span>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</td>" +
                    "<td colspan=2 style=\"vertical-align: bottom\">" +
                        "<a>" +
                            item.MessageContent +
                        "</a>" +
                        "<br>" +
                        (
                            (((DateTime.Now - Convert.ToDateTime(item.CreatedDate)).TotalMinutes <= 30) && item.IsIncoming) ?
                            "<i class=\"wait icon green\"></i>" + item.CreatedDate + "" :
                            (((DateTime.Now - Convert.ToDateTime(item.CreatedDate)).TotalMinutes <= 60) && ((DateTime.Now - Convert.ToDateTime(item.CreatedDate)).TotalMinutes > 30) && item.IsIncoming) ?
                            "<i class=\"wait icon yellow\"></i>" + item.CreatedDate + "" :
                            (((DateTime.Now - Convert.ToDateTime(item.CreatedDate)).TotalMinutes > 60) && item.IsIncoming) ?
                            "<i class=\"wait icon red\"></i>" + item.CreatedDate + "" :
                            ""
                        ) +
                    "</td>" +
                "</tr>");
            });
            return temp;
        }

        public Database.Entity.ExpenseCategory FindExpenseCategoryById(int id)
        {
            try
            {
                int companyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    return db.ExpenseCategories.FirstOrDefault(x => x.Id == id && x.CompanyId == companyId);
                }
            }
            catch { }
            return null;
        }

        public bool UpdateExpenseCategory(Database.Entity.ExpenseCategory model)
        {
            bool success = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var record = db.ExpenseCategories.FirstOrDefault(x => x.Id == model.Id);
                    if (record != null)
                    {
                        record.ExpenseCategoryType = model.ExpenseCategoryType;
                        record.CompanyTerm = model.CompanyTerm;
                        db.Entry(record).State = EntityState.Modified;
                        success = db.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch { }
            return success;
        }

        public bool DeleteExpenseCategory(int id)
        {
            bool success = false;
            try
            {
                int companyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    var currentRow = db.ExpenseCategories.FirstOrDefault(x => x.Id == id && x.CompanyId == companyId);
                    if (currentRow != null)
                    {
                        db.ExpenseCategories.Remove(currentRow);
                        success = db.SaveChanges() > 0 ? true : false;
                    }
                }
            }
            catch { }
            return success;
        }

        public bool InsertExpenseCategory(Database.Entity.ExpenseCategory model)
        {
            bool success = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.ExpenseCategories.Add(model);
                    success = db.SaveChanges() > 0 ? true : false;
                }
            }
            catch { }
            return success;
        }

        public List<Database.Entity.ExpenseCategory> GetAllExpenseCategory(int status, string textSearch)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                if (status == 0)
                {
                    return db.ExpenseCategories.Where(x => (x.CompanyId == 0 || x.CompanyId == companyId) && x.ExpenseCategoryType.Contains(textSearch)).ToList();
                }
                else
                {
                    return db.ExpenseCategories.Where(x => (x.CompanyId == 0 || x.CompanyId == companyId) && x.Type == status && x.ExpenseCategoryType.Contains(textSearch)).ToList();
                }
            }
        }

        public List<Core.Database.Entity.SiteType> GetAllSiteType()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.SiteType.ToList();
            }
        }

        public List<BookingStatus> GetAllBookingStatus()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from b in db.BookingStatus select b).ToList();
            }
        }

        public decimal ExtractDecimal(string input)
        {
            decimal result = 0;
            decimal.TryParse(Regex.Match(input, @"\d+").Value, out result);
            return result;
        }

        public Guest AddGuest(Guest g)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Guests.Add(g);
                    if (db.SaveChanges() > 0)
                    {
                        return g;
                    }
                }
            }
            catch (Exception e) { }
            return null;
        }

        public bool AddIncome(Income income, HttpPostedFileWrapper[] Images =null)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    db.Income.Add(income);
                    db.SaveChanges();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            db.IncomeFiles.Add(new IncomeFile() { IncomeId = income.Id, FileUrl = UploadImage(image, "~/Images/Income/") });
                            db.SaveChanges();
                        }
                    }
                    if (income.Status == 2)
                    {
                        IncomePayment objIncomePayment = new IncomePayment();
                        objIncomePayment.Amount = income.Amount.ToDecimal();
                        objIncomePayment.CreatedAt = DateTime.UtcNow;
                        objIncomePayment.Description = income.Description;
                        objIncomePayment.Type = (int)Core.Enumerations.IncomePaymentType.Payment;//(int)IncomePaymentType.Payment;
                        objIncomePayment.IncomeId = income.Id;
                        db.IncomePayment.Add(objIncomePayment);
                        db.SaveChanges();
                     
                    }
                    return true;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                }
                return false;
            }
        }
        string UploadImage(HttpPostedFileWrapper file, string Destination)
        {
            //var file = Model.Tasks[0].Image;
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(file.FileName);
            string url = Destination + PasswordGenerator.Generate(10) + fileExtension;
            file.SaveAs(System.Web.HttpContext.Current.Server.MapPath(url));
            return url.Replace("~", "");

        }
        public bool EditIncome(Income income,HttpPostedFileWrapper[] Images =null)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var temp = db.Income.SingleOrDefault(x => x.Id == income.Id);
                    temp.PropertyId = income.PropertyId;
                    temp.IncomeTypeId = income.IncomeTypeId;
                    temp.Description = income.Description;
                    temp.Amount = income.Amount;
                    temp.DueDate = income.DueDate;
                    temp.Status = income.Status;
                    db.Entry(temp).State = EntityState.Modified;
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            db.IncomeFiles.Add(new IncomeFile() { IncomeId = temp.Id, FileUrl = UploadImage(image, "~/Images/Income/") });
                            db.SaveChanges();
                        }
                    }
                    if (income.Status == 2)
                    {
                        IncomePayment objIncomePayment = new IncomePayment();
                        objIncomePayment.Amount = income.Amount.ToDecimal();
                        objIncomePayment.CreatedAt = DateTime.UtcNow;
                        objIncomePayment.Description = income.Description;
                        objIncomePayment.Type = (int)Core.Enumerations.IncomePaymentType.Payment;//(int)IncomePaymentType.Payment;
                        objIncomePayment.IncomeId = income.Id;
                        db.IncomePayment.Add(objIncomePayment);
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                }
                return false;
            }
        }

        public bool DeleteIncome(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var temp = db.Income.SingleOrDefault(x => x.Id == id);
                    db.Income.Remove(temp);
                    return db.SaveChanges() > 0 ? true : false;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                }
                return false;
            }
        }

        public bool AddExpense(int bookingId, int propertyId, string description, decimal value, int feeTypeId, bool isPreduducted, bool isPaid, string paymentDate, DateTime? dateCreated, DateTime? checkInDate)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    DateTime pd;
                    DateTime.TryParse(paymentDate, out pd);
                    int paymentMethodId = GetLocalBookingDefaultPaymentMethodId();
                    int expenseCategoryId = 0;
                    if (IsFeeTypeEqualsVat(feeTypeId)) { expenseCategoryId = GetSalesTaxExpenseCategoryId(); }
                    else { expenseCategoryId = GetBookingExpenseCategoryId(); }
                    Expense e = new Expense();
                    e.Description = description;
                    e.PropertyId = propertyId;
                    e.BookingId = bookingId;
                    e.Amount = value;
                    e.PaymentMethodId = paymentMethodId;
                    e.FeeTypeId = feeTypeId;
                    if (checkInDate.HasValue) { checkInDate.Value.AddDays(1); }
                    e.DateCreated = dateCreated.HasValue ? dateCreated.ToDateTime() : DateTime.Now;
                    e.IsPreDeducted = isPreduducted;
                    //e.IsPaid = isPaid;
                    e.ExpenseCategoryId = expenseCategoryId;
                    //if(!string.IsNullOrEmpty(paymentDate) && isPaid)
                    //{
                    //    e.PaymentDate = pd;
                    //}
                    e.CompanyId = SessionHandler.CompanyId;
                    db.Expenses.Add(e);
                    return db.SaveChanges() > 0 ? true : false;
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
                return false;
            }
        }

        public bool HasNewMessage()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from h in db.Hosts
                        join hc in db.HostCompanies on h.Id equals hc.HostId
                        join i in db.Inbox on h.Id equals i.HostId
                        where hc.CompanyId == SessionHandler.CompanyId && i.HasUnread == true
                        select i).ToList().Count > 0;
            }
        }

        public IList<InboxMessage> GetNewInbox()
        {
            using (var db = new ApplicationDbContext())
            {
                List<InboxMessage> i = new List<InboxMessage>();
                var inboxes = (from h in db.Hosts
                               join hc in db.HostCompanies on h.Id equals hc.HostId
                               join inbox in db.Inbox on h.Id equals inbox.HostId
                               where hc.CompanyId == SessionHandler.CompanyId && inbox.HasUnread == true
                               select inbox).ToList();
                foreach (var inbox in inboxes)
                {
                    InboxMessage model = new InboxMessage();
                    model.Inbox = inbox;
                    model.Message = db.Messages.Where(m => m.ThreadId == inbox.ThreadId).OrderBy(x => x.CreatedDate).ToList();
                    i.Add(model);
                }
                return i;
            }
        }

        public IList<Message> GetMessageThread(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                //return (from message in db.Messages where message.ThreadId == threadId select message).OrderBy(x => x.CreatedDate).ToList();

                var messages = db.Messages.Where(x => x.ThreadId == threadId).OrderBy(x => x.CreatedDate).ToList();
                var inbox = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                var property = db.Properties.Where(x => x.ListingId == inbox.PropertyId).FirstOrDefault();
                if (property.TimeZoneName != null)
                {
                    foreach (var message in messages)
                    {
                        message.CreatedDate = TimeZoneInfo.ConvertTimeFromUtc(message.CreatedDate, TimeZoneInfo.FindSystemTimeZoneById(property.TimeZoneName));
                    }
                }
                return messages;
            }
        }

        public MessageThreadDetail GetThreadDetail(string threadId, long inquiryId = 0)
        {
            //if (GlobalVariables.UserWorkerId == 0)
            //{
            //    inquiryId = 0;
            //}GetThreadDetail

            if (threadId == "0") return new MessageThreadDetail();

            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var threadDetail = (from i in db.Inbox
                                            //join h in db.Hosts on i.HostId equals h.Id
                                        join p in db.Properties on i.PropertyId equals p.ListingId
                                        //join g in db.Guests on i.GuestId equals g.Id
                                        where i.ThreadId == threadId.ToString()
                                        select new MessageThreadDetail
                                        {
                                            Nights =i.Nights,
                                            NightsFee =i.NightsFee,
                                            NightsFeeSummary = i.NightsFeeSummary,
                                            LocalPropertyId = p.Id,
                                            PropertyId = p.ListingId,
                                            PropertyName = p.Name,
                                            GuestId = db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault() != null ? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().Id : 0,
                                            GuestName = db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault() != null ? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().Name : db.Renters.Where(x => x.BookingId == db.Inquiries.Where(y=>y.ThreadId==i.ThreadId).FirstOrDefault().Id).Select( x=> new { Name = x.Firstname + " " + x.Lastname }).FirstOrDefault().Name,
                                            GuestProfilePictureUrl = db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault() != null ? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().ProfilePictureUrl: null,//g.ProfilePictureUrl,
                                            GuestCount = i.GuestCount,
                                            CheckInDate = i.CheckInDate,//CheckInDate = (p.TimeZoneName!=null?TimeZoneInfo.ConvertTimeFromUtc(i.CheckInDate.ToDateTime(),TimeZoneInfo.FindSystemTimeZoneById(Core.API.Airbnb.Properties.TimeZome(p.TimeZoneName))):i.CheckInDate.ToDateTime()),
                                            CheckOutDate = i.CheckOutDate,//CheckOutDate = (p.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc(i.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(Core.API.Airbnb.Properties.TimeZome(p.TimeZoneName))) : i.CheckOutDate.ToDateTime()),
                                            Status = i.Status,
                                            IsSpecialOfferSent = i.IsSpecialOfferSent,
                                            CanPreApproveInquiry = i.CanPreApproveInquiry,
                                            CanWithdrawPreApprovalInquiry = i.CanWithdrawPreApprovalInquiry,
                                            HostId = i.HostId,
                                            HostName = (db.Hosts.Where(x => x.Id == i.HostId).FirstOrDefault()!=null? db.Hosts.Where(x => x.Id == i.HostId).FirstOrDefault().Firstname+" " +db.Hosts.Where(x => x.Id == i.HostId).FirstOrDefault().Lastname:""),// h.Firstname + " " + h.Lastname,
                                            HostPicture = (db.Hosts.Where(x => x.Id == i.HostId).FirstOrDefault() != null ? db.Hosts.Where(x => x.Id == i.HostId).FirstOrDefault().ProfilePictureUrl : "/Content/img/matthew.png"),// h.ProfilePictureUrl,
                                            ReservationCost = i.CleaningFee + i.RentalFee,
                                            CleaningCost = i.CleaningFee,
                                            AirbnbFee = ((i.CleaningFee + i.RentalFee) - i.TotalPayout),
                                            Payout = i.TotalPayout,
                                            SiteType = i.SiteType,
                                            PropertyAddress = p.Address,
                                            Email = db.GuestEmails.Where(x => x.GuestId == db.Guests.Where(y=>y.Id==i.GuestId).FirstOrDefault().GuestId).FirstOrDefault() != null ? db.GuestEmails.Where(x => x.GuestId == db.Guests.Where(y => y.Id == i.GuestId).FirstOrDefault().GuestId).FirstOrDefault().Email: db.Renters.Where(x => x.BookingId == db.Inquiries.Where(y => y.ThreadId == i.ThreadId).FirstOrDefault().Id).FirstOrDefault().Email,
                                            PhoneNumber = db.GuestContactInfos.Where(x => x.GuestId ==i.GuestId).FirstOrDefault() != null? db.GuestContactInfos.Where(x => x.GuestId == i.GuestId).FirstOrDefault().Value : db.Renters.Where(x => x.BookingId == db.Inquiries.Where(y => y.ThreadId == i.ThreadId).FirstOrDefault().Id).FirstOrDefault().PhoneNumber,
                                            GuestFullName = db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault()!=null? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().FullName : db.Renters.Where(x => x.BookingId == db.Inquiries.Where(y => y.ThreadId == i.ThreadId).FirstOrDefault().Id).Select(x => new { Name = x.Firstname + " " + x.Lastname }).FirstOrDefault().Name,
                                            TimeZoneId = p.TimeZoneId,
                                            UserWorkerId = GlobalVariables.UserWorkerId,
                                            Source = i.Source,
                                            InquiryDate = i.InquiryDate.Value,
                                            Location = db.Guests.Where(x=>x.Id==i.GuestId).FirstOrDefault() !=null? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().Location:"",
                                            Rating =db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault() != null ? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().Rating : "",
                                            JoinDate = db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault() != null ? db.Guests.Where(x => x.Id == i.GuestId).FirstOrDefault().JoinDate : ""
                                        }).FirstOrDefault();

                    if (threadDetail != null)
                    {
                        threadDetail.Inquiries = ((from inq in db.Inquiries where inq.ThreadId == threadId.ToString() && ((inq.BookingStatusCode == "B" && inq.IsActive)) select inq).Count() > 0 ?
                                                    (from inq in db.Inquiries
                                                     join p in db.Properties on inq.PropertyId equals p.ListingId
                                                     where inq.ThreadId == threadId.ToString() && ((inq.BookingStatusCode == "B" && inq.IsActive))
                                                     select new ThreadInquiry
                                                     {
                                                         Id = inq.Id,
                                                         InquiryDate = inq.InquiryDate.Value,
                                                         ConfirmationDate = inq.BookingConfirmDate.Value,
                                                         BookingCancelDate = inq.BookingCancelDate,
                                                         BookingConfirmCancelDate = inq.BookingConfirmCancelDate,
                                                         CheckInDate = inq.CheckInDate,
                                                         CheckOutDate = inq.CheckOutDate,
                                                         ConfirmationCode = inq.ConfirmationCode,
                                                         InquiryId = inq.ReservationId,
                                                         GuestId = threadDetail.GuestId,
                                                         GuestCount = inq.GuestCount,
                                                         Status = inq.BookingStatusCode,
                                                         IsActive = inq.IsActive,
                                                         IsAltered = inq.IsAltered,
                                                         LocalPropertyId = threadDetail.LocalPropertyId,
                                                         PropertyId = p.ListingId,
                                                         PropertyName = p.Name,
                                                         ReservationCost = inq.ReservationCost,
                                                         SumPerNight = inq.SumPerNightAmount,
                                                         CleaningCost = inq.CleaningFee,
                                                         AirbnbFee = inq.ServiceFee,
                                                         FeesSerializedJson = inq.Fees,
                                                         RateDetailsSerializedJson = inq.RateDetails,
                                                         Payout = inq.TotalPayout,
                                                         SiteType = inq.SiteType,
                                                         PropertyAddress = threadDetail.PropertyAddress,
                                                         PhoneNumber = threadDetail.PhoneNumber,
                                                         Email = threadDetail.Email,
                                                         TimeZoneId = threadDetail.TimeZoneId,
                                                         Source = inq.Source,
                                                         Location = threadDetail.Location,
                                                         Nights = threadDetail.Nights,
                                                         NightsFee = threadDetail.NightsFee,
                                                         NightsFeeSummary = threadDetail.NightsFeeSummary,
                                                     }).ToList() : ((from inq in db.Inquiries where inq.ThreadId == threadId.ToString() select inq).Count() == 0) ?
                                                     (new List<ThreadInquiry>() {
                                                         new ThreadInquiry {

                                                             CheckInDate = threadDetail.CheckInDate,
                                                             CheckOutDate = threadDetail.CheckOutDate,
                                                             GuestCount = threadDetail.GuestCount,
                                                             Status = threadDetail.Status,
                                                             LocalPropertyId = threadDetail.LocalPropertyId,
                                                             PropertyId = threadDetail.PropertyId,
                                                             PropertyName = threadDetail.PropertyName,
                                                             IsSpecialOfferSent = threadDetail.IsSpecialOfferSent,
                                                             ReservationCost = threadDetail.ReservationCost,
                                                             CleaningCost = threadDetail.CleaningCost,
                                                             AirbnbFee = threadDetail.AirbnbFee,
                                                             Payout = threadDetail.Payout,
                                                             CanPreApproveInquiry = threadDetail.CanPreApproveInquiry,
                                                             CanWithdrawPreApprovalInquiry = threadDetail.CanWithdrawPreApprovalInquiry,
                                                             GuestId = threadDetail.GuestId,
                                                             SiteType = threadDetail.SiteType,
                                                             PropertyAddress = threadDetail.PropertyAddress,
                                                             PhoneNumber = threadDetail.PhoneNumber,
                                                             Email = threadDetail.Email,
                                                             TimeZoneId = threadDetail.TimeZoneId,
                                                             Source = threadDetail.Source,
                                                             InquiryDate = threadDetail.InquiryDate,
                                                             Location = threadDetail.Location,
                                                             Nights =threadDetail.Nights,
                                                              NightsFee =threadDetail.NightsFee,
                                                              NightsFeeSummary = threadDetail.NightsFeeSummary,

                                                         }
                                                     }) :
                                                    ((from inq in db.Inquiries
                                                      join p in db.Properties on inq.PropertyId equals p.ListingId
                                                      where inq.ThreadId == threadId.ToString() && (inquiryId == 0 ? true : inq.Id == inquiryId)
                                                      select new ThreadInquiry
                                                      {
                                                          Id = inq.Id,
                                                          InquiryDate = inq.InquiryDate.Value,
                                                          ConfirmationDate = inq.BookingConfirmDate.Value,
                                                          BookingCancelDate = inq.BookingCancelDate,
                                                          BookingConfirmCancelDate = inq.BookingConfirmCancelDate,
                                                          CheckInDate = inq.CheckInDate,
                                                          CheckOutDate = inq.CheckOutDate,
                                                          ConfirmationCode = inq.ConfirmationCode,
                                                          InquiryId = inq.ReservationId,
                                                          GuestId = threadDetail.GuestId,
                                                          GuestCount = inq.GuestCount,
                                                          Status = inq.BookingStatusCode,
                                                          IsActive = inq.IsActive,
                                                          IsAltered = inq.IsAltered,
                                                          PropertyId = p.ListingId,
                                                          LocalPropertyId = threadDetail.LocalPropertyId,
                                                          PropertyName = p.Name,
                                                          ReservationCost = inq.ReservationCost,
                                                          SumPerNight = inq.SumPerNightAmount,
                                                          CleaningCost = inq.CleaningFee,
                                                          AirbnbFee = inq.ServiceFee,
                                                          FeesSerializedJson = inq.Fees,
                                                          RateDetailsSerializedJson = inq.RateDetails,
                                                          Payout = inq.TotalPayout,
                                                          SiteType = inq.SiteType,
                                                          PropertyAddress = threadDetail.PropertyAddress,
                                                          PhoneNumber = threadDetail.PhoneNumber,
                                                          Email = threadDetail.Email,
                                                          TimeZoneId = threadDetail.TimeZoneId,
                                                          Source = inq.Source,
                                                          Location = threadDetail.Location,
                                                          Nights = inq.Nights,
                                                          NightsFee = inq.NightsFee,
                                                          NightsFeeSummary = inq.NightsFeeSummary,
                                                      }).ToList()));
                        if (threadDetail.SiteType == 3)
                        {
                            threadDetail.Inquiries[0].Fees =
                                Newtonsoft.Json.JsonConvert.DeserializeObject<List<Core.Models.ReservationFee>>
                                (threadDetail.Inquiries[0].FeesSerializedJson);

                            threadDetail.Inquiries[0].RateDetails =
                                Newtonsoft.Json.JsonConvert.DeserializeObject<List<Core.Models.ReservationRates>>
                                (threadDetail.Inquiries[0].RateDetailsSerializedJson);
                        }
                        for (int x = 0; x < threadDetail.Inquiries.Count; x++)
                        {
                            threadDetail.Inquiries[x].PaymentHistory =
                            Core.API.Booking.com.Inquiries.GetPaymentHistory(threadDetail.Inquiries[x].ConfirmationCode);
                          
                            threadDetail.Inquiries[x].Incomes = Core.API.AllSite.Inquiries.GetIncomes(threadDetail.Inquiries[x].Id) ;
                        }
                        threadDetail.Inquiries[0].Email = Core.API.Booking.com.Guests.GetEmailByLocalId(threadDetail.Inquiries[0].GuestId.ToInt());
                        threadDetail.Inquiries[0].AlterHistory = Core.API.AllSite.Inquiries.GetAlterHistory(threadId);
                        threadDetail.Inquiries = threadDetail.Inquiries.OrderBy(x => x.CheckInDate).Reverse().ToList();

                        foreach (var item in threadDetail.Inquiries)
                        {
                            if (item.CheckInDate != null && item.CheckOutDate != null)
                            {
                                var p = db.Properties.Where(x => x.ListingId == item.PropertyId).FirstOrDefault();
                                item.CheckInDate = (p.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc(item.CheckInDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(p.TimeZoneName)) : item.CheckInDate.ToDateTime());
                                item.CheckOutDate = (p.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc(item.CheckOutDate.ToDateTime(), TimeZoneInfo.FindSystemTimeZoneById(p.TimeZoneName)) : item.CheckOutDate.ToDateTime());
                            }
                            item.HasWorkerAssignment = item.Id==0?false: db.WorkerAssignments.Where(x => x.ReservationId == item.Id).Any();
                            item.HasTravelEvent = item.Id ==0?false:db.tbNote.Where(x => x.ReservationId == item.Id).Any();
                        }
                        //threadDetail.CheckInDate = null;
                        //threadDetail.CheckOutDate = null;
                        //threadDetail.GuestCount = 0;
                        //threadDetail.Status = "";
                        //threadDetail.GuestId = 0;
                        //threadDetail.LocalPropertyId = 0;
                        threadDetail.SiteBaseUrl = System.Web.HttpContext.Current.Request.Url.Authority;
                    }
                    threadDetail.Inquiries = threadDetail.Inquiries.OrderBy(x => x.Id != inquiryId).ToList();
                    threadDetail.HostPicture = string.IsNullOrEmpty(threadDetail.HostPicture) ? Utilities.GetDefaultAvatar(threadDetail.HostName) : threadDetail.HostPicture;
                    threadDetail.GuestProfilePictureUrl =string.IsNullOrEmpty(threadDetail.GuestProfilePictureUrl) ? Utilities.GetDefaultAvatar(threadDetail.GuestName) : threadDetail.GuestProfilePictureUrl;
                    return threadDetail;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.StackTrace + " " + e.ToString());
                    return new MessageThreadDetail();
                }
            }
        }



        public List<PropertyType> GetAllPropertyType()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from p in db.PropertyType select p).ToList();
            }
        }

        public List<FeeType> GetAllFeeType()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.FeeTypes.Where(x => x.CompanyId == SessionHandler.CompanyId || x.CompanyId == 0).ToList();
            }
        }

        public void UpdateInboxStatus(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.Inbox.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (result != null)
                {
                    result.HasUnread = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public User SaveUser(User objtbUser)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Users.Add(objtbUser);
                db.SaveChanges();
                //setting company id for main account
                if (objtbUser.CompanyId == 0)
                {
                    var results = (from d in db.Users
                                   where d.Id == objtbUser.Id
                                   select d).FirstOrDefault();
                    if (results != null)
                    {
                        results.CompanyId = objtbUser.Id;
                    }
                    db.SaveChanges();
                }
                return objtbUser;
            }
        }


        public LoginUser Login(User objtbUser)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from c in db.Users
                        where c.UserName.Trim().ToLower() == objtbUser.UserName.Trim().ToLower()
                        && c.Password == objtbUser.Password && c.Status == true
                        select new LoginUser
                        {
                            CompanyId = c.CompanyId,
                            FirstName = c.FirstName,
                            LastName = c.LastName,
                            UserName = c.UserName,
                            ValidStatus = c.ValidStatus,
                            ConfirmEmail = c.ConfirmEmail,
                            Status = c.Status,
                            Id = c.Id,
                            WorkerId = c.WorkerId,
                            RoleName = (from cr in db.tbRole
                                        join r in db.tbRoleLink
                                        on cr.Id equals r.RoleId
                                        where r.UserId == c.Id
                                        select cr.RoleName).FirstOrDefault().ToString(),
                        }).FirstOrDefault();
            }
        }

        public User ValidUser(string username)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from c in db.Users
                        where c.UserName.Trim().ToLower() == username.Trim().ToLower()
                        select c).FirstOrDefault();
            }
        }

        public User GetUser(int intUserId)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return (from c in db.Users
                        where c.Id == intUserId && c.CompanyId == companyId
                        select c).FirstOrDefault();
            }
        }

        public List<tbRoleLink> GetUserRoleList(int intUserId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from c in db.tbRoleLink
                        where c.UserId == intUserId
                        select c).ToList();
            }
        }

        public void UpdateValidStatus(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var results = (from d in db.Users
                               where d.Id == id
                               select d).FirstOrDefault();
                if (results != null)
                {
                    results.ValidStatus = true;
                }
                db.SaveChanges();
            }
        }
        public int GetBookingExpenseCategoryId(string name)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ExpenseCategories.FirstOrDefault(x => x.ExpenseCategoryType.ToLower() == name.ToLower());
                if (temp != null)
                {
                    return temp.Id;
                }
                else
                {
                    Database.Entity.ExpenseCategory model = new Database.Entity.ExpenseCategory();
                    model.ExpenseCategoryType = name;
                    db.ExpenseCategories.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }
        public bool UpdateUser(User objUser)
        {
            using (var db = new ApplicationDbContext())
            {
                var results = (from d in db.Users
                               where d.Id == objUser.Id
                               select d).FirstOrDefault();
                if (results != null)
                {
                    results.FirstName = objUser.FirstName;
                    results.LastName = objUser.LastName;
                    results.UserName = objUser.UserName;
                    results.TimeZone = objUser.TimeZone;
                    results.ImageUrl = objUser.ImageUrl;
                    if (objUser.Password != "")
                    {
                        results.Password = objUser.Password;
                    }
                    db.Entry(results).State = System.Data.Entity.EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public void SaveRoleLink(int roleId, int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                tbRoleLink objRole = new tbRoleLink();
                objRole.RoleId = roleId;
                objRole.UserId = userId;
                db.tbRoleLink.Add(objRole);
                db.SaveChanges();
            }
        }

        public List<GroupMessage> DashboardMessages(string status)
        {
            using (var db = new ApplicationDbContext())
            {
                List<GroupMessage> temp = new List<GroupMessage>();
                var sessionCompanyId = SessionHandler.CompanyId;

                var tempresult = db.Inbox
                   .Join(db.Guests, x => x.GuestId, y => y.Id, (x, y) => new { Inbox = x, Guest = y })
                   .Join(db.Properties, x => x.Inbox.PropertyId, y => y.ListingId, (x, y) => new { Property = y, Inbox = x.Inbox, Guest = x.Guest })
                   .Join(db.HostCompanies, x => x.Property.HostId, y => y.HostId, (x, y) => new { HostCompany = y, Property = x.Property, Inbox = x.Inbox, Guest = x.Guest })
                   .Where(x => x.HostCompany.CompanyId == sessionCompanyId && x.Inbox.IsEnd == false)
                   .Where(x => x.Inbox.Status == status || (status == "I" ? (x.Inbox.Status == "NP") : false))
                   .OrderByDescending(x => x.Inbox.LastMessageAt)
                   .Take(5)
                   .ToList();
                var result = tempresult;

                for (int i = 0; i < tempresult.Count; i++)
                {
                    var threadId = tempresult[i].Inbox.ThreadId;
                    
                    var ismyMessage = (from m in db.Messages where m.ThreadId == threadId orderby m.CreatedDate descending select m.IsMyMessage).FirstOrDefault();
                    if (ismyMessage)
                    {
                        result.RemoveAt(i);
                        i = i - 1;
                    }
                }

                result.ForEach(x =>
                {
                    GroupMessage gm = new GroupMessage();
                    gm.GuestId = x.Guest.GuestId;
                    gm.GuestName = x.Guest.Name;
                    gm.GuestPicture = x.Guest.ProfilePictureUrl ==null ? Core.Helper.Utilities.GetDefaultAvatar(x.Guest.Name) : x.Guest.ProfilePictureUrl;
                    gm.Status = x.Inbox.Status;
                    gm.ThreadId = x.Inbox.ThreadId;
                    var lastMessage = db.Messages.Where(r => r.ThreadId == x.Inbox.ThreadId).OrderByDescending(r => r.CreatedDate).FirstOrDefault();
                    if (lastMessage != null)
                    {
                        gm.IsIncoming = !lastMessage.IsMyMessage;
                    }
                    gm.HostId = x.Inbox.HostId;
                    gm.MessageContent = x.Inbox.LastMessage;
                    gm.CreatedDate = Core.Helper.DateTimeZone.ConvertToUTC((DateTime)x.Inbox.LastMessageAt, x.Inbox.PropertyId);// x.Inbox.LastMessageAt;

                    gm.ReadStatus = x.Inbox.HasUnread;
                    var inquiry = db.Inquiries.Where(a => a.ThreadId == x.Inbox.ThreadId).FirstOrDefault();
                    if (inquiry != null)
                    {
                        try { gm.StartStayDate = inquiry.CheckInDate; }
                        catch { gm.StartStayDate = null; }

                        try
                        {
                            gm.IsActive = inquiry.IsActive;
                            gm.IsAltered = inquiry.IsAltered;
                            gm.ReservationCode = inquiry.ConfirmationCode;

                        }
                        catch { }
                    }
                    temp.Add(gm);
                });
                return temp;
            }
        }

        public bool DeleteUser(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;

                var user = db.Users.Where(x => x.Id == id).FirstOrDefault();
                var worker = db.Workers.Where(x => x.Id == user.WorkerId).FirstOrDefault();
                if (worker != null)
                {
                    var workerAssignment = db.WorkerAssignments.Where(x => x.WorkerId == worker.Id).ToList();
                    if (workerAssignment.Count > 0)
                    {
                        return false;
                    }
                    else
                    {

                        if (worker != null)
                        {
                            db.Workers.Remove(worker);
                        }
                        var workerJobs = db.WorkerJobs.Where(x => x.WorkerId == worker.Id).ToList();
                        if (workerJobs.Count > 0)
                        {
                            db.WorkerJobs.RemoveRange(workerJobs);
                        }
                        var workerSchedules = db.WorkerSchedules.Where(x => x.WorkerId == worker.Id).ToList();
                        if (workerSchedules.Count > 0)
                        {
                            db.WorkerSchedules.RemoveRange(workerSchedules);
                        }
                        if (user != null)
                        {
                            db.Users.Remove(user);
                        }

                    }
                }
                else
                {
                    if (user != null)
                    {
                        db.Users.Remove(user);
                    }
                }

                return db.SaveChanges() > 0;
            }
        }

        public void DeleteUserRole(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var query = db.tbRoleLink.Where(x => x.UserId == id).ToList();
                foreach (var item in query)
                {
                    db.tbRoleLink.Remove(item);
                }
                db.SaveChanges();
            }
        }

        public int GetCompanyid(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.Where(x => x.Id == id).Select(x => x.CompanyId).FirstOrDefault();
            }
        }

        public List<User> GetSubUser(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = GetCompanyid(userId);
                return db.Users.Where(x => x.CompanyId == companyId && x.Id != userId).ToList();
            }
        }

        public bool BlockUser(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                var results = (from d in db.Users
                               where d.Id == id && d.CompanyId == companyId
                               select d).FirstOrDefault();
                if (results != null)
                {
                    results.Status = false;
                    db.Entry(results).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool UnBlockUser(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                var results = (from d in db.Users
                               where d.Id == id && d.CompanyId == companyId
                               select d).FirstOrDefault();
                if (results != null)
                {
                    results.Status = true;
                    db.Entry(results).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool ChangePassword(int userId, string oldPassword, string newPassword)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                var result = (from c in db.Users
                              where c.Id == userId && c.Password == oldPassword && c.CompanyId == companyId
                              select c).FirstOrDefault();
                if (result != null)
                {
                    result.Password = newPassword;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static bool GetStatusRole(int userId, int roleId)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = (from c in db.tbRoleLink
                              where c.UserId == userId && c.RoleId == roleId
                              select c).FirstOrDefault();
                if (result != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static User GetCurrentUserDetail(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.Where(x => x.Id == userId).FirstOrDefault();
            }
        }
        public List<Database.Entity.CompanyType> CompanyTypes()
        {
            using(var db = new ApplicationDbContext())
            {
                return db.CompanyTypes.ToList();
            }
        }
        public List<TemplateMessage> GetTemplateMessages()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.TemplateMessages.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
            }
        }
        public List<WorkerTemplateMessage> GetWorkerTemplateMessages()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.WorkerTemplateMessages.Where(x => x.CompanyId == SessionHandler.CompanyId).ToList();
            }
        }
        #region "Following method are used for security panel"

        //it is used to check the valid open page
        public bool CheckPageRole(int userId, string pageName)
        {
            using (var db = new ApplicationDbContext())
            {
                List<int> ilst = (from d in db.tbRolePage
                                  where d.PageName.ToLower().Trim() == pageName.ToLower().Trim() && d.Status == true
                                  select d.RoleId).ToList();

                var result = (from rT in db.tbRoleLink
                              where ilst.Contains(rT.RoleId) && rT.UserId == userId
                              select rT).ToList();
                if (result.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //it is used to check the valid request
        public bool CheckValidRequest(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                List<int> obj = new List<int>();
                obj.Add(1);
                obj.Add(2);
                obj.Add(3);
                obj.Add(4);
                obj.Add(5);
                List<int> ilst = (from d in db.tbRoleLink
                                  where d.UserId == userId
                                  select d.RoleId).ToList();
                var result = (from rT in db.tbRoleLink
                              join d in db.Users on rT.UserId equals d.Id
                              where rT.UserId == userId && obj.Contains(rT.RoleId)
                              select rT.RoleId).ToList();
                if (result.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool CheckValidCompany(int userId, int companyId)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = (from d in db.Users
                              where d.Id == userId && d.CompanyId == companyId
                              select d.Id).ToList();
                if (result.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        #region "Add Booking"

        public int AddBooking(Inquiry iq)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Inquiries.Add(iq);
                    return db.SaveChanges() > 0 ? iq.Id : 0;
                }
            }
            catch (Exception e) { }
            return 0;
        }

        public int UpdateBooking(int GuestId, int GuestCount, int Property, string CheckinDate, string CheckinTime, string CheckoutDate, string CheckoutTime, string InquiryPrice, string PayoutAmt, string Payoutdate, string Bookeddate, string Bookedtime, string Inquirydate, string Status, int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                int compnayId = Convert.ToInt32(SessionHandler.CompanyId);
                int res = db.SaveChanges();
                return res;
            }
        }
        public int DeleteBooking(int Id)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyid = Convert.ToInt32(SessionHandler.CompanyId);
                db.Inquiries.Remove((from h in db.Hosts
                                     join hc in db.HostCompanies on h.Id equals hc.HostId
                                     join i in db.Inquiries on h.Id equals i.HostId
                                     where hc.CompanyId == SessionHandler.CompanyId && i.Id == Id
                                     select i).FirstOrDefault());
                db.Income.RemoveRange(db.Income.Where(x => x.BookingId == Id));
                db.Expenses.RemoveRange(db.Expenses.Where(x => x.BookingId == Id));
                return db.SaveChanges();
            }
        }

        public List<Income> GetIncomeInfo(int propertyId, bool isPaidOnly, bool includeChild)
        {

            //var value = isPaidOnly ? 0 : 1;
            using (var db = new ApplicationDbContext())
            {
                var propertyIds = new List<int>();
                if (includeChild)
                {
                    propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                }
                propertyIds.Add(propertyId);
                List<Income> resultList = new List<Income>();
                var temps = db.Income.Where(x =>
                 (propertyIds.Contains(0) ?true : propertyIds.Contains(x.PropertyId))).ToList();
                if (isPaidOnly)
                {
                    foreach (var temp in temps)
                    {
                        var payments = db.IncomePayment.Where(x => x.IncomeId == temp.Id && x.IsRejected == false).ToList();
                        if (payments.Sum(x => x.Amount) > 0.0M)
                        {
                            resultList.Add(temp);
                        }
                    }
                }
                else
                {
                    resultList = temps;
                }
                return resultList;

            }
        }
        public List<Expense> GetExpenseInfo(int propertyId, bool isPaidOnly, bool includeChild)
        {
            using (var db = new ApplicationDbContext())
            {
                var propertyIds = new List<int>();
                if (includeChild)
                {
                    propertyIds.AddRange(db.ParentChildProperties.Where(x => x.ParentPropertyId == propertyId).Select(x => x.ChildPropertyId).ToList());
                }
                propertyIds.Add(propertyId);
                List<Expense> resultList = new List<Expense>();
                var temps = db.Expenses.Where(x =>(propertyIds.Contains(0) ? true : propertyIds.Contains(x.PropertyId))).ToList();

                if (isPaidOnly)
                {
                    foreach (var temp in temps)
                    {
                        var payments = db.ExpensePayment.Where(x => x.ExpenseId == temp.Id).ToList();
                        if (payments.Sum(x => x.Amount) > 0.0M)
                        {
                            resultList.Add(temp);
                        }
                    }
                }
                else
                {
                    resultList = temps;
                }

                return resultList;


            }
        }

        public enum Role
        {
            Admin = 1,
            GuestManager = 2,
            Account = 3
        };
        #endregion

        #region "saving income"
        public int Save(Income entity)
        {
            using (var db = new ApplicationDbContext())
            {
                if (entity.Id == 0)
                {
                    Income objIncome = new Income();
                    db.Income.Add(entity);
                    db.SaveChanges();
                    return entity.Id;
                }
                else
                {
                    db.Entry(entity).State = EntityState.Modified;
                    db.SaveChanges();
                    return entity.Id;
                }
            }
        }
        #endregion

        #region "Expense code"

        public List<Expense> GetAllExpense()
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                return db.Expenses.Where(x => x.CompanyId == companyId).ToList();
            }
        }
        public int InsertExpense(Expense model, HttpPostedFileWrapper[] Images)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Expenses.Add(model);
                    db.SaveChanges();
                    if (Images != null)
                    {
                        foreach (var image in Images)
                        {
                            db.ExpenseFiles.Add(new ExpenseFile() { ExpenseId = model.Id, FileUrl = UploadImage(image, "~/Images/Expense/") });
                            db.SaveChanges();
                        }
                    }
                    if (model.IsPaid)
                    {
                        ExpensePayment e = new ExpensePayment();
                        e.ExpenseId = model.Id;
                        e.Description = model.Description;
                        e.Amount = model.Amount.ToDecimal();
                        e.Type = model.PaymentMethodId;
                        e.CreatedAt = model.PaymentDate.ToDateTime();
                        db.ExpensePayment.Add(e);
                        db.SaveChanges();
                    }
                    return model.Id;
                }
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteExpense(int id)
        {
            bool success = false;
            try
            {
                int companyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    var recordToDelete = db.Expenses.FirstOrDefault(x => x.Id == id && x.CompanyId == companyId);
                    if (recordToDelete != null)
                    {
                        db.Expenses.Remove(recordToDelete);
                    }
                    success = db.SaveChanges() > 0 ? true : false;
                }
            }
            catch { }
            return success;
        }

        public Expense FindExpensesById(int expenseId)
        {
            try
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                using (var db = new ApplicationDbContext())
                {
                    return db.Expenses.FirstOrDefault(x => x.Id == expenseId && x.CompanyId == companyId);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return new Expense();

        }

        public bool UpdateExpenses(Expense expenseModel)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                    var record = db.Expenses.FirstOrDefault(x => x.Id == expenseModel.Id && x.CompanyId == companyId);
                    if (record != null)
                    {
                        record.Amount = expenseModel.Amount;
                        record.Description = expenseModel.Description;
                        record.ExpenseCategoryId = expenseModel.ExpenseCategoryId;
                        record.DueDate = expenseModel.DueDate;
                        //record.PaymentDate = expenseModel.PaymentDate;
                        record.IsCapitalExpense = expenseModel.IsCapitalExpense;
                        record.IsStartupCost = expenseModel.IsStartupCost;
                        record.Frequency = expenseModel.Frequency;
                        //record.IsPaid = expenseModel.IsPaid;
                        record.PropertyId = expenseModel.PropertyId;
                        if (record.Frequency == (int)ExpenseFrequency.Monthly)
                        {
                            record.DueDateDMn = expenseModel.DueDateDMn;
                        }
                        else if (record.Frequency == (int)ExpenseFrequency.Weekly)
                        {
                            record.DueDateDQt = expenseModel.DueDateDQt;
                            record.DueDateMQt = expenseModel.DueDateMQt;
                        }
                        else if (record.Frequency == (int)ExpenseFrequency.Daily)
                        {
                            record.DueDateDQt = expenseModel.DueDateDQt;
                            record.DueDateMQt = expenseModel.DueDateMQt;
                        }
                        else if (record.Frequency == (int)ExpenseFrequency.Quarterly)
                        {
                            record.DueDateDQt = expenseModel.DueDateDQt;
                            record.DueDateMQt = expenseModel.DueDateMQt;
                        }
                        else if (record.Frequency == (int)ExpenseFrequency.Yearly)
                        {
                            record.DueDateDYr = expenseModel.DueDateDYr;
                            record.DueDateMYr = expenseModel.DueDateMYr;
                        }
                        db.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }
        #endregion

        #region "Code For Cashflow"

        public List<CashFlows> GetAllCashFlowRecord()
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                return new List<CashFlows>(db.CashFlows.Where(x => x.CompanyId == companyId).ToList());
            }
        }

        public string GetPropertyNameById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                return (from h in db.Hosts
                        join hc in db.HostCompanies on h.Id equals hc.HostId
                        join p in db.Properties on h.Id equals p.HostId
                        where hc.CompanyId == companyId
                        select p.Name).FirstOrDefault();//db.Properties.Where(x => x.Id == id && x.CompanyId == companyId).Select(x => x.Name).FirstOrDefault();
            }
        }

        public string GetCashFlowTypeNameById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.CashFlowTypes.FirstOrDefault(x => x.Id == id).CashFlowTypeName;
            }
        }

        public string GetCompanyNameById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Companies.FirstOrDefault(x => x.Id == id).CompanyName;
            }
        }

        public int GetCompanyId(string companyName)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Companies.FirstOrDefault(x => x.CompanyName.Equals(companyName)).Id;
            }
        }

        public CashFlows GetCashFlowById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return db.CashFlows.FirstOrDefault(x => x.Id == id && x.CompanyId == companyId);
            }
        }

        public bool CreateNewCashFlow(CashFlows newRecord)
        {
            var status = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    newRecord.CompanyId = SessionHandler.CompanyId;
                    db.CashFlows.Add(newRecord);
                    db.SaveChanges();
                    status = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return status;
        }

        public bool DeleteCashFlowById(int cashFlowId)
        {
            var status = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    int companyId = SessionHandler.CompanyId;
                    var record = db.CashFlows.FirstOrDefault(x => x.Id == cashFlowId && x.CompanyId == companyId);
                    db.CashFlows.Attach(record);
                    db.CashFlows.Remove(record);
                    db.SaveChanges();
                    status = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return status;
        }

        public bool UpdateCashFlowBy(CashFlows updateRecord)
        {
            var status = false;
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    int companyId = Convert.ToInt32(SessionHandler.CompanyId);
                    var record = db.CashFlows.FirstOrDefault(x => x.Id == updateRecord.Id && x.CompanyId == companyId);
                    if (record != null)
                    {
                        record.Amount = updateRecord.Amount;
                        record.CashFlowTypeId = updateRecord.CashFlowTypeId;
                        record.CompanyId = SessionHandler.CompanyId;
                        record.PaymentDate = updateRecord.PaymentDate;
                        record.PropertyId = updateRecord.PropertyId;
                        db.SaveChanges();
                        status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return status;
        }

        public List<Company> GetAllCompany()
        {
            using (var db = new ApplicationDbContext())
            {
                return new List<Company>(db.Companies);
            }
        }

        public List<CashFlowType> GetAllCashFlowType()
        {
            using (var db = new ApplicationDbContext())
            {
                return new List<CashFlowType>(db.CashFlowTypes);
            }
        }

        public List<Property> GetAllProperty()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where hc.CompanyId == SessionHandler.CompanyId select p).ToList();
            }
        }
        public List<Property> GetNotParentProperty()
        {
            using (var db = new ApplicationDbContext())
            {
                var list = new List<Property>();
                var childs = (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId join p in db.Properties on h.Id equals p.HostId where p.IsParentProperty == false && hc.CompanyId == SessionHandler.CompanyId select p).ToList();
                foreach (var c in childs)
                {
                    var temp = db.ParentChildProperties.Where(x => x.ChildPropertyId == c.Id && x.CompanyId == SessionHandler.CompanyId && x.ParentType == (int)Core.Enumerations.ParentPropertyType.Sync).FirstOrDefault();
                    if (temp == null)
                        list.Add(c);
                }
                list.AddRange(db.Properties.Where(x => x.SiteType == 0 && x.IsParentProperty == false && x.CompanyId == SessionHandler.CompanyId).ToList());
                return list;
            }
        }


        public List<CashFlows> GetPureCashFlowRecord()
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return db.CashFlows.Where(x => x.CompanyId == companyId).ToList();
            }
        }

        #endregion

        #region "Code for worker"

        public int AddWorker(Worker model)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Workers.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
            catch { return 0; }
        }

        public bool UpdateWorker(Worker worker, List<WorkerJob> workerJobs)
        {
            using (var db = new ApplicationDbContext())
            {
                var results = db.Workers.Where(x => x.Id == worker.Id).Where(x => x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                if (results != null)
                {
                    results.Firstname = worker.Firstname;
                    results.Lastname = worker.Lastname;
                    results.Bonus = worker.Bonus;
                    results.Mobile = worker.Mobile;
                    results.Address = worker.Address;
                    results.FixedRate = worker.FixedRate;
                    results.Frequency = worker.Frequency;
                    results.RetainerRate = worker.RetainerRate;
                    results.RetainerFrequency = worker.RetainerFrequency;
                    results.ImageUrl = worker.ImageUrl;
                }
                db.WorkerJobs.RemoveRange(db.WorkerJobs.Where(x => x.WorkerId == worker.Id).ToList());
                workerJobs.ForEach(
                    item => {
                        item.WorkerId = results.Id; db.WorkerJobs.Add(item);
                    });
                return db.SaveChanges() > 0;
            }
        }

        public void DeleteWorker(int workerId)
        {
            int companyId = SessionHandler.CompanyId;
            using (var db = new ApplicationDbContext())
            {
                var worker = db.Workers.Where(x => x.Id == workerId).Where(x => x.CompanyId == companyId).FirstOrDefault();
                if (worker != null)
                {
                    db.Workers.Remove(worker);
                }
                var workerJobs = db.WorkerJobs.Where(x => x.WorkerId == workerId).ToList();
                if (workerJobs.Count > 0)
                {
                    db.WorkerJobs.RemoveRange(workerJobs);
                }
                var workerSchedules = db.WorkerSchedules.Where(x => x.WorkerId == workerId).ToList();
                if (workerSchedules.Count > 0)
                {
                    db.WorkerSchedules.RemoveRange(workerSchedules);
                }
                var workerAssignment = db.WorkerAssignments.Where(x => x.WorkerId == workerId).ToList();
                if (workerAssignment.Count > 0)
                {
                    db.WorkerAssignments.RemoveRange(workerAssignment);
                }
                var user = db.Users.Where(x => x.WorkerId == workerId).FirstOrDefault();
                if (user != null)
                {
                    db.Users.Remove(user);
                }
                db.SaveChanges();
            }
        }

        public IList<WorkerJobType> GetWorkerJobTypes()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.WorkerJobTypes.Where(x => x.CompanyId == 0 || x.CompanyId == SessionHandler.CompanyId).ToList();
                }
            }
            catch (Exception) { return new List<WorkerJobType>(); }
        }

        public IList<WorkerJob> GetWorkerJobs(int workerId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.WorkerJobs.Where(x => x.WorkerId == workerId).ToList();
                }
            }
            catch (Exception) { return new List<WorkerJob>(); }
        }

        public IList<WorkerJobAssignment> GetWorkerJobAssignments(int workerId)
        {
            IList<WorkerJobAssignment> workerJobAssignments = new List<WorkerJobAssignment>();

            using (var db = new ApplicationDbContext())
            {
                var jobs = db.WorkerJobs.Where(x => x.WorkerId == workerId).ToList();
                //var assignment = db.WorkerAssignments.Where(x => x.WorkerId == workerId).ToList();
                foreach (var job in jobs)
                {
                    WorkerJobAssignment workerAssignment = new WorkerJobAssignment()
                    {
                        WorkerJob = job,
                        Count = db.WorkerAssignments.Where(x => x.WorkerId == workerId && x.JobTypeId == job.JobTypeId).ToList().Count
                    };

                    workerJobAssignments.Add(workerAssignment);
                }
                return workerJobAssignments;

            }

        }
        //public IList<WorkerAssignment> GetWorkerAssignments(int workerId)
        //{
        //    using(var db = new ApplicationDbContext())
        //    {
        //        return db.WorkerAssignments.Where(x => x.WorkerId == workerId).ToList();
        //    }
        //}

        public IList<Worker> GetWorkers()
        {
            try
            {
                int sessionCompanyId = SessionHandler.CompanyId;
                using (var db = new ApplicationDbContext())
                {
                    return (from d in db.Workers
                            where d.CompanyId == sessionCompanyId
                            select d).ToList();
                }
            }
            catch (Exception e) { return new List<Worker>(); }
        }

        public IList<Worker> GetWorkers(int WorkerTypeId, string search)
        {
            search = search == "" ? "" : search;

            int companyId = Convert.ToInt32(SessionHandler.CompanyId);
            if (WorkerTypeId > 0)
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from d in db.Workers
                            join jt in db.WorkerJobs on d.Id equals jt.WorkerId
                            where jt.JobTypeId == WorkerTypeId && d.CompanyId == companyId && (d.Firstname.Contains(search) || d.Lastname.Contains(search))
                            select d).ToList();

                }
            }
            else
            {
                using (var db = new ApplicationDbContext())
                {
                    return (from d in db.Workers
                            where d.CompanyId == companyId && (d.Firstname.Contains(search) || d.Lastname.Contains(search))
                            select d).ToList();
                }
            }
        }

        public Worker GetWorker(int intWorkerId)
        {
            int companyId = Convert.ToInt32(SessionHandler.CompanyId);
            using (var db = new ApplicationDbContext())
            {
                return (from d in db.Workers
                        where d.Id == intWorkerId && d.CompanyId == companyId
                        select d).FirstOrDefault();
            }
        }

        public int AddSchedule(WorkerSchedule objSchedule)
        {
            using (var db = new ApplicationDbContext())
            {
                db.WorkerSchedules.Add(objSchedule);
                db.SaveChanges();
                return objSchedule.Id;
            }
        }
        public void UpdateSchedule(int intScheduleId, WorkerSchedule objSchedule)
        {
            using (var db = new ApplicationDbContext())
            {
                var results = (from d in db.WorkerSchedules
                               where (d.Id == intScheduleId)
                               select d).FirstOrDefault();
                if (results != null)
                {
                    results.UniqueId = objSchedule.UniqueId;
                    results.WorkerId = objSchedule.WorkerId;
                    results.AvailDay = objSchedule.AvailDay;
                    results.AvailTimeStart = objSchedule.AvailTimeStart;
                    results.AvailTimeEnd = objSchedule.AvailTimeEnd;
                    results.StartDate = objSchedule.StartDate;
                    results.EndDate = objSchedule.EndDate;
                    results.Status = objSchedule.Status;
                    results.CompanyId = objSchedule.CompanyId;
                }
                db.SaveChanges();
            }
        }
        public void DeleteSchedule(int intScheduleId)
        {
            using (var db = new ApplicationDbContext())
            {
                var results = (from d in db.WorkerSchedules
                               where (d.Id == intScheduleId)
                               select d).FirstOrDefault();
                if (results != null)
                {
                    db.WorkerSchedules.Remove(results);
                }
                db.SaveChanges();
            }
        }

        public IList<WorkerSchedule> GetSchedules(int WorkerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from d in db.WorkerSchedules
                        where d.WorkerId == WorkerId
                        select d).ToList();
            }
        }

        public WorkerSchedule GetSchedule(int intScheduleId)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from d in db.WorkerSchedules
                        where d.Id == intScheduleId
                        select d).FirstOrDefault();
            }
        }
        #endregion

        #region PROPERTIES
        //9/11/18 JM applied Get only Active property
        public IList<Property> GetProperties()
        {
            using (var db = new ApplicationDbContext())
            {
                var properties = new List<Property>();
                int sessionCompanyId = SessionHandler.CompanyId;
                var tempp = (from h in db.Hosts
                             join hc in db.HostCompanies on h.Id equals hc.HostId
                             join p in db.Properties on h.Id equals p.HostId
                             where hc.CompanyId == sessionCompanyId && p.IsActive
                             select p).ToList();
                properties.AddRange(tempp);
                properties.AddRange(db.Properties.Where(x => x.CompanyId == sessionCompanyId && (x.ParentType != (int)Core.Enumerations.ParentPropertyType.Accounts || x.IsParentProperty == false)).ToList());
                return properties;
            }
        }

        public Property GetPropertyRecord(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                int sessionCompanyId = SessionHandler.CompanyId;
                return (from h in db.Hosts
                        join hc in db.HostCompanies on h.Id equals hc.HostId
                        join p in db.Properties on h.Id equals p.HostId
                        where hc.CompanyId == sessionCompanyId
                        && p.ListingId == propertyId
                        select p).FirstOrDefault();
            }
        }
        #endregion      
        public List<Inquiry> GetBookingsByProperty(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return
                (from h in db.Hosts
                 join hc in db.HostCompanies on h.Id equals hc.HostId
                 join i in db.Inquiries on h.Id equals i.HostId
                 where hc.CompanyId == SessionHandler.CompanyId && i.PropertyId == propertyId && i.BookingStatusCode == "A"
                 select i).ToList();
                //db.Inquiries.Where(
                //        x =>
                //            x.CompanyId == companyId && x.PropertyId == propertyId && x.BookingStatusCode =="A").ToList();
            }
        }


        #region TaskTemplate

        public List<TaskTemplate> GetAllTaskTemplate()
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;
                return db.TaskTemplates.Where(x => x.CompanyId == companyId).ToList();
            }
        }

        #endregion

        #region HashDictionary

        public HashDictionary GetHashInfo(string hashValue)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.HashDictionaries.FirstOrDefault(x => x.HashValue.Equals(hashValue));
            }
        }

        public bool IsHashValueExpired(HashDictionary hashObject)
        {
            //Currently is comparing in UTC 0 timezone (Store in DB also in UTC 0 timezone)
            return hashObject.ExpiryDateTime <= DateTime.UtcNow;
        }

        #endregion

        #region TravelTypes

        public List<TravelType> GetTravelTypeList(TravelTypeEnum travelType)
        {
            using (var db = new ApplicationDbContext())
            {
                if (travelType == TravelTypeEnum.ARRIVAL)
                {
                    return new List<TravelType>(db.TravelTypes.Where(x => x.ForArrival));
                }

                if (travelType == TravelTypeEnum.DEPARTURE)
                {
                    return new List<TravelType>(db.TravelTypes.Where(x => x.ForDeparture));
                }
            }
            return null;
        }

        #endregion

        public List<Note> GetAllTravelEvents(int inquiryId)
        {
            using (var db = new ApplicationDbContext())
            {
                return new List<Note>(db.tbNote.Where(x => x.ReservationId == inquiryId &&
                    x.NoteType != 1 && x.NoteType != 4));
            }
        }

        public Note GetTravelEventRecord(int recordId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.tbNote.FirstOrDefault(x => x.Id == recordId);
            }
        }

        public List<WorkerAssignment> GetAvailableWorkerList(string inquiryId, string dateSearch, int workerCategory, int workerAssignmentType)
        {
            int companyId = Convert.ToInt32(SessionHandler.CompanyId);
            var dateCriteria = Convert.ToDateTime(dateSearch);
            List<WorkerAssignment> workerList = new List<WorkerAssignment>();

            using (var db = new ApplicationDbContext())
            {
                if (workerAssignmentType == (int)Core.Enumerations.Worker.WorkerAssignmentType.PRE_CHECK_IN)
                {
                    workerList = (from d in db.WorkerAssignments
                                  where d.ReservationId == inquiryId.ToInt()
                                        //&& d.WorkerCategory == workerCategory
                                        && d.StartDate < dateCriteria
                                        && d.CompanyId == companyId
                                  select d).ToList();
                }

                if (workerAssignmentType == (int)Core.Enumerations.Worker.WorkerAssignmentType.POST_CHECK_OUT)
                {
                    workerList = (from d in db.WorkerAssignments
                                  where d.ReservationId == inquiryId.ToInt()
                                        //&& d.WorkerCategory == workerCategory
                                        && d.StartDate >= dateCriteria
                                        && d.CompanyId == companyId
                                  select d).ToList();
                }
            }

            return workerList;
        }
        public int GetExpenseCategoryId(string name)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.ExpenseCategories.Where(x => x.Type == 2 && x.ExpenseCategoryType.ToLower() == name.ToLower()).FirstOrDefault();
                if (result != null)
                {
                    return result.Id;
                }
                else
                {
                    Database.Entity.ExpenseCategory model = new Database.Entity.ExpenseCategory();
                    model.ExpenseCategoryType = name;
                    model.Type = 2;
                    db.ExpenseCategories.Add(model);
                    db.SaveChanges();
                    return model.Id;
                }
            }
        }
        public bool AddReinburstmentNotes(List<ReimbursementNote> notes, int assignmentId)
        {
            using (var db = new ApplicationDbContext())
            {
                // generate expense category if not exist
                #region Create Expense Category
                int expenseCategoryId = GetExpenseCategoryId("Reimburse");
                //var result = db.ExpenseCategories.Where(x => x.Type == 1 && x.ExpenseCategoryType.ToLower() == "Reimburse").FirstOrDefault();
                //if (result != null)
                //{
                //    expenseCategoryId = result.Id;
                //}
                //else
                //{
                //    ExpenseCategory model = new ExpenseCategory();
                //    model.ExpenseCategoryType = "Reimburse";
                //    model.Type = 1;
                //    db.ExpenseCategories.Add(model);
                //    db.SaveChanges();
                //    expenseCategoryId = model.Id;
                //}
                #endregion
                //need to check if we need this code
                //var exp = db.Expenses.Where(x => x.ExpenseCategoryId == expenseCategoryId && x.WorkerAssignmentId == assignmentId &&x.ExpenseCategoryId ==expenseCategoryId).ToList();
                //db.Expenses.RemoveRange(exp);
                //db.SaveChanges();

                //var n = db.ReimbursementNotes.Where(x => x.AssignmentId == assignmentId).ToList();
                //db.ReimbursementNotes.RemoveRange(n);
                //db.SaveChanges();
                var assignment = (from a in db.WorkerAssignments
                                  join w in db.Workers on new { WorkerId = a.WorkerId } equals new { WorkerId = w.Id }
                                  join j in db.WorkerJobs on new { a.WorkerId, a.JobTypeId } equals new { j.WorkerId, j.JobTypeId }
                                  where a.Id == assignmentId
                                  select new
                                  {
                                      WorkerName = w.Firstname + " " + w.Lastname,
                                      DateAssigned = a.StartDate,
                                      a.EndDate,
                                      a.PropertyId,
                                      j.PaymentRate,
                                      j.PaymentType

                                  }).FirstOrDefault();
                var reimbursementIds = db.ReimbursementNotes.Where(x => x.AssignmentId == assignmentId).Select(x=>x.Id).ToList();
                reimbursementIds = reimbursementIds.Where(x => !notes.Select(y => y.Id).ToList().Contains(x)).ToList();
                db.ReimbursementNotes.RemoveRange(db.ReimbursementNotes.Where(x=> reimbursementIds.Contains(x.Id)));
                db.Expenses.RemoveRange(db.Expenses.Where(x => reimbursementIds.Contains(x.ReimbursementId.Value)));
                db.SaveChanges();
                foreach (var note in notes)
                {
                    note.AssignmentId = assignmentId;
                    if (note.Id == 0)
                    {
                        db.ReimbursementNotes.Add(note);
                        db.SaveChanges();
                    }
                    else
                    {
                        var temp = db.ReimbursementNotes.Where(x => x.Id == note.Id).FirstOrDefault();
                        temp.Amount = note.Amount;
                        temp.Note = note.Note;
                        db.Entry(temp).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    int paymentMethodId = GetWorkerDefaultPaymentMethodId();
                 
                    //JM 5/6/2020 check if expense is already created
                    var expense = db.Expenses.Where(x => x.WorkerAssignmentId == assignmentId && x.ReimbursementId==note.Id).FirstOrDefault();

                    if (expense == null && assignment != null)
                    {
                        Expense e = new Expense();
                        e.Description = assignment.WorkerName +" - " +note.Note;
                        e.ExpenseCategoryId = expenseCategoryId;
                        e.PropertyId = assignment.PropertyId;
                        e.DueDate = assignment.DateAssigned;
                        e.PaymentMethodId = paymentMethodId;
                        e.DateLastJobRun = null;
                        e.DateCreated = DateTime.Now;
                        e.DateNextJobRun = null;
                        e.CompanyId = SessionHandler.CompanyId;
                        e.Amount = note.Amount;
                        e.WorkerAssignmentId = assignmentId;
                        e.ReimbursementId = note.Id;
                        db.Expenses.Add(e);
                        db.SaveChanges();
                    }
                    else if(expense!=null && assignment!=null)
                    {
                        expense.Amount = note.Amount;
                        expense.DueDate = assignment.DateAssigned;
                        expense.Description= assignment.WorkerName + " - " + note.Note;
                        db.Entry(expense).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            return true;
        }

        public List<Guest> GetGuestsList()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Guests.ToList();
            }
        }
        public List<Renter> GetRenters()
        {
            using(var db = new ApplicationDbContext())
            {
                return (from renter in db.Renters join reservation in db.Inquiries on renter.BookingId equals reservation.Id join property in db.Properties on reservation.PropertyId equals property.ListingId select renter).ToList();
            }
        }
        public List<Expense> GetExpenseInfoByPropertyId(int propertyId)
        {
            using (var db = new ApplicationDbContext())
            {
                int companyId = SessionHandler.CompanyId;

                return db.Expenses.Where(
                       x =>
                           x.CompanyId == companyId && x.PropertyId == propertyId).ToList();

            }
        }

    }
}
