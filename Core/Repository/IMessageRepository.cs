﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Repository
{
    public interface IMessageRepository
    {
        List<InboxListing> GetInboxList(int skip,int take);
        List<InboxListing> GetNewInbox();
        List<CommunicationInboxThread> GetWorkerVendorInboxList();
        List<CommunicationMessagesModel> GetCommunicationInboxMessages(string threadId);
        List<CommunicationInboxThread> GetRenterInboxList(int userId, string search, int bookingId = 0);
    }
}
