﻿using Core.Database.Context;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Helper;
using System.Diagnostics;

namespace Core.Repository
{
    public class MessageRepository : IMessageRepository
    {
        public List<CommunicationInboxThread> GetRenterInboxList(int userId, string search, int bookingId = 0)
        {
            List<CommunicationInboxThread> List = new List<CommunicationInboxThread>();
            using (var db = new ApplicationDbContext())
            {
                var renterIds = db.Renters.Where(x => x.BookingId == bookingId).Select(x => x.Id).ToList();
                var temp = (from r in db.Renters join t in db.CommunicationThreads on r.Id equals t.RenterId where (search == "" ? true : r.Firstname.Contains(search) || r.Lastname.Contains(search) || (r.Firstname + " " + r.Lastname).Contains(search)) && (bookingId == 0 ? true : renterIds.Contains(r.Id)) && t.CompanyId == SessionHandler.CompanyId && t.IsRenter select new { Renter = r, Thread = t }).ToList();
                foreach (var item in temp)
                {
                    CommunicationInboxThread inboxThread = new CommunicationInboxThread();
                    inboxThread.RenterId = item.Renter.Id;
                    var thread = db.CommunicationThreads.Where(x => x.RenterId == item.Renter.Id && x.CompanyId == SessionHandler.CompanyId && x.IsRenter).FirstOrDefault();
                    inboxThread.ThreadId = (thread != null ? thread.ThreadId : "0");
                    inboxThread.ImageUrl = Utilities.GetDefaultAvatar(item.Renter.Firstname);
                    inboxThread.IsIncoming = item.Thread.IsEnd == true ? false : (from m in db.CommunicationSMS where m.ThreadId == item.Thread.ThreadId orderby m.CreatedDate descending select m.UserId).FirstOrDefault() != userId;
                    if (thread != null)
                    {
                        var lastMessage = db.CommunicationMessages.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.DateCreated).Select(x => x).FirstOrDefault();
                        if (lastMessage != null)
                        {
                            inboxThread.LastMessage = lastMessage.Message;
                            inboxThread.LastMessageAt = lastMessage.DateCreated;
                        }
                        else if (lastMessage == null)
                        {
                            var lastSMS = db.CommunicationSMS.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.CreatedDate).Select(x => x).FirstOrDefault();
                            if (lastSMS != null)
                            {
                                inboxThread.LastMessage = lastSMS.Message;
                                inboxThread.LastMessageAt = lastSMS.CreatedDate;
                            }
                            else
                            {
                                inboxThread.LastMessage = "";
                            }
                        }
                        else if (lastMessage == null)
                        {
                            var lastEmail = db.CommunicationEmails.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.CreatedDate).Select(x => x).FirstOrDefault();
                            if (lastEmail != null)
                            {
                                inboxThread.LastMessage = lastEmail.Message;
                                inboxThread.LastMessageAt = lastEmail.CreatedDate;
                            }
                            else
                            {
                                inboxThread.LastMessage = "";
                            }
                        }

                    }
                    else
                    {
                        inboxThread.LastMessage = "No Message";
                    }

                    inboxThread.Name = item.Renter.Firstname + " " + item.Renter.Lastname;
                    List.Add(inboxThread);
                    List = List.OrderByDescending(x => x.LastMessageAt).ToList();
                }
            }
            return List;
        }
        public List<CommunicationInboxThread> GetWorkerVendorInboxList()
        {
            using (var db = new ApplicationDbContext())
            {
                List<CommunicationInboxThread> List = new List<CommunicationInboxThread>();
                if (GlobalVariables.UserWorkerId == 0)
                {
                    var temp = (from w in db.Workers join t in db.CommunicationThreads on w.Id equals t.WorkerId where t.CompanyId == SessionHandler.CompanyId && t.IsRenter==false select w).ToList();
                    foreach (var item in temp)
                    {
                        CommunicationInboxThread inboxThread = new CommunicationInboxThread();
                        inboxThread.WorkerId = item.Id;
                        var thread = db.CommunicationThreads.Where(x => x.WorkerId ==item.Id && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                        inboxThread.ThreadId = (thread != null ? thread.ThreadId : "0");
                        inboxThread.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Firstname) : item.ImageUrl;
                        inboxThread.IsIncoming = thread.IsEnd == true ? false : (from m in db.CommunicationSMS where m.ThreadId == thread.ThreadId orderby m.CreatedDate descending select m.UserId).FirstOrDefault() != System.Web.HttpContext.Current.User.Identity.Name.ToInt();
                        if (thread != null)
                        {
                            var lastMessage = db.CommunicationMessages.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.DateCreated).Select(x => x).FirstOrDefault();
                            if (lastMessage != null)
                            {
                                inboxThread.LastMessage = lastMessage.Message;
                                inboxThread.LastMessageAt = lastMessage.DateCreated;
                            }
                            else if(lastMessage==null)
                            {
                              var lastSMS = db.CommunicationSMS.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.CreatedDate).Select(x => x).FirstOrDefault();
                                if (lastSMS != null)
                                {
                                    inboxThread.LastMessage = lastSMS.Message;
                                    inboxThread.LastMessageAt = lastSMS.CreatedDate;
                                }
                                else
                                {
                                    inboxThread.LastMessage = "";
                                }
                            }
                            
                        }   
                        else
                        {
                            inboxThread.LastMessage ="No Message";
                        }
                        inboxThread.Name = item.Firstname + " " + item.Lastname;
                        List.Add(inboxThread);
                        List =  List.OrderByDescending(x => x.LastMessageAt).ToList();
                    }
                    var tempVendor = (from v in db.Vendors join t in db.CommunicationThreads on v.Id equals t.VendorId where t.CompanyId == SessionHandler.CompanyId && t.IsRenter == false select v).ToList();
                    foreach (var item in tempVendor)
                    {
                        CommunicationInboxThread inboxThread = new CommunicationInboxThread();
                        inboxThread.VendorId = item.Id;
                        var thread = db.CommunicationThreads.Where(x => x.VendorId == item.Id && x.CompanyId == SessionHandler.CompanyId).FirstOrDefault();
                        inboxThread.ThreadId = (thread != null ? thread.ThreadId : "0");
                        inboxThread.ImageUrl =Utilities.GetDefaultAvatar(item.Name);
                        if (thread != null)
                        {
                            var lastMessage = db.CommunicationMessages.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.DateCreated).Select(x => x).FirstOrDefault();
                            if (lastMessage != null)
                            {
                                inboxThread.LastMessage = lastMessage.Message;
                                inboxThread.LastMessageAt = lastMessage.DateCreated;
                            }
                            else if (lastMessage == null)
                            {
                                var lastSMS = db.CommunicationSMS.Where(x => x.ThreadId == thread.ThreadId).OrderByDescending(x => x.CreatedDate).Select(x => x).FirstOrDefault();
                                if (lastSMS != null)
                                {
                                    inboxThread.LastMessage = lastSMS.Message;
                                    inboxThread.LastMessageAt = lastSMS.CreatedDate;
                                }
                                else
                                {
                                    inboxThread.LastMessage = "";
                                }
                            }

                        }
                        else
                        {
                            inboxThread.LastMessage = "No Message";
                        }
                        inboxThread.Name = item.Name;
                        List.Add(inboxThread);
                        List = List.OrderByDescending(x => x.LastMessageAt).ToList();
                    }
                }
                else
                {
                    var temp = db.CommunicationThreads.Where(x => x.WorkerId == GlobalVariables.UserWorkerId).ToList();
                    foreach (var item in temp)
                    {
                        var lastUserId = db.CommunicationMessages.Where(x => x.UserId != GlobalVariables.UserId && x.ThreadId ==item.ThreadId).OrderByDescending(x => x.DateCreated).Select(x=>x.UserId).FirstOrDefault();
                        var lastUser = db.Users.Where(x => x.Id == lastUserId).FirstOrDefault();
                        CommunicationInboxThread inboxThread = new CommunicationInboxThread();
                        inboxThread.WorkerId = GlobalVariables.UserWorkerId;
                        inboxThread.ThreadId = item.ThreadId;
                        inboxThread.ImageUrl = string.IsNullOrEmpty(lastUser.ImageUrl) ? Utilities.GetDefaultAvatar(lastUser.FirstName) :lastUser.ImageUrl;

                        var lastMessage = db.CommunicationMessages.Where(x=> x.ThreadId == item.ThreadId).OrderByDescending(x => x.DateCreated).Select(x=>x).FirstOrDefault();
                        inboxThread.LastMessage = lastMessage.Message;
                        inboxThread.LastMessageAt = lastMessage.DateCreated;
                        inboxThread.Name = lastUser.FirstName + " " +lastUser.LastName ;
                        List.Add(inboxThread);
                    }
                }
                return List;
            }
        }
        public List<CommunicationMessagesModel> GetCommunicationInboxMessages(string threadId)
        {
           List<CommunicationMessagesModel> MessageList = new List<CommunicationMessagesModel>();
            using (var db = new ApplicationDbContext())
            {
                var renterId = db.CommunicationThreads.Where(x => x.ThreadId == threadId).Select(x=>x.RenterId).FirstOrDefault();

                var notices = (from notice in db.Notices join noticeRenter in db.RenterNotices on notice.Id equals noticeRenter.NoticeId where noticeRenter.RenterId == renterId select notice).ToList();
                foreach(var notice in notices)
                {
                    CommunicationMessagesModel message = new CommunicationMessagesModel();
                    message.Message = notice.Description;
                    message.IsMyMessage = notice.IsForRenter;
                    message.CreatedDate = notice.DateOfNotice;
                    message.Source = "Renter";
                    message.Name = "Notice";
                    message.Images = db.NoticeDocuments.Where(x => x.NoticeId ==notice.Id).Select(x => x.AttachmentPath).ToList();
                    message.Type = 5;
                    message.ImageUrl = Utilities.GetDefaultAvatar("Notice");
                    MessageList.Add(message);
                }
               //this will get sms received on application
                var renterSMS = (from user in db.Renters
                                 join t in db.CommunicationThreads on user.Id equals t.RenterId
                                 join m in db.CommunicationSMS on t.ThreadId  equals m.ThreadId
                                 where t.ThreadId == threadId && m.UserId == 0
                                 select new {Id=m.Id, Message = m.Message, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.Firstname+" "+user.Lastname,From=m.From }).ToList();

                var temp = (from user in db.Users
                            join m in db.CommunicationMessages on user.Id equals m.UserId
                            join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId where t.ThreadId ==threadId
                            select new { Message = m.Message,CreatedDate=m.DateCreated,UserId= user.Id,Name =user.FirstName +" "+user.LastName,ImageUrl = user.ImageUrl}).ToList();

                //This will get sms send by user
                var sms= (from user in db.Users
                            join m in db.CommunicationSMS on user.Id equals m.UserId
                            join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                            where t.ThreadId == threadId
                            select new {Id=m.Id ,ResourceSid = m.MessageSid, To = m.To,From =m.From, Message = m.Message, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();

                var call = (from user in db.Users
                            join m in db.CommunicationCalls on user.Id equals m.UserId
                            join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                            where t.ThreadId == threadId
                            select new { CallSid = m.CallSid, ResourceSid = m.ResourceSid, To = m.To, From = m.From, RecordingUrl = m.RecordingUrl, Duration = m.CallDuration == null ? m.RecordingDuration : m.CallDuration, CreatedDate = m.CreatedDate, UserId = user.Id, Name = user.FirstName + " " + user.LastName, ImageUrl = user.ImageUrl }).ToList();
                var Emails = (from m in db.CommunicationEmails
                           join t in db.CommunicationThreads on m.ThreadId equals t.ThreadId
                           where t.ThreadId == threadId
                           select new { IsMyMessage = m.IsMyMessage, Id = m.Id, To = m.To, From = m.From, Message = m.Message, CreatedDate = m.CreatedDate }).ToList();
        
                foreach (var item in Emails)
                {
                    CommunicationMessagesModel message = new CommunicationMessagesModel();
                    message.Message = item.Message;
                    message.IsMyMessage = item.IsMyMessage;
                    message.CreatedDate = item.CreatedDate;
                    message.Source = message.IsMyMessage ? item.To : item.From;
                    message.Name = "Email";
                    message.Images = db.EmailAttachments.Where(x => x.CommunicationEmailId == item.Id).Select(x => x.FileUrl).ToList();
                    message.Type = 5;
                    message.ImageUrl = string.IsNullOrEmpty(message.ImageUrl) ? Utilities.GetDefaultAvatar(message.Name) : message.ImageUrl;
                    MessageList.Add(message);
                }
                foreach (var item in renterSMS)
                {
                    CommunicationMessagesModel message = new CommunicationMessagesModel();
                    message.Message = item.Message;
                    message.IsMyMessage = false;
                    message.CreatedDate = item.CreatedDate;
                    message.Source =  item.From;
                    message.Name = item.Name;
                    message.Images = db.MMSImages.Where(x => x.CommunicationSMSId == item.Id).Select(x => x.ImageUrl).ToList();
                    if (message.Source.Substring(0, 8) == "whatsapp")
                    {
                        message.Type = 3;
                    }
                    else if (message.Source.Substring(0, 9) == "messenger")
                    {
                        message.Type = 4;
                    }
                    else
                    {
                        message.Type = 1;
                    }
                    message.ImageUrl = Utilities.GetDefaultAvatar(item.Name);

                    MessageList.Add(message);
                }
                foreach (var item in temp)
                {

                    CommunicationMessagesModel message = new CommunicationMessagesModel();
                    message.Message = item.Message;
                    message.IsMyMessage = (item.UserId == System.Web.HttpContext.Current.User.Identity.Name.ToInt() ? true : false);
                    message.CreatedDate= item.CreatedDate;     
                    message.Name = item.Name;
                    message.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl;
                    message.Type = 0;
                    MessageList.Add(message);
                }
                foreach(var item in sms)
                {
                    CommunicationMessagesModel message = new CommunicationMessagesModel();
                    message.Message = item.Message;
                    message.IsMyMessage = (item.UserId == System.Web.HttpContext.Current.User.Identity.Name.ToInt() ? true : false);
                    message.CreatedDate = item.CreatedDate;
                    message.Source = message.IsMyMessage ? item.To : item.From;
                    message.Name = item.Name;
                    message.Images = db.MMSImages.Where(x => x.CommunicationSMSId == item.Id).Select(x => x.ImageUrl).ToList();
                    if (message.Source.Substring(0, 8) == "whatsapp")
                    {
                        message.Type = 3;
                    }
                    else if (message.Source.Substring(0, 9) == "messenger")
                    {
                        message.Type = 4;
                    }
                    else
                    {
                        message.Type = 1;
                    }
                    message.ResourceSid = item.ResourceSid;
                    message.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl;
                    
                    MessageList.Add(message);
                }

                
                  foreach (var item in call)
                {
                    CommunicationMessagesModel message = new CommunicationMessagesModel();
                  
                    message.IsMyMessage = (item.UserId == System.Web.HttpContext.Current.User.Identity.Name.ToInt() ? true : false);
                    message.Source = message.IsMyMessage ? item.To : item.From;
                    message.CreatedDate = item.CreatedDate;
                    message.ResourceSid = item.CallSid;
                    message.RecordingUrl = item.RecordingUrl;
                    message.Duration = item.Duration;

                    message.Name = item.Name;
                    message.Type = 2;
                    message.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? Utilities.GetDefaultAvatar(item.Name) : item.ImageUrl;

                    MessageList.Add(message);
                }



            }
            MessageList = MessageList.OrderBy(x => x.CreatedDate).ToList();

                return MessageList;
        }

        //public WorkerInboxListing GetWorkerInboxMessageByThreadId(string threadId)
        //{
         

        //        return inbox;
        //}
        public List<InboxListing> GetInboxList(int skip,int take)
        {
            List<InboxListing> temp = new List<InboxListing>();
            using (var db = new ApplicationDbContext())
            {

                var inboxList = (from i in db.Inbox
                                 join g in db.Guests on i.GuestId equals g.Id
                                 join p in db.Properties on i.PropertyId equals p.ListingId join h in db.Hosts on i.HostId equals h.Id join hc in db.HostCompanies on h.Id equals hc.HostId
                                 where hc.CompanyId ==SessionHandler.CompanyId
                                    orderby i.LastMessageAt descending
                                    select new
                                    {
                                        i.HostId,
                                        i.ThreadId,
                                        i.GuestId,
                                        GuestName = g.Name,
                                        GuestPicture = g.ProfilePictureUrl,
                                        i.LastMessage,
                                        i.LastMessageAt,
                                        i.HasUnread,
                                        i.PropertyId,
                                        PropertyName = p.Name,
                                        i.SiteType,
                                        p.TimeZoneName,
                                        i.IsEnd
                                        
                                    }).Skip(skip).Take(take).ToList();

                //var count = db.Messages.Count();
                //var messages = db.Messages.ToList();
                foreach (var item in inboxList)
                {
                    InboxListing il = new InboxListing();
                    il.HostAccountId = item.HostId;
                    il.ThreadId = item.ThreadId;
                    il.GuestId = item.GuestId;
                    il.GuestName = item.GuestName;
                    il.GuestProfilePictureUrl = string.IsNullOrEmpty(item.GuestPicture) ? Utilities.GetDefaultAvatar(item.GuestName) : item.GuestPicture;
                    il.LastMessage = item.LastMessage;
                    il.LastMessageAt = item.LastMessageAt; //(item.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc((from m in db.Messages where m.ThreadId == item.ThreadId select m.CreatedDate).FirstOrDefault(), TimeZoneInfo.FindSystemTimeZoneById(item.TimeZoneName)) : item.LastMessageAt);
                    il.HasUnread = item.HasUnread;
                    il.IsIncoming =  item.IsEnd==true?false:!(from m in db.Messages where m.ThreadId == item.ThreadId orderby m.CreatedDate descending select m.IsMyMessage).FirstOrDefault(); //!messages.Where(x => x.ThreadId == item.ThreadId).OrderByDescending(x => x.CreatedDate).FirstOrDefault().IsMyMessage;
                    il.PropertyId = item.PropertyId;
                    il.PropertyName = item.PropertyName;
                    il.SiteType = item.SiteType;
                    temp.Add(il);
                }
                return temp;
            }
        }

        public List<InboxListing> GetNewInbox()
        {
            List<InboxListing> temp = new List<InboxListing>();
            using (var db = new ApplicationDbContext())
            {

                var inboxList = (from i in db.Inbox
                                 join g in db.Guests on i.GuestId equals g.Id
                                 join p in db.Properties on i.PropertyId equals p.ListingId where i.HasUnread ==true
                                 orderby i.LastMessageAt ascending
                                 select new
                                 {
                                     i.HostId,
                                     i.ThreadId,
                                     i.GuestId,
                                     GuestName = g.Name,
                                     GuestPicture = g.ProfilePictureUrl,
                                     i.LastMessage,
                                     i.LastMessageAt,
                                     i.HasUnread,
                                     i.PropertyId,
                                     PropertyName = p.Name,
                                     i.SiteType,
                                     p.TimeZoneName,
                                     i.IsEnd
                                 }).ToList();

                //var count = db.Messages.Count();
                //var messages = db.Messages.ToList();
                foreach (var item in inboxList)
                {
                    InboxListing il = new InboxListing();
                    il.HostAccountId = item.HostId;
                    il.ThreadId = item.ThreadId;
                    il.GuestId = item.GuestId;
                    il.GuestName = item.GuestName;
                    il.GuestProfilePictureUrl = string.IsNullOrEmpty(item.GuestPicture) ? Utilities.GetDefaultAvatar(item.GuestName) : item.GuestPicture;
                    il.LastMessage = item.LastMessage;
                    il.LastMessageAt = item.LastMessageAt;// (item.TimeZoneName != null ? TimeZoneInfo.ConvertTimeFromUtc((from m in db.Messages where m.ThreadId == item.ThreadId select m.CreatedDate).FirstOrDefault(), TimeZoneInfo.FindSystemTimeZoneById(item.TimeZoneName)) : item.LastMessageAt);
                    //  il.LastMessageAt = (item.TimeZoneName !=null?TimeZoneInfo.ConvertTimeFromUtc(item.LastMessageAt, TimeZoneInfo.FindSystemTimeZoneById(Core.API.Airbnb.Properties.TimeZome(item.TimeZoneName))):item.LastMessageAt); //item.LastMessageAt;
                    il.HasUnread = item.HasUnread;
                    il.IsIncoming = item.IsEnd == true ? false :!(from m in db.Messages where m.ThreadId == item.ThreadId orderby m.CreatedDate descending select m.IsMyMessage).FirstOrDefault(); //!messages.Where(x => x.ThreadId == item.ThreadId).OrderByDescending(x => x.CreatedDate).FirstOrDefault().IsMyMessage;
                    il.PropertyId = item.PropertyId;
                    il.PropertyName = item.PropertyName;
                    il.SiteType = item.SiteType;

                    //JM 02/22/19 Also call,sms when new message received and customize with user
                    ICommonRepository objIList = new CommonRepository();
                    var messageThread = objIList.GetMessageThread(item.ThreadId);
                    var threadDetails = objIList.GetThreadDetail(item.ThreadId);

                    var inbox = db.Inbox.Where(x => x.ThreadId == item.ThreadId).FirstOrDefault();
                    var sms = db.SMS.Where(x => x.GuestId == inbox.GuestId).ToList();
                    var calls = db.Calls.Where(x => x.GuestId == inbox.GuestId).ToList();

                    var contactInfos = db.GuestContactInfos.Where(x => x.GuestId == inbox.GuestId).ToList();

                    messageThread.ToList().ForEach(x =>
                    {

                        var message = new MessageEntry();
                        message.MessageContent = x.MessageContent;
                        message.IsMyMessage = x.IsMyMessage;
                        message.CreatedDate = x.CreatedDate;
                        message.Id = x.Id;
                        message.Type = 0;
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        il.Messages.Add(message);
                    });
                    sms.ForEach(x =>
                    {
                        var message = new MessageEntry();
                        message.Id = x.Id;
                        message.MessageContent = x.Body;
                        message.IsMyMessage = x.IsMyMessage;
                        message.Source = x.IsMyMessage ? x.To : x.From;
                        if (message.Source.Substring(0, 8) == "whatsapp")
                        {
                            message.Type = 3;
                        }
                        else if (message.Source.Substring(0, 9) == "messenger")
                        {
                            message.Type = 4;
                        }
                        else
                        {
                            message.Type = 1;
                        }
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        message.CreatedDate = x.CreatedDate;
                        message.ResourceSid = x.MessageSid;
                        il.Messages.Add(message);
                    }
                    );
                    calls.ForEach(x =>
                    {
                        var message = new MessageEntry();
                        message.Id = x.Id;
                        message.IsMyMessage = x.IsMyMessage;
                        message.Type = 2;
                        message.Source = x.IsMyMessage ? x.To : x.From;
                        message.CreatedDate = x.CreatedDate;
                        message.ResourceSid = x.CallSid;
                        message.RecordingUrl = x.RecordingUrl;
                        message.Duration = x.CallDuration == null ? x.RecordingDuration : x.CallDuration;
                        var user = db.Users.Where(y => y.Id == x.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            message.Name = user.FirstName + " " + user.LastName;
                            message.ImageUrl = string.IsNullOrEmpty(user.ImageUrl) ? Core.Helper.Utilities.GetDefaultAvatar(user.FirstName) : user.ImageUrl;
                        }
                        il.Messages.Add(message);
                    });


                    temp.Add(il);
                    
                }
                var unreadMessage = db.Inbox.Where(x => x.HasUnread == true).ToList();
                foreach(var item in unreadMessage)
                {
                    item.HasUnread = false;
                    db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return temp;
            }
        }

    }
}
