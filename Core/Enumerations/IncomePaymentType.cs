﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum IncomePaymentType
    {
        Payment=1,
        Credit=2
    }
}
