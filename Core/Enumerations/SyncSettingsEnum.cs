﻿namespace Core.Enumerations
{
    public enum SymbolType
    {
        Percent = 0,
        Amount = 1
    }

    public enum AdjustmentType
    {
        Add = 0,
        Deduct = 1
    }
}
