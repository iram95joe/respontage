﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum MaintenanceStatus
    {
        NotYetStarted = 0,
        Started = 1,
        Complete = 2
    }
}
