﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum TaskTemplateType
    {
        INQUIRY = 1,
        TURNOVER = 2,
        BOOKING = 3,
        CHECK_IN = 4,
        CHECK_OUT = 5
    }
}
