﻿namespace Core.Enumerations
{
    public enum SiteType
    {
        Local = 0,
        Airbnb = 1,
        VRBO = 2,
        BookingCom = 3,
        Homeaway = 4
    }
}
