﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations.Worker
{
    public enum WorkerCategory
    {
        TURNOVER = 1,
        ACCESSORIES = 2
    }

    public enum WorkerAssignmentType
    {
        PRE_CHECK_IN = 1,
        POST_CHECK_OUT = 2
    }
}
