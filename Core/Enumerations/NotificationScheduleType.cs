﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum NotificationScheduleType
    {
        LatePayment=1,
        LastRentIncrease=2,
        RenewOfContract=3,
    }
}
