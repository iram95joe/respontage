﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum ParentPropertyType
    {
        Sync =1,
        MultiUnit=2,
        Accounts=3
    }
}
