﻿namespace Core.Enumerations
{
    public enum NotificationType
    {
        INFORMATION,
        SUCCESS,
        WARNING,
        ERROR
    }
}