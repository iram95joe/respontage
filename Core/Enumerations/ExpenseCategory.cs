﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum ExpenseCategory
    {
        BookingFee=1,
        SalesTax=2,
        Labor =3,
        Reimburse=4,
        Fixed=5,
        ResolutionAdjustment=6,
        Retainer=7,
        RefundCancel=8
    }
}
