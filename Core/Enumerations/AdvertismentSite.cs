﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum AdvertismentSite
    {
        Facebook = 1,
        Craigslist = 2,
        Lamudi = 3
    }
}
