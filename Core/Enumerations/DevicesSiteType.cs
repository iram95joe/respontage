﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum DevicesSiteType
    {
        Arlo = 0,
        Blink = 1,
        Vera = 2,
        Smartthings = 3,
        Wyze =4
    }   
}
