﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum IncomeType
    {
    Booking=1,
    Reservation=2,
    ResolutionPayout=3,
    SecurityDeposit=4,
    Refund=5,
    Rent=12,
    TenantBill=13
    }
}
