﻿namespace Core.Enumerations
{
    public enum TravelTypeEnum
    {
        ARRIVAL,
        DEPARTURE,
        SELF_CHECK_IN =13,
        SELF_CHECK_OUT =14,

    }

    public enum NoteType
    {
        NOTE = 1,
        FLIGHT_ARRIVAL = 2,
        FLIGHT_DEPARTURE = 3,
        MEETING = 4,
        DRIVE_ARRIVAL = 5,
        DRIVE_DEPARTURE = 6,
        CRUISE_ARRIVAL = 7,
        CRUISE_DEPARTURE = 8,
        TRAIN_ARRIVAL = 9,
        TRAIN_DEPARTURE = 10,
        BUS_ARRIVAL = 11,
        BUS_DEPARTURE = 12,
        CHECK_IN = 13,
        CHECK_OUT = 14

    }
}