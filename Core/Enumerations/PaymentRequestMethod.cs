﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum PaymentRequestMethod
    {
        Paypal = 0,
        Square = 1,
        Stripe = 2,
        Others =3
    }
}
