﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum CompanyType
    {
        Shared = 0,
        Longterm = 1,
        ShortTerm = 2
    }
}
