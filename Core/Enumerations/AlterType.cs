﻿namespace Core.Enumerations
{
    public enum AlterType
    {
        Update,
        Add,
        Replace
    }
}
