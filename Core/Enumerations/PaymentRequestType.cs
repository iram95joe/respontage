﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enumerations
{
    public enum PaymentRequestType
    {
        Full = 0,
        Partial = 1
    }
}
