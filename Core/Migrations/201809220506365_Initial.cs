namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookingStatus",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 128),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Cashflow",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CashFlowTypeId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Amount = c.Double(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        PaymentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CashFlowType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CashFlowTypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CreditCards",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReservationId = c.String(),
                        Type = c.String(),
                        Number = c.String(),
                        HolderName = c.String(),
                        CVC = c.String(),
                        Expiry = c.String(),
                        CardAlertHeading = c.String(),
                        CardAlertDescription = c.String(),
                        PaymentStatus = c.String(),
                        CardStatus = c.String(),
                        CardViewLimit = c.String(),
                        ReplyActions = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DefaultRate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StandardRate = c.Double(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExpenseCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseCategoryType = c.String(),
                        Type = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExpensePayment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseId = c.Int(nullable: false),
                        Description = c.String(),
                        Type = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Expense",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        UnitId = c.Int(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        BookingId = c.Int(nullable: false),
                        PropertyOwnerId = c.Int(nullable: false),
                        PropertyManagerId = c.Int(nullable: false),
                        RenterId = c.Int(nullable: false),
                        WorkerAssignmentId = c.Int(nullable: false),
                        BillId = c.Int(nullable: false),
                        PaymentMethodId = c.Int(nullable: false),
                        Frequency = c.Int(nullable: false),
                        DueDateDMn = c.Int(nullable: false),
                        DueDateMQt = c.Int(nullable: false),
                        DueDateDQt = c.Int(nullable: false),
                        DueDateMYr = c.Int(nullable: false),
                        DueDateDYr = c.Int(nullable: false),
                        DueDate = c.DateTime(),
                        DateCreated = c.DateTime(nullable: false),
                        DateLastJobRun = c.DateTime(),
                        DateNextJobRun = c.DateTime(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsAutoCreate = c.Boolean(nullable: false),
                        IsOwnerExpense = c.Boolean(nullable: false),
                        IsRenterExpense = c.Boolean(nullable: false),
                        IsPreDeducted = c.Boolean(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        PaymentDate = c.DateTime(),
                        FeeTypeId = c.Int(nullable: false),
                        ExpenseCategoryId = c.Int(nullable: false),
                        Receipts = c.String(),
                        LeaseId = c.Int(nullable: false),
                        IsStartupCost = c.Boolean(nullable: false),
                        IsCapitalExpense = c.Boolean(nullable: false),
                        IsRefund = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeeType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fee_Type = c.String(),
                        CompanyId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Guest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GuestId = c.String(),
                        Name = c.String(),
                        FullName = c.String(),
                        ProfilePictureUrl = c.String(),
                        ContactNumber = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HashDictionary",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HashValue = c.String(),
                        ExpiryDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Host",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SiteHostId = c.String(),
                        SiteType = c.Int(nullable: false),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        ProfilePictureUrl = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        AutoSync = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HostSessionCookie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HostId = c.Int(nullable: false),
                        Token = c.String(),
                        Cookies = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        SiteType = c.Int(nullable: false),
                        SessionKey = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Inbox",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        HostId = c.Int(nullable: false),
                        ThreadId = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                        Status = c.String(),
                        HasUnread = c.Boolean(nullable: false),
                        PropertyId = c.Long(nullable: false),
                        GuestId = c.Int(nullable: false),
                        LastMessage = c.String(),
                        LastMessageAt = c.DateTime(nullable: false),
                        InquiryId = c.String(),
                        CheckInDate = c.DateTime(),
                        CheckOutDate = c.DateTime(),
                        GuestCount = c.Int(nullable: false),
                        Adult = c.Int(nullable: false),
                        Children = c.Int(nullable: false),
                        IsInquiryOnly = c.Boolean(nullable: false),
                        IsSpecialOfferSent = c.Boolean(nullable: false),
                        RentalCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CleaningCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GuestFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ServiceFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Subtotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GuestPay = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HostEarn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatusType = c.Int(nullable: false),
                        CanPreApproveInquiry = c.Boolean(nullable: false),
                        CanWithdrawPreApprovalInquiry = c.Boolean(nullable: false),
                        CanDeclineInquiry = c.Boolean(nullable: false),
                        SiteType = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Income",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomeTypeId = c.Int(nullable: false),
                        BookingId = c.Int(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        Description = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        PaymentDate = c.DateTime(),
                        DateRecorded = c.DateTime(nullable: false),
                        IsBatchLoad = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IncomePayment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomeId = c.Int(nullable: false),
                        Description = c.String(),
                        Type = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IncomeType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Inquiry",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HostId = c.Int(nullable: false),
                        ReservationId = c.String(),
                        PropertyId = c.Int(nullable: false),
                        GuestId = c.Int(nullable: false),
                        ConfirmationCode = c.String(),
                        BookingStatusCode = c.String(),
                        CheckInDate = c.DateTime(),
                        CheckOutDate = c.DateTime(),
                        Nights = c.Int(nullable: false),
                        ReservationCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SumPerNightAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CleaningFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ServiceFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPayout = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PayoutDate = c.DateTime(),
                        NetRevenueAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InquiryDate = c.DateTime(),
                        BookingDate = c.DateTime(),
                        BookingCancelDate = c.DateTime(),
                        BookingConfirmDate = c.DateTime(),
                        BookingConfirmCancelDate = c.DateTime(),
                        SiteType = c.Int(nullable: false),
                        GuestCount = c.Int(nullable: false),
                        Adult = c.Int(nullable: false),
                        Children = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsImported = c.Boolean(nullable: false),
                        IsAltered = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        ThreadId = c.String(),
                        Type = c.String(),
                        Fees = c.String(),
                        RateDetails = c.String(),
                        Paid = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InquiryTask",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        TaskId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        ReservationId = c.String(),
                        TemplateId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        TaskTitle = c.String(),
                        TaskDescription = c.String(),
                        TemplateName = c.String(),
                        TemplateTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MessageRule",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PropertyId = c.Int(nullable: false),
                        TemplateMessageId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        DateDifference = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Message",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MessageId = c.String(),
                        ThreadId = c.String(),
                        MessageContent = c.String(),
                        IsMyMessage = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentAccount",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ConfigurationKeys = c.String(),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentHistory",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ConfirmationCode = c.String(),
                        PaymentType = c.Byte(nullable: false),
                        PaymentMethod = c.Byte(nullable: false),
                        PaymentName = c.String(),
                        PaymentPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BalanceAfter = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Paid = c.Boolean(nullable: false),
                        PaypalRedirectUrl = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentMethod",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Property",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HostId = c.Int(nullable: false),
                        ListingId = c.Long(nullable: false),
                        ListingUrl = c.String(),
                        Name = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        Address = c.String(),
                        UnitCount = c.Int(nullable: false),
                        MinStay = c.Int(nullable: false),
                        Internet = c.String(),
                        Pets = c.String(),
                        WheelChair = c.String(),
                        Description = c.String(),
                        Reviews = c.Int(nullable: false),
                        Sleeps = c.Int(nullable: false),
                        Bedrooms = c.Int(nullable: false),
                        Bathrooms = c.Int(nullable: false),
                        PropertyType = c.String(),
                        Type = c.Int(nullable: false),
                        AccomodationType = c.String(),
                        Smoking = c.String(),
                        AirCondition = c.String(),
                        SwimmingPool = c.String(),
                        PriceNightlyMin = c.String(),
                        PriceNightlyMax = c.String(),
                        PriceWeeklyMin = c.String(),
                        PriceWeeklyMax = c.String(),
                        Images = c.String(),
                        Status = c.String(),
                        MonthlyExpense = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExpenseNotes = c.String(),
                        ActiveDateStart = c.DateTime(),
                        ActiveDateEnd = c.DateTime(),
                        CommissionId = c.Int(nullable: false),
                        CommissionAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AutoCreateIncomeExpense = c.Boolean(nullable: false),
                        CheckInTime = c.Time(nullable: false, precision: 7),
                        CheckOutTime = c.Time(nullable: false, precision: 7),
                        IsLocallyCreated = c.Boolean(nullable: false),
                        IsParentProperty = c.Boolean(nullable: false),
                        ParentPropertyId = c.Int(nullable: false),
                        SiteType = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        CalendarRemark = c.String(),
                        Currency = c.String(),
                        TimeZoneName = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PropertyBookingDate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PropertyId = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                        IsAvailable = c.Boolean(nullable: false),
                        Price = c.Double(nullable: false),
                        RoomId = c.Long(),
                        SiteType = c.Byte(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        ReservationId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PropertyFee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FeeTypeId = c.Int(nullable: false),
                        ComputeBasis = c.Int(nullable: false),
                        Show = c.Boolean(nullable: false),
                        Method = c.Int(nullable: false),
                        AmountPercent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PropertyId = c.Int(nullable: false),
                        InclusiveOrExclusive = c.Int(nullable: false),
                        IsPreDeducted = c.Boolean(nullable: false),
                        CleaningFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ServiceFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PropertyTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PropertyId = c.Int(nullable: false),
                        TemplateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PropertyType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Property_Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Room",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ListingId = c.Long(nullable: false),
                        RoomId = c.Long(nullable: false),
                        RoomName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SiteType",
                c => new
                    {
                        Code = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        TemplateId = c.Int(nullable: false),
                        Picture = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TemplateName = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        TemplateTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Note",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReservationId = c.Int(nullable: false),
                        NoteType = c.Int(nullable: false),
                        InquiryNote = c.String(),
                        FlightNumber = c.String(),
                        ArrivalDateTime = c.DateTime(),
                        DepartureDateTime = c.DateTime(),
                        DepartureCityCode = c.String(),
                        LeavingDate = c.DateTime(),
                        ArrivalAirportCode = c.String(),
                        MeetingPlace = c.String(),
                        MeetingDateTime = c.DateTime(),
                        People = c.String(),
                        Airline = c.String(),
                        NumberOfPeople = c.Int(),
                        CarType = c.String(),
                        CarColor = c.String(),
                        PlateNumber = c.String(),
                        SourceCity = c.String(),
                        CruiseName = c.String(),
                        AirportCodeDeparture = c.String(),
                        AirportCodeArrival = c.String(),
                        IdentifiableFeature = c.String(),
                        DepartureCity = c.String(),
                        DeparturePort = c.String(),
                        ArrivalPort = c.String(),
                        TerminalStation = c.String(),
                        Name = c.String(),
                        ContactPerson = c.String(),
                        ContactNumber = c.String(),
                        HashValue = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tbRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tbRoleLink",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tbRolePage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        PageName = c.String(),
                        PageUrl = c.String(),
                        Status = c.Boolean(nullable: false),
                        IconPath = c.String(),
                        PageOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TemplateMessage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Message = c.String(),
                        CompanyId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TravelType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ForArrival = c.Boolean(nullable: false),
                        ForDeparture = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedTime = c.DateTime(),
                        TravelNoteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        CompanyId = c.Int(nullable: false),
                        TimeZone = c.String(),
                        ConfirmEmail = c.Boolean(nullable: false),
                        ValidStatus = c.Boolean(nullable: false),
                        Status = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerAssignment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkerId = c.Int(nullable: false),
                        JobTypeId = c.Int(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        ReservationId = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        AssignmentStatus = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerClassification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerException",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkerId = c.Int(nullable: false),
                        ExceptionStart = c.DateTime(nullable: false),
                        ExceptionEnd = c.DateTime(nullable: false),
                        ExceptionType = c.String(),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerJob",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkerId = c.Int(nullable: false),
                        JobTypeId = c.Int(nullable: false),
                        PaymentType = c.Int(nullable: false),
                        PaymentRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerJobType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        ClassificationId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Worker",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        Address = c.String(),
                        Mobile = c.String(),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerSchedule",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UniqueId = c.Int(nullable: false),
                        WorkerId = c.Int(nullable: false),
                        AvailDay = c.String(),
                        AvailTimeStart = c.String(),
                        AvailTimeEnd = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkerType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WorkerType");
            DropTable("dbo.WorkerSchedule");
            DropTable("dbo.Worker");
            DropTable("dbo.WorkerJobType");
            DropTable("dbo.WorkerJob");
            DropTable("dbo.WorkerException");
            DropTable("dbo.WorkerClassification");
            DropTable("dbo.WorkerAssignment");
            DropTable("dbo.User");
            DropTable("dbo.TravelType");
            DropTable("dbo.TemplateMessage");
            DropTable("dbo.tbRolePage");
            DropTable("dbo.tbRoleLink");
            DropTable("dbo.tbRole");
            DropTable("dbo.Note");
            DropTable("dbo.TaskTemplate");
            DropTable("dbo.Task");
            DropTable("dbo.SiteType");
            DropTable("dbo.Room");
            DropTable("dbo.PropertyType");
            DropTable("dbo.PropertyTemplate");
            DropTable("dbo.PropertyFee");
            DropTable("dbo.PropertyBookingDate");
            DropTable("dbo.Property");
            DropTable("dbo.PaymentMethod");
            DropTable("dbo.PaymentHistory");
            DropTable("dbo.PaymentAccount");
            DropTable("dbo.Message");
            DropTable("dbo.MessageRule");
            DropTable("dbo.InquiryTask");
            DropTable("dbo.Inquiry");
            DropTable("dbo.IncomeType");
            DropTable("dbo.IncomePayment");
            DropTable("dbo.Income");
            DropTable("dbo.Inbox");
            DropTable("dbo.HostSessionCookie");
            DropTable("dbo.Host");
            DropTable("dbo.HashDictionary");
            DropTable("dbo.Guest");
            DropTable("dbo.FeeType");
            DropTable("dbo.Expense");
            DropTable("dbo.ExpensePayment");
            DropTable("dbo.ExpenseCategory");
            DropTable("dbo.DefaultRate");
            DropTable("dbo.CreditCards");
            DropTable("dbo.Company");
            DropTable("dbo.CashFlowType");
            DropTable("dbo.Cashflow");
            DropTable("dbo.BookingStatus");
        }
    }
}
