﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class SiteConstants
    {
        public static int CompanyId { get; set; }
        public static int TotalRecordPerPage = 10;
        public static List<HostAccount> hostAccounts = new List<HostAccount>();
    }
}
