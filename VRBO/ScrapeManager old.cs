﻿using BaseScrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Xml;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Collections;
using System.Reflection;
using VRBO.Models;
using VRBO.Enumerations;
using Core.Enumerations;

namespace VRBO
{
    public class ScrapeManager : CommunicationEngine, IDisposable
    {
        public CommunicationPage commPage = null;
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://admin.vrbo.com");
        }
        List<InboxListing> listings = new List<InboxListing>();

        #region LOGIN
        public LoginResult Login(string email, string password, bool forceLogin = false)
        {
            var result = new LoginResult();
            if (!forceLogin && commPage != null && commPage.IsLoggedIn)
            {
                result.Success = true;
                return result;
            }
            string url = "https://admin.vrbo.com/haod";
            commPage = new CommunicationPage(url);
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                if (xd != null)
                {
                    var formAction = xd.SelectSingleNode("//form[@id='login-form']").GetAttributeFromNode("action");
                    var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "login-form");
                    hiddenFields["username"] = email;
                    hiddenFields["password"] = password.Trim();
                    commPage.RequestURL = "https://cas.homeaway.com" + formAction;
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);

                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/dash/"))
                    {
                        commPage.IsLoggedIn = true;
                        var tempCommPage = commPage;
                        tempCommPage.Html = "";

                        var cks = GetAllCookies(commPage.COOKIES);
                        var cookieList = cks.ToList();
                        var serializedCookies = JsonConvert.SerializeObject(cookieList);
                        var saveResult = Core.API.Vrbo.HostSessionCookies.Add(serializedCookies);

                        result.Success = true;
                        result.SessionToken = saveResult.HostSessionCookie.Token;
                        return result;
                    }
                    else if (commPage.IsValid && commPage.Html.Contains("Two-Factor Authentication"))
                    {
                        xd = commPage.ToXml();
                        var phoneNumbers = new Dictionary<string, string>();
                        var radioNodes = xd.SelectNodes("//div[@id='sms-numbers']//div[@class='radio']");
                        foreach (XmlNode radio in radioNodes)
                        {
                            var val = radio.SelectSingleNode(".//input").GetAttributeFromNode("value");
                            var label = radio.GetInnerTextFromNode();
                            phoneNumbers.Add(val, label);
                        }
                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-form");
                        var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-form']").GetAttributeFromNode("action");

                        result.Success = false;
                        result.AirLock = new AirlockViewModel()
                        {
                            HiddenFields = hdnFields,
                            FormAction = "https://cas.homeaway.com" + authformAction,
                            PhoneNumbers = phoneNumbers,
                        };
                        return result;
                    }
                }
            }
            result.Success = false;
            return result;
        }

        #endregion

        #region Airlock

        public AirlockViewModel ProcessAirlockSelection(AirlockViewModel airlockModel)
        {
            var parameters = airlockModel.HiddenFields;
            switch (airlockModel.SelectedChoice)
            {
                case AirlockChoice.SMS:
                    parameters["sendType"] = "SMS";
                    break;
                case AirlockChoice.Call:
                    parameters["sendType"] = "CALL";

                    break;

            }
            parameters["phoneId"] = airlockModel.SelectedPhoneNumber;


            commPage.RequestURL = airlockModel.FormAction;
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = parameters;
            ProcessRequest(commPage);
            if (commPage.IsValid && commPage.Html.Contains("A unique verification code has been sent"))
            {

                var xd = commPage.ToXml();
                var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-verify-form");
                var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-verify-form']").GetAttributeFromNode("action");

                airlockModel.HiddenFields = hdnFields;
                airlockModel.FormAction = "https://cas.homeaway.com" + authformAction;
                return airlockModel;

            }
            return airlockModel;

        }
        public Tuple<bool, string> ProcessAirlock(AirlockViewModel airlockModel)
        {
            var parameters = airlockModel.HiddenFields;
            parameters["rememberMe"] = "true";
            parameters["_rememberMe"] = "on";
            parameters["code"] = airlockModel.Code;
            // parameters.Remove("phoneId");
            // parameters.Remove("sendType");
            //  parameters["htmlFormName"] = "cas2FAVerifyCode:two-factor-auth-verify-form";
            // parameters["flowKey"] = "e24a12a0f298f43bf9125461a5cce8b7as3";
            commPage.RequestURL = airlockModel.FormAction;
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = parameters;
            commPage.Referer = airlockModel.FormAction;
            ProcessRequest(commPage);
            if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("admin.vrbo.com")))
            {
                commPage.IsLoggedIn = true;


                var cks = GetAllCookies(commPage.COOKIES);
                var cookieList = cks.ToList();
                var serializedCookies = JsonConvert.SerializeObject(cookieList);
                var saveResult = Core.API.Vrbo.HostSessionCookies.Add(serializedCookies);

                return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);

            }
            var redirectcount = 1;
            if (!commPage.IsLoggedIn)
            {
            REDIRECT:
                var xd = commPage.ToXml();
                var prms = ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "continue-form");
                prms["_eventId"] = "true";
                commPage.RequestURL = airlockModel.FormAction;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                CommPage.Parameters = prms;
                commPage.Referer = airlockModel.FormAction;
                ProcessRequest(commPage);
                if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("admin.vrbo.com")))
                {
                    commPage.IsLoggedIn = true;
                }
                else
                {
                    if (redirectcount < 2)
                    {
                        redirectcount++;
                        goto REDIRECT;
                    }
                }
            }
            if (commPage.IsLoggedIn)
            {
                var cks = GetAllCookies(commPage.COOKIES);
                var cookieList = cks.ToList();
                var serializedCookies = JsonConvert.SerializeObject(cookieList);
                var saveResult = Core.API.Vrbo.HostSessionCookies.Add(serializedCookies);

                return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);
            }


            return new Tuple<bool, string>(false, "");
        }
        #endregion

        #region Inbox
        public ScrapeResult ScrapeInboxThread(string token, string threadId)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    listings = ScrapeInboxThreadListing(false, InboxThreadType.All);
                    var requiredThread = listings.Where(f => f.Id == threadId).FirstOrDefault();
                    if (requiredThread != null)
                    {
                        var r = ScrapeMessageDetails(requiredThread.Id);
                        sresult.Success = true;
                        sresult.Message = "Operation successful!, Total messages in thread: " + r.Count();
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            return sresult;
        }
        public ScrapeResult ScrapeInbox(string token, bool newOnly, InboxThreadType type)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    listings = ScrapeInboxThreadListing(newOnly, type);
                    var totalItems = MessageDetailWrapper();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        if (newOnly)
                            sresult.Message = "No new message";
                        else
                            sresult.Message = "An error occurred.";
                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }

        
        private List<InboxListing> ScrapeInboxThreadListing(bool newOnly, InboxThreadType threadType)
        {
            List<InboxListing> threadList = new List<InboxListing>();
            var pageNumber = 1;
            var hasMore = false;
            do
            {
                string url = string.Format("https://admin.vrbo.com/rm/proxies/conversations/inbox?_restfully=true&page={0}&pageSize=25&locale=en_US&sort=received&sortOrder=desc", pageNumber);

                switch (threadType)
                {
                    case InboxThreadType.All:
                        break;
                    case InboxThreadType.Inquiries:
                        url = url + "&status=INQUIRY";
                        break;
                    case InboxThreadType.ReservationRequests:
                        url = url + "&status=RESERVATION_REQUEST";
                        break;
                    case InboxThreadType.Reservations:
                        url = url + "&status=RESERVATION";
                        break;
                    case InboxThreadType.Tentative:
                        url = url + "&status=tentative_reservation";
                        break;
                    case InboxThreadType.Cancelled:
                        url = url + "&status=cancelled";
                        break;
                    case InboxThreadType.CurrentStay:
                        url = url + "&status=staying";
                        break;
                    case InboxThreadType.PostStay:
                        url = url + "&status=post_stay";
                        break;
                    default:
                        break;
                }

                hasMore = false;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.Referer = "";
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var ar = (JArray)ob["conversations"];

                    if (ar.Count > 0)
                    {
                        var currentPage = ob["display"]["page"].ToInt();
                        var pageSize = ob["display"]["pageSize"].ToInt();
                        var totalResults = ob["display"]["totalResults"].ToInt();

                        if ((currentPage * pageSize) < totalResults)
                            hasMore = true;

                        pageNumber++;
                        foreach (var item in ar)
                        {
                            var readStatus = item["unreadMessages"].ToBoolean();

                            if (newOnly && (currentPage > 1))
                            {
                                hasMore = false;
                                break;
                            }

                            var id = item["id"].ToSafeString();
                            var guestFirstName = item["correspondent"]["firstName"].ToSafeString();
                            var guestLastName = item["correspondent"]["lastName"].ToSafeString();
                            var guestId = item["correspondent"]["profileUuid"].ToSafeString();
                            var lastMessageAt = item["lastMessageReceivedDate"].ToDateTime();
                            var messageSnippet = item["lastMessage"]["message"].ToSafeString();
                            var inquiryDateRange = item["created"].ToSafeString();
                            var status = item["status"].ToSafeString();
                            var listingName = item["property"]["headline"].ToSafeString();
                            var listingId = item["property"]["id"].ToLong();
                            var reservationId = item["reservationReferenceNumber"].ToSafeString();
                            var stayStartDate = item["stayStartDate"].ToDateTime();
                            var stayEndDate = item["stayEndDate"].ToDateTime();
                            var isInquiryOnly = status == "INQUIRY";

                            threadList.Add(new InboxListing
                            {
                                Id = id,
                                GuestId = guestId,
                                GuestFirstName = guestFirstName,
                                GuestLastName = guestLastName,
                                LastMessageAt = lastMessageAt,
                                Unread = readStatus,
                                MessageSnippet = messageSnippet,
                                InquiryOnly = isInquiryOnly,
                                InquiryDate = inquiryDateRange,
                                Status = status,
                                ListingId = listingId,
                                ReservationCode = reservationId,
                                StayStartDate = stayStartDate,
                                StayEndDate = stayEndDate
                            });
                        }
                    }
                    else
                    {
                        hasMore = false;
                    }
                }
            } while (hasMore);

            return threadList;
        }
        private int MessageDetailWrapper()
        {
            List<Inbox> InboxItems = new List<Inbox>();
            foreach (var item in listings)
            {
                var result = ScrapeMessageDetails(item.Id);

                var thread = Core.API.Vrbo.Inboxes.Add(item.LastMessageAt, item.Unread, item.MessageSnippet, item.GuestId, item.GuestFirstName, item.GuestLastName, item.Id, item.InquiryOnly, item.Status, item.StayStartDate, item.StayEndDate, item.ListingId);
                if (thread.IsSaved)
                {
                    foreach (var msg in result)
                    {
                        var addResult = Core.API.Vrbo.InboxMessages.Add(thread.Thread.ThreadId, msg.MessageId, msg.Message, msg.IsMyMessage, msg.Timestamp);
                        if (addResult.IsDuplicate) //all next would be already in db
                            break;
                    }
                }
            }
            return InboxItems.Count;
        }
        public List<InboxMessage> ScrapeMessageDetails(string id)
        {
            List<InboxMessage> messages = new List<InboxMessage>();
            string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&conversationId={0}&_restfully=true", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(CommPage.Html);
                var ar_temp = (JArray)ob["messages"];
                if (ar_temp.Count > 0)
                {
                    var ar = ar_temp.Where(f => f["from"]["role"].ToSafeString() != "SYSTEM").ToList();
                    foreach (var item in ar)
                    {
                        var userId = item["from"]["participantUuid"].ToSafeString();
                        var myMessage = false;

                        var sender = item["from"]["name"].ToSafeString();
                        if (sender == "You")
                        {
                            myMessage = true;
                        }
                        var profilelink = "https://www.vrbo.com/traveler/profiles/" + userId;
                        var profileImagelink = item["from"]["avatarThumbnailUrl"].ToSafeString();

                        var message = item["body"].ToSafeString();
                        var createdAt = item["sentTimestamp"].ToDateTime();
                        var messageId = item["uuid"].ToSafeString();
                        var type = item["title"].ToSafeString();
                        if (!string.IsNullOrEmpty(message))
                        {
                            messages.Add(new InboxMessage { Message = message, Timestamp = createdAt, MessageId = messageId, IsMyMessage = myMessage });
                        }
                    }
                }
            }
            return messages;
        }
        #endregion

        #region MyListings
        public ScrapeResult ScrapeMyListings(string token)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            isloaded = true;
            if (isloaded)
            {
                string url = "https://admin.vrbo.com/haod/properties.html";
                try
                {
                    int count = 1;
                    List<ListingDetail> propertiesList = new List<ListingDetail>();
                    count++;
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var rows = xd.SelectNodes("//td[@class='listing-summary']");

                            foreach (XmlNode item in rows)
                            {
                                var result = GetPublicUuidAndUserId(commPage.Html);

                                var id = item.GetAttributeFromNode("data-listingguid");

                                var property = MyProperty(id, result.Item1, result.Item2);

                                if (property != null)
                                {
                                    var prop = Core.API.Vrbo.Properties.Add(property.Id, property.ListingUrl, property.Title,
                                        property.Longitude, property.Latitude, property.MinStay.ToInt(), property.Internet,
                                        property.Pets, property.WheelChair, property.Description, 0, property.Sleeps,
                                        property.Bedrooms, property.Bathrooms, property.PropertyType, property.AccomodationType,
                                        property.Smoking, property.AirCondition, property.SwimmingPool, property.Status);
                                    propertiesList.Add(property);
                                }

                                else
                                {

                                }
                            }
                        }
                    }

                    var totalItems = propertiesList.Count();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Invalid Token, Please check you details and try again.";
            }

            return sresult;
        }

        public ListingDetail MyProperty(string id, string ownerPublicuuid, string ownerUserid)
        {
            ListingDetail listing = new ListingDetail();
            listing.OwnerPublicuuid = ownerPublicuuid;
            listing.OwnerGuid = ownerUserid;

            var propertyId = ScrapeHelper.SplitSafely(id, ".", 1).ToSafeString().ToLong();
            listing.Id = propertyId;

            MyProperty_Location(id, listing);
            MyProperty_Description(id, listing);
            MyProperty_Status(id, listing);
            MyProperty_Photos(id, listing);
            MyProperty_Amenities(id, listing);
            MyProperty_Settings(id, listing);
            MyProperty_Rates(id, listing);

            return listing;

        }
        public void MyProperty_Location(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/location.html", id);
                listing.ListingUrl = url;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Longitude = xd.SelectSingleNode("//input[@id='locationAddressLng']").GetAttributeFromNode("value");
                        listing.Latitude = xd.SelectSingleNode("//input[@id='locationAddressLat']").GetAttributeFromNode("value");
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Status(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/experience/panel.json", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Status = ob["listingExperienceState"].ToSafeString();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void MyProperty_Description(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/description.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Title = xd.SelectSingleNode("//input[@id='headlineField_en_tmp']").GetAttributeFromNode("value");
                        listing.Description = xd.SelectSingleNode("//textarea[@id='descriptionField_en_tmp']").GetInnerTextFromNode();
                        listing.HostImageUrl = xd.SelectSingleNode("//img[@id='owner-photo']").GetAttributeFromNode("src");
                        listing.HostAbout = xd.SelectSingleNode("//textarea[@id='ownerListingStoryField_en_tmp']").GetInnerTextFromNode();
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Photos(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/photos.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        List<string> imgsList = new List<string>();
                        var imgNodes = xd.SelectNodes("//ul[@id='ulPhoto']//img");
                        foreach (XmlNode imgNode in imgNodes)
                        {
                            var imgSrc = imgNode.GetAttributeFromNode("src").Replace(".s8", ".s10").Replace(".c8", ".c10");
                            if (!string.IsNullOrEmpty(imgSrc))
                                imgsList.Add(imgSrc);
                        }
                        listing.Images = string.Join("|", imgsList);
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Amenities(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/amenities.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.PropertyType = xd.SelectSingleNode("//select[@id='propertyTypeDropdown']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
                        listing.Bedrooms = xd.SelectNodes("//div[contains(@id,'bedroom-')]").Count.ToInt();
                        listing.Bathrooms = xd.SelectNodes("//div[contains(@id,'bathroom-')]").Count.ToInt();
                        var wheelChairNode = xd.SelectSingleNode("//select[@id='suitabilityFeaturesField_121_dropDown']//option[@selected='selected']");
                        if (wheelChairNode != null)
                            listing.WheelChair = wheelChairNode.NextSibling.GetInnerTextFromNode();
                        else
                            listing.WheelChair = "ask owner";
                        listing.Internet = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_135'][@checked='checked']") == null ? "No" : "Yes";
                        listing.AirCondition = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_45'][@checked='checked']") == null ? "No" : "Yes";


                        var acomTypeVacational = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesFieldDefault'][@checked='checked']");
                        var acomTypeBed = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesField2'][@checked='checked']");
                        if (acomTypeVacational == null && acomTypeBed == null)
                            listing.AccomodationType = "Vacation Rental";
                        else
                            if (acomTypeVacational != null)
                                listing.AccomodationType = "Vacation Rental";

                            else
                                listing.AccomodationType = "Bed & Breakfast";

                        listing.SwimmingPool = xd.SelectNodes("//input[contains(@id,'poolAndSpaFeaturesField_fv_')][@checked='checked']").Count == 0 ? "No" : "Yes";
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Settings(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/gd/proxies/lodgingPolicy/get?listingId={0}&isOnboarding=false&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Sleeps = ob["lodgingPolicy"]["maximumOccupancyRule"]["guests"].ToSafeString();
                    listing.Pets = ob["lodgingPolicy"]["petsAllowedRule"]["allowed"].ToSafeString();
                    listing.Smoking = ob["lodgingPolicy"]["smokingAllowedRule"]["allowed"].ToSafeString();
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Rates(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var html = commPage.Html;
                    var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                    var to = html.IndexOf(";", start);
                    String result = html.Substring(start, to - start);
                    result = result.Trim().TrimEnd(';');
                    var ob = JObject.Parse(result);

                    listing.PriceNightlyMin = ob["basicRate"]["amount"].ToSafeString();
                    listing.MinStay = ob["basicRate"]["minimumStay"].ToSafeString();
                }
            }
            catch (Exception e)
            {

            }
        }
        #endregion

        #region Send Message To Inbox
        public ScrapeResult SendMessageToInbox(string token, string threadId, string messageText)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/conversations/message?conversationId={0}&_restfully=true", threadId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"attachments\":[],\"message\":\"{0}\",\"typeKey\":\"REPLIED\"}}", messageText);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["typeKey"].ToSafeString();
                        if (status == "REPLIED")
                        {
                            sresult.Success = true;
                            ScrapeInbox(token,true, InboxThreadType.All);
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }
        #endregion

        #region Message Thread Metadata
        public ThreadMetadata MessageThreadMetadata(string token, string id)
        {
            ThreadMetadata metadata = new ThreadMetadata();
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                var reservationLink = "";
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);



                    var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                    var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
                    var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
                    var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

                    var fees = ((JArray)ob["quote"]["fees"]).ToList();


                    var rentalCost = "";
                    var serviceFee = "";
                    var cleaningFee = "";
                    var refundableDamageDeposit = "";
                    var taxRate = "";

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                    {
                        rentalCost = rentalNode["amount"]["amount"].ToSafeString();
                        if (rentalNode["feeBasis"]["tax"] != null)
                        {
                            taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

                        }
                    }

                    var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
                    if (cleaningNode != null)
                        cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

                    var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
                    if (serviceNode != null)
                        serviceFee = serviceNode["amount"]["amount"].ToSafeString();

                    var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
                    if (ddNode != null)
                        refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();



                    metadata.ServiceFee = serviceFee;
                    metadata.CleaningFee = cleaningFee;
                    metadata.TotalPayout = payableToHost;
                    metadata.ReservationCost = quoteTotal;
                    metadata.RefundableDamageDeposit = refundableDamageDeposit;
                    metadata.RentalCost = rentalCost;
                    metadata.TaxRate = taxRate;
                    metadata.TotalTax = taxAmount;


                    var conversation = ob["conversation"];
                    if (conversation != null)
                    {
                        metadata.CheckinDate = conversation["checkinDate"].ToSafeString();
                        metadata.CheckoutDate = conversation["checkoutDate"].ToSafeString();


                        metadata.GuestId = conversation["otherParticipants"][0]["profileUuid"].ToSafeString();
                        metadata.GuestName = conversation["otherParticipants"][0]["firstName"].ToSafeString() + " " + conversation["otherParticipants"][0]["lastName"].ToSafeString();
                        metadata.GuestEmail = conversation["otherParticipants"][0]["emailAddress"].ToSafeString();
                        metadata.GuestPhone = conversation["otherParticipants"][0]["phone"].ToSafeString();


                        metadata.HostId = conversation["primaryParticipant"]["profileUuid"].ToSafeString();
                        metadata.HostName = conversation["primaryParticipant"]["firstName"].ToSafeString() + " " + conversation["primaryParticipant"]["lastName"].ToSafeString();
                        metadata.HostEmail = conversation["primaryParticipant"]["emailAddress"].ToSafeString();
                        metadata.HostPhone = conversation["primaryParticipant"]["phone"].ToSafeString();



                        //  metadata.ConfirmationCode = reservationInfo["confirmation_code"].ToSafeString();
                        metadata.NoOfGuests = ob["overview"]["guestCount"].ToSafeString();
                        metadata.ListingName = ob["property"]["unit"]["headline"].ToSafeString();
                        metadata.ListingAddress = ob["property"]["unit"]["address"]["address1"].ToSafeString();
                        metadata.ListingAddress += ", " + ob["property"]["unit"]["address"]["address3"].ToSafeString();
                        metadata.ListingAddress += ", " + ob["property"]["unit"]["address"]["addressLine4"].ToSafeString();

                        //TODO
                        // metadata.NextAvailableCheckinDate;
                        // metadata.NextAvailableCheckoutDate


                        var reservationNode = ob["reservation"];
                        var inquiryNode = ob["inquiry"];
                        if (reservationNode != null)
                        {
                            metadata.CheckinTime = reservationNode["checkinTime"].ToSafeString();
                            metadata.CheckoutTime = reservationNode["checkoutTime"].ToSafeString();

                            metadata.GuestAddress = reservationNode["guest"]["addressLine1"].ToSafeString();
                            metadata.GuestCity = reservationNode["guest"]["city"].ToSafeString();
                            metadata.GuestCountry = reservationNode["guest"]["country"].ToSafeString();

                            metadata.Status = reservationNode["status"].ToSafeString();
                        }
                        else if (inquiryNode != null)
                        {
                            metadata.Status = "Inquiry";
                        }

                    }






                }
            }
            return metadata;
        }
        #endregion

        private string Helper_ListingToTriad(string listingId)
        {
            if (!string.IsNullOrEmpty(listingId) && !listingId.Contains("."))
                return string.Format("321.{0}.1375137", listingId);

            return listingId;
        }

        #region CALENDAR ACTIONS
        public List<Calendar> ScrapeCalendar_Reservations(string token, string listingId, DateTime from, DateTime to)
        {
            //NOTE
            //availabilityStatus=UNAVAILABLE >> Blocked
            //availabilityStatus=RESERVE     >> Reservation
            //type=ICALENDAR_EVENT           >> Airbnb Booking
            var calendarList = new List<Calendar>();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                var listingTriad = Helper_ListingToTriad(listingId);
                var druid = Helper_GetDruidFromListingId(listingTriad);


                string url = string.Format("https://admin.vrbo.com/gd/proxies/calendar/reservations?druidUuid={0}&startDate={1}&endDate={2}&site=vrbo&locale=en_US&_restfully=true", druid, from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));
                try
                {
                    List<ListingDetail> propertiesList = new List<ListingDetail>();

                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var arr = JArray.Parse(commPage.Html);
                        if (arr != null)
                        {
                            foreach (var resevation in arr)
                            {
                                var availabilityStatus = resevation["availabilityStatus"].ToSafeString();
                                var content = resevation["content"].ToSafeString();
                                var convId = resevation["conversationId"].ToSafeString();
                                var resId = resevation["resId"].ToSafeString();
                                var startDate = resevation["startDate"].ToSafeString();
                                var endDate = resevation["endDate"].ToSafeString();
                                var type = resevation["type"].ToSafeString();
                                var iCalId = resevation["icalId"].ToSafeString();

                                //var result = Core.API.Calendar_Reservation.Add(Database, listingId, listingTriad, druid, iCalId, startDate, endDate, content, resId, convId, availabilityStatus, type);

                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
            return calendarList;
        }

        public bool BlockDates(string token, DateTime startDate, DateTime endDate, string listingId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var druid = Helper_GetDruidFromListingId(listingTriad);

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/reservations/create?locale=en_US&site=vrbo&_restfully=true");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"numAdults\":\"0\",\"numChildren\":\"0\",\"bookingChannel\":{{}},\"checkinDate\":\"{0}\",\"checkinTime\":\"16:00\",\"checkoutDate\":\"{1}\",\"checkoutTime\":\"11:00\",\"comments\":\"\",\"expirationDateTime\":\"\",\"guest\":{{\"firstName\":\"\",\"lastName\":\"\",\"emailAddress\":\"\",\"homePhone\":\"\",\"mobilePhone\":\"\",\"faxNumber\":\"\",\"addressLine1\":\"\",\"addressLine2\":\"\",\"city\":\"\",\"state\":\"\",\"country\":\"\",\"postalCode\":\"\",\"hideContactInfo\":false}},\"reservationStatus\":\"BUILDING\",\"status\":\"UNAVAILABLE\",\"source\":\"DASH\",\"druidId\":\"{2}\",\"listingId\":\"{3}\"}}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), druid, listingTriad);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["source"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            //Refresh db
                            ScrapeCalendar_Reservations(token, listingTriad, startDate.AddDays(-1), endDate.AddDays(2));
                            return true;

                        }
                    }

                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
        public bool UnBlockDates(string token, string reservationId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/reservations/cancel?reservationId={0}&_restfully=true", reservationId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.DELETE;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["source"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            //Mark as deleted in db
                            //Core.API.Calendar_Reservation.MarkAsDeleted(Database, reservationId);
                            return true;

                        }
                    }

                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
        #endregion

        #region RESERVATIONS
        public List<Reservation> ScrapeReservations(string token, SyncType syncType, bool reservationRequests = false, bool reservations = false, bool inquiries = false, bool all = false)
        {
            // for testing only remove later
            int hostId = 1;

            var sresult = new ScrapeResult();
            List<Reservation> reservationList = new List<Reservation>();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var threads = new List<InboxListing>();
                    if (all)
                        threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.All));
                    else
                    {
                        if (reservationRequests)
                            threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.ReservationRequests));
                        if (reservations)
                            threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Reservations));
                        if (inquiries)
                            threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Inquiries));
                    }
                    foreach (var thread in threads)
                    {
                        var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", thread.Id);
                        commPage.RequestURL = detailUrl;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.PersistCookies = true;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(CommPage.Html);
                            var ar = (JArray)ob["replyActions"];
                            List<ReplyAction> actions = new List<ReplyAction>();
                            if (ar.Count > 0)
                            {
                                foreach (var item in ar)
                                {
                                    var actionType = item["type"].ToSafeString();
                                    if (actionType == "NONE")
                                    {
                                        actionType = item["buttonTitle"].ToSafeString();
                                    }
                                    ReplyAction act = new ReplyAction
                                    {
                                        Type = actionType
                                    };
                                    if (actionType == "DECLINE_BOOKING")
                                    {
                                        var reasons = (JArray)item["declineReasons"];
                                        Dictionary<string, string> declineReasons = new Dictionary<string, string>();
                                        reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));
                                        act.MetaData = declineReasons;
                                    }
                                    actions.Add(act);
                                }
                            }
                            if (!actions.Any(f => f.Type.Contains("EDIT") || f.Type.Contains("ADD")))
                            {
                                if (thread.Status.Contains("TENTATIVE"))
                                {
                                    actions.Add(new ReplyAction { Type = "EDIT_BOOKING" });
                                }
                            }

                            var adults = ob["quote"]["numAdults"].ToInt();
                            var children = ob["quote"]["numChildren"].ToInt();
                            var guestCount = adults + children;
                            var nights = ob["quote"]["numNights"].ToInt();
                            var pets = ob["quote"]["numPets"].ToInt();
                            var quoteId = ob["quote"]["quoteGuid"].ToSafeString();
                            var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                            var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
                            var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
                            var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

                            var fees = ((JArray)ob["quote"]["fees"]).ToList();

                            var rentalCost = "";
                            var serviceFee = "";
                            var cleaningFee = "";
                            var refundableDamageDeposit = "";
                            var taxRate = "";

                            var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                            if (rentalNode != null)
                            {
                                rentalCost = rentalNode["amount"]["amount"].ToSafeString();
                                if (rentalNode["feeBasis"]["tax"] != null)
                                {
                                    taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();
                                }
                            }

                            var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
                            if (cleaningNode != null)
                                cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

                            var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
                            if (serviceNode != null)
                                serviceFee = serviceNode["amount"]["amount"].ToSafeString();

                            var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
                            if (ddNode != null)
                                refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();

                            var phone = ob["conversation"]["otherParticipants"][0]["phone"].ToSafeString();
                            var email = ob["conversation"]["otherParticipants"][0]["emailAddress"].ToSafeString();
                            var reservationNode = ob["reservation"];
                            var reservationId = "";
                            var reservationCreatedAt = "";
                            var reservationExpiry = "";
                            if (reservationNode != null)
                            {
                                reservationId = ob["reservation"]["id"].ToSafeString();
                                reservationCreatedAt = ob["reservation"]["created"].ToSafeString();
                                reservationExpiry = ob["reservation"]["expirationDateTime"].ToSafeString();
                            }
                            var reservationDeclineDateTime = "";
                            var reservationAcceptedDatetime = "";

                            var messages = ((JArray)ob["messages"]).ToList();

                            var declineNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_DECLINED").FirstOrDefault();
                            if (declineNode != null)
                                reservationDeclineDateTime = declineNode["sentTimestamp"].ToSafeString();

                            var acceptNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_ACCEPTED").FirstOrDefault();
                            if (acceptNode != null)
                                reservationAcceptedDatetime = acceptNode["sentTimestamp"].ToSafeString();

                            var listingUniqueId = ob["property"]["druidUuid"].ToSafeString();

                            var summary = ob["quote"]["quoteTotals"].ToSafeString();

                            var reservation = new Reservation
                            {
                                CheckInDate = thread.StayStartDate,
                                CheckoutDate = thread.StayEndDate,
                                Code = thread.ReservationCode,

                                ServiceFee = serviceFee,
                                CleaningFee = cleaningFee,
                                TotalPayout = payableToHost,//quoteTotal,
                                ReservationCost = quoteTotal, //reservationCost,
                                RefundableDamageDeposit = refundableDamageDeposit,
                                RentalCost = rentalCost,
                                TaxRate = taxRate,
                                TotalTax = taxAmount,
                                ReservationCostSummary = summary,

                                Nights = nights,
                                GuestCount = guestCount,
                                Adult = adults,
                                Children = children,
                                GuestEmail = email,
                                GuestPhone = phone,
                                ListingId = thread.ListingId,
                                GuestId = thread.GuestId,
                                InquiryDatetime = thread.InquiryDate,
                                ReservationId = reservationId, //check this
                                Status = thread.Status,
                                IsActive = (thread.Status == "STAYING" || thread.Status == "BOOKED"),
                                ReplyActions = actions,
                                Pets = pets.ToSafeString(),
                                ReservationRequestDatetime = reservationCreatedAt,
                                ReservationRequestExpiryDatetime = reservationExpiry,
                                ReservationRequestAcceptedDatetime = reservationAcceptedDatetime,
                                ReservationRequestCancelledDatetime = reservationDeclineDateTime,
                                ThreadId = thread.Id,
                                ListingUniqueId = listingUniqueId
                            };

                            var addResult = Core.API.Vrbo.Inquiries.Add(hostId, reservation.Code, reservation.ReservationId,
                                reservation.Status, reservation.CheckInDate, reservation.CheckoutDate, reservation.Nights,
                                reservation.ListingId, reservation.GuestId, reservation.ReservationCost, reservation.CleaningFee,
                                reservation.ServiceFee, reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(),
                                reservation.ReservationRequestDatetime.ToDateTime(),reservation.GuestCount, reservation.Adult, reservation.Children,
                                reservation.ReservationRequestAcceptedDatetime.ToDateTime(), reservation.ReservationRequestCancelledDatetime.ToDateTime(),
                                reservation.ThreadId);

                            if (syncType == SyncType.New && addResult.IsDuplicate)
                            {
                                break;
                            }
                            else
                            {
                                reservationList.Add(reservation);
                            }
                        }
                    }

                    var totalItems = reservationList.Count();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";//, file generated at " + outputPath;
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return reservationList;
        }
        #endregion

        #region RESERVATION ACTIONS

        public ScrapeResult AcceptOffer(string token, string reservationId, string message)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/acceptv2?_restfully=true");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"acceptRequestPaymentType\":\"ONLINE\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\"}}", reservationId, message);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    sresult.Message = "Compage not valid";
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["reservationUuid"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            sresult.Message = "Operation successful.";
                            sresult.Success = true;
                            return sresult;
                        }
                    }
                }
                catch (Exception e) { sresult.Message = e.ToString(); }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public ScrapeResult RejectOffer(string token, string reservationId, string reasonType, string reason, string message)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/declinev2?_restfully=true");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"declineReason\":\"{3}\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\",\"customDeclineReason\":\"{2}\"}}", reservationId, message, reason, reasonType);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["reservationUuid"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            sresult.Message = "Operation successful.";
                            sresult.Success = true;
                            return sresult;
                        }
                    }
                }
                catch (Exception e) { sresult.Message = e.ToString(); }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }
        public Tuple<bool, string, string> GetAlterReservationDetail(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children)
        {
            var reservationCost = "";
            var total = "";
            var isSuccess = false;
            try
            {
                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var quote = ob["quote"];
                    quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                    quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");

                    var fees = ((JArray)ob["quote"]["fees"]).ToList();

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                        rentalNode["amount"]["amount"] = price;

                    quote["numChildren"] = children;
                    quote["numAdults"] = adults;

                    try
                    {
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(CommPage.Html);
                            total = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                            fees = ((JArray)ob["fees"]).ToList();

                            rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                            if (rentalNode != null)
                                reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                            isSuccess = true;
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            catch (Exception e)
            {
            }
            return new Tuple<bool, string, string>(isSuccess, reservationCost, total);
        }
        public bool ProcessAlterReservation(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message = "")
        {
            var alterResult = ProcessAlterReservation(conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Add);
            if (alterResult == false)
                alterResult = ProcessAlterReservation(conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Replace);
            else if (alterResult == false)
                alterResult = ProcessAlterReservation(conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Update);

            return alterResult;
        }
        private bool ProcessAlterReservation(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message, AlterType alterType)
        {
            bool processAlteration = true;
            try
            {
                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var quote = ob["quote"];
                    quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                    quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");
                    var fees = ((JArray)ob["quote"]["fees"]).ToList();

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                        rentalNode["amount"]["amount"] = price;

                    quote["numChildren"] = children;
                    quote["numAdults"] = adults;
                    quote["bodyText"] = message;

                    if (processAlteration)
                    {
                        try
                        {
                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1");
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(CommPage.Html);
                            }
                            switch (alterType)
                            {
                                case AlterType.Update:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                    break;
                                case AlterType.Add:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

                                    break;
                                case AlterType.Replace:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true");

                                    break;
                                default:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                    break;
                            }

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            CommPage.JsonStringToPost = CommPage.Html;
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(CommPage.Html);
                                var quoteGuid = ob["quoteGuid"].ToSafeString();
                                if (string.IsNullOrEmpty(quoteGuid))
                                {
                                    return false;
                                }
                                return true;
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {
                        try
                        {
                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(CommPage.Html);
                                var quoteTotal = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                                fees = ((JArray)ob["fees"]).ToList();

                                var reservationCost = "";

                                rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                                if (rentalNode != null)
                                    reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return false;
        }

        
        #endregion

        #region COOKIES

        public bool ValidateTokenAndLoadCookies(string token)
        {
            var hasValidCookies = LoadCookies(token);
            if (hasValidCookies)
            {
                BasicUserProfile profile = new Models.BasicUserProfile();

                string url = "https://www.vrbo.com/traveler/profile/edit";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    bool asd = commPage.Uri.AbsoluteUri.Contains("/profile/edit");
                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                    {
                        return true;
                    }
                }
                catch (Exception e) { }
            }
            return false;
        }

        public bool LoadCookies(string token)
        {
            var result = false;
            var session = Core.API.Vrbo.HostSessionCookies.HostSessionCookieForToken(token);
            if (session != null)
            {
                try
                {
                    var cookies = JsonConvert.DeserializeObject<List<Cookie>>(session.Cookies);
                    CookieCollection cc = new CookieCollection();
                    foreach (var cok in cookies)
                    {
                        cc.Add(cok);
                    }
                    CookieContainer cont = new CookieContainer();

                    cont.Add(cc);
                    commPage.COOKIES = cont;
                    result = true;
                }
                catch (Exception e)
                {
                    
                }
            }
            else
            {
                commPage.COOKIES = null;
            }
            return result;
        }
        #endregion

        #region HOST PROFILE
        public ScrapeResult ScrapeHostProfile(string token, string password = "")
        {
            var sresult = new ScrapeResult();

            var isLoaded = ValidateTokenAndLoadCookies(token);
            if (isLoaded)
            {
                string url = "https://admin.vrbo.com/haod/up/basic.html";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var firstName = xd.SelectSingleNode("//input[@id='firstName']").GetAttributeFromNode("value");
                            var lastName = xd.SelectSingleNode("//input[@id='lastName']").GetAttributeFromNode("value");
                            var userEmail = xd.SelectSingleNode("//input[@id='emailAddress0']").GetAttributeFromNode("value");
                            var result = GetPublicUuidAndUserId(commPage.Html);
                            var userId = result.Item2;

                            if(!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                            {
                                Core.API.Vrbo.Hosts.Add(userId, firstName, lastName, userEmail, password, "");
                            }
                        }
                    }
                }
                catch { }
            }
            return sresult;
        }
        #endregion

        public Tuple<string, string> GetPublicUuidAndUserId(string html)
        {
            var publicUuid = "";
            var userId = "";
            try
            {
                var start = commPage.Html.IndexOf("publicuuid: ") + "publicuuid: ".Length;
                var end = commPage.Html.IndexOf(",", start);
                var substring = commPage.Html.Substring(start, end - start).Replace(";", "");
                publicUuid = substring.Replace("this.formatUUID(\"", "").Replace("\"", "").Replace(")", "");
            }
            catch { }
            try
            {
                var start = commPage.Html.IndexOf("userid:") + "userid:".Length;
                var end = commPage.Html.IndexOf(")", start);
                var substring = commPage.Html.Substring(start, end - start);

                userId = substring.Replace("this.formatUUID(\"", "").Replace("\"", "").Replace(")", "").Trim();
            }
            catch { }

            return new Tuple<string, string>(publicUuid, userId);
        }

        private string Helper_GetDruidFromListingId(string listingTriad)
        {
            try
            {
                commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/properties/legacySummaries/?_restfully=true&locale=en_US&listingTriad={0}", listingTriad);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var arr = JArray.Parse(commPage.Html);
                    return arr[0]["druid"].ToSafeString();
                }
            }
            catch { }
            return string.Empty;
        }

        #region Shared
        public static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }
        #endregion

        #region DISPOSE
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
        #endregion
    }
}
