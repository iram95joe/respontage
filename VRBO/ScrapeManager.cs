﻿using BaseScrapper;
using Core.Database.Context;
using Core.Enumerations;
using Core.Extensions;
using Core.Helper;
using Core.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Xml;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Collections;
using System.Reflection;
using VRBO.Models;
using VRBO.Enumerations;

namespace VRBO
{
    public class ScrapeManager : CommunicationEngine, IDisposable
    {
        public CommunicationPage commPage = null;
        private Object lObject = new Object();
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://airbnb.com");
        }
        List<InboxListing> listings = new List<InboxListing>();

        #region LOG IN

        public LoginResult Login(string email, string password, bool forceLogin = false)
        {


            var result = new LoginResult();
            if (!forceLogin && commPage != null && commPage.IsLoggedIn)
            {
                result.Success = true;
                return result;
            }
            string url = "https://admin.vrbo.com/haod";
            commPage = new CommunicationPage(url);
            //  commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                if (xd != null)
                {
                    var formAction = xd.SelectSingleNode("//form[@id='login-form']").GetAttributeFromNode("action");
                    var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "login-form");
                    hiddenFields["username"] = email;
                    hiddenFields["password"] = password.Trim();




                    // commPage.RequestURL = "https://cas.homeaway.com" + formAction;
                    commPage.RequestURL = "https://www.vrbo.com" + formAction;
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);

                    if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
                    {
                        xd = commPage.ToXml();
                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
                        var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

                        commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.PersistCookies = true;
                        commPage.Referer = "";
                        commPage.Parameters = hdnFields;
                        ProcessRequest(commPage);

                    }
                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/dash/"))
                    {
                        commPage.IsLoggedIn = true;
                        var tempCommPage = commPage;
                        tempCommPage.Html = "";

                        var cks = GetAllCookies(commPage.COOKIES);
                        var cookieList = cks.ToList();
                        var serializedCookies = JsonConvert.SerializeObject(cookieList);
                        var hostId = Core.API.Vrbo.Hosts.GetHostId(email, password);
                        var saveResult = Core.API.Vrbo.HostSessionCookies.Add(hostId, 1, serializedCookies);

                        result.Success = true;
                        result.SessionToken = saveResult.HostSessionCookie.Token;

                        return result;
                    }
                    else if (commPage.IsValid && commPage.Html.Contains("Two-Factor Authentication"))
                    {
                        //var lockId = HttpUtility.ParseQueryString(commPage.Uri.Query).Get("al_id");

                        xd = commPage.ToXml();
                        var userName = "";
                        var userProfileImageUrl = "";
                        var phoneNumbers = new Dictionary<string, string>();
                        var radioNodes = xd.SelectNodes("//div[@id='sms-numbers']//div[@class='radio']");
                        foreach (XmlNode radio in radioNodes)
                        {
                            var val = radio.SelectSingleNode(".//input").GetAttributeFromNode("value");
                            var label = radio.GetInnerTextFromNode();
                            phoneNumbers.Add(val, label);
                        }
                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-form");
                        var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-form']").GetAttributeFromNode("action");



                        result.Success = false;
                        result.AirLock = new AirlockViewModel()
                        {
                            HiddenFields = hdnFields,
                            //FormAction = "https://cas.homeaway.com" + authformAction,
                            FormAction = "https://www.vrbo.com" + authformAction,
                            PhoneNumbers = phoneNumbers,

                        };
                        return result;


                        /*
                                                //_bootstrap-airlock_data
                                                string airlockUrl = commPage.Uri.AbsoluteUri;
                                                var u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", lockId);
                                                var tt = string.Format("{0}", lockId);
                                                var pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{2}}}}}}}", lockId,currentUserId,phoneNumId);

                                                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                                                var token = cc["_csrf_token"].Value.DecodeURL();

                                                commPage.RequestURL = u;
                                                commPage.PersistCookies = true;
                                                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                                                CommPage.JsonStringToPost = pJson;
                                                commPage.RequestHeaders = new Dictionary<string, string>();
                                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                                                CommPage.RequestHeaders.Add("X-CSRF-Token", token);

                                                commPage.Referer = airlockUrl;
                                                ProcessRequest(commPage);
                                                if (commPage.IsValid)
                                                {
                                                    result.Success = false;
                                                    result.AirLock = new AirlockViewModel()
                                                    {
                                                          LockId = lockId,
                                                           PhoneId = phoneNumId,
                                                            UserId = currentUserId
                                                    };
                                                    return result;

                                                }

                                                */
                    }
                }
            }
            result.Success = false;
            return result;
        }

        #endregion

        #region AIRLOCK

        public AirlockViewModel ProcessAirlockSelection(AirlockViewModel airlockModel)
        {
            var parameters = airlockModel.HiddenFields;
            switch (airlockModel.SelectedChoice)
            {
                case AirlockChoice.SMS:
                    parameters["sendType"] = "SMS";
                    break;
                case AirlockChoice.Call:
                    parameters["sendType"] = "CALL";

                    break;

            }
            parameters["phoneId"] = airlockModel.SelectedPhoneNumber;


            commPage.RequestURL = airlockModel.FormAction;
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            CommPage.Parameters = parameters;
            ProcessRequest(commPage);
            if (commPage.IsValid && commPage.Html.Contains("A unique verification code has been sent"))
            {

                var xd = commPage.ToXml();
                var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-verify-form");
                var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-verify-form']").GetAttributeFromNode("action");

                airlockModel.HiddenFields = hdnFields;
                // airlockModel.FormAction = "https://cas.homeaway.com" + authformAction;
                airlockModel.FormAction = "https://www.vrbo.com" + authformAction;
                return airlockModel;

            }
            return airlockModel;

        }

        public Tuple<bool, string> ProcessAirlock(AirlockViewModel airlockModel)
        {
            var parameters = airlockModel.HiddenFields;
            parameters["rememberMe"] = "true";
            parameters["_rememberMe"] = "on";
            parameters["code"] = airlockModel.Code;
            // parameters.Remove("phoneId");
            // parameters.Remove("sendType");
            //  parameters["htmlFormName"] = "cas2FAVerifyCode:two-factor-auth-verify-form";
            // parameters["flowKey"] = "e24a12a0f298f43bf9125461a5cce8b7as3";
            commPage.RequestURL = airlockModel.FormAction.Replace("https://cas.homeaway.com", "https://www.vrbo.com");
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = parameters;
            commPage.RequestHeaders = new Dictionary<string, string>();
            //commPage.RequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
            commPage.Referer = airlockModel.FormAction;
            ProcessRequest(commPage);
            if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties")))
            {
                commPage.IsLoggedIn = true;


                var cks = GetAllCookies(commPage.COOKIES);
                var cookieList = cks.ToList();
                var serializedCookies = JsonConvert.SerializeObject(cookieList);
                var hostId = Core.API.Vrbo.Hosts.GetHostId(airlockModel.Username, airlockModel.Password);
                var saveResult = Core.API.Vrbo.HostSessionCookies.Add(hostId, 1, serializedCookies);

                return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);
            }
            var redirectcount = 1;
            if (!commPage.IsLoggedIn)
            {
            REDIRECT:
                var xd = commPage.ToXml();
                var prms = ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "continue-form");
                prms["_eventId"] = "submit";
                commPage.RequestURL = airlockModel.FormAction;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                CommPage.Parameters = prms;
                commPage.Referer = airlockModel.FormAction;
                ProcessRequest(commPage);
                if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties")))
                {
                    //ValidateTokenAndLoadCookies1();
                    commPage.IsLoggedIn = true;
                }
                else
                {
                    if (redirectcount < 2)
                    {
                        redirectcount++;
                        goto REDIRECT;
                    }
                }
            }
            if (commPage.IsLoggedIn)
            {
                var cks = GetAllCookies(commPage.COOKIES);
                var cookieList = cks.ToList();
                var serializedCookies = JsonConvert.SerializeObject(cookieList);
                bool successScrapeHost = ScrapeHostProfile(airlockModel.Password, true);

                if (successScrapeHost)
                {
                    var hostId = Core.API.Vrbo.Hosts.GetHostId(airlockModel.Username, airlockModel.Password);
                    var saveResult = Core.API.Vrbo.HostSessionCookies.Add(hostId, 1, serializedCookies);

                    return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);
                }
                else
                {
                    return new Tuple<bool, string>(false, "");
                }
            }


            return new Tuple<bool, string>(false, "");
        }

        #endregion

        #region Inbox
        public ScrapeResult ScrapeInboxThread(string token, string threadId)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    listings = ScrapeInboxThreadListing(false, InboxThreadType.All);
                    var requiredThread = listings.Where(f => f.Id == threadId).FirstOrDefault();
                    if (requiredThread != null)
                    {
                        var r = ScrapeMessageDetails(requiredThread.Id);
                        sresult.Success = true;
                        sresult.Message = "Operation successful!, Total messages in thread: " + r.Count();
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            return sresult;
        }
        public ScrapeResult ScrapeInbox(string token, int hostId, bool newOnly, InboxThreadType type, bool isImporting = false)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    listings = ScrapeInboxThreadListing(newOnly, type);
                    var totalItems = MessageDetailWrapper(hostId, isImporting);
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        if (newOnly)
                            sresult.Message = "No new message";
                        else
                            sresult.Message = "An error occurred.";
                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }

        
        private List<InboxListing> ScrapeInboxThreadListing(bool newOnly, InboxThreadType threadType, CommunicationPage currentCommPage = null)
        {
            List<InboxListing> threadList = new List<InboxListing>();
            var pageNumber = 1;
            var hasMore = false;
            do
            {
                string url = string.Format("https://admin.vrbo.com/rm/proxies/conversations/inbox?_restfully=true&page={0}&pageSize=25&locale=en_US&sort=received&sortOrder=desc", pageNumber);

                switch (threadType)
                {
                    case InboxThreadType.All:
                        break;
                    case InboxThreadType.Inquiry:
                        url = url + "&status=INQUIRY";
                        break;
                    case InboxThreadType.Reservation:
                        url = url + "&status=RESERVATION";
                        break;
                    case InboxThreadType.Tentative:
                        url = url + "&status=TENTATIVE_RESERVATION";
                        break;
                    case InboxThreadType.Cancelled:
                        url = url + "&status=CANCELLED";
                        break;
                    case InboxThreadType.CurrentStay:
                        url = url + "&status=STAYING";
                        break;
                    case InboxThreadType.PostStay:
                        url = url + "&status=POST_STAY";
                        break;
                    default:
                        break;
                }

                hasMore = false;
                JObject ob = null;
                if (currentCommPage == null)
                {
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        ob = JObject.Parse(commPage.Html);
                    }
                }
                else
                {
                    currentCommPage.RequestURL = url;
                    currentCommPage.ReqType = CommunicationPage.RequestType.GET;
                    currentCommPage.PersistCookies = true;
                    currentCommPage.Referer = "";
                    currentCommPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(currentCommPage);
                    if (currentCommPage.IsValid)
                    {
                        ob = JObject.Parse(currentCommPage.Html);
                    }
                }

                var ar = (JArray)ob["conversations"];
                if (ar == null || ar.Count == 0)
                {

                }
                if (ar.Count > 0)
                {

                    var currentPage = ob["display"]["page"].ToInt();
                    var pageSize = ob["display"]["pageSize"].ToInt();
                    var totalResults = ob["display"]["totalResults"].ToInt();

                    if ((currentPage * pageSize) < totalResults)
                        hasMore = true;

                    pageNumber++;
                    foreach (var item in ar)
                    {
                        var readStatus = item["unreadMessages"].ToBoolean();
                        if (newOnly && readStatus == false)
                        {
                            hasMore = false;
                            break;
                        }

                        var id = item["id"].ToSafeString();
                        var guestFirstName = item["correspondent"]["firstName"].ToSafeString();
                        var guestLastName = item["correspondent"]["lastName"].ToSafeString();
                        var guestId = item["correspondent"]["profileUuid"].ToSafeString();
                        var lastMessageAt = item["lastMessageReceivedDate"].ToDateTime();
                        var messageSnippet = item["lastMessage"]["message"].ToSafeString();
                        var inquiryDateRange = item["created"].ToSafeString();
                        var status = item["status"].ToSafeString();
                        var listingName = item["property"]["headline"].ToSafeString();
                        var listingId = item["property"]["id"].ToLong();
                        var reservationId = item["reservationReferenceNumber"].ToSafeString();
                        var stayStartDate = item["stayStartDate"].ToDateTime();
                        var stayEndDate = item["stayEndDate"].ToDateTime();
                        var isInquiryOnly = status == "INQUIRY";

                        threadList.Add(new InboxListing
                        {
                            Id = id,
                            GuestId = guestId,
                            GuestFirstName = guestFirstName,
                            GuestLastName = guestLastName,
                            LastMessageAt = lastMessageAt,
                            Unread = readStatus,
                            MessageSnippet = messageSnippet,
                            InquiryOnly = isInquiryOnly,
                            InquiryDate = inquiryDateRange,
                            Status = status,
                            ListingId = listingId,
                            ReservationCode = reservationId,
                            StayStartDate = stayStartDate,
                            StayEndDate = stayEndDate,
                            Type = threadType.ToSafeString()
                        });
                    }
                }
                else
                {
                    hasMore = false;
                }

            } while (hasMore);

            return threadList;
        }
        private int MessageDetailWrapper(int hostId, bool isImporting)
        {
            int count = 1;
            List<Inbox> InboxItems = new List<Inbox>();
            foreach (var item in listings)
            {
                var result = ScrapeMessageDetails(item.Id);

                var thread = Core.API.Vrbo.Inboxes.Add(hostId, item.LastMessageAt, item.Unread, item.MessageSnippet, item.GuestId, item.GuestFirstName, item.GuestLastName, item.Id, item.InquiryOnly, item.Status, item.StayStartDate, item.StayEndDate, item.ListingId);
                if (thread.IsSaved)
                {
                    foreach (var msg in result)
                    {
                        var addResult = Core.API.Vrbo.InboxMessages.Add(thread.Thread.ThreadId, msg.MessageId, msg.Message, msg.IsMyMessage, msg.Timestamp);

                        if (isImporting)
                        {
                            GlobalVariables.ImportedMessagesCount = count;
                            ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                            count++;
                        }

                        if (addResult.IsDuplicate) //all next would be already in db
                            break;
                    }
                }
            }

            //ImportHub.DoneImporting(GlobalVariables.UserId.ToString(), ImportStage.Messages);
            return InboxItems.Count;
        }
        public List<InboxMessage> ScrapeMessageDetails(string id)
        {
            List<InboxMessage> messages = new List<InboxMessage>();
            string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&conversationId={0}&_restfully=true", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(CommPage.Html);
                var ar_temp = (JArray)ob["messages"];
                if (ar_temp.Count > 0)
                {
                    var ar = ar_temp.Where(f => f["from"]["role"].ToSafeString() != "SYSTEM").ToList();
                    foreach (var item in ar)
                    {
                        var userId = item["from"]["participantUuid"].ToSafeString();
                        var myMessage = false;

                        var sender = item["from"]["name"].ToSafeString();
                        if (sender == "You")
                        {
                            myMessage = true;
                        }
                        var profilelink = "https://www.vrbo.com/traveler/profiles/" + userId;
                        var profileImagelink = item["from"]["avatarThumbnailUrl"].ToSafeString();

                        var message = item["body"].ToSafeString();
                        var createdAt = item["sentTimestamp"].ToDateTime();
                        var messageId = item["uuid"].ToSafeString();
                        var type = item["title"].ToSafeString();
                        if (!string.IsNullOrEmpty(message))
                        {
                            messages.Add(new InboxMessage { Message = message, Timestamp = createdAt, MessageId = messageId, IsMyMessage = myMessage });
                        }
                    }
                }
            }
            return messages;
        }
        #endregion

        #region MyListings
        public ScrapeResult ScrapeMyListings(string token, int hostId, bool isImporting = false)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = "https://admin.vrbo.com/haod/properties.html";
                try
                {
                    int count = 1;
                    List<ListingDetail> propertiesList = new List<ListingDetail>();
                    count++;
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var rows = xd.SelectNodes("//td[@class='listing-summary']");
                            
                            foreach (XmlNode item in rows)
                            {
                                var result = GetPublicUuidAndUserId(commPage.Html);

                                var id = item.GetAttributeFromNode("data-listingguid");

                                var property = MyProperty(id, result.Item1, result.Item2);

                                if (property != null)
                                {
                                    var listingTriad = Helper_ListingToTriad(property.Id.ToString());
                                    url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                                    commPage.RequestURL = url;
                                    commPage.ReqType = CommunicationPage.RequestType.GET;

                                    ProcessRequest(commPage);
                                    if (commPage.IsValid)
                                    {
                                        string calendarRemark = "VRBO_NEW";
                                         
                                        try
                                        {
                                            string flagText = commPage.ToXml().SelectSingleNode("//div[@class='le-rates-header']//h2").InnerText;

                                            if (flagText == "Manage Rates to Maximize Revenue")
                                                calendarRemark = "VRBO_OLD";
                                        }
                                        catch (Exception e)
                                        {

                                        }

                                        var prop = Core.API.Vrbo.Properties.Add(hostId, property.Id, property.ListingUrl, property.Title,
                                        property.Longitude, property.Latitude, property.MinStay.ToInt(), property.Internet,
                                        property.Pets, property.WheelChair, property.Description, 0, property.Sleeps,
                                        property.Bedrooms, property.Bathrooms, property.PropertyType, property.AccomodationType,
                                        property.Smoking, property.AirCondition, property.SwimmingPool, property.Status,
                                        calendarRemark);

                                        propertiesList.Add(property);

                                        if (isImporting)
                                        {
                                            GlobalVariables.ImportedPropertyCount = count;
                                            ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
                                            count++;
                                        }
                                    }
                                }
                                else
                                {

                                }
                            }
                        }
                    }

                    var totalItems = propertiesList.Count();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Invalid Token, Please check you details and try again.";
            }


            //if (isImporting)
            //    ImportHub.DoneImporting(GlobalVariables.UserId.ToString(), ImportStage.Property);

            return sresult;
        }

        public ListingDetail MyProperty(string id, string ownerPublicuuid, string ownerUserid)
        {
            ListingDetail listing = new ListingDetail();
            listing.OwnerPublicuuid = ownerPublicuuid;
            listing.OwnerGuid = ownerUserid;

            var propertyId = ScrapeHelper.SplitSafely(id, ".", 1).ToSafeString().ToLong();
            listing.Id = propertyId;
            listing.ListingUrl = string.Format("https://www.vrbo.com/{0}", propertyId);

            MyProperty_Location(id, listing);
            MyProperty_Description(id, listing);
            MyProperty_Status(id, listing);
            MyProperty_Photos(id, listing);
            MyProperty_Amenities(id, listing);
            MyProperty_Settings(id, listing);
            MyProperty_Rates(id, listing);

            return listing;

        }
        public void MyProperty_Location(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/loc.html", id);
                //listing.ListingUrl = url;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Longitude = xd.SelectSingleNode("//input[@id='locationAddressLng']").GetAttributeFromNode("value");
                        listing.Latitude = xd.SelectSingleNode("//input[@id='locationAddressLat']").GetAttributeFromNode("value");
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Status(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/experience/panel.json", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Status = ob["listingExperienceState"].ToSafeString();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void MyProperty_Description(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/description.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Title = xd.SelectSingleNode("//input[@id='headlineField_en_tmp']").GetAttributeFromNode("value");
                        listing.Description = xd.SelectSingleNode("//textarea[@id='descriptionField_en_tmp']").GetInnerTextFromNode();
                        listing.HostImageUrl = xd.SelectSingleNode("//img[@id='owner-photo']").GetAttributeFromNode("src");
                        listing.HostAbout = xd.SelectSingleNode("//textarea[@id='ownerListingStoryField_en_tmp']").GetInnerTextFromNode();
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Photos(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/photos.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        List<string> imgsList = new List<string>();
                        var imgNodes = xd.SelectNodes("//ul[@id='ulPhoto']//img");
                        foreach (XmlNode imgNode in imgNodes)
                        {
                            var imgSrc = imgNode.GetAttributeFromNode("src").Replace(".s8", ".s10").Replace(".c8", ".c10");
                            if (!string.IsNullOrEmpty(imgSrc))
                                imgsList.Add(imgSrc);
                        }
                        listing.Images = string.Join("|", imgsList);
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Amenities(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/amenities.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.PropertyType = xd.SelectSingleNode("//select[@id='propertyTypeDropdown']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
                        listing.Bedrooms = xd.SelectNodes("//div[contains(@id,'bedroom-')]").Count.ToInt();
                        listing.Bathrooms = xd.SelectNodes("//div[contains(@id,'bathroom-')]").Count.ToInt();
                        var wheelChairNode = xd.SelectSingleNode("//select[@id='suitabilityFeaturesField_121_dropDown']//option[@selected='selected']");
                        if (wheelChairNode != null)
                            listing.WheelChair = wheelChairNode.NextSibling.GetInnerTextFromNode();
                        else
                            listing.WheelChair = "ask owner";
                        listing.Internet = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_135'][@checked='checked']") == null ? "No" : "Yes";
                        listing.AirCondition = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_45'][@checked='checked']") == null ? "No" : "Yes";


                        var acomTypeVacational = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesFieldDefault'][@checked='checked']");
                        var acomTypeBed = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesField2'][@checked='checked']");
                        if (acomTypeVacational == null && acomTypeBed == null)
                            listing.AccomodationType = "Vacation Rental";
                        else
                            if (acomTypeVacational != null)
                                listing.AccomodationType = "Vacation Rental";

                            else
                                listing.AccomodationType = "Bed & Breakfast";

                        listing.SwimmingPool = xd.SelectNodes("//input[contains(@id,'poolAndSpaFeaturesField_fv_')][@checked='checked']").Count == 0 ? "No" : "Yes";
                    }
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Settings(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/gd/proxies/lodgingPolicy/get?listingId={0}&isOnboarding=false&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Sleeps = ob["lodgingPolicy"]["maximumOccupancyRule"]["guests"].ToSafeString();
                    listing.Pets = ob["lodgingPolicy"]["petsAllowedRule"]["allowed"].ToSafeString();
                    listing.Smoking = ob["lodgingPolicy"]["smokingAllowedRule"]["allowed"].ToSafeString();
                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Rates(string id, ListingDetail listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var html = commPage.Html;
                    var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                    var to = html.IndexOf(";", start);
                    String result = html.Substring(start, to - start);
                    result = result.Trim().TrimEnd(';');
                    var ob = JObject.Parse(result);

                    listing.PriceNightlyMin = ob["basicRate"]["amount"].ToSafeString();
                    listing.MinStay = ob["basicRate"]["minimumStay"].ToSafeString();
                }
            }
            catch (Exception e)
            {

            }
        }
        #endregion

        #region Send Message To Inbox
        public ScrapeResult SendMessageToInbox(string token, string threadId, string messageText)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&declineReasonsVersion=2&include=conversation&conversationId={0}&_restfully=true", threadId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.Referer = "";
                    ProcessRequest(commPage);


                    //commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/conversations/message?conversationId={0}&_restfully=true", threadId);
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/conversations/message?conversationId={0}&blockUntilResponseScore=true&_restfully=true", threadId);

                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    //CommPage.JsonStringToPost = string.Format("{{\"attachments\":[],\"message\":\"{0}\",\"typeKey\":\"REPLIED\"}}", messageText);
                    commPage.JsonStringToPost = string.Format("{{\"attachments\":[],\"message\":\"{0}\",\"typeKey\":\"REPLIED\"}}", messageText);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    //CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["typeKey"].ToSafeString();
                        if (status == "REPLIED")
                        {
                            sresult.Success = true;
                            //ScrapeInbox(token,true, InboxThreadType.All);
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }
        #endregion

        #region Message Thread Metadata
        public ThreadMetadata MessageThreadMetadata(string token, string id)
        {
            ThreadMetadata metadata = new ThreadMetadata();
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                var reservationLink = "";
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);



                    var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                    var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
                    var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
                    var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

                    var fees = ((JArray)ob["quote"]["fees"]).ToList();


                    var rentalCost = "";
                    var serviceFee = "";
                    var cleaningFee = "";
                    var refundableDamageDeposit = "";
                    var taxRate = "";

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                    {
                        rentalCost = rentalNode["amount"]["amount"].ToSafeString();
                        if (rentalNode["feeBasis"]["tax"] != null)
                        {
                            taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

                        }
                    }

                    var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
                    if (cleaningNode != null)
                        cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

                    var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
                    if (serviceNode != null)
                        serviceFee = serviceNode["amount"]["amount"].ToSafeString();

                    var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
                    if (ddNode != null)
                        refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();



                    metadata.ServiceFee = serviceFee;
                    metadata.CleaningFee = cleaningFee;
                    metadata.TotalPayout = payableToHost;
                    metadata.ReservationCost = quoteTotal;
                    metadata.RefundableDamageDeposit = refundableDamageDeposit;
                    metadata.RentalCost = rentalCost;
                    metadata.TaxRate = taxRate;
                    metadata.TotalTax = taxAmount;


                    var conversation = ob["conversation"];
                    if (conversation != null)
                    {
                        metadata.CheckinDate = conversation["checkinDate"].ToSafeString();
                        metadata.CheckoutDate = conversation["checkoutDate"].ToSafeString();


                        metadata.GuestId = conversation["otherParticipants"][0]["profileUuid"].ToSafeString();
                        metadata.GuestName = conversation["otherParticipants"][0]["firstName"].ToSafeString() + " " + conversation["otherParticipants"][0]["lastName"].ToSafeString();
                        metadata.GuestEmail = conversation["otherParticipants"][0]["emailAddress"].ToSafeString();
                        metadata.GuestPhone = conversation["otherParticipants"][0]["phone"].ToSafeString();


                        metadata.HostId = conversation["primaryParticipant"]["profileUuid"].ToSafeString();
                        metadata.HostName = conversation["primaryParticipant"]["firstName"].ToSafeString() + " " + conversation["primaryParticipant"]["lastName"].ToSafeString();
                        metadata.HostEmail = conversation["primaryParticipant"]["emailAddress"].ToSafeString();
                        metadata.HostPhone = conversation["primaryParticipant"]["phone"].ToSafeString();



                        //  metadata.ConfirmationCode = reservationInfo["confirmation_code"].ToSafeString();
                        metadata.NoOfGuests = ob["overview"]["guestCount"].ToSafeString();
                        metadata.ListingName = ob["property"]["unit"]["headline"].ToSafeString();
                        metadata.ListingAddress = ob["property"]["unit"]["address"]["address1"].ToSafeString();
                        metadata.ListingAddress += ", " + ob["property"]["unit"]["address"]["address3"].ToSafeString();
                        metadata.ListingAddress += ", " + ob["property"]["unit"]["address"]["addressLine4"].ToSafeString();

                        //TODO
                        // metadata.NextAvailableCheckinDate;
                        // metadata.NextAvailableCheckoutDate


                        var reservationNode = ob["reservation"];
                        var inquiryNode = ob["inquiry"];
                        if (reservationNode != null)
                        {
                            metadata.CheckinTime = reservationNode["checkinTime"].ToSafeString();
                            metadata.CheckoutTime = reservationNode["checkoutTime"].ToSafeString();

                            metadata.GuestAddress = reservationNode["guest"]["addressLine1"].ToSafeString();
                            metadata.GuestCity = reservationNode["guest"]["city"].ToSafeString();
                            metadata.GuestCountry = reservationNode["guest"]["country"].ToSafeString();

                            metadata.Status = reservationNode["status"].ToSafeString();
                        }
                        else if (inquiryNode != null)
                        {
                            metadata.Status = "Inquiry";
                        }

                    }
                }
            }
            return metadata;
        }
        #endregion

        #region CALENDAR ACTIONS

        public string ScrapeCalendarReservationId(string token, string listingId, DateTime from, DateTime to)
        {
            //NOTE
            //availabilityStatus=UNAVAILABLE >> Blocked
            //availabilityStatus=RESERVE     >> Reservation
            //type=ICALENDAR_EVENT           >> Airbnb Booking

            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                var listingTriad = Helper_ListingToTriad(listingId);
                var druid = Helper_GetDruidFromListingId(listingTriad);

                List<string> reservationIds = new List<string>();

                string url = string.Format("https://admin.vrbo.com/gd/proxies/calendar/reservations?druidUuid={0}&startDate={1}&endDate={2}&site=vrbo&locale=en_US&_restfully=true", druid, from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));

                try
                {
                    List<ListingDetail> propertiesList = new List<ListingDetail>();

                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var arr = JArray.Parse(commPage.Html);
                        if (arr != null)
                        {
                            //foreach (var resevation in arr)
                            //{
                            //    var availabilityStatus = resevation["availabilityStatus"].ToSafeString();
                            //    var content = resevation["content"].ToSafeString();
                            //    var convId = resevation["conversationId"].ToSafeString();
                            //    var resId = resevation["resId"].ToSafeString();
                            //    var startDate = resevation["startDate"].ToSafeString();
                            //    var endDate = resevation["endDate"].ToSafeString();
                            //    var type = resevation["type"].ToSafeString();
                            //    var iCalId = resevation["icalId"].ToSafeString();

                            //}

                            foreach (var reservation in arr)
                            {
                                if (reservation["type"].ToSafeString() == "RESERVATION" &&
                                    reservation["startDate"].ToSafeString() == from.ToString("yyyy-MM-dd") &&
                                    reservation["endDate"].ToSafeString() == to.ToString("yyyy-MM-dd"))
                                    return reservation["resId"].ToSafeString();
                            }

                            //return arr[0]["resId"].ToSafeString();
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }

            return null;
        }

        public List<Core.Database.Entity.PropertyBookingDate> ScrapeCalendar(string token, string listingId, DateTime startDate, DateTime endDate)
        {
            var calendarList = new List<Core.Database.Entity.PropertyBookingDate>();
            var ratesList = new List<Core.Database.Entity.PropertyBookingDate>();
            var blockList = new List<Core.Database.Entity.PropertyBookingDate>();

            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var druid = Helper_GetDruidFromListingId(listingTriad);
                    var unitUrl = Helper_GetUnitUrl(listingTriad);

                    string url = string.Format("https://admin.vrbo.com/gd/proxies/px-rates/rateSummary?unitUrl={0}&startDate={1}&endDate={2}&_restfully=true", unitUrl, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        var arr = JObject.Parse(commPage.Html);
                        if (arr != null)
                        {
                            #region Scrape Rate List

                            var rateOverrides = JObject.Parse(arr["rateOverrides"].ToString());

                            foreach (var year in rateOverrides.Properties())
                            {
                                foreach (var month in JObject.Parse(year.Value.ToString()).Properties())
                                {
                                    foreach (var day in JObject.Parse(month.Value.ToString()).Properties())
                                    {
                                        Core.Database.Entity.PropertyBookingDate pbd = new Core.Database.Entity.PropertyBookingDate();
                                        var date = (year.Name + "-" + month.Name + "-" + day.Name).ToDateTime();
                                        pbd.Date = date;

                                        try
                                        {
                                            pbd.Price = JObject.Parse(day.Value.ToString())["overrideRate"].ToLong();

                                            if (pbd.Price == 0)
                                                pbd.Price = (arr["baseRate"][date.DayOfWeek.ToString().ToLower()]).ToDouble();
                                        }
                                        catch (Exception e)
                                        {
                                            pbd.Price = (arr["baseRate"][date.DayOfWeek.ToString().ToLower()]).ToDouble();
                                        }

                                        pbd.PropertyId = listingId.ToLong();
                                        pbd.SiteType = 2;
                                        //pbd.CompanyId = GlobalVariables.CompanyId;
                                        pbd.IsAvailable = true;

                                        ratesList.Add(pbd);
                                    }
                                }
                            }

                            #endregion

                            #region Scrape Block List

                            url = string.Format("https://admin.vrbo.com/gd/proxies/calendar/reservations?druidUuid={0}&startDate={1}&endDate={2}&site=vrbo&locale=en_US&_restfully=true", druid, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

                            commPage.RequestURL = url;
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                var array = JArray.Parse(commPage.Html);

                                if (array != null)
                                {
                                    foreach (var reservation in array)
                                    {
                                        var availabilityStatus = reservation["availabilityStatus"].ToSafeString();
                                        var resId = reservation["resId"].ToSafeString();
                                        var sDate = reservation["startDate"].ToSafeString().ToDateTime();
                                        var eDate = reservation["endDate"].ToSafeString().ToDateTime();

                                        bool available = availabilityStatus == "UNAVAILABLE" ? false : true;

                                        for (DateTime date = sDate; date <= eDate; date = date.AddDays(1))
                                        {
                                            blockList.Add(new Core.Database.Entity.PropertyBookingDate
                                            {
                                                IsAvailable = available,
                                                Date = date,
                                                ReservationId = resId
                                            });
                                        }
                                    }

                                    #region Merge To Calendar List

                                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                                    {
                                        var r = ratesList.Where(x => x.Date.Date == date.Date).First();
                                        var b = blockList.Where(x => x.Date.Date == date.Date).First();

                                        calendarList.Add(new Core.Database.Entity.PropertyBookingDate
                                        {
                                            PropertyId = r.PropertyId,
                                            SiteType = r.SiteType,
                                            //CompanyId = r.CompanyId,
                                            IsAvailable = b.IsAvailable,
                                            Date = r.Date,
                                            ReservationId = b.ReservationId
                                        });
                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }
                }
                catch (Exception e)
                {
                    ElmahLogger.LogInfo("Error while scrapping calendar !");
                }
            }
            else
            {
                ElmahLogger.LogInfo("Error while scrapping calendar !");
            }

            return calendarList;
        }

        public Tuple<bool, string, string> BlockDates(string token, DateTime startDate, DateTime endDate, string listingId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var druid = Helper_GetDruidFromListingId(listingTriad);

                    commPage.RequestURL = "https://admin.vrbo.com/pxcalendars/reservations/" + listingTriad;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/pxcalendars/proxies/reservations/create?locale=en_US&site=vrbo&_restfully=true");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"numAdults\":\"0\",\"numChildren\":\"0\",\"bookingChannel\":{{}},\"checkinDate\":\"{0}\",\"checkinTime\":\"16:00\",\"checkoutDate\":\"{1}\",\"checkoutTime\":\"11:00\",\"comments\":\"\",\"expirationDateTime\":\"\",\"guest\":{{\"firstName\":\"\",\"lastName\":\"\",\"emailAddress\":\"\",\"homePhone\":\"\",\"mobilePhone\":\"\",\"faxNumber\":\"\",\"addressLine1\":\"\",\"addressLine2\":\"\",\"city\":\"\",\"state\":\"\",\"country\":\"\",\"postalCode\":\"\",\"hideContactInfo\":false}},\"reservationStatus\":\"BUILDING\",\"status\":\"UNAVAILABLE\",\"source\":\"DASH\",\"druidId\":\"{2}\",\"listingId\":\"{3}\"}}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), druid, listingTriad);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                    
                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["source"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            string reservationId = ScrapeCalendarReservationId(token, listingId, startDate, endDate);
                            return new Tuple<bool, string, string>(true, "Success", reservationId);
                        }
                    }

                }
                catch (Exception e)
                {

                }
            }
            return new Tuple<bool, string, string>(false, "An Error Occured", "");
        }

        public bool UnBlockDates(string token, string listingId, string reservationId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);

                    commPage.RequestURL = "https://admin.vrbo.com/pxcalendars/reservations/" + listingTriad;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/pxcalendars/proxies/reservations/cancel?_restfully=true&reservationId={0}", reservationId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.DELETE;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/plain, */*");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["source"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            //Mark as deleted in db
                            //Core.API.Calendar_Reservation.MarkAsDeleted(Database, reservationId);
                            return true;

                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }

        public bool UpdateAvailability(string token, string listingId, bool available, DateTime startDate, DateTime endDate)//reservationId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    string availability = "";

                    int days = (int)(endDate - startDate).TotalDays;
                    for (int day = 0; day < days; day++)
                    {
                        availability = availability + (available ? "N" : "Y");
                    }

                    var listingTriad = Helper_ListingToTriad(listingId);
                    var druid = Helper_GetDruidFromListingId(listingTriad);
                    var unitUrl = Helper_GetUnitUrl(listingTriad);

                    commPage.RequestURL = "https://admin.vrbo.com/pxcalendars/reservations/" + listingTriad;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/pxcalendars/proxies/px-rates/availabilityOverrides?_restfully=true&unitUrl={0}", unitUrl);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                    CommPage.JsonStringToPost = string.Format("{{\"availability\":\"{0}\",\"startDate\":\"{1}\"}}", availability, startDate.ToString("yyyy-MM-dd"));
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/plain, */*");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";
                    ProcessRequest(commPage);

                    if (commPage.IsValid || (commPage.StatusCode == HttpStatusCode.NoContent && commPage.Html == ""))
                        return true;
                    else return false;
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }

        public Tuple<bool, string> SetRateOnOldCalendar(string token, string listingId, string name, DateTime startDate, DateTime endDate, string minStay, string nightlyPrice)
        {
            var resultBool = false;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;

                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {


                        var html = commPage.Html;
                        var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                        var to = html.IndexOf(";", start);
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');
                        var ob = JObject.Parse(result);


                        var newRateJson = string.Format("{{\"name\":\"{0}\",\"range\":{{\"start\":{{\"asCalendar\":\"{1}\",\"day\":{2},\"month\":{3},\"year\":{4}}},\"end\":{{\"asCalendar\":\"{5}\",\"day\":{6},\"month\":{7},\"year\":{8}}}}},\"minStay\":\"{9}\",\"nightlyAmount\":{10},\"weeklyAmount\":\"\",\"monthlyAmount\":\"\",\"changeoverDay\":\"\",\"chargeWeekends\":false,\"chargeAdditionalGuests\":false,\"additionalGuestRate\":\"\",\"nightlyAmounts\":[{{\"amount\":{10},\"minStay\":\"{9}\",\"payType\":\"PROPERTY\",\"periodType\":\"NIGHTLY\"}}],\"rateType\":\"SEASON\"}}",
                            name, startDate.ToString("MM/dd/yyyy"), startDate.Day, startDate.Month, startDate.Year, endDate.ToString("MM/dd/yyyy"), endDate.Day, endDate.Month, endDate.Year, minStay, nightlyPrice);
                        var newJObject = JToken.Parse(newRateJson);

                        var newCsrfToken = Helper_GetCSRFToken();

                        ((JArray)ob["seasonRates"]).Add(newJObject);

                        var newJson = JsonConvert.SerializeObject(ob);
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/haolb/{0}/le/save.html", listingTriad);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("requestJSON", newJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        CommPage.RequestHeaders.Add("X-Csrf-Token-Requested", newCsrfToken);
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        commPage.RequestHeaders.Add("Ajax", "true");
                        commPage.Referer = url;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var resultOb = JObject.Parse(commPage.Html);
                            if (resultOb["status"].ToSafeString() == "0")
                                return new Tuple<bool, string>(true, "Successfully changed.");
                            else
                                return new Tuple<bool, string>(false, "The dates are overlapping with existing seasonal rates.");
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return new Tuple<bool, string>(false, "The dates are overlapping with existing seasonal rates.");
        }

        public Tuple<bool, string> EditRateOnOldCalendar(string token, string listingId, string existingName, DateTime existingStartDate, DateTime existingEndDate, string name, DateTime startDate, DateTime endDate, string minStay, string nightlyPrice)
        {
            var resultBool = false;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;

                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {


                        var html = commPage.Html;
                        var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                        var to = html.IndexOf(";", start);
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');
                        var ob = JObject.Parse(result);

                        var seasonRates = ((JArray)ob["seasonRates"]);
                        var existing = seasonRates.Where(f => f["name"].ToSafeString() == Uri.EscapeUriString(existingName) && f["range"]["start"]["asDate"].ToSafeString() == existingStartDate.ToString("MM/dd/yyyy") && f["range"]["end"]["asDate"].ToSafeString() == existingEndDate.ToString("MM/dd/yyyy")).FirstOrDefault();
                        if (existing == null)
                            return new Tuple<bool, string>(false, "No exising rate with the given name and date");
                        
                        ((JArray)ob["seasonRates"]).Remove(existing);

                        bool isOverlapping = seasonRates.Any(f => f["range"]["start"]["asDate"].ToDateTime().Date <= endDate.Date && startDate.Date <= f["range"]["end"]["asDate"].ToDateTime().Date);
                        if (isOverlapping)
                            return new Tuple<bool, string>(false, "The dates are overlapping with existing seasonal rates.");

                        //bool overlapped = false;
                        //string messageResult = "";

                        //foreach (var seasonRate in seasonRates)
                        //{
                        //    if (seasonRate["range"]["start"]["asDate"].ToDateTime().Date <= endDate.Date && startDate.Date <= seasonRate["range"]["end"]["asDate"].ToDateTime().Date)
                        //    {

                        //    }
                        //}

                        //if (!overlapped)
                        //{
                            var feesJson = "";
                            var taxjson = "";
                            var rateVersion = "V2";
                            var fees = (JArray)ob["costs"];
                            var checkedProp = new JProperty("checked", "true");
                            fees[0]["name"].Parent.AddAfterSelf(checkedProp);
                            feesJson = JsonConvert.SerializeObject(fees);

                            var taxIncluded = ob["taxPercentage"]["included"].ToSafeString();
                            var rate = ob["taxPercentage"]["rate"].ToSafeString();

                            taxjson = string.Format("{{\"included\":\"{0}\",\"rate\":{1}}}", taxIncluded.ToLower(), rate);



                            var newRateJson = string.Format("{{\"name\":\"{0}\",\"range\":{{\"start\":{{\"asCalendar\":\"{1}\",\"day\":{2},\"month\":{3},\"year\":{4}}},\"end\":{{\"asCalendar\":\"{5}\",\"day\":{6},\"month\":{7},\"year\":{8}}}}},\"minStay\":\"{9}\",\"nightlyAmount\":{10},\"weeklyAmount\":\"\",\"monthlyAmount\":\"\",\"changeoverDay\":\"\",\"chargeWeekends\":false,\"chargeAdditionalGuests\":false,\"additionalGuestRate\":\"\",\"nightlyAmounts\":[{{\"amount\":{10},\"minStay\":\"{9}\",\"payType\":\"PROPERTY\",\"periodType\":\"NIGHTLY\"}}],\"rateType\":\"SEASON\"}}",
                                name, startDate.ToString("MM/dd/yyyy"), startDate.Day, startDate.Month, startDate.Year, endDate.ToString("MM/dd/yyyy"), endDate.Day, endDate.Month, endDate.Year, minStay, nightlyPrice);

                            var newCsrfToken = Helper_GetCSRFToken();

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/haolb/{0}/rates/setup/validateNonNegativeQuote.html", listingTriad);
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.POST;
                            commPage.Parameters = new Dictionary<string, string>();
                            commPage.Parameters.Add("feesJson", feesJson);
                            commPage.Parameters.Add("rateJson", newRateJson);
                            commPage.Parameters.Add("taxJson", taxjson);
                            commPage.Parameters.Add("rateVersion", rateVersion);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("X-Csrf-Token-Requested", newCsrfToken);
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            commPage.RequestHeaders.Add("Ajax", "true");
                            commPage.Referer = url;
                            ProcessRequest(commPage);
                            if (commPage.IsValid && commPage.Html.Equals("{}"))
                            {

                                var newJObject = JToken.Parse(newRateJson);

                                seasonRates = ((JArray)ob["seasonRates"]);
                                // var ss=  seasonRates.Where(f => f["name"].ToSafeString() == Uri.EscapeUriString(existingName) && f["range"]["start"]["asDate"].ToSafeString() == existingStartDate.ToString("MM/dd/yyyy") && f["range"]["end"]["asDate"].ToSafeString() == existingEndDate.ToString("MM/dd/yyyy")).FirstOrDefault();

                                newCsrfToken = Helper_GetCSRFToken();
                                //  ((JArray)ob["seasonRates"]).Remove(ss);
                                ((JArray)ob["seasonRates"]).Add(newJObject);

                                var newJson = JsonConvert.SerializeObject(ob);
                                commPage.RequestURL = string.Format("https://admin.vrbo.com/haolb/{0}/le/save.html", listingTriad);
                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.POST;
                                commPage.Parameters = new Dictionary<string, string>();
                                commPage.Parameters.Add("requestJSON", newJson);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                CommPage.RequestHeaders.Add("X-Csrf-Token-Requested", newCsrfToken);
                                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                                commPage.RequestHeaders.Add("Ajax", "true");
                                commPage.Referer = url;
                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    var resultOb = JObject.Parse(commPage.Html);
                                    if (resultOb["status"].ToSafeString() == "0")
                                        return new Tuple<bool, string>(true, "Successfully changed.");
                                }
                            }
                        }
                    //    else
                    //    {
                    //        return new Tuple<bool, string>(resultBool, "");
                    //    }
                    //}
                }
                catch (Exception e)
                {

                }
            }
            return new Tuple<bool, string>(resultBool, "Error in changing. Something went wrong.");
        }

        public Tuple<bool, string> EditRateOnNewCalendar(string token, string listingId, double price, DateTime dateFrom, DateTime dateTo)
        {
            bool isLoaded = ValidateTokenAndLoadCookies(token);
            if (isLoaded)
            {
                var listingTriad = Helper_ListingToTriad(listingId);
                var druuId = Helper_GetDruidFromListingId(listingTriad);
                var unitUrl = Helper_GetUnitUrl(listingTriad);

                string url = string.Format("https://admin.vrbo.com/pxcalendars/rates/{0}", listingTriad);

                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/pxcalendars/proxies/px-rates/ratesAndDiscountsOverride?_restfully=true&unitUrl={0}", unitUrl);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                    CommPage.JsonStringToPost = string.Format("{{\"startDate\":\"{0}\",\"endDate\":\"{1}\",\"ratesAndDiscounts\":{{\"nightlyRate\":{{\"sunday\":\"{2}\",\"monday\":\"{2}\",\"tuesday\":\"{2}\",\"wednesday\":\"{2}\",\"thursday\":\"{2}\",\"friday\":\"{2}\",\"saturday\":\"{2}\"}},\"discount7Night\":{{}},\"discount28Night\":{{}},\"minimumStay\":3,\"changeOverDays\":[0,1,2,3,4,5,6]}}}}", dateFrom.ToString("yyyy-MM-dd"), dateTo.ToString("yyyy-MM-dd"), price);
                    
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/plain, */*");
                    commPage.Referer = url;


                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    ProcessRequest(commPage);

                    if (commPage.IsValid || (commPage.StatusCode == HttpStatusCode.NoContent && commPage.Html == ""))
                        return new Tuple<bool, string>(true, "Successfully changed.");
                    else return new Tuple<bool, string>(false, "Error in changing. Something went wrong.");
                }
            }

            return new Tuple<bool, string>(false, "Error in changing. Something went wrong.");
        }

        public List<Core.Database.Entity.PropertyBookingDate> Calendar_Rates(string token, string listingId)
        {
            List<Core.Database.Entity.PropertyBookingDate> bookingDates = new List<Core.Database.Entity.PropertyBookingDate>();

            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var druid = Helper_GetDruidFromListingId(listingTriad);
                    var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;

                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var html = commPage.Html;
                        var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                        var to = html.IndexOf(";", start);
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');

                        try
                        {
                            var ob = JObject.Parse(result);

                            var seasonRates = ob["seasonRates"].ToList();
                            if (seasonRates.Count > 0)
                            {
                                //var db = new ApplicationDbContext();

                                //Core.API.Vrbo.Calendar.DeleteAllExistingRate(db);

                                ////Test Start Date and End Date Between All Rates
                                var startDateYear = seasonRates[0]["range"]["start"]["asDate"].ToSafeString().ToDateTime().Year;
                                var endDateYear = seasonRates[seasonRates.Count - 1]["range"]["end"]["asDate"].ToSafeString().ToDateTime().Year;
                                var loopStartDate = ("01/01/" + startDateYear).ToDateTime();
                                var loopEndDate = ("12/31/" + (startDateYear > endDateYear ? startDateYear : endDateYear)).ToDateTime();

                                List<Core.Database.Entity.PropertyBookingDate> scrappedBookingDates = new List<Core.Database.Entity.PropertyBookingDate>();

                                foreach (var item in seasonRates)
                                {
                                    var name = item["name"].ToSafeString();
                                    var startDate = item["range"]["start"]["asDate"].ToSafeString();
                                    var endDate = item["range"]["end"]["asDate"].ToSafeString();

                                    var amounts = item["nightlyAmounts"].ToList();

                                    var nightlyAmount = "";
                                    var nightlyMinStay = "";
                                    var nightlyNode = amounts.Where(f => f["periodType"].ToSafeString() == "NIGHTLY").FirstOrDefault();
                                    if (nightlyNode != null)
                                    {
                                        nightlyMinStay = nightlyNode["minStay"].ToSafeString();
                                        nightlyAmount = nightlyNode["amount"].ToSafeString();
                                    }

                                    var isExpired = item["expired"].ToSafeString();
                                    var type = item["rateType"].ToSafeString();

                                    for (DateTime date = startDate.ToDateTime();
                                        date <= endDate.ToDateTime(); date = date.AddDays(1))
                                    {
                                        var cal = new Core.Database.Entity.PropertyBookingDate
                                        {
                                            PropertyId = Convert.ToInt64(listingId),
                                            Date = date,
                                            IsAvailable = true,
                                            Price = Convert.ToDouble(nightlyAmount),
                                            SiteType = 2,
                                            //CompanyId = GlobalVariables.CompanyId
                                        };

                                        scrappedBookingDates.Add(cal);
                                    }
                                }

                                url = string.Format("https://admin.vrbo.com/gd/proxies/calendar/reservations?druidUuid={0}&startDate={1}&endDate={2}&site=vrbo&locale=en_US&_restfully=true", druid, loopStartDate.ToString("yyyy-MM-dd"), loopEndDate.ToString("yyyy-MM-dd"));

                                List<Core.Database.Entity.PropertyBookingDate> availabilityList = new List<Core.Database.Entity.PropertyBookingDate>();

                                commPage.RequestURL = url;
                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.GET;
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    var arr = JArray.Parse(commPage.Html);
                                    if (arr != null)
                                    {
                                        foreach (var resevation in arr)
                                        {
                                            var availabilityStatus = resevation["availabilityStatus"].ToSafeString();
                                            var resId = resevation["resId"].ToSafeString();
                                            var startDate = resevation["startDate"].ToSafeString().ToDateTime();
                                            var endDate = resevation["endDate"].ToSafeString().ToDateTime();

                                            bool available = availabilityStatus == "UNAVAILABLE" ? false : true;

                                            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                                            {
                                                availabilityList.Add(new Core.Database.Entity.PropertyBookingDate
                                                {
                                                    IsAvailable = available,
                                                    Date = date,
                                                    ReservationId = resId
                                                });
                                            }
                                        }
                                    }
                                }

                                for (DateTime date = loopStartDate; date <= loopEndDate; date = date.AddDays(1))
                                {
                                    Core.Database.Entity.PropertyBookingDate _sbd = null;

                                    try
                                    {
                                        _sbd = (from s in scrappedBookingDates
                                                where
                                                     s.Date.Date == date
                                                select s).First();
                                    }
                                    catch (ArgumentNullException err1) { _sbd = null; }
                                    catch (InvalidOperationException err2) { _sbd = null; }


                                    Core.Database.Entity.PropertyBookingDate _a = null;

                                    try
                                    {
                                        _a = (from a in availabilityList
                                              where
                                                   a.Date.Date == date
                                              select a).First();
                                    }
                                    catch (ArgumentNullException err) { }
                                    catch (InvalidOperationException err) { }

                                    if (_sbd == null)
                                    {
                                        var cal = new Core.Database.Entity.PropertyBookingDate
                                        {
                                            PropertyId = Convert.ToInt64(listingId),
                                            Date = date,
                                            IsAvailable = true,
                                            Price = Core.API.AnySite.DefaultRate,
                                            SiteType = 2,
                                            //CompanyId = GlobalVariables.CompanyId,
                                            ReservationId = (_a == null ? null : _a.ReservationId)
                                        };

                                        bookingDates.Add(cal);
                                    }
                                    else
                                    {
                                        var cal = new Core.Database.Entity.PropertyBookingDate
                                        {
                                            PropertyId = Convert.ToInt64(_sbd.PropertyId),
                                            Date = _sbd.Date,
                                            IsAvailable = (_a == null ? _sbd.IsAvailable : _a.IsAvailable),
                                            Price = Convert.ToDouble(_sbd.Price),
                                            SiteType = _sbd.SiteType,
                                            //CompanyId = _sbd.CompanyId,
                                            ReservationId = (_a == null ? null : _a.ReservationId)

                                        };

                                        bookingDates.Add(cal);
                                    }
                                }
                            }
                        }
                        catch (Exception err)
                        {
                            var loopStartDate = ("01/01/" + DateTime.UtcNow.Year).ToDateTime();
                            var loopEndDate = ("12/31/" + DateTime.UtcNow.Year).ToDateTime();

                            for (DateTime date = loopStartDate; date <= loopEndDate; date = date.AddDays(1))
                            {
                                var cal = new Core.Database.Entity.PropertyBookingDate
                                {
                                    PropertyId = Convert.ToInt64(listingId),
                                    Date = date,
                                    IsAvailable = true,
                                    Price = Core.API.AnySite.DefaultRate,
                                    SiteType = 2,
                                    //CompanyId = GlobalVariables.CompanyId
                                };

                                bookingDates.Add(cal);
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }

            return bookingDates;
        }

        private string Helper_GetCSRFToken()
        {
            try
            {
                commPage.RequestURL = "https://admin.vrbo.com/haolb/csrf/async/token.html?preventCache=";
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JToken.Parse(commPage.Html);
                    return ob["csrftkn"].ToSafeString();

                }
            }
            catch { }
            return string.Empty;
        }

        #endregion

        //#region RESERVATIONS
        ////public ScrapeResult ScrapeReservations(string token, int hostId, SyncType syncType, bool reservationRequests = false, bool reservations = false, bool inquiries = false, bool all = false, bool isImporting = false)
        ////{
        ////    int count = 1;

        ////    var sresult = new ScrapeResult();
        ////    List<Reservation> reservationList = new List<Reservation>();
        ////    var isloaded = ValidateTokenAndLoadCookies(token);
        ////    if (isloaded)
        ////    {
        ////        try
        ////        {
        ////            var threads = new List<InboxListing>();
        ////            if (all)
        ////                threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.All));
        ////            else
        ////            {
        ////                if (reservationRequests)
        ////                    threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.ReservationRequests));
        ////                if (reservations)
        ////                    threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Reservations));
        ////                if (inquiries)
        ////                    threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Inquiry));
        ////            }
        ////            foreach (var thread in threads)
        ////            {
        ////                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", thread.Id);
        ////                commPage.RequestURL = detailUrl;
        ////                commPage.ReqType = CommunicationPage.RequestType.GET;
        ////                commPage.PersistCookies = true;
        ////                ProcessRequest(commPage);
        ////                if (commPage.IsValid)
        ////                {
        ////                    var ob = JObject.Parse(CommPage.Html);
        ////                    var ar = (JArray)ob["replyActions"];
        ////                    List<ReplyAction> actions = new List<ReplyAction>();
        ////                    if (ar.Count > 0)
        ////                    {
        ////                        foreach (var item in ar)
        ////                        {
        ////                            var actionType = item["type"].ToSafeString();
        ////                            if (actionType == "NONE")
        ////                            {
        ////                                actionType = item["buttonTitle"].ToSafeString();
        ////                            }
        ////                            ReplyAction act = new ReplyAction
        ////                            {
        ////                                Type = actionType
        ////                            };
        ////                            if (actionType == "DECLINE_BOOKING")
        ////                            {
        ////                                var reasons = (JArray)item["declineReasons"];
        ////                                Dictionary<string, string> declineReasons = new Dictionary<string, string>();
        ////                                reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));
        ////                                act.MetaData = declineReasons;
        ////                            }
        ////                            actions.Add(act);
        ////                        }
        ////                    }
        ////                    if (!actions.Any(f => f.Type.Contains("EDIT") || f.Type.Contains("ADD")))
        ////                    {
        ////                        if (thread.Status.Contains("TENTATIVE"))
        ////                        {

        ////                            actions.Add(new ReplyAction { Type = "EDIT_BOOKING" });
        ////                        }
        ////                    }

        ////                    if (ob["quote"]["editable"].ToBoolean() == true)
        ////                        actions.Add(new ReplyAction { Type = "EDIT_QUOTE" });

        ////                    var adults = ob["quote"]["numAdults"].ToInt();
        ////                    var children = ob["quote"]["numChildren"].ToInt();
        ////                    var guestCount = adults + children;
        ////                    var nights = ob["quote"]["numNights"].ToInt();
        ////                    var pets = ob["quote"]["numPets"].ToInt();
        ////                    var quoteId = ob["quote"]["quoteGuid"].ToSafeString();
        ////                    var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
        ////                    var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
        ////                    var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
        ////                    var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

        ////                    var fees = ((JArray)ob["quote"]["fees"]).ToList();


        ////                    var rentalCost = "";
        ////                    var serviceFee = "";
        ////                    var cleaningFee = "";
        ////                    var refundableDamageDeposit = "";
        ////                    var taxRate = "";

        ////                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
        ////                    if (rentalNode != null)
        ////                    {
        ////                        rentalCost = rentalNode["amount"]["amount"].ToSafeString();
        ////                        if (rentalNode["feeBasis"]["tax"] != null)
        ////                        {
        ////                            taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

        ////                        }
        ////                    }

        ////                    var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
        ////                    if (cleaningNode != null)
        ////                        cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

        ////                    var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
        ////                    if (serviceNode != null)
        ////                        serviceFee = serviceNode["amount"]["amount"].ToSafeString();

        ////                    var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
        ////                    if (ddNode != null)
        ////                        refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();


        ////                    var phone = ob["conversation"]["otherParticipants"][0]["phone"].ToSafeString();
        ////                    var email = ob["conversation"]["otherParticipants"][0]["emailAddress"].ToSafeString();
        ////                    var reservationNode = ob["reservation"];
        ////                    var reservationId = "";
        ////                    var reservationCreatedAt = "";
        ////                    var reservationExpiry = "";
        ////                    if (reservationNode != null)
        ////                    {
        ////                        reservationId = ob["reservation"]["id"].ToSafeString();
        ////                        reservationCreatedAt = ob["reservation"]["created"].ToSafeString();
        ////                        reservationExpiry = ob["reservation"]["expirationDateTime"].ToSafeString();
        ////                    }
        ////                    var reservationDeclineDateTime = "";
        ////                    var reservationAcceptedDatetime = "";

        ////                    var messages = ((JArray)ob["messages"]).ToList();

        ////                    var declineNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_DECLINED").FirstOrDefault();
        ////                    if (declineNode != null)
        ////                        reservationDeclineDateTime = declineNode["sentTimestamp"].ToSafeString();

        ////                    var acceptNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_ACCEPTED").FirstOrDefault();
        ////                    if (acceptNode != null)
        ////                        reservationAcceptedDatetime = acceptNode["sentTimestamp"].ToSafeString();

        ////                    var listingUniqueId = ob["property"]["druidUuid"].ToSafeString();


        ////                    var summary = ob["quote"]["quoteTotals"].ToSafeString();
        ////                    //var jsonSummary =  JsonConvert.SerializeObject(summary);



        ////                    var reservation = new Reservation
        ////                    {
        ////                        HostId = hostId,

        ////                        CheckInDate = thread.StayStartDate,
        ////                        CheckoutDate = thread.StayEndDate,
        ////                        Code = thread.ReservationCode,

        ////                        ServiceFee = serviceFee,
        ////                        CleaningFee = cleaningFee,
        ////                        TotalPayout = payableToHost,//quoteTotal,
        ////                        ReservationCost = quoteTotal, //reservationCost,
        ////                        RefundableDamageDeposit = refundableDamageDeposit,
        ////                        RentalCost = rentalCost,
        ////                        TaxRate = taxRate,
        ////                        TotalTax = taxAmount,
        ////                        ReservationCostSummary = summary,

        ////                        Nights = nights,
        ////                        GuestCount = guestCount,
        ////                        Adult = adults,
        ////                        Children = children,
        ////                        GuestEmail = email,
        ////                        GuestPhone = phone,
        ////                        ListingId = thread.ListingId,
        ////                        GuestId = thread.GuestId,
        ////                        InquiryDatetime = thread.InquiryDate,
        ////                        ReservationId = reservationId, //check this
        ////                        Status = thread.Status,
        ////                        IsActive = (thread.Status == "STAYING" || thread.Status == "BOOKED"),
        ////                        ReplyActions = actions,
        ////                        Pets = pets.ToSafeString(),
        ////                        ReservationRequestDatetime = reservationCreatedAt,
        ////                        ReservationRequestExpiryDatetime = reservationExpiry,
        ////                        ReservationRequestAcceptedDatetime = reservationAcceptedDatetime,
        ////                        ReservationRequestCancelledDatetime = reservationDeclineDateTime,
        ////                        ThreadId = thread.Id,
        ////                        ListingUniqueId = listingUniqueId,
        ////                        Type = thread.Type
        ////                    };

        ////                    var addResult = Core.API.Vrbo.Inquiries.Add(reservation.HostId, reservation.Code, reservation.ReservationId,
        ////                        reservation.Status, reservation.CheckInDate, reservation.CheckoutDate, reservation.Nights,
        ////                        reservation.ListingId, reservation.GuestId, reservation.ReservationCost, reservation.CleaningFee,
        ////                        reservation.ServiceFee, reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(),
        ////                        reservation.ReservationRequestDatetime.ToDateTime(), reservation.GuestCount, reservation.Adult, reservation.Children,
        ////                        reservation.ReservationRequestAcceptedDatetime.ToDateTime(), reservation.ReservationRequestCancelledDatetime.ToDateTime(),
        ////                        reservation.ThreadId, reservation.Type);

        ////                    if (isImporting)
        ////                    {
        ////                        ImportHub.UpdateProgress(GlobalVariables.UserId.ToString(), ImportStage.Inquiry, count);
        ////                        count++;
        ////                    }

        ////                    if (syncType == SyncType.New && addResult.IsDuplicate)
        ////                    {
        ////                        break;
        ////                    }
        ////                    else
        ////                    {
        ////                        reservationList.Add(reservation);
        ////                    }
        ////                }
        ////            }

        ////            var totalItems = reservationList.Count();
        ////            if (totalItems > 0)
        ////            {
        ////                sresult.Success = true;
        ////                sresult.Message = "Operation Successful";//, file generated at " + outputPath;
        ////                sresult.TotalRecords = totalItems;
        ////            }
        ////            else
        ////            {
        ////                sresult.Message = "An error occurred.";

        ////            }

        ////        }
        ////        catch (Exception e)
        ////        {
        ////            sresult.Message = e.Message;
        ////        }
        ////    }

        ////    else
        ////    {
        ////        sresult.Message = "Could not login, Please check you details and try again.";
        ////    }

        ////    if (isImporting)
        ////        ImportHub.DoneImporting(GlobalVariables.UserId.ToString(), ImportStage.Inquiry);

        ////    return sresult;
        ////}

        //public ScrapeResult ScrapeReservations(string token, int hostId, SyncType syncType, bool reservations = false, bool inquiries = false, bool tentativeOrPreApproved = false, bool all = false, bool isImporting = false)
        //{
        //    int count = 1;

        //    var sresult = new ScrapeResult();
        //    List<Reservation> reservationList = new List<Reservation>();
        //    var isloaded = ValidateTokenAndLoadCookies(token);
        //    if (isloaded)
        //    {
        //        try
        //        {
        //            var newOnly = syncType == SyncType.New;
        //            var threads = new List<InboxListing>();
        //            if (all)
        //                threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.All));
        //            else
        //            {
        //                if (reservations)
        //                    threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.Reservation));
        //                if (inquiries)
        //                    threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.Inquiry));
        //                if (tentativeOrPreApproved)
        //                    threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.Tentative));
        //            }
        //            foreach (var thread in threads)
        //            {
        //                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", thread.Id);
        //                commPage.RequestURL = detailUrl;
        //                commPage.ReqType = CommunicationPage.RequestType.GET;
        //                commPage.PersistCookies = true;
        //                ProcessRequest(commPage);
        //                if (commPage.IsValid)
        //                {
        //                    var ob = JObject.Parse(CommPage.Html);
        //                    var ar = (JArray)ob["replyActions"];
        //                    List<ReplyAction> actions = new List<ReplyAction>();
        //                    if (ar.Count > 0)
        //                    {
        //                        foreach (var item in ar)
        //                        {
        //                            var actionType = item["type"].ToSafeString();
        //                            if (actionType == "NONE")
        //                            {
        //                                actionType = item["buttonTitle"].ToSafeString();
        //                            }
        //                            ReplyAction act = new ReplyAction
        //                            {
        //                                Type = actionType
        //                            };
        //                            if (actionType == "DECLINE_BOOKING")
        //                            {
        //                                var reasons = (JArray)item["declineReasons"];
        //                                Dictionary<string, string> declineReasons = new Dictionary<string, string>();
        //                                reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));
        //                                act.MetaData = declineReasons;
        //                            }
        //                            actions.Add(act);
        //                        }
        //                    }
        //                    if (!actions.Any(f => f.Type.Contains("EDIT") || f.Type.Contains("ADD")))
        //                    {
        //                        if (thread.Status.Contains("TENTATIVE"))
        //                        {

        //                            actions.Add(new ReplyAction { Type = "EDIT_BOOKING" });
        //                        }
        //                    }


        //                    if (ob["paymentDetails"]["quoteSection"]["editable"].ToBoolean() == true)
        //                        actions.Add(new ReplyAction { Type = "EDIT_QUOTE" });

        //                    var adults = ob["quote"]["numAdults"].ToInt();
        //                    var children = ob["quote"]["numChildren"].ToInt();
        //                    var guestCount = adults + children;
        //                    var nights = ob["quote"]["numNights"].ToInt();
        //                    var pets = ob["quote"]["numPets"].ToInt();
        //                    var quoteId = ob["quote"]["quoteGuid"].ToSafeString();
        //                    var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
        //                    var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
        //                    var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
        //                    var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

        //                    var fees = ((JArray)ob["quote"]["fees"]).ToList();


        //                    var rentalCost = "";
        //                    var serviceFee = "";
        //                    var cleaningFee = "";
        //                    var refundableDamageDeposit = "";
        //                    var taxRate = "";

        //                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
        //                    if (rentalNode != null)
        //                    {
        //                        rentalCost = rentalNode["amount"]["amount"].ToSafeString();
        //                        if (rentalNode["feeBasis"]["tax"] != null)
        //                        {
        //                            taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

        //                        }
        //                    }

        //                    var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
        //                    if (cleaningNode != null)
        //                        cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

        //                    var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
        //                    if (serviceNode != null)
        //                        serviceFee = serviceNode["amount"]["amount"].ToSafeString();

        //                    var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
        //                    if (ddNode != null)
        //                        refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();


        //                    var phone = ob["conversation"]["otherParticipants"][0]["phone"].ToSafeString();
        //                    var email = ob["conversation"]["otherParticipants"][0]["emailAddress"].ToSafeString();
        //                    var reservationNode = ob["reservation"];
        //                    var reservationId = "";
        //                    var reservationCreatedAt = "";
        //                    var reservationExpiry = "";
        //                    if (reservationNode != null)
        //                    {
        //                        reservationId = ob["reservation"]["id"].ToSafeString();
        //                        reservationCreatedAt = ob["reservation"]["created"].ToSafeString();
        //                        reservationExpiry = ob["reservation"]["expirationDateTime"].ToSafeString();
        //                    }
        //                    var reservationDeclineDateTime = "";
        //                    var reservationAcceptedDatetime = "";

        //                    var messages = ((JArray)ob["messages"]).ToList();

        //                    var declineNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_DECLINED").FirstOrDefault();
        //                    if (declineNode != null)
        //                        reservationDeclineDateTime = declineNode["sentTimestamp"].ToSafeString();

        //                    var acceptNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_ACCEPTED").FirstOrDefault();
        //                    if (acceptNode != null)
        //                        reservationAcceptedDatetime = acceptNode["sentTimestamp"].ToSafeString();

        //                    var listingUniqueId = ob["property"]["druidUuid"].ToSafeString();


        //                    var summary = ob["quote"]["quoteTotals"].ToSafeString();
        //                    //var jsonSummary =  JsonConvert.SerializeObject(summary);




        //                    var reservation = new Reservation
        //                    {
        //                        HostId = hostId,

        //                        CheckInDate = thread.StayStartDate,
        //                        CheckoutDate = thread.StayEndDate,
        //                        Code = thread.ReservationCode,

        //                        ServiceFee = serviceFee,
        //                        CleaningFee = cleaningFee,
        //                        TotalPayout = payableToHost,//quoteTotal,
        //                        ReservationCost = quoteTotal, //reservationCost,
        //                        RefundableDamageDeposit = refundableDamageDeposit,
        //                        RentalCost = rentalCost,
        //                        TaxRate = taxRate,
        //                        TotalTax = taxAmount,
        //                        ReservationCostSummary = summary,


        //                        Nights = nights,
        //                        GuestCount = guestCount,
        //                        Adult = adults,
        //                        Children = children,
        //                        GuestEmail = email,
        //                        GuestPhone = phone,
        //                        ListingId = thread.ListingId,
        //                        GuestId = thread.GuestId,
        //                        InquiryDatetime = thread.InquiryDate,
        //                        ReservationId = reservationId, //check this
        //                        Status = thread.Status,
        //                        IsActive = (thread.Status == "STAYING" || thread.Status == "BOOKED"),
        //                        ReplyActions = actions,
        //                        Pets = pets.ToSafeString(),
        //                        ReservationRequestDatetime = reservationCreatedAt,
        //                        ReservationRequestExpiryDatetime = reservationExpiry,
        //                        ReservationRequestAcceptedDatetime = reservationAcceptedDatetime,
        //                        ReservationRequestCancelledDatetime = reservationDeclineDateTime,
        //                        ThreadId = thread.Id,
        //                        ListingUniqueId = listingUniqueId,
        //                        Type = thread.Type
        //                    };

        //                    var addResult = Core.API.Vrbo.Inquiries.Add(reservation.HostId, reservation.Code, reservation.ReservationId,
        //                        reservation.Status, reservation.CheckInDate, reservation.CheckoutDate, reservation.Nights,
        //                        reservation.ListingId, reservation.GuestId, reservation.ReservationCost, reservation.CleaningFee,
        //                        reservation.ServiceFee, reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(),
        //                        reservation.ReservationRequestDatetime.ToDateTime(), reservation.GuestCount, reservation.Adult, reservation.Children,
        //                        reservation.ReservationRequestAcceptedDatetime.ToDateTime(), reservation.ReservationRequestCancelledDatetime.ToDateTime(),
        //                        reservation.ThreadId, reservation.Type);

        //                    using (var sm = new VRBO.ScrapeManager())
        //                    {
        //                        List<long> propertyIds = new List<long>();
        //                        List<Tuple<long, string>> properties = new List<Tuple<long, string>>();

        //                        using (var db = new ApplicationDbContext())
        //                        {
        //                            int parentPropertyId =
        //                                (from p in db.Properties
        //                                 where
        //                                      p.ListingId == reservation.ListingId
        //                                 select p.ParentPropertyId)
        //                                 .FirstOrDefault();

        //                            if (parentPropertyId != 0)
        //                            {
        //                                (from p in db.Properties
        //                                 where
        //                                      p.ParentPropertyId == parentPropertyId
        //                                 select p.ListingId)
        //                                 .ToList()
        //                                 .ForEach(p => propertyIds.Add(p));

        //                                (from pbd in db.PropertyBookingDate
        //                                 where
        //                                      DbFunctions.TruncateTime(pbd.Date) == reservation.CheckInDate.Date &&
        //                                      propertyIds.Contains(pbd.PropertyId)
        //                                 select new
        //                                 {
        //                                     ListingId = pbd.PropertyId,
        //                                     ReservationId = pbd.ReservationId
        //                                 })
        //                                .ToList()
        //                                .ForEach(p => properties.Add(new Tuple<long, string>(p.ListingId, p.ReservationId)));
        //                            }

        //                            //bool success = false;
        //                            //bool isAvailable = false;
        //                            //string resId = null;

        //                            if (reservation.ListingId == 827199 && reservation.CheckInDate.ToString("yyyy-MM-dd") == "2018-05-21")
        //                            {

        //                            }

        //                            foreach (var prop in properties)
        //                            {
        //                                string status = Utilities.GetReservationStatusCode(reservation.Status);

        //                                if (prop.Item1 != reservation.ListingId)
        //                                {
        //                                    if (status == "A" || status == "B")// || status == "PAYMENT_REQUEST_SENT")
        //                                    {
        //                                        var blocking = sm.BlockDates(token, reservation.CheckInDate, reservation.CheckoutDate, prop.Item1.ToString());

        //                                        bool success = blocking.Item1;
        //                                        bool isAvailable = false;
        //                                        string resId = blocking.Item3;

        //                                        if (success)
        //                                        {
        //                                            #region Db Update

        //                                            for (DateTime date = reservation.CheckInDate; date <= reservation.CheckoutDate.AddDays(-1); date = date.AddDays(1))
        //                                            {
        //                                                try
        //                                                {
        //                                                    Core.Database.Entity.PropertyBookingDate pbd = (from p in db.PropertyBookingDate
        //                                                                                                    where
        //                                                                                                         p.PropertyId == prop.Item1 &&
        //                                                                                                         DbFunctions.TruncateTime(p.Date) == date.Date
        //                                                                                                    select p).First();

        //                                                    pbd.IsAvailable = isAvailable;
        //                                                    pbd.ReservationId = resId;
        //                                                }
        //                                                catch (ArgumentNullException err)
        //                                                {
        //                                                    db.PropertyBookingDate.Add(new Core.Database.Entity.PropertyBookingDate
        //                                                    {
        //                                                        PropertyId = prop.Item1,
        //                                                        Date = date,
        //                                                        IsAvailable = isAvailable,
        //                                                        Price = Core.API.AnySite.DefaultRate,
        //                                                        ReservationId = resId,
        //                                                        SiteType = 2,
        //                                                        //CompanyId = GlobalVariables.CompanyId
        //                                                    });
        //                                                }
        //                                                catch (InvalidOperationException errr)
        //                                                {
        //                                                    db.PropertyBookingDate.Add(new Core.Database.Entity.PropertyBookingDate
        //                                                    {
        //                                                        PropertyId = prop.Item1,
        //                                                        Date = date,
        //                                                        IsAvailable = isAvailable,
        //                                                        Price = Core.API.AnySite.DefaultRate,
        //                                                        ReservationId = resId,
        //                                                        SiteType = 2,
        //                                                        //CompanyId = GlobalVariables.CompanyId
        //                                                    });
        //                                                }

        //                                                db.BulkSaveChanges();
        //                                            }

        //                                            #endregion
        //                                        }
        //                                        //sm.UpdateAvailability(token, pId.ToString(), false, reservation.CheckInDate, reservation.CheckoutDate);
        //                                    }
        //                                    else if (status == "IC" || status == "BC")
        //                                    {
        //                                        bool success = sm.UnBlockDates(token, prop.Item1.ToString(), prop.Item2);
        //                                        bool isAvailable = true;
        //                                        string resId = null;

        //                                        if (success)
        //                                        {
        //                                            #region Db Update

        //                                            for (DateTime date = reservation.CheckInDate; date <= reservation.CheckoutDate; date = date.AddDays(1))
        //                                            {
        //                                                try
        //                                                {
        //                                                    Core.Database.Entity.PropertyBookingDate pbd = (from p in db.PropertyBookingDate.ToList()
        //                                                                                                    where
        //                                                                                                         p.PropertyId == prop.Item1 &&
        //                                                                                                         DbFunctions.TruncateTime(p.Date) == date.Date
        //                                                                                                    select p).First();

        //                                                    pbd.IsAvailable = isAvailable;
        //                                                    pbd.ReservationId = resId;
        //                                                }
        //                                                catch (ArgumentNullException err)
        //                                                {
        //                                                    db.PropertyBookingDate.Add(new Core.Database.Entity.PropertyBookingDate
        //                                                    {
        //                                                        PropertyId = prop.Item1,
        //                                                        Date = date,
        //                                                        IsAvailable = isAvailable,
        //                                                        Price = Core.API.AnySite.DefaultRate,
        //                                                        ReservationId = resId,
        //                                                        SiteType = 2,
        //                                                        //CompanyId = GlobalVariables.CompanyId
        //                                                    });
        //                                                }
        //                                                catch (InvalidOperationException errr)
        //                                                {
        //                                                    db.PropertyBookingDate.Add(new Core.Database.Entity.PropertyBookingDate
        //                                                    {
        //                                                        PropertyId = prop.Item1,
        //                                                        Date = date,
        //                                                        IsAvailable = isAvailable,
        //                                                        Price = Core.API.AnySite.DefaultRate,
        //                                                        ReservationId = resId,
        //                                                        SiteType = 2,
        //                                                        //CompanyId = GlobalVariables.CompanyId
        //                                                    });
        //                                                }

        //                                                db.BulkSaveChanges();
        //                                            }

        //                                            #endregion
        //                                        }
        //                                        //sm.UpdateAvailability(token, pId.ToString(), true, reservation.CheckInDate, reservation.CheckoutDate);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }

        //                    if (isImporting)
        //                    {
        //                        GlobalVariables.ImportedBookingCount = count;
        //                        ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
        //                        count++;
        //                    }

        //                    //CalendarHub.ReloadCalendar(GlobalVariables.UserId.ToString());

        //                    if (syncType == SyncType.New && addResult.IsDuplicate)
        //                    {
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        reservationList.Add(reservation);
        //                    }
        //                }
        //            }

        //            var totalItems = reservationList.Count();
        //            if (totalItems > 0)
        //            {
        //                sresult.Success = true;
        //                sresult.Message = "Operation Successful";//, file generated at " + outputPath;
        //                sresult.TotalRecords = totalItems;
        //            }
        //            else
        //            {
        //                sresult.Message = "An error occurred.";

        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            sresult.Message = e.Message;
        //        }
        //    }

        //    else
        //    {
        //        sresult.Message = "Could not login, Please check you details and try again.";
        //    }

        //    //if (isImporting)
        //    //    ImportHub.DoneImporting(GlobalVariables.UserId.ToString(), ImportStage.Inquiry);

        //    return sresult;
        //}

        //public bool Pre_Approve_Inquiry(string token, string conversationId, string message)
        //{
        //    var isLogedIn = ValidateTokenAndLoadCookies(token);
        //    try
        //    {

        //        ////var inboxUrl = string.Format("https://admin.vrbo.com/rm");
        //        ////commPage.RequestURL = inboxUrl;
        //        ////commPage.ReqType = CommunicationPage.RequestType.GET;
        //        ////commPage.PersistCookies = true;
        //        ////// commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
        //        ////commPage.RequestHeaders = new Dictionary<string, string>();
        //        ////commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        //        ////ProcessRequest(commPage);



        //        ////var startUrl = string.Format("https://admin.vrbo.com/rm/message/l-321.827199.1375137/g-{0}", conversationId);
        //        ////commPage.RequestURL = startUrl;
        //        ////commPage.ReqType = CommunicationPage.RequestType.GET;
        //        ////commPage.PersistCookies = true;
        //        ////// commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
        //        ////commPage.RequestHeaders = new Dictionary<string, string>();
        //        ////commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*;q=0.8");
        //        ////ProcessRequest(commPage);


        //        var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&declineReasonsVersion=2&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", conversationId);
        //        commPage.RequestURL = detailUrl;
        //        commPage.ReqType = CommunicationPage.RequestType.GET;
        //        commPage.PersistCookies = true;
        //        // commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
        //        commPage.RequestHeaders = new Dictionary<string, string>();
        //        commPage.RequestHeaders.Add("Accept", "*");
        //        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

        //        //var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
        //        //var auth_token = cc["crumb"].Value.DecodeURL();
        //        //commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

        //        ProcessRequest(commPage);
        //        if (commPage.IsValid)
        //        {
        //            var ob = JObject.Parse(CommPage.Html);
        //            var quote = (JObject)ob["quote"];

        //            quote["commissionable"] = true;
        //            quote["commissioned"] = true;

        //            try
        //            {

        //                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?locale=en_US_VRBO&useRates=true&conversationId={0}&_restfully=true", conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");
        //                                                                                                                                                                                                        // commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?locale=en_US_VRBO&_restfully=true", conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

        //                commPage.PersistCookies = true;
        //                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
        //                commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
        //                commPage.RequestHeaders = new Dictionary<string, string>();
        //                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
        //                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



        //                var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
        //                var auth_token = cc["crumb"].Value.DecodeURL();
        //                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);




        //                //commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.827199.1375137/g-{0}", conversationId);
        //                ProcessRequest(commPage);
        //                if (commPage.IsValid)
        //                {
        //                    ob = JObject.Parse(commPage.Html);
        //                }

        //                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/book?_restfully=true");
        //                ob.Remove("bodyText");
        //                ob.Add("bodyText", message);


        //                commPage.PersistCookies = true;
        //                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
        //                commPage.JsonStringToPost = JsonConvert.SerializeObject(ob);
        //                commPage.RequestHeaders = new Dictionary<string, string>();
        //                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
        //                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


        //                cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
        //                auth_token = cc["crumb"].Value.DecodeURL();
        //                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

        //                commPage.Referer = "";
        //                ProcessRequest(commPage);
        //                if (commPage.IsValid)
        //                {
        //                    ob = JObject.Parse(commPage.Html);
        //                    var quoteGuid = ob["quoteGuid"].ToSafeString();
        //                    if (string.IsNullOrEmpty(quoteGuid))
        //                    {
        //                        return false;
        //                    }

        //                    return true;
        //                }

        //            }
        //            catch (Exception e)
        //            {

        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //    }
        //    return false;
        //}

        //#endregion

        ////  for testing only remove later
        //    int hostId = 1;

        //    var sresult = new ScrapeResult();
        //    List<Reservation> reservationList = new List<Reservation>();
        //    var isloaded = ValidateTokenAndLoadCookies(token);
        //    if (isloaded)
        //    {
        //        try
        //        {
        //            var threads = new List<InboxListing>();
        //            if (all)
        //                threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.All));
        //            else
        //            {
        //                if (reservationRequests)
        //                    threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.ReservationRequests));
        //                if (reservations)
        //                    threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Reservations));
        //                if (inquiries)
        //                    threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Inquiries));
        //            }
        //            foreach (var thread in threads)
        //            {
        //                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", thread.Id);
        //                commPage.RequestURL = detailUrl;
        //                commPage.ReqType = CommunicationPage.RequestType.GET;
        //                commPage.PersistCookies = true;
        //                ProcessRequest(commPage);
        //                if (commPage.IsValid)
        //                {
        //                    var ob = JObject.Parse(CommPage.Html);
        //                    var ar = (JArray)ob["replyActions"];
        //                    List<ReplyAction> actions = new List<ReplyAction>();
        //                    if (ar.Count > 0)
        //                    {
        //                        foreach (var item in ar)
        //                        {
        //                            var actionType = item["type"].ToSafeString();
        //                            if (actionType == "NONE")
        //                            {
        //                                actionType = item["buttonTitle"].ToSafeString();
        //                            }
        //                            ReplyAction act = new ReplyAction
        //                            {
        //                                Type = actionType
        //                            };
        //                            if (actionType == "DECLINE_BOOKING")
        //                            {
        //                                var reasons = (JArray)item["declineReasons"];
        //                                Dictionary<string, string> declineReasons = new Dictionary<string, string>();
        //                                reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));
        //                                act.MetaData = declineReasons;
        //                            }
        //                            actions.Add(act);
        //                        }
        //                    }
        //                    if (!actions.Any(f => f.Type.Contains("EDIT") || f.Type.Contains("ADD")))
        //                    {
        //                        if (thread.Status.Contains("TENTATIVE"))
        //                        {
        //                            actions.Add(new ReplyAction { Type = "EDIT_BOOKING" });
        //                        }
        //                    }

        //                    var adults = ob["quote"]["numAdults"].ToInt();
        //                    var children = ob["quote"]["numChildren"].ToInt();
        //                    var guestCount = adults + children;
        //                    var nights = ob["quote"]["numNights"].ToInt();
        //                    var pets = ob["quote"]["numPets"].ToInt();
        //                    var quoteId = ob["quote"]["quoteGuid"].ToSafeString();
        //                    var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
        //                    var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
        //                    var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
        //                    var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

        //                    var fees = ((JArray)ob["quote"]["fees"]).ToList();

        //                    var rentalCost = "";
        //                    var serviceFee = "";
        //                    var cleaningFee = "";
        //                    var refundableDamageDeposit = "";
        //                    var taxRate = "";

        //                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
        //                    if (rentalNode != null)
        //                    {
        //                        rentalCost = rentalNode["amount"]["amount"].ToSafeString();
        //                        if (rentalNode["feeBasis"]["tax"] != null)
        //                        {
        //                            taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();
        //                        }
        //                    }

        //                    var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
        //                    if (cleaningNode != null)
        //                        cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

        //                    var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
        //                    if (serviceNode != null)
        //                        serviceFee = serviceNode["amount"]["amount"].ToSafeString();

        //                    var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
        //                    if (ddNode != null)
        //                        refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();

        //                    var phone = ob["conversation"]["otherParticipants"][0]["phone"].ToSafeString();
        //                    var email = ob["conversation"]["otherParticipants"][0]["emailAddress"].ToSafeString();
        //                    var reservationNode = ob["reservation"];
        //                    var reservationId = "";
        //                    var reservationCreatedAt = "";
        //                    var reservationExpiry = "";
        //                    if (reservationNode != null)
        //                    {
        //                        reservationId = ob["reservation"]["id"].ToSafeString();
        //                        reservationCreatedAt = ob["reservation"]["created"].ToSafeString();
        //                        reservationExpiry = ob["reservation"]["expirationDateTime"].ToSafeString();
        //                    }
        //                    var reservationDeclineDateTime = "";
        //                    var reservationAcceptedDatetime = "";

        //                    var messages = ((JArray)ob["messages"]).ToList();

        //                    var declineNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_DECLINED").FirstOrDefault();
        //                    if (declineNode != null)
        //                        reservationDeclineDateTime = declineNode["sentTimestamp"].ToSafeString();

        //                    var acceptNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_ACCEPTED").FirstOrDefault();
        //                    if (acceptNode != null)
        //                        reservationAcceptedDatetime = acceptNode["sentTimestamp"].ToSafeString();

        //                    var listingUniqueId = ob["property"]["druidUuid"].ToSafeString();

        //                    var summary = ob["quote"]["quoteTotals"].ToSafeString();

        //                    var reservation = new Reservation
        //                    {
        //                        CheckInDate = thread.StayStartDate,
        //                        CheckoutDate = thread.StayEndDate,
        //                        Code = thread.ReservationCode,

        //                        ServiceFee = serviceFee,
        //                        CleaningFee = cleaningFee,
        //                        TotalPayout = payableToHost,//quoteTotal,
        //                        ReservationCost = quoteTotal, //reservationCost,
        //                        RefundableDamageDeposit = refundableDamageDeposit,
        //                        RentalCost = rentalCost,
        //                        TaxRate = taxRate,
        //                        TotalTax = taxAmount,
        //                        ReservationCostSummary = summary,

        //                        Nights = nights,
        //                        GuestCount = guestCount,
        //                        Adult = adults,
        //                        Children = children,
        //                        GuestEmail = email,
        //                        GuestPhone = phone,
        //                        ListingId = thread.ListingId,
        //                        GuestId = thread.GuestId,
        //                        InquiryDatetime = thread.InquiryDate,
        //                        ReservationId = reservationId, //check this
        //                        Status = thread.Status,
        //                        IsActive = (thread.Status == "STAYING" || thread.Status == "BOOKED"),
        //                        ReplyActions = actions,
        //                        Pets = pets.ToSafeString(),
        //                        ReservationRequestDatetime = reservationCreatedAt,
        //                        ReservationRequestExpiryDatetime = reservationExpiry,
        //                        ReservationRequestAcceptedDatetime = reservationAcceptedDatetime,
        //                        ReservationRequestCancelledDatetime = reservationDeclineDateTime,
        //                        ThreadId = thread.Id,
        //                        ListingUniqueId = listingUniqueId
        //                    };

        //                    var addResult = Core.API.Vrbo.Inquiries.Add(hostId, reservation.Code, reservation.ReservationId,
        //                        reservation.Status, reservation.CheckInDate, reservation.CheckoutDate, reservation.Nights,
        //                        reservation.ListingId, reservation.GuestId, reservation.ReservationCost, reservation.CleaningFee,
        //                        reservation.ServiceFee, reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(),
        //                        reservation.ReservationRequestDatetime.ToDateTime(),reservation.GuestCount, reservation.Adult, reservation.Children,
        //                        reservation.ReservationRequestAcceptedDatetime.ToDateTime(), reservation.ReservationRequestCancelledDatetime.ToDateTime(),
        //                        reservation.ThreadId);

        //                    if (syncType == SyncType.New && addResult.IsDuplicate)
        //                    {
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        reservationList.Add(reservation);
        //                    }
        //                }
        //            }

        //            var totalItems = reservationList.Count();
        //            if (totalItems > 0)
        //            {
        //                sresult.Success = true;
        //                sresult.Message = "Operation Successful";//, file generated at " + outputPath;
        //                sresult.TotalRecords = totalItems;
        //            }
        //            else
        //            {
        //                sresult.Message = "An error occurred.";
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            sresult.Message = e.Message;
        //        }
        //    }
        //    else
        //    {
        //        sresult.Message = "Could not login, Please check you details and try again.";
        //    }

        //    //return reservationList;
        //    return sresult;

        #region RESERVATION ACTIONS

        public ScrapeResult AcceptOffer(string token, string reservationId, string message, string threadId)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/acceptv2?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        //CommPage.JsonStringToPost = string.Format("{{\"acceptRequestPaymentType\":\"ONLINE\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\"}}", reservationId, message);
                        commPage.JsonStringToPost = string.Format("{{\"acceptRequestPaymentType\":\"ONLINE\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\"}}", reservationId, message);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        sresult.Message = "Compage not valid";
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var status = ob["reservationUuid"].ToSafeString();
                            if (!string.IsNullOrEmpty(status))
                            {
                                sresult.Message = "Operation successful.";
                                sresult.Success = true;
                                return sresult;
                            }
                        }
                    }
                }
                catch (Exception e) { sresult.Message = e.ToString(); }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public ScrapeResult RejectOffer(string token, string reservationId, string reasonType, string reason, string message, string threadId)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/declinev2?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = string.Format("{{\"declineReason\":\"{3}\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\",\"customDeclineReason\":\"{2}\"}}", reservationId, message, reason, reasonType);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var status = ob["reservationUuid"].ToSafeString();
                            if (!string.IsNullOrEmpty(status))
                            {
                                sresult.Message = "Operation successful.";
                                sresult.Success = true;
                                return sresult;
                            }
                        }
                    }
                }
                catch (Exception e) { sresult.Message = e.ToString(); }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public Tuple<bool, string, string> GetAlterReservationDetail(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children)
        {
            var reservationCost = "";
            var total = "";
            var isSuccess = false;
            try
            {
                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var quote = ob["quote"];
                    quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                    quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");

                    var fees = ((JArray)ob["quote"]["fees"]).ToList();

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                        rentalNode["amount"]["amount"] = price;

                    quote["numChildren"] = children;
                    quote["numAdults"] = adults;

                    try
                    {
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(CommPage.Html);
                            total = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                            fees = ((JArray)ob["fees"]).ToList();

                            rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                            if (rentalNode != null)
                                reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                            isSuccess = true;
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            catch (Exception e)
            {
            }
            return new Tuple<bool, string, string>(isSuccess, reservationCost, total);
        }

        public bool ProcessAlterReservation(string token, string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message = "")
        {
            var alterResult = ProcessAlterReservation(token, conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Add);
            if (alterResult == false)
                alterResult = ProcessAlterReservation(token, conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Replace);
            if (alterResult == false)
                alterResult = ProcessAlterReservation(token, conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Update);

            return alterResult;
        }

        private bool ProcessAlterReservation(string token, string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message, AlterType alterType)
        {
            bool processAlteration = true;
            var isLoaded = ValidateTokenAndLoadCookies(token);

            if (isLoaded)
            {
                try
                {


                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        var quote = ob["quote"];
                        quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                        quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");

                        // quote["paymentRequests"][0]["status"] = "SCHEDULED";
                        var fees = ((JArray)ob["quote"]["fees"]).ToList();



                        var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                        if (rentalNode != null)
                            rentalNode["amount"]["amount"] = price;

                        quote["numChildren"] = children;
                        quote["numAdults"] = adults;
                        quote["bodyText"] = message;

                        if (processAlteration)
                        {
                            try
                            {

                                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1");//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");
                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                                var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                                var auth_token = cc["crumb"].Value.DecodeURL();
                                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                                commPage.Referer = "";
                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(CommPage.Html);
                                    if (ob["violations"] != null)
                                    {
                                        return false;
                                    }
                                }



                                switch (alterType)
                                {
                                    case AlterType.Update:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                        break;
                                    case AlterType.Add:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

                                        break;
                                    case AlterType.Replace:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true");

                                        break;
                                    default:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                        break;
                                }

                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                commPage.JsonStringToPost = CommPage.Html;//JsonConvert.SerializeObject(quote);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                                cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                                auth_token = cc["crumb"].Value.DecodeURL();
                                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                                commPage.Referer = "";
                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(CommPage.Html);
                                    var quoteGuid = ob["quoteGuid"].ToSafeString();
                                    if (string.IsNullOrEmpty(quoteGuid))
                                    {
                                        return false;
                                    }

                                    return true;
                                }

                            }
                            catch (Exception e)
                            {

                            }
                        }
                        else
                        {
                            try
                            {
                                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                                commPage.Referer = "";
                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(CommPage.Html);
                                    var quoteTotal = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                                    fees = ((JArray)ob["fees"]).ToList();

                                    var reservationCost = "";
                                    var serviceFee = "";
                                    var cleaningFee = "";
                                    var refundableDamageDeposit = "";

                                    rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                                    if (rentalNode != null)
                                        reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                                }

                            }
                            catch (Exception e)
                            {

                            }

                        }

                    }









                }
                catch (Exception e)
                {
                }
            }

            return false;
        }

        public bool CancelAlteration(string reservationCode)
        {
            try
            {
                commPage.RequestURL = commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);
                string alterationId = "";
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {


                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-alter_cancel_data']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);
                        var alts = (JArray)ob["reservation"]["reservation_alterations"];
                        if (alts != null)
                        {
                            alterationId = alts[0]["reservation_alteration"]["id"].ToSafeString();
                        }
                    }
                }

                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                var url = "https://www.airbnb.com/reservation_alterations/cancel/" + alterationId;
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;

                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("utf8", "✓");
                commPage.Parameters.Add("authenticity_token", token);
                commPage.RequestHeaders = new Dictionary<string, string>();

                commPage.Referer = string.Format("https://www.airbnb.com/reservation_alterations/{0}", alterationId);
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation/itinerary"))
                    {
                        return true;
                    }
                }

            }
            catch (Exception e)
            {
            }
            return false;
        }

        public enum AlterType
        {
            Update,
            Add,
            Replace

        }

        public Tuple<Dictionary<string, string>, string> GetCancelReservationReasons(string token)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {



                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/cancellationReasonOptions?_restfully=true&site=vrbo");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var reasons = (JArray)ob["cancellationReasonOptions"];
                        Dictionary<string, string> declineReasons = new Dictionary<string, string>();
                        reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));

                        var reasonVersion = ob["version"].ToSafeString();
                        return new Tuple<Dictionary<string, string>, string>(declineReasons, reasonVersion);

                    }

                }
                catch (Exception e)
                {

                }
            }


            return null;

        }

        public JObject CancelReservationTotals(string quoteGuid, string unitLink)
        {

            try
            {



                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/cancellationRefundTotals?_restfully=true&locale=en_US&quoteGuid={0}&unitLink={1}&override=false&overrideType=NO_OVERRIDE", quoteGuid, unitLink);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                commPage.Referer = "";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob;

                }

            }
            catch (Exception e)
            {

            }



            return null;

        }

        public bool CancelReservation(string token, string conversationId, string cancelReason)
        {
            var result = false;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", conversationId);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        var cancellationPolicies = (JArray)ob["quote"]["cancellationPolicies"];
                        var quoteUuid = ob["quote"]["quoteGuid"].ToSafeString();
                        var unitLink = ob["quote"]["unitLink"].ToSafeString();
                        var reservationId = ob["reservation"]["reservationReferenceNumber"].ToSafeString();


                        var cancelTotals = CancelReservationTotals(quoteUuid, unitLink);

                        string postJsonRaw = string.Format("{{\"overridable\":false,\"unitLink\":\"{0}\",\"quoteUuid\":\"{1}\",\"currencyCode\":\"USD\",\"resId\":\"{2}\",\"hasLodgingTax\":false,\"isPPB\":false,\"damageDepositType\":null,\"damageDepositAutoRefundStatus\":null,\"amountEntered\":0,\"maxRefund\":0,\"maxPartialRefund\":0,\"cancelableAmount\":0,\"paymentScheduleUuid\":null,\"description\":null,\"shouldRefundProducts\":true,\"type\":\"REFUND\",\"cancelReservation\":true,\"cancellationRefundPolicyOverrideType\":\"NO_OVERRIDE\",\"cancellationReason\":\"{3}\",\"cancellationReasonsVersion\":\"ppb1t\"}}", unitLink, quoteUuid, reservationId, cancelReason);

                        var postJson = JObject.Parse(postJsonRaw);
                        postJson.Add("cancellationPolicies", cancellationPolicies);
                        postJson.Add("refundTotals", cancelTotals);


                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/refundAndCancel?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        CommPage.JsonStringToPost = JsonConvert.SerializeObject(postJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.Html == "" && (commPage.StatusCode == HttpStatusCode.OK || commPage.StatusCode == HttpStatusCode.NoContent))
                        {
                            return true;
                        }



                    }
                }

                catch (Exception e)
                { }
            }
            return result;
        }

        public bool EditReservation(string token, string threadId, DateTime? checkinDate = null, DateTime? checkoutDate = null, int? adults = null, int? children = null)
        {
            string quoteGuid = "";
            string apiReservationUrl = "";
            string amount = "";
            float maxRefundable = 0;
            string unitLink = "";
            var isLogedIn = ValidateTokenAndLoadCookies(token);
            if (isLogedIn)
            {
                try
                {

                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        unitLink = ob.SelectTokens("unitLink").FirstOrDefault().ToSafeString();
                        quoteGuid = ob.SelectTokens("quoteGuid").FirstOrDefault().ToSafeString();
                        var reservation = (JObject)ob["reservation"];
                        var reservationId = reservation["id"].ToString();
                        if (checkinDate.HasValue)
                            reservation["checkinDate"] = checkinDate.Value.ToString("yyyy-MM-dd");
                        if (checkoutDate.HasValue)
                            reservation["checkinDate"] = checkoutDate.Value.ToString("yyyy-MM-dd");
                        if (adults.HasValue)
                            reservation["numAdults"] = adults.Value;
                        if (children.HasValue)
                            reservation["numChildren"] = children.Value;
                        try
                        {

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/reservations/update?_restfully=true&locale=en_US&site=vrbo&reservationId={0}", reservationId);

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                            commPage.JsonStringToPost = JsonConvert.SerializeObject(reservation);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                            var auth_token = cc["crumb"].Value.DecodeURL();
                            commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                apiReservationUrl = ob["apiReservationUrl"].ToSafeString();


                            }

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/getAdjustedRent?externalRefLink={0}&_restfully=true", apiReservationUrl);

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            
                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                amount = ob["amount"].ToSafeString();
                                var curencyCode = ob["currencyCode"].ToSafeString();

                                var conversationURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                                commPage.RequestURL = conversationURL;
                                commPage.ReqType = CommunicationPage.RequestType.GET;
                                commPage.PersistCookies = true;
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "*");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(CommPage.Html);
                                    var tokens = ob.SelectTokens("refundableAmount");
                                    maxRefundable = tokens.Where(x => x is JObject)
                                          .Where(x => x["amount"].ToShort() > 0)
                                          .Select(x => x["amount"].ToShort()).FirstOrDefault();

                                    // EditReservation_Step2_RefundPayment(1, "testing..", conversationId);
                                    // EditReservation_Step2_AddPaymentRequest(quoteGuid, unitLink, "testing code", DateTime.Now.AddDays(3), "my message", 2);
                                    return true;
                                }
                                return true;
                            }

                        }
                        catch (Exception e)
                        {
                            return false;
                        }
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return false;
        }

        public bool AddPaymentRequest(string token, string threadId, string description, DateTime dueDate, string message, float amount)
        {
            try
            {
                bool isAvailable = ValidateTokenAndLoadCookies(token);
                if (isAvailable)
                {
                    string unitLink = "";
                    string quoteGuid = "";

                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        unitLink = ob["quote"]["unitLink"].ToSafeString();
                        quoteGuid = ob["quote"]["quoteGuid"].ToSafeString();
                    }

                    string postJson = string.Format("{{\"quoteGuid\":\"{0}\",\"unitLink\":\"{1}\",\"payment\":{{\"amount\":{2},\"currencyCode\":\"USD\"}},\"description\":\"{3}\",\"ownerMessage\":\"{4}\",\"dueDate\":\"{5}\"}}", quoteGuid, unitLink, amount, description, message, dueDate.ToString("yyyy-MM-dd"));

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/add?_restfully=true");

                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.JsonStringToPost = postJson;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        quoteGuid = ob["quotePaymentScheduleGuid"].ToSafeString();
                        if (string.IsNullOrEmpty(quoteGuid))
                        {
                            return false;
                        }

                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        public bool SendRefund(string token, string threadId, short amount, string description)
        {
            try
            {
                bool isLoggedIn = ValidateTokenAndLoadCookies(token);

                if (isLoggedIn)
                {
                    var conversationURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                    commPage.RequestURL = conversationURL;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        var partialRefund = ob.FindTokens("partialRefundableAmount").FirstOrDefault();
                        var maxRefund = ob.FindTokens("refundableAmount").FirstOrDefault();
                        var cancelable = ob.FindTokens("cancelableAmount").FirstOrDefault();

                        var quoteUuid = ob.FindTokens("quoteGuid").FirstOrDefault().ToSafeString();
                        var resId = ob.FindTokens("reservationReferenceNumber").FirstOrDefault().ToSafeString();
                        var paymentScheduleUuid = ob.FindTokens("paymentRequestUUID").FirstOrDefault().ToSafeString();

                        string postJsonRaw = string.Format("{{\"paymentScheduleUuid\":\"{0}\",\"paymentMethodLabel\":\"\",\"quoteUuid\":\"{1}\",\"currencyCode\":\"USD\",\"resId\":\"{2}\",\"isPPB\":false,\"hasLodgingTax\":false,\"isOfflinePayment\":false,\"amountEntered\":\"{3}\",\"description\":\"{4}\",\"shouldRefundProducts\":false,\"type\":\"REFUND\",\"cancelReservation\":false,\"amount\":{{\"amount\":{3},\"currencyCode\":\"USD\",\"localized\":\"${3}\"}},\"totalRefundAmountLocalized\":\"${3}\"}}", paymentScheduleUuid, quoteUuid, resId, amount.ToString("0.0"), description);
                        var postJson = JObject.Parse(postJsonRaw);
                        postJson.Add("maxRefund", maxRefund);
                        postJson.Add("cancelableAmount", cancelable);
                        postJson.Add("maxPartialRefund", partialRefund);

                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/refund?_restfully=true");

                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(postJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.StatusCode == HttpStatusCode.NoContent)
                        {
                            return true;
                        }
                    }
                }
            }
            catch { return false; }
            return false;
        }

        #endregion

        #region COOKIES

        public bool ValidateTokenAndLoadCookies1()
        {
            
                BasicUserProfile profile = new Models.BasicUserProfile();

                string url = "https://www.vrbo.com/traveler/profile/edit";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    bool asd = commPage.Uri.AbsoluteUri.Contains("/profile/edit");
                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                    {
                        return true;
                    }
                }
                catch (Exception e) { }
            
            return false;
        }

        public bool ValidateTokenAndLoadCookies(string token, CommunicationPage currentCommPage = null)
        {
            var hasValidCookies = LoadCookies(token, currentCommPage);
            if (hasValidCookies)
            {
                BasicUserProfile profile = new Models.BasicUserProfile();

                string url = "https://www.vrbo.com/traveler/profile/edit";
                try
                {
                    if (currentCommPage == null)
                    {
                        commPage.RequestURL = url;
                        commPage.PersistCookies = true;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        ProcessRequest(commPage);
                        if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                        {

                            return true;

                        }

                    }
                    else
                    {
                        currentCommPage.RequestURL = url;
                        currentCommPage.PersistCookies = true;
                        currentCommPage.RequestHeaders = new Dictionary<string, string>();
                        currentCommPage.ReqType = CommunicationPage.RequestType.GET;
                        ProcessRequest(currentCommPage);
                        if (currentCommPage.IsValid && currentCommPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                        {

                            return true;

                        }
                    }
                }
                catch (Exception e) { }
            }
            return false;
        }

        public bool HomeawayValidateTokenAndLoadCookies(string token, CommunicationPage currentCommPage = null)
        {
            BasicUserProfile profile = new Models.BasicUserProfile();

            commPage = new CommunicationPage("https://www.homeaway.com/haod");
            commPage.PersistCookies = true;
            ProcessRequest(commPage);

            var hasValidCookies = LoadCookies(token, currentCommPage);
            if (hasValidCookies)
            {

                string url = "https://www.homeaway.com/traveler/profile/edit";
                try
                {
                    if (currentCommPage == null)
                    {
                        commPage.RequestURL = url;
                        commPage.PersistCookies = true;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.ReqType = CommunicationPage.RequestType.GET;


                        ProcessRequest(commPage);
                        if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                        {

                            return true;

                        }
                        else
                        {
                            using (var db = new ApplicationDbContext())
                            {
                                var entity = db.HostSessionCookies.Where(x => x.Token == token).FirstOrDefault();
                                if (entity != null)
                                {
                                    entity.IsActive = false;
                                    db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }

                    }
                    else
                    {
                        currentCommPage.RequestURL = url;
                        currentCommPage.PersistCookies = true;
                        currentCommPage.RequestHeaders = new Dictionary<string, string>();
                        currentCommPage.ReqType = CommunicationPage.RequestType.GET;


                        ProcessRequest(currentCommPage);
                        if (currentCommPage.IsValid && currentCommPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                        {

                            return true;

                        }
                    }
                }
                catch (Exception e)
                {
                }
            }

            return false;
        }

        public bool LoadCookies(string token, CommunicationPage currentCommPage = null)
        {
            var result = false;
            var session = Core.API.Vrbo.HostSessionCookies.HostSessionCookieForToken(token);
            if (session != null)
            {
                try
                {
                    var cookies = JsonConvert.DeserializeObject<List<Cookie>>(session.Cookies);
                    CookieCollection cc = new CookieCollection();
                    foreach (var cok in cookies)
                    {
                        cc.Add(cok);
                    }
                    CookieContainer cont = new CookieContainer();

                    cont.Add(cc);
                    commPage.COOKIES = cont;
                    if (currentCommPage != null)
                        currentCommPage.COOKIES = cont;
                    result = true;
                }
                catch (Exception e)
                {
                    
                }
            }
            else
            {
                commPage.COOKIES = null;
            }
            return result;
        }

        #endregion

        #region HOST PROFILE
        public bool ScrapeHostProfile(string password = "", bool isImporting = false, string token = "")
        {
            bool isLoaded = false;

            if (!string.IsNullOrEmpty(token))
            {
                isLoaded = ValidateTokenAndLoadCookies(token);
            }
            else isLoaded = true;

            if (isLoaded)
            {
                string url = "https://admin.vrbo.com/haod/up/basic.html";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var firstName = xd.SelectSingleNode("//input[@id='firstName']").GetAttributeFromNode("value");
                            var lastName = xd.SelectSingleNode("//input[@id='lastName']").GetAttributeFromNode("value");
                            var userEmail = xd.SelectSingleNode("//input[@id='emailAddress0']").GetAttributeFromNode("value");
                            var result = GetPublicUuidAndUserId(commPage.Html);
                            var userId = result.Item2;

                            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                            {
                                Core.API.Vrbo.Hosts.Add(userId, firstName, lastName, userEmail, password, "");

                                if (isImporting)
                                    //ImportHub.ShowHostName(GlobalVariables.UserId.ToString(), firstName, lastName, "Vrbo");

                                return true;
                            }
                        }
                    }
                }
                catch { }
            }
            return false;
        }
        #endregion

        public void SampleScrape()
        {
            string url = "http://animeheaven.eu/watch.php?a=Sword Art Online Alternative - Gun Gale Online=2";

            commPage = new CommunicationPage(url, CommunicationPage.RequestType.GET);

            ProcessRequest(commPage);

            if (commPage.IsValid)
            {

            }
        }

        public Tuple<string, string> GetPublicUuidAndUserId(string html)
        {
            var publicUuid = "";
            var userId = "";
            try
            {
                var start = commPage.Html.IndexOf("publicuuid: ") + "publicuuid: ".Length;
                var end = commPage.Html.IndexOf(",", start);
                var substring = commPage.Html.Substring(start, end - start).Replace(";", "");
                publicUuid = substring.Replace("this.formatUUID(\"", "").Replace("\"", "").Replace(")", "");
            }
            catch { }
            try
            {
                var start = commPage.Html.IndexOf("userid:") + "userid:".Length;
                var end = commPage.Html.IndexOf(")", start);
                var substring = commPage.Html.Substring(start, end - start);

                userId = substring.Replace("this.formatUUID(\"", "").Replace("\"", "").Replace(")", "").Trim();
            }
            catch { }

            return new Tuple<string, string>(publicUuid, userId);
        }

        private string Helper_ListingToTriad(string listingId)
        {
            try
            {
                commPage.RequestURL = "https://admin.vrbo.com/haod/properties.html";
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();

                    var properties = xd
                        .SelectSingleNode("//table[@class='list-table listingTable table table-hover']//tbody")
                        .SelectNodes("//tr[@class='ACTIVE']//td[@class='listing-summary']");

                    return properties
                        .Cast<XmlNode>()
                        .Where(x => x
                            .Attributes["data-listingguid"]
                            .Value
                            .Contains(listingId))
                        .First()
                        .Attributes["data-listingguid"]
                        .Value;
                }
                else return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private string Helper_GetUnitUrl(string listingTriad)
        {
            try
            {
                commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/minimumContent/getChecklistReportByListingTriad?id=PUBLISH_YOUR_LISTING&listingTriad={0}&_restfully=true", listingTriad);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    return ob["url"].ToString();
                }
                else return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private string Helper_GetDruidFromListingId(string listingTriad)
        {
            try
            {
                commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/properties/legacySummaries/?_restfully=true&locale=en_US&listingTriad={0}", listingTriad);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var arr = JArray.Parse(commPage.Html);
                    return arr[0]["druid"].ToSafeString();
                }
            }
            catch { }
            return string.Empty;
        }

        #region Shared
        public static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }
        #endregion

        #region DISPOSE
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
        #endregion

        #region ANALYTICS

        public Core.Models.Analytics GetAnalytics(string latitude, string longitude, DateTime date, int rooms, string excludedListings)
        {
            try
            {
                commPage.RequestURL = string.Format("{0}/api/vrbo/getanalytics?latitude={1}&longitude={2}&date={3}&propertycountgetavailable=true&averagepriceonlyavailable=false&rooms={4}{5}", GlobalVariables.ApiAnalyticsUrl, latitude.ToDouble(), longitude.ToDouble(), date.ToString("yyyy-MM-dd"), rooms, excludedListings);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    Core.Models.Analytics analytics = new Core.Models.Analytics()
                    {
                        AvailabilityRateByDate = ob["analytics"]["AvailabilityRateByDate"].ToDouble(),
                        AvailabilityRateByDateForXRooms = ob["analytics"]["AvailabilityRateByDateForXRooms"].ToDouble(),
                        AvailabilityRateByMonth = ob["analytics"]["AvailabilityRateByMonth"].ToDouble(),
                        AvailabilityRateByMonthForXRooms = ob["analytics"]["AvailabilityRateByMonthForXRooms"].ToDouble(),
                        AveragePriceByDate = ob["analytics"]["AveragePriceByDate"].ToDouble(),
                        AveragePriceByDateForXRooms = ob["analytics"]["AveragePriceByDateForXRooms"].ToDouble(),
                        AveragePriceByMonth = ob["analytics"]["AveragePriceByMonth"].ToDouble(),
                        AveragePriceByMonthForXRooms = ob["analytics"]["AveragePriceByMonthForXRooms"].ToDouble(),
                        OccupancyRateByDate = ob["analytics"]["OccupancyRateByDate"].ToDouble(),
                        OccupancyRateByDateForXRooms = ob["analytics"]["OccupancyRateByDateForXRooms"].ToDouble(),
                        OccupancyRateByMonth = ob["analytics"]["OccupancyRateByMonth"].ToDouble(),
                        OccupancyRateByMonthForXRooms = ob["analytics"]["OccupancyRateByMonthForXRooms"].ToDouble(),
                        PropertyCountByDate = ob["analytics"]["PropertyCountByDate"].ToInt(),
                        PropertyCountByDateForXRooms = ob["analytics"]["PropertyCountByDateForXRooms"].ToInt(),
                        TotalPropertyCountByDate = ob["analytics"]["TotalPropertyCountByDate"].ToInt(),
                        TotalPropertyCountByDateForXRooms = ob["analytics"]["TotalPropertyCountByDateForXRooms"].ToInt()
                        //PropertyCountByRooms = ob["analytics"]["PropertyCountByRooms"].ToInt()
                    };

                    return analytics;
                }
            }
            catch (Exception e)
            {
                return new Core.Models.Analytics();
            }

            return new Core.Models.Analytics();
        }

        #endregion

        #region GUEST

        public List<Core.Models.Reviews> HostReviews(string token, string guestId)
        {
            try
            {
                bool isValid = HomeawayValidateTokenAndLoadCookies(token);
                List<Core.Models.Reviews> hostReviews = new List<Core.Models.Reviews>();

                commPage.RequestURL = string.Format("https://admin.vrbo.com/traveler/profile/api/user-profile-service/extendedProfile/findByAccount?accountUuid={0}&reviewLevel=LIST", guestId);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviews = JArray.Parse(ob["receivedTravelerReviews"].ToString());

                    foreach (var review in reviews)
                    {
                        hostReviews.Add(new Core.Models.Reviews
                        {
                            Name = review["header"].ToString(),
                            ProfilePictureUrl = review["propertyImageUrl"].ToString(),
                            Comments = review["text"].ToString() == null ? "(No reviews given)" : review["text"].ToString(),
                            CreatedAt = review["stayDate"].ToDateTime()
                        });
                    }
                }

                return hostReviews;
            }
            catch (Exception e)
            {

            }

            return new List<Core.Models.Reviews>();
        }

        public List<Core.Models.Reviews> GuestReviews(string token, string guestId)
        {
            try
            {
                bool isValid = HomeawayValidateTokenAndLoadCookies(token);
                List<Core.Models.Reviews> hostReviews = new List<Core.Models.Reviews>();

                commPage.RequestURL = string.Format("https://admin.vrbo.com/traveler/profile/api/user-profile-service/extendedProfile/findByAccount?accountUuid={0}&reviewLevel=LIST", guestId);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviews = JArray.Parse(ob["givenUnitReviews"].ToString());

                    foreach (var review in reviews)
                    {
                        hostReviews.Add(new Core.Models.Reviews
                        {
                            Name = review["header"].ToString(),
                            ProfilePictureUrl = review["propertyImageUrl"].ToString(),
                            Comments = review["text"].ToString() == null ? "(No reviews given)" : review["text"].ToString(),
                            CreatedAt = review["stayDate"].ToDateTime()
                        });
                    }
                }

                return hostReviews;
            }
            catch (Exception e)
            {

            }

            return new List<Core.Models.Reviews>();
        }

        public List<Core.Models.Reviews> HomeawayHostReviews(string token, string guestId)
        {
            try
            {
                bool isValid = HomeawayValidateTokenAndLoadCookies(token);
                List<Core.Models.Reviews> hostReviews = new List<Core.Models.Reviews>();

                commPage.RequestURL = string.Format("https://www.homeaway.com/traveler/profile/api/user-profile-service/extendedProfile/findByAccount?accountUuid={0}&reviewLevel=LIST", guestId);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviews = JArray.Parse(ob["receivedTravelerReviews"].ToString());

                    foreach (var review in reviews)
                    {
                        hostReviews.Add(new Core.Models.Reviews
                        {
                            Name = review["header"].ToString(),
                            ProfilePictureUrl = review["propertyImageUrl"].ToString(),
                            Comments = review["text"].ToString() == null ? "(No reviews given)" : review["text"].ToString(),
                            CreatedAt = review["stayDate"].ToDateTime()
                        });
                    }
                }

                return hostReviews;
            }
            catch (Exception e)
            {

            }

            return new List<Core.Models.Reviews>();
        }

        public List<Core.Models.Reviews> HomeawayGuestReviews(string token, string guestId)
        {
            try
            {
                bool isValid = HomeawayValidateTokenAndLoadCookies(token);
                List<Core.Models.Reviews> hostReviews = new List<Core.Models.Reviews>();

                commPage.RequestURL = string.Format("https://www.homeaway.com/traveler/profile/api/user-profile-service/extendedProfile/findByAccount?accountUuid={0}&reviewLevel=LIST", guestId);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviews = JArray.Parse(ob["givenUnitReviews"].ToString());

                    foreach (var review in reviews)
                    {
                        hostReviews.Add(new Core.Models.Reviews
                        {
                            Name = review["header"].ToString(),
                            ProfilePictureUrl = review["propertyImageUrl"].ToString(),
                            Comments = review["text"].ToString() == null ? "(No reviews given)" : review["text"].ToString(),
                            CreatedAt = review["stayDate"].ToDateTime()
                        });
                    }
                }

                return hostReviews;
            }
            catch (Exception e)
            {

            }

            return new List<Core.Models.Reviews>();
        }

        #endregion
    }
}
