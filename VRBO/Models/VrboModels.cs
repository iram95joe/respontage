﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRBO.Enumerations;

namespace VRBO.Models
{
    public class InboxListing
    {
        public string Id { get; set; }
        public string GuestId { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestLastName { get; set; }
        public long ListingId { get; set; }
        public DateTime LastMessageAt { get; set; }
        public bool Unread { get; set; }
        public string MessageSnippet { get; set; }
        public bool InquiryOnly { get; set; }
        public string InquiryDate { get; set; }
        public string Status { get; set; }
        public string ReservationCode { get; set; }
        public DateTime StayStartDate { get; set; }
        public DateTime StayEndDate { get; set; }
        public string Type { get; set; }
    }

    public class InboxMessage
    {
        public string MessageId { get; set; }
        public string ThreadId { get; set; }
        public string Message { get; set; }
        public bool IsMyMessage { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Inbox : InboxMessage
    {
        public string Name { get; set; }
        public string ReservationLink { get; set; }
        public List<InboxMessage> Messages { get; set; }
    }

    public class ScrapeResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int TotalRecords { get; set; }
    }
    public class ListingDetail
    {
        public long Id { get; set; }
        public string ListingUrl { get; set; }
        public string Title { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string MinStay { get; set; }
        public string Internet { get; set; }

        public string Pets { get; set; }
        public string WheelChair { get; set; }
        public string Description { get; set; }
        public string Reviews { get; set; }
        public string Sleeps { get; set; }


        public int Bedrooms { get; set; }
        public int Bathrooms { get; set; }
        public string PropertyType { get; set; }
        public string AccomodationType { get; set; }
        public string Smoking { get; set; }
        public string AirCondition { get; set; }
        public string SwimmingPool { get; set; }

        public string PriceNightlyMin { get; set; }
        public string PriceNightlyMax { get; set; }
        public string PriceWeeklyMin { get; set; }
        public string PriceWeeklyMax { get; set; }
        public string Images { get; set; }
        public string Status { get; set; }

        public string HostName { get; set; }
        public string HostId { get; set; }
        public string HostImageUrl { get; set; }
        public string HostAddress { get; set; }
        public string HostAbout { get; set; }
        public string HostMemberSince { get; set; }
        public string HostReviewsCount { get; set; }

        public DateTime LastReviewAt { get; set; }

        public string OwnerPublicuuid { get; set; }
        public string OwnerGuid { get; set; }


    }
    public class ListingMetadata
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int MinStay { get; set; }
        public string Price { get; set; }
        public string HostName { get; set; }
        public string HostId { get; set; }
        public string NextAvailableCheckinDate { get; set; }
        public string NextAvailableCheckoutDate { get; set; }
    }
    public class ThreadMetadata
    {
        public ThreadMetadata()
        {
            MessageTemplates = new Dictionary<string, string>();
        }
        public string GuestId { get; set; }
        public string GuestName { get; set; }
        public string GuestPhone { get; set; }
        public string GuestEmail { get; set; }
        public string GuestAddress { get; set; }
        public string GuestCity { get; set; }
        public string GuestCountry { get; set; }

        public string HostId { get; set; }
        public string HostName { get; set; }
        public string HostPhone { get; set; }
        public string HostEmail { get; set; }


        public string CheckinDate { get; set; }
        public string CheckoutDate { get; set; }
        public string CheckinTime { get; set; }
        public string CheckoutTime { get; set; }


        public string ConfirmationCode { get; set; }
        public string NoOfGuests { get; set; }

        public string ListingName { get; set; }
        public string ListingAddress { get; set; }

        public string CleaningFee { get; set; }
        public string ServiceFee { get; set; }
        public string TotalPayout { get; set; }
        public string ReservationCost { get; set; }
        public string RefundableDamageDeposit { get; set; }
        public string RentalCost { get; set; }
        public string TaxRate { get; set; }
        public string TotalTax { get; set; }

        public string Status { get; set; }

        public string NextAvailableCheckinDate { get; set; }
        public string NextAvailableCheckoutDate { get; set; }

        public Dictionary<string, string> MessageTemplates { get; set; }
    }
    public class AirbnbUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ProfileImageUrl { get; set; }

    }
    public class Metadata
    {
        public string PropertyName { get; set; }
        public string DisplayName { get; set; }
        public string Value { get; set; }

        public static List<Metadata> GetList(ListingMetadata listing)
        {
            List<Metadata> list = new List<Metadata>();
            list.Add(new Metadata { PropertyName = "Title", DisplayName = "l_title", Value = listing.Title });
            list.Add(new Metadata { PropertyName = "Price", DisplayName = "l_price", Value = listing.Price });
            list.Add(new Metadata { PropertyName = "MinStay", DisplayName = "l_min_stay", Value = listing.MinStay.ToString() });
            list.Add(new Metadata { PropertyName = "NextAvailableCheckinDate", DisplayName = "l_checkin", Value = listing.NextAvailableCheckinDate });
            list.Add(new Metadata { PropertyName = "NextAvailableCheckoutDate", DisplayName = "l_checkout", Value = listing.NextAvailableCheckoutDate });
            list.Add(new Metadata { PropertyName = "HostId", DisplayName = "h_id", Value = listing.HostId });
            list.Add(new Metadata { PropertyName = "HostName", DisplayName = "h_name", Value = listing.HostName });
            return list;
        }
        public static List<Metadata> GetList(ThreadMetadata listing)
        {
            List<Metadata> list = new List<Metadata>();
            list.Add(new Metadata { PropertyName = "GuestId", DisplayName = "g_id", Value = listing.GuestId });
            list.Add(new Metadata { PropertyName = "GuestName", DisplayName = "g_name", Value = listing.GuestName });
            list.Add(new Metadata { PropertyName = "GuestEmail", DisplayName = "g_email", Value = listing.GuestEmail });
            list.Add(new Metadata { PropertyName = "GuestPhone", DisplayName = "g_phone", Value = listing.GuestPhone });
            list.Add(new Metadata { PropertyName = "GuestAddress", DisplayName = "g_address", Value = listing.GuestAddress });
            list.Add(new Metadata { PropertyName = "GuestCity", DisplayName = "g_city", Value = listing.GuestCity });
            list.Add(new Metadata { PropertyName = "GuestCountry", DisplayName = "g_country", Value = listing.GuestCountry });

            list.Add(new Metadata { PropertyName = "HostId", DisplayName = "h_id", Value = listing.HostId });
            list.Add(new Metadata { PropertyName = "HostName", DisplayName = "h_name", Value = listing.HostName });
            list.Add(new Metadata { PropertyName = "HostPhone", DisplayName = "h_phone", Value = listing.HostPhone });
            list.Add(new Metadata { PropertyName = "HostEmail", DisplayName = "h_email", Value = listing.HostEmail });

            list.Add(new Metadata { PropertyName = "CheckinDate", DisplayName = "checkin_date", Value = listing.CheckinDate });
            list.Add(new Metadata { PropertyName = "CheckoutDate", DisplayName = "checkout_date", Value = listing.CheckoutDate });
            list.Add(new Metadata { PropertyName = "CheckinTime", DisplayName = "checkin_time", Value = listing.CheckinTime });
            list.Add(new Metadata { PropertyName = "CheckoutTime", DisplayName = "checkout_time", Value = listing.CheckoutTime });
            //list.Add(new Metadata { PropertyName = "ConfirmationCode", DisplayName = "confirmation_code", Value = listing.ConfirmationCode });
            list.Add(new Metadata { PropertyName = "NoOfGuests", DisplayName = "no_of_guests", Value = listing.NoOfGuests });
            list.Add(new Metadata { PropertyName = "ListingName", DisplayName = "l_name", Value = listing.ListingName });
            list.Add(new Metadata { PropertyName = "ListingAddress", DisplayName = "l_address", Value = listing.ListingAddress });

            list.Add(new Metadata { PropertyName = "CleaningFee", DisplayName = "cleaning_fee", Value = listing.CleaningFee });
            list.Add(new Metadata { PropertyName = "ServiceFee", DisplayName = "service_fee", Value = listing.ServiceFee });
            list.Add(new Metadata { PropertyName = "TotalPayout", DisplayName = "total_payout", Value = listing.TotalPayout });
            list.Add(new Metadata { PropertyName = "ReservationCost", DisplayName = "reservation_cost", Value = listing.ReservationCost });
            list.Add(new Metadata { PropertyName = "RefundableDamageDeposit", DisplayName = "refundable_damage_deposit", Value = listing.RefundableDamageDeposit });
            list.Add(new Metadata { PropertyName = "RentalCost", DisplayName = "rental_cost", Value = listing.RentalCost });
            list.Add(new Metadata { PropertyName = "TaxRate", DisplayName = "tax_rate", Value = listing.TaxRate });
            list.Add(new Metadata { PropertyName = "TotalTax", DisplayName = "total_tax", Value = listing.TotalTax });
            list.Add(new Metadata { PropertyName = "Status", DisplayName = "status", Value = listing.Status });
            list.Add(new Metadata { PropertyName = "NextAvailableCheckinDate", DisplayName = "next_checkin", Value = listing.NextAvailableCheckinDate });
            list.Add(new Metadata { PropertyName = "NextAvailableCheckoutDate", DisplayName = "next_checkout", Value = listing.NextAvailableCheckoutDate });
            foreach (var item in listing.MessageTemplates)
            {
                list.Add(new Metadata { PropertyName = "MessageTemplates", DisplayName = item.Key, Value = item.Value });
            }
            return list;
        }
    }


    public class Reservation
    {
        public Reservation()
        { }
        //public Reservation(Core.API.Inquiry inq)
        //{
        //    ReservationId = inq.ReservationId;
        //    Code = inq.Code;
        //    Status = inq.Status;
        //    CheckInDate = inq.StayStartDate.ToSafeString();
        //    CheckoutDate = inq.StayEndDate.ToSafeString();
        //    Nights = inq.Nights;
        //    ListingName = inq.ListingName;
        //    ListingId = inq.ListingId;
        //    GuestId = inq.GuestId;
        //    GuestName = inq.GuestName;
        //    GuestEmail = inq.GuestEmail;
        //    GuestPhone = inq.GuestPhone;
        //    ReservationCost = inq.ReservationCost;
        //    CleaningFee = inq.CleaningFee;
        //    ServiceFee = inq.ServiceFee;
        //    TotalPayout = inq.TotalPayout;
        //    GuestCount = inq.GuestCount.ToSafeString();
        //    Pets = inq.Pets;
        //    ReplyActions = JsonConvert.DeserializeObject<List<ReplyAction>>(inq.ReplyActions);
        //    ConversationId = inq.ConversationId;
        //    ListingUniqueId = inq.ListingUniqueId;
        //}
        
        public string ReservationId { get; set; }
        public int HostId { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public int Nights { get; set; }
        public string ListingName { get; set; }
        public long ListingId { get; set; }
        public string ListingUrl { get; set; }
        public string GuestId { get; set; }
        public string GuestUrl { get; set; }
        public string GuestEmail { get; set; }
        public string GuestPhone { get; set; }
        public string ReservationCost { get; set; }
        public string ReservationCostSummary { get; set; }
        public string CleaningFee { get; set; }
        public string ServiceFee { get; set; }
        public string TotalPayout { get; set; }
        public string InquiryDatetime { get; set; }
        public string ReservationRequestDatetime { get; set; }
        public string ReservationRequestExpiryDatetime { get; set; }
        public string ReservationRequestAcceptedDatetime { get; set; }
        public string ReservationRequestCancelledDatetime { get; set; }
        public int GuestCount { get; set; }
        public int Adult { get; set; }
        public int Children { get; set; }
        public int StatusType { get; set; }
        public string StatusDesc { get; set; }
        public bool CanSendSpecialOffer { get; set; }
        public string GuestName { get; set; }
        public bool IsActive { get; set; }
        public List<ReplyAction> ReplyActions { get; set; }
        public string Pets { get; set; }

        public bool CanChange(List<Reservation> reservations, Reservation currentReservation)
        {
            var hasAnyOtherWithActive = reservations.Where(f => f.ReservationId != currentReservation.ReservationId && f.IsActive == true).FirstOrDefault();
            return hasAnyOtherWithActive != null;
        }

        public string ThreadId { get; set; }
        public string ListingUniqueId { get; set; }
        public string RentalCost { get; set; }
        public string TaxRate { get; set; }
        public string TotalTax { get; set; }
        public string RefundableDamageDeposit { get; set; }
        public string Type { get; set; }
    }
    public class ReplyAction
    {
        public string Type { get; set; }
        public Dictionary<string, string> MetaData { get; set; }
    }
    public class TransactionHistory
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string ReleaseDate { get; set; }
        public string ArrivalDate { get; set; }
        public string PayoutId { get; set; }
        public string ListingId { get; set; }
        public string ListingName { get; set; }
        public string GuestName { get; set; }
        public string ReservationCode { get; set; }
        public string ReservationId { get; set; }
    }
    public class OwnerProfile
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string BirthMonth { get; set; }
        public string BirthDate { get; set; }
        public string BirthYear { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Language { get; set; }
        public string School { get; set; }
        public string Currency { get; set; }
        public string Work { get; set; }
        public string AboutMe { get; set; }
        public string Timezone { get; set; }
        public string VideoUrl { get; set; }
        public string ImageUrl { get; set; }

        public string PublicUuId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
    public class BasicUserProfile
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }

    }
    public class SearchViewModel
    {
        public DateTime Checkin { get; set; }
        public DateTime Checkout { get; set; }
        public string Location { get; set; }
        public int? NoOfRecords { get; set; }
        public int NoOfGuests { get; set; }
    }
    public class SearchRangeViewModel
    {
        public int SearchForDaysAhead { get; set; }
        public int MaxNightStays { get; set; }
        public string Location { get; set; }

        public int? NoOfRecords { get; set; }

    }
    public class Calendar
    {
        public DateTime Date { get; set; }
        public bool IsAvailable { get; set; }
        public string Price { get; set; }
    }

    public class SpecialOfferSummary
    {
        public decimal Subtotal { get; set; }
        public decimal GuestPays { get; set; }
        public decimal HostEarns { get; set; }
        public bool IsAvailable { get; set; }
    }
    public class ThreadViewModel
    {
        public string ThreadId { get; set; }
        public string OtherUserName { get; set; }
        public string OtherUserId { get; set; }
        public string OtherUserProfilePic { get; set; }
        public string ListingName { get; set; }
        public string LastMessageAt { get; set; }
        public bool HasUnreadMessages { get; set; }
        public string LastMessageSnippet { get; set; }
        public string ReservatoinLink { get; set; }

        //public ThreadViewModel(Core.API.Inbox inbox)
        //{
        //    ThreadId = inbox.ThreadId;
        //    OtherUserName = inbox.OtherUserName;
        //    OtherUserId = inbox.OtherUserId;
        //    OtherUserProfilePic = "";
        //    ListingName = inbox.ListingName;
        //    LastMessageAt = inbox.LastMessageAt;
        //    HasUnreadMessages = inbox.HasUnreadMessages;
        //    LastMessageSnippet = inbox.LastMessageSnippet;
        //    ReservatoinLink = inbox.ReservationId;
        //}
    }

    public class MessageViewModel
    {
        public string Sender { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public string ProfilePath { get; set; }
        public string ProfileImage { get; set; }
        public string MessageId { get; set; }
        public bool IsMyMessage { get; set; }
        public string ProfileId { get; set; }
        //public MessageViewModel(Core.API.InboxMessage message)
        //{
        //    Sender = message.Sender;
        //    Message = message.Message;
        //    Timestamp = message.Timestamp;
        //    ProfilePath = message.ProfilePath;
        //    ProfileImage = message.ProfileImagePath;
        //    MessageId = message.MessageId;
        //    IsMyMessage = message.IsMyMessage;
        //    ProfileId = message.ProfileId;
        //}
    }
    public class SearchResult
    {
        public DateTime Checkin { get; set; }
        public DateTime Ceckout { get; set; }
        public int NoOfGuests { get; set; }
        public List<string> ListingIds { get; set; }
    }
    public class LoginResult
    {
        public bool Success { get; set; }
        public string SessionToken { get; set; }
        public AirlockViewModel AirLock { get; set; }
    }

    public class AirlockViewModel
    {
        public string ChallengeToken { get; set; }
        public string Code { get; set; }
        public Dictionary<string, string> HiddenFields { get; set; }
        public string FormAction { get; set; }
        public Dictionary<string, string> PhoneNumbers { get; set; }
        public AirlockChoice SelectedChoice { get; set; }
        public string SelectedPhoneNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
