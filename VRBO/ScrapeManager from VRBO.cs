﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using BaseScrapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Collections;
using System.Reflection;
using VRBO.Models;
using VRBO.Enumerations;
using Core.Enumerations;

namespace Scraper
{
    public class ScrapeManager : CommunicationEngine
    {

        public ScrapeManager()
        {
            commPage = new CommunicationPage("https://airbnb.com");
        }
        private Core.Database.Context.ApplicationDbContext _db;
        public Core.Database.Context.ApplicationDbContext Database
        {
            get
            {
                if (_db == null)
                    _db = new Core.Database.Context.ApplicationDbContext();

                return _db;
            }
        }
        public CommunicationPage commPage = null;

        public void Testing(string email, string password)
        {
            //var token = LoginWithApi(email, password);
            var url = string.Format("https://api.airbnb.com/v2/listings?_limit=10&has_availability=false&_format=v1_legacy_long&_offset=0&user_id={0}&client_id=d306zoyjsyarp7ifhu67rjxn52tv0t20&locale=en-US&currency=USD", "24056456");
            commPage = new CommunicationPage(url);
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {

            }
        }

        #region LOGIN

        public Core.API.SortableBindingList<Core.API.Vrbo.HostsHostList> GetHosts(string hostName = "", int? reviewsMoreThan = null, int? noOfPropertiesMoreThan = null, int? totalRecords = null)
        {
            return Core.API.Property.Hosts(Database, hostName, noOfPropertiesMoreThan, reviewsMoreThan, totalRecords);
        }
        public string LoginWithApi(string email, string password)
        {
            string url = "https://api.airbnb.com/v1/authorize";
            commPage = new CommunicationPage(url);
            commPage.ReqType = CommunicationPage.RequestType.POST;
            var parms = new Dictionary<string, string>();
            parms.Add("client_id", "d306zoyjsyarp7ifhu67rjxn52tv0t20");
            parms.Add("locale", "en-US");
            parms.Add("currency", "USD");

            parms.Add("grant_type", "password");
            parms.Add("username", email);
            parms.Add("password", password);
            commPage.PersistCookies = true;
            commPage.Parameters = parms;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(commPage.Html);
                var token = ob["access_token"].ToSafeString();

                return token;
            }
            return string.Empty;
        }

        public LoginResult Login(string email, string password, bool forceLogin = false)
        {
            var result = new LoginResult();
            if (!forceLogin && commPage != null && commPage.IsLoggedIn)
            {
                result.Success = true;
                return result;
            }
            string url = "https://admin.vrbo.com/haod";
            commPage = new CommunicationPage(url);
            //  commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                if (xd != null)
                {
                    var formAction = xd.SelectSingleNode("//form[@id='login-form']").GetAttributeFromNode("action");
                    var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "login-form");
                    hiddenFields["username"] = email;
                    hiddenFields["password"] = password.Trim();




                   // commPage.RequestURL = "https://cas.homeaway.com" + formAction;
                    commPage.RequestURL = "https://www.vrbo.com" + formAction;
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);

                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/dash/"))
                    {
                        commPage.IsLoggedIn = true;
                        var tempCommPage = commPage;
                        tempCommPage.Html = "";

                        var cks = GetAllCookies(commPage.COOKIES);
                        var cookieList = cks.ToList();
                        var serializedCookies = JsonConvert.SerializeObject(cookieList);
                        var saveResult = Core.API.Vrbo.HostSessionCookies.Add(serializedCookies);

                        result.Success = true;
                        result.SessionToken = saveResult.HostSessionCookie.Token;
                        return result;
                    }
                    else if (commPage.IsValid && commPage.Html.Contains("Two-Factor Authentication"))
                    {
                        //var lockId = HttpUtility.ParseQueryString(commPage.Uri.Query).Get("al_id");

                        xd = commPage.ToXml();
                        var userName = "";
                        var userProfileImageUrl = "";
                        var phoneNumbers = new Dictionary<string, string>();
                        var radioNodes = xd.SelectNodes("//div[@id='sms-numbers']//div[@class='radio']");
                        foreach (XmlNode radio in radioNodes)
                        {
                            var val = radio.SelectSingleNode(".//input").GetAttributeFromNode("value");
                            var label = radio.GetInnerTextFromNode();
                            phoneNumbers.Add(val, label);
                        }
                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-form");
                        var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-form']").GetAttributeFromNode("action");



                        result.Success = false;
                        result.AirLock = new AirlockViewModel()
                        {
                            HiddenFields = hdnFields,
                            //FormAction = "https://cas.homeaway.com" + authformAction,
                            FormAction = "https://www.vrbo.com" + authformAction,
                            PhoneNumbers = phoneNumbers,

                        };
                        return result;


                        /*
                                                //_bootstrap-airlock_data
                                                string airlockUrl = commPage.Uri.AbsoluteUri;
                                                var u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", lockId);
                                                var tt = string.Format("{0}", lockId);
                                                var pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{2}}}}}}}", lockId,currentUserId,phoneNumId);

                                                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                                                var token = cc["_csrf_token"].Value.DecodeURL();

                                                commPage.RequestURL = u;
                                                commPage.PersistCookies = true;
                                                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                                                CommPage.JsonStringToPost = pJson;
                                                commPage.RequestHeaders = new Dictionary<string, string>();
                                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                                                CommPage.RequestHeaders.Add("X-CSRF-Token", token);

                                                commPage.Referer = airlockUrl;
                                                ProcessRequest(commPage);
                                                if (commPage.IsValid)
                                                {
                                                    result.Success = false;
                                                    result.AirLock = new AirlockViewModel()
                                                    {
                                                          LockId = lockId,
                                                           PhoneId = phoneNumId,
                                                            UserId = currentUserId
                                                    };
                                                    return result;

                                                }

                                                */
                    }
                }
            }
            result.Success = false;
            return result;
        }

        public AirlockViewModel ProcessAirlockSelection(AirlockViewModel airlockModel)
        {
            var parameters = airlockModel.HiddenFields;
            switch (airlockModel.SelectedChoice)
            {
                case AirlockChoice.SMS:
                    parameters["sendType"] = "SMS";
                    break;
                case AirlockChoice.Call:
                    parameters["sendType"] = "CALL";

                    break;

            }
            parameters["phoneId"] = airlockModel.SelectedPhoneNumber;


            commPage.RequestURL = airlockModel.FormAction;
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            CommPage.Parameters = parameters;
            ProcessRequest(commPage);
            if (commPage.IsValid && commPage.Html.Contains("A unique verification code has been sent"))
            {

                var xd = commPage.ToXml();
                var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-verify-form");
                var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-verify-form']").GetAttributeFromNode("action");

                airlockModel.HiddenFields = hdnFields;
                // airlockModel.FormAction = "https://cas.homeaway.com" + authformAction;
                airlockModel.FormAction = "https://www.vrbo.com" + authformAction;
                return airlockModel;

            }
            return airlockModel;

        }
        public Tuple<bool, string> ProcessAirlock(AirlockViewModel airlockModel)
        {
            var parameters = airlockModel.HiddenFields;
            parameters["rememberMe"] = "true";
            parameters["_rememberMe"] = "on";
            parameters["code"] = airlockModel.Code;
            // parameters.Remove("phoneId");
            // parameters.Remove("sendType");
            //  parameters["htmlFormName"] = "cas2FAVerifyCode:two-factor-auth-verify-form";
            // parameters["flowKey"] = "e24a12a0f298f43bf9125461a5cce8b7as3";
            commPage.RequestURL = airlockModel.FormAction.Replace("https://cas.homeaway.com", "https://www.vrbo.com");
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Parameters = parameters;
            commPage.RequestHeaders = new Dictionary<string, string>();
            //commPage.RequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
            commPage.Referer = airlockModel.FormAction;
            ProcessRequest(commPage);
            if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties")))
            {
                commPage.IsLoggedIn = true;


                var cks = GetAllCookies(commPage.COOKIES);
                var cookieList = cks.ToList();
                var serializedCookies = JsonConvert.SerializeObject(cookieList);
                var saveResult = Core.API.Vrbo.HostSessionCookies.Add(serializedCookies);
                return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);

            }
            var redirectcount = 1;
            if (!commPage.IsLoggedIn)
            {
                REDIRECT:
                var xd = commPage.ToXml();
                var prms = ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "continue-form");
                prms["_eventId"] = "submit";
                commPage.RequestURL = airlockModel.FormAction;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                CommPage.Parameters = prms;
                commPage.Referer = airlockModel.FormAction;
                ProcessRequest(commPage);
                if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties")))
                {
                    commPage.IsLoggedIn = true;
                }
                else
                {
                    if (redirectcount < 2)
                    {
                        redirectcount++;
                        goto REDIRECT;
                    }
                }
            }
            if (commPage.IsLoggedIn)
            {
                var cks = GetAllCookies(commPage.COOKIES);
                var cookieList = cks.ToList();
                var serializedCookies = JsonConvert.SerializeObject(cookieList);
                var saveResult = Core.API.Vrbo.HostSessionCookies.Add(serializedCookies);

                return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);
            }


            return new Tuple<bool, string>(false, "");
        }
        #endregion

        //#region Inbox
        //public ScrapeResult ScrapeInboxThread(string token, string threadId)
        //{
        //    var sresult = new ScrapeResult();
        //    var isloaded = ValidateTokenAndLoadCookies(token);
        //    if (isloaded)
        //    {
        //        try
        //        {
        //            ScrapeInboxThreadListing(false, false);
        //            var requiredThread = listings.Where(f => f.Id == threadId).FirstOrDefault();
        //            if (requiredThread != null)
        //            {
        //                var r = ScrapeMessageDetails(requiredThread.Id);
        //                sresult.Success = true;
        //                sresult.Message = "Operation successful!, Total messages in thread: " + r.Item2.Count();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            sresult.Message = e.Message;
        //        }
        //    }
        //    return sresult;
        //}
        //public ScrapeResult ScrapeInbox(string token, bool newOnly, bool inquiriesOnly)
        //{
        //    var sresult = new ScrapeResult();
        //    var isloaded = ValidateTokenAndLoadCookies(token);
        //    if (isloaded)
        //    {
        //        try
        //        {
        //            ScrapeInboxThreadListing(newOnly, inquiriesOnly);
        //            var totalItems = MessageDetailWrapper();
        //            if (totalItems > 0)
        //            {
        //                sresult.Success = true;
        //                sresult.Message = "Operation Successful";
        //                sresult.TotalRecords = totalItems;
        //            }
        //            else
        //            {
        //                if (newOnly)
        //                    sresult.Message = "No new message";
        //                else
        //                    sresult.Message = "An error occurred.";

        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            sresult.Message = e.Message;
        //        }
        //    }
        //    else
        //    {
        //        sresult.Message = "Could not login, Please check you details and try again.";
        //    }

        //    return sresult;
        //}


        //private void ScrapeInboxThreadListing(bool newOnly, bool inquiryOnly)
        //{
        //    var offset = 0;
        //    var hasMore = false;
        //    do
        //    {
        //        string url = string.Format("https://www.airbnb.com/api/v2/threads?_format=for_web_inbox&_offset={0}&role={1}&selected_inbox_type=host&include_help_threads=true&include_support_messaging_threads=true&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", offset, newOnly ? "unread" : "all");

        //        hasMore = false;
        //        commPage.RequestURL = url;
        //        commPage.ReqType = CommunicationPage.RequestType.GET;
        //        commPage.PersistCookies = true;
        //        commPage.Referer = "https://www.airbnb.com/login";
        //        commPage.RequestHeaders = new Dictionary<string, string>();
        //        ProcessRequest(commPage);
        //        if (commPage.IsValid)
        //        {
        //            var ob = JObject.Parse(commPage.Html);
        //            var ar_tempt = (JArray)ob["threads"];
        //            var ar = ar_tempt.ToList();
        //            if (inquiryOnly)
        //            {
        //                ar = ar.Where(f => f["total_price_web_inbox"].ToSafeString() == "" || f["status"].ToSafeString() == "special_offer" || f["status"].ToSafeString() == "inquiry").ToList();

        //            }
        //            if (ar.Count > 0)
        //            {
        //                hasMore = true;
        //                offset += 10;
        //                foreach (var item in ar)
        //                {
        //                    var id = item["id"].ToSafeString();
        //                    var userName = item["other_user"]["first_name"].ToSafeString();
        //                    var userId = item["other_user"]["id"].ToSafeString();
        //                    var userProfilePic = item["other_user"]["profile_pic_url_small"].ToSafeString();
        //                    var lastMessageAt = item["last_message_at_smarter"].ToSafeString();
        //                    var readStatus = item["unread"].ToBoolean();
        //                    var messageSnippet = item["message_snippet"].ToSafeString();
        //                    var inquiryDateRange = item["inquiry_date_range"].ToSafeString();
        //                    var status = item["status_string"].ToSafeString();
        //                    var listingName = item["listing"]["name"].ToSafeString();

        //                    var isInquiryOnly = string.IsNullOrEmpty(item["total_price_web_inbox"].ToSafeString()) || status == "Special Offer" || status == "Inquiry";
        //                    var isSpecialOfferSent = status == "Special Offer";
        //                    listings.Add(new InboxListing { Id = id, ListingName = listingName, OtherUserName = userName, OtherUserId = userId, LastMessageAt = lastMessageAt, OtherUserProfilePic = userProfilePic, Unread = readStatus, MessageSnippet = messageSnippet, InquiryOnly = isInquiryOnly, InquiryDateRange = inquiryDateRange, Status = status, IsSpecialOfferSent = isSpecialOfferSent });
        //                }
        //            }
        //            else
        //            {
        //                hasMore = false;
        //            }
        //        }
        //    } while (hasMore);
        //}
        //private int MessageDetailWrapper()
        //{
        //    List<Inbox> InboxItems = new List<Inbox>();
        //    foreach (var item in listings)
        //    {
        //        var result = ScrapeMessageDetails(item.Id);

        //        var thread = Core.API.Inbox.Add(Database, item.OtherUserName, item.ListingName, result.Item1, item.LastMessageAt, item.Unread, item.MessageSnippet, item.OtherUserId, item.OtherUserProfilePic, item.Id, item.InquiryOnly, item.InquiryDateRange, item.Status);
        //        if (thread.IsSaved)
        //        {
        //            foreach (var msg in result.Item2)
        //            {
        //                var addResult = Core.API.InboxMessage.Add(Database, thread.Thread, msg.MessageId, msg.Message, false, msg.Sender, msg.ProfileImage, msg.ProfilePath, msg.ProfileId, msg.IsMyMessage, msg.Timestamp.ToDateTime());
        //                if (addResult.IsDuplicate)//all next would be already in db
        //                    break;
        //            }
        //        }


        //        //InboxItems.Add(new Inbox
        //        //{
        //        //    Name = item.OtherUserName, Messages = result.Item2, ReservationLink = result.Item1 }
        //        //);
        //    }

        //    // var dir = AppDomain.CurrentDomain.BaseDirectory + "inbox.json";
        //    //string jsonString = JsonConvert.SerializeObject(InboxItems, Newtonsoft.Json.Formatting.Indented);

        //    //using (StreamWriter outputFile = new StreamWriter(outputPath))
        //    //{
        //    //    outputFile.Write(jsonString);
        //    //}
        //    return InboxItems.Count;
        //}
        //public Tuple<string, List<InboxMessage>> ScrapeMessageDetails(string id)
        //{
        //    List<InboxMessage> messages = new List<InboxMessage>();
        //    string url = string.Format("https://www.airbnb.com/z/q/{0}", id);
        //    commPage.RequestURL = url;
        //    commPage.ReqType = CommunicationPage.RequestType.GET;
        //    commPage.PersistCookies = true;
        //    ProcessRequest(commPage);
        //    var reservationLink = "";
        //    if (commPage.IsValid)
        //    {
        //        XmlDocument xd = commPage.ToXml();
        //        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
        //        var json = meta.GetAttributeFromNode("content").HtmlDecode();
        //        var ob = JObject.Parse(json);
        //        var ar = (JArray)ob["appData"]["posts"];
        //        var currentUserId = ob["appData"]["currentUserId"].ToSafeString();
        //        //  var users = (JArray)ob["appData"]["participantsProfiles"];
        //        //participantsProfiles
        //        if (ar.Count > 0)
        //        {
        //            foreach (var item in ar)
        //            {
        //                var userId = item["user_id"].ToSafeString();
        //                var myMessage = false;
        //                if (userId == currentUserId)
        //                {
        //                    myMessage = true;
        //                }
        //                var sender = ob["appData"]["participantsProfiles"][userId]["full_name"].ToSafeString();
        //                var profilelink = "https://www.airbnb.com" + ob["appData"]["participantsProfiles"][userId]["profile_path"].ToSafeString();
        //                var profileImagelink = ob["appData"]["participantsProfiles"][userId]["profile_pic_url"].ToSafeString();

        //                var message = item["message"].ToSafeString();
        //                var createdAt = item["created_at"].ToSafeString();
        //                var messageId = item["id"].ToSafeString();
        //                if (!string.IsNullOrEmpty(message))
        //                {
        //                    messages.Add(new InboxMessage { Message = message, Sender = sender, Timestamp = createdAt, ProfilePath = profilelink, ProfileImage = profileImagelink, MessageId = messageId, IsMyMessage = myMessage, ProfileId = userId });

        //                }
        //            }

        //            var reservationCode = ob["appData"]["reservationInfo"]["confirmation_code"].ToSafeString();
        //            if (!string.IsNullOrEmpty(reservationCode))
        //                reservationLink = "https://www.airbnb.com/reservation/itinerary?code=" + reservationCode;
        //        }
        //    }
        //    return new Tuple<string, List<InboxMessage>>(reservationLink, messages);
        //}
        //#endregion
        #region Inbox
        public ScrapeResult ScrapeInboxThread(string token, string threadId)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    List<InboxListing> listings = ScrapeInboxThreadListing(false, InboxThreadType.All);
                    var requiredThread = listings.Where(f => f.Id == threadId).FirstOrDefault();
                    if (requiredThread != null)
                    {
                        var r = ScrapeMessageDetails(requiredThread.Id);
                        sresult.Success = true;
                        sresult.Message = "Operation successful!, Total messages in thread: " + r.Count();
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            return sresult;
        }
        public ScrapeResult ScrapeInbox(string token, bool newOnly, InboxThreadType type)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    List<InboxListing> listings = ScrapeInboxThreadListing(newOnly, type);
                    var totalItems = MessageDetailWrapper(listings);
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        if (newOnly)
                            sresult.Message = "No new message";
                        else
                            sresult.Message = "An error occurred.";

                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }

        public enum InboxThreadType
        {
            All,
            Inquiries,
            ReservationRequests,
            Reservations,
            Tentative,
            Cancelled,
            CurrentStay,
            PostStay
        }
        private List<InboxListing> ScrapeInboxThreadListing(bool newOnly, InboxThreadType threadType)
        {
            List<InboxListing> threadList = new List<InboxListing>();
            var pageNumber = 1;
            var hasMore = false;
            do
            {
                string url = string.Format("https://admin.vrbo.com/rm/proxies/conversations/inbox?_restfully=true&page={0}&pageSize=25&locale=en_US&sort=received&sortOrder=desc", pageNumber);

                switch (threadType)
                {
                    case InboxThreadType.All:
                        break;
                    case InboxThreadType.Inquiries:
                        url = url + "&status=INQUIRY";
                        break;
                    case InboxThreadType.ReservationRequests:
                        url = url + "&status=RESERVATION_REQUEST";
                        break;
                    case InboxThreadType.Reservations:
                        url = url + "&status=RESERVATION";
                        break;
                    case InboxThreadType.Tentative:
                        url = url + "&status=tentative_reservation";
                        break;
                    case InboxThreadType.Cancelled:
                        url = url + "&status=cancelled";
                        break;
                    case InboxThreadType.CurrentStay:
                        url = url + "&status=staying";
                        break;
                    case InboxThreadType.PostStay:
                        url = url + "&status=post_stay";
                        break;
                    default:
                        break;
                }

                hasMore = false;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.Referer = "";
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var ar = (JArray)ob["conversations"];

                    if (ar.Count > 0)
                    {

                        var currentPage = ob["display"]["page"].ToInt();
                        var pageSize = ob["display"]["pageSize"].ToInt();
                        var totalResults = ob["display"]["totalResults"].ToInt();

                        if ((currentPage * pageSize) < totalResults)
                            hasMore = true;

                        pageNumber++;
                        foreach (var item in ar)
                        {
                            var readStatus = item["unreadMessages"].ToBoolean();
                            if (newOnly && readStatus == false)
                            {
                                hasMore = false;
                                break;
                            }

                            var id = item["id"].ToSafeString();
                            var userName = item["correspondent"]["firstName"].ToSafeString();
                            var userId = item["correspondent"]["profileUuid"].ToSafeString();
                            var lastMessageAt = item["lastMessageReceivedDate"].ToSafeString();
                            var messageSnippet = item["lastMessage"]["message"].ToSafeString();
                            var inquiryDateRange = item["created"].ToSafeString();
                            var status = item["status"].ToSafeString();
                            var listingName = item["property"]["headline"].ToSafeString();


                            var listingId = item["property"]["id"].ToSafeString();
                            var reservationId = item["reservationReferenceNumber"].ToSafeString();
                            var stayStartDate = item["stayStartDate"].ToSafeString();
                            var stayEndDate = item["stayEndDate"].ToSafeString();

                            var isInquiryOnly = status == "INQUIRY";
                            threadList.Add(new InboxListing
                            {
                                Id = id,
                                ListingName = listingName,
                                OtherUserName = userName,
                                OtherUserId = userId,
                                LastMessageAt = lastMessageAt,
                                Unread = readStatus,
                                MessageSnippet = messageSnippet,
                                InquiryOnly = isInquiryOnly,
                                InquiryDate = inquiryDateRange,
                                Status = status,
                                ListingId = listingId,
                                ReservationCode = reservationId,
                                StayStartDate = stayStartDate,
                                StayEndDate = stayEndDate
                            });
                        }
                    }
                    else
                    {
                        hasMore = false;
                    }
                }
            } while (hasMore);

            return threadList;
        }

        private int MessageDetailWrapper(List<InboxListing> listings)
        {
            List<Inbox> InboxItems = new List<Inbox>();
            foreach (var item in listings)
            {
                var result = ScrapeMessageDetails(item.Id);

                var thread = Core.API.Inbox.Add(Database, item.OtherUserName, item.ListingName, item.ReservationCode, item.LastMessageAt, item.Unread, item.MessageSnippet, item.OtherUserId, item.Id, item.InquiryOnly, item.InquiryDate, item.Status, item.StayStartDate, item.StayEndDate, item.ListingId);
                if (thread.IsSaved)
                {
                    foreach (var msg in result)
                    {
                        var addResult = Core.API.InboxMessage.Add(Database, thread.Thread, msg.MessageId, msg.Message, false, msg.Sender, msg.ProfileImage, msg.ProfilePath, msg.ProfileId, msg.IsMyMessage, msg.Timestamp.ToDateTime(), msg.Type);
                        if (addResult.IsDuplicate)//all next would be already in db
                            break;
                    }
                }


                //InboxItems.Add(new Inbox
                //{
                //    Name = item.OtherUserName, Messages = result.Item2, ReservationLink = result.Item1 }
                //);
            }

            // var dir = AppDomain.CurrentDomain.BaseDirectory + "inbox.json";
            //string jsonString = JsonConvert.SerializeObject(InboxItems, Newtonsoft.Json.Formatting.Indented);

            //using (StreamWriter outputFile = new StreamWriter(outputPath))
            //{
            //    outputFile.Write(jsonString);
            //}
            return InboxItems.Count;
        }
        public List<InboxMessage> ScrapeMessageDetails(string id)
        {
            List<InboxMessage> messages = new List<InboxMessage>();
            string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&conversationId={0}&_restfully=true", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(CommPage.Html);
                var ar_temp = (JArray)ob["messages"];
                //  var users = (JArray)ob["appData"]["participantsProfiles"];
                //participantsProfiles
                if (ar_temp.Count > 0)
                {
                    var ar = ar_temp.Where(f => f["from"]["role"].ToSafeString() != "SYSTEM").ToList();
                    foreach (var item in ar)
                    {
                        var userId = item["from"]["participantUuid"].ToSafeString();
                        var myMessage = false;

                        var sender = item["from"]["name"].ToSafeString();
                        if (sender == "You")
                        {
                            myMessage = true;
                        }
                        var profilelink = "https://www.vrbo.com/traveler/profiles/" + userId;
                        var profileImagelink = item["from"]["avatarThumbnailUrl"].ToSafeString();
                        //https://www.homeaway.com/bizops/travelerHome/picturePublicPathByAccount?accountUuid=7c89c988-9055-4906-b4d0-1289c5b7a17c&pictureSize=square&site=gd
                        //in the above url if we replace 'square' with 'large' it gets the original image that is larger one
                        if (!string.IsNullOrEmpty(profileImagelink))
                            profileImagelink = profileImagelink.Replace("=square", "=large");
                        var message = item["body"].ToSafeString();
                        var createdAt = item["sentTimestamp"].ToSafeString();
                        var messageId = item["uuid"].ToSafeString();
                        var type = item["title"].ToSafeString();
                        if (!string.IsNullOrEmpty(message))
                        {
                            messages.Add(new InboxMessage { Message = message, Sender = sender, Timestamp = createdAt, ProfilePath = profilelink, ProfileImage = profileImagelink, MessageId = messageId, IsMyMessage = myMessage, ProfileId = userId, Type = type });

                        }
                    }
                }
            }
            return messages;
        }

        #region New Messages Only
        public List<NewMessageViewModel> GetLatestFromInbox(string token)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                var newThreads = ScrapeInboxThreadListing_NewAndUnRead();
                return SaveNewMessagesInDb(newThreads);
            }
            return null;
        }
        private List<InboxListing> ScrapeInboxThreadListing_NewAndUnRead()
        {
            List<InboxListing> threadList = new List<InboxListing>();
            try
            {
                var pageNumber = 1;
                var hasMore = false;
                do
                {
                    string url = string.Format("https://admin.vrbo.com/rm/proxies/conversations/inbox?_restfully=true&page={0}&pageSize=25&locale=en_US&sort=received&sortOrder=desc", pageNumber);



                    hasMore = false;
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var ar = (JArray)ob["conversations"];

                        if (ar.Count > 0)
                        {

                            var currentPage = ob["display"]["page"].ToInt();
                            var pageSize = ob["display"]["pageSize"].ToInt();
                            var totalResults = ob["display"]["totalResults"].ToInt();

                            if ((currentPage * pageSize) < totalResults)
                                hasMore = true;

                            pageNumber++;
                            foreach (var item in ar)
                            {
                                var readStatus = item["unreadMessages"].ToBoolean();


                                var id = item["id"].ToSafeString();
                                var lastMessageAt = item["lastMessageReceivedDate"].ToSafeString();

                                var existingThread = Core.API.Inbox.ThreadForThreadId(Database, id);
                                if (existingThread != null)
                                {
                                    var existingTime = DateTime.Now;
                                    if (!existingThread.LastMessageAt.Contains("AM") && !existingThread.LastMessageAt.Contains("PM"))
                                    {
                                        existingTime = DateTime.ParseExact(existingThread.LastMessageAt, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                                    }
                                    else
                                    {
                                        existingTime = DateTime.ParseExact(existingThread.LastMessageAt, "M/dd/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    //18/09/2017 18:33:53
                                    var currentItemTime = DateTime.Now;
                                    if (!lastMessageAt.Contains("AM") && !lastMessageAt.Contains("PM"))
                                    {
                                        currentItemTime = DateTime.ParseExact(lastMessageAt, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        currentItemTime = DateTime.ParseExact(lastMessageAt, "M/dd/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

                                    }
                                    if (existingTime == currentItemTime)
                                    {
                                        //it means top thread is already latest in db
                                        hasMore = false;
                                        break;
                                    }
                                }
                                var userName = item["correspondent"]["firstName"].ToSafeString();
                                var userId = item["correspondent"]["profileUuid"].ToSafeString();

                                var messageSnippet = item["lastMessage"]["message"].ToSafeString();
                                var inquiryDateRange = item["created"].ToSafeString();
                                var status = item["status"].ToSafeString();
                                var listingName = item["property"]["headline"].ToSafeString();


                                var listingId = item["property"]["id"].ToSafeString();
                                var reservationId = item["reservationReferenceNumber"].ToSafeString();
                                var stayStartDate = item["stayStartDate"].ToSafeString();
                                var stayEndDate = item["stayEndDate"].ToSafeString();

                                var isInquiryOnly = status == "INQUIRY";
                                threadList.Add(new InboxListing
                                {
                                    Id = id,
                                    ListingName = listingName,
                                    OtherUserName = userName,
                                    OtherUserId = userId,
                                    LastMessageAt = lastMessageAt,
                                    Unread = readStatus,
                                    MessageSnippet = messageSnippet,
                                    InquiryOnly = isInquiryOnly,
                                    InquiryDate = inquiryDateRange,
                                    Status = status,
                                    ListingId = listingId,
                                    ReservationCode = reservationId,
                                    StayStartDate = stayStartDate,
                                    StayEndDate = stayEndDate
                                });
                            }
                        }
                        else
                        {
                            hasMore = false;
                        }
                    }
                } while (hasMore);
            }
            catch (Exception e)
            {

            }
            return threadList;
        }
        private List<NewMessageViewModel> SaveNewMessagesInDb(List<InboxListing> threadListings)
        {
            List<NewMessageViewModel> newMessages = new List<NewMessageViewModel>();
            List<Inbox> InboxItems = new List<Inbox>();
            foreach (var item in threadListings)
            {

                var existingThread = Core.API.Inbox.ThreadForThreadId(Database, item.Id);
                if (existingThread != null)
                {
                    var existingTime = DateTime.Now;
                    if (!existingThread.LastMessageAt.Contains("AM") && !existingThread.LastMessageAt.Contains("PM"))
                    {
                        existingTime = DateTime.ParseExact(existingThread.LastMessageAt, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    }
                    else
                    {
                        existingTime = DateTime.ParseExact(existingThread.LastMessageAt, "M/dd/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    //18/09/2017 18:33:53
                    var currentItemTime = DateTime.Now;
                    if (!item.LastMessageAt.Contains("AM") && !item.LastMessageAt.Contains("PM"))
                    {
                        currentItemTime = DateTime.ParseExact(item.LastMessageAt, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        currentItemTime = DateTime.ParseExact(item.LastMessageAt, "M/dd/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);

                    }

                    if (existingTime == currentItemTime)
                    {
                        //it means top thread is already latest in db
                        break;
                    }
                }

                var result = ScrapeMessageDetails(item.Id);

                var thread = Core.API.Inbox.Add(Database, item.OtherUserName, item.ListingName, item.ReservationCode, item.LastMessageAt, item.Unread, item.MessageSnippet, item.OtherUserId, item.Id, item.InquiryOnly, item.InquiryDate, item.Status, item.StayStartDate, item.StayEndDate, item.ListingId);
                if (thread.IsSaved)
                {
                    var newThread = new NewMessageViewModel();
                    newThread.Thread = thread.Thread;
                    newThread.NewMessages = new List<Core.API.InboxMessage>();
                    foreach (var msg in result)
                    {
                        var addResult = Core.API.InboxMessage.Add(Database, thread.Thread, msg.MessageId, msg.Message, false, msg.Sender, msg.ProfileImage, msg.ProfilePath, msg.ProfileId, msg.IsMyMessage, msg.Timestamp.ToDateTime(), msg.Type);
                        if (addResult.IsDuplicate)//all next would be already in db
                            break;
                        else
                            newThread.NewMessages.Add(addResult.Message);
                    }
                    newMessages.Add(newThread);
                }



            }


            return newMessages;
        }
        #endregion
        #endregion
        #region MyListings
        public ScrapeResult ScrapeMyListings(string token)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = "https://admin.vrbo.com/haod/properties.html";

                //commPage.RequestURL = url;
                //commPage.PersistCookies = true;
                //commPage.ReqType = CommunicationPage.RequestType.GET;
                //commPage.RequestHeaders = new Dictionary<string, string>();
                //ProcessRequest(commPage);
                //if (!commPage.IsValid)
                //    return sresult;
                //var t = commPage.Uri;
                ////TODO test with multiple properties
                ////https://admin.vrbo.com/gd/proxies/properties/legacySummaries/?_restfully=true&locale=en_US&listingTriad=321.827199.1375137  
                //url = string.Format("https://admin.vrbo.com/gd/proxies/properties/singleLegacySummary/?_restfully=true&locale=en_US&site=vrbo");
                try
                {

                    int count = 1;
                    List<ListingDetail> propertiesList = new List<ListingDetail>();

                    count++;
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    //commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var rows = xd.SelectNodes("//td[@class='listing-summary']");


                            foreach (XmlNode item in rows)
                            {
                                var result = GetPublicUuidAndUserId(commPage.Html);

                                var id = item.GetAttributeFromNode("data-listingguid");

                                var property = MyProperty(id, result.Item1, result.Item2);

                                if (property != null)
                                {
                                    var prop = Core.API.Vrbo.Properties.Add(property.Id, property.ListingUrl, property.Title, property.Longitude, property.Latitude, Convert.ToInt32(property.MinStay), property.Internet, property.Pets,
                                          property.WheelChair, property.Description, Convert.ToInt32(property.Reviews), property.Sleeps, property.Bedrooms, property.Bathrooms, property.PropertyType, property.AccomodationType, property.Smoking,
                                          property.AirCondition, property.SwimmingPool, property.Status);
                                    propertiesList.Add(property);

                                }

                                else
                                {

                                }



                            }
                        }






                    }

                    var totalItems = propertiesList.Count();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";

                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Invalid Token, Please check you details and try again.";
            }

            return sresult;
        }

        public ListingDetail MyProperty(string id, string ownerPublicuuid, string ownerUserid)
        {
            ListingDetail listing = new ListingDetail();
            listing.OwnerPublicuuid = ownerPublicuuid;
            listing.OwnerGuid = ownerUserid;

            var propertyId = ScrapeHelper.SplitSafely(id, ".", 1);
            listing.Id = propertyId.ToLong();

            MyProperty_Location(id, listing);
            MyProperty_Description(id, listing);
            MyProperty_Status(id, listing);
            MyProperty_Photos(id, listing);
            MyProperty_Amenities(id, listing);
            MyProperty_Settings(id, listing);
            MyProperty_Rates(id, listing);

            return listing;

        }
        public void MyProperty_Location(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/lm/{0}/location.html", id);
                listing.ListingUrl = url;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Longitude = xd.SelectSingleNode("//input[@id='locationAddressLng']").GetAttributeFromNode("value");
                        listing.Latitude = xd.SelectSingleNode("//input[@id='locationAddressLat']").GetAttributeFromNode("value");







                    }

                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Status(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/lm/{0}/experience/panel.json", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Status = ob["listingExperienceState"].ToSafeString();

                }
            }
            catch (Exception e)
            {

            }

        }

        public void MyProperty_Description(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/lm/{0}/description.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Title = xd.SelectSingleNode("//input[@id='headlineField_en_tmp']").GetAttributeFromNode("value");
                        listing.Description = xd.SelectSingleNode("//textarea[@id='descriptionField_en_tmp']").GetInnerTextFromNode();
                        listing.HostImageUrl = xd.SelectSingleNode("//img[@id='owner-photo']").GetAttributeFromNode("src");
                        listing.HostAbout = xd.SelectSingleNode("//textarea[@id='ownerListingStoryField_en_tmp']").GetInnerTextFromNode();





                    }

                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Photos(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/lm/{0}/photos.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        List<string> imgsList = new List<string>();
                        var imgNodes = xd.SelectNodes("//ul[@id='ulPhoto']//img");
                        foreach (XmlNode imgNode in imgNodes)
                        {
                            var imgSrc = imgNode.GetAttributeFromNode("src").Replace(".s8", ".s10").Replace(".c8", ".c10");
                            if (!string.IsNullOrEmpty(imgSrc))
                                imgsList.Add(imgSrc);


                        }
                        listing.Images = string.Join("|", imgsList);




                    }

                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Amenities(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/lm/{0}/amenities.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.PropertyType = xd.SelectSingleNode("//select[@id='propertyTypeDropdown']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
                        listing.Bedrooms = xd.SelectNodes("//div[contains(@id,'bedroom-')]").Count;
                        listing.Bathrooms = xd.SelectNodes("//div[contains(@id,'bathroom-')]").Count;
                        var wheelChairNode = xd.SelectSingleNode("//select[@id='suitabilityFeaturesField_121_dropDown']//option[@selected='selected']");
                        if (wheelChairNode != null)
                            listing.WheelChair = wheelChairNode.NextSibling.GetInnerTextFromNode();
                        else
                            listing.WheelChair = "ask owner";
                        listing.Internet = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_135'][@checked='checked']") == null ? "No" : "Yes";
                        listing.AirCondition = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_45'][@checked='checked']") == null ? "No" : "Yes";


                        var acomTypeVacational = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesFieldDefault'][@checked='checked']");
                        var acomTypeBed = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesField2'][@checked='checked']");
                        if (acomTypeVacational == null && acomTypeBed == null)
                            listing.AccomodationType = "Vacation Rental";
                        else
                        if (acomTypeVacational != null)
                            listing.AccomodationType = "Vacation Rental";

                        else
                            listing.AccomodationType = "Bed & Breakfast";

                        listing.SwimmingPool = xd.SelectNodes("//input[contains(@id,'poolAndSpaFeaturesField_fv_')][@checked='checked']").Count == 0 ? "No" : "Yes";

                    }

                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Settings(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/gd/proxies/lodgingPolicy/get?listingId={0}&isOnboarding=false&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    //listing.CheckInTime = ob["lodgingPolicy"]["checkInTime"].ToSafeString();
                    //listing.CheckOutTime = ob["lodgingPolicy"]["checkOutTime"].ToSafeString();

                    listing.Sleeps = ob["lodgingPolicy"]["maximumOccupancyRule"]["guests"].ToSafeString();
                    listing.Pets = ob["lodgingPolicy"]["petsAllowedRule"]["allowed"].ToSafeString();
                    listing.Smoking = ob["lodgingPolicy"]["smokingAllowedRule"]["allowed"].ToSafeString();

                }
            }
            catch (Exception e)
            {

            }

        }
        public void MyProperty_Rates(string id, ListingDetail listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {


                    var html = commPage.Html;
                    var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                    var to = html.IndexOf(";", start);
                    String result = html.Substring(start, to - start);
                    result = result.Trim().TrimEnd(';');
                    var ob = JObject.Parse(result);

                    listing.PriceNightlyMin = ob["basicRate"]["amount"].ToSafeString();
                    listing.MinStay = ob["basicRate"]["minimumStay"].ToSafeString();

                }
            }
            catch (Exception e)
            {

            }

        }


        #region Calendar Rates And Availability

        public bool ScrapeRatesAndAvailability(List<string> listingIds)
        {

            foreach (var item in listingIds)
            {
                RatesAndAvailability(item);
            }
            return true;
        }
        private bool RatesAndAvailability(string listingId)
        {
            try
            {
                commPage.RequestURL = "https://www.vrbo.com/" + listingId;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {



                        var html = commPage.Html;
                        var start = html.IndexOf("window.__PAGE_DATA__ =") + "window.__PAGE_DATA__ =".Length;
                        var to = html.IndexOf("window.__APP_CONFIG__ =");
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');
                        var ob = JObject.Parse(result);

                        var listingNode = ob["listing"];

                        Dictionary<DateTime, Avail_Price> avl_Calendar = new Dictionary<DateTime, Avail_Price>();

                        var avl_startDate = listingNode["availabilityCalendar"]["availability"]["dateRange"]["beginDate"].ToDateTime();
                        var avl_endDate = listingNode["availabilityCalendar"]["availability"]["dateRange"]["endDate"].ToDateTime();
                        var availability = listingNode["availabilityCalendar"]["availability"]["unitAvailabilityConfiguration"]["availability"].ToSafeString();
                        List<string> availabilityList = availability.Select(x => x.ToSafeString()).ToList();
                        int k = 0;
                        for (DateTime i = avl_startDate; i <= avl_endDate; i = i.AddDays(1))
                        {
                            avl_Calendar.Add(i, new Avail_Price { Available = availabilityList[k].ToLower().Trim() == "y" });
                            k++;


                        }




                        var rate_startDate = listingNode["rateSummary"]["beginDate"].ToDateTime();
                        var rate_endDate = listingNode["rateSummary"]["endDate"].ToDateTime();
                        var rates = listingNode["rateSummary"]["rentNights"].ToList();
                        int l = 0;
                        for (DateTime i = rate_startDate; i <= rate_endDate; i = i.AddDays(1))
                        {
                            if (avl_Calendar.ContainsKey(i))
                            {
                                avl_Calendar[i].Price = rates[l].ToSafeString();
                            }
                            else
                            {
                                avl_Calendar.Add(i, new Avail_Price { Price = rates[l].ToSafeString() });
                            }

                            l++;
                        }


                        avl_Calendar = avl_Calendar.OrderBy(f => f.Key).ToDictionary(p => p.Key, p => p.Value);

                        foreach (var item in avl_Calendar)
                        {
                            //Core.API.Vrbo.rates.ratesRatesAndAvailability.Add(Database, listingId, item.Key, item.Value.Available, item.Value.Price);

                        }



                    }

                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public class Avail_Price
        {
            public bool Available { get; set; }
            public string Price { get; set; }
        }
        #endregion
        public ListingDetail AirBnBScrapeProperties(string url, string status)
        {
            try
            {

                // url = "https://www.vrbo.com/146160";
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        string longitude = "";
                        string latitude = "";


                        var html = commPage.Html;
                        var start = html.IndexOf("window.__PAGE_DATA__ =") + "window.__PAGE_DATA__ =".Length;
                        var to = html.IndexOf("window.__APP_CONFIG__ =");
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');
                        var ob = JObject.Parse(result);

                        var listingNode = ob["listing"];
                        latitude = listingNode["geoCode"]["latitude"].ToSafeString();
                        longitude = listingNode["geoCode"]["longitude"].ToSafeString();




                        #region Host Data
                        var hostName = listingNode["contact"]["name"].ToSafeString(); ;
                        var hostId = "";
                        var hostPicPath = listingNode["contact"]["ownerProfilePhoto"].ToSafeString();

                        var hostAbout = "";
                        if (listingNode["ownersListingProfile"].HasValues)
                            hostAbout = listingNode["ownersListingProfile"]["aboutYou"].ToSafeString();

                        var hostLocation = "";
                        var hostMemberSince = listingNode["contact"]["memberSince"].ToSafeString();
                        var hostReviews = "";
                        var rating = listingNode["scaledAverageRating"].ToSafeString();

                        #endregion
                        var description = listingNode["description"].ToSafeString();


                        var pets = "Not allowed";
                        var petsbool = listingNode["petsAllowed"].ToBoolean();
                        if (petsbool == true)
                            pets = "Allowed";

                        var airCondition = "No";
                        var internet = "No";
                        var swimmingPool = "No";
                        var smoking = "Not allowed";
                        var wheelchair = "Not allowed";

                        var amenitiesNode = (JArray)listingNode["amenities"];

                        var generalAmenities = amenitiesNode.Where(f => f["title"].ToSafeString() == "General").FirstOrDefault();
                        if (generalAmenities != null)
                        {
                            var gnrlAttr = (JArray)generalAmenities["attributes"];
                            if (gnrlAttr.Any(f => f.ToSafeString().Contains("Air Condition")))
                            {
                                airCondition = "Yes";
                            }
                            if (gnrlAttr.Any(f => f.ToSafeString().Contains("Internet")))
                            {
                                internet = "Yes";
                            }
                        }

                        var poolAmenities = amenitiesNode.Where(f => f["title"].ToSafeString().Contains("Pool")).FirstOrDefault();
                        if (poolAmenities != null)
                        {
                            var poolAttr = (JArray)poolAmenities["attributes"];
                            if (poolAttr.Any(f => f.ToSafeString().Contains("Pool")))
                            {
                                swimmingPool = "Yes";
                            }

                        }

                        var suitAmenities = amenitiesNode.Where(f => f["title"].ToSafeString() == "Suitability").FirstOrDefault();
                        if (suitAmenities != null)
                        {
                            var suitAttr = (JArray)suitAmenities["attributes"];

                            var smokingNode = suitAttr.Where(f => f.ToSafeString().Contains("smoking")).FirstOrDefault();
                            if (smokingNode != null)
                            {
                                smoking = smokingNode.ToSafeString();
                            }


                            var wheelNode = suitAttr.Where(f => f.ToSafeString().Contains("wheelchair")).FirstOrDefault();
                            if (wheelNode != null)
                            {
                                wheelchair = wheelNode.ToSafeString();
                            }

                        }




                        var minStay = listingNode["minStayRange"].ToSafeString();
                        var listingId = listingNode["propertyId"].ToSafeString();
                        var title = listingNode["headline"].ToSafeString();
                        var nightlyMin = ob["advertisedAveragePrice"].ToSafeString();


                        var bathrooms = listingNode["bathrooms"]["full"].ToSafeString();
                        var bedrooms = listingNode["bedrooms"].ToSafeString();
                        var sleeps = listingNode["sleeps"].ToSafeString();
                        var propertyType = listingNode["propertyType"].ToSafeString();
                        var accomodationType = "";



                        var reviews = listingNode["reviewCount"].ToSafeString();
                        DateTime lastReviewAt = new DateTime();
                        if (listingNode["reviews"].HasValues)
                        {


                            var arrReviews = (JArray)listingNode["reviews"];
                            if (arrReviews != null)
                            {
                                var rawDate = arrReviews[0]["createdAt"].ToSafeString();
                                lastReviewAt = DateTime.ParseExact(rawDate,
                                      "yyyy-MM-dd'T'HH:mm:ss:fff'Z'",
                                      System.Globalization.CultureInfo.InvariantCulture,
                                      System.Globalization.DateTimeStyles.AssumeUniversal |
                                      System.Globalization.DateTimeStyles.AdjustToUniversal);
                                // lastReviewAt =Convert.ToDateTime(arrReviews[0]["createdAt"].ToSafeString());
                            }
                        }
                        string images = "";
                        try
                        {
                            List<string> imgsList = new List<string>();


                            var arr = (JArray)listingNode["images"];
                            foreach (var item in arr)
                            {
                                var imgs = (JArray)item["imageFiles"];
                                var largeImg = imgs.LastOrDefault();
                                var imgUrl = largeImg["uri"].ToSafeString();
                                if (!string.IsNullOrEmpty(imgUrl))
                                    imgsList.Add(imgUrl);
                            }
                            images = string.Join("|", imgsList);

                        }
                        catch (Exception e)
                        {

                        }

                        //var hostName = topNode["listing"]["user"]["host_name"].ToSafeString();
                        //var hostId = topNode["listing"]["user"]["id"].ToSafeString();
                        //var hostProfilePic = topNode["listing"]["user"]["profile_pic_path"].ToSafeString();
                        var ownerPublicuuid = "";
                        var ownerGuid = "";
                        try
                        {
                            var strt = commPage.Html.IndexOf("var analyticsdatalayer =") + "var analyticsdatalayer =".Length;
                            var end = commPage.Html.IndexOf(";", strt);
                            var substring = commPage.Html.Substring(strt, end - strt);


                            ob = JObject.Parse(substring);
                            ownerPublicuuid = ob["publicuuid"].ToSafeString();
                            ownerGuid = ob["guid"].ToSafeString();
                        }
                        catch { }




                        return new ListingDetail()
                        {
                            Id = listingId.ToLong(),
                            AccomodationType = accomodationType,
                            Bathrooms = bathrooms.ToInt(),
                            Bedrooms = bedrooms.ToInt(),
                            Description = description.HtmlDecode(),
                            Images = images,
                            Internet = internet,
                            ListingUrl = url,
                            MinStay = minStay,
                            Pets = pets,
                            PriceNightlyMax = "",
                            PriceNightlyMin = nightlyMin.ToSafeString(),
                            PriceWeeklyMax = "",
                            PriceWeeklyMin = "",
                            PropertyType = propertyType,
                            Reviews = reviews,
                            Sleeps = sleeps,
                            Smoking = smoking,
                            Title = title.HtmlDecode(),
                            WheelChair = wheelchair,
                            Longitude = longitude,
                            Latitude = latitude,
                            AirCondition = airCondition,
                            SwimmingPool = swimmingPool,
                            Status = status,

                            HostName = hostName,
                            HostAbout = hostAbout,
                            HostAddress = hostLocation,
                            HostId = hostId,
                            HostImageUrl = hostPicPath,
                            HostMemberSince = hostMemberSince,
                            HostReviewsCount = hostReviews,
                            LastReviewAt = lastReviewAt,
                            OwnerGuid = ownerGuid,
                            OwnerPublicuuid = ownerPublicuuid


                        };
                    }

                }
            }
            catch (Exception e)
            {

            }
            return null;
        }

        public string GetPrice(string propertyId, string checkin = "", string checkout = "")
        {
            try
            {

                string url = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?guests=1&listing_id={0}&_format=for_dateless_booking_info_on_web_p3&_interaction_type=pageload&_intents=p3_book_it&_parent_request_uuid=5e7baaa5-bf39-4ee6-8319-37f930b2ab5e&_p3_impression_id=p3_1490352774_Ymuf7NI5Bk956LLB&show_smart_promotion=0&number_of_adults=1&number_of_children=0&number_of_infants=0&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", propertyId);
                if (!string.IsNullOrEmpty(checkout) && !string.IsNullOrEmpty(checkin))
                {
                    url = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?guests=1&listing_id={0}&_format=for_detailed_booking_info_on_web_p3_with_message_data&_interaction_type=pageload&_intents=p3_book_it&_parent_request_uuid=07a8f796-adcb-4863-a0ca-5ae04ef06d0e&_p3_impression_id=p3_1490559375_zW9jEkRLeySfE5eH&show_smart_promotion=0&check_in={1}&check_out={2}&number_of_adults=1&number_of_children=0&number_of_infants=0&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", propertyId, checkin, checkout);

                }
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var price = ob["pricing_quotes"][0]["rate"]["amount"].ToSafeString();
                    var currency = ob["pricing_quotes"][0]["rate"]["currency"].ToSafeString();
                    return price + " " + currency;
                }
            }
            catch (Exception e)
            {

            }
            return string.Empty;
        }

        #endregion
        #region Send Message To Inbox
        public ScrapeResult SendMessageToInbox(string token, string threadId, string messageText)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {



                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/conversations/message?conversationId={0}&_restfully=true", threadId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"attachments\":[],\"message\":\"{0}\",\"typeKey\":\"REPLIED\"}}", messageText);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["typeKey"].ToSafeString();
                        if (status == "REPLIED")
                        {
                            sresult.Success = true;

                        }
                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }
        public ScrapeResult SendInquiry(string token, string listingId, string messageText, bool nextAvailable, string checkinDate, string checkoutDate, int noOfGuests)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/rooms/" + listingId;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        if (nextAvailable)
                        {
                            var listingDetail = AirBnBScrapeProperties("https://www.airbnb.com/rooms/" + listingId, "active");
                            if (listingDetail != null)
                            {
                                var minNights = listingDetail.MinStay.ToSafeString().ToInt();
                                if (minNights > 0)
                                {
                                    var availability = ScrapeCalendar(listingId, 12);
                                    if (availability != null)
                                    {
                                        var range = availability.Where(f => f.IsAvailable == true).OrderBy(f => f.Date).Take(minNights + 1).ToList();
                                        checkinDate = range.FirstOrDefault().Date.ToString("yyyy-MM-dd");
                                        checkoutDate = range.LastOrDefault().Date.ToString("yyyy-MM-dd");
                                    }
                                }
                            }
                        }

                        commPage.RequestURL = string.Format("https://www.airbnb.com/users/ask_question/{3}?checkin={0}&checkout={1}&guests={2}", checkinDate, checkoutDate, noOfGuests, listingId);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>() { { "message", messageText } };
                        commPage.Parameters.Add("authenticity_token", auth_token);
                        commPage.Parameters.Add("message_checkin", checkinDate);
                        commPage.Parameters.Add("message_checkout", checkoutDate);
                        commPage.Parameters.Add("question", messageText);
                        commPage.Parameters.Add("message_number_of_guests", noOfGuests.ToSafeString());
                        commPage.Parameters.Add("number_of_adults", noOfGuests.ToSafeString());
                        commPage.Parameters.Add("number_of_children", "0");
                        commPage.Parameters.Add("number_of_infants", "0");


                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.Referer = "https://www.airbnb.com/rooms/" + listingId;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var message = ob["message"].ToSafeString();

                            sresult.Message = message;
                            if (string.IsNullOrEmpty(message))
                            {
                                sresult.Success = true;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }

        #endregion
        #region Inquiry Metadata
        public ListingMetadata GetMetadataForInquiry(string listingId)
        {
            var listingMetadata = new ListingMetadata();

            try
            {
                if (commPage == null)
                {
                    commPage = new CommunicationPage("");

                }
                listingMetadata = GetListingMetadata(listingId);
                if (listingMetadata != null)
                {
                    var minNights = listingMetadata.MinStay.ToSafeString().ToInt();
                    if (minNights > 0)
                    {
                        var availability = ScrapeCalendar(listingId, 12);
                        if (availability != null)
                        {
                            var range = availability.Where(f => f.IsAvailable == true).OrderBy(f => f.Date).Take(minNights + 1).ToList();
                            listingMetadata.NextAvailableCheckinDate = range.FirstOrDefault().Date.ToString("yyyy-MM-dd");
                            listingMetadata.NextAvailableCheckoutDate = range.LastOrDefault().Date.ToString("yyyy-MM-dd");
                        }
                    }
                }




            }
            catch (Exception e)
            {
            }
            return listingMetadata;
        }


        #endregion
        #region Message Thread Metadata
        public ThreadMetadata MessageThreadMetadata(string token, string id)
        {
            ThreadMetadata metadata = new ThreadMetadata();
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                var reservationLink = "";
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);



                    var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                    var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
                    var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
                    var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

                    var fees = ((JArray)ob["quote"]["fees"]).ToList();


                    var rentalCost = "";
                    var serviceFee = "";
                    var cleaningFee = "";
                    var refundableDamageDeposit = "";
                    var taxRate = "";

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                    {
                        rentalCost = rentalNode["amount"]["amount"].ToSafeString();
                        if (rentalNode["feeBasis"]["tax"] != null)
                        {
                            taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

                        }
                    }

                    var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
                    if (cleaningNode != null)
                        cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

                    var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
                    if (serviceNode != null)
                        serviceFee = serviceNode["amount"]["amount"].ToSafeString();

                    var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
                    if (ddNode != null)
                        refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();



                    metadata.ServiceFee = serviceFee;
                    metadata.CleaningFee = cleaningFee;
                    metadata.TotalPayout = payableToHost;
                    metadata.ReservationCost = quoteTotal;
                    metadata.RefundableDamageDeposit = refundableDamageDeposit;
                    metadata.RentalCost = rentalCost;
                    metadata.TaxRate = taxRate;
                    metadata.TotalTax = taxAmount;


                    var conversation = ob["conversation"];
                    if (conversation != null)
                    {
                        metadata.CheckinDate = conversation["checkinDate"].ToSafeString();
                        metadata.CheckoutDate = conversation["checkoutDate"].ToSafeString();


                        metadata.GuestId = conversation["otherParticipants"][0]["profileUuid"].ToSafeString();
                        metadata.GuestName = conversation["otherParticipants"][0]["firstName"].ToSafeString() + " " + conversation["otherParticipants"][0]["lastName"].ToSafeString();
                        metadata.GuestEmail = conversation["otherParticipants"][0]["emailAddress"].ToSafeString();
                        metadata.GuestPhone = conversation["otherParticipants"][0]["phone"].ToSafeString();


                        metadata.HostId = conversation["primaryParticipant"]["profileUuid"].ToSafeString();
                        metadata.HostName = conversation["primaryParticipant"]["firstName"].ToSafeString() + " " + conversation["primaryParticipant"]["lastName"].ToSafeString();
                        metadata.HostEmail = conversation["primaryParticipant"]["emailAddress"].ToSafeString();
                        metadata.HostPhone = conversation["primaryParticipant"]["phone"].ToSafeString();



                        //  metadata.ConfirmationCode = reservationInfo["confirmation_code"].ToSafeString();
                        metadata.NoOfGuests = ob["overview"]["guestCount"].ToSafeString();
                        metadata.ListingName = ob["property"]["unit"]["headline"].ToSafeString();
                        metadata.ListingAddress = ob["property"]["unit"]["address"]["address1"].ToSafeString();
                        metadata.ListingAddress += ", " + ob["property"]["unit"]["address"]["address3"].ToSafeString();
                        metadata.ListingAddress += ", " + ob["property"]["unit"]["address"]["addressLine4"].ToSafeString();

                        //TODO
                        // metadata.NextAvailableCheckinDate;
                        // metadata.NextAvailableCheckoutDate


                        var reservationNode = ob["reservation"];
                        var inquiryNode = ob["inquiry"];
                        if (reservationNode != null)
                        {
                            metadata.CheckinTime = reservationNode["checkinTime"].ToSafeString();
                            metadata.CheckoutTime = reservationNode["checkoutTime"].ToSafeString();

                            metadata.GuestAddress = reservationNode["guest"]["addressLine1"].ToSafeString();
                            metadata.GuestCity = reservationNode["guest"]["city"].ToSafeString();
                            metadata.GuestCountry = reservationNode["guest"]["country"].ToSafeString();

                            metadata.Status = reservationNode["status"].ToSafeString();
                        }
                        else if (inquiryNode != null)
                        {
                            metadata.Status = "Inquiry";
                        }

                    }






                }
            }
            return metadata;
        }
        #endregion
        #region Calendar Set Price and Availability
        public ScrapeResult UpdateAvailability(string token, string listingId, bool available, string dateFrom, string dateTo, string price)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://www.airbnb.com/manage-listing/{0}/calendar", listingId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        // var xd = commPage.ToXml();
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();



                        commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/calendars/{0}/{1}/{2}?_format=host_calendar_detailed&_price_tips=false&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", listingId, dateFrom, dateTo);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                        CommPage.JsonStringToPost = string.Format("{{\"availability\":\"{1}\",\"daily_price\":{0},\"notes\":\"\",\"demand_based_pricing_overridden\":false}}", price, available ? "available" : "unavailable");
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        commPage.Referer = string.Format("https://www.airbnb.com/manage-listing/{0}/calendar", listingId);
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            if (ob["calendar"] != null)
                            {
                                sresult.Message = "Success!";
                                sresult.Success = true;

                            }
                            else
                            {
                                sresult.Message = "An error occurred.";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }

        #endregion
        private string Helper_ListingToTriad(string listingId)
        {
            if (!string.IsNullOrEmpty(listingId) && !listingId.Contains("."))
                return string.Format("321.{0}.1375137", listingId);

            return listingId;
        }
        #region Calendar Prices And Availability
        public List<Calendar> ScrapeCalendar(string listingId, int noOfMonths)
        {
            var calendarList = new List<Calendar>();
            string listingUrl = "https://www.airbnb.com/rooms/" + listingId;
            var property = AirBnBScrapeProperties(listingUrl, "active");
            var prop = new Core.API.Vrbo.AddPropertyResult();
            if (property != null)
            {
                prop = Core.API.Vrbo.Properties.Add(property.Id, property.ListingUrl, property.Title, property.Longitude, property.Latitude, Convert.ToInt32(property.MinStay), property.Internet, property.Pets,
                     property.WheelChair, property.Description, Convert.ToInt32(property.Reviews), property.Sleeps, property.Bedrooms, property.Bathrooms, property.PropertyType, property.AccomodationType, property.Smoking,
                     property.AirCondition, property.SwimmingPool, property.Status);
            }
            string url = string.Format("https://www.airbnb.com/api/v2/calendar_months?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en&listing_id={0}&month={1}&year=2017&count={2}&_format=with_conditions", listingId, DateTime.Now.Month, noOfMonths);
            try
            {
                List<ListingDetail> propertiesList = new List<ListingDetail>();





                if (commPage == null)
                    commPage = new CommunicationPage(url);

                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {

                    var ob = JObject.Parse(commPage.Html);
                    var months = (JArray)ob["calendar_months"];
                    if (months != null)
                    {

                        foreach (var month in months)
                        {
                            var days = (JArray)month["days"];
                            foreach (var day in days)
                            {
                                var dt = day["date"].ToSafeString().ToDateTime();
                                if (dt.Date < DateTime.Now.Date)
                                {
                                    continue;
                                }
                                var isAvlbl = day["available"].ToSafeString().ToBoolean();
                                var price = day["price"]["local_price"].ToSafeString() + " CAD";
                                var cal = new Calendar
                                {
                                    Date = dt,
                                    IsAvailable = isAvlbl,
                                    Price = price
                                };
                                calendarList.Add(cal);



                                //var r = Core.API.Calendar.Add(Database, prop.Property, dt, isAvlbl, price);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }

            return calendarList;
        }

        public List<Calendar> ScrapeCalendar_Reservations(string token, string listingId, DateTime from, DateTime to)
        {
            //NOTE
            //availabilityStatus=UNAVAILABLE >> Blocked
            //availabilityStatus=RESERVE     >> Reservation
            //type=ICALENDAR_EVENT           >> Airbnb Booking
            var calendarList = new List<Calendar>();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                var listingTriad = Helper_ListingToTriad(listingId);
                var druid = Helper_GetDruidFromListingId(listingTriad);


                string url = string.Format("https://admin.vrbo.com/gd/proxies/calendar/reservations?druidUuid={0}&startDate={1}&endDate={2}&site=vrbo&locale=en_US&_restfully=true", druid, from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));
                try
                {
                    List<ListingDetail> propertiesList = new List<ListingDetail>();

                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {

                        var arr = JArray.Parse(commPage.Html);
                        if (arr != null)
                        {

                            foreach (var resevation in arr)
                            {

                                var availabilityStatus = resevation["availabilityStatus"].ToSafeString();
                                var content = resevation["content"].ToSafeString();
                                var convId = resevation["conversationId"].ToSafeString();
                                var resId = resevation["resId"].ToSafeString();
                                var startDate = resevation["startDate"].ToSafeString();
                                var endDate = resevation["endDate"].ToSafeString();
                                var type = resevation["type"].ToSafeString();
                                var iCalId = resevation["icalId"].ToSafeString();

                               // var result = Core.API.Calendar_Reservation.Add(Database, listingId, listingTriad, druid, iCalId, startDate, endDate, content, resId, convId, availabilityStatus, type);






                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
            return calendarList;
        }

        public bool BlockDates(string token, DateTime startDate, DateTime endDate, string listingId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var druid = Helper_GetDruidFromListingId(listingTriad);

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/reservations/create?locale=en_US&site=vrbo&_restfully=true");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    CommPage.JsonStringToPost = string.Format("{{\"numAdults\":\"0\",\"numChildren\":\"0\",\"bookingChannel\":{{}},\"checkinDate\":\"{0}\",\"checkinTime\":\"16:00\",\"checkoutDate\":\"{1}\",\"checkoutTime\":\"11:00\",\"comments\":\"\",\"expirationDateTime\":\"\",\"guest\":{{\"firstName\":\"\",\"lastName\":\"\",\"emailAddress\":\"\",\"homePhone\":\"\",\"mobilePhone\":\"\",\"faxNumber\":\"\",\"addressLine1\":\"\",\"addressLine2\":\"\",\"city\":\"\",\"state\":\"\",\"country\":\"\",\"postalCode\":\"\",\"hideContactInfo\":false}},\"reservationStatus\":\"BUILDING\",\"status\":\"UNAVAILABLE\",\"source\":\"DASH\",\"druidId\":\"{2}\",\"listingId\":\"{3}\"}}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), druid, listingTriad);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["source"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            //Refresh db
                            ScrapeCalendar_Reservations(token, listingTriad, startDate.AddDays(-1), endDate.AddDays(2));
                            return true;

                        }
                    }

                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
        public bool UnBlockDates(string token, string reservationId)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/reservations/cancel?reservationId={0}&_restfully=true", reservationId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.DELETE;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["source"].ToSafeString();
                        if (!string.IsNullOrEmpty(status))
                        {
                            //Mark as deleted in db
                           // Core.API.Calendar_Reservation.MarkAsDeleted(Database, reservationId);
                            return true;

                        }
                    }

                }
                catch (Exception e)
                {

                }
            }
            return false;
        }




        public bool Calendar_Rates(string token, string listingId)
        {
            var resultBool = false;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;

                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {


                        var html = commPage.Html;
                        var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                        var to = html.IndexOf(";", start);
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');
                        var ob = JObject.Parse(result);

                        var seasonRates = ob["seasonRates"].ToList();
                        if (seasonRates.Count > 0)
                        {
                            //Core.API.Vrbo.Calendar_Rate.DeleteAllExising(Database);

                            foreach (var item in seasonRates)
                            {
                                var name = item["name"].ToSafeString();
                                var startDate = item["range"]["start"]["asDate"].ToSafeString();
                                var endDate = item["range"]["end"]["asDate"].ToSafeString();

                                var amounts = item["nightlyAmounts"].ToList();

                                var nightlyAmount = "";
                                var nightlyMinStay = "";
                                var nightlyNode = amounts.Where(f => f["periodType"].ToSafeString() == "NIGHTLY").FirstOrDefault();
                                if (nightlyNode != null)
                                {
                                    nightlyMinStay = nightlyNode["minStay"].ToSafeString();
                                    nightlyAmount = nightlyNode["amount"].ToSafeString();
                                }

                                var isExpired = item["expired"].ToSafeString();
                                var type = item["rateType"].ToSafeString();

                                //Core.API.Calendar_Rate.Add(Database, listingId, listingTriad, name, startDate.ToDateTime(), endDate.ToDateTime(), isExpired.ToBoolean(), type, nightlyAmount, nightlyMinStay);
                                resultBool = true;
                            }
                        }


                    }
                }
                catch (Exception e)
                {

                }
            }
            return resultBool;
        }
        public bool Calendar_SetRates(string token, string listingId, string name, DateTime startDate, DateTime endDate, string minStay, string nightlyPrice)
        {
            var resultBool = false;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var listingTriad = Helper_ListingToTriad(listingId);
                    var url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;

                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {


                        var html = commPage.Html;
                        var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                        var to = html.IndexOf(";", start);
                        String result = html.Substring(start, to - start);
                        result = result.Trim().TrimEnd(';');
                        var ob = JObject.Parse(result);


                        var newRateJson = string.Format("{{\"name\":\"{0}\",\"range\":{{\"start\":{{\"asCalendar\":\"{1}\",\"day\":{2},\"month\":{3},\"year\":{4}}},\"end\":{{\"asCalendar\":\"{5}\",\"day\":{6},\"month\":{7},\"year\":{8}}}}},\"minStay\":\"{9}\",\"nightlyAmount\":{10},\"weeklyAmount\":\"\",\"monthlyAmount\":\"\",\"changeoverDay\":\"\",\"chargeWeekends\":false,\"chargeAdditionalGuests\":false,\"additionalGuestRate\":\"\",\"nightlyAmounts\":[{{\"amount\":{10},\"minStay\":\"{9}\",\"payType\":\"PROPERTY\",\"periodType\":\"NIGHTLY\"}}],\"rateType\":\"SEASON\"}}",
                            name, startDate.ToString("MM/dd/yyyy"), startDate.Day, startDate.Month, startDate.Year, endDate.ToString("MM/dd/yyyy"), endDate.Day, endDate.Month, endDate.Year, minStay, nightlyPrice);
                        var newJObject = JToken.Parse(newRateJson);

                        var newCsrfToken = Helper_GetCSRFToken();

                        ((JArray)ob["seasonRates"]).Add(newJObject);

                        var newJson = JsonConvert.SerializeObject(ob);
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/haolb/{0}/le/save.html", listingTriad);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("requestJSON", newJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        CommPage.RequestHeaders.Add("X-Csrf-Token-Requested", newCsrfToken);
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        commPage.RequestHeaders.Add("Ajax", "true");
                        commPage.Referer = url;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var resultOb = JObject.Parse(commPage.Html);
                            if (resultOb["status"].ToSafeString() == "0")
                                return true;




                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return resultBool;
        }
        #endregion

        private void Helper_CookiesTester()
        {

            // Hashtable table = (Hashtable)commPage.COOKIES
            //.GetType().InvokeMember("m_domainTable",
            //BindingFlags.NonPublic |
            //BindingFlags.GetField |
            //BindingFlags.Instance,
            //null,
            //commPage.COOKIES,
            //new object[] { });


            // foreach (var key in table.Keys)
            // {
            //     if (key.ToSafeString().StartsWith("."))
            //         continue;
            //     // Look for http cookies.
            //     if (commPage.COOKIES.GetCookies(
            //         new Uri(string.Format("http://{0}/", key))).Count > 0)
            //     {
            //         Console.WriteLine(commPage.COOKIES.Count + " HTTP COOKIES FOUND:");
            //         Console.WriteLine("----------------------------------");
            //         foreach (Cookie cookie in commPage.COOKIES.GetCookies(
            //             new Uri(string.Format("http://{0}/", key))))
            //         {
            //             Console.WriteLine(
            //                 "Name = {0} ; Value = {1} ; Domain = {2}",
            //                 cookie.Name, cookie.Value, cookie.Domain);
            //         }
            //     }

            //     // Look for https cookies
            //     if (commPage.COOKIES.GetCookies(
            //         new Uri(string.Format("https://{0}/", key))).Count > 0)
            //     {
            //         Console.WriteLine(commPage.COOKIES.Count + " HTTPS COOKIES FOUND:");
            //         Console.WriteLine("----------------------------------");
            //         foreach (Cookie cookie in commPage.COOKIES.GetCookies(
            //             new Uri(string.Format("https://{0}/", key))))
            //         {
            //             Console.WriteLine(
            //                 "Name = {0} ; Value = {1} ; Domain = {2}",
            //                 cookie.Name, cookie.Value, cookie.Domain);
            //         }
            //     }
            // }

        }
        private string Helper_GetCSRFToken()
        {
            try
            {
                commPage.RequestURL = "https://admin.vrbo.com/haolb/csrf/async/token.html?preventCache=";
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JToken.Parse(commPage.Html);
                    return ob["csrftkn"].ToSafeString();

                }
            }
            catch { }
            return string.Empty;
        }

        private string Helper_GetDruidFromListingId(string listingTriad)
        {
            try
            {
                commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/properties/legacySummaries/?_restfully=true&locale=en_US&listingTriad={0}", listingTriad);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var arr = JArray.Parse(commPage.Html);
                    return arr[0]["druid"].ToSafeString();

                }
            }
            catch { }
            return string.Empty;
        }

        #region Automated Search API

        public int Search(string location, int noOfDays, int maxNights, int? max)
        {
            if (noOfDays < maxNights)
            {
                return 0;
            }
            var dates = GetDateCombinations(DateTime.Now, DateTime.Now.AddDays(noOfDays), maxNights);
            var searchResults = new List<SearchResult>();
            foreach (var date in dates)
            {
                for (int i = 1; i < 17; i++)
                {
                    searchResults.Add(new SearchResult() { Checkin = date.Item1, Ceckout = date.Item2, NoOfGuests = i, ListingIds = null });
                }
            }

            List<string> AllListingIds = new List<string>();
            int currentCount = 0;
            foreach (var combo in searchResults)
            {
                var listingIds = SearchListingsForIds(location, combo.Checkin.ToString("MM/dd/yyyy"), combo.Ceckout.ToString("MM/dd/yyyy"), combo.NoOfGuests, null);
                combo.ListingIds = listingIds;
                AllListingIds.AddRange(listingIds);
                currentCount++;

                if (max.HasValue && currentCount > max.Value)
                {
                    break;


                }
            }

            AllListingIds = AllListingIds.Distinct().ToList();

            foreach (var listingId in AllListingIds)
            {
                var url = "https://www.vrbo.com/" + listingId;
                var property = AirBnBScrapeProperties(url, "");
                if (property != null)
                {
                    var prop = Core.API.Vrbo.Properties.Add(property.Id, property.ListingUrl, property.Title, property.Longitude, property.Latitude, property.MinStay.ToInt(), property.Internet, property.Pets,
          property.WheelChair, property.Description, property.Reviews.ToInt(), property.Sleeps, property.Bedrooms, property.Bathrooms, property.PropertyType, property.AccomodationType, property.Smoking,
          property.AirCondition, property.SwimmingPool, property.Status);
                }
            }

            foreach (var searchResult in searchResults)
            {
                var Ids = searchResult.ListingIds.Distinct().ToList();
                foreach (var id in Ids)
                {
                    //Core.API.Vrbo.SearchResult.Add(Database, location, id, searchResult.Checkin, searchResult.Ceckout, searchResult.NoOfGuests);

                }
            }

            return searchResults.Count;
        }

        public List<string> SearchListingsForIds(string location, string checkin, string checkout, int noOfGuests, int? max)
        {

            location = location.Trim().EncodeURL();

            int count = 1;
            List<string> propertiesList = new List<string>();
            bool hasMore = true;
            var searchParms = GetSearchParameters(location, checkin, checkout, noOfGuests);
            var pageNumber = 1;
            do
            {


                string url = string.Format("https://www.vrbo.com/ajax/map/results/@,,,,z?q={0}&page={1}&region={2}&from-date={3}&to-date={4}&searchTermContext={5}&sleeps={6}-plus",
                    location, pageNumber, searchParms.Item1, checkin, checkout, searchParms.Item2, noOfGuests);
                try
                {

                    if (commPage == null)
                        commPage = new CommunicationPage(url);

                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {

                        var ob = JObject.Parse(commPage.Html);

                        var listings = (JArray)ob["results"]["hits"];

                        if (listings.Count > 0)
                        {
                            foreach (var item in listings)
                            {
                                if (max.HasValue && count > max.Value)
                                {
                                    break;
                                }
                                var id = item["propertyId"].ToSafeString();

                                propertiesList.Add(id);


                                count++;

                            }


                            //hasMore = tab[0]["pagination_metadata"]["has_next_page"].ToSafeString().ToBoolean();
                            //offset = tab[0]["pagination_metadata"]["section_offset"].ToSafeString();
                        }
                        else
                        {
                            hasMore = false;
                        }
                    }
                }
                catch (Exception e)
                {

                }
                pageNumber++;
            } while (hasMore);


            return propertiesList;






        }


        public List<string> SearchListings(string location, string checkin, string checkout, int noOfGuests, int? max)
        {
            if (commPage == null)
                commPage = new CommunicationPage("");
            // var res =  Login("vancouverbb98@gmail.com", "kendrickpublic3");
            location = location.Trim().EncodeURL();
            int count = 1;
            List<string> propertiesList = new List<string>();
            string offset = "0";
            bool hasMore = true;
            do
            {

                string url = "";
                if (commPage.IsLoggedIn)
                {


                    url = string.Format("https://www.airbnb.com/api/v2/explore_tabs?version=1.1.3&_format=for_explore_search_web&" +
                       "items_per_grid=300&experiences_per_grid=20&guidebooks_per_grid=20&fetch_filters=true&supports_for_you_v3=true&screen_size=large&timezone_offset=300" +
                       "&auto_ib=true&selected_tab_id=home_tab&location={0}&adults={4}&allow_override[]=&checkin={1}&checkout={2}&guests={4}&ib=true&_intents=p1&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en&section_offset={3}",
                       location, checkin, checkout, offset, noOfGuests);
                }
                else
                {
                    url = string.Format("https://www.airbnb.com/api/v2/explore_tabs?version=1.1.3&_format=for_explore_search_web&" +
        "items_per_grid=300&experiences_per_grid=20&guidebooks_per_grid=20&fetch_filters=true&supports_for_you_v3=true&screen_size=large&timezone_offset=300" +
        "&auto_ib=true&selected_tab_id=home_tab&location={0}&adults={4}&allow_override[]=&checkin={1}&checkout={2}&guests={4}&_intents=p1&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en&section_offset={3}",
        location, checkin, checkout, offset, noOfGuests);
                }
                try
                {



                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {

                        var ob = JObject.Parse(commPage.Html);
                        var tab = (JArray)ob["explore_tabs"];
                        if (tab != null)
                        {
                            var sections = (JArray)tab[0]["sections"];
                            if (sections != null)
                            {
                                var listings = (JArray)sections[0]["listings"];

                                if (listings.Count > 0)
                                {
                                    foreach (var item in listings)
                                    {
                                        if (max.HasValue && count > max.Value)
                                        {
                                            break;
                                        }
                                        var id = item["listing"]["id"].ToSafeString();
                                        propertiesList.Add(id);
                                        count++;

                                    }
                                }
                            }
                            hasMore = tab[0]["pagination_metadata"]["has_next_page"].ToSafeString().ToBoolean();
                            offset = tab[0]["pagination_metadata"]["section_offset"].ToSafeString();
                        }

                    }
                }
                catch (Exception e)
                {
                }

            } while (hasMore);
            return propertiesList;
        }

        #endregion
        #region Search Manually
        public ScrapeResult SearchListings(string location, string checkin, string checkout, string outputPath, int noOfGuests, int? max)
        {
            // var res =  Login("vancouverbb98@gmail.com", "kendrickpublic3");
            var sresult = new ScrapeResult();
            location = location.Trim().EncodeURL();
            var targetFilePath = outputPath;
            int count = 1;
            List<ListingDetail> propertiesList = new List<ListingDetail>();
            string offset = "0";
            bool hasMore = true;
            var searchParms = GetSearchParameters(location, checkin, checkout, noOfGuests);
            var pageNumber = 1;
            do
            {


                string url = string.Format("https://www.vrbo.com/ajax/map/results/@,,,,z?q={0}&page={1}&region={2}&from-date={3}&to-date={4}&searchTermContext={5}&sleeps={6}-plus",
                    location, pageNumber, searchParms.Item1, checkin, checkout, searchParms.Item2, noOfGuests);
                try
                {







                    if (commPage == null)
                        commPage = new CommunicationPage(url);

                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {

                        var ob = JObject.Parse(commPage.Html);

                        var listings = (JArray)ob["results"]["hits"];

                        if (listings.Count > 0)
                        {
                            foreach (var item in listings)
                            {
                                if (max.HasValue && count > max.Value)
                                {
                                    break;
                                }
                                var id = item["propertyId"].ToSafeString();
                                var link = "https://www.vrbo.com/" + id;
                                var property = AirBnBScrapeProperties(link, "");

                                if (property != null)
                                    propertiesList.Add(property);
                                else
                                {

                                }

                                count++;

                            }


                            //hasMore = tab[0]["pagination_metadata"]["has_next_page"].ToSafeString().ToBoolean();
                            //offset = tab[0]["pagination_metadata"]["section_offset"].ToSafeString();
                        }
                        else
                        {
                            hasMore = false;
                        }






                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
                pageNumber++;
            } while (hasMore);



            //Console.WriteLine("Writing to file..");
            //ExcelWriter.WriteToExcel(propertiesList, targetFilePath);





            var totalItems = propertiesList.Count();
            if (totalItems > 0)
            {
                sresult.Success = true;
                sresult.Message = "Operation Successful, file generated at " + outputPath;
                sresult.TotalRecords = totalItems;
            }
            else
            {
                sresult.Message = "An error occurred.";

            }


            return sresult;
        }

        public Tuple<string, string> GetSearchParameters(string location, string checkin, string checkout, int noOfGuests)
        {
            string url = string.Format("https://www.vrbo.com/results");
            if (commPage == null)
                commPage = new CommunicationPage(url);

            commPage.RequestURL = url;
            commPage.PersistCookies = true;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.Referer = "";

            commPage.Parameters = new Dictionary<string, string>();
            commPage.Parameters.Add("adults", noOfGuests.ToSafeString());
            commPage.Parameters.Add("children", "0");
            commPage.Parameters.Add("pets", "false");
            commPage.Parameters.Add("adultsCount", noOfGuests.ToSafeString());
            commPage.Parameters.Add("childrenCount", "0");
            commPage.Parameters.Add("petIncluded", "");
            commPage.Parameters.Add("q", location);
            commPage.Parameters.Add("uuid", "");
            commPage.Parameters.Add("from-date", checkin);
            commPage.Parameters.Add("to-date", checkout);
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var html = commPage.Html;
                var start = html.IndexOf("window.__PAGE_DATA__ =") + "window.__PAGE_DATA__ =".Length;
                var to = html.IndexOf("window.__APP_CONFIG__ =");
                String result = html.Substring(start, to - start);
                result = result.Trim().TrimEnd(';');
                var ob = JObject.Parse(result);
                var regionId = ob["searchUrl"]["regionId"].ToSafeString();
                var searchTermContext = ob["searchUrl"]["searchTermContext"].ToSafeString();
                return new Tuple<string, string>(regionId, searchTermContext);

            }
            return new Tuple<string, string>("", "");
        }


        public ListingDetail GetListingDetail(string url, string checkin, string checkout)
        {
            try
            {

                // url = "https://www.vrbo.com/887802";
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        string longitude = "";
                        string latitude = "";
                        try
                        {
                            latitude = xd.SelectSingleNode("//meta[@property='airbedandbreakfast:location:latitude']").GetAttributeFromNode("content");
                            longitude = xd.SelectSingleNode("//meta[@property='airbedandbreakfast:location:longitude']").GetAttributeFromNode("content");

                            //var s = commPage.Html.IndexOf("gon.space_lat_long={") + 20;
                            //var e = commPage.Html.IndexOf(";gon.translations", s);
                            //var raw = commPage.Html.Substring(s, e - s);

                            //s = raw.IndexOf("lat\":\"") + 6;
                            //e = raw.IndexOf(",", s);
                            //var lat = raw.Substring(s, e - s).Replace("\"","");

                            //s = raw.IndexOf("lng\":\"") + 6;
                            //e = raw.IndexOf("\"}", s);
                            //var lng = raw.Substring(s, e - s);

                            //longitude = lng;
                            //latitude = lat;
                        }
                        catch
                        {
                        }

                        var json = xd.SelectSingleNode("//script[@data-hypernova-key='p3hero_and_slideshowbundlejs']").GetInnerTextFromNode().HtmlDecode().Trim();
                        json = json.Replace("//\r\n", "");
                        json = json.Replace("<!--", "");
                        json = json.Replace("-->", "");
                        var ob = JObject.Parse(json);
                        var description = ob["slideshowProps"]["listing"]["summary"].ToSafeString();
                        var amenitiesArr = (JArray)ob["slideshowProps"]["listing"]["listing_amenities"];


                        var petNode = (JToken)amenitiesArr.Where(f => f["id"].ToSafeString() == "12").FirstOrDefault();
                        var pets = "Not allowed";
                        var petsbool = petNode["is_present"].ToBoolean();
                        if (petsbool == true)
                            pets = "Allowed";


                        var wheelChairNode = (JToken)amenitiesArr.Where(f => f["id"].ToSafeString() == "6").FirstOrDefault();
                        var wheelchair = "Not allowed";
                        var wheelchairbool = wheelChairNode["is_present"].ToBoolean();
                        if (wheelchairbool == true)
                            wheelchair = "Allowed";

                        var swimmingPoolNode = (JToken)amenitiesArr.Where(f => f["id"].ToSafeString() == "7").FirstOrDefault();
                        var swimmingPool = "No";
                        var swimmingPoolbool = swimmingPoolNode["is_present"].ToBoolean();
                        if (swimmingPoolbool == true)
                            swimmingPool = "Yes";

                        var smokingNode = (JToken)amenitiesArr.Where(f => f["id"].ToSafeString() == "11").FirstOrDefault();
                        var smoking = "Not allowed";
                        var smokingbool = smokingNode["is_present"].ToBoolean();
                        if (smokingbool == true)
                            smoking = "Allowed";

                        var airConditionNode = (JToken)amenitiesArr.Where(f => f["id"].ToSafeString() == "5").FirstOrDefault();
                        var airCondition = "No";
                        var airConditionbool = airConditionNode["is_present"].ToBoolean();
                        if (airConditionbool == true)
                            airCondition = "Yes";



                        var internetNode = (JToken)amenitiesArr.Where(f => f["id"].ToSafeString() == "4").FirstOrDefault();
                        var internet = "No";
                        var internetbool = internetNode["is_present"].ToBoolean();
                        if (internetbool == true)
                            internet = "Yes";

                        var minStay = ob["slideshowProps"]["listing"]["min_nights"].ToSafeString();
                        var listingId = ob["slideshowProps"]["listing"]["id"].ToSafeString();
                        var title = ob["slideshowProps"]["listing"]["name"].ToSafeString();
                        var nightlyMin = GetPrice(listingId, checkin, checkout);


                        var bathrooms = "";
                        var bedrooms = "";
                        var sleeps = "";
                        var propertyType = "";
                        var accomodationType = "";

                        var spaceArray = (JArray)ob["slideshowProps"]["listing"]["space_interface"];
                        var bth = (JToken)spaceArray.Where(f => f["label"].ToSafeString() == "Bathrooms:").FirstOrDefault();
                        if (bth != null)
                            bathrooms = bth["value"].ToSafeString();

                        var bed = (JToken)spaceArray.Where(f => f["label"].ToSafeString() == "Bedrooms:").FirstOrDefault();
                        if (bed != null)
                            bedrooms = bed["value"].ToSafeString();

                        var propType = (JToken)spaceArray.Where(f => f["label"].ToSafeString() == "Property type:").FirstOrDefault();
                        if (propType != null)
                            propertyType = propType["value"].ToSafeString();

                        var accomType = (JToken)spaceArray.Where(f => f["label"].ToSafeString() == "Room type:").FirstOrDefault();
                        if (accomType != null)
                            accomodationType = accomType["value"].ToSafeString();

                        var slps = (JToken)spaceArray.Where(f => f["label"].ToSafeString() == "Beds:").FirstOrDefault();
                        if (slps != null)
                            sleeps = slps["value"].ToSafeString();

                        var reviews = ob["slideshowProps"]["listing"]["review_details_interface"]["review_count"].ToSafeString();



                        string images = "";
                        try
                        {
                            List<string> imgsList = new List<string>();

                            var arr = (JArray)ob["heroProps"]["photos"];
                            foreach (var item in arr)
                            {
                                var imgUrl = item["picture"].ToSafeString();
                                if (!string.IsNullOrEmpty(imgUrl))
                                    imgsList.Add(imgUrl);
                            }
                            images = string.Join("|", imgsList);

                        }
                        catch (Exception e)
                        {

                        }

                        return new ListingDetail()
                        {
                            Id = listingId.ToInt(),
                            AccomodationType = accomodationType,
                            Bathrooms = bathrooms.ToInt(),
                            Bedrooms = bedrooms.ToInt(),
                            Description = description.HtmlDecode(),
                            Images = images,
                            Internet = internet,
                            ListingUrl = url,
                            MinStay = minStay,
                            Pets = pets,
                            PriceNightlyMax = "",
                            PriceNightlyMin = nightlyMin.ToSafeString(),
                            PriceWeeklyMax = "",
                            PriceWeeklyMin = "",
                            PropertyType = propertyType,
                            Reviews = reviews,
                            Sleeps = sleeps,
                            Smoking = smoking,
                            Title = title.HtmlDecode(),
                            WheelChair = wheelchair,
                            Longitude = longitude,
                            Latitude = latitude,
                            AirCondition = airCondition,
                            SwimmingPool = swimmingPool,

                        };
                    }

                }
            }
            catch (Exception e)
            {

            }
            return null;
        }

        #endregion



        public enum SyncType
        {
            All,
            New,
        }


        #region Reservations
        public List<Reservation> ScrapeReservations(string token, SyncType syncType, bool reservationRequests = false, bool reservations = false, bool inquiries = false, bool all = false)
        {
            var sresult = new ScrapeResult();
            List<Reservation> reservationList = new List<Reservation>();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var threads = new List<InboxListing>();
                    if (all)
                        threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.All));
                    else
                    {
                        if (reservationRequests)
                            threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.ReservationRequests));
                        if (reservations)
                            threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Reservations));
                        if (inquiries)
                            threads.AddRange(ScrapeInboxThreadListing(false, InboxThreadType.Inquiries));
                    }
                    foreach (var thread in threads)
                    {
                        var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", thread.Id);
                        commPage.RequestURL = detailUrl;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.PersistCookies = true;
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(CommPage.Html);
                            var ar = (JArray)ob["replyActions"];
                            List<ReplyAction> actions = new List<ReplyAction>();
                            if (ar.Count > 0)
                            {
                                foreach (var item in ar)
                                {
                                    var actionType = item["type"].ToSafeString();
                                    if (actionType == "NONE")
                                    {
                                        actionType = item["buttonTitle"].ToSafeString();
                                    }
                                    ReplyAction act = new ReplyAction
                                    {
                                        Type = actionType
                                    };
                                    if (actionType == "DECLINE_BOOKING")
                                    {
                                        var reasons = (JArray)item["declineReasons"];
                                        Dictionary<string, string> declineReasons = new Dictionary<string, string>();
                                        reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));
                                        act.MetaData = declineReasons;
                                    }
                                    actions.Add(act);
                                }
                            }
                            if (!actions.Any(f => f.Type.Contains("EDIT") || f.Type.Contains("ADD")))
                            {
                                if (thread.Status.Contains("TENTATIVE"))
                                {

                                    actions.Add(new ReplyAction { Type = "EDIT_BOOKING" });
                                }
                            }

                            var guests = ob["quote"]["numAdults"].ToInt();
                            guests += ob["quote"]["numChildren"].ToInt();
                            var nights = ob["quote"]["numNights"].ToInt();
                            var pets = ob["quote"]["numPets"].ToInt();
                            var quoteId = ob["quote"]["quoteGuid"].ToSafeString();
                            var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                            var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
                            var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
                            var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

                            var fees = ((JArray)ob["quote"]["fees"]).ToList();


                            var rentalCost = "";
                            var serviceFee = "";
                            var cleaningFee = "";
                            var refundableDamageDeposit = "";
                            var taxRate = "";

                            var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                            if (rentalNode != null)
                            {
                                rentalCost = rentalNode["amount"]["amount"].ToSafeString();
                                if (rentalNode["feeBasis"]["tax"] != null)
                                {
                                    taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

                                }
                            }

                            var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
                            if (cleaningNode != null)
                                cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

                            var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
                            if (serviceNode != null)
                                serviceFee = serviceNode["amount"]["amount"].ToSafeString();

                            var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
                            if (ddNode != null)
                                refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();


                            var phone = ob["conversation"]["otherParticipants"][0]["phone"].ToSafeString();
                            var email = ob["conversation"]["otherParticipants"][0]["emailAddress"].ToSafeString();
                            var reservationNode = ob["reservation"];
                            var reservationId = "";
                            var reservationCreatedAt = "";
                            var reservationExpiry = "";
                            if (reservationNode != null)
                            {
                                reservationId = ob["reservation"]["id"].ToSafeString();
                                reservationCreatedAt = ob["reservation"]["created"].ToSafeString();
                                reservationExpiry = ob["reservation"]["expirationDateTime"].ToSafeString();
                            }
                            var reservationDeclineDateTime = "";
                            var reservationAcceptedDatetime = "";

                            var messages = ((JArray)ob["messages"]).ToList();

                            var declineNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_DECLINED").FirstOrDefault();
                            if (declineNode != null)
                                reservationDeclineDateTime = declineNode["sentTimestamp"].ToSafeString();

                            var acceptNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_ACCEPTED").FirstOrDefault();
                            if (acceptNode != null)
                                reservationAcceptedDatetime = acceptNode["sentTimestamp"].ToSafeString();

                            var listingUniqueId = ob["property"]["druidUuid"].ToSafeString();


                            var summary = ob["quote"]["quoteTotals"].ToSafeString();
                            //var jsonSummary =  JsonConvert.SerializeObject(summary);




                            var reservation = new Reservation
                            {
                                CheckInDate = thread.StayStartDate,
                                CheckoutDate = thread.StayEndDate,
                                Code = thread.ReservationCode,

                                ServiceFee = serviceFee,
                                CleaningFee = cleaningFee,
                                TotalPayout = payableToHost,//quoteTotal,
                                ReservationCost = quoteTotal, //reservationCost,
                                RefundableDamageDeposit = refundableDamageDeposit,
                                RentalCost = rentalCost,
                                TaxRate = taxRate,
                                TotalTax = taxAmount,
                                ReservationCostSummary = summary,


                                Nights = nights,
                                GuestCount = guests,
                                GuestEmail = email,
                                GuestPhone = phone,
                                ListingId = thread.ListingId,
                                GuestName = thread.GuestFirstName + " " + thread.GuestLastName,
                                GuestId = thread.GuestId,
                                InquiryDatetime = thread.InquiryDate,
                                ReservationId = reservationId, //check this
                                Status = thread.Status,
                                IsActive = thread.Status == "STAYING",
                                ReplyActions = actions,
                                Pets = pets.ToSafeString(),
                                ReservationRequestDatetime = reservationCreatedAt,
                                ReservationRequestExpiryDatetime = reservationExpiry,
                                ReservationRequestAcceptedDatetime = reservationAcceptedDatetime,
                                ReservationRequestCancelledDatetime = reservationDeclineDateTime,
                                ListingUniqueId = listingUniqueId


                            };






                            var addResult = Core.API.Vrbo.Inquiries.Add(Database, reservation.ReservationId, reservation.Code, reservation.Status, reservation.CheckInDate.ToDateTime(),
                                   reservation.CheckoutDate.ToDateTime(), reservation.Nights, reservation.ListingName, reservation.ListingId,
                                   reservation.ListingUrl, reservation.GuestId, reservation.GuestUrl, reservation.GuestEmail, reservation.GuestPhone,
                                   reservation.ReservationCost, reservation.ReservationCostSummary, reservation.CleaningFee, reservation.ServiceFee,
                                   reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(), reservation.ReservationRequestDatetime.ToDateTime(),
                                   reservation.ReservationRequestExpiryDatetime.ToDateTime(), reservation.GuestCount.ToInt(), reservation.ReservationRequestAcceptedDatetime.ToDateTime(),
                                   reservation.ReservationRequestCancelledDatetime.ToDateTime(), reservation.Pets, JsonConvert.SerializeObject(reservation.ReplyActions), reservation.GuestName, reservation.ListingUniqueId,
                                   reservation.TaxRate, reservation.TotalTax, reservation.RefundableDamageDeposit, reservation.RentalCost);

                            if (syncType == SyncType.New && addResult.IsDuplicate)
                            {
                                break;
                            }
                            else
                            {
                                reservationList.Add(reservation);
                            }


                        }








                    }











                    var totalItems = reservationList.Count();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";//, file generated at " + outputPath;
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";

                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }

            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return reservationList;
        }















        public void ScrapeReservationDetails(string url, Reservation reservation)
        {
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {


                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-shared_itinerary_app']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);

                        reservation.ReservationCost = ob["base_price"].ToSafeString();
                        reservation.TotalPayout = ob["total_price"].ToSafeString();
                        reservation.CleaningFee = ob["extras_price"].ToSafeString();
                        reservation.ServiceFee = ob["host_fee"].ToSafeString();
                        reservation.ReservationCostSummary = ob["price_per_night_summary"].ToSafeString();


                        reservation.ListingName = ob["hosting_name"].ToSafeString();
                        reservation.Nights = ob["nights"].ToSafeString();
                        reservation.CheckInDate = ob["checkin_date"].ToSafeString();

                        reservation.CheckoutDate = ob["checkout_date"].ToSafeString();
                        reservation.GuestEmail = ob["other_user_email"].ToSafeString();
                        reservation.GuestPhone = ((JArray)ob["other_user_phone_numbers"])[0].ToSafeString();


                        reservation.GuestId = ob["booker"]["user_id"].ToSafeString();




                    }

                }
            }
            catch (Exception e)
            {

            }
        }
        public void GetInquiryDetails(string url, Reservation reservation, bool hasReservationDetailLink)
        {
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {


                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);

                        var isReservationAccepted = ob["is_reservation_accepted"].ToSafeString().ToBoolean();
                        var appData = ob["appData"];
                        reservation.StatusType = appData["statusType"].ToInt();
                        var statusTypes = appData["statusTypes"];
                        reservation.StatusDesc = ((JProperty)statusTypes.Where(f => ((JProperty)f).Value.ToSafeString() == reservation.StatusType.ToSafeString()).FirstOrDefault()).Name.ToSafeString();
                        reservation.CanSendSpecialOffer = appData["canSendSpecialOffer"].ToSafeString().ToBoolean();
                        if (reservation.StatusType == 5)
                        {
                            ////var alterationNode = appData["alterationRequestInfo"];
                            ////if (alterationNode != null)
                            ////{
                            ////    reservation.Alt_InitiatedBy = alterationNode["initiated_by"].ToSafeString();
                            ////    reservation.Alt_DateRange = alterationNode["dateRange"].ToSafeString();
                            ////    reservation.Alt_HostEarns = alterationNode["hostEarns"].ToSafeString();
                            ////    reservation.Alt_GuestPays = alterationNode["guestPays"].ToSafeString();
                            ////    reservation.ListingName = alterationNode["listingName"].ToSafeString();
                            ////    reservation.Alt_Guests = alterationNode["guests"].ToInt();


                            ////}
                        }
                        //foreach (JProperty item in statusTypes)
                        //{

                        //    if (item.Value.ToSafeString() == statusType)
                        //    {
                        //        item.Name
                        //    }
                        //}
                        // ss.Parent.
                        var ar = (JArray)ob["appData"]["posts"];
                        if (reservation.Status.ToLower().Contains("pending"))
                        {


                            var non_response = ob["non_response"].ToSafeString();
                            if (!string.IsNullOrEmpty(non_response))
                            {
                                var dt = non_response.ToDateTime();
                                var origDt = dt.AddDays(-1);
                                //- reservation.BookingRequestDatetime = origDt.ToString();

                            }
                        }


                        if (string.IsNullOrEmpty(reservation.CheckInDate) && string.IsNullOrEmpty(reservation.CheckoutDate))
                        {
                            ////var tuple = ScrapeDates(reservation.BookingDatetime);
                            ////if (tuple != null)
                            ////{
                            ////    reservation.CheckInDate = tuple.Item1.ToSafeString();
                            ////    reservation.CheckoutDate = tuple.Item2.ToSafeString();
                            ////}
                            ////else
                            ////{
                            ////    reservation.CheckInDate = ob["tracking"]["rebooking_promotion"]["start_date"].ToSafeString();
                            ////    reservation.CheckoutDate = ob["tracking"]["rebooking_promotion"]["end_date"].ToSafeString();

                            ////}
                        }
                        else
                        {
                            reservation.CheckInDate = ob["tracking"]["rebooking_promotion"]["start_date"].ToSafeString();
                            reservation.CheckoutDate = ob["tracking"]["rebooking_promotion"]["end_date"].ToSafeString();

                        }


                        //This is what inquired for(during inquiry these dates were asked)
                        //ob["inquiry"]["checkin_date"].ToSafeString();
                        //  ob["inquiry"]["checkout_date"].ToSafeString();

                        try
                        {
                            var dtStrt = reservation.CheckInDate.ToDateTime();
                            var dtEnd = reservation.CheckoutDate.ToDateTime();
                            reservation.Nights = (dtEnd - dtStrt).Days.ToSafeString();
                        }
                        catch (Exception)
                        {

                        }

                        //  var users = (JArray)ob["appData"]["participantsProfiles"];
                        //participantsProfiles
                        if (ar.Count > 0)
                        {
                            var bookingpost = ar.Where(f => f["template_name"].ToSafeString() == "44").FirstOrDefault();
                            if (bookingpost != null)
                            {
                                //- reservation.BookingDatetime = bookingpost["created_at"].ToSafeString();
                            }

                            if (reservation.Status == "Accepted")
                            {
                                // var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "25").FirstOrDefault();
                                var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "25" && f["created_at"].ToDateTime().Date <= reservation.CheckInDate.ToDateTime().Date).FirstOrDefault();

                                if (reservationConfirmedPost != null)
                                {
                                    //- reservation.ReservationConfirmationDatetime = reservationConfirmedPost["created_at"].ToSafeString();
                                }
                            }
                            else
                            {
                                var reservationCanceledPost = ar.Where(f => f["template_name"].ToSafeString() == "24" && f["created_at"].ToDateTime().Date <= reservation.CheckInDate.ToDateTime().Date).FirstOrDefault();
                                if (reservationCanceledPost != null)
                                {
                                    //-reservation.ReservationCancelledDatetime = reservationCanceledPost["created_at"].ToSafeString();
                                }
                            }
                            var reservationPost = ar.Where(f => f["link_type"].ToSafeString() == "Hosting" && f["created_at"].ToDateTime().Date <= reservation.CheckInDate.ToDateTime().Date).LastOrDefault();
                            if (reservationPost != null)
                            {
                                reservation.InquiryDatetime = reservationPost["created_at"].ToSafeString();
                            }
                        }


                        if (!hasReservationDetailLink)
                        {


                            var paymentInfo = ob["appData"]["paymentInfo"];
                            if (paymentInfo != null)
                            {
                                reservation.ReservationCost = paymentInfo["basePrice"].ToSafeString();
                                reservation.TotalPayout = paymentInfo["hostEarns"].ToSafeString();
                                reservation.CleaningFee = paymentInfo["extrasPrice"].ToSafeString();

                            }


                            var otherPersonInfo = ob["appData"]["otherPerson"];
                            if (otherPersonInfo != null)
                            {
                                reservation.GuestEmail = otherPersonInfo["email"].ToSafeString();
                                reservation.GuestPhone = otherPersonInfo["phone"].ToSafeString();
                            }

                        }





                        var reservationInfo = ob["appData"]["reservationInfo"];
                        if (reservationInfo != null)
                        {
                            reservation.GuestCount = reservationInfo["number_of_guests"].ToSafeString();
                        }




                    }

                }
            }
            catch (Exception e)
            {

            }
        }

        public bool AcceptOffer(string reservationId, string message)
        {
            try
            {



                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/acceptv2?_restfully=true");
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                CommPage.JsonStringToPost = string.Format("{{\"acceptRequestPaymentType\":\"ONLINE\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\"}}", reservationId, message);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                commPage.Referer = "";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var status = ob["reservationUuid"].ToSafeString();
                    if (!string.IsNullOrEmpty(status))
                    {
                        return true;

                    }
                }

            }
            catch (Exception e)
            {

            }
            return false;
        }
        public bool RejectOffer(string reservationId, string reasonType, string reason, string message)
        {
            try
            {



                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/declinev2?_restfully=true");
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                CommPage.JsonStringToPost = string.Format("{{\"declineReason\":\"{3}\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\",\"customDeclineReason\":\"{2}\"}}", reservationId, message, reason, reasonType);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                commPage.Referer = "";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var status = ob["reservationUuid"].ToSafeString();
                    if (!string.IsNullOrEmpty(status))
                    {
                        return true;

                    }
                }

            }
            catch (Exception e)
            {

            }
            return false;
        }
        public bool ProcessAlterReservation_old(string reservationCode, DateTime checkInDate, DateTime checkoutDate, decimal price, string listingId, int guests)
        {
            try
            {

                var chkInDateStr = checkInDate.ToString("MM/dd/yyyy");
                var chkOutdateStr = checkoutDate.ToString("MM/dd/yyyy");
                var xd = commPage.ToXml();

                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                var url = "https://www.airbnb.com/reservation_alterations";
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;

                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("code", reservationCode);
                commPage.Parameters.Add("pricing[hosting_id]", listingId);
                commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                commPage.Parameters.Add("pricing[start_date]", chkInDateStr);
                commPage.Parameters.Add("pricing[end_date]", chkOutdateStr);
                commPage.Parameters.Add("price", price.ToSafeString());
                commPage.Parameters.Add("authenticity_token", token);
                commPage.RequestHeaders = new Dictionary<string, string>();

                commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation_alterations/"))
                    {
                        return true;
                    }
                }

            }
            catch (Exception e)
            {
            }
            return false;
        }
        public bool ProcessAlterReservation(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message)
        {
            var alterResult = ProcessAlterReservation(conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Add);
            if (alterResult == false)
                alterResult = ProcessAlterReservation(conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Replace);
            else if (alterResult == false)
                alterResult = ProcessAlterReservation(conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Update);

            return alterResult;
        }
        private bool ProcessAlterReservation(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message, AlterType alterType)
        {
            bool processAlteration = true;
            try
            {


                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var quote = ob["quote"];
                    quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                    quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");

                    // quote["paymentRequests"][0]["status"] = "SCHEDULED";
                    var fees = ((JArray)ob["quote"]["fees"]).ToList();



                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                        rentalNode["amount"]["amount"] = price;

                    quote["numChildren"] = children;
                    quote["numAdults"] = adults;
                    quote["bodyText"] = message;

                    if (processAlteration)
                    {
                        try
                        {

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1");//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(CommPage.Html);
                            }



                            switch (alterType)
                            {
                                case AlterType.Update:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                    break;
                                case AlterType.Add:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

                                    break;
                                case AlterType.Replace:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true");

                                    break;
                                default:
                                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                    break;
                            }

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            CommPage.JsonStringToPost = CommPage.Html;//JsonConvert.SerializeObject(quote);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(CommPage.Html);
                                var quoteGuid = ob["quoteGuid"].ToSafeString();
                                if (string.IsNullOrEmpty(quoteGuid))
                                {
                                    return false;
                                }

                                return true;
                            }

                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {







                        try
                        {



                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(CommPage.Html);
                                var quoteTotal = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                                fees = ((JArray)ob["fees"]).ToList();

                                var reservationCost = "";
                                var serviceFee = "";
                                var cleaningFee = "";
                                var refundableDamageDeposit = "";

                                rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                                if (rentalNode != null)
                                    reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                            }

                        }
                        catch (Exception e)
                        {

                        }

                    }

                }









            }
            catch (Exception e)
            {
            }
            return false;
        }
        public Tuple<string, string> GetAlterReservationDetail(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children)
        {
            var reservationCost = "";
            var total = "";
            try
            {


                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var quote = ob["quote"];
                    quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                    quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");


                    var fees = ((JArray)ob["quote"]["fees"]).ToList();



                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                        rentalNode["amount"]["amount"] = price;

                    quote["numChildren"] = children;
                    quote["numAdults"] = adults;









                    try
                    {



                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        CommPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(CommPage.Html);
                            total = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                            fees = ((JArray)ob["fees"]).ToList();

                            var serviceFee = "";
                            var cleaningFee = "";
                            var refundableDamageDeposit = "";

                            rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                            if (rentalNode != null)
                                reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                        }

                    }
                    catch (Exception e)
                    {

                    }



                }









            }
            catch (Exception e)
            {
            }
            return new Tuple<string, string>(reservationCost, total);
        }

        public bool CancelAlteration(string reservationCode)
        {
            try
            {
                commPage.RequestURL = commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);
                string alterationId = "";
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {


                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-alter_cancel_data']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);
                        var alts = (JArray)ob["reservation"]["reservation_alterations"];
                        if (alts != null)
                        {
                            alterationId = alts[0]["reservation_alteration"]["id"].ToSafeString();
                        }
                    }
                }

                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                var url = "https://www.airbnb.com/reservation_alterations/cancel/" + alterationId;
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;

                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("utf8", "✓");
                commPage.Parameters.Add("authenticity_token", token);
                commPage.RequestHeaders = new Dictionary<string, string>();

                commPage.Referer = string.Format("https://www.airbnb.com/reservation_alterations/{0}", alterationId);
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation/itinerary"))
                    {
                        return true;
                    }
                }

            }
            catch (Exception e)
            {
            }
            return false;
        }

        public enum AlterType
        {
            Update,
            Add,
            Replace

        }
        #endregion
        public bool Pre_Approve_Inquiry(string token, string conversationId, string message)
        {
            var isLogedIn = ValidateTokenAndLoadCookies(token);
            try
            {

                ////var inboxUrl = string.Format("https://admin.vrbo.com/rm");
                ////commPage.RequestURL = inboxUrl;
                ////commPage.ReqType = CommunicationPage.RequestType.GET;
                ////commPage.PersistCookies = true;
                ////// commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
                ////commPage.RequestHeaders = new Dictionary<string, string>();
                ////commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                ////ProcessRequest(commPage);



                ////var startUrl = string.Format("https://admin.vrbo.com/rm/message/l-321.827199.1375137/g-{0}", conversationId);
                ////commPage.RequestURL = startUrl;
                ////commPage.ReqType = CommunicationPage.RequestType.GET;
                ////commPage.PersistCookies = true;
                ////// commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
                ////commPage.RequestHeaders = new Dictionary<string, string>();
                ////commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*;q=0.8");
                ////ProcessRequest(commPage);


                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&declineReasonsVersion=2&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                // commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "*");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                //var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                //var auth_token = cc["crumb"].Value.DecodeURL();
                //commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var quote = (JObject)ob["quote"];

                    quote["commissionable"] = true;
                    quote["commissioned"] = true;

                    try
                    {

                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?locale=en_US_VRBO&useRates=true&conversationId={0}&_restfully=true", conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");
                                                                                                                                                                                                                // commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?locale=en_US_VRBO&_restfully=true", conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);




                        //commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.827199.1375137/g-{0}", conversationId);
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(commPage.Html);
                        }

                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/book?_restfully=true");
                        ob.Remove("bodyText");
                        ob.Add("bodyText", message);


                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(ob);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                        cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(commPage.Html);
                            var quoteGuid = ob["quoteGuid"].ToSafeString();
                            if (string.IsNullOrEmpty(quoteGuid))
                            {
                                return false;
                            }

                            return true;
                        }

                    }
                    catch (Exception e)
                    {

                    }


                }









            }
            catch (Exception e)
            {
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="checkinDate">pass it null if you don't want to edit this</param>
        /// <param name="checkoutDate">pass it null if you don't want to edit this</param>
        /// <param name="adults">pass it null if you don't want to edit this</param>
        /// <param name="children">pass it null if you don't want to edit this</param>
        /// <returns></returns>
        public EditReservationStep1 EditReservation_Step1(string token, string conversationId, DateTime? checkinDate = null, DateTime? checkoutDate = null, int? adults = null, int? children = null)
        {

            
            string quoteGuid = "";
            string apiReservationUrl = "";
            string amount = "";
            float maxRefundable = 0;
            string unitLink = "";
            var isLogedIn = ValidateTokenAndLoadCookies(token);
            if (isLogedIn)
            {
                try
                {

                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", conversationId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        unitLink = ob.FindTokens("unitLink").FirstOrDefault().ToSafeString();
                        quoteGuid = ob.FindTokens("quoteGuid").FirstOrDefault().ToSafeString();
                        var reservation = (JObject)ob["reservation"];
                        var reservationId = reservation["id"].ToString();
                        if (checkinDate.HasValue)
                            reservation["checkinDate"] = checkinDate.Value.ToString("yyyy-MM-dd");
                        if (checkoutDate.HasValue)
                            reservation["checkinDate"] = checkoutDate.Value.ToString("yyyy-MM-dd");
                        if (adults.HasValue)
                            reservation["numAdults"] = adults.Value;
                        if (children.HasValue)
                            reservation["numChildren"] = children.Value;
                        try
                        {

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/reservations/update?_restfully=true&locale=en_US&site=vrbo&reservationId={0}", reservationId);

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                            commPage.JsonStringToPost = JsonConvert.SerializeObject(reservation);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                            var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                            var auth_token = cc["crumb"].Value.DecodeURL();
                            commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                apiReservationUrl = ob["apiReservationUrl"].ToSafeString();


                            }

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/getAdjustedRent?externalRefLink={0}&_restfully=true", apiReservationUrl);



                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                            commPage.Referer = "";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                amount = ob["amount"].ToSafeString();
                                var curencyCode = ob["currencyCode"].ToSafeString();



                                var conversationURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", conversationId);
                                commPage.RequestURL = conversationURL;
                                commPage.ReqType = CommunicationPage.RequestType.GET;
                                commPage.PersistCookies = true;
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "*");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(CommPage.Html);
                                    var tokens = ob.FindTokens("refundableAmount");
                                    maxRefundable = tokens.Where(x => x is JObject)
                                          .Where(x => x["amount"].ToShort() > 0)
                                          .Select(x => x["amount"].ToShort()).FirstOrDefault();

                                    // EditReservation_Step2_RefundPayment(1, "testing..", conversationId);
                                   // EditReservation_Step2_AddPaymentRequest(quoteGuid, unitLink, "testing code", DateTime.Now.AddDays(3), "my message", 2);
                                }

                            }

                        }
                        catch (Exception e)
                        {

                        }


                    }









                }
                catch (Exception e)
                {
                }
            }
            return new EditReservationStep1 {
                 CalculatedChange = amount,
                  ConversationId = conversationId,
                   MaxRefund = maxRefundable,
                    QuoteGuid = quoteGuid,
                     UnitLink = unitLink
            };
        }
        public bool EditReservation_Step2_AddPaymentRequest(string quoteId, string unitLink, string description, DateTime dueDate, string message, float amount)
        {
            try
            {
                string postJson = string.Format("{{\"quoteGuid\":\"{0}\",\"unitLink\":\"{1}\",\"payment\":{{\"amount\":{2},\"currencyCode\":\"USD\"}},\"description\":\"{3}\",\"ownerMessage\":\"{4}\",\"dueDate\":\"{5}\"}}", quoteId, unitLink, amount, description, message, dueDate.ToString("yyyy-MM-dd"));


                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/add?_restfully=true");

                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                commPage.JsonStringToPost = postJson;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                var auth_token = cc["crumb"].Value.DecodeURL();
                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                commPage.Referer = "";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var quoteGuid = ob["quotePaymentScheduleGuid"].ToSafeString();
                    if (string.IsNullOrEmpty(quoteGuid))
                    {
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception e)
            {


            }
            return false;
        }
        public bool EditReservation_Step2_RefundPayment(short amount, string description, string conversationId)
        {
            try
            {

                var conversationURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", conversationId);
                commPage.RequestURL = conversationURL;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "*");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(CommPage.Html);
                    var partialRefund = ob.FindTokens("partialRefundableAmount").FirstOrDefault();
                    var maxRefund = ob.FindTokens("refundableAmount").FirstOrDefault();
                    var cancelable = ob.FindTokens("cancelableAmount").FirstOrDefault();

                    var quoteUuid = ob.FindTokens("quoteGuid").FirstOrDefault().ToSafeString();
                    var resId = ob.FindTokens("reservationReferenceNumber").FirstOrDefault().ToSafeString();
                    var paymentScheduleUuid = ob.FindTokens("paymentRequestUUID").FirstOrDefault().ToSafeString();















                    string postJsonRaw = string.Format("{{\"paymentScheduleUuid\":\"{0}\",\"paymentMethodLabel\":\"\",\"quoteUuid\":\"{1}\",\"currencyCode\":\"USD\",\"resId\":\"{2}\",\"isPPB\":false,\"hasLodgingTax\":false,\"isOfflinePayment\":false,\"amountEntered\":\"{3}\",\"description\":\"{4}\",\"shouldRefundProducts\":false,\"type\":\"REFUND\",\"cancelReservation\":false,\"amount\":{{\"amount\":{3},\"currencyCode\":\"USD\",\"localized\":\"${3}\"}},\"totalRefundAmountLocalized\":\"${3}\"}}", paymentScheduleUuid, quoteUuid,resId,amount.ToString("0.0"), description);
                    var postJson = JObject.Parse(postJsonRaw);
                    postJson.Add("maxRefund", maxRefund);
                    postJson.Add("cancelableAmount", cancelable);
                    postJson.Add("maxPartialRefund", partialRefund);

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/refund?_restfully=true");

                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.JsonStringToPost = JsonConvert.SerializeObject(postJson);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.StatusCode == HttpStatusCode.NoContent)
                    {
                            return true;
                    }
                }
            }
            catch { }
            return false;
        }
        #region InboxUserDetails
        public ScrapeResult ScrapeInboxUserDetails(string token, string outputPath)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    List<InboxListing> listings = ScrapeInboxThreadListing(false, InboxThreadType.All);
                    var totalItems = UserDetailsFromInbox_Wrapper(listings);
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";

                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }


        public int UserDetailsFromInbox_Wrapper(List<InboxListing> listings)
        {
            List<AirbnbUser> userList = new List<AirbnbUser>();
            foreach (var item in listings)
            {
                var result = ScrapeInboxUserDetails(item.Id);
                if (result != null)
                {
                    var res = Core.API.AirbnbUser.Add(Database, result.Id, result.Name, result.Phone, result.Email, result.ProfileImageUrl);
                    userList.Add(result);
                }
            }

            //string jsonString = JsonConvert.SerializeObject(userList, Newtonsoft.Json.Formatting.Indented);

            //using (StreamWriter outputFile = new StreamWriter(outputPath))
            //{
            //    outputFile.Write(jsonString);
            //}
            return userList.Count;
        }
        public AirbnbUser ScrapeInboxUserDetails(string id)
        {
            string url = string.Format("https://www.airbnb.com/z/q/{0}", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);

                var name = ob["appData"]["otherPerson"]["fullName"].ToSafeString();
                if (string.IsNullOrEmpty(name))
                    name = ob["appData"]["otherPerson"]["smartName"].ToSafeString();


                var userId = ob["appData"]["otherPerson"]["id"].ToSafeString();
                var email = ob["appData"]["otherPerson"]["email"].ToSafeString();
                var phone = ob["appData"]["otherPerson"]["phone"].ToSafeString();
                var ProfilePic = ob["appData"]["otherPerson"]["profilePicUrl"].ToSafeString();

                return new AirbnbUser
                {
                    Email = email,
                    Id = userId,
                    Name = name,
                    Phone = phone,
                    ProfileImageUrl = ProfilePic
                };
            }
            return null;
        }
        #endregion

        #region TransactionHistory
        public ScrapeResult ScrapeTransactionHistory(string token)
        {
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = "https://www.airbnb.com/users/transaction_history";
                try
                {

                    int count = 1;
                    List<TransactionHistory> transactionList = new List<TransactionHistory>();




                    count++;
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-transaction_history']");
                            var json = meta.GetAttributeFromNode("content").HtmlDecode();
                            var ob = JObject.Parse(json);

                            var yearsRaw = ob["filter_data"]["available_years"].ToSafeString();
                            yearsRaw = yearsRaw.Replace("[", "").Replace("]", "");
                            var years = yearsRaw.Split(',').ToList();
                            years = years.Select(f => ScrapeHelper.CleanText(f.Trim())).ToList();
                            foreach (var year in years)
                            {
                                var page = 1;
                                var totalPages = 1;
                                do
                                {


                                    //year = year.Trim();
                                    //year = ScrapeHelper.CleanText(year);
                                    string fiterUrl = string.Format("https://www.airbnb.com/transaction_history/24056456?year={0}&start_month=1&end_month=12&for_payout_tracker=true&page={1}", year, page);
                                    commPage.RequestURL = fiterUrl;
                                    commPage.PersistCookies = true;
                                    commPage.ReqType = CommunicationPage.RequestType.JSONGET;
                                    ProcessRequest(commPage);
                                    if (commPage.IsValid)
                                    {
                                        ob = JObject.Parse(commPage.Html);
                                        page = ob["current_page"].ToSafeString().ToInt();
                                        totalPages = ob["total_pages"].ToSafeString().ToInt();

                                        var lineItems = (JArray)ob["line_items"];
                                        foreach (var item in lineItems)
                                        {
                                            TransactionHistory transaction = new TransactionHistory();

                                            transaction.Id = item["id"].ToSafeString();
                                            transaction.ArrivalDate = item["arrival_date"].ToSafeString();
                                            transaction.ReleaseDate = item["ready_for_release_at"].ToSafeString();
                                            transaction.Description = item["details"].ToSafeString();

                                            transaction.Type = item["sub_type"].ToSafeString();
                                            transaction.Amount = item["display_amount"].ToSafeString();

                                            transaction.PayoutId = item["payout_info"]["id"].ToSafeString();

                                            var reservationNode = item["reservation2"];
                                            if (reservationNode != null)
                                            {
                                                transaction.ReservationId = reservationNode["id"].ToSafeString();
                                                transaction.ReservationCode = reservationNode["confirmation_code"].ToSafeString();
                                                transaction.GuestName = reservationNode["guest_name"].ToSafeString();
                                                var hostingNode = reservationNode["hosting"];
                                                if (hostingNode != null)
                                                {
                                                    transaction.ListingId = hostingNode["id"].ToSafeString();
                                                    transaction.ListingName = hostingNode["name"].ToSafeString();


                                                }
                                            }





                                            transactionList.Add(transaction);
                                            var trans = Core.API.Transaction.Add(Database, transaction.Id, transaction.Type, transaction.ReleaseDate.ToDateTime(), transaction.ArrivalDate.ToDateTime(),
                                                  transaction.Description, transaction.Amount, transaction.PayoutId, transaction.ListingId, transaction.ListingName, transaction.GuestName, transaction.ReservationCode, transaction.ReservationId);

                                        }


                                    }
                                    page++;
                                } while (page <= totalPages);
                            }


                        }
                    }






                    //string jsonString = JsonConvert.SerializeObject(transactionList, Newtonsoft.Json.Formatting.Indented);

                    //using (StreamWriter outputFile = new StreamWriter(outputPath))
                    //{
                    //    outputFile.Write(jsonString);
                    //}





                    var totalItems = transactionList.Count();
                    if (totalItems > 0)
                    {
                        sresult.Success = true;
                        sresult.Message = "Operation Successful";
                        sresult.TotalRecords = totalItems;
                    }
                    else
                    {
                        sresult.Message = "An error occurred.";

                    }

                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            else
            {
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }





        #endregion


        #region Owner Profile
        //public ScrapeResult OwnerProfile(string email, string password, string outputPath)
        //{

        //    var sresult = new ScrapeResult();
        //    OwnerProfile profile = new Models.OwnerProfile();
        //    var result = Login(email, password);
        //    if (result.Item1 == true)
        //    {
        //        string url = "https://www.airbnb.com/users/edit";
        //        try
        //        {
        //            commPage.RequestURL = url;
        //            commPage.PersistCookies = true;
        //            commPage.RequestHeaders = new Dictionary<string, string>();
        //            commPage.ReqType = CommunicationPage.RequestType.GET;
        //            ProcessRequest(commPage);
        //            if (commPage.IsValid)
        //            {
        //                XmlDocument xd = commPage.ToXml();
        //                if (xd != null)
        //                {
        //                    var userId = xd.SelectSingleNode("//input[@name='user_id']").GetAttributeFromNode("value");
        //                    var firstName = xd.SelectSingleNode("//input[@id='user_first_name']").GetAttributeFromNode("value");
        //                    var lastName = xd.SelectSingleNode("//input[@id='user_last_name']").GetAttributeFromNode("value");
        //                    var gender = xd.SelectSingleNode("//select[@id='user_sex']//option[@selected='selected']").GetAttributeFromNode("value");

        //                    var birthMonth = xd.SelectSingleNode("//select[@id='user_birthdate_2i']//option[@selected='selected']").GetAttributeFromNode("value");
        //                    var birthDate = xd.SelectSingleNode("//select[@id='user_birthdate_3i']//option[@selected='selected']").GetAttributeFromNode("value");
        //                    var birthYear = xd.SelectSingleNode("//select[@id='user_birthdate_1i']//option[@selected='selected']").GetAttributeFromNode("value");
        //                    var userEmail = xd.SelectSingleNode("//input[@id='user_email']").GetAttributeFromNode("value");
        //                    var userPhone = xd.SelectSingleNode("//table[@class='phone-numbers-table']//tr[@class='verified']").GetAttributeFromNode("data-number");

        //                    var language = xd.SelectSingleNode("//select[@id='user_profile_info_preferred_language']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
        //                    var currency = xd.SelectSingleNode("//select[@id='user_profile_info_preferred_currency']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
        //                    var aboutMe = xd.SelectSingleNode("//textarea[@id='user_profile_info_about']").GetAttributeFromNode("value");

        //                    var school = xd.SelectSingleNode("//input[@id='user_profile_info_university']").GetAttributeFromNode("value");
        //                    var work = xd.SelectSingleNode("//input[@id='user_profile_info_employer']").GetAttributeFromNode("value");
        //                    var timeZone = xd.SelectSingleNode("//select[@id='user_preference_time_zone']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
        //                    var imgUrl = "";
        //                    var videoUrl = "";
        //                    var mediaUrl = xd.SelectSingleNode("//a[contains(.,'Photos and Video')]").GetAttributeFromNode("href");
        //                    commPage.RequestURL = mediaUrl;
        //                    ProcessRequest(commPage);
        //                    if (commPage.IsValid)
        //                    {
        //                        xd = commPage.ToXml();
        //                        if (xd != null)
        //                        {
        //                            videoUrl = xd.SelectSingleNode("//video[@id='video_profile']/source").GetAttributeFromNode("src");
        //                            imgUrl = xd.SelectSingleNode("//div[@class='media-photo media-round']/img").GetAttributeFromNode("src");
        //                        }
        //                    }

        //                    profile = new Models.OwnerProfile()
        //                    {
        //                        UserId = userId,
        //                        FirstName = firstName,
        //                        AboutMe = aboutMe,
        //                        BirthDate = birthDate,
        //                        BirthMonth = birthMonth,
        //                        BirthYear = birthYear,
        //                        Currency = currency,
        //                        Email = userEmail,
        //                        Gender = gender,
        //                        ImageUrl = imgUrl,
        //                        Language = language,
        //                        LastName = lastName,
        //                        Phone = userPhone,
        //                        Timezone = timeZone,
        //                        VideoUrl = videoUrl,
        //                        School = school,
        //                        Work = work,
        //                    };
        //                }
        //            }
        //        }
        //        catch (Exception e) { }
        //    }

        //    if (profile != null && !string.IsNullOrEmpty(profile.FirstName))
        //    {
        //        var owner = Core.API.Owner.Add(Database, profile.UserId, profile.FirstName, profile.LastName, profile.Gender, profile.BirthMonth,
        //              profile.BirthDate, profile.BirthYear, profile.Email, profile.Phone, profile.Language, profile.Currency,
        //              profile.AboutMe, profile.School, profile.Work, profile.Timezone, profile.ImageUrl, profile.VideoUrl);
        //    }




        //    return sresult;
        //}

        public BasicUserProfile GetProfile(string token)
        {

            BasicUserProfile profile = null;
            var isLoaded = ValidateTokenAndLoadCookies(token);
            if (isLoaded)
            {
                profile = new Models.BasicUserProfile();
                string url = "https://www.airbnb.com/users/edit";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var userId = xd.SelectSingleNode("//input[@name='user_id']").GetAttributeFromNode("value");
                            var firstName = xd.SelectSingleNode("//input[@id='user_first_name']").GetAttributeFromNode("value");
                            var lastName = xd.SelectSingleNode("//input[@id='user_last_name']").GetAttributeFromNode("value");

                            var imgUrl = "";
                            var mediaUrl = xd.SelectSingleNode("//a[contains(.,'Photos and Video')]").GetAttributeFromNode("href");
                            commPage.RequestURL = mediaUrl;
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                xd = commPage.ToXml();
                                if (xd != null)
                                {
                                    imgUrl = xd.SelectSingleNode("//div[@class='media-photo media-round']/img").GetAttributeFromNode("src");
                                }
                            }

                            profile = new Models.BasicUserProfile()
                            {
                                UserId = userId,
                                FirstName = firstName,
                                ImageUrl = imgUrl,
                                LastName = lastName

                            };
                        }
                    }
                }
                catch (Exception e) { }
            }
            return profile;
        }
        public bool ValidateTokenAndLoadCookies(string token)
        {
            var hasValidCookies = LoadCookies(token);
            if (hasValidCookies)
            {
                BasicUserProfile profile = new Models.BasicUserProfile();

                string url = "https://www.vrbo.com/traveler/profile/edit";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                    {

                        return true;

                    }
                }
                catch (Exception e) { }
            }
            return false;
        }

        public bool LoadCookies(string token)
        {
            var result = false;
            var session = Core.API.UserSession.UserSessionForId(Database, token);
            if (session != null)
            {
                try
                {
                    var cookies = JsonConvert.DeserializeObject<List<Cookie>>(session.Value);
                    CookieCollection cc = new CookieCollection();
                    foreach (var cok in cookies)
                    {
                        cc.Add(cok);
                    }
                    CookieContainer cont = new CookieContainer();

                    cont.Add(cc);
                    commPage.COOKIES = cont;
                    result = true;
                }
                catch (Exception e)
                {

                }
            }
            else
            {
                commPage.COOKIES = null;
            }

            return result;
        }
        public ScrapeResult OwnerProfile(string token, string outputPath)
        {

            var sresult = new ScrapeResult();

            var isLoaded = ValidateTokenAndLoadCookies(token);
            if (isLoaded)
            {


                OwnerProfile profile = new Models.OwnerProfile();

                string url = "https://admin.vrbo.com/haod/up/basic.html";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {

                            var title = xd.SelectSingleNode("//input[@id='title']").GetAttributeFromNode("value");
                            var firstName = xd.SelectSingleNode("//input[@id='firstName']").GetAttributeFromNode("value");
                            var middleName = xd.SelectSingleNode("//input[@id='middleName']").GetAttributeFromNode("value");
                            var lastName = xd.SelectSingleNode("//input[@id='lastName']").GetAttributeFromNode("value");
                            var gender = "";

                            var birthMonth = "";
                            var birthDate = "";
                            var birthYear = "";
                            var userEmail = xd.SelectSingleNode("//input[@id='emailAddress0']").GetAttributeFromNode("value");
                            var userPhone = "";
                            var countryCode = "";
                            var phone = xd.SelectSingleNode("//input[@id='phone0-value']").GetAttributeFromNode("value");
                            var ext = xd.SelectSingleNode("//input[@id='phone0-extension']").GetAttributeFromNode("value");
                            if (!string.IsNullOrEmpty(countryCode))
                            {
                                userPhone = countryCode + "-" + phone;
                            }
                            else
                            {
                                userPhone = phone;
                            }
                            if (!string.IsNullOrEmpty(ext))
                            {
                                userPhone += ("-" + ext);
                            }

                            var language = "";
                            var currency = "";
                            var aboutMe = "";

                            var school = "";
                            var work = "";
                            var timeZone = "";
                            var imgUrl = "";
                            var videoUrl = "";


                            var addressLine1 = xd.SelectSingleNode("//input[@id='address1']").GetAttributeFromNode("value");
                            var addressLine2 = xd.SelectSingleNode("//input[@id='address2']").GetAttributeFromNode("value");
                            var addressLine3 = xd.SelectSingleNode("//input[@id='address3']").GetAttributeFromNode("value");
                            var city = xd.SelectSingleNode("//input[@id='city']").GetAttributeFromNode("value");
                            var state = xd.SelectSingleNode("//input[@id='stateProvince']").GetAttributeFromNode("value");
                            var postCode = xd.SelectSingleNode("//input[@id='postalCode']").GetAttributeFromNode("value");
                            var country = xd.SelectSingleNode("//select[@id='country']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();

                            var result = GetPublicUuidAndUserId(commPage.Html);
                            var publicUuid = result.Item1;
                            var userId = result.Item2;
                            profile = new Models.OwnerProfile()
                            {
                                UserId = userId,
                                FirstName = firstName,
                                AboutMe = aboutMe,
                                BirthDate = birthDate,
                                BirthMonth = birthMonth,
                                BirthYear = birthYear,
                                Currency = currency,
                                Email = userEmail,
                                Gender = gender,
                                ImageUrl = imgUrl,
                                Language = language,
                                LastName = lastName,
                                Phone = userPhone,
                                Timezone = timeZone,
                                VideoUrl = videoUrl,
                                School = school,
                                Work = work,

                                PublicUuId = publicUuid,
                                AddressLine1 = addressLine1,
                                AddressLine2 = addressLine2,
                                AddressLine3 = addressLine3,
                                City = city,
                                State = state,
                                PostalCode = postCode,
                                Country = country,
                            };
                        }
                    }
                }
                catch (Exception e) { }


                if (profile != null && !string.IsNullOrEmpty(profile.FirstName))
                {
                    var owner = Core.API.Owner.Add(Database, profile.UserId, profile.FirstName, profile.LastName, profile.Gender, profile.BirthMonth,
                          profile.BirthDate, profile.BirthYear, profile.Email, profile.Phone, profile.Language, profile.Currency,
                          profile.AboutMe, profile.School, profile.Work, profile.Timezone, profile.ImageUrl, profile.VideoUrl, profile.PublicUuId, profile.AddressLine1, profile.AddressLine2, profile.AddressLine3, profile.City, profile.State, profile.Country, profile.PostalCode);
                }


            }

            return sresult;
        }

        #endregion


        #region CancelReservation
        public Tuple<Dictionary<string, string>, string> GetCancelReservationReasons(string token)
        {
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {



                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/cancellationReasonOptions?_restfully=true&site=vrbo");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var reasons = (JArray)ob["cancellationReasonOptions"];
                        Dictionary<string, string> declineReasons = new Dictionary<string, string>();
                        reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));

                        var reasonVersion = ob["version"].ToSafeString();
                        return new Tuple<Dictionary<string, string>, string>(declineReasons, reasonVersion);

                    }

                }
                catch (Exception e)
                {

                }
            }


            return null;

        }

        public JObject CancelReservationTotals(string quoteGuid, string unitLink)
        {

            try
            {



                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/cancellationRefundTotals?_restfully=true&locale=en_US&quoteGuid={0}&unitLink={1}&override=false&overrideType=NO_OVERRIDE", quoteGuid, unitLink);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                commPage.Referer = "";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob;

                }

            }
            catch (Exception e)
            {

            }



            return null;

        }

        public bool CancelReservation(string token, string conversationId, string cancelReason)
        {
            var result = false;
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", conversationId);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(CommPage.Html);
                        var cancellationPolicies = (JArray)ob["quote"]["cancellationPolicies"];
                        var quoteUuid = ob["quote"]["quoteGuid"].ToSafeString();
                        var unitLink = ob["quote"]["unitLink"].ToSafeString();
                        var reservationId = ob["reservation"]["reservationReferenceNumber"].ToSafeString();


                        var cancelTotals = CancelReservationTotals(quoteUuid, unitLink);

                        string postJsonRaw = string.Format("{{\"overridable\":false,\"unitLink\":\"{0}\",\"quoteUuid\":\"{1}\",\"currencyCode\":\"USD\",\"resId\":\"{2}\",\"hasLodgingTax\":false,\"isPPB\":false,\"damageDepositType\":null,\"damageDepositAutoRefundStatus\":null,\"amountEntered\":0,\"maxRefund\":0,\"maxPartialRefund\":0,\"cancelableAmount\":0,\"paymentScheduleUuid\":null,\"description\":null,\"shouldRefundProducts\":true,\"type\":\"REFUND\",\"cancelReservation\":true,\"cancellationRefundPolicyOverrideType\":\"NO_OVERRIDE\",\"cancellationReason\":\"{3}\",\"cancellationReasonsVersion\":\"ppb1t\"}}", unitLink, quoteUuid, reservationId, cancelReason);

                        var postJson = JObject.Parse(postJsonRaw);
                        postJson.Add("cancellationPolicies", cancellationPolicies);
                        postJson.Add("refundTotals", cancelTotals);


                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/refundAndCancel?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        CommPage.JsonStringToPost = JsonConvert.SerializeObject(postJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        commPage.Referer = "";
                        ProcessRequest(commPage);
                        if (commPage.Html == "" && commPage.StatusCode == HttpStatusCode.OK)
                        {
                            return true;
                        }



                    }
                }

                catch (Exception e)
                { }
            }
            return result;
        }
        #endregion


        public Tuple<string, string> GetPublicUuidAndUserId(string html)
        {
            var publicUuid = "";
            var userId = "";
            try
            {
                var start = commPage.Html.IndexOf("publicuuid: ") + "publicuuid: ".Length;
                var end = commPage.Html.IndexOf(",", start);
                var substring = commPage.Html.Substring(start, end - start).Replace(";", "");


                publicUuid = substring.Replace("this.formatUUID(\"", "").Replace("\"", "").Replace(")", "");
                // userId = ob["userid"].ToSafeString().Replace("this.formatUUID(\"", "").Replace("\"", "");
            }
            catch { }
            try
            {
                var start = commPage.Html.IndexOf("userid:") + "userid:".Length;
                var end = commPage.Html.IndexOf(")", start);
                var substring = commPage.Html.Substring(start, end - start);


                userId = substring.Replace("this.formatUUID(\"", "").Replace("\"", "").Replace(")", "").Trim();
            }
            catch { }

            return new Tuple<string, string>(publicUuid, userId);
        }

        #region Special Offer
        public SpecialOfferSummary GetSpecialOfferSummary(string token, string listingId, string guestId, DateTime startDate, DateTime enddate, int guests)
        {
            var summary = new SpecialOfferSummary();
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    summary.IsAvailable = IsAvailableForDate(listingId, startDate, enddate);
                    if (summary.IsAvailable)
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?_format=for_detailed_booking_info_on_web_p3&_intents=p3_book_it&listing_id={0}&check_in={1}&check_out={2}&guests={3}&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", listingId, startDate.Date.ToString("yyyy-MM-dd"), enddate.Date.ToString("yyyy-MM-dd"), guests);
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        CommPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var amount = ((JArray)ob["pricing_quotes"])[0]["p3_display_rate_with_cleaning_fee"]["amount"].ToSafeString().ToDecimal();
                            summary.Subtotal = amount;

                            commPage.RequestURL = string.Format("https://www.airbnb.com/rooms/pricing/?pricing[start_date]={1}&pricing[end_date]={2}&pricing[hosting_id]={0}&pricing[price]={3}&pricing[is_inline]=true&pricing[guest_id]={4}&pricing[number_of_guests]={5}&pricing[currency]=CAD", listingId, startDate.Date.ToString("yyyy-MM-dd"), enddate.Date.ToString("yyyy-MM-dd"), amount, guestId, guests);
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();

                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            CommPage.RequestHeaders.Add("X-CSRF-Token", token);

                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                summary.GuestPays = ob["guest_pays_native"].ToSafeString().ToDecimal();
                                summary.HostEarns = ob["host_earns_native"].ToSafeString().ToDecimal();
                            }

                        }
                    }
                    return summary;
                }
                catch { }
            }
            return summary;
        }

        public bool SendSpecialOffer(string token, string listingId, string threadId, int guests, DateTime startDate, DateTime enddate, string price)
        {
            var summary = new SpecialOfferSummary();
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    summary.IsAvailable = IsAvailableForDate(listingId, startDate, enddate);
                    if (summary.IsAvailable)
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("authenticity_token", auth_token);
                        commPage.Parameters.Add("pricing[hosting_id]", listingId);
                        commPage.Parameters.Add("pricing[start_date]", startDate.Date.ToString("MM/dd/yyyy"));
                        commPage.Parameters.Add("pricing[end_date]", enddate.Date.ToString("MM/dd/yyyy"));
                        commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                        commPage.Parameters.Add("pricing[unit]", "nightly");
                        commPage.Parameters.Add("template", "2");
                        commPage.Parameters.Add("message", "");
                        commPage.Parameters.Add("pricing[price]", price.ToSafeString());


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            //{"status":"Your message has been sent."}
                            var ob = JObject.Parse(commPage.Html);
                            if (ob != null && ob["status"].ToSafeString() == "Your message has been sent.")
                            {
                                return true;
                            }
                        }
                    }
                }
                catch { }
            }
            return false;
        }

        public string GetRemoveOfferUrl(string id)
        {
            List<InboxMessage> messages = new List<InboxMessage>();
            string url = string.Format("https://www.airbnb.com/z/q/{0}", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            var reservationLink = "";
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);
                var removeOfferUrl = ob["ajax_urls"]["remove_offer"].ToSafeString();
                if (!string.IsNullOrEmpty(removeOfferUrl))
                {
                    removeOfferUrl = "https://www.airbnb.com" + removeOfferUrl;
                    return removeOfferUrl;
                }
            }

            return string.Empty;
        }

        public bool WithdrawSpecialOffer(string url)
        {
            try
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.PersistCookies = true;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("_method", "post");
                commPage.Parameters.Add("authenticity_token", token);

                commPage.Referer = "https://www.airbnb.com/z/q/326933359";
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsolutePath.Contains("/z/q/"))
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }


        #endregion
        #region Shared

        public bool IsAvailableForDate(string listingId, DateTime startDate, DateTime endDate)
        {
            try
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/ajax_special_offer_dates_available?hosting_id={0}&start_date={1}&end_date={2}", listingId, startDate.Date.ToString("yyyy-MM-dd"), endDate.Date.ToString("yyyy-MM-dd"));
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                CommPage.RequestHeaders.Add("X-CSRF-Token", token);

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob["available"].ToSafeString().ToBoolean();
                }
            }
            catch { }
            return false;
        }

        public ListingMetadata GetListingMetadata(string listingId)
        {
            try
            {
                commPage.RequestURL = "https://www.airbnb.ca/rooms/" + listingId;
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {


                        // var json = xd.SelectSingleNode("//script[@data-hypernova-key='p3hero_and_slideshowbundlejs']").GetInnerTextFromNode().HtmlDecode().Trim();
                        var json = xd.SelectSingleNode("//script[@data-hypernova-key='p3hero_and_slideshowbundlejs']").GetInnerTextFromNode().HtmlDecode().Trim();
                        if (string.IsNullOrEmpty(json))
                            json = xd.SelectSingleNode("//script[@data-hypernova-key='p3indexbundlejs']").GetInnerTextFromNode().HtmlDecode().Trim();

                        json = json.Replace("//\r\n", "");
                        json = json.Replace("<!--", "");
                        json = json.Replace("-->", "");
                        var ob = JObject.Parse(json);
                        JToken topNode = null;
                        if (ob["slideshowProps"] != null)
                        {
                            topNode = ob["slideshowProps"];
                        }
                        else
                        {
                            topNode = ob["bootstrapData"]["slideshowProps"];
                        }
                        if (topNode == null)
                        {
                            topNode = ob["bootstrapData"];
                        }
                        var amenitiesArr = (JArray)topNode["listing"]["listing_amenities"];

                        var minStay = topNode["listing"]["min_nights"].ToSafeString();
                        var title = topNode["listing"]["name"].ToSafeString();
                        var nightlyMin = GetPrice(listingId);


                        var hostName = topNode["listing"]["user"]["host_name"].ToSafeString();
                        var hostId = topNode["listing"]["user"]["id"].ToSafeString();
                        var hostProfilePic = topNode["listing"]["user"]["profile_pic_path"].ToSafeString();


                        return new ListingMetadata()
                        {
                            Id = listingId,
                            MinStay = minStay,
                            Title = title.HtmlDecode(),
                            Price = nightlyMin,
                            HostId = hostId,
                            HostName = hostName,
                        };
                    }

                }
            }
            catch (Exception e)
            {

            }
            return null;
        }

        public static Tuple<DateTime, DateTime> ScrapeDates(string dateRange)
        {
            var dates = dateRange.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (dates.Length == 2)
            {
                var year1 = Core.Helpers.Utility.SplitSafely(dates[0], ",", 1);
                var year2 = Core.Helpers.Utility.SplitSafely(dates[1], ",", 1);

                var month1 = Core.Helpers.Utility.SplitSafely(dates[0], " ", 0);
                var date1 = Core.Helpers.Utility.SplitSafely(dates[0], " ", 1).Trim(',');

                var month2 = "";
                var date2 = "";

                var rawDate2WithoutYear = Core.Helpers.Utility.SplitSafely(dates[1], ",", 0);
                var date2parts = rawDate2WithoutYear.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (date2parts.Length == 2)
                {
                    month2 = Core.Helpers.Utility.SplitSafely(rawDate2WithoutYear, " ", 0);
                    date2 = Core.Helpers.Utility.SplitSafely(rawDate2WithoutYear, " ", 1);

                }
                else
                {
                    date2 = Core.Helpers.Utility.SplitSafely(rawDate2WithoutYear, " ", 0);

                }

                if (string.IsNullOrEmpty(year1))
                    year1 = year2;
                if (string.IsNullOrEmpty(month2))
                    month2 = month1;

                var datefrom = string.Format("{0} {1} {2}", month1, date1, year1);
                var dateto = string.Format("{0} {1} {2}", month2, date2, year2);

                var dtfrom = datefrom.ToDateTime();
                var dtto = dateto.ToDateTime();
                return new Tuple<DateTime, DateTime>(dtfrom, dtto);
            }
            return null;
        }


        public List<Tuple<DateTime, DateTime>> GetDateCombinations(DateTime start, DateTime end, int maxDifference)
        {

            var dates = Enumerable.Range(0, 1 + end.Subtract(start).Days)
            .Select(offset => start.AddDays(offset))
            .ToList();

            List<Tuple<DateTime, DateTime>> results = new List<Tuple<DateTime, DateTime>>();
            foreach (var item in dates)
            {
                foreach (var inner in dates)
                {
                    if (item == inner || inner < item)
                    {
                        continue;
                    }

                    if ((inner.Subtract(item).Days) > maxDifference)
                    {
                        break;
                    }

                    results.Add(new Tuple<DateTime, DateTime>(item, inner));
                }
            }
            return results;
        }

        public static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }


        #endregion
    }
}