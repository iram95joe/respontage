﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRBO.Enumerations
{
    public enum InboxThreadType
    {
        All,
        Inquiry,
        //ReservationRequests,
        Reservation,
        Tentative,
        Cancelled,
        CurrentStay,
        PostStay
    }
}
