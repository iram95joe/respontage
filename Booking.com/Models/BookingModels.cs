﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Booking.com.Models
{
    public class LoginResult
    {
        public bool Success { get; set; }
        public string SessionToken { get; set; }
        public AirlockViewModel AirLock { get; set; }
        public string Message { get; set; }
    }

    public class AirlockViewModel
    {
        public string Code { get; set; }
        public Dictionary<string, string> HiddenFields { get; set; }
        public string FormAction { get; set; }
        public Dictionary<string, string> PhoneNumbersCalls { get; set; }
        public Dictionary<string, string> PhoneNumbersSMS { get; set; }
        public AirlockChoice SelectedChoice { get; set; }
        public string SelectedPhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public enum AirlockChoice
    {
        SMS = 1,
        Call = 2,

    }

    public class BasicUserProfile
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
    }

    public class ScrapeResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int TotalRecords { get; set; }
    }

    public class InboxListing
    {
        public string Id { get; set; }
        public string GuestId { get; set; }
        public string GuestFirstname { get; set; }
        public string GuestLastname { get; set; }
        public string ListingId { get; set; }
        public string ListingName { get; set; }
        public string LastMessageAt { get; set; }
        public bool Unread { get; set; }
        public string MessageSnippet { get; set; }
        public bool InquiryOnly { get; set; }
        public string InquiryDate { get; set; }
        public string Status { get; set; }
        public string ReservationCode { get; set; }
        public string StayStartDate { get; set; }
        public string StayEndDate { get; set; }
        public string Type { get; set; }
        public bool IsCancelled { get; set; }
        public int MessageCount { get; set; }
        public int UnreadCount { get; set; }

    }

    public class InboxMessage
    {
        public string Sender { get; set; }
        public string Message { get; set; }
        public string Timestamp { get; set; }
        public string ProfilePath { get; set; }
        public string ProfileImage { get; set; }
        public string MessageId { get; set; }
        public bool IsMyMessage { get; set; }
        public string ProfileId { get; set; }
        public string Type { get; set; }
        public bool IsUnread { get; set; }
    }

    public class Inbox : InboxMessage
    {
        public string Name { get; set; }
        public string ReservationLink { get; set; }

        public List<InboxMessage> Messages { get; set; }

    }

    public class Reservation
    {
        public Reservation()
        { }

        public int HostId { get; set; }
        public string ReservationId { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        public string CheckInDate { get; set; }
        public string CheckoutDate { get; set; }
        public string Nights { get; set; }
        public string ListingName { get; set; }
        public string ListingId { get; set; }
        public string ListingUrl { get; set; }
        public string GuestId { get; set; }
        public string GuestUrl { get; set; }
        public string GuestEmail { get; set; }
        public string GuestPhone { get; set; }
        public string ReservationCost { get; set; }
        public string ReservationCostSummary { get; set; }
        public string CleaningFee { get; set; }
        public string ServiceFee { get; set; }
        public string TotalPayout { get; set; }
        public string InquiryDatetime { get; set; }
        public string ReservationRequestDatetime { get; set; }
        public string ReservationRequestExpiryDatetime { get; set; }
        public string ReservationRequestAcceptedDatetime { get; set; }
        public string ReservationRequestCancelledDatetime { get; set; }
        public string GuestCount { get; set; }
        public int StatusType { get; set; }
        public string StatusDesc { get; set; }
        public bool CanSendSpecialOffer { get; set; }
        public string GuestName { get; set; }
        public bool IsActive { get; set; }
        public List<ReplyAction> ReplyActions { get; set; }
        public string Pets { get; set; }

        public bool CanChange(List<Reservation> reservations, Reservation currentReservation)
        {
            var hasAnyOtherWithActive = reservations.Where(f => f.ReservationId != currentReservation.ReservationId && f.IsActive == true).FirstOrDefault();
            return hasAnyOtherWithActive != null;
        }

        public string ConversationId { get; set; }
        public string ListingUniqueId { get; set; }
        public string RentalCost { get; set; }
        public string TaxRate { get; set; }
        public string TotalTax { get; set; }
        public string RefundableDamageDeposit { get; set; }
        public string Type { get; set; }
        public string GuestCountry { get; set; }
        public string IATACode { get; set; }
        public string Fees { get; set; }
        public string RateDetails { get; set; }
        public string GuestsRawString { get; set; }
    }

    public class ReplyAction
    {
        public string Type { get; set; }
        public Dictionary<string, string> MetaData { get; set; }
    }

    public class ReplyActionCreditCard
    {
        public ReservationActionType ActionType { get; set; }
        public string Name { get; set; }
        public bool IsDisabled { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> MetaData { get; set; }
    }
    
    public enum ReservationActionType
    {
        ChangeReservationDatesAndPrices,
        MarkAsNoShow,
        //We are not going to do this for now
        ReportMisconduct,
        RequestToCancelReservation,
        CC_MarkInvalid,
        CC_ViewDetail,
        CC_UpdatePaymentStatus,
        CC_CancelReservation_BecauseNoNewCardAdded,
        CC_MarkNewCardAdded

    }

    public class ReservationListing
    {
        public string ReferenceNumber { get; set; }
        public string Arrival { get; set; }
        public string Departure { get; set; }
        public string RoomName { get; set; }
        public string BookedOn { get; set; }
        public string Status { get; set; }
    }

    public class SendPaymentDetails
    {
        public string ReservationId { get; set; }

        public long PaymentHistoryId { get; set; }

        public string ConfirmationCode { get; set; }

        public string Customer { get; set; }

        public string CustomerId { get; set; }

        public string Room { get; set; }

        public string TotalAmount { get; set; }

        public string AmountDue { get; set; }

        public DateTime CheckIn { get; set; }

        public DateTime CheckOut { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public string Email { get; set; }

        public string MailSubject { get; set; }

        public string CreditCardNonce { get; set; }

        public string Currency { get; set; }

        public Core.Enumerations.PaymentRequestMethod PaymentMethod { get; set; }
    }
}
