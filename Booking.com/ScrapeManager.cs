﻿//using Core.Database.Context;
//using Core.Extensions;
//using Core.Helper;
//using Core.SignalR.Hubs;
//using BaseScrapper;
//using Booking.com.Models;
//using Microsoft.Win32.SafeHandles;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Drawing;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Reflection;
//using System.Runtime.CompilerServices;
//using System.Runtime.InteropServices;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Web;
//using System.Xml;

//namespace Booking.com
//{
//    public class ScrapeManager : CommunicationEngine, IDisposable
//    {
//        public ScrapeManager()
//        {
//            commPage = new CommunicationPage("https://admin.booking.com/");
//        }

//        public CommunicationPage commPage = null;
//        bool disposed = false;
//        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

//        #region LOGIN

//        public LoginResult Login(string email, string password, bool forceLogin = false, bool clearPreviousSession = true)
//        {
//            var result = new LoginResult();
//            try
//            {
//                if (!forceLogin && commPage != null && commPage.IsLoggedIn)
//                {
//                    result.Success = true;
//                    return result;
//                }
//                string url = "https://admin.booking.com/";
//                if (clearPreviousSession)
//                    commPage = new CommunicationPage(url);
//                else
//                    commPage = InitializeCommunication(email, password);
//                //  commPage.ReqType = CommunicationPage.RequestType.POST;

//                commPage.PersistCookies = true;

//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {

//                    //var chromeOptions = new ChromeOptions();
//                    //chromeOptions.AddArguments(new List<string>() { /*"headless",*/ "–lang=es" });
//                    //var driver = new ChromeDriver(chromeOptions);
//                    //driver.Manage().Window.Size = new Size(800, 600);
//                    //driver.Navigate().GoToUrl("https://admin.booking.com");
//                    //string p = driver.PageSource;
//                    //CookieCollection cc = new CookieCollection();

//                    //foreach (var c in driver.Manage().Cookies.AllCookies)
//                    //{
//                    //    cc.Add(new Cookie(c.Name, c.Value));
//                    //}

//                    //CookieContainer cont = new CookieContainer();
//                    //cont.Add(cc);
//                    //commPage.COOKIES = cont;

//                    //driver.Close();
//                    //driver.Dispose();
//                    //var csrfToken = Helper_GetCsrfToken(p);

//                    XmlDocument xd = commPage.ToXml();
//                    if (xd != null)
//                    {

//                        var tokenValue = Helper_GetCsrfToken();

//                        var formAction = xd.SelectSingleNode("//form[@class='form-box-login__fields']").GetAttributeFromNode("action");
//                        var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "class", "form-box-login__fields");
//                        hiddenFields["loginname"] = email;
//                        hiddenFields["password"] = password.Trim();
//                        hiddenFields["lang"] = "xu";
//                        hiddenFields["csrf_token"] = tokenValue;


//                        // commPage.RequestURL = "https://cas.homeaway.com" + formAction;
//                        commPage.RequestURL = "https://admin.booking.com/";
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        commPage.PersistCookies = true;
//                        commPage.Referer = "";
//                        commPage.Parameters = hiddenFields;
//                        ProcessRequest(commPage);
//                        xd = commPage.ToXml();
//                        if (commPage.IsValid)
//                        {
//                            if (commPage.Uri.AbsoluteUri.Contains("manage/home") || commPage.Uri.AbsoluteUri.Contains("hotel/hoteladmin/index-hotel.html"))
//                            {
//                                #region Logged In

//                                commPage.IsLoggedIn = true;
//                                //var tempCommPage = commPage;
//                                //tempCommPage.Html = "";

//                                System.Collections.Specialized.NameValueCollection qs = System.Web.HttpUtility.ParseQueryString(commPage.Uri.Query.Replace(';', '&'));
//                                var sessionKey = qs["ses"].ToSafeString();
//                                //var propertyId = qs["hotel_id"].ToSafeString();

//                                var cks = GetAllCookies(commPage.COOKIES);
//                                var cookieList = cks.ToList();

//                                var serializedCookies = JsonConvert.SerializeObject(cookieList);
//                                var hostId = Core.API.Booking.com.Hosts.GetHostId(email, password);
//                                var saveResult = Core.API.Booking.com.HostSessionCookies.Add(hostId, serializedCookies, sessionKey, tokenValue);
//                                commPage.Token = saveResult.HostSessionCookie.Token;
//                                result.Success = true;
//                                result.SessionToken = saveResult.HostSessionCookie.Token;

//                                return result;

//                                #endregion
//                            }
//                            else if (commPage.Html.Contains("Select a Verification Method"))
//                            {
//                                #region AirLock

//                                //var lockId = HttpUtility.ParseQueryString(commPage.Uri.Query).Get("al_id");

//                                xd = commPage.ToXml();
//                                var userName = "";
//                                var userProfileImageUrl = "";
//                                var phoneNumbersCall = new Dictionary<string, string>();
//                                var phoneNumbersSMS = new Dictionary<string, string>();

//                                var radioNodes = xd.SelectNodes("//select[@name='phone_id_call']//option");
//                                foreach (XmlNode radio in radioNodes)
//                                {
//                                    var val = radio.GetAttributeFromNode("value");
//                                    var label = radio.NextSibling.GetInnerTextFromNode();
//                                    if (!string.IsNullOrEmpty(label))
//                                        phoneNumbersCall.Add(val, label);
//                                }

//                                var radioNodesSms = xd.SelectNodes("//select[@name='phone_id_sms']//option");
//                                foreach (XmlNode radio in radioNodes)
//                                {
//                                    var val = radio.GetAttributeFromNode("value");
//                                    var label = radio.NextSibling.GetInnerTextFromNode();
//                                    if (!string.IsNullOrEmpty(label))
//                                        phoneNumbersSMS.Add(val, label);
//                                }


//                                var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "select_phone_number");
//                                var authformAction = xd.SelectSingleNode("//form[@id='select_phone_number']").GetAttributeFromNode("action");

//                                var csrf = Helper_GetCsrfToken();
//                                hdnFields["csrf_token"] = csrf;
//                                hdnFields["email"] = email;
//                                result.Success = false;
//                                result.AirLock = new AirlockViewModel()
//                                {
//                                    HiddenFields = hdnFields,
//                                    FormAction = "https://admin.booking.com/hotel/hoteladmin/new_location/" + authformAction,
//                                    PhoneNumbersCalls = phoneNumbersCall,
//                                    // We're going to use PhoneNumberSMS only, because each the same number on call and sms
//                                    // has the same phoneId. (can use either phoneNumberCall or phoneNumberSMS)
//                                    PhoneNumbersSMS = phoneNumbersSMS,
//                                };
//                                return result;

//                                #endregion
//                            }
//                            else if (commPage.IsValid && commPage.Html.Contains("your account has been locked"))
//                            {

//                            }
//                            else if (commPage.IsValid && commPage.Html.Contains("The login name and password you entered"))
//                            {
//                                result.Message = xd.SelectSingleNode("//form[@class='form-box-login__fields']//span[@class='help-block']").GetInnerTextFromNode();
//                            }
//                            else if (commPage.IsValid && commPage.Html.Contains("run out of login attempts"))
//                            {
//                                result.Message = xd.SelectSingleNode("//span[@class='reset-password-step-1']").GetInnerTextFromNode();
//                            }
//                            else
//                            {

//                            }
//                        }
//                        else
//                        {

//                        }
//                    }
//                }
//                else
//                {

//                }
//                result.Success = false;
//            }
//            catch (Exception e)
//            {
//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }
//            return result;
//        }

//        #endregion

//        #region AIRLOCK

//        public AirlockViewModel ProcessAirlockSelection(AirlockViewModel airlockModel)
//        {
//            try
//            {
//                var parameters = airlockModel.HiddenFields;
//                switch (airlockModel.SelectedChoice)
//                {
//                    case AirlockChoice.SMS:
//                        parameters["message_type"] = "sms";
//                        break;
//                    case AirlockChoice.Call:
//                        parameters["message_type"] = "call";

//                        break;

//                }
//                parameters["phone_id"] = airlockModel.SelectedPhoneNumber;
//                parameters["phone_id_call"] = airlockModel.SelectedPhoneNumber;
//                parameters["phone_id_sms"] = airlockModel.SelectedPhoneNumber;

//                commPage.RequestURL = airlockModel.FormAction;
//                commPage.PersistCookies = true;
//                commPage.ReqType = CommunicationPage.RequestType.POST;
//                CommPage.Parameters = parameters;
//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {
//                    if (commPage.Html.Contains("Enter the PIN provided to you"))
//                    {
//                        var xd = commPage.ToXml();
//                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "enter_security_pin");
//                        var authformAction = xd.SelectSingleNode("//form[@id='enter_security_pin']").GetAttributeFromNode("action");
//                        hdnFields["csrf_token"] = Helper_GetCsrfToken();
//                        hdnFields["email"] = parameters["email"].ToSafeString();
//                        airlockModel.HiddenFields = hdnFields;
//                        airlockModel.FormAction = "https://admin.booking.com/hotel/hoteladmin/new_location/" + authformAction;
//                        return airlockModel;
//                    }
//                    else
//                    {

//                    }
//                }
//                else
//                {

//                }
//            }
//            catch (Exception e)
//            {

//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }
//            return airlockModel;

//        }

//        public Tuple<bool, string> ProcessAirlock(AirlockViewModel airlockModel)
//        {
//            try
//            {
//                var parameters = airlockModel.HiddenFields;
//                parameters["ask_pin"] = airlockModel.Code;
//                commPage.RequestURL = airlockModel.FormAction;
//                commPage.PersistCookies = true;
//                commPage.ReqType = CommunicationPage.RequestType.POST;
//                commPage.Parameters = parameters;
//                commPage.RequestHeaders = new Dictionary<string, string>();
//                //commPage.RequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
//                commPage.Referer = airlockModel.FormAction;
//                ProcessRequest(commPage);
//                if (commPage.IsValid &&
//                    (commPage.Html.Contains("You've successfully verified and saved this new device or location")
//                    || commPage.Html.Contains("Add new property") || commPage.Uri.AbsoluteUri.Contains("manage/home")))
//                {
//                    commPage.IsLoggedIn = true;

//                    System.Collections.Specialized.NameValueCollection qs = System.Web.HttpUtility.ParseQueryString(commPage.Uri.Query.Replace(';', '&'));
//                    var sessionKey = qs["ses"].ToSafeString();
//                    //var propertyId = qs["hotel_id"].ToSafeString();
//                    var cks = GetAllCookies(commPage.COOKIES);
//                    var cookieList = cks.ToList();
//                    var serializedCookies = JsonConvert.SerializeObject(cookieList);

//                    bool successScrapeHost = ScrapeHostProfile(airlockModel.Email, airlockModel.Password, true, null, sessionKey);

//                    if (successScrapeHost)
//                    {
//                        var hostId = Core.API.Booking.com.Hosts.GetHostId(airlockModel.Email, airlockModel.Password);
//                        var saveResult = Core.API.Booking.com.HostSessionCookies.Add(hostId, serializedCookies, sessionKey, parameters["csrf_token"].ToString());

//                        commPage.Token = saveResult.HostSessionCookie.Token;
//                        return new Tuple<bool, string>(true, saveResult.HostSessionCookie.Token);
//                    }
//                    else return new Tuple<bool, string>(false, "");

//                }
//            }
//            catch (Exception e)
//            {

//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }

//            return new Tuple<bool, string>(false, "");
//        }

//        #endregion

//        #region HOST PROFILE

//        // if sessionKey has a value, that means the call came from after solving the airlock (ProcessAirlock).
//        public bool ScrapeHostProfile(string username, string password, bool isImporting = false, string token = "",
//            string sessionKey = "")
//        {
//            bool success = false;
//            Core.Database.Entity.HostSessionCookie session;

//            try
//            {
//                // Check if host profile scrapping will be executed if there's no airlock to solve.
//                if (string.IsNullOrEmpty(sessionKey))
//                {
//                    success = ValidateTokenAndLoadCookies(token);
//                    session = Core.API.Booking.com.Hosts.GetHostSession(token);

//                    sessionKey = session.SessionKey;
//                }
//                else
//                    success = true;

//                if (success)
//                {
//                    commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/index-hotel.html?ses={0}&lang=en", sessionKey);
//                    commPage.ReqType = CommunicationPage.RequestType.GET;

//                    ProcessRequest(commPage);

//                    if (commPage.IsValid)
//                    {
//                        var xd = commPage.ToXml();
//                        var firstListingId = xd.SelectSingleNode("//table[@id='group_login_overview']//tbody//tr").Attributes["data-hotel-id"].Value;

//                        // sessionKey and defaultListing will be used in here, if the call came from ProcessAirlock(), then 
//                        // it will initialize a value to parameter, else it will use a database checking just to get the
//                        // sessionKey and defaultListing because you already have an existing host profile.
//                        commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/accounts_and_permissions.html?hotel_id={0}&lang=en&ses={1}", firstListingId, sessionKey);
//                        commPage.ReqType = CommunicationPage.RequestType.GET;

//                        ProcessRequest(commPage);

//                        if (commPage.IsValid)
//                        {
//                            var xm = commPage.ToXml();

//                            var tr = xm.SelectNodes("//table[@class='bui-table js-table-selectable js-table-sort uap-table']//tbody//tr");

//                            foreach (XmlNode xmlNode in tr)
//                            {
//                                var email = xmlNode.SelectSingleNode("td[@data-heading='User']//p//span").Attributes["title"].Value;

//                                if (username == email)
//                                {
//                                    var siteHostId = xmlNode.Attributes["data-user-id"].Value;
//                                    var name = xmlNode.SelectSingleNode("td[@data-heading='User']//span").InnerText;
//                                    var lastname = name.Split(' ').Last();
//                                    var firstname = name.Replace(" " + lastname, "");

//                                    var result = Core.API.Booking.com.Hosts.Add(siteHostId, firstname, lastname, username, password);

//                                    success = true;

//                                    if (isImporting)
//                                        ImportHub.ShowHostName(GlobalVariables.UserId.ToString(), firstname, lastname, "Booking.com");

//                                    break;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                return success;
//            }

//            return success;
//        }

//        #endregion

//        #region PROPERTY

//        public ScrapeResult ScrapeMyListing(string token, int hostId, bool isImporting = false)
//        {
//            var sresult = new ScrapeResult();
//            var isloaded = ValidateTokenAndLoadCookies(token);
//            if (isloaded)
//            {
//                var session = Core.API.Booking.com.Hosts.GetHostSession(token);

//                commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/index-hotel.html?lang=en&ses={0}", session.SessionKey);
//                commPage.PersistCookies = true;
//                commPage.ReqType = CommunicationPage.RequestType.GET;
//                commPage.RequestHeaders = new Dictionary<string, string>();

//                ProcessRequest(commPage);

//                if (commPage.IsValid)
//                {
//                    int count = 1;
//                    var xm = commPage.ToXml();
//                    var listings = xm.SelectNodes("//table[@id='group_login_overview']//tbody//tr");

//                    foreach (XmlNode listing in listings)
//                    {
//                        var listingId = listing.Attributes["data-hotel-id"].Value.ToLong();
//                        var listingUrl = "http://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/index.html?ses={0}&lang=en&hotel_id=" + listingId + "&e_src=index";
//                        var name = listing.Attributes["data-hotel-name"].Value.ToSafeString();
//                        var address = listing.SelectNodes("td")[2].InnerText;

//                        var ld = MyProperty(session.SessionKey, listingId.ToString());
//                        var roomScrapeResult = ScrapeRoom(token, listingId, session.SessionKey);

//                        if (roomScrapeResult.Success)
//                        {
//                            Core.API.Booking.com.Properties.Add(hostId, listingId, ld.ListingUrl, ld.Title, ld.Address, ld.Longitude, ld.Latitude, ld.PropertyType,
//                                ld.MinStay.ToInt(), ld.Internet, ld.Pets, ld.WheelChair, ld.Description, ld.Reviews.ToInt(),
//                                ld.Sleeps, ld.Bedrooms.ToInt(), ld.Bathrooms.ToInt(), ld.AccomodationType, ld.Smoking,
//                                ld.AirCondition, ld.SwimmingPool, ld.Status, null);

//                            if (isImporting)
//                            {
//                                GlobalVariables.ImportedPropertyCount = count;
//                                ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
//                                count++;
//                            }
//                        }
//                        //commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/general_info.html?hotel_id={0}&ses={1}", listingId, session.SessionKey);
//                        //commPage.ReqType = CommunicationPage.RequestType.GET;

//                        //ProcessRequest(commPage);

//                        //if (commPage.IsValid)
//                        //{
//                        //    //HtmlDocument doc = new HtmlDocument();
//                        //    //doc.LoadHtml(commPage.Html);

//                        //    //var element = doc.DocumentNode.SelectNodes("//table[@class='table']//tbody//tr");
//                        //    //var one = element[0].SelectSingleNode("td").InnerText;
//                        //    //var two = element[1].SelectSingleNode("td").InnerText;

//                        //    var xd = commPage.ToHtmlDocument();
//                        //    var info = xd.DocumentNode.SelectNodes("//table//tbody//tr");

//                        //    var asd = info[0].SelectSingleNode("td").InnerText;
//                        //    var asdq = info[1].SelectSingleNode("td").InnerText;
//                        //    var asde = info[2].SelectSingleNode("td").InnerText;
//                        //}
//                    }
//                }
//            }
//            else
//            {
//                sresult.Success = false;
//                sresult.Message = "Could not login, Please check you details and try again.";
//            }

//            return sresult;
//        }

//        private ScrapeResult ScrapeRoom(string token, long listingId, string sessionKey)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);

//            if (isLoaded)
//            {
//                try
//                {
//                    commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/rooms.html?hotel_id={0}&ses={1}&lang=xu", listingId, sessionKey);
//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.GET;

//                    ProcessRequest(commPage);

//                    if (commPage.IsValid)
//                    {
//                        var xm = commPage.ToXml();

//                        var roomNodes = xm.SelectSingleNode("//div[@id='room_summaries']//div[@class='row']").ChildNodes;

//                        using (var db = new ApplicationDbContext())
//                        {
//                            foreach (XmlNode rn in roomNodes)
//                            {
//                                if (rn.Attributes != null && rn.Attributes["id"].Value.Contains("room_summary"))
//                                {
//                                    var name = rn.FirstChild.FirstChild.FirstChild.FirstChild.ChildNodes[0].InnerText;
//                                    var roomId = rn.FirstChild.FirstChild.FirstChild.FirstChild.ChildNodes[1].InnerText.Replace("(", "").Replace(")", " ");

//                                    Core.API.Booking.com.Properties.AddRoom(db, new Core.Database.Entity.Room
//                                    {
//                                        ListingId = listingId,
//                                        RoomId = roomId.ToLong(),
//                                        RoomName = name
//                                    });
//                                }
//                            }
//                        }

//                        return new ScrapeResult() { Success = true, Message = "Rooms successfully scrapped" };
//                    }

//                }
//                catch (Exception e)
//                {

//                }
//            }

//            return new ScrapeResult() { Success = false, Message = "An error occured while scrapping rooms for property " + listingId };
//        }

//        public ListingDetail MyProperty(string sessionKey, string propertyId)
//        {
//            ListingDetail listing = new ListingDetail();

//            MyProperty_GeneralInfo(propertyId, sessionKey, listing);
//            MyProperty_VATTaxCharges(propertyId, sessionKey, listing);
//            MyProperty_Photos(propertyId, sessionKey, listing);
//            MyProperty_FacilitiesAndServices(propertyId, sessionKey, listing);

//            //TODO: Other services  like Rates,Room details, Room amenities
//            // MyProperty_Rates(propertyId, sessionKey, listing);

//            return listing;

//        }

//        public void MyProperty_GeneralInfo(string hotelId, string sessionKey, ListingDetail listing)
//        {
//            try
//            {

//                string url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/general_info.html?lang=xu&hotel_id={0}&ses={1}", hotelId, sessionKey);
//                //listing.ListingUrl = url;
//                commPage.RequestURL = url;
//                commPage.ReqType = CommunicationPage.RequestType.GET;

//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {
//                    var xd = commPage.ToXml();
//                    if (xd != null)
//                    {
//                        listing.Title = xd.SelectSingleNode("//tr[contains(.,'Property name')]/td").GetInnerTextFromNode();
//                        listing.Address = xd.SelectSingleNode("//tr[contains(.,'Property address')]/td").GetInnerTextFromNode();
//                        var rawLocation = xd.SelectSingleNode("//tr[contains(.,'Property location')]/td").GetInnerTextFromNode();
//                        var latlng = ScrapeHelper.SplitSafely(rawLocation, "(", 0);
//                        var arr = latlng.Split(',').ToList();
//                        if (arr.Count == 2)
//                        {
//                            listing.Latitude = arr[0].ToSafeString();
//                            listing.Longitude = arr[1].ToSafeString();
//                        }
//                    }

//                }
//            }
//            catch (Exception e)
//            {

//            }

//        }

//        public void MyProperty_VATTaxCharges(string hotelId, string sessionKey, ListingDetail listing)
//        {
//            //try
//            //{

//            var url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/vat_tax_charges.html?ses={0}&lang=xu&hotel_id={1}", sessionKey, hotelId);
//            //listing.ListingUrl = url;
//            commPage.RequestURL = url;
//            commPage.ReqType = CommunicationPage.RequestType.GET;

//            ProcessRequest(commPage);
//            if (commPage.IsValid)
//            {
//                var xd = commPage.ToXmlWithSGML(commPage.Html);
//                if (xd != null)
//                {
//                    var rows = xd.SelectNodes("//form[@name='price_details']//table//tbody/tr");
//                    Dictionary<string, string> taxAndCharges = new Dictionary<string, string>();
//                    foreach (XmlNode row in rows)
//                    {
//                        var name = row.SelectSingleNode("./td[1]").GetInnerTextFromNode();
//                        var val = row.SelectSingleNode("./td[2]").GetInnerTextFromNode();
//                        if (!string.IsNullOrEmpty(name))
//                        {
//                            taxAndCharges.Add(name, val);
//                        }
//                    }

//                    //TODO-by-Ison
//                    //listing.TaxAndCharges = taxAndCharges;
//                    //Save in DB as a json, because these values are not defined,
//                    //there can be any number/type of charges.
//                }
//            }
//            //}
//            //catch (Exception e)
//            //{

//            //}

//        }

//        public void MyProperty_Photos(string hotelId, string sessionKey, ListingDetail listing)
//        {
//            try
//            {

//                var url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/photos.html?hotel_id={0}&lang=xu&ses={1}", hotelId, sessionKey);
//                commPage.RequestURL = url;
//                commPage.ReqType = CommunicationPage.RequestType.GET;

//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {
//                    var xd = commPage.ToXml();
//                    if (xd != null)
//                    {
//                        List<string> imgsList = new List<string>();
//                        var imgNodes = xd.SelectNodes("//div[@id='photos-content']//div[contains(@class,'js-photo-edit')]");
//                        foreach (XmlNode imgNode in imgNodes)
//                        {
//                            var imgSrc = imgNode.GetAttributeFromNode("data-src").Replace("/square200/", "/max600/");
//                            if (!string.IsNullOrEmpty(imgSrc))
//                                imgsList.Add(imgSrc);


//                        }
//                        listing.Images = string.Join("|", imgsList);




//                    }

//                }
//            }
//            catch (Exception e)
//            {

//            }

//        }

//        public void MyProperty_FacilitiesAndServices(string hotelId, string sessionKey, ListingDetail listing)
//        {
//            //This is half done, need to complete it
//            //Most of the things are done.
//            //This is to be scrapped and saved as json string in the "Services" structure
//            //So, there will be individual columns for each service/area that will contain the Json string
//            //Like there will be coulums like Meals,Languages,FoodAndDrink
//            try
//            {

//                var url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/facilities.html?lang=xu&hotel_id={0}&ses={1}", hotelId, sessionKey);
//                commPage.RequestURL = url;
//                commPage.ReqType = CommunicationPage.RequestType.GET;

//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {
//                    var xd = commPage.ToXml();
//                    if (xd != null)
//                    {

//                        Services Meals = new Services();
//                        Services PriceForMeals = new Services();
//                        Services BreakfastDetails = new Services();
//                        Services LanguagesSpoken = new Services();
//                        Services TopFacilities = new Services();
//                        Services Activities = new Services();
//                        Services FoodAndDrink = new Services();
//                        Services PoolAndSpa = new Services();
//                        Services Transportation = new Services();
//                        Services AdditionalServices = new Services();
//                        Services CommonAreas = new Services();
//                        Services EntertainmentAndFamilyServices = new Services();
//                        Services CleaningServices = new Services();
//                        Services BusinessFacilities = new Services();
//                        Services Shops = new Services();
//                        Services Miscellaneous = new Services();


//                        var mealNodes = xd.SelectNodes("//fieldset[@id='meal_agreeements_section']//div[@class='form-group']");
//                        Meals.Name = "Meals";
//                        foreach (XmlNode item in mealNodes)
//                        {
//                            var label = item.SelectSingleNode(".//label").GetInnerTextFromNode();
//                            var option = item.SelectSingleNode(".//iput[@checked]").GetInnerTextFromNode();
//                            Meals.Options.Add(new ServiceOption { Name = label, Value = option });

//                        }

//                        PriceForMeals.Name = "Price for Meals";
//                        var mealPlansNodes = xd.SelectNodes("//fildset[@id='meal_plans_section']//li");
//                        foreach (XmlNode item in mealPlansNodes)
//                        {
//                            var isChecked = item.SelectSingleNode(".//input[@type='checkbox']").Attributes["checked"] == null;

//                            var name = item.SelectSingleNode(".//div[@class='meal-plan-name']").GetInnerTextFromNode();
//                            var desc = item.SelectSingleNode(".//div[@class='col-sm-4']").GetInnerTextFromNode();
//                            var price = item.SelectSingleNode(".//input[@type='number']").GetAttributeFromNode("value");
//                            var metaData = new Dictionary<string, string>();
//                            metaData.Add("Included in Room Rate", desc);
//                            metaData.Add("Price per person", price);
//                            PriceForMeals.Options.Add(new ServiceOption { Name = name, Value = isChecked.ToSafeString(), MetaData = metaData });

//                        }

//                        BreakfastDetails.Name = "Breakfast Details";

//                        var breakfastNodes = xd.SelectNodes("//fildset[@id='breakfast_section']//div[contains(@class,'breakfast-section')]");
//                        foreach (XmlNode item in breakfastNodes)
//                        {
//                            var labels = item.SelectNodes(".//label");
//                            foreach (XmlNode label in labels)
//                            {
//                                var isButtonList = false;
//                                var nextItem = label.NextSibling;

//                                if (nextItem != null)
//                                {
//                                    var isButtons = nextItem.GetAttributeFromNode("class") == "button-list";
//                                    if (isButtons)
//                                    {
//                                        isButtonList = true;
//                                        var activeButtons = nextItem.SelectNodes(".//div[contains(@class,'active')]");
//                                        foreach (XmlNode btn in activeButtons)
//                                        {
//                                            BreakfastDetails.Options.Add(new ServiceOption { Name = btn.GetInnerTextFromNode() });
//                                        }
//                                        //var selectedButtons = activeButtons.Cast<XmlNode>().Select(f => f.GetInnerTextFromNode()).ToList();
//                                    }
//                                }

//                                if (isButtonList == false)
//                                {
//                                    var days = item.SelectNodes(".//div[contains(@class,'weektimes--item')]");
//                                    foreach (XmlNode dayNode in days)
//                                    {
//                                        var day = dayNode.SelectSingleNode(".//span[@data-var='day']").GetInnerTextFromNode();
//                                        var time = dayNode.SelectSingleNode(".//span[@data-var='time']").GetInnerTextFromNode();
//                                        BreakfastDetails.Options.Add(new ServiceOption { Name = "Day", Value = day });
//                                        BreakfastDetails.Options.Add(new ServiceOption { Name = "Time", Value = time });

//                                    }

//                                }
//                            }
//                        }

//                        LanguagesSpoken.Name = "Languages Spoken";
//                        var languagesDDNodes = xd.SelectNodes("//div[@id='languages-spoken']//div[@class='lang_spoken_line']//select");
//                        foreach (XmlNode item in languagesDDNodes)
//                        {
//                            var selected = item.SelectSingleNode(".//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
//                            if (!string.IsNullOrEmpty(selected))
//                                LanguagesSpoken.Options.Add(new ServiceOption { Name = selected });

//                        }




//                        listing.PropertyType = "Condominium";//xd.SelectSingleNode("//select[@id='propertyTypeDropdown']//option[@selected='selected']").NextSibling.GetInnerTextFromNode();
//                        listing.Bedrooms = xd.SelectNodes("//div[contains(@id,'bedroom-')]").Count.ToSafeString();
//                        listing.Bathrooms = xd.SelectNodes("//div[contains(@id,'bathroom-')]").Count.ToSafeString();
//                        var wheelChairNode = xd.SelectSingleNode("//select[@id='suitabilityFeaturesField_121_dropDown']//option[@selected='selected']");
//                        if (wheelChairNode != null)
//                            listing.WheelChair = wheelChairNode.NextSibling.GetInnerTextFromNode();
//                        else
//                            listing.WheelChair = "ask owner";
//                        listing.Internet = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_135'][@checked='checked']") == null ? "No" : "Yes";
//                        listing.AirCondition = xd.SelectSingleNode("//input[@id='amenitiesFeaturesField_fv_45'][@checked='checked']") == null ? "No" : "Yes";


//                        var acomTypeVacational = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesFieldDefault'][@checked='checked']");
//                        var acomTypeBed = xd.SelectSingleNode("//input[@id='accommodationTypeFeaturesField2'][@checked='checked']");
//                        if (acomTypeVacational == null && acomTypeBed == null)
//                            listing.AccomodationType = "Vacation Rental";
//                        else
//                        if (acomTypeVacational != null)
//                            listing.AccomodationType = "Vacation Rental";

//                        else
//                            listing.AccomodationType = "Bed & Breakfast";

//                        listing.SwimmingPool = xd.SelectNodes("//input[contains(@id,'poolAndSpaFeaturesField_fv_')][@checked='checked']").Count == 0 ? "No" : "Yes";

//                    }

//                }
//            }
//            catch (Exception e)
//            {

//            }

//        }

//        #endregion

//        #region INBOX

//        public ScrapeResult ScrapeInbox(string token, int hostId, bool newOnly, InboxThreadType type, bool isImporting = false)
//        {
//            var sresult = new ScrapeResult();
//            var isloaded = ValidateTokenAndLoadCookies(token);
//            if (isloaded)
//            {
//                var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                var properties = Core.API.Booking.com.Properties.List;

//                try
//                {
//                    foreach (var p in properties)
//                    {
//                        var authKey = AuthInbox(session.SessionKey, p.ListingId.ToString());
//                        List<InboxListing> listings = ScrapeInboxThreadListing(newOnly, authKey, p.ListingId.ToString(), type);
//                        var totalItems = MessageDetailWrapper(hostId, listings, authKey, true, isImporting);
//                        if (totalItems > 0)
//                        {
//                            sresult.Success = true;
//                            sresult.Message = "Operation Successful";
//                            sresult.TotalRecords = totalItems;
//                        }
//                        else
//                        {
//                            if (newOnly)
//                                sresult.Message = "No new message";
//                            else
//                                sresult.Message = "An error occurred.";

//                        }
//                    }
//                }
//                catch (Exception e)
//                {
//                    ElmahLogger.LogError(e, GetCurrentMethod());
//                    sresult.Message = e.Message;
//                }
//            }
//            else
//            {
//                sresult.Message = "Could not login, Please check you details and try again.";
//            }

//            return sresult;
//        }

//        public enum InboxThreadType
//        {
//            All,
//            UnAnswered,
//            //// ReservationRequests,
//            //Reservation,
//            //Tentative,
//            //Cancelled,
//            //CurrentStay,
//            //PostStay
//        }

//        internal string AuthInbox(string sessionKey, string propertyId, CommunicationPage currentCommPage = null)
//        {
//            string authKey = "";
//            try
//            {
//                string url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/messaging_inbox.html?ses={0}&lang=xu&hotel_id={1}", sessionKey, propertyId);
//                if (currentCommPage == null)
//                {
//                    commPage.RequestURL = url;
//                    commPage.ReqType = CommunicationPage.RequestType.GET;
//                    commPage.PersistCookies = true;
//                    commPage.Referer = "";
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid)
//                    {
//                        var start = commPage.Html.IndexOf("intercom_auth\":\"") + "intercom_auth\":\"".Length;
//                        var to = commPage.Html.IndexOf("\"", start);
//                        authKey = commPage.Html.Substring(start, to - start);
//                        authKey = authKey.Trim().TrimEnd("\"".ToChar());
//                        return authKey;
//                    }
//                    else
//                    {

//                    }
//                }
//                else
//                {
//                    currentCommPage.RequestURL = url;
//                    currentCommPage.ReqType = CommunicationPage.RequestType.GET;
//                    currentCommPage.PersistCookies = true;
//                    currentCommPage.Referer = "";
//                    currentCommPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(currentCommPage);
//                    if (currentCommPage.IsValid)
//                    {
//                        var start = currentCommPage.Html.IndexOf("intercom_auth\":\"") + "intercom_auth\":\"".Length;
//                        var to = currentCommPage.Html.IndexOf("\"", start);
//                        authKey = currentCommPage.Html.Substring(start, to - start);
//                        authKey = authKey.Trim().TrimEnd("\"".ToChar());
//                        return authKey;
//                    }
//                    else
//                    {

//                    }
//                }

//            }
//            catch (Exception e)
//            {

//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }
//            return authKey;
//        }

//        internal List<InboxListing> ScrapeInboxThreadListing(bool newOnly, string auth, string propertyId, InboxThreadType threadType, CommunicationPage currentCommPage = null)
//        {
//            //TODO: make the currentcommpage mandatory
//            List<InboxListing> threadList = new List<InboxListing>();
//            try
//            {
//                var pageNumber = 1;
//                var hasMore = false;
//                do
//                {
//                    var json = "";

//                    switch (threadType)
//                    {
//                        case InboxThreadType.All:
//                            json = string.Format("{{\"auth\":[{{\"type\":\"Contextual\",\"auth\":\"{0}\",\"otype\":\"Hotel\",\"oid\":\"{1}\"}}],\"image_sizes\":[\"50\"],\"lang\":\"en-us\",\"presentation\":\"hotel\"}}", auth, propertyId);
//                            break;
//                        case InboxThreadType.UnAnswered:
//                            json = string.Format("{{\"auth\":[{{\"type\":\"Contextual\",\"auth\":\"{0}\",\"otype\":\"Hotel\",\"oid\":\"{1}\"}}],\"image_sizes\":[\"50\"],\"lang\":\"en-us\",\"presentation\":\"hotel\",\"status\":\"pending_property\"}}", auth, propertyId);
//                            break;

//                        default:
//                            break;
//                    }
//                    string url = string.Format("https://chat.booking.com/3/list_recent?json={0}", json.EncodeURL());

//                    hasMore = false;
//                    JObject ob = null;
//                    if (currentCommPage == null)
//                    {
//                        commPage.RequestURL = url;
//                        commPage.ReqType = CommunicationPage.RequestType.GET;
//                        commPage.PersistCookies = true;
//                        commPage.Referer = "";
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        ProcessRequest(commPage);
//                        if (commPage.IsValid)
//                        {
//                            ob = JObject.Parse(commPage.Html);

//                        }

//                        else
//                        {

//                        }
//                    }
//                    else
//                    {
//                        currentCommPage.RequestURL = url;
//                        currentCommPage.ReqType = CommunicationPage.RequestType.GET;
//                        currentCommPage.PersistCookies = true;
//                        currentCommPage.Referer = "";
//                        currentCommPage.RequestHeaders = new Dictionary<string, string>();
//                        ProcessRequest(currentCommPage);
//                        if (currentCommPage.IsValid)
//                        {
//                            ob = JObject.Parse(currentCommPage.Html);

//                        }
//                        else
//                        {

//                        }
//                    }


//                    var ar = (JArray)ob["threads"];
//                    if (ar == null || ar.Count == 0)
//                    {

//                    }
//                    if (ar.Count > 0)
//                    {
//                        //TODO paging


//                        //var currentPage = ob["display"]["page"].ToInt();
//                        //var pageSize = ob["display"]["pageSize"].ToInt();
//                        //var totalResults = ob["display"]["totalResults"].ToInt();

//                        //if ((currentPage * pageSize) < totalResults)
//                        //    hasMore = true;

//                        //pageNumber++;
//                        foreach (var item in ar)
//                        {
//                            var threadNode = item["thread"];
//                            var unreadCount = threadNode["unread_count"].ToInt();
//                            var readStatus = unreadCount > 0 ? true : false;
//                            if (newOnly && readStatus == false)
//                            {
//                                hasMore = false;
//                                break;
//                            }

//                            var id = threadNode["id"].ToSafeString();
//                            var userName = threadNode["context_data"]["booker_name"].ToSafeString();
//                            var userId = item["last_messages"][0]["sender"]["id"].ToSafeString();
//                            var lastMessageAt = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(item["last_messages"][0]["time"].ToSafeString().ToDouble());
//                            var messageSnippet = item["last_messages"][0]["message_preview"].ToSafeString();
//                            var inquiryDateRange = "";

//                            var status = threadNode["status"].ToSafeString();
//                            var listingName = threadNode["context_data"]["hotel_name"].ToSafeString();


//                            var listingId = threadNode["context_data"]["hotel_id"].ToSafeString();
//                            var reservationId = threadNode["context_data"]["hotelreservation_id"].ToSafeString();
//                            var stayStartDate = threadNode["context_data"]["checkin"].ToSafeString();
//                            var stayEndDate = threadNode["context_data"]["checkout"].ToSafeString();
//                            var type = threadNode["topic"].ToSafeString();
//                            var totalMessages = threadNode["message_count"].ToInt();
//                            var isCancelled = threadNode["context_data"]["is_cancelled"].ToBoolean();
//                            var isInquiryOnly = false;
//                            threadList.Add(new InboxListing
//                            {
//                                Id = id,
//                                ListingName = listingName,
//                                GuestId = reservationId + "-guest-sample-id",
//                                GuestFirstname = userName.Replace(" " + userName.Split(' ').Last(), ""),
//                                GuestLastname = userName.Split(' ').Last(),
//                                LastMessageAt = lastMessageAt.ToString(),
//                                Unread = readStatus,
//                                MessageSnippet = messageSnippet,
//                                InquiryOnly = isInquiryOnly,
//                                InquiryDate = inquiryDateRange,
//                                Status = status,
//                                ListingId = listingId,
//                                ReservationCode = reservationId,
//                                StayStartDate = stayStartDate,
//                                StayEndDate = stayEndDate,
//                                Type = type,
//                                MessageCount = totalMessages,
//                                UnreadCount = unreadCount,
//                                IsCancelled = isCancelled,
//                            });
//                        }
//                    }
//                    else
//                    {
//                        hasMore = false;
//                    }

//                } while (hasMore);

//            }
//            catch (Exception e)
//            {

//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }
//            return threadList;
//        }

//        internal int MessageDetailWrapper(int hostId, List<InboxListing> listings, string auth, bool saveToDb, bool isImporting, CommunicationPage currentCommPage = null)
//        {
//            List<Inbox> InboxItems = new List<Inbox>();
//            var count = 1;
//            foreach (var item in listings)
//            {
//                try
//                {
//                    var result = ScrapeMessageDetails(item.Id, auth, currentCommPage);
//                    if (saveToDb)
//                    {
//                        var thread = Core.API.Booking.com.Inboxes.Add(hostId, item.LastMessageAt.ToDateTime(), item.Unread, item.MessageSnippet, item.GuestId, item.GuestFirstname, item.GuestLastname, item.Id, item.InquiryOnly, item.Status, item.StayStartDate.ToDateTime(), item.StayEndDate.ToDateTime(), item.ListingId.ToLong());
//                        if (thread.IsSaved)
//                        {

//                            foreach (var msg in result)
//                            {
//                                var addResult = Core.API.Booking.com.InboxMessages.Add(thread.Thread.ThreadId, msg.MessageId, msg.Message, msg.IsMyMessage, new System.DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(msg.Timestamp.ToDouble()));

//                                if (addResult.IsDuplicate)//all next would be already in db
//                                    break;
//                            }
//                        }
//                    }

//                    if (isImporting)
//                    {
//                        GlobalVariables.ImportedMessagesCount = count;
//                        ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
//                        count++;
//                    }
//                }
//                catch (Exception e)
//                {
//                    ElmahLogger.LogError(e, GetCurrentMethod());

//                }
//            }

//            if (saveToDb)
//                return count; //return InboxItems.Count;
//            else
//                return count;
//        }

//        public List<InboxMessage> ScrapeMessageDetails(string id, string auth, CommunicationPage currentCommPage = null)
//        {
//            List<InboxMessage> messages = new List<InboxMessage>();
//            try
//            {
//                var json = string.Format("{{\"thread\":{{\"type\":\"Contextual\",\"id\":\"{0}\",\"auth\":\"{1}\"}},\"include_childs\":\"1\",\"presentation\":\"hotel\",\"lang\":\"en-us\"}}", id, auth);
//                string url = string.Format("https://chat.booking.com/3/get_messages?json={0}", json.EncodeURL());
//                JObject ob = null;
//                if (currentCommPage == null)
//                {
//                    commPage.RequestURL = url;
//                    commPage.ReqType = CommunicationPage.RequestType.GET;
//                    commPage.PersistCookies = true;
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid)
//                    {
//                        ob = JObject.Parse(CommPage.Html);
//                    }
//                    else
//                    {

//                    }
//                }
//                else
//                {
//                    currentCommPage.RequestURL = url;
//                    currentCommPage.ReqType = CommunicationPage.RequestType.GET;
//                    currentCommPage.PersistCookies = true;
//                    ProcessRequest(currentCommPage);
//                    if (currentCommPage.IsValid)
//                    {
//                        ob = JObject.Parse(currentCommPage.Html);
//                    }
//                    else
//                    {

//                    }
//                }
//                var ar_temp = (JArray)ob["messages"];
//                if (ar_temp == null || ar_temp.Count == 0)
//                {

//                }
//                //  var users = (JArray)ob["appData"]["participantsProfiles"];
//                //participantsProfiles
//                if (ar_temp.Count > 0)
//                {
//                    foreach (var item in ar_temp)
//                    {
//                        var userId = item["sender"]["id"].ToSafeString();
//                        var myMessage = false;

//                        var sender = "";

//                        var profilelink = "";
//                        var profileImagelink = "";
//                        var message = "";
//                        var selectedOptions = (JArray)item["message"]["selected_options"];
//                        if (selectedOptions != null && selectedOptions.Count > 0)
//                        {
//                            message = selectedOptions[0]["input_value"].ToSafeString();
//                            myMessage = true;
//                        }
//                        if (string.IsNullOrEmpty(message))
//                        {
//                            message = item["message"]["text"].ToSafeString();
//                        }
//                        var createdAt = item["time"].ToSafeString();
//                        var messageId = item["message_id"].ToSafeString();
//                        var type = item["message"]["type"].ToSafeString();
//                        var isUnread = item["unread"].ToBoolean();
//                        if (!string.IsNullOrEmpty(message))
//                        {
//                            messages.Add(new InboxMessage { Message = message, Sender = sender, Timestamp = createdAt, ProfilePath = profilelink, ProfileImage = profileImagelink, MessageId = messageId, IsMyMessage = myMessage, ProfileId = userId, Type = type, IsUnread = isUnread });

//                        }
//                    }

//                }

//            }
//            catch (Exception e)
//            {

//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }
//            return messages;
//        }

//        public ScrapeResult SendMessageToInbox(string token, string threadId, string propertyId, string reservationId, string messageText)
//        {
//            var sresult = new ScrapeResult();
//            var isloaded = ValidateTokenAndLoadCookies(token);
//            if (isloaded)
//            {
//                var session = Core.API.Booking.com.Hosts.GetHostSession(token);

//                try
//                {
//                    var authKey = AuthInbox(session.SessionKey, propertyId);


//                    commPage.RequestURL = string.Format("https://chat.booking.com/3/post_message");
//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
//                    commPage.JsonStringToPost = string.Format("{{\"thread\":{{\"type\":\"Contextual\",\"id\":\"{0}\",\"auth\":\"{1}\"}},\"message\":{{\"type\":\"ContextualMessage\",\"sender\":{{\"channel\":\"extranet\",\"type\":\"hotel\",\"hotel_id\":\"{2}\"}},\"command\":\"/start\",\"command_params\":{{\"entry_point\":\"hotel_free_text_to_guest\",\"hotelreservation_id\":\"{3}\"}},\"selected_options\":[{{\"type\":\"PlainText\",\"input_value\":\"{4}\"}}]}}}}", threadId, authKey, propertyId, reservationId, messageText);
//                    commPage.RequestHeaders = new Dictionary<string, string>();

//                    commPage.Referer = "";
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid)
//                    {
//                        var ob = JObject.Parse(commPage.Html);
//                        var msgId = ob["message_id"].ToSafeString();
//                        if (!string.IsNullOrEmpty(msgId))
//                        {
//                            sresult.Success = true;

//                        }
//                    }
//                    else
//                    {

//                    }

//                }
//                catch (Exception e)
//                {
//                    sresult.Message = e.Message;
//                    ElmahLogger.LogError(e, GetCurrentMethod());
//                }
//            }
//            else
//            {
//                sresult.Message = "Could not login, Please check you details and try again.";
//            }

//            return sresult;
//        }

//        #endregion

//        #region RESERVATIONS

//        private decimal ExtractNumberAsDecimal(string number)
//        {
//            decimal newNumber = Convert.ToDecimal(Regex.Replace(number, "[^0-9.]", ""));
//            return newNumber;
//        }

//        public List<Reservation> ScrapeReservations(string token, int hostId, SyncType syncType, bool isImporting = false)
//        {
//            int count = 1;

//            var sresult = new ScrapeResult();
//            List<Reservation> reservationList = new List<Reservation>();
//            var isloaded = ValidateTokenAndLoadCookies(token);
//            if (isloaded)
//            {
//                var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                var properties = Core.API.Booking.com.Properties.List;

//                try
//                {
//                    foreach (var p in properties)
//                    {
//                        if (p.ListingId == 3138722)
//                        {

//                        }

//                        List<ReservationListing> listing = new List<Models.ReservationListing>();
//                        var today = DateTime.UtcNow;
//                        Tuple<bool, List<ReservationListing>> listingResult = null;
//                        //string csrf = Helper_GetCsrfToken(true, token, session.SessionKey);

//                        using (var db = new ApplicationDbContext())
//                        {
//                            do
//                            {
//                                var dateTo = today.AddDays(-60);
//                                if (syncType == SyncType.All)
//                                {
//                                    dateTo = today.AddYears(1);
//                                }
//                                listingResult = ReservationListing(db, token, session.SessionKey, syncType, p.ListingId.ToString(), today, dateTo);
//                                listing.AddRange(listingResult.Item2);
//                                today = dateTo;
//                            } while (listingResult.Item1 == true);
//                        }

//                        foreach (var thread in listing)
//                        {
//                            var detailUrl = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?lang=xu&ses={0}&hotel_id={1}&res_id={2}", session.SessionKey, p.ListingId, thread.ReferenceNumber);
//                            commPage.RequestURL = detailUrl;
//                            commPage.ReqType = CommunicationPage.RequestType.GET;
//                            commPage.PersistCookies = true;
//                            ProcessRequest(commPage);
//                            if (commPage.IsValid)
//                            {
//                                var xd = commPage.ToXml();
//                                #region Reply Actions

//                                List<ReplyAction> actions = new List<ReplyAction>();

//                                //var ar = (JArray)ob["replyActions"];
//                                //if (ar.Count > 0)
//                                //{
//                                //    foreach (var item in ar)
//                                //    {
//                                //        var actionType = item["type"].ToSafeString();
//                                //        if (actionType == "NONE")
//                                //        {
//                                //            actionType = item["buttonTitle"].ToSafeString();
//                                //        }
//                                //        ReplyAction act = new ReplyAction
//                                //        {
//                                //            Type = actionType
//                                //        };
//                                //        if (actionType == "DECLINE_BOOKING")
//                                //        {
//                                //            var reasons = (JArray)item["declineReasons"];
//                                //            Dictionary<string, string> declineReasons = new Dictionary<string, string>();
//                                //            reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));
//                                //            act.MetaData = declineReasons;
//                                //        }
//                                //        actions.Add(act);
//                                //    }
//                                //}
//                                //if (!actions.Any(f => f.Type.Contains("EDIT") || f.Type.Contains("ADD")))
//                                //{
//                                //    if (thread.Status.Contains("TENTATIVE"))
//                                //    {

//                                //        actions.Add(new ReplyAction { Type = "EDIT_BOOKING" });
//                                //    }
//                                //}


//                                //if (ob["paymentDetails"]["quoteSection"]["editable"].ToBoolean() == true)
//                                //    actions.Add(new ReplyAction { Type = "EDIT_QUOTE" });
//                                #endregion
//                                var guestsString = xd.SelectSingleNode("//div[@class='res-detail-card']//div[@class='bks-item' and contains(.,'Total guests:')]/span[@class='bui_font_body']").GetInnerTextFromNode();
//                                var guests = guestsString.ToInt();
//                                var nights = xd.SelectSingleNode("//div[@class='res-detail-card']//div[@class='bks-item' and contains(.,'Length of stay:')]/div[@class='bui_font_body']").GetInnerTextFromNode().Replace("nights", "").Replace("night", "").ToInt();
//                                //var rentalCost = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//tr[@class='js-stay-date-row ']/td[3]").GetInnerTextFromNode();
//                                var quoteSubTotal = "";
//                                var sumPerNight = quoteSubTotal = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//tr[contains(.,'Subtotal')]/td[3]").GetInnerTextFromNode();
//                                var taxAmount = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//tr[contains(.,'Tax')]/td[3]").GetInnerTextFromNode();
//                                var taxRate = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//tr[contains(.,'Tax')]/td[2]").GetInnerTextFromNode();
//                                var quoteTotal = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//tr[@class='total']/td[3]").GetInnerTextFromNode();

//                                // var payableToHost = xd.SelectSingleNode("//div[@class='guest-detail']//div[@class='bks-item' and contains(.,'Commissionable amount:')]/span[@class='bui_font_body']").GetInnerTextFromNode();

//                                var reservationCreatedAt = thread.BookedOn;
//                                var reservationId = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]").GetAttributeFromNode("data-room-reservation-id");
//                                var guestName = xd.SelectSingleNode("//div[@data-id='booker_info']/span[@class='bui_font_display_one bhpb_guest_name_float']").GetInnerTextFromNode();
//                                var guestCountry = xd.SelectSingleNode("//div[@data-id='booker_info']/span[@class='bui_font_caption']").GetInnerTextFromNode();

//                                var summary = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//table[@class='table pricing bui_font_caption']").GetOuterXML();
//                                var reservationDeclineDateTime = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//div[@class='room-details-info-block' and contains(.,'Canceled')]//time").GetInnerTextFromNode();

//                                var email = xd.SelectSingleNode("//div[@class='guest-detail']//a[@id='email']").GetInnerTextFromNode();

//                                var phone = xd.SelectSingleNode("//div[@class='guest-detail']//a[contains(@class,'rtl_fix_phone')]").GetInnerTextFromNode();

//                                var IATACode = xd.SelectSingleNode("//div[@class='guest-detail']//div[@class='bks-item' and contains(.,'IATA/TIDS code:')]/span[@class='bui_font_body']").GetInnerTextFromNode();
//                                var serviceFee = xd.SelectSingleNode("//div[@class='guest-detail']//div[@class='bks-item' and contains(.,'Commission:')]/span[@class='bui_font_body']").GetInnerTextFromNode();

//                                var rateDetails = new List<Core.Models.ReservationRates>();
//                                var rentalNodes = xd.SelectNodes("//div[contains(@class,'room-info-row js-room-details')]//tr[@class='js-stay-date-row ']");
//                                if (rentalNodes != null)
//                                {
//                                    foreach (XmlNode renNode in rentalNodes)
//                                    {
//                                        var dt = renNode.GetAttributeFromNode("data-date").ToSafeString();
//                                        var rateid = renNode.GetAttributeFromNode("data-rate-id").ToSafeString();
//                                        var dateString = renNode.SelectSingleNode("./td[1]").GetInnerTextFromNode();
//                                        var rateName = renNode.SelectSingleNode("./td[2]").GetInnerTextFromNode();
//                                        var price = renNode.SelectSingleNode("./td[3]").GetInnerTextFromNode();
//                                        rateDetails.Add(new Core.Models.ReservationRates() { Date = dt, RateId = rateid, Price = price, RateName = rateName, DateString = dateString });
//                                    }
//                                }
//                                var fees = new List<Core.Models.ReservationFee>();
//                                var lastRateNode = xd.SelectSingleNode("//div[contains(@class,'room-info-row js-room-details')]//tr[@class='js-stay-date-row '][last()]");
//                                for (int i = 0; i < 20; i++)
//                                {
//                                    var sibling = lastRateNode.NextSibling;
//                                    lastRateNode = sibling;
//                                    if (sibling.GetInnerTextFromNode().ToLower().Contains("subtotal"))
//                                        continue;
//                                    //if (sibling.SelectSingleNode("./td[1]").GetInnerTextFromNode().ToLower().Equals("tax"))
//                                    //    continue;
//                                    if (sibling.GetAttributeFromNode("class").Equals("total"))
//                                        break;
//                                    var feeName = sibling.SelectSingleNode("./td[1]").GetInnerTextFromNode().ToSafeString();
//                                    var rate = sibling.SelectSingleNode("./td[2]").GetInnerTextFromNode().ToSafeString();
//                                    var price = sibling.SelectSingleNode("./td[3]").GetInnerTextFromNode().ToSafeString();

//                                    if (!string.IsNullOrEmpty(feeName))
//                                        fees.Add(new Core.Models.ReservationFee() { FeeName = feeName, Price = price, Rate = rate });

//                                }

//                                var payableToHost = quoteTotal.ExtractNumberAsInt() - serviceFee.ExtractNumberAsInt();

//                                var auth = AuthReservation(session.SessionKey, p.ListingId.ToString(), thread.ReferenceNumber);
//                                var conversation = InquiryConversation(auth, thread.ReferenceNumber, guestName, hostId,
//                                    thread.Arrival.ToDateTime(), thread.Departure.ToDateTime(), p.ListingId);

//                                // Total Room Price = reservationCost
//                                // Subtotal = sumPerNight
//                                // TotalPayout = reservationCost - serviceFee

//                                var reservation = new Reservation
//                                {
//                                    HostId = hostId,

//                                    CheckInDate = thread.Arrival,
//                                    CheckoutDate = thread.Departure,
//                                    Code = thread.ReferenceNumber,

//                                    ServiceFee = ExtractNumberAsDecimal(serviceFee),
//                                    CleaningFee = "",
//                                    // TotalPayout = reservationCost - serviceFee. Pending because of serviceFee, reservationCost for now
//                                    TotalPayout = ExtractNumberAsDecimal(quoteTotal),//.ExtractNumberAsString(),//payableToHost.ToSafeString(),//quoteTotal,
//                                    ReservationCost = ExtractNumberAsDecimal(quoteTotal), //reservationCost,
//                                    RefundableDamageDeposit = "",//refundableDamageDeposit,
//                                    SumPerNight = ExtractNumberAsDecimal(sumPerNight),
//                                    TaxRate = taxRate,
//                                    TotalTax = ExtractNumberAsDecimal(taxAmount).ToString(),
//                                    ReservationCostSummary = summary,


//                                    Nights = nights.ToSafeString(),
//                                    GuestCount = guests.ToSafeString(),
//                                    GuestEmail = email,
//                                    GuestPhone = phone,
//                                    ListingName = thread.RoomName,
//                                    GuestName = guestName,
//                                    GuestId = conversation.Item2,
//                                    InquiryDatetime = thread.BookedOn,
//                                    ReservationId = reservationId,
//                                    Status = (thread.Status == "OK" ? "A" : "C"),
//                                    IsActive = false,//NOT KNOWN
//                                    ReplyActions = actions,
//                                    Pets = "",
//                                    ReservationRequestDatetime = reservationCreatedAt,
//                                    ReservationRequestExpiryDatetime = "",
//                                    ReservationRequestAcceptedDatetime = "",
//                                    ReservationRequestCancelledDatetime = reservationDeclineDateTime,
//                                    ConversationId = conversation.Item1,
//                                    ListingUniqueId = "",
//                                    ListingId = p.ListingId.ToString(),

//                                    //Type = thread.Type // we dont have a type here. we only have bookings here, no inquity etc
//                                    Fees = JsonConvert.SerializeObject(fees),
//                                    IATACode = IATACode,
//                                    RateDetails = JsonConvert.SerializeObject(rateDetails),
//                                    GuestCountry = guestCountry,
//                                    GuestsRawString = guestsString,

//                                };

//                                if (reservation.ConversationId == "f229ce9a-ca0e-43bf-aae3-1afa5a6ca194")
//                                {

//                                }

//                                if (reservation.CheckInDate.ToDateTime().ToString("yyyy-MM-dd") == "2018-09-26")
//                                {
//                                }

//                                var addResult = Core.API.Booking.com.Inquiries.Add(reservation.HostId, reservation.Code, reservation.ReservationId,
//                                    reservation.Status, reservation.CheckInDate.ToDateTime(), reservation.CheckoutDate.ToDateTime(), reservation.Nights.ToInt(),
//                                    reservation.ListingId.ToLong(), reservation.GuestId, reservation.ReservationCost, reservation.SumPerNight, reservation.CleaningFee,
//                                    reservation.ServiceFee, reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(),
//                                    reservation.ReservationRequestDatetime.ToDateTime(), reservation.GuestCount.ToInt(), 0, 0,
//                                    reservation.ReservationRequestAcceptedDatetime.ToDateTime(), reservation.ReservationRequestCancelledDatetime.ToDateTime(),
//                                    reservation.ConversationId, reservation.Type, reservation.Fees, reservation.RateDetails);

//                                if (!Core.API.Booking.com.Inquiries.HasCreditCardInformation(reservation.Code) ||
//                                    reservation.CheckInDate.ToDateTime().ToString("yyyy-MM-dd") == "2018-09-26")
//                                {
//                                    var host = Core.API.Booking.com.Hosts.GetHostUsernameAndPassword(hostId);
//                                    GetCreditCardDetails(token, p.ListingId.ToString(), reservation.Code, host.Item1, host.Item2);
//                                }

//                                if (isImporting)
//                                {
//                                    GlobalVariables.ImportedBookingCount = count;
//                                    ImportHub.UpdateProgress(GlobalVariables.UserId.ToString());
//                                    count++;
//                                }

//                                if (syncType == SyncType.New && addResult.IsDuplicate)
//                                {
//                                    break;
//                                }
//                                else
//                                {
//                                    reservationList.Add(reservation);
//                                }
//                            }
//                        }

//                        var totalItems = reservationList.Count();
//                        if (totalItems > 0)
//                        {
//                            sresult.Success = true;
//                            sresult.Message = "Operation Successful";//, file generated at " + outputPath;
//                            sresult.TotalRecords = totalItems;
//                        }
//                        else
//                        {
//                            sresult.Message = "An error occurred.";

//                        }
//                    }
//                }
//                catch (Exception e)
//                {
//                    sresult.Message = e.Message;
//                }
//            }

//            else
//            {
//                sresult.Message = "Could not login, Please check you details and try again.";
//            }

//            return reservationList;
//        }

//        public Tuple<bool, List<ReservationListing>> ReservationListing(ApplicationDbContext db, string token, string sessionKey, SyncType syncType, string listingId, DateTime from, DateTime to)
//        {
//            bool haveMore = true;
//            List<ReservationListing> reservationList = new List<ReservationListing>();

//            try
//            {
//                var page = 1;

//                //commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/search_reservations.html?perpage=50&lang=xu&ses={0}&hotel_id={1}&type=arrival&stay_from={2}&stay_to={3}", sessionKey, listingId, from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));
//                //commPage.ReqType = CommunicationPage.RequestType.GET;
//                //commPage.RequestHeaders = new Dictionary<string, string>();
//                //commPage.PersistCookies = true;

//                //ProcessRequest(commPage);


//                //var chromeOptions = new ChromeOptions();
//                //chromeOptions.AddArguments(new List<string>() { /*"headless",*/ "–lang=es" });
//                //var driver = new ChromeDriver(chromeOptions);
//                //driver.Manage().Window.Size = new Size(800, 600);
//                //driver.Navigate().GoToUrl("https://admin.booking.com");
//                //LoadCookies(token, driver);
//                //driver.Navigate().GoToUrl(commPage.RequestURL);
//                //string p = driver.PageSource;
//                //driver.Close();
//                //driver.Dispose();
//                //var csrf = Helper_GetCsrfToken(p);

//                string url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/search_reservations.html?perpage=50&lang=xu&ses={0}&hotel_id={1}&type=arrival&stay_from={2}&stay_to={3}", sessionKey, listingId, from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));
//                do
//                {
//                    commPage.RequestURL = url;
//                    commPage.PersistCookies = true;
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    commPage.ReqType = CommunicationPage.RequestType.GET;
//                    //commPage.RequestHeaders = new Dictionary<string, string>();
//                    //commPage.RequestHeaders.Add("X-Booking-CSRF", csrf);

//                    ProcessRequest(commPage);

//                    url = string.Empty;
//                    if (commPage.IsValid)
//                    {
//                        var xd = commPage.ToXml();
//                        var rows = xd.SelectNodes("//div[@class='reservation-list']//table/tr");
//                        if (rows != null)
//                        {
//                            foreach (XmlNode row in rows)
//                            {
//                                var cols = row.SelectNodes(".//td");
//                                if (cols != null)
//                                {
//                                    var referenceNumber = cols[8].GetInnerTextFromNode();
//                                    Core.Database.Entity.Inquiry existing = null;
//                                    if (syncType == SyncType.New)
//                                        existing = Core.API.Booking.com.Inquiries.InquiryForConversationId(db, referenceNumber);
//                                    if (existing != null)
//                                    {
//                                        haveMore = false;
//                                        break;
//                                    }
//                                    else
//                                    {
//                                        ReservationListing rl = new Models.ReservationListing();
//                                        rl.Arrival = cols[1].GetInnerTextFromNode();
//                                        rl.Departure = cols[2].GetInnerTextFromNode();
//                                        rl.RoomName = cols[3].GetInnerTextFromNode();
//                                        rl.BookedOn = cols[4].GetInnerTextFromNode();
//                                        rl.Status = cols[5].GetInnerTextFromNode();
//                                        rl.ReferenceNumber = referenceNumber;
//                                        reservationList.Add(rl);
//                                    }
//                                }
//                            }
//                        }

//                        var nextNode = xd.SelectSingleNode("//ul[@class='pagination']//li//a[@aria-label='Next']");
//                        if (nextNode != null)
//                        {
//                            var nextClass = nextNode.ParentNode.GetAttributeFromNode("class");
//                            if (nextClass != "hidden")
//                            {
//                                url = nextNode.GetAttributeFromNode("href");
//                                url = "https://admin.booking.com" + url;
//                                page++;
//                            }
//                        }
//                    }


//                } while (!string.IsNullOrEmpty(url));
//                if (page < 2)
//                {
//                    haveMore = false;
//                }

//            }
//            catch (Exception e)
//            { }

//            return new Tuple<bool, List<Models.ReservationListing>>(haveMore, reservationList);
//        }

//        internal string AuthReservation(string sessionKey, string listingId, string reservationId, CommunicationPage currentCommPage = null)
//        {
//            string authKey = "";
//            try
//            {
//                string url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?hotel_id={0}&res_id={1}&from_groups=1&ses={2}&lang=xu", listingId, reservationId, sessionKey);
//                if (currentCommPage == null)
//                {
//                    commPage.RequestURL = url;
//                    commPage.ReqType = CommunicationPage.RequestType.GET;
//                    commPage.PersistCookies = true;
//                    commPage.Referer = "";
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid)
//                    {
//                        var start = commPage.Html.IndexOf("intercom_auth\":\"") + "intercom_auth\":\"".Length;
//                        var to = commPage.Html.IndexOf("\"", start);
//                        authKey = commPage.Html.Substring(start, to - start);
//                        authKey = authKey.Trim().TrimEnd("\"".ToChar());
//                        return authKey;
//                    }
//                    else
//                    {

//                    }
//                }
//                else
//                {
//                    currentCommPage.RequestURL = url;
//                    currentCommPage.ReqType = CommunicationPage.RequestType.GET;
//                    currentCommPage.PersistCookies = true;
//                    currentCommPage.Referer = "";
//                    currentCommPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(currentCommPage);
//                    if (currentCommPage.IsValid)
//                    {
//                        var start = currentCommPage.Html.IndexOf("intercom_auth\":\"") + "intercom_auth\":\"".Length;
//                        var to = currentCommPage.Html.IndexOf("\"", start);
//                        authKey = currentCommPage.Html.Substring(start, to - start);
//                        authKey = authKey.Trim().TrimEnd("\"".ToChar());
//                        return authKey;
//                    }
//                    else
//                    {

//                    }
//                }

//            }
//            catch (Exception e)
//            {

//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }
//            return authKey;
//        }

//        internal Tuple<string, string> InquiryConversation(string auth, string reservationId, string guestName, int hostId,
//            DateTime checkInDate, DateTime checkOutDate, long listingId)
//        {
//            string guestFirstname = guestName.Replace(" " + guestName.Split(' ').Last(), "");
//            string guestLastname = guestName.Split(' ').Last();
//            try
//            {
//                var json = string.Format("{{\"auth\":\"{0}\",\"reservation_ids\":[\"{1}\"],\"presentation\":\"hotel\"}}", auth, reservationId);

//                string url = string.Format("https://chat.booking.com/3/find_thread?json={0}", json.EncodeURL());

//                commPage.RequestURL = url;
//                commPage.ReqType = CommunicationPage.RequestType.GET;
//                commPage.PersistCookies = true;
//                commPage.Referer = "";
//                commPage.RequestHeaders = new Dictionary<string, string>();
//                ProcessRequest(commPage);

//                if (commPage.IsValid)
//                {
//                    var ob = JObject.Parse(commPage.Html);
//                    string guestId = reservationId + "-guest-sample-id";//ob["threads"][0]["last_messages"][0]["recipient"]["id"].ToSafeString();
//                    string threadId = ob["threads"][0]["thread"]["id"].ToSafeString();

//                    int count = ob["threads"][0]["thread"]["message_count"].ToInt();

//                    // if no messages in a reservation, manually create an inbox in the database
//                    if (count == 0)
//                    {
//                        Core.API.Booking.com.Inboxes.Add(hostId, DateTime.UtcNow, false, "", reservationId + "-guest-sample-id",
//                            guestFirstname, guestLastname, threadId, false, "",
//                            checkInDate, checkOutDate, listingId);
//                    }


//                    return new Tuple<string, string>(threadId, guestId);
//                }

//            }
//            catch (ArgumentOutOfRangeException e)
//            {
//                Core.API.Booking.com.Guests.Add(reservationId + "-guest-sample-id", guestFirstname, guestLastname);
//                //Core.API.Booking.com.Inboxes.Add(hostId, DateTime.UtcNow, false, "", reservationId + "-guest-sample-id",
//                //    guestFirstname, guestLastname, reservationId + "-inbox-thread-sample-id", false, "",
//                //    checkInDate, checkOutDate, listingId);

//                return new Tuple<string, string>(reservationId + "-inbox-thread-sample-id",
//                    reservationId + "-guest-sample-id");
//            }
//            catch (Exception error)
//            {
//                ElmahLogger.LogError(error, GetCurrentMethod());
//            }

//            return new Tuple<string, string>("", "");
//        }

//        public enum SyncType
//        {
//            All,
//            New,
//        }

//        #region BOOKING ACTIONS

//        public void GetCreditCardDetails(string token, string listingId, string reservationId, string userName, string password)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);
//            if (isLoaded)
//            {
//                try
//                {
//                    using (var db = new ApplicationDbContext())
//                    {
//                        var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                        var csrfToken = Helper_GetCSRFToken(session.SessionKey, listingId, reservationId);
//                        commPage.RequestURL = string.Format("https://secure-admin.booking.com/authenticate.html");

//                        commPage.PersistCookies = true;
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                        commPage.Parameters = new Dictionary<string, string>();
//                        commPage.Parameters.Add("url_to_redirect", string.Format("booking_cc_details.html?bn={0}&hotel_id={1}", reservationId, listingId));
//                        commPage.Parameters.Add("hotel_id", listingId);
//                        commPage.Parameters.Add("res_id", reservationId);
//                        commPage.Parameters.Add("hotel_currencycode", "CAD");
//                        commPage.Parameters.Add("loginname", userName);
//                        commPage.Parameters.Add("password", password);
//                        commPage.Parameters.Add("login", "View credit card details");

//                        // commPage.Parameters.Add("csrf_token", csrfToken);
//                        commPage.Referer = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?ses={0}&lang=xu&hotel_id={1}&res_id={2}", session.SessionKey, listingId, reservationId);
//                        ProcessRequest(commPage);
//                        if (commPage.IsValid)
//                        {
//                            var xd = commPage.ToXml();
//                            if (xd != null)
//                            {
//                                var desc = xd.SelectSingleNode("//div[@class='row-fluid']//h2").GetInnerTextFromNode();
//                                var cardType = xd.SelectSingleNode("//tr[contains(.,'Card type:')]/td[2]").GetInnerTextFromNode();
//                                var cardNumber = xd.SelectSingleNode("//tr[contains(.,'Card number:')]/td[2]").GetInnerTextFromNode();
//                                var cardHolderName = xd.SelectSingleNode("//tr[contains(.,'Card holder')]/td[2]").GetInnerTextFromNode();
//                                var cardExpiry = xd.SelectSingleNode("//tr[contains(.,'Expiration Date:')]/td[2]").GetInnerTextFromNode();
//                                var cardCVC = xd.SelectSingleNode("//tr[contains(.,'CVC Code:')]/td[2]").GetInnerTextFromNode();

//                                //This will update the view limit etc
//                                var isAdded = Core.API.Booking.com.CreditCards.AddCardDetails(db, reservationId, cardType, cardNumber, cardHolderName, cardExpiry, cardCVC);

//                                if (isAdded)
//                                {
//                                    UpdateReservationActionData(db, session.SessionKey, reservationId, listingId);
//                                }
//                            }
//                        }
//                    }
//                }
//                catch (Exception e)
//                {

//                }
//            }
//        }

//        public Tuple<bool, string> MarkCreditCardAsInvalid(string token, string listingId, string reservationId, string cardLast4Digits, string reasonId, string otherReasonDescription)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);
//            if (isLoaded)
//            {
//                try
//                {
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                    var transactionId = TransactionId(session.SessionKey, listingId, reservationId);

//                    commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/checkcc_bibit.html?trans_id={0}&ajax=1&hotel_id={1}&lang=xu&ses={2}", transactionId, listingId, session.SessionKey);
//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.GET;
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(commPage);

//                    if (commPage.IsValid)
//                    {
//                        commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/checkcc_bibit.html");

//                        commPage.PersistCookies = true;
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                        commPage.Parameters = new Dictionary<string, string>();
//                        commPage.Parameters.Add("ses", session.SessionKey);
//                        commPage.Parameters.Add("trans_id", transactionId);
//                        commPage.Parameters.Add("hotel_id", listingId);
//                        commPage.Parameters.Add("period", "");
//                        commPage.Parameters.Add("selected_period", "");
//                        commPage.Parameters.Add("order", "");
//                        commPage.Parameters.Add("type", "");
//                        commPage.Parameters.Add("ccnumber", cardLast4Digits);
//                        commPage.Parameters.Add("cc_invalid_reason", reasonId);
//                        commPage.Parameters.Add("other_reason", otherReasonDescription);
//                        commPage.Parameters.Add("ajax", "1");
//                        commPage.Parameters.Add("check", "Confirm");


//                        commPage.Referer = "";
//                        ProcessRequest(commPage);
//                        if (commPage.IsValid && commPage.Html.Contains("We'll ask the guest to provide new credit card details"))
//                        {
//                            using (var db = new ApplicationDbContext())
//                            {
//                                UpdateReservationActionData(db, session.SessionKey, reservationId, listingId);
//                                return new Tuple<bool, string>(true, "We'll ask the guest to provide new credit card details within 24 hours. ");
//                            }
//                        }
//                    }

//                }
//                catch (Exception e)
//                {
//                    return new Tuple<bool, string>(false, "An error occured. Something went wrong while processing your request.");
//                }
//            }

//            return new Tuple<bool, string>(false, "An error occured. Something went wrong while processing your request.");
//        }

//        internal string TransactionId(string sessionKey, string listingId, string reservationId, CommunicationPage currentCommPage = null)
//        {
//            string id = "";
//            try
//            {
//                string url = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?hotel_id={0}&res_id={1}&from_groups=1&ses={2}&lang=xu", listingId, reservationId, sessionKey);
//                if (currentCommPage == null)
//                {
//                    commPage.RequestURL = url;
//                    commPage.ReqType = CommunicationPage.RequestType.GET;
//                    commPage.PersistCookies = true;
//                    commPage.Referer = "";
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid)
//                    {
//                        var start = commPage.Html.IndexOf("trans_id\":") + "trans_id\":".Length;
//                        var to = commPage.Html.IndexOf(",\"", start);
//                        id = commPage.Html.Substring(start, to - start);
//                        id = id.Trim().TrimEnd(",\"".ToChar());
//                        return id;
//                    }
//                    else
//                    {

//                    }
//                }
//                else
//                {
//                    currentCommPage.RequestURL = url;
//                    currentCommPage.ReqType = CommunicationPage.RequestType.GET;
//                    currentCommPage.PersistCookies = true;
//                    currentCommPage.Referer = "";
//                    currentCommPage.RequestHeaders = new Dictionary<string, string>();
//                    ProcessRequest(currentCommPage);
//                    if (currentCommPage.IsValid)
//                    {
//                        var start = currentCommPage.Html.IndexOf("trans_id\":\"") + "trans_id\":\"".Length;
//                        var to = currentCommPage.Html.IndexOf("\"", start);
//                        id = currentCommPage.Html.Substring(start, to - start);
//                        id = id.Trim().TrimEnd("\"".ToChar());
//                        return id;
//                    }
//                    else
//                    {

//                    }
//                }

//            }
//            catch (Exception e)
//            {
//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }

//            return id;
//        }

//        public void UpdateReservationActionData(ApplicationDbContext db, string sessionKey, string reservationId, string propertyId)
//        {
//            try
//            {
//                var detailUrl = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?lang=xu&ses={0}&hotel_id={1}&res_id={2}", sessionKey, propertyId, reservationId);
//                commPage.RequestURL = detailUrl;
//                commPage.ReqType = CommunicationPage.RequestType.GET;
//                commPage.PersistCookies = true;
//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {
//                    var xd = commPage.ToXml();
//                    var cardStatus = "";
//                    var cardViewDesc = "";
//                    var paymentStatus = "";
//                    var ccAlertHeading = "";
//                    var ccAlertDesc = "";
//                    var cardLis = xd.SelectNodes("//ul[contains(@class,'js-credit-card-status')]/li");
//                    if (cardLis != null)
//                    {
//                        if (cardLis.Count == 2)
//                        {
//                            cardStatus = cardLis[0].GetInnerTextFromNode();
//                            cardViewDesc = cardLis[1].GetInnerTextFromNode();
//                        }
//                        else if (cardLis.Count == 1)
//                            cardViewDesc = cardLis[0].GetInnerTextFromNode();

//                    }
//                    #region Reply Actions

//                    List<ReplyActionCreditCard> actions = new List<ReplyActionCreditCard>();
//                    var changeDateNode = xd.SelectSingleNode("//div[@id='change_dates']/h4");
//                    if (changeDateNode != null)
//                    {
//                        var name = changeDateNode.GetInnerTextFromNode();
//                        var isDisabled = changeDateNode.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, ActionType = ReservationActionType.ChangeReservationDatesAndPrices });
//                    }

//                    var markNoShowNode = xd.SelectSingleNode("//div[@id='mark_noshow']/h4");
//                    if (markNoShowNode != null)
//                    {
//                        var name = markNoShowNode.GetInnerTextFromNode();
//                        var isDisabled = markNoShowNode.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, ActionType = ReservationActionType.MarkAsNoShow });
//                    }

//                    var reportGuestNode = xd.SelectSingleNode("//div[@id='flag_misconduct']/h4");
//                    if (reportGuestNode != null)
//                    {
//                        var name = reportGuestNode.GetInnerTextFromNode();
//                        var isDisabled = reportGuestNode.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, ActionType = ReservationActionType.ReportMisconduct });
//                    }
//                    var cancelReservationtNode = xd.SelectSingleNode("//div[@id='request_cancellation']/h4");
//                    if (cancelReservationtNode != null)
//                    {
//                        var name = cancelReservationtNode.GetInnerTextFromNode();
//                        var isDisabled = cancelReservationtNode.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, ActionType = ReservationActionType.RequestToCancelReservation });
//                    }

//                    var ccInvalidNode = xd.SelectSingleNode("//div[@id='mark_cc_invalid']/h4");
//                    if (ccInvalidNode != null)
//                    {
//                        var name = ccInvalidNode.GetInnerTextFromNode();
//                        var isDisabled = ccInvalidNode.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, ActionType = ReservationActionType.CC_MarkInvalid });
//                    }

//                    var ccAddNew = xd.SelectSingleNode("//div[@id='new_cc_provided']/h4");
//                    if (ccAddNew != null)
//                    {
//                        var name = ccAddNew.GetInnerTextFromNode();
//                        var isDisabled = ccAddNew.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, ActionType = ReservationActionType.CC_MarkNewCardAdded });
//                    }

//                    var ccViewNode = xd.SelectSingleNode("//div[@id='view_cc_details']/h4");
//                    if (ccViewNode != null)
//                    {
//                        var name = ccViewNode.GetInnerTextFromNode();
//                        var isDisabled = ccViewNode.GetAttributeFromNode("class").Contains("disabled");

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, Description = cardViewDesc, ActionType = ReservationActionType.CC_ViewDetail });
//                    }
//                    var paymentStatusNode = xd.SelectSingleNode("//div[@id='payment_status_block']");
//                    if (paymentStatusNode != null)
//                    {
//                        paymentStatus = xd.SelectSingleNode("//select[@id='payment_status']//option[@selected]").GetInnerTextFromNode();
//                        var name = "Payment Status";
//                        var isDisabled = false;

//                        var options = xd.SelectNodes("//select[@id='payment_status']//option");
//                        Dictionary<string, string> metaData = new Dictionary<string, string>();
//                        foreach (XmlNode item in options)
//                        {
//                            var val = item.GetAttributeFromNode("value").ToSafeString();
//                            var optionName = item.NextSibling.GetInnerTextFromNode();
//                            metaData.Add(val, optionName);
//                        }

//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, MetaData = metaData, ActionType = ReservationActionType.CC_UpdatePaymentStatus });
//                    }

//                    var cardDetailsNode = xd.SelectSingleNode("//div[@class='credit-card-section js-credit-card-section']");
//                    if (cardDetailsNode != null)
//                    {
//                        var alert = cardDetailsNode.SelectSingleNode(".//div[@role='alert']");
//                        if (alert != null)
//                        {
//                            ccAlertHeading = alert.SelectSingleNode(".//strong").GetInnerTextFromNode();
//                            ccAlertDesc = alert.SelectSingleNode("./p").GetInnerTextFromNode();
//                        }

//                    }
//                    var cancelReservation_NoNewCC = xd.SelectSingleNode("//div[@id='no_new_cc_provided']/h4");
//                    if (cancelReservation_NoNewCC != null)
//                    {
//                        var name = cancelReservation_NoNewCC.GetInnerTextFromNode();
//                        var isDisabled = cancelReservation_NoNewCC.GetAttributeFromNode("class").Contains("disabled");
//                        var desc = xd.SelectSingleNode("//div[@id='no_new_cc_provided']//div[@class='cancel-booking__limit js-cancellation-limit']").GetInnerTextFromNode();
//                        actions.Add(new ReplyActionCreditCard { Name = name, IsDisabled = isDisabled, Description = desc, ActionType = ReservationActionType.CC_CancelReservation_BecauseNoNewCardAdded });
//                    }

//                    #endregion

//                    Core.API.Booking.com.CreditCards.UpdateActionData(db, reservationId, cardStatus, cardViewDesc, paymentStatus, ccAlertHeading, ccAlertDesc, JsonConvert.SerializeObject(actions));
//                }
//            }
//            catch (Exception e)
//            {


//            }
//        }

//        public Tuple<bool, string> RequestCancelBooking(string token, string listingId, string reservationId)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);

//            if (isLoaded)
//            {
//                try
//                {
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);

//                    commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/cancellation_request.html?lang=xu&hotel_id={0}&ses={1}", listingId, session.SessionKey);
//                    commPage.Parameters = new Dictionary<string, string>();
//                    commPage.Parameters.Add("res_id", reservationId);
//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.POST;
//                    commPage.RequestHeaders = new Dictionary<string, string>();

//                    ProcessRequest(commPage);

//                    if (commPage.IsValid)
//                    {
//                        commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/cancellation_request.html?lang=xu&ses={0}&hotel_id={1}", session.SessionKey, listingId);
//                        //commPage.JsonStringToPost = string.Format("{{\"action\":\"request_cancellation\", \"request_cancellation_reason\":\"4\", \"res_id\":\"{0}\"}}", reservationId);
//                        commPage.Parameters = new Dictionary<string, string>();
//                        commPage.Parameters.Add("action", "request_cancellation");
//                        commPage.Parameters.Add("request_cancellation_reason", "4");
//                        commPage.Parameters.Add("res_id", reservationId);
//                        commPage.PersistCookies = true;
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

//                        ProcessRequest(commPage);

//                        if (commPage.IsValid && commPage.Html.Contains("Cancellation request sent"))
//                        {
//                            return new Tuple<bool, string>(true, "Cancellation request sent. Please remember that the reservation remains valid until you receive a notification that the guest has agreed.");
//                        }
//                    }
//                }
//                catch (Exception)
//                {
//                    return new Tuple<bool, string>(false, "An error occured, something went wrong while processing your request.");
//                }
//            }

//            return new Tuple<bool, string>(false, "An error occured, something went wrong while processing your request.");
//        }

//        public JObject CheckAvailabilityAndPrice(string token, string reservationId, string listingId, DateTime checkInDate, DateTime checkOutDate)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);

//            if (isLoaded)
//            {
//                var session = Core.API.Booking.com.Hosts.GetHostSession(token);

//                string resDataParam = string.Format("{{\"{0}\":{{\"checkin\":\"{1}\",\"checkout\":\"{2}\"}}}}", reservationId, checkInDate.ToString("yyyy-MM-dd"), checkOutDate.ToString("yyyy-MM-dd"));

//                commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/json/confirmdates.json?res_id={0}&ses={1}&hotel_id={2}&lang=xu", reservationId, session.SessionKey, listingId);
//                commPage.ReqType = CommunicationPage.RequestType.POST;
//                commPage.PersistCookies = true;
//                commPage.RequestHeaders = new Dictionary<string, string>();
//                commPage.Parameters = new Dictionary<string, string>();
//                commPage.Parameters.Add("res_data", resDataParam);

//                ProcessRequest(commPage);

//                if (commPage.IsValid)
//                {
//                    // Avoid too much request above this part, commPage is being manipulated by another
//                    // request, causing json not to be parsed.
//                    return JObject.Parse(commPage.Html);
//                }
//            }

//            return null;
//        }

//        public Tuple<bool, string> ChangeAvailabilityAndPrice(string token, string listingId, string confirmationCode,
//            string reservationId, DateTime checkInDate, DateTime checkOutDate, double price)
//        {
//            // the whole method is working even without csrf token.
//            var isLoaded = ValidateTokenAndLoadCookies(token);
//            if (isLoaded)
//            {
//                try
//                {
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                    var csrfToken = Helper_GetCSRFToken(session.SessionKey, listingId, confirmationCode);

//                    commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html");

//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.POST;
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    commPage.Parameters = new Dictionary<string, string>();
//                    commPage.Parameters.Add("lang", "xu");
//                    commPage.Parameters.Add("ses", session.SessionKey);
//                    commPage.Parameters.Add("hotel_id", listingId);
//                    commPage.Parameters.Add("action", "change_dates");
//                    commPage.Parameters.Add("csrf_token", csrfToken);
//                    commPage.Parameters.Add("res_id", confirmationCode);
//                    commPage.Parameters.Add("rres_affected", reservationId);
//                    // MM/DD/YYYY format is not working in here, better use dashes.
//                    commPage.Parameters.Add("checkin_date", checkInDate.ToString("yyyy-MM-dd"));
//                    commPage.Parameters.Add("checkout_date", checkOutDate.ToString("yyyy-MM-dd"));
//                    commPage.Parameters.Add("newprice", price.ToString());

//                    commPage.Referer = "";
//                    ProcessRequest(commPage);

//                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("error_processing_action=0"))
//                    {
//                        using (var db = new ApplicationDbContext())
//                        {
//                            UpdateReservationActionData(db, session.SessionKey, confirmationCode, listingId);
//                        }

//                        return new Tuple<bool, string>(true, "Success!");
//                    }
//                }
//                catch (Exception e)
//                {
//                    return new Tuple<bool, string>(false, "An error occured while processing your request.");
//                }
//            }

//            return new Tuple<bool, string>(false, "An error occured while processing your request.");
//        }

//        public Tuple<bool, string> MarkAsNoShow(string token, string listingId, string bookingReferenceNumber, string reservationId)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);
//            if (isLoaded)
//            {
//                try
//                {
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                    var csrfToken = Helper_GetCSRFToken(session.SessionKey, listingId, bookingReferenceNumber);

//                    commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?hotel_id={0}&res_id={1}&lang=xu&ses={2}", listingId, bookingReferenceNumber, session.SessionKey);

//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.POST;
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    commPage.Parameters = new Dictionary<string, string>();
//                    commPage.Parameters.Add("action", "no_show");
//                    commPage.Parameters.Add("csrf_token", csrfToken);
//                    commPage.Parameters.Add("rres_affected", reservationId);

//                    commPage.Referer = "";
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("error_processing_action=0"))
//                    {
//                        using (var db = new ApplicationDbContext())
//                        {
//                            UpdateReservationActionData(db, session.SessionKey, bookingReferenceNumber, listingId);
//                        }

//                        return new Tuple<bool, string>(true, "Success!");
//                    }


//                }
//                catch (Exception e)
//                {
//                    return new Tuple<bool, string>(false, "An error occured while processing your request.");
//                }
//            }

//            return new Tuple<bool, string>(false, "An error occured while processing your request.");
//        }

//        #endregion

//        #endregion

//        #region CALENDAR

//        public List<string> GetRateIds(string sessionKey, DateTime startDate, DateTime endDate, string listingId, string roomId)
//        {
//            List<string> ids = new List<string>();
//            try
//            {
//                commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/fetch?ses={0}&hotel_id={1}&lang=xu", sessionKey, listingId);

//                commPage.PersistCookies = true;
//                commPage.ReqType = CommunicationPage.RequestType.POST;
//                var json = string.Format("{{\"dates\":{{\"range\":true,\"dates\":[\"{0}\",\"{1}\"]}},\"hotel\":{{\"fields\":[\"rooms\"],\"rooms\":{{\"id\":[\"{2}\"],\"fields\":[\"rates\"],\"rates\":{{\"fields\":[\"name\",\"status\"]}}}}}}}}", DateTime.UtcNow.ToString("yyyy-MM-dd"), DateTime.UtcNow.AddMonths(6).ToString("yyyy-MM-dd"), roomId);
//                commPage.RequestHeaders = new Dictionary<string, string>();
//                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                commPage.Parameters = new Dictionary<string, string>();
//                commPage.Parameters.Add("request", json);
//                commPage.Referer = "";
//                ProcessRequest(commPage);
//                if (commPage.IsValid)
//                {

//                    var ob = JObject.Parse(commPage.Html);

//                    var hotels = ob["data"]["hotel"];

//                    var rates = (JObject)ob["data"]["hotel"][listingId]["rooms"][roomId]["rates"];
//                    var rateIds = rates.Properties().Select(f => f.Name).ToList();
//                    return rateIds;

//                }
//            }
//            catch
//            {

//            }
//            return ids;
//        }

//        public bool UpdateBlockDates(string token, bool isAvailable, DateTime startDate, DateTime endDate, long listingId, long roomId)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);

//            if (isLoaded)
//            {
//                try
//                {
//                    var blockValue = isAvailable ? "true" : "false";
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                    var rateIds = GetRateIds(session.SessionKey, startDate, endDate, listingId.ToString(), roomId.ToString());

//                    foreach (var rateId in rateIds)
//                    {
//                        commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/update?lang=xu&ses={0}&hotel_id={1}", session.SessionKey, listingId);

//                        commPage.PersistCookies = true;
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        var json = string.Format("{{\"update\":[{{\"room_id\":\"{0}\",\"rate_id\":\"{1}\",\"field_name\":\"closed\",\"field_value\":{4},\"from_date\":\"{2}\",\"until_date\":\"{3}\"}}]}}", roomId, rateId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), blockValue);
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                        commPage.Parameters = new Dictionary<string, string>();
//                        commPage.Parameters.Add("request", json);
//                        commPage.Referer = "";
//                        ProcessRequest(commPage);
//                        if (commPage.IsValid)
//                        {

//                            var ob = JObject.Parse(commPage.Html);

//                            var isSuccessful = ob["success"].ToSafeString() == "1" ? true : false;

//                            /* if (!string.IsNullOrEmpty(status))
//                             {
//                                 //Refresh db
//                                 ScrapeCalendar_Reservations(token, listingTriad, startDate.AddDays(-1), endDate.AddDays(2));
//                                 return true;

//                             }
//                             */
//                        }
//                    }
//                    /*
//                    We can do it in one request, but there is an issue, the status that is set individually can only be changed indivisually

//                    commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/update?lang=xu&ses={0}&hotel_id={1}", tokenResult.Item2.SessionKey, listingId);

//                    commPage.PersistCookies = true;
//                    commPage.ReqType = CommunicationPage.RequestType.POST;
//                    var json = string.Format("{{\"update\":[{{\"room_id\":\"{0}\",\"field_name\":\"hidden\",\"field_value\":{3},\"from_date\":\"{1}\",\"until_date\":\"{2}\"}}]}}", roomId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), blockValue);
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                    commPage.Parameters = new Dictionary<string, string>();
//                    commPage.Parameters.Add("request", json);
//                    commPage.Referer = "";
//                    ProcessRequest(commPage);
//                    if (commPage.IsValid)
//                    {

//                        var ob = JObject.Parse(commPage.Html);

//                        result = ob["success"].ToSafeString() == "1" ? true : false;

//                        if (!string.IsNullOrEmpty(status))
//                         {
//                             //Refresh db
//                             ScrapeCalendar_Reservations(token, listingTriad, startDate.AddDays(-1), endDate.AddDays(2));
//                             return true;

//                         }

//                    }
//                */


//                }
//                catch (Exception e)
//                {
//                    return false;
//                }
//            }
//            return true;
//        }

//        public Tuple<bool, string> SetRates(string token, double price, DateTime startDate, DateTime endDate, long listingId, long roomId)
//        {
//            var isLoaded = ValidateTokenAndLoadCookies(token);

//            if (isLoaded)
//            {
//                try
//                {
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                    commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/fetch?hotel_id={0}&lang=xu&ses={1}", listingId, session.SessionKey);
//                    commPage.ReqType = CommunicationPage.RequestType.POST;
//                    var json = string.Format("{{\"dates\":{{\"range\":true,\"dates\":[\"{0}\",\"{1}\"]}},\"hotel\":{{\"fields\":[\"rooms\"],\"rooms\":{{\"id\":[\"{2}\"],\"fields\":[\"permissions\",\"name\",\"num_guests\",\"rates\"],\"rates\":{{\"fields\":[\"permissions\",\"name\",\"status\",\"price\",\"restrictions\",\"promotions\"]}}}}}}}}", DateTime.UtcNow.ToString("yyyy-MM-dd"), DateTime.UtcNow.AddYears(1).ToString("yyyy-MM-dd"), roomId);//"2018-06-18", "2018-06-24", roomId);
//                    commPage.RequestHeaders = new Dictionary<string, string>();
//                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                    commPage.Parameters = new Dictionary<string, string>();
//                    commPage.Parameters.Add("request", json);
//                    commPage.Referer = "";

//                    ProcessRequest(commPage);

//                    if (commPage.IsValid)
//                    {
//                        string standardRateId = "";
//                        var ob = JObject.Parse(commPage.Html);

//                        standardRateId = ((JArray)ob["data"]["hotel"][listingId.ToString()]["rooms"][roomId.ToString()]["rate_ids"]).First.ToSafeString();
//                        //var rates = ob["data"]["hotel"][listingId.ToString()]["rooms"][roomId.ToString()]["rates"];
//                        //standardRateId = rates.First["name"].tosafe
//                        //foreach (var rateId in rateIds)
//                        //{
//                        //    var rates = ob["data"]["hotel"][listingId.ToString()]["rooms"][roomId.ToString()]["rates"];
//                        //    if (rates[rateId.ToString()]["name"].ToSafeString() == "Standard Rate")
//                        //    {
//                        //        standardRateId = rateId.ToSafeString();
//                        //        break;
//                        //    }
//                        //}

//                        //commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/availability_calendar.html?hotel_id={0}&ses={1}&lang=xu", listingId, session.SessionKey);
//                        //commPage.ReqType = CommunicationPage.RequestType.GET;

//                        //ProcessRequest(commPage);

//                        commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/update?lang=xu&ses={0}&hotel_id={1}", session.SessionKey, listingId);

//                        commPage.PersistCookies = true;
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        json = string.Format("{{\"update\":[{{\"room_id\":\"{0}\",\"rate_id\":\"{1}\",\"occupancy\":1,\"field_name\":\"price\",\"field_value\":\"{2}\",\"from_date\":\"{3}\",\"until_date\":\"{4}\"}}]}}", roomId, standardRateId, price, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                        commPage.Parameters = new Dictionary<string, string>();
//                        commPage.Parameters.Add("request", json);
//                        commPage.Referer = "";
//                        ProcessRequest(commPage);

//                        if (commPage.IsValid)
//                        {
//                            ob = JObject.Parse(commPage.Html);
//                            var success = ob["success"].ToSafeString() == "1" ? true : false;

//                            return new Tuple<bool, string>(success, "Success");
//                        }
//                    }
//                }
//                catch (Exception e)
//                {

//                }
//            }
//            return new Tuple<bool, string>(false, "An error occured");
//        }

//        //public bool SetMinimumStay(string token, string minStay, string rateId, DateTime startDate, DateTime endDate, string listingId, string roomId)
//        //{
//        //    var tokenResult = ValidateTokenAndLoadCookies(token);
//        //    if (tokenResult.Item1)
//        //    {
//        //        try
//        //        {

//        //            commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/update?lang=xu&ses={0}&hotel_id={1}", tokenResult.Item2.SessionKey, listingId);

//        //            commPage.PersistCookies = true;
//        //            commPage.ReqType = CommunicationPage.RequestType.POST;
//        //            var json = string.Format("{{\"update\":[{{\"room_id\":\"{0}\",\"rate_id\":\"{1}\",\"field_name\":\"min_stay_through\",\"field_value\":\"{2}\",\"from_date\":\"{3}\",\"until_date\":\"{4}\"}}]}}", roomId, rateId, minStay, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
//        //            commPage.RequestHeaders = new Dictionary<string, string>();
//        //            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//        //            commPage.Parameters = new Dictionary<string, string>();
//        //            commPage.Parameters.Add("request", json);
//        //            commPage.Referer = "";
//        //            ProcessRequest(commPage);
//        //            if (commPage.IsValid)
//        //            {

//        //                var ob = JObject.Parse(commPage.Html);

//        //                var isSuccessful = ob["success"].ToSafeString().ToBoolean();
//        //            }



//        //        }
//        //        catch (Exception e)
//        //        {

//        //        }
//        //    }
//        //    return false;
//        //}

//        #region CALENDAR RATES AND AVAILABILITY

//        public List<Core.Database.Entity.PropertyBookingDate> ScrapeCalendar(string token, string listingId)
//        {
//            var isloaded = ValidateTokenAndLoadCookies(token);
//            if (isloaded)
//            {
//                try
//                {
//                    var session = Core.API.Booking.com.Hosts.GetHostSession(token);
//                    var roomIds = Core.API.Booking.com.Properties.GetRoomIds(listingId.ToLong());
//                    var calendarList = new List<Core.Database.Entity.PropertyBookingDate>();

//                    foreach (var roomId in roomIds)
//                    {
//                        commPage.RequestURL = string.Format("https://admin.booking.com/fresa/extranet/inventory/fetch?ses={0}&hotel_id={1}&room_id={2}&lang=xu", session.SessionKey, listingId, roomId);

//                        commPage.PersistCookies = true;
//                        commPage.ReqType = CommunicationPage.RequestType.POST;
//                        var json = string.Format("{{\"dates\":{{\"range\":true,\"dates\":[\"{0}\",\"{1}\"]}},\"hotel\":{{\"fields\":[\"rooms\"],\"rooms\":{{\"id\":[\"{2}\"],\"fields\":[\"permissions\",\"name\",\"num_guests\",\"rates\"],\"rates\":{{\"fields\":[\"permissions\",\"name\",\"status\",\"price\",\"restrictions\",\"promotions\"]}}}}}}}}", DateTime.UtcNow.ToString("yyyy-MM-dd"), DateTime.UtcNow.AddYears(1).ToString("yyyy-MM-dd"), roomId);//"2018-06-18", "2018-06-24", roomId);
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
//                        commPage.Parameters = new Dictionary<string, string>();
//                        commPage.Parameters.Add("request", json);
//                        commPage.Referer = "";

//                        ProcessRequest(commPage);

//                        if (commPage.IsValid)
//                        {
//                            var ob = JObject.Parse(commPage.Html);

//                            var hotels = ob["data"]["hotel"];
//                            var rates = (JObject)ob["data"]["hotel"][listingId]["rooms"][roomId.ToString()]["rates"];
//                            var rateIds = rates.Properties().Select(f => f.Name).ToList();

//                            foreach (var id in rateIds)
//                            {
//                                var rate = rates[id];
//                                var rateId = id;
//                                var name = rate["name"].ToSafeString();

//                                if (name == "Standard Rate")
//                                {
//                                    var dates = (JObject)rate["dates"];
//                                    var dateNames = dates.Properties().Select(f => f.Name).ToList();

//                                    foreach (var d in dateNames)
//                                    {
//                                        // These are the status of each dates
//                                        // ( closed, soldout, bookable )

//                                        // if status == bookable, then it is available
//                                        // else, it is not

//                                        var date = dates[d];
//                                        var dt = d.ToDateTime();
//                                        var isAvailable = date["status"].ToSafeString() == "bookable" ? true : false;
//                                        var prices = (JObject)date["price"];
//                                        var priceNames = prices.Properties().Select(f => f.Name).ToList();
//                                        priceNames = priceNames.OrderBy(f => f).ToList();
//                                        var price = priceNames.Count > 1 ? prices["1"].ToDouble() : prices[priceNames[0]].ToDouble();

//                                        calendarList.Add(new Core.Database.Entity.PropertyBookingDate
//                                        {
//                                            PropertyId = listingId.ToLong(),
//                                            Date = dt,
//                                            IsAvailable = isAvailable,
//                                            Price = price,
//                                            RoomId = roomId,
//                                            CompanyId = GlobalVariables.CompanyId,
//                                            SiteType = 3
//                                        });
//                                    }
//                                }
//                            }
//                        }
//                    }

//                    calendarList = calendarList.OrderBy(c => c.Date).ToList();
//                    return calendarList;
//                }
//                catch (Exception e)
//                {
//                    return new List<Core.Database.Entity.PropertyBookingDate>();
//                }
//            }
//            return new List<Core.Database.Entity.PropertyBookingDate>();
//        }

//        public class Avail_Price
//        {
//            public string Status { get; set; }
//            public string Price { get; set; }
//            public string RateId { get; set; }
//            public List<GuestPrice> ExtraGuestRates { get; set; }
//        }

//        public class GuestPrice
//        {
//            public int NoOfGuests { get; set; }
//            public string Price { get; set; }
//        }

//        #endregion

//        public enum UpdateBlockType
//        {
//            Block,
//            UnBlock
//        }

//        #endregion

//        #region HELPERS

//        public bool ValidateTokenAndLoadCookies(string token, CommunicationPage currentCommPage = null)
//        {
//            var loadCookiesResult = LoadCookies(token, currentCommPage);
//            if (loadCookiesResult.Item1)
//            {
//                BasicUserProfile profile = new Models.BasicUserProfile();

//                // changed the url instead of having 2 parameters (with hotelId)
//                string url = string.Format("https://admin.booking.com/hotel/hoteladmin/index-hotel.html?ses={0}&lang=en", loadCookiesResult.Item2.SessionKey);
//                try
//                {
//                    if (currentCommPage == null)
//                    {
//                        commPage.RequestURL = url;
//                        commPage.PersistCookies = true;
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.ReqType = CommunicationPage.RequestType.GET;
//                        ProcessRequest(commPage);

//                        // check url if it has "/hotel/" in it instead of "/manage/"
//                        if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/hotel/"))
//                        {
//                            return true;
//                        }

//                    }
//                    else
//                    {
//                        currentCommPage.RequestURL = url;
//                        currentCommPage.PersistCookies = true;
//                        currentCommPage.RequestHeaders = new Dictionary<string, string>();
//                        currentCommPage.ReqType = CommunicationPage.RequestType.GET;
//                        ProcessRequest(currentCommPage);

//                        if (currentCommPage.IsValid && currentCommPage.Uri.AbsoluteUri.Contains("/hotel/"))
//                        {
//                            return true;
//                        }
//                    }
//                }
//                catch (Exception e) { }
//            }

//            return false;
//        }

//        public Tuple<bool, Core.Database.Entity.HostSessionCookie> LoadCookies(string token, CommunicationPage currentCommPage = null)
//        {
//            var result = false;
//            var session = Core.API.Booking.com.Hosts.GetHostSession(token);

//            if (session != null)
//            {
//                try
//                {
//                    var cookies = JsonConvert.DeserializeObject<List<Cookie>>(session.Cookies);
//                    CookieCollection cc = new CookieCollection();
//                    foreach (var cok in cookies)
//                    {
//                        cc.Add(cok);
//                    }
//                    CookieContainer cont = new CookieContainer();

//                    cont.Add(cc);
//                    commPage.COOKIES = cont;
//                    if (currentCommPage != null)
//                        currentCommPage.COOKIES = cont;

//                    commPage.Token = session.Token;
//                    result = true;
//                }
//                catch (Exception e)
//                {
//                    return new Tuple<bool, Core.Database.Entity.HostSessionCookie>(false, null);
//                }
//            }
//            else
//            {
//                commPage.COOKIES = null;
//            }

//            return new Tuple<bool, Core.Database.Entity.HostSessionCookie>(result, session);
//        }

//        public CommunicationPage InitializeCommunication(string username, string password)
//        {
//            var newCommPage = new CommunicationPage("https://admin.booking.com/");
//            try
//            {
//                var session = Core.API.Booking.com.Hosts.GetHostSession(username, password);
//                if (session != null)
//                {

//                    var cookies = JsonConvert.DeserializeObject<List<Cookie>>(session.Cookies);
//                    CookieCollection cc = new CookieCollection();
//                    foreach (var cok in cookies)
//                    {
//                        //if (cok.Name == "auth_token")
//                        //    continue;

//                        cc.Add(cok);
//                    }
//                    CookieContainer cont = new CookieContainer();

//                    cont.Add(cc);
//                    newCommPage.COOKIES = cont;
//                    newCommPage.Token = session.Token;

//                }
//            }
//            catch (Exception e)
//            {
//                ElmahLogger.LogError(e, GetCurrentMethod());
//            }

//            return newCommPage;
//        }

//        private string Helper_GetCsrfToken(string htmlPage)
//        {
//            var start = htmlPage.IndexOf("var token                = '") + "var token                = '".Length;
//            var to = htmlPage.IndexOf("'", start);
//            var tokenValue = htmlPage.Substring(start, to - start);
//            tokenValue = tokenValue.Trim().TrimEnd("'".ToChar());
//            return tokenValue;
//        }

//        private string Helper_GetCsrfToken(bool fromDefaultPage = false, string token = "", string sessionKey = "")
//        {
//            try
//            {
//                if (fromDefaultPage)
//                {
//                    var isLoaded = ValidateTokenAndLoadCookies(token);

//                    if (isLoaded)
//                    {
//                        commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/index-hotel.html?perform_routing=1&ses={0}&lang=xu", sessionKey);
//                        commPage.ReqType = CommunicationPage.RequestType.GET;
//                        commPage.RequestHeaders = new Dictionary<string, string>();
//                        commPage.PersistCookies = true;
//                        commPage.Referer = "";

//                        ProcessRequest(commPage);

//                        if (commPage.IsValid)
//                        {
//                            var start = commPage.Html.IndexOf("var token                = '") + "var token                = '".Length;
//                            var to = commPage.Html.IndexOf("'", start);
//                            var tokenValue = commPage.Html.Substring(start, to - start);
//                            tokenValue = tokenValue.Trim().TrimEnd("'".ToChar());
//                            return tokenValue;
//                        }
//                    }
//                }
//                else
//                {
//                    var start = commPage.Html.IndexOf("var token                = '") + "var token                = '".Length;
//                    var to = commPage.Html.IndexOf("'", start);
//                    var tokenValue = commPage.Html.Substring(start, to - start);
//                    tokenValue = tokenValue.Trim().TrimEnd("'".ToChar());
//                    return tokenValue;
//                }
//            }
//            catch (Exception e)
//            {

//            }

//            return "";
//        }

//        private string Helper_GetCSRFToken(string sessionKey, string listingId, string reservationId)
//        {
//            commPage.RequestURL = string.Format("https://admin.booking.com/hotel/hoteladmin/extranet_ng/manage/booking.html?ses={0}&lang=xu&hotel_id={1}&res_id={2}", sessionKey, listingId, reservationId);

//            commPage.PersistCookies = true;
//            commPage.ReqType = CommunicationPage.RequestType.GET;
//            commPage.RequestHeaders = new Dictionary<string, string>();
//            ProcessRequest(commPage);
//            if (commPage.IsValid)
//            {
//                var token = Helper_GetCsrfToken();
//                return token;

//            }
//            return string.Empty;
//        }

//        public static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
//        {
//            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
//            foreach (DictionaryEntry element in k)
//            {
//                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
//                foreach (var e in l)
//                {
//                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
//                    foreach (Cookie fc in cl)
//                    {
//                        yield return fc;
//                    }
//                }
//            }
//        }

//        [MethodImpl(MethodImplOptions.NoInlining)]
//        public string GetCurrentMethod()
//        {
//            StackTrace st = new StackTrace();
//            StackFrame sf = st.GetFrame(1);

//            var method = sf.GetMethod().Name;
//            return method;
//        }

//        #endregion

//        #region DISPOSE

//        public void Dispose()
//        {
//            Dispose(true);
//            GC.SuppressFinalize(this);
//        }

//        protected virtual void Dispose(bool disposing)
//        {
//            if (disposed)
//                return;

//            if (disposing)
//            {
//                handle.Dispose();
//            }
//            disposed = true;
//        }

//        #endregion
//    }
//}